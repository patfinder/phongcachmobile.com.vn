
REM Source: https://www.dev.ais.msu.edu/_env_docs/pmst_resources/ProjectFolderStructure&NamingConvention%2004.21.10.doc

REM * Project Management
REM 	Project Background
REM 	Project Proposal
REM 	Project Charter
REM 	Stakeholder Analysis
REM 	Resource Management
REM 	Project Management Approach
REM 	Project Plans
REM 	Risk Management
REM 	Project Change Management
REM 	Team Meetings
REM 	Communication Plan
REM 	Post Project Closure
REM 	Issue Management
REM 	Budget
REM * RFP
REM * Vendor Management
REM * Requirements
REM * Design and Technical Specifications
REM * Technical Reviews
REM 	Requirement
REM 	Design
REM 	Code
REM * Development
REM * User Interface
REM * Database
REM * Security
REM * Testing
REM * Implementation


mkdir "Project Management"
cd "Project Management"

mkdir "Project Background"
mkdir "Project Proposal"
mkdir "Project Charter"
mkdir "Stakeholder Analysis"
mkdir "Resource Management"
mkdir "Project Management Approach"
mkdir "Project Plans"
mkdir "Risk Management"
mkdir "Project Change Management"
mkdir "Team Meetings"
mkdir "Communication Plan"
mkdir "Post Project Closure"
mkdir "Issue Management"
mkdir "Budget"

cd ..
mkdir "RFP"
mkdir "Vendor Management"
mkdir "Requirements"
mkdir "Design and Technical Specifications"
mkdir "Technical Reviews"

cd "Technical Reviews"
mkdir "Requirement"
mkdir "Design"
mkdir "Code"

cd ..
mkdir "Development"
mkdir "User Interface"
mkdir "Database"
mkdir "Security"
mkdir "Testing"
REM mkdir "Implementation"