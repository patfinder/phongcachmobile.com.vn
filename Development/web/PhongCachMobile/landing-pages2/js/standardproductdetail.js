var template_standard_2014 = (function() {
  //"use strict"; //unable to use strict for functions are declared randomly throughout the code.

  if ($('body').hasClass('template_standard_2014') || $('body').hasClass('template_standard_2014_dots')) { 
      
      // mobile detector
      function mobile() {
          var viewportWidth = $(window).width();
          if (viewportWidth < 720 /*HtcChrome.mobileMaxWidth*/) {
              return true;
          } else {
              return false;
          }
      }

      // hide the video thumbnails strip and upper title if there's only one video
      function hideVideoThumbs() {
          var numVideos = $('.video-thumbs-container .video-thumbs li').length;
          if (numVideos == 1) {
              $('.video-thumbs-container, .video-title').hide();
              $('.video-area').addClass('alt');
          }
      }

      // function: insert video
      function insertVideo(videoID, videoPlayer, type) {
          var autoplayYoutube;
          var autoplayYouku;
          var videoString;
          if (type == 'autoplay') {
              autoplayYoutube = '1';
              autoplayYouku = 'true';
          } else {
              autoplayYoutube = '0';
              autoplayYouku = 'false';
          }
          if (videoPlayer == 'youtube') {
              videoString = '<iframe id="video-player" type="text/html" src="//www.youtube.com/embed/' 
              + videoID + '?enablejsapi=1&html5=1&rel=0&modestbranding=1&showinfo=0&autoplay=' 
              + autoplayYoutube + '&start=0&wmode=opaque" frameborder="0" allowfullscreen="1"></iframe>';
          } else {
              videoString = '<iframe id="video-player" src="http://player.youku.com/embed/' 
              + videoID + '?autoplay=' + autoplayYouku + '" frameborder="0" allowfullscreen="1"></iframe>';
          }
          $('.video-area').html(videoString);
      }

      // pause active video when user navigates away
      function pauseVideoTips() {
          if ($('#video-player').length > 0) {
              callPlayer('video-player', 'pauseVideo');
          }
      }

      //show buttom area : footer-button

      $(window).load(function() {
          HtcChrome.detectFooterButton();
      });
      HtcChrome.detectFooterButton();
  }
      
  /* SECTION: 360 */
  if (mobile() == false && $('.sketchfab-info').length !== 0) {      
      // color interactions
      $('.palette a').hover(function() {
          $('.palette a').removeClass('active');
          $(this).addClass('active');
          var colorName = $(this).attr('data-name');
          $('.color-name').html(colorName);
          var colorIndex = $(this).index() + 1;
          $('.phone-preview img').hide();
          $('.phone-preview img:nth-child(' + colorIndex + ')').show();
      });
  }
      
  /* SECTION: ACCESSORIES */
  $(window).resize(function() {
    if($(this).height() < 715) {
      $('.product-thumbs li').each(function(index) {
        if(index > 3) {
          $(this).hide();
        }
      });
    } else {
      $('.product-thumbs li').each(function(index) {
        if(index > 3) {
          $(this).show();
        }
      });    
    }
  });

  /* SECTION: REVIEWS */
  if ($('#frame-reviews .ratings').length > 0) {
      $('.ratings .rating-property').each(function() {
          var hasScore = $(this).find('.score').html();
          if (hasScore == '') { // remove review category if the category doesn't exist
              $(this).hide();
          }
      });
      var starsInsert = $('.ratings .stars-insert').html(); // fetch stars insert
      $('.ratings .ratings-stars .stars').append(starsInsert); // copy stars insert into each rating category
      $('.ratings .stars-insert').remove(); // remove starts insert
      var starBlockWidth = $('.ratings .rating-property:first-child .stars .star.first').width(); // width of a single star block
      var starBlockGapWidth = $('.ratings .rating-property:first-child .stars .star.first').css('border-right-width'); // width of the gaps in between stars   
      if (starBlockGapWidth !== undefined) {
          starBlockGapWidth = parseInt(starBlockGapWidth.replace('px',''));
      } 
      var ratingSum = 0; // var used to add up all the ratings
      $('.ratings-stars .score').each(function() { // loop through each rating category
          var score = $(this).html(); // get total score
          var scoreInt = Math.floor(score); // get base integer score
          var scoreDecimal = score - scoreInt; // get only the decimal score
          // calculations for the green bar's width
          var barMovement = Math.round((scoreInt * starBlockWidth) + (scoreInt * starBlockGapWidth) + (scoreDecimal * starBlockWidth)) + 'px';
          $(this).next('.green-bar').css('width',barMovement); // display the green bar
          ratingSum += Number(score); // calculate total sum of all categories
          return ratingSum;
      });
      // reformat value
      $('.overall-rating span.average').text( parseFloat($('.overall-rating span.average').text()).toFixed(1) );    
  }

  /* SNAP SCROLL FRAMEWORK AND GLOBAL INTERACTIONS */
  // signup email processing
  $('.email-signup form.signup-form input.submit-button').click(function (e) {
      // prevent default behavior
      if (e.preventDefault) { e.preventDefault(); } else { e.returnValue = false; }

      var reviewSite = new Site();

      reviewSite.emailAddPDP = $(this).parent().find('input.launch-email').val();

      if (isEmail(reviewSite.emailAddPDP)) { // email is valid
          $('form.signup-form input.launch-email').removeClass('invalid');

          var product_id = $('meta[name=product_name_id]').attr('content');

          product_id = (typeof product_id === 'undefined') ? '' : product_id;
          var cta_id = (product_id == '') ? 'review-form' : (product_id+'-signup'),
              pname = (product_id == '') ? '' : (product_id.replace(/-/g, ' '));


          reviewSite.subscriptionSubmission('pdp', cta_id, product_id, pname);  

      } else { // email NOT valid
          $('.email-signup .error-message').show();
          $('form.signup-form input.launch-email').addClass('invalid');
      } 
  }); // END: email sign-up submission


  // remove videos in certain situations
  if (!$('html').hasClass('csstransforms3d') || $('html').hasClass('touch') || (mobile() == true)) {
      $('video').remove();
  }
      
  
    // variables
    var slideCount = $('.slide-nav > ul > li:visible').length;

    // set first dot on right nav to active state
    $('.slide-nav ul li:first-child').addClass('active');

    // set the right nav data attributes
    function reorderDotNav() {
        var totalDots = $('.slide-nav ul li').length;
        for (var i = 1; i <= totalDots; i++) {
            $('.slide-nav ul li:nth-child(' + i + ')').attr('data-slide', i);
        }
    }

    function adjustSlides() {          
        if ($('html').hasClass('touch')) {
            var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
            if (isSafari) {
                /*
                    var tabletHeight = $(window).height() - 12;
                $('html').css({
                    'height': tabletHeight
                });
                
                    $('.base-nav').css({
                        'bottom':'12px'
                  });
                  $('.advance-slide').css({
                        'bottom':'37px'
                  });
                */
            }    
        }

        //var listCount = $('.slide-nav ul li').size();
        //listCount = typeof listCount == 'undefined'? 0 : listCount;
        //if (listCount > 0) {
            var windowHeight = $(window).height();
            $('section.slide').css('height', windowHeight);
            var totalHeight = windowHeight * slideCount;
            $('.slide-wrapper').css('height', totalHeight);
            var slideNumber = $('.slide-nav ul li.active').attr('data-slide');
            var moveHeight = '-' + (windowHeight * (slideNumber - 1)) + 'px';
            $('.slide-wrapper').css('margin-top', moveHeight);
        //}
    }

    // function: snap scroll triggers (behaviors that occur after a snap scroll is completed)
    function snapScrollTriggers(section) {
        if ($('video').length > 0) {
            $('video').each(function() {
                $(this).get(0).pause();
            });
        }
        if ($('video[data-section=' + section + ']').length > 0) {
            $('video[data-section=' + section + ']').each(function() {
                $(this).get(0).play();
            });
        }
        if ($('#video-player').length > 0) {
            pauseVideoTips();
        }
    }

    // function: check if snap scrolling is currently in progress
    function scrollAnimating() {
        if ($('.slide-wrapper').is(':animated')) {
            return true;
        } else {
            return false;
        }
    }

    // function: check for conditions that should temporarily disable snap scroll navigation of all types
    function noSnapConditions() {
        if ($('.specs-container-overlay').is(':visible')) {
            return true;
        } else {
            return false;
        }
    }

    // function: check for conditions that should temporarily disable snap scroll navigation via mouse scroll or keyboard
    function noSnapConditionsMouseKeyboard() {
        return false;
    }

    // function: invert the right nav if it blends in too much with the background
    function invertNav(section) {
        var navState = $('.slide-nav ul li[data-slide=' + section + ']').attr('data-nav');
        if (navState == 'invert') {
            $('.slide-nav').addClass('invert');
        } else {
            $('.slide-nav').removeClass('invert');
        }
    }

    // function: display the header only when on the first slide
    function headerDisplay(section) {
        if (section == 1) {
            $('#htc-header').fadeIn(500);
            $('.info-bar, .social-icons').removeClass('top');
            $('.info-bar .logo-small').removeClass('active');
        } else {
            $('#htc-header').fadeOut(500);
            $('.info-bar, .social-icons').addClass('top');
            $('.info-bar .logo-small').addClass('active');
        }
    }

    // function: animate the snap scroll
    function snapScroll(section, speed) {
        if (scrollAnimating() == false && noSnapConditions() == false) {
            if (speed == undefined) {
                speed = 1100;
            }
            var windowHeight = $(window).height();
            var slideHeight = windowHeight * (section - 1);
            $('.slide-wrapper').animate({
                marginTop: '-' + slideHeight
            }, speed, 'easeOutCubic', function() {
                snapScrollTriggers(section);
            });
            invertNav(section);
            headerDisplay(section);
            HtcChrome.detectFooterButton();
        }
    }

    // function: scroll up or down by one slide
    function slideOnce(direction) {
        if (scrollAnimating() == false && noSnapConditions() == false) {
            var currentDot = $('.slide-nav ul li.active').attr('data-slide');
            if (direction == 'up') {
                if (currentDot <= slideCount && currentDot > 1) {
                    var scrollDot = $('.slide-nav ul li.active').removeClass('active').prev().addClass('active').attr('data-slide');
                    snapScroll(scrollDot);
                }
            } else {
                if (currentDot < slideCount) {
                    var scrollDot = $('.slide-nav ul li.active').removeClass('active').next().addClass('active').attr('data-slide');
                    snapScroll(scrollDot);
                }
            }
        }
    }

    // specs adjustments
    function adjustSpecs() {
        var currentSlide = $('.slide-nav ul li.active').index() + 1;
        var windowHeight = $(window).height();
        var specsTop;
        var specsHeight;
        var headerHeight =  $('#htc-header').height();
        var infoBarHeight = $('.info-bar').height();
        var keepSpace = 52;

        if (currentSlide == 1) {
            specsTop = (headerHeight + infoBarHeight)  + 'px';
            specsHeight = windowHeight - (headerHeight + infoBarHeight + keepSpace);
        } else {
            specsTop = (infoBarHeight)  + 'px';
            specsHeight = windowHeight - (infoBarHeight + keepSpace);
        }
        $('.specs-container').css({
            'top' : specsTop
        });
        $('.specs-container .specs-list').css({
            'height' : specsHeight
        });
    }

    // social sharing - insert correct URL of the page
    function socialSharingURLs() {
        var pageURL = document.URL;
        var facebookHREF = 'https://www.facebook.com/sharer/sharer.php?u=' + pageURL;
        var twitterHREF = 'https://twitter.com/share?url=' + pageURL;
        var googleHREF = 'https://plus.google.com/share?url=' + pageURL;
        $('.social-icons .facebook').attr('href', facebookHREF);
        $('.social-icons .twitter').attr('href', twitterHREF);
        $('.social-icons .google').attr('href', googleHREF);
    }

    // remove tabbed nav if there's only one slide
    function removeTabs() {
        $('ul.base-nav').each(function() {
            var navItems = $(this).find('li').length;
            if (navItems == 1) {
                $(this).remove();
            }
        });
    }

    // anchor white bar and specs to correct position
    function anchorBar() {
        var currentScrollOffset = $(window).scrollTop();
        if (currentScrollOffset >= 48) {
            $('.info-bar').addClass('top');
            $('.specs-container').removeAttr('style');
        } else {
            $('.info-bar').removeClass('top');
            var specsAdjustment = 88 - currentScrollOffset;
            $('.specs-container').css({
                'top' : specsAdjustment
            });
        }
    }

    // anchor white info bar
    function anchorInfoBar() {
        var scrollPosition = $(window).scrollTop();
        if (scrollPosition >= 84) {
            $('.info-bar').addClass('anchor');
        } else {
            $('.info-bar').removeClass('anchor');
        }
    }

    // show or hide back to top button
    function backToTop() {
        var scrollPosition = $(window).scrollTop();
        if (scrollPosition > 1200) {
            $('.back-to-top').addClass('active');
        } else {
            $('.back-to-top').removeClass('active');
        }   
    }

    var template_standard_2014 =  {
        init: function() {
            if ($('body').hasClass('template_standard_2014') || $('body').hasClass('template_standard_2014_dots')) { 

                if ($('.video-area').length !== 0) {                   
                    hideVideoThumbs();
                    //hideVideoThumbs();

                    // display first video by default
                    $('ul.video-thumbs li:first-child').addClass('active');
                    var videoID = $('ul.video-thumbs li:first-child').attr('data-video');
                    var videoPlayer = $('ul.video-thumbs li:first-child').attr('data-videoplayer');
                    insertVideo(videoID, videoPlayer, 'no-autoplay');
                    var videoTitle = $('ul.video-thumbs li:first-child').find('span').html();
                    $('.video-title').html(videoTitle);

                    // video thumbnail interactions
                    $('ul.video-thumbs li').unbind('click').click(function() {
                        if (!$(this).hasClass('active')) {
                            $('ul.video-thumbs li').removeClass('active');
                            $(this).addClass('active');
                            var videoID = $(this).attr('data-video');
                            var videoPlayer = $(this).attr('data-videoplayer');
                            insertVideo(videoID, videoPlayer, 'autoplay');
                            var videoTitle = $(this).find('span').html();
                            $('.video-title').html(videoTitle);
                        }
                    });
                }
                
                /* SECTION: 360 */
                if (mobile() == false && $('.sketchfab-info').length !== 0) {
                    // color interactions
                    $('.palette a').hover(function() {
                        $('.palette a').removeClass('active');
                        $(this).addClass('active');
                        var colorName = $(this).attr('data-name');
                        $('.color-name').html(colorName);
                        var colorIndex = $(this).index() + 1;
                        $('.phone-preview img').hide();
                        $('.phone-preview img:nth-child(' + colorIndex + ')').show();
                    });
                }

                /* SECTION: ACCESSORIES */
                $(window).resize(function() {
                  if($(this).height() < 715) {
                    $('.product-thumbs li').each(function(index) {
                      if(index > 3) {
                        $(this).hide();
                      }
                    });
                  } else {
                    $('.product-thumbs li').each(function(index) {
                      if(index > 3) {
                        $(this).show();
                      }
                    });    
                  }
                });

                /* SECTION: REVIEWS */

                // HD-3247 :: inject the #form=review to open review modal
                var linkStr = $('.write-review, .call-to-action').attr('href');
                
                var id = -1;
                if(linkStr != undefined && linkStr.indexOf('form=') != undefined){
                  id = linkStr.indexOf('form=');
                
                  var isLinkReviewPage = (linkStr.indexOf('reviews') >= 0) ? true : false;

                  if (isLinkReviewPage && id < 0) { // need to add hashbang  
                    var reviewModal = linkStr + '#form=review';

                    $('.write-review, .call-to-action').attr('href', reviewModal);
                  } // END: IF check for hashbang 
                }
                
                if ($('#frame-reviews .ratings').length > 0) {
                    $('.ratings .rating-property').each(function() {
                        var hasScore = $(this).find('.score').html();
                        if (hasScore == '') { // remove review category if the category doesn't exist
                            $(this).hide();
                        }
                    });
                    var starsInsert = $('.ratings .stars-insert').html(); // fetch stars insert
                    $('.ratings .ratings-stars .stars').append(starsInsert); // copy stars insert into each rating category
                    $('.ratings .stars-insert').remove(); // remove starts insert
                    var starBlockWidth = $('.ratings .rating-property:first-child .stars .star.first').width(); // width of a single star block
                    var starBlockGapWidth = $('.ratings .rating-property:first-child .stars .star.first').css('border-right-width'); // width of the gaps in between stars   
                    if (starBlockGapWidth !== undefined) {
                        starBlockGapWidth = parseInt(starBlockGapWidth.replace('px',''));
                    } 
                    var ratingSum = 0; // var used to add up all the ratings
                    $('.ratings-stars .score').each(function() { // loop through each rating category
                        var score = $(this).html(); // get total score
                        var scoreInt = Math.floor(score); // get base integer score
                        var scoreDecimal = score - scoreInt; // get only the decimal score
                        // calculations for the green bar's width
                        var barMovement = Math.round((scoreInt * starBlockWidth) + (scoreInt * starBlockGapWidth) + (scoreDecimal * starBlockWidth)) + 'px';
                        $(this).next('.green-bar').css('width',barMovement); // display the green bar
                        ratingSum += Number(score); // calculate total sum of all categories
                        return ratingSum;
                    });
                    // reformat value
                    $('.overall-rating span.average').text( parseFloat($('.overall-rating span.average').text()).toFixed(1) );  

                    // bazaar voice api
                    var bazaarID = $('.bazaar-id').html();
                    if (bazaarID !== undefined) {
                        var bazaarIDbase = bazaarID.split('_')[0];
                        var actualRegion = $('body').attr('data-site');
                        var parentRegion = $('body').attr('data-parent-site');
                        var bazaarParentID = bazaarIDbase + '_' + parentRegion;
                        $.ajax({
                            type: "GET",
                            url: "http://api.bazaarvoice.com/data/reviews.xml?passkey=e9ybm3vhfhr7zzgeiy9djl2po&Include=Products&apiversion=5.4&Stats=reviews&Filter=ProductId:" + bazaarID,
                            dataType: "xml",
                            success: function (xml) {
                                var recommendCount = 0;
                                var notRecommendCount = 0;
                                $(xml).find('Product#' + bazaarID + ' RecommendedCount').each(function() {
                                    recommendCount = recommendCount + parseInt($(this).text());
                                });
                                $(xml).find('Product#' + bazaarID + ' NotRecommendedCount').each(function() {
                                    notRecommendCount = notRecommendCount + parseInt($(this).text());
                                });
                                if (recommendCount == 0) { // meaning that the actual regions does not have any reviews, so we use parent region
                                    $(xml).find('Product#' + bazaarParentID + ' RecommendedCount').each(function() {
                                        recommendCount = recommendCount + parseInt($(this).text());
                                    });
                                    $(xml).find('Product#' + bazaarParentID + ' NotRecommendedCount').each(function() {
                                        notRecommendCount = notRecommendCount + parseInt($(this).text());
                                    });
                                }
                                
                                var percentageRecommended = Math.round((recommendCount / (recommendCount + notRecommendCount)) * 100);
                                //$('.ratings-info .recommended .percentage').html(percentageRecommended);
                                
                                if (isNaN(percentageRecommended) || percentageRecommended == 0) {
                                
                                } else {
                                    $('.ratings-info .recommended .percentage').html(percentageRecommended);
                                };

                            }
                        });  
                    }
                }


                /* SNAP SCROLL FRAMEWORK AND GLOBAL INTERACTIONS */
                    
                // remove videos in certain situations
                if (!$('html').hasClass('csstransforms3d') || $('html').hasClass('touch') || (mobile() == true)) {
                    $('video').remove();
                }


                if (mobile() == false && $('html').hasClass('csstransforms3d')) {

                    // set first dot on right nav to active state
                    $('.slide-nav ul li:first-child').addClass('active');


                    reorderDotNav();

                    // right nav tool tips
                    if (!$('html').hasClass('touch')) {
                        $('.slide-nav ul li').hover(function() {
                            $(this).find('.label').stop().fadeIn(150);
                        }, function() {
                            $(this).find('.label').stop().fadeOut(150);
                        });
                    }

                    // establish slide dimensions
                    
                    $(window).load(function() {
                        adjustSlides();
                    });
                    $(window).resize(function() {
                        adjustSlides();
                    });
                    adjustSlides();                      
                    
                    // navigation: clicking the reviews shortcut on the white bar
                    $('.reviews-shortcut').unbind('click').click(function(e) {
                        e.preventDefault();
                        if (scrollAnimating() == false) {
                            $('.specs-toggle').removeClass('active');
                            $('.specs-container').fadeOut(300);
                            $('.specs-container-overlay').hide();
                            $('.slide-nav ul li').removeClass('active');
                            var reviewsLocation = $('.slide-wrapper section#slide-reviews').index() + 1;
                            $('.slide-nav ul li:nth-child(' + reviewsLocation + ')').addClass('active');
                            snapScroll(reviewsLocation);
                        }
                    });
                    
                    // navigation: clicking the advance slide prompt
                    $('.advance-slide').unbind('click').click(function() {
                        slideOnce('down');
                    });
                    
                    //Footer interaction: Click the button to get the footer to animate in
                    /*
                    $('.footer-button').click(function(){
                        $('#htc-footer').css({"display":"inherit", "position":"absolute"});
                        $('#htc-footer').show();
                        $('#htc-footer').animate({
                            bottom: "0"
                        }, 300);
                        $('.chrome-overlay').fadeIn(300);
                    });
                    */                      
                    
                    // navigation: clicking the back to top button on the last slide or logo in white bar
                    $('.info-bar .logo-small').unbind('click').click(function() {
                        if (scrollAnimating() == false) {
                            $('.specs-toggle').removeClass('active');
                            $('.specs-container').fadeOut(300);
                            $('.specs-container-overlay').hide();
                            // $('.page-overlay').css({"display":"initial"});
                        }
                        if (scrollAnimating() == false && !$('.slide-nav ul li:first-child').hasClass('active')) {
                            $('.slide-nav ul li').removeClass('active');
                            $('.slide-nav ul li:first-child').addClass('active');
                            snapScroll(1);
                        }
                    });
                    
                    // navigation: clicking right nav dots
                    $('.slide-nav ul li').unbind('click').click(function() {
                        if (scrollAnimating() == false && noSnapConditions() == false) {
                            if (!$(this).hasClass('active')) {
                                $('.slide-nav ul li').removeClass('active');
                                $(this).addClass('active');
                                var slideNumber = $(this).attr('data-slide');
                                snapScroll(slideNumber);
                            }
                        }
                    });

                    // navigation: using mousewheel
                    $('body').bind('mousewheel', function(e) {
                        if (scrollAnimating() == false && noSnapConditions() == false && noSnapConditionsMouseKeyboard() == false) {
                            e.preventDefault();
                            if (e.originalEvent.wheelDelta > 0) {
                                slideOnce('up');
                            } else {
                                slideOnce('down');
                            }
                        }
                    });
                    
                    $('body').bind('DOMMouseScroll', function(e) {
                        if (scrollAnimating() == false && noSnapConditions() == false && noSnapConditionsMouseKeyboard() == false) {
                            e.preventDefault();
                            if (e.originalEvent.detail < 0) {
                                slideOnce('up');
                            } else {
                                slideOnce('down');
                            }
                        }
                    });

                    // navigation: using the keyboard
                    $(document).keydown(function(e) {
                        if (!$('textarea, input').is(':focus') && noSnapConditionsMouseKeyboard() == false) {
                            if (e.keyCode == 32 || e.keyCode == 40 || e.keyCode == 34) {
                                e.preventDefault();
                                slideOnce('down');
                            }
                            if (e.keyCode == 38 || e.keyCode == 33) {
                                e.preventDefault();
                                slideOnce('up');
                            }
                        }
                    });

                    // navigation: swipe
                    if ($('html').hasClass('touch')) {
                        $('#htc-header, .info-bar').touchwipe({
                            wipeDown: function() {
                                // null
                            },
                            wipeUp: function() {
                                // null
                            }
                        });
                        $('.slide-wrapper').touchwipe({
                            wipeDown: function() {
                                slideOnce('down');
                            },
                            wipeUp: function() {
                                slideOnce('up');
                            }
                        });
                    }
                    
                    // social icons hover width
                    if ($('.social-icons').length > 0) {
                        var socialIcons = $('.social-icons > .icon').length;
                        var socialLength = socialIcons * 53;
                        $('.social-icons').css('width', socialLength);
                    }
                                          
                    $(window).load(function() {
                        adjustSpecs();
                    });
                    $(window).resize(function() {
                        adjustSpecs();
                    });
                    adjustSpecs();                      
                    
                    // specs toggle
                    $('.specs-toggle').unbind('click').click(function(e) {
                        e.preventDefault();
                        if ($('.specs-container').is(':visible')) {
                            $(this).removeClass('active');
                            $('.specs-container, .specs-container-overlay').fadeOut(300);
                        } else {
                            $(this).addClass('active');
                            adjustSpecs();
                            $('.specs-container, .specs-container-overlay').fadeIn(300);
                        }
                    });

                } // mobile false and modern browsers

                if (mobile() == false) {
                    socialSharingURLs();
                    
                    // set the first tab active on all base-navs    
                    $('ul.base-nav li:first-child').addClass('active');
                    
                    // tabbed nav interactions
                    $('ul.base-nav li').unbind('click').click(function() {
                        if (!$(this).hasClass('active')) {
                            var tabIndex = $(this).index() + 1;
                            var tabContainer = $(this).parent();
                            var frameContainer = $(this).parent().parent().find('.frame-container');
                            $(tabContainer).find('li').not(this).removeClass('active');
                            $(this).addClass('active');
                            frameContainer.find('.frame').fadeOut(300);
                            setTimeout(function() {
                                frameContainer.find('.frame:nth-child(' + tabIndex + ')').fadeIn();
                            }, 300);
                            setTimeout(function() {
                                if ($('#video-player').length > 0) {
                                    pauseVideoTips();
                                }
                            }, 600);
                        }
                    });
                                          
                    removeTabs();

                } // mobile false and all browsers

                if (mobile() == true) {

                    $(window).load(function() {
                        anchorBar();
                    });
                    $(window).resize(function() {
                        anchorBar();
                    });
                    $(window).scroll(function() {
                        anchorBar();
                    });
                    anchorBar();
                    
                    // specs toggle
                    $('.specs-toggle').unbind('click').click(function() {
                        if ($('.specs-container').is(':visible')) {
                            $(this).removeClass('active');
                            $('.specs-container').removeClass('show');
                        } else {
                            $(this).addClass('active');
                            $('.specs-container').addClass('show');
                            // adjust specs spacing on mobile
                            $('.specs-list .column').each(function() {
                                if ($(this).height() == 0) {
                                    $(this).remove();
                                }
                            });
                        }
                    });
                    
                    // close specs when left side menu is open (specs are fixed position, so it would not move at all)
                    $('.hamburger').unbind('click').click(function() {
                        $('.specs-toggle').removeClass('active');
                        $('.specs-container').removeClass('show');
                    });
                    

                    $(window).scroll(function() {
                        backToTop();
                    });
                    backToTop();
                    
                    // scroll back to top
                    $('.back-to-top').unbind('click').click(function() {
                        $('html, body').animate({
                            scrollTop: 0
                        }, 500);
                    });
                    
                    // reviews shortcut 
                    $('.reviews-shortcut').unbind('click').click(function(e) {
                        e.preventDefault();
                        $('.specs-toggle').removeClass('active');
                        $('.specs-container').removeClass('show');
                        var reviewsOffset = $('#slide-reviews').offset().top;
                        $('html, body').animate({
                            scrollTop: reviewsOffset
                        }, 500);
                    });
                    
                    // take user to the top of the videos section after selecting a video
                    $('.video-thumbs li').unbind('click').click(function() {
                        var videoOffset = $('#frame-video-tips').offset().top - 40;
                        $('html, body').animate({
                            scrollTop: videoOffset
                        }, 300);
                    });

                } // mobile true

                /* SECTION: OLD BROWSERS */
                if ($('html').hasClass('no-csstransforms3d') && $('html').hasClass('no-touch')) {
                    
                    $(window).load(function() {
                        anchorInfoBar();
                    });
                    $(window).resize(function() {
                        anchorInfoBar();
                    });
                    $(window).scroll(function() {
                        anchorInfoBar();
                    });
                    anchorInfoBar();
                    
                    // white info bar interactions
                    $('.info-bar .logo-small').unbind('click').click(function(e) {
                        e.preventDefault();
                        $('html, body').scrollTop(0);
                    });
                    $('.info-bar .reviews-shortcut').unbind('click').click(function(e) {
                        e.preventDefault();
                        var elementPosition = $('#slide-reviews').offset().top - 40;
                        $('html, body').scrollTop(elementPosition);
                    });
                    $('.info-bar .specs-toggle').unbind('click').click(function(e) {
                        e.preventDefault();
                        var elementPosition = $('.specs-container').offset().top - 40;
                        $('html, body').scrollTop(elementPosition);
                    });

                }

                //set header & footer via common function from HtcChrome
                //HtcChrome.setHeaderFooterButton();
            }
        }
    };
    
    template_standard_2014.init();

    return template_standard_2014;

})();


//$(document).ready(function() {
  //template_standard_2014.init();
//});