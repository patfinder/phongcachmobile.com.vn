﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="landing-page2.aspx.cs" Inherits="PhongCachMobile.landing_pages.landing_page2" %>

<!DOCTYPE html>

<html lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>HTC One M9 | ĐT thông minh One | HTC [USA] | HTC Việt Nam</title>
    <meta charset="UTF-8">
    <meta content="#" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!<![endif]-->
    <link rel="canonical" href="./htc-one-m9_files/htc-one-m9.html">
    <!--[if IE]><! -->
    <script type="text/javascript">
        /*@cc_on
         @if (@_jscript_version == 10)
         document.documentElement.className += ' ie10';
         @elif (@_jscript_version == 9)
         document.documentElement.className += ' ie9';
         @elif (@_jscript_version == 5.8)
         document.documentElement.className += ' ie8';
         @else
         document.documentElement.className += ' lt-ie8';
         @end
         @*/
    </script>
    <!--<![endif] -->
    <script type="text/javascript">
        /**
         * PREVENT CLICKJACKING
         */
        if (self != top) {
            top.location = self.location;
        }
        var regionType = 'noecomm';
    </script>
    <style></style>
    <link href="css/main.css" type="text/css" rel="stylesheet">
    <style>
        /* style overrides for custom background images */
        body.template_standard_2014_dots .main section.slide#slide-hero {
            background-image: url('images/htc-one-m9-hero-bg.jpg');
        }

        body.template_standard_2014_dots .main section.slide#slide-hero .hero-slide {
            background-image: url('images/htc-one-m9-hero-bg.jpg');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-1 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-2 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-3 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-4 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-5 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-6 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-7 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-8 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-360 {
            background-image: url('images/htc-one-m9-global-ksp-sketchfab.jpg');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-reviews {
            background-image: url('images/htc-one-m9-reviews.jpg');
        }

        @media only screen and (max-width: 720px) {
            /* mobile */
            body.template_standard_2014_dots .main section.slide#slide-hero {
                background-image: url('images/htc-one-m9-hero-bg-mobile.jpg');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-1 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-2 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-3 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-4 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-5 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-6 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-7 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-8 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-360 {
                background-image: url('images/htc-one-m9-global-ksp-sketchfab-mobile.jpg');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-reviews {
                background-image: url('images/htc-one-m9-reviews-mobile.jpg');
            }

        }
    </style>
    <script src="js/jquery-2.1.4.min.js" class="chrome-plugin-js"></script>


    <link href="css/htc.chrome.ltr.css" type="text/css" rel="stylesheet" id="chrome-css">
    <style>
        /* style overrides for custom background images */
        body.template_standard_2014_dots .main section.slide#slide-hero {
            background-image: url('images/htc-one-m9-hero-bg.jpg');
        }

        body.template_standard_2014_dots .main section.slide#slide-hero .hero-slide {
            background-image: url('images/htc-one-m9-hero-bg.jpg');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-1 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-2 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-3 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-4 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-5 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-6 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-7 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-promo-8 {
            background-image: url('');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-360 {
            background-image: url('images/htc-one-m9-global-ksp-sketchfab.jpg');
        }

        body.template_standard_2014_dots .main section.slide .frame#frame-reviews {
            background-image: url('images/htc-one-m9-reviews.jpg');
        }

        @media only screen and (max-width: 720px) {
            /* mobile */
            body.template_standard_2014_dots .main section.slide#slide-hero {
                background-image: url('images/htc-one-m9-hero-bg-mobile.jpg');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-1 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-2 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-3 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-4 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-5 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-6 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-7 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-promo-8 {
                background-image: url('');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-360 {
                background-image: url('images/htc-one-m9-global-ksp-sketchfab-mobile.jpg');
            }

            body.template_standard_2014_dots .main section.slide .frame#frame-reviews {
                background-image: url('images/htc-one-m9-reviews-mobile.jpg');
            }

        }
    </style>
    <!-- RAW CONTENT BEGIN -->
    <style type="text/css">

        body.template_standard_2014 .main section.slide .header h3, html.template_standard_2014_dots .main section.slide .header h3 {
            display: none;
        }

        .bg1-1 {
            background-image: url('images/htc-one-m9-global-ksp-award-winning-design.jpg');
        }

        .bg1-2 {
            background-image: url('images/htc-one-m9-global-ksp-all-metal-elegance.jpg');
        }

        .bg1-3 {
            background-image: url('images/htc-one-m9-global-ksp-refinement-with-an-edge.jpg');
        }

        .bg2-1 {
            background-image: url('images/htc-one-m9-global-ksp-premium-made-personal.jpg');
        }

        .bg2-2 {
            background-image: url('images/htc-one-m9-global-ksp-location-based-homescreen.jpg');
        }

        .bg2-3 {
            background-image: url('images/htc-one-m9-global-ksp-your-world-in-a-glance.jpg');
            background-position: initial !important;
        }

        .bg3-1 {
            background-image: url('images/htc-one-m9-global-ksp-brilliant-pics.jpg');
        }

        .bg3-2 {
            background-image: url('images/htc-one-m9-global-ksp-sensational-selfies.jpg');
        }

        .bg3-3 {
            background-image: url('images/htc-one-m9-global-ksp-easy-photo-access.jpg');
        }

        .bg4-1 {
            background-image: url('images/htc-one-m9-global-ksp-best-audio-just-got-better.jpg');
        }

        .bg4-2 {
            background-image: url('images/htc-one-m9-global-ksp-multimedia-control.jpg');
        }

        .bg4-3 {
            background-image: url('images/htc-one-m9-global-ksp-play-it-in-high-def.jpg');
        }

        .bg8-1 {
            background-image: url('images/htc-one-m9-sign-up.jpg');
        }

        @media only screen and (max-width: 720px) {

            #slide-hero {
                background-size: 100% !important;
            }

            body.template_standard_2014 .main section.slide .header h3, html.template_standard_2014_dots .main section.slide .header h3 {
                display: block;
            }

            .bg1-1 {
                background-image: url('images/htc-one-m9-global-ksp-award-winning-design-mobile.jpg');
                background-size: 100% !important;
            }

            .bg1-2 {
                background-image: url('images/htc-one-m9-global-ksp-all-metal-elegance-mobile.jpg');
                background-size: 100% !important;
            }

            .bg1-3 {
                background-image: url('images/htc-one-m9-global-ksp-refinement-with-an-edge-mobile.jpg');
                background-size: 100% !important;
            }

            .bg2-1 {
                background-image: url('images/htc-one-m9-global-ksp-premium-made-personal-mobile.jpg');
                background-size: 100% !important;
            }

            .bg2-2 {
                background-image: url('images/htc-one-m9-global-ksp-location-based-homescreen-mobile.jpg');
                background-size: 100% !important;
            }

            .bg2-3 {
                background-image: url('images/htc-one-m9-global-ksp-your-world-in-a-glance-mobile.jpg');
                background-size: 100% !important;
            }

            .bg3-1 {
                background-image: url('images/htc-one-m9-global-ksp-brilliant-pics-mobile.jpg');
                background-size: 100% !important;
            }

            .bg3-2 {
                background-image: url('images/htc-one-m9-global-ksp-sensational-selfies-mobile.jpg');
                background-size: 100% !important;
            }

            .bg3-3 {
                background-image: url('images/htc-one-m9-global-ksp-easy-photo-access-mobile.jpg');
                background-size: 100% !important;
            }

            .bg4-1 {
                background-image: url('images/htc-one-m9-global-ksp-best-audio-just-got-better-mobile.jpg');
                background-size: 100% !important;
            }

            .bg4-2 {
                background-image: url('images/htc-one-m9-global-ksp-multimedia-control-mobile.jpg');
                background-size: 100% !important;
            }

            .bg4-3 {
                background-image: url('images/htc-one-m9-global-ksp-play-it-in-high-def-mobile.jpg');
                background-size: 100% !important;
            }

            .bg8-1 {
                background-image: url('images/htc-one-m9-sign-up-mobile.jpg');
                background-size: 100% !important;
            }
        }
    </style>

    <script src="js/iframe_api"></script>
    <script src="js/json3.js"></script>
    <script src="js/NWPLegacy_v2.js"></script>
    <script src="js/bloomfilter.js"></script>
    <link rel="stylesheet" type="text/css" href="css/reset.css" class="ver1352274">
    <link rel="stylesheet" type="text/css" href="css/foicons.css" class="ver2781104">
    <link rel="stylesheet" type="text/css" href="css/dealsRightRibbon.css" class="ver3008190">
    <link rel="stylesheet" type="text/css" href="css/displayWindow.css" class="ver7126817">
    <link rel="stylesheet" type="text/css" href="css/cssImages.css" class="ver6491011">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" class="ver3541750">
    <link rel="stylesheet" type="text/css" href="css/multiButton.css" class="ver8952819">

</head>
<body data-page=" htc-one-m9 template_standard_2014_dots" data-parent-site="" data-locale="vi" data-site="vn"
      data-version="1.0.44" data-env="prod" class="vn  htc-one-m9 template_standard_2014_dots ltr"
      s7792703540823217841="1" jhjlijpomuhn_9="1">
<div class="mboxDefault" id="mbox-target-global-mbox-1436139476688-284473"
     style="visibility: visible; display: block;"></div>
<!-- RAW CONTENT BEGIN --><!-- RAW CONTENT END --><a name="top"></a>

<div class="htc-chrome" id="htc-site">
    <div class="htc-header" id="htc-header" style="display: block;">
        <header>
            <div class="header-wrapper">
                <div class="d-header">
                    <div class="logo-block"><a href="#"></a></div>
                    <!--<div class="top-navi-block">
                        <ul class="top-navi-wrapper">
                            <li class="top-item signup"><a rel="follow" target="_self"
                                                           href="#"
                                                           class="top-title">ĐĂNG KÝ</a>

                                <div class="popover signup-popover">
                                    <div class="signup-wrapper">
                                        <div class="form-block">
                                            <div class="title">NHẬN THÔNG TIN MỚI NHẤT</div>
                                            <div class="context">Ưu đãi, thông tin và mọi vấn đề bạn quan tâm về HTC.
                                            </div>
                                            <form action="" method="get" id="HtcSignup" novalidate="novalidate"><input
                                                    data-msg-specialchars="Vui lòng nhập địa chỉ email hợp lệ"
                                                    data-msg-required="Vui lòng nhập địa chỉ email hợp lệ"
                                                    data-rule-specialchars="true" data-rule-required="true"
                                                    aria-label="Địa chỉ email" placeholder="Địa chỉ email" type="text"
                                                    maxlength="100" class="input-text" id="emailtext" name="emailtext"
                                                    aria-required="true">

                                                <div class="question-wrapper">
                                                    <div class="context">Bạn đã từng sở hữu một chiếc HTC chưa?</div>
                                                    <div class="YN-wrapper">
                                                        <div class="hidden-area"><input
                                                                data-msg-required="Thông tin bắt buộc"
                                                                data-rule-required="true" name="ownedHtc" id="ownedHtc"
                                                                type="radio" aria-required="true"></div>
                                                        <input value="Có" aria-label="Có" class="btnYN Y" name="yes"
                                                               type="button"><input value="Không" aria-label="Không"
                                                                                    class="btnYN N" name="no"
                                                                                    type="button"></div>
                                                </div>
                                                <div class="btn-yn-error error"></div>
                                                <div class="checker-wrapper">
                                                    <div class="checker"><input aria-label="Có" name="subscriptchecker"
                                                                                id="subscriptchecker" value="y"
                                                                                type="checkbox"><label
                                                            for="subscriptchecker"></label></div>
                                                    <div class="context">Email cho tôi về các ưu đãi chọn lọc từ đối tác
                                                        và ưu đãi đặc biệt của HTC, thông tin sản phẩm và bản tin.
                                                    </div>
                                                </div>
                                                <div class="privacy-wrapper">
                                                    <div class="context">Để xem chính sách bảo
                                                        mật của chúng tôi, vui lòng truy cập <a
                                                                href="#"
                                                                target="_blank">nhấp vào đây</a>.
                                                       </div>
                                                </div>
                                                <div class="submit-wrapper"><input aria-label="Gửi" value="Gửi"
                                                                                   class="poper-submit"
                                                                                   name="signup-submit" type="submit">
                                                </div>
                                            </form>
                                        </div>
                                        <div class="thank-you-wrapper">
                                            <div class="signup-closer"></div>
                                            <div class="envolope-wrapper">
                                                <div class="image-holder-email"></div>
                                                <div class="title">Cảm ơn bạn!</div>
                                            </div>
                                            <div class="context">Chúng tôi đã nhận được thông tin của bạn và sẽ sớm gửi
                                                cập nhật về HTC cho bạn.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> -->
                    <div class="main-navi-block">
                        <ul class="menu-list">
                            <!--<li class="has-sub-menu">
                                <div class="title"><a rel="follow" target="_self"
                                                      href="http://www.htc.com/vn/smartphones/">SẢN PHẨM</a></div>
                                <div class="sub-menu">
                                    <div class="column col-1 permanent-link">
                                        <div class="align-center"><a rel="" target="_self"
                                                                     href="#">__ SẢN
                                            PHẨM
                                        </a><a rel="" target="_self" href="#">__ RE
                                            CAMERA
                                        </a></div>
                                    </div>
                                    <div class="column col-1 standard-product">
                                        <div class="align-center"><a href="#"><img
                                                src="images/htc-one-m9-global-2v-listing.png">

                                            <div class="product-name">
                                                HTC One M9
                                            </div>
                                            <div class="product-desc">Hãy tận hưởng từng phút giây của cuộc sống</div>
                                        </a></div>
                                    </div>
                                    <div class="column col-1 standard-product">
                                        <div class="align-center"><a
                                                href="#"><img
                                                src="images/htc-desire-eye-global-2v-listing-white-red.png">

                                            <div class="product-name">
                                                HTC Desire EYE
                                            </div>
                                            <div class="product-desc">Thay đổi quan điểm của bạn</div>
                                        </a></div>
                                    </div>
                                    <div class="column col-2 product-story"><a
                                            href="#">
                                        <div class="align-center"><img src="images/recamera-story.png">
                                        </div>
                                        <div class="product-name">
                                            Máy ảnh RE
                                        </div>
                                        <div class="product-desc">
                                            Chiếc máy ảnh nhỏ xinh đặc biệt
                                        </div>
                                    </a></div>
                                </div>
                            </li>
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self"
                                                      href="#">PHỤ KIỆN</a></div>
                            </li>
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self"
                                                      href="#">PHẦN MỀM +ỨNG DỤNG</a>
                                </div>
                            </li>
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="#">HỖ
                                    TRỢ</a></div>
                            </li> -->
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="../">HOME</a></div>
                            </li>
                        </ul>
                    </div>
                    <!--<div class="search-block">
                        <div class="align-center">
                            <form class="htc-search-form d-search-form" method="get" name="search"
                                  action="#" id="HtcSearchForm"><input class="submit"
                                                                                                   type="submit"
                                                                                                   name="submit"
                                                                                                   value=""><input
                                    placeholder="Tìm kiếm" aria-label="Tìm kiếm" class="query" type="text"
                                    maxlength="100" name="q"></form>
                        </div>
                    </div> -->
                </div>
                <div class="m-header">
                    <div class="logo-block"><a href="#"></a></div>
                    <div class="main-navi-block">
                        <ul class="menu-list">
                        <!--
                            <li class="has-sub-menu product-tab">
                                <div class="title">SẢN PHẨM</div>
                                <div class="sub-menu"><a rel="follow" target="_self"
                                                         href="#" class="sub-item">SẢN
                                    PHẨM</a><a rel="follow" target="_self" href="#"
                                               class="sub-item">RE CAMERA</a></div>
                            </li>
                            <li class="has-sub-menu has-icon search-tab">
                                <div class="title"></div>
                                <div class="sub-menu">
                                    <div class="align-center">
                                        <form class="htc-search-form m-search-form" method="get" name="search"
                                              action="#"><input class="submit" type="submit"
                                                                                            name="submit"
                                                                                            value=""><input
                                                placeholder="Tìm kiếm" class="query" type="text" maxlength="100"
                                                name="q"></form>
                                    </div>
                                </div>
                            </li>
                            <li class="has-sub-menu has-icon menu-tab">
                                <div class="title"></div>
                                <div class="sub-menu">
                                    <div class="sub-item"><a rel="follow" target="_self"
                                                             href="#"> PHỤ KIỆN</a>
                                    </div>
                                    <div class="sub-item"><a rel="follow" target="_self"
                                                             href="#"> PHẦN MỀM +ỨNG
                                        DỤNG</a></div>
                                    <div class="sub-item"><a rel="follow" target="_self"
                                                             href="#"> HỖ TRỢ</a></div>

                            </li> -->
                            <li class="has-sub-menu has-icon menu-tab">
                            <div class="sub-menu">
                                <div class="sub-item"><a rel="follow" target="_self" href="../">HOME</a></div>
                                                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <section class="htc-main" id="htc-main">
        <div class="main htc-dots">
            <div class="chrome-overlay"></div>
            <div class="info-bar"><h1 class="logo-text"><strong>HTC</strong> One M9</h1>

                <div class="right-info">
                    <div style="text-transform:uppercase;" class="specs-toggle">Thông số kỹ thuật</div>
                    <a style="text-transform:uppercase;" href="#reviews"
                       class="reviews-shortcut">Đánh giá <span class="review-count">(38)</span></a></div>
            </div>
            <div class="info-bar-dummy"></div>
            <div class="social-icons" style="width: 212px;">
                <div class="icon share"></div>
                <a class="icon facebook"
                   href="#"
                   target="_blank"
                   style="background-image: url(images/social-facebook.png);"></a>
                <a class="icon twitter"
                   href="#"
                   target="_blank"></a>
                <a class="icon google"
                   href="#"
                   target="_blank"></a>
            </div>
            <nav class="slide-nav">
                <ul id="slides">
                    <li data-nav="default" class="active" data-slide="1">
                        <div class="label">HTC One M9</div>
                    </li>
                    <li data-nav="default" data-slide="2" class="">
                        <div class="label">THIẾT KẾ</div>
                    </li>
                    <li data-nav="default" data-slide="3" class="">
                        <div class="label">PHẦN MỀM TÙY CHỈNH</div>
                    </li>
                    <li data-nav="default" data-slide="4" class="">
                        <div class="label">MÁY ẢNH</div>
                    </li>
                    <li data-nav="default" data-slide="5" class="">
                        <div class="label">BOOMSOUND</div>
                    </li>
                    <li data-nav="default" data-slide="6" class="">
                        <div class="label">ĐĂNG KÝ NGAY HÔM NAY</div>
                    </li>
                    <li data-nav="default" data-slide="7" class="">
                        <div class="label">ĐĂNG KÝ NGAY HÔM NAY</div>
                    </li>
                </ul>
            </nav>
            <div class="slide-container">
                <div class="slide-wrapper" style="height: 5285px; margin-top: 0px;">
                    <section class="slide" id="slide-hero" style="height: 755px;">
                        <div class="header">
                            <h3>HTC One M9</h3>

                            <h2>TẬN HƯỞNG CUỘC SỐNG</h2>
                        </div>
                        <div class="copy align-left"><img alt="HTC One M9 logo"
                                                          src="images/htc-one-m9-product-logo-rev.png"
                                                          class="logo">

                            <div class="hr"></div>
                            <p>Cặp đôi hoàn hảo thiết kế và phần mềm cùng cao cấp: Phần mềm phát hiện hành vi cung cấp
                                thông tin dựa trên vị trí bạn đang đứng, khi bạn’re đang có mặt ở đó. Thiết kế mang vẻ
                                đẹp vô song có tông màu kép với các cạnh đối xứng tinh tế.</p>

                            <p>Hãy tận hưởng từng phút giây của cuộc sống bằng trải nghiệm phong cách độc đáo nhất.</p>

                            <p></p></div>
                        <div class="email-signup">
                            <div class="email-icon"></div>
                            <div class="sign-up">
                                CÓ! Báo cho tôi biết khi HTC One M9 có mặt trên thị trường
                            </div>
                            <form class="signup-form"><label>Địa chỉ email:</label><!-- IE fix --><input maxlength="40"
                                                                                                         required="required"
                                                                                                         placeholder="Địa chỉ email"
                                                                                                         type="email"
                                                                                                         class="launch-email"><input
                                    value=" " type="submit" class="submit-button"></form>
                            <div data-registration="email-signup" id="registrationsuccess-email"
                                 class="confirmation-message"></div>
                            <div data-registration="email-signup" id="registrationfailure-email" class="error-message">
                                Vui lòng nhập địa chỉ email hợp lệ
                            </div>
                        </div>
                        <div class="hero-slide"></div>
                        <div class="advance-slide"></div>
                        <!-- on the first section ONLY --></section>
                    <section class="slide" style="height: 755px;">
                        <div class="frame-container">
                            <div class="frame frame-custom bg1-1">
                                <div class="header">
                                    <h3 style="color: #333;">
                                        HTC One M9
                                    </h3>

                                    <h2 style="color: #333;">
                                        THU HÚT MỌI ÁNH NHÌN
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    Thiết kế nguyên khối kim loại hai tông màu (dual-tone) với các cạnh đối xứng, vát
                                    nhẹ ở các góc mang lại cảm giác dễ cầm nắm trong lòng bàn tay. Lưng máy unibody uốn
                                    cong, thon gọn làm nổi bật nét đặc trưng riêng của thiết kế dòng sản phẩm HTC One.
                                    <br>
                                    <a href="#"
                                       class="call-to-action green">TÌM HIỂU THÊM</a>
                                </p>
                                </div>
                            </div>
                            <div class="frame" id="frame-360">
                                <video loop="loop" data-section="3">
                                    <source src="" type="video/mp4">
                                    <source src="" type="video/webm">
                                </video>
                                <div class="header">
                                    <h3 style="color: #333;">HTC One M9</h3>

                                    <h2 style="color: #333;">THIẾT KẾ NGUYÊN KHỐI KIM LOẠI SANG TRỌNG &amp; TAO NHÃ</h2>
                                </div>
                                <div class="sketchfab-info">
                                    <div class="phone-preview"><img alt="HTC One M9 Gold on Gold"
                                                                    src="images/htc-one-m9-global-sketchfab-gold.png"><img
                                            alt="HTC One M9 Gold on Silver"
                                            src="images/htc-one-m9-global-sketchfab-silver.png"><img
                                            alt="HTC One M9 Gunmetal Gray"
                                            src="images/htc-one-m9-global-sketchfab-gunmetal.png"></div>
                                    <div class="color-options">
                                        <div class="content"><span class="instructions">BẤM ĐỂ XEM XOAY</span>
<span class="description">
Sản phẩm có 3 màu để lựa chọn: Vàng ánh kim, Bạc viền Vàng và Xám Gunmetal.
</span>

                                            <div class="palette"><a data-name="Gold on Gold" target="_blank"
                                                                    href="https://sketchfab.com/models/74b7d1446e16417bb01db0343c2a8113"
                                                                    class="gold"></a><a data-name="Gold on Silver"
                                                                                        target="_blank"
                                                                                        href="https://sketchfab.com/models/c510adeb84ed46d28b21483d199765b0"
                                                                                        class="sliver two-tone tone-gold"></a><a
                                                    data-name="Gunmetal Gray" target="_blank"
                                                    href="https://sketchfab.com/models/a56838cd3a994d4e932b353fabb02fc3"
                                                    class="dim-gray"></a></div>
                                            <div class="color-name">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="frame frame-custom bg1-3">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        KHÁT KHAO ĐƯỢC SỞ HỮU NGAY
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    Các chi tiết sắp xếp khéo léo bên trong giúp HTC One M9 giữ nguyên kích thước nhỏ
                                    gọn và thiết kế tuyệt đẹp không thể cưỡng lại trên một màn hình 5” sống động. Thiết
                                    bị cũng được tích hợp máy ảnh với ống kính phủ sapphire mang lại độ bền cao và chống
                                    trầy xước.
                                </p>
                                </div>
                            </div>
                        </div>
                        <ul class="base-nav">
                            <li class="active"><span>THU HÚT MỌI ÁNH NHÌN</span></li>
                            <li class=""><span>THIẾT KẾ NGUYÊN KHỐI KIM LOẠI SANG TRỌNG &amp; TAO NHÃ</span></li>
                            <li class=""><span>KHÁT KHAO ĐƯỢC SỞ HỮU NGAY</span></li>
                        </ul>
                    </section>
                    <section class="slide" style="height: 755px;">
                        <div class="frame-container">
                            <div class="frame frame-custom bg2-1">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        ĐỘT PHÁ VỀ PHONG CÁCH
                                    </h2></div>
                                <div class="copy align-left"><p>
                                    HTC One M9 mang lại khả năng cá nhân hoá dễ dàng và phong phú hơn bao giờ hết. Với
                                    ứng dụng HTC Themes tích hợp, bạn có thể thay đổi cho chiếc điện thoại nhìn thêm
                                    sinh động bằng ảnh hoặc thậm chí là hình ảnh trực tuyến yêu thích. Bạn cũng có thể
                                    tuỳ chỉnh từ hình dáng cho đến kích thước biểu tượng, nhạc chuông và thậm chí cả
                                    hình nền của điện thoại.
                                    <br>
                                    <a href="#"
                                       class="call-to-action green">TÌM HIỂU THÊM</a>
                                </p>
                                </div>
                            </div>
                            <div class="frame frame-custom bg2-2">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        DÙ BẠN Ở BẤT CỨ NƠI ĐÂU
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    Widget màn hình chính dựa trên bối cảnh sẽ tự động phát hiện vị trí và cung cấp ứng
                                    dụng dựa trên sự kết hợp vị trí của bạn và lựa chọn bạn yêu thích.
                                </p>

                                    <p>
                                        Nhận công cụ và thông tin dựa vào vị trí của bạn để mang lại những giải pháp cá
                                        nhân hóa dành riêng cho bạn. Với HTC One M9, bạn sẽ luôn giữ kết nối dù ở bất cứ
                                        nơi đâu.
                                    </p>
                                </div>
                            </div>
                            <div class="frame frame-custom bg2-3">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        NHANH NHƯ CHỚP
                                    </h2></div>
                                <div class="copy align-left"><p>
                                    HTC One M9 với BlinkFeed phiên bản nâng cao sẽ đưa ra đề xuất và liên kết dựa trên
                                    vị trí của bạn. Bạn sẽ nhận được những tin tức đặc biệt trong giờ nghỉ trưa tại nơi
                                    bạn đang đến, dù đang ở bất cứ nơi đâu.
                                </p>
                                </div>
                            </div>
                        </div>
                        <ul class="base-nav">
                            <li class="active"><span>ĐỘT PHÁ VỀ PHONG CÁCH</span></li>
                            <li class=""><span>DÙ BẠN Ở BẤT CỨ NƠI ĐÂU</span></li>
                            <li class=""><span>NHANH NHƯ CHỚP</span></li>
                        </ul>
                    </section>
                    <section class="slide" style="height: 755px;">
                        <div class="frame-container">
                            <div class="frame frame-custom bg3-1">
                                <div class="header">
                                    <h3 style="color: #333;">
                                        HTC One M9
                                    </h3>

                                    <h2 style="color: #333;">
                                        AI CŨNG PHẢI THỐT LÊN ĐẦY THÁN PHỤC
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    Máy ảnh chính 20MP và máy ảnh selfie UltraPixel mang lại cho bạn những bức hình và
                                    khả năng chỉnh sửa ảnh chưa bao giờ đẹp và tuyệt vời đến thế. Phần mềm HTC Eye™
                                    Experience cho phép bạn Chụp ảnh kép, Selfie bằng giọng nói, tính năng Make-up trực
                                    tiếp và nhiều tính năng hấp dẫn khác. Ngoài ra, với ứng dụng Zoe, bạn có thể kết hợp
                                    hình ảnh và phim với bạn bè để cho ra đời thước phim từ nhiều góc độ.
                                </p>

                                    <p>
                                        Với hai máy ảnh xuất sắc của HTC One M9, cứ thoải mái tận hưởng ảnh đẹp ngất
                                        ngây.<br>
                                        <a href="#"
                                           class="call-to-action green">TÌM HIỂU THÊM</a>
                                    </p>
                                </div>
                            </div>
                            <div class="frame frame-custom bg3-2">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        ẢNH SELFIE TUYỆT ĐẸP
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    Máy ảnh selfie của HTC One M9 với công nghệ UltraPixel giúp thu được nhiều ánh sáng
                                    hơn, cho những bức ảnh selfie cực kỳ rõ nét dù là ban ngày hay ban đêm. Thêm vào đó,
                                    ống kính góc rộng cũng được trang bị để mang lại tầm nhìn rộng hơn và không bỏ sót
                                    bất cứ ai trong khuôn hình.
                                </p>
                                </div>
                            </div>
                            <div class="frame frame-custom bg3-3">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        TRUY CẬP ẢNH DỄ DÀNG
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    HTC One M9 với tính năng Bộ sưu tập lưu trữ từ đám mây giúp tập hợp và tìm kiếm hình
                                    ảnh và phim một cách dễ dàng. Bạn có thể tìm kiếm ngay trên chiếc điện thoại của
                                    mình những hình ảnh và video đã lưu giữ trên các công cụ lưu trữ đám mây hoặc các
                                    trang trực tuyến khác một cách dễ dàng. Nhờ Bộ sưu tập từ đám mây trên HTC One M9,
                                    chỉ cần vài bước đơn giản là bạn đã có thể lưu lại mọi khoảnh khắc quý giá.
                                </p>
                                </div>
                            </div>
                        </div>
                        <ul class="base-nav">
                            <li class="active"><span>AI CŨNG PHẢI THỐT LÊN ĐẦY THÁN PHỤC</span></li>
                            <li class=""><span>ẢNH SELFIE TUYỆT ĐẸP</span></li>
                            <li class=""><span>TRUY CẬP ẢNH DỄ DÀNG</span></li>
                        </ul>
                    </section>
                    <section class="slide" style="height: 755px;">
                        <div class="frame-container">
                            <div class="frame frame-custom bg4-1">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        ÂM THANH VÒM DOLBY SURROUND
                                    </h2></div>
                                <div class="copy align-left"><p>
                                    Nhờ sự hợp tác với Dolby, HTC One M9 mang lại hiệu ứng âm thanh vòm 5.1 giúp bạn
                                    thực sự tận hưởng trải nghiệm âm thanh sống động bao bọc xung quanh như trong rạp
                                    xem phim. HTC BoomSound™ cùng Dolby Audio mang lại âm thanh tối ưu cho mọi nguồn nội
                                    dung trực tuyến chẳng hạn như kênh YouTube.
                                </p>

                                    <p>
                                        Bạn có thể đắm chìm trong thế giới âm thanh vòm cực kỳ sôi động và chân thật từ
                                        tất cả phim và trò chơi ở cả chế độ nghe từ loa ngoài BoomSound trên điện thoại
                                        cũng như chế độ sử dụng tai nghe.<br>
                                        <a href="#"
                                           class="call-to-action green">TÌM HIỂU THÊM</a>
                                    </p>
                                </div>
                            </div>
                            <div class="frame frame-custom bg4-2">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        PHÁT NHẠC THEO CÁCH BẠN MUỐN
                                    </h2></div>
                                <div class="copy align-left"><p>
                                    Với Cấu hình kết nối HTC BoomSound™, chỉ cần vuốt màn hình chính bằng ba ngón tay,
                                    bạn có thể truyền phát tất cả âm nhạc hoặc phim từ HTC One M9 đến các thiết bị
                                    stereo tương thích tại nhà. Thậm chí bạn cũng có thể truyền phát nhiều bài nhạc khác
                                    nhau tới nhiều loa khác nhau cùng lúc, tưởng chừng như bạn đang có trong tay chiếc
                                    điều khiển từ xa tất cả trong một.
                                </p>
                                </div>
                            </div>
                            <div class="frame frame-custom bg4-3">
                                <div class="header">
                                    <h3>
                                        HTC One M9
                                    </h3>

                                    <h2>
                                        THƯỞNG THỨC HÒA NHẠC HI-FI NGAY TẠI NHÀ
                                    </h2></div>
                                <div class="copy align-right"><p>
                                    HTC One M9 hỗ trợ chơi nhạc số chất lượng cao 24-bit. Nhờ vậy, bạn có thể thưởng
                                    thức âm nhạc sống động và chân thật với chất lượng còn cao hơn cả những đĩa nhạc CD.
                                </p>
                                </div>
                            </div>
                        </div>
                        <ul class="base-nav">
                            <li class="active"><span>ÂM THANH VÒM DOLBY SURROUND</span></li>
                            <li class=""><span>PHÁT NHẠC THEO CÁCH BẠN MUỐN</span></li>
                            <li class=""><span>THƯỞNG THỨC HÒA NHẠC HI-FI NGAY TẠI NHÀ</span></li>
                        </ul>
                    </section>
                    <section style="height: 755px;" id="slide-reviews" class="slide">
                        <div class="frame-container">
                            <div class="frame" id="frame-reviews">
                                <div class="header"><!-- <h3>THE HTC ONE (M8) FOR WINDOWS</h3> --><h3>HTC One M9</h3>

                                    <h2>VIẾT BÀI ĐÁNH GIÁ</h2>

                                    <div class="write-review">
                                        <p>Đánh giá điện thoại HTC One M9 của bạn</p>
                                        <a href="#"
                                           class="cta-button" title="Đăng một đánh giá">ĐĂNG MỘT ĐÁNH GIÁ</a>
                                    </div>
                                </div>
                                <div class="ratings-info">
                                    <div class="outer-container">
                                        <div class="inner-container">
                                            <div class="recommended"><span class="percentage">87</span><!-- RAW CONTENT BEGIN -->
                                                % NGƯỜI DÙNG <b>GIỚI THIỆU</b><!-- RAW CONTENT END --></div>
                                            <div class="more-reviews"><a
                                                    href="#">Đọc
                                                đánh giá</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ratings">
                                    <div class="ratings-stars">
                                        <div class="rating-property"><span>Máy ảnh</span>

                                            <div class="stars">
                                                <div class="score">3.6667</div>
                                                <div class="green-bar" style="width: 96px;"></div>
                                                <div class="star first"></div>
                                                <div class="star second"></div>
                                                <div class="star third"></div>
                                                <div class="star fourth"></div>
                                                <div class="star fifth"></div>
                                            </div>
                                        </div>
                                        <div class="rating-property"><span>Chất lượng âm thanh</span>

                                            <div class="stars">
                                                <div class="score">4.6111</div>
                                                <div class="green-bar" style="width: 121px;"></div>
                                                <div class="star first"></div>
                                                <div class="star second"></div>
                                                <div class="star third"></div>
                                                <div class="star fourth"></div>
                                                <div class="star fifth"></div>
                                            </div>
                                        </div>
                                        <div class="rating-property"><span>Màn hình</span>

                                            <div class="stars">
                                                <div class="score">4.8000</div>
                                                <div class="green-bar" style="width: 126px;"></div>
                                                <div class="star first"></div>
                                                <div class="star second"></div>
                                                <div class="star third"></div>
                                                <div class="star fourth"></div>
                                                <div class="star fifth"></div>
                                            </div>
                                        </div>
                                        <div class="rating-property"><span>Dễ sử dụng</span>

                                            <div class="stars">
                                                <div class="score">4.4857</div>
                                                <div class="green-bar" style="width: 119px;"></div>
                                                <div class="star first"></div>
                                                <div class="star second"></div>
                                                <div class="star third"></div>
                                                <div class="star fourth"></div>
                                                <div class="star fifth"></div>
                                            </div>
                                        </div>
                                        <div class="rating-property"><span>Thời gian dùng pin</span>

                                            <div class="stars">
                                                <div class="score">4.1667</div>
                                                <div class="green-bar" style="width: 112px;"></div>
                                                <div class="star first"></div>
                                                <div class="star second"></div>
                                                <div class="star third"></div>
                                                <div class="star fourth"></div>
                                                <div class="star fifth"></div>
                                            </div>
                                        </div>
                                        <div class="rating-property"><span>Chiêm ngưỡng &amp; Cảm nhận</span>

                                            <div class="stars">
                                                <div class="score">4.6111</div>
                                                <div class="green-bar" style="width: 121px;"></div>
                                                <div class="star first"></div>
                                                <div class="star second"></div>
                                                <div class="star third"></div>
                                                <div class="star fourth"></div>
                                                <div class="star fifth"></div>
                                            </div>
                                        </div>
                                        <div class="overall-rating">Tổng thể <span class="average">4.2</span> / null
                                        </div>
                                    </div>
                                </div>
                                <div class="right-section ratings">
                                    <div class="ratings-stars"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="slide" style="height: 755px;">
                        <div class="frame-container">
                            <div class="frame frame-custom bg8-1">
                                <div class="header">
                                    <h3 style="color: #333;">
                                        HTC One M9
                                    </h3>

                                    <h2 style="color: #333;">
                                        ĐĂNG KÝ NGAY HÔM NAY
                                    </h2>
                                </div>
                                <div class="copy align-left">
                                    <p>
                                        Giới thiệu HTC One M9:<br>
                                        - Màn hình chính phát hiện vị trí<br>
                                        - Máy ảnh selfie UltraPixel™<br>
                                        - HTC BoomSound với Dolby Audio
                                    </p>

                                    <p>
                                        Hãy tận hưởng từng phút giây của cuộc sống với trải nghiệm phong cách độc đáo
                                        nhất
                                    </p>
                                </div>
                                <div class="email-signup">
                                    <div class="email-icon"></div>
                                    <div class="sign-up">
                                        CÓ! Báo cho tôi biết khi HTC One M9 có mặt trên thị trường
                                    </div>
                                    <form class="signup-form">
                                        <label>Địa chỉ email:</label> <!-- IE fix -->
                                        <input class="launch-email" type="email" placeholder="Email Address"
                                               required="required" maxlength="40">
                                        <input class="submit-button" type="submit" value="">
                                    </form>
                                    <div class="confirmation-message" id="registrationsuccess-email"
                                         data-registration="email-signup">Cảm ơn bạn đã đăng ký!
                                    </div>
                                    <div class="error-message" id="registrationfailure-email"
                                         data-registration="email-signup">Vui lòng nhập địa chỉ email hợp lệ
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer-button" style="text-transform: uppercase; display: none;">Hiện chân trang
                        </div>
                    </section>
                </div>
            </div>
            <div class="specs-container-overlay"></div>
            <div class="specs-container" style="top: 150px;">
                <div class="specs-list" style="height: 553px;">
                    <div class="column one">
                        <ul>
                            <li class="mobile-hide"><h4>Kích thước<sup></sup></h4><span class="long">144,6 x 69,7 x 9,61 mm</span>
                            </li>
                            <li class=""><h4>Tốc độ chip xử lý<sup></sup></h4><span class="short"><ul>
                                <li>Qualcomm<sup>®</sup> Snapdragon™ 810, lõi tám</li>
                            </ul></span><span class="long"><ul>
                                <li>Qualcomm<sup>®</sup> Snapdragon™ 810, lõi tám</li>
                                <li>64-bit, 4 x 2,0GHz + 4 x 1,5GHz</li>
                            </ul></span></li>
                            <li class=""><h4>Bộ nhớ<sup>1</sup></h4><span class="short"><p>Tổng dung lượng bộ nhớ trong:
                                32GB / RAM: 3GB</p></span><span class="long"><ul>
                                <li>Tổng dung lượng bộ nhớ trong: 32GB<br>RAM: 3GB</li>
                                <li><strong>Thẻ nhớ mở rộng</strong>: hỗ trợ thẻ nhớ microSD™ dung lượng đến 2TB</li>
                            </ul></span></li>
                            <li class="mobile-hide"><h4>Cảm biến<sup></sup></h4><span class="long"><ul>
                                <li>Cảm biến ánh sáng</li>
                                <li>Cảm biến không gian gần</li>
                                <li>Gia tốc kế</li>
                                <li>Cảm biến la bàn</li>
                                <li>Cảm biến con quay hồi chuyển</li>
                                <li>Cảm biến nam châm</li>
                                <li>Bộ cảm biến – sensor hub</li>
                            </ul></span></li>
                            <li class=""><h4>Máy ảnh<sup></sup></h4><span class="short"><ul>
                                <li><strong>Máy ảnh chính</strong>: 20MP, quay phim 4K</li>
                                <li><strong>Máy ảnh phụ</strong>: HTC UltraPixel™, quay phim 1080p</li>
                            </ul></span><span class="long"><ul>
                                <li><strong>Máy ảnh chính</strong>: 20MP, ống kính phủ sapphire, tự động lấy nét, cảm
                                    biến BSI, khẩu độ f/2.2, ống kính 27,8mm, quay phim 4K
                                </li>
                                <li><strong>Máy ảnh phụ</strong>: HTC UltraPixel™, cảm biến BSI, khẩu độ f/2.0, ống kính
                                    26,8mm, quay phim 1080p
                                </li>
                            </ul></span></li>
                            <li class="mobile-hide"><h4><sup></sup></h4><span class="long"><ul>
                                <li>Thiết kế nguyên khối unibody kim loại hai tông màu (dual – tone)</li>
                                <li>Chủ đề HTC</li>
                                <li>Màn hình chính™ HTC Sense</li>
                                <li>Bộ sưu tập lưu trữ từ đám mây HTC</li>
                                <li>Trình chỉnh sửa ảnh</li>
                            </ul></span></li>
                        </ul>
                    </div>
                    <div class="column two">
                        <ul>
                            <li class="mobile-hide"><h4>Trọng lượng<sup></sup></h4><span class="long">157g</span></li>
                            <li class="mobile-hide"><h4>Hệ điều hành Android<sup></sup></h4><span class="long"><p>
                                Android™ với HTC Sense™</p></span></li>
                            <li class="mobile-hide"><h4>Mạng<sup>2</sup></h4><span class="long"><ul>
                                <li><strong>2G/2,5G - GSM/GPRS/EDGE</strong>:</li>
                                <li>850/900/1800/1900 MHz</li>
                                <li><strong>3G UMTS</strong>:</li>
                                <li>850/900/1900/2100 MHz</li>
                                <li><strong>4G LTE</strong>:</li>
                                <li>FDD: Dải băng tần 1,3,5,7,8,20,28</li>
                                <li>TDD: Dải băng tần 38, 40, 41</li>
                            </ul></span></li>
                            <li class="mobile-hide"><h4>Kết nối<sup></sup></h4><span class="long"><ul>
                                <li>NFC</li>
                                <li>Bluetooth<sup>®</sup> 4,1</li>
                                <li>Wi-Fi<sup>®</sup>: 802.11 a/b/g/n/ac (2,4 &amp; 5 GHz)</li>
                                <li>DLNA<sup>®</sup></li>
                                <li>HDMI MHL 3.0</li>
                                <li>CIR</li>
                                <li>Giắc âm thanh stereo 3,5 mm</li>
                                <li>cổng micro-USB 2,0 (5-pin)</li>
                            </ul></span></li>
                            <li class="mobile-hide"><h4>Đa phương tiện<sup></sup></h4><span class="long"><ul>
                                <li><strong>Định dạng âm thanh được hỗ trợ:</strong></li>
                                <li><strong>Phát lại</strong>: .aac, .amr, .ogg, .m4a, .mid, .mp3, .wav, .wma, ac3, ec3,
                                    eac3
                                </li>
                                <li><strong>Ghi âm</strong>: .aac</li>
                                <li><strong>Định dạng video được hỗ trợ:</strong></li>
                                <li><strong>Phát lại</strong>: .3gp, .3g2, .mp4, .wmv, .avi</li>
                                <li><strong>Ghi âm</strong>: .mp4</li>
                            </ul></span></li>
                        </ul>
                    </div>
                    <div class="column three">
                        <ul>
                            <li class=""><h4>Màn hình<sup></sup></h4><span class="short"><p>5.0 inch, Full HD 1080p</p></span><span
                                    class="long"><p>5.0 inch, Full HD 1080p</p></span></li>
                            <li class="mobile-hide"><h4>Loại Thẻ SIM<sup></sup></h4><span class="long"><p>nano
                                SIM</p></span></li>
                            <li class="mobile-hide"><h4>GPS<sup></sup></h4><span class="long"><p>Ăngten GPS tích hợp +
                                GLONASS</p></span></li>
                            <li class=""><h4>Âm thanh<sup></sup></h4><span class="short"><p>HTC BoomSound™ hợp tác với
                                Dolby Audio™</p></span><span class="long"><p>HTC BoomSound™ với Dolby Audio™</p></span>
                            </li>
                            <li class=""><h4>Pin<sup>3</sup></h4><span class="short"><p>Dung lượng: 2840
                                mAh</p></span><span class="long"><ul>
                                <li><strong>Dung lượng</strong>: 2840 mAh</li>
                                <li><strong>Tối đa 25.4 giờ cho mạng 2G/ 21.7 giờ cho mạng 3G</strong></li>
                                <strong>
                                    <li><strong>Thời gian chờ</strong>: Tối đa 391 giờ cho mạng 2G/ 402 giờ cho mạng 3G
                                    </li>
                                </strong></ul></span></li>
                        </ul>
                    </div>
                    <div style="clear:both;"></div>
                    <div class="footnote">
                        <ol>
                            <li><p><strong>Bộ nhớ trống có thể ít hơn tùy thuộc vào phần mềm hiện đang sử dụng của máy.
                                Người dùng có khoảng 21 GB bộ nhớ để lưu trữ nội dung. Dung lượng bộ nhớ khả dụng có thể
                                thay đổi tùy theo bản cập nhật phần mềm điện thoại và lưu lượng sử dụng của ứng
                                dụng.</strong></p></li>
                            <li><p><strong>Băng tần mạng ở các khu vực có thể khác nhau, tùy thuộc vào nhà khai thác
                                dịch vụ điện thoại di động và vị trí của bạn. LTE 4G chỉ sẵn có tại một số quốc gia nhất
                                định. Tốc độ tải lên và tải xuống cũng tùy thuộc vào nhà khai thác dịch vụ điện thoại di
                                động.</strong></p></li>
                            <li><p><strong>
                                Thời gian sử dụng pin (thời gian đàm thoại, thời gian chờ, v.v.) tùy thuộc vào mạng và
                                cách sử dụng điện thoại.<br>
                                Đặc điểm kỹ thuật Thời gian chờ ("đặc điểm kỹ thuật") là tiêu chuẩn ngành chỉ có mục
                                đích cho phép so sánh các thiết bị di động khác nhau trong cùng điều kiện hoạt động. Mức
                                tiêu hao năng lượng trong chế độ chờ còn phụ thuộc vào nhiều yếu tố bao gồm nhưng không
                                hạn chế ở mạng điện thoại, chế độ cài đặt, vị trí, sự di chuyển, cường độ sóng và số
                                lượng thuê bao tập trung tại khu vực đó. Do đó, việc so sánh thời gian chờ của các thiết
                                bị di động khác nhau chỉ có thể được thực hiện trong điều kiện kiểm soát chặt chẽ của
                                phòng thí nghiệm. Khi sử dụng bất kỳ thiết bị di động nào trong điều kiện thực tế, thời
                                gian chờ sẽ có thể thấp hơn và phụ thuộc rất nhiều vào các yếu tố đã nêu trên.
                            </strong></p></li>
                        </ol>
                    </div>
                    <div class="specs-footnote"><strong>Lưu ý: Thông số kỹ thuật có thể thay đổi mà không cần thông báo
                        trước</strong></div>
                </div>
            </div>
            <div class="back-to-top"></div>
        </div>
    </section>
    <div class="footer-space"></div>
    <div class="htc-footer" id="htc-footer" style="bottom: -25px; opacity: 0.7; display: none;">
        <footer>
            <div class="footer-wrapper">
                <div class="main-desk-modern">
                    <div class="footer-align-center">
                        <div class="region-block">
                            <div class="click-area">
                                <div class="region-text-wrapper">
                                    <div class="region-text"><strong>Việt Nam</strong></div>
                                </div>
                                <div class="globe"></div>
                            </div>
                            <div class="left-declaration-block"><strong>© 2014-2015 HTC Corporation</strong></div>
                        </div>
                        <div class="arrow-block">
                            <div class="arrow-back"></div>
                            <div class="arrow-icon"></div>
                        </div>
                        <div class="connect-block">
                            <div class="right-declaration-block">
                                <ul>
                                    <li><strong><a rel="follow" target="_self"
                                                   href="#">Chính sách Bảo
                                        mật</a></strong></li>
                                    <li><strong><a rel="follow" target="_self"
                                                   href="#">Bảo mật sản
                                        phẩm</a></strong></li>
                                </ul>
                            </div>
                            <div class="click-area">
                                <div class="default-area">
                                    <div class="dot-grey">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="connect-wording"><strong>
                                        KẾT NỐI
                                    </strong></div>
                                </div>
                            </div>
                        </div>
                        <div class="popover region-popover">
                            <div class="region-list">
                                <div class="popover-wrapper">
                                    <div class="title-wrapper">
                                        <div class="title"><strong>Quốc gia</strong></div>
                                    </div>
                                    <div class="list-wrapper desktop-wrapper">
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/au/">Australia</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/at/">Österreich</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/be-nl/">België</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/be-fr/">Belgique</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/br/support/">Brazil</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/ca/">Canada - English</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/ca-fr/">Canada -
                                                Français</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/cz/">Česká republika</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/cn/">中国</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/dk/">Danmark</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/de/">Deutschland</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/es/">España</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/fi/">Suomi</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/fr/">France</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/gr/">Ελλάδα</a></strong></li>
                                        </ul>
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/hk-en/">Hong Kong</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/hk-tc/">香港</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/hr/">Hrvatska</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/in/">India</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/id/">Indonesia</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ie/">Ireland</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/it/">Italia</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/jp/">日本</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/kr/">한국</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/kz/">Казахстан</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/latam/">América Latina</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/hu/">Magyarország</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/mea-en/">Middle East</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/mea-sa/">الشرق الأوسط</a></strong>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/mm/">Myanmar</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/nl/">Nederland</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/nz/">New Zealand</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/no/">Norge</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ru/">Россия и СНГ</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/pl/">Polska</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/pt/">Portugal</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ro/">România</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ch-de/">die Schweiz</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/ch-it/">Svizzera</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ch-fr/">Suisse</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/rs/">Србија</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/sk/">Slovensko</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/sea/">Singapore</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/se/">Sverige</a></strong></li>
                                        </ul>
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/tw/">台灣</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/th/">ประเทศไทย</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/tr/">Türkiye</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ua/">Україна</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/uk/">United Kingdom</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/us/">United States</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/vn/">Việt Nam</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/us/contact/additional_support.html">Additional
                                                Support</a></strong></li>
                                        </ul>
                                    </div>
                                    <div class="list-wrapper mobile-wrapper">
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/au/">Australia</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/at/">Österreich</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/be-nl/">België</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/be-fr/">Belgique</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/br/support/">Brazil</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/ca/">Canada - English</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/ca-fr/">Canada -
                                                Français</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/cz/">Česká republika</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/cn/">中国</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/dk/">Danmark</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/de/">Deutschland</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/es/">España</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/fi/">Suomi</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/fr/">France</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/gr/">Ελλάδα</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/hk-en/">Hong Kong</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/hk-tc/">香港</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/hr/">Hrvatska</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/in/">India</a></strong></li>
                                        </ul>
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/id/">Indonesia</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ie/">Ireland</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/it/">Italia</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/jp/">日本</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/kr/">한국</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/kz/">Казахстан</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/latam/">América Latina</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/hu/">Magyarország</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/mea-en/">Middle East</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/mea-sa/">الشرق الأوسط</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/mm/">Myanmar</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/nl/">Nederland</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/nz/">New Zealand</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/no/">Norge</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ru/">Россия и СНГ</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/pl/">Polska</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/pt/">Portugal</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ro/">România</a></strong></li>
                                        </ul>
                                        <ul>
                                            <li><strong><a href="http://www.htc.com/ch-de/">die Schweiz</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/ch-it/">Svizzera</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ch-fr/">Suisse</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/rs/">Србија</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/sk/">Slovensko</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/sea/">Singapore</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/se/">Sverige</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/tw/">台灣</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/th/">ประเทศไทย</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/tr/">Türkiye</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/ua/">Україна</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/uk/">United Kingdom</a></strong>
                                            </li>
                                            <li><strong><a href="http://www.htc.com/us/">United States</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/vn/">Việt Nam</a></strong></li>
                                            <li><strong><a href="http://www.htc.com/us/contact/additional_support.html">Additional
                                                Support</a></strong></li>
                                        </ul>
                                    </div>
                                    <div class="list-closer"></div>
                                </div>
                            </div>
                            <div class="extra-space"></div>
                        </div>
                        <div class="popover connect-popover">
                            <div class="social-list">
                                <div class="align-center"><strong><a rel="follow" title="Facebook"
                                                                     href="#"><img
                                        alt="Facebook" src="images/connect-fb.png"></a><a rel="follow"
                                                                                                      title="Twitter"
                                                                                                      href="#"><img
                                        alt="Twitter" src="images/connect-twitter.png"></a><a rel="follow"
                                                                                                          title="Google+"
                                                                                                          href="#"><img
                                        alt="Google+" src="images/connect-googleplus.png"></a><a
                                        rel="follow" title="Youtube" href="#"><img
                                        alt="Youtube" src="images/connect-youtube.png"></a></strong></div>
                            </div>
                            <div class="extra-space"></div>
                        </div>
                    </div>
                </div>
                <div class="main-desk">
                    <div class="footer-align-center">
                        <div class="level-one">
                            <div class="region-block">
                                <div class="align-center click-area"><strong>Việt Nam</strong></div>
                                <div class="globe click-area"></div>
                            </div>
                            <div class="popover region-popover">
                                <div class="region-list">
                                    <div class="popover-wrapper">
                                        <div class="title-wrapper">
                                            <div class="title"><strong>Quốc gia</strong></div>
                                        </div>
                                        <div class="list-wrapper desktop-wrapper">
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/au/">Australia</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/at/">Österreich</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/be-nl/">België</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/be-fr/">Belgique</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/br/support/">Brazil</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/ca/">Canada -
                                                    English</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ca-fr/">Canada -
                                                    Français</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/cz/">Česká
                                                    republika</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/cn/">中国</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/dk/">Danmark</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/de/">Deutschland</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/es/">España</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/fi/">Suomi</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/fr/">France</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/gr/">Ελλάδα</a></strong></li>
                                            </ul>
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/hk-en/">Hong Kong</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/hk-tc/">香港</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/hr/">Hrvatska</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/in/">India</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/id/">Indonesia</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ie/">Ireland</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/it/">Italia</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/jp/">日本</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/kr/">한국</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/kz/">Казахстан</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/latam/">América
                                                    Latina</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/hu/">Magyarország</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/mea-en/">Middle
                                                    East</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/mea-sa/">الشرق
                                                    الأوسط</a></strong></li>
                                            </ul>
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/mm/">Myanmar</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/nl/">Nederland</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/nz/">New Zealand</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/no/">Norge</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ru/">Россия и СНГ</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/pl/">Polska</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/pt/">Portugal</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ro/">România</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ch-de/">die Schweiz</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/ch-it/">Svizzera</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/ch-fr/">Suisse</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/rs/">Србија</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/sk/">Slovensko</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/sea/">Singapore</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/se/">Sverige</a></strong></li>
                                            </ul>
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/tw/">台灣</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/th/">ประเทศไทย</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/tr/">Türkiye</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ua/">Україна</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/uk/">United Kingdom</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/us/">United States</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/vn/">Việt Nam</a></strong></li>
                                                <li><strong><a
                                                        href="http://www.htc.com/us/contact/additional_support.html">Additional
                                                    Support</a></strong></li>
                                            </ul>
                                        </div>
                                        <div class="list-wrapper mobile-wrapper">
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/au/">Australia</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/at/">Österreich</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/be-nl/">België</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/be-fr/">Belgique</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/br/support/">Brazil</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/ca/">Canada -
                                                    English</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ca-fr/">Canada -
                                                    Français</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/cz/">Česká
                                                    republika</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/cn/">中国</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/dk/">Danmark</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/de/">Deutschland</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/es/">España</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/fi/">Suomi</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/fr/">France</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/gr/">Ελλάδα</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/hk-en/">Hong Kong</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/hk-tc/">香港</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/hr/">Hrvatska</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/in/">India</a></strong></li>
                                            </ul>
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/id/">Indonesia</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ie/">Ireland</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/it/">Italia</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/jp/">日本</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/kr/">한국</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/kz/">Казахстан</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/latam/">América
                                                    Latina</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/hu/">Magyarország</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/mea-en/">Middle
                                                    East</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/mea-sa/">الشرق
                                                    الأوسط</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/mm/">Myanmar</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/nl/">Nederland</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/nz/">New Zealand</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/no/">Norge</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ru/">Россия и СНГ</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/pl/">Polska</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/pt/">Portugal</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ro/">România</a></strong></li>
                                            </ul>
                                            <ul>
                                                <li><strong><a href="http://www.htc.com/ch-de/">die Schweiz</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/ch-it/">Svizzera</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/ch-fr/">Suisse</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/rs/">Србија</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/sk/">Slovensko</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/sea/">Singapore</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/se/">Sverige</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/tw/">台灣</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/th/">ประเทศไทย</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/tr/">Türkiye</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/ua/">Україна</a></strong></li>
                                                <li><strong><a href="http://www.htc.com/uk/">United Kingdom</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/us/">United States</a></strong>
                                                </li>
                                                <li><strong><a href="http://www.htc.com/vn/">Việt Nam</a></strong></li>
                                                <li><strong><a
                                                        href="http://www.htc.com/us/contact/additional_support.html">Additional
                                                    Support</a></strong></li>
                                            </ul>
                                        </div>
                                        <div class="list-closer"></div>
                                    </div>
                                </div>
                                <div class="extra-space"></div>
                            </div>
                            <div class="arrow-block"></div>
                            <div class="connect-block">
                                <div class="default-area click-area">
                                    <div class="dot-grey">
                                        <div class="circle"></div>
                                    </div>
                                    <strong>
                                        KẾT NỐI
                                    </strong></div>
                            </div>
                            <div class="popover connect-popover">
                                <div class="social-list">
                                    <div class="align-center"><strong><a rel="follow" title="Facebook"
                                                                         href="#"><img
                                            alt="Facebook" src="images/connect-fb.png"></a><a rel="follow"
                                                                                                          title="Twitter"
                                                                                                          href="http://www.twitter.com/htc"><img
                                            alt="Twitter" src="images/connect-twitter.png"></a><a
                                            rel="follow" title="Google+" href="#"><img
                                            alt="Google+" src="images/connect-googleplus.png"></a><a
                                            rel="follow" title="Youtube" href="#"><img
                                            alt="Youtube" src="images/connect-youtube.png"></a></strong>
                                    </div>
                                </div>
                                <div class="extra-space"></div>
                            </div>
                        </div>
                        <div class="level-two">
                            <div class="left-col">
                                <div class="copyright"><strong>
                                    © 2014-2015 HTC Corporation
                                    <ul>
                                        <li><a rel="follow" target="_self" href="images/">Chính
                                            sách Bảo mật</a></li>
                                        <li><a rel="follow" target="_self"
                                               href="images/">Bảo mật sản phẩm</a>
                                        </li>
                                    </ul>
                                </strong></div>
                            </div>
                            <div class="right-col">
                                <div class="list-of-links">
                                    <ul>
                                        <li><strong><a rel="" target="_self"
                                                       href="images/">Điều khoản sử
                                            dụng</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="http://www.htc.com/vn/terms/privacy/">Chính sách bảo
                                            mật</a></strong></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-list">
                    <div class="footer-align-center">
                        <div class="links">
                            <div class="links-col1 m-group-col-1">
                                <ul>
                                    <li>
                                        <div class="title"><strong>SẢN PHẨM</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self"
                                                           href="images/">SẢN
                                                PHẨM</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">RE
                                                CAMERA</a></strong></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title"><strong>HỖ TRỢ</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self" href="#/">Trung
                                                tâm hỗ trợ</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Hỗ
                                                trợ bảo hành HTC</a></strong></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title"><strong>Pháp lý</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Chính sách Bảo
                                                mật</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Quy Định Sử
                                                Dụng</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Quy Định
                                                Xử Sự</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Bản Quyền Và Sở
                                                Hữu Trí Tuệ</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Tìm hiểu
                                                thêm</a></strong></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="links-col2 m-group-col-1">
                                <ul>
                                    <li>
                                        <div class="title"><strong><a href="#">Phụ
                                            kiện</a></strong></div>
                                        <ul class="sublinks"></ul>
                                    </li>
                                    <li>
                                        <div class="title"><strong>PHÒNG TIN TỨC</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Phòng tin
                                                tức</a></strong></li>
                                            <li><strong><a rel="" target="_self" href="#">Tổng
                                                quan về Công ty</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Lãnh
                                                đạo</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Trách
                                                nhiệm của Doanh nghiệp</a></strong></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="links-col3 m-group-col-2">
                                <ul>
                                    <li>
                                        <div class="title"><strong>Công nghệ cải tiến</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self"
                                                           href="#/">Công nghệ cải
                                                tiến</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Máy ảnh
                                                ultrapixel</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">HTC
                                                Transfer</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">Power to
                                                Give</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">HTC
                                                Connect</a></strong></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title"><strong>Nghề nghiệp</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self" href="#">Làm
                                                việc tại HTC</a></strong></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="links-col4 m-group-col-2">
                                <ul>
                                    <li>
                                        <div class="title"><strong>LIÊN HỆ VỚI CHÚNG TÔI</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self"
                                                           href="#">GỌI
                                                ĐIỆN</a></strong></li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">EMAIL HỖ TRỢ</a></strong>
                                            </li>
                                            <li><strong><a rel="" target="_self"
                                                           href="#">LÊN LỊCH
                                                HẸN</a></strong></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title"><strong>Sites</strong></div>
                                        <ul class="sublinks">
                                            <li><strong><a rel="" target="_self" href="#">HTC
                                                Blog</a></strong></li>
                                            <li><strong><a rel="" target="_self" href="#">HTC
                                                Dev</a></strong></li>
                                            <li><strong><a rel="" target="_self" href="#">HTC
                                                Pro</a></strong></li>
                                            <li><strong><a rel="" target="_self" href="#">Bắt đầu
                                                ngay</a></strong></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-links">
                            <ul>
                                <li>
                                    <div class="title"><strong>SẢN PHẨM</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self" href="#">SẢN
                                            PHẨM</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">RE
                                            CAMERA</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong><a href="#">Phụ kiện</a></strong>
                                    </div>
                                    <ul class="sublinks"></ul>
                                </li>
                                <li>
                                    <div class="title"><strong>Công nghệ cải tiến</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self" href="#">Công
                                            nghệ cải tiến</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">Máy
                                            ảnh ultrapixel</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">HTC
                                            Transfer</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Power to Give</a></strong>
                                        </li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">HTC
                                            Connect</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong>LIÊN HỆ VỚI CHÚNG TÔI</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self"
                                                       href="#">GỌI ĐIỆN</a></strong>
                                        </li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">EMAIL HỖ
                                            TRỢ</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">LÊN LỊCH
                                            HẸN</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong>HỖ TRỢ</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self" href="#">Trung
                                            tâm hỗ trợ</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Hỗ
                                            trợ bảo hành HTC</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong>PHÒNG TIN TỨC</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Phòng tin
                                            tức</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">Tổng
                                            quan về Công ty</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Lãnh
                                            đạo</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Trách
                                            nhiệm của Doanh nghiệp</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong>Nghề nghiệp</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self" href="#">Làm
                                            việc tại HTC</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong>Sites</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self" href="#">HTC
                                            Blog</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">HTC
                                            Dev</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">HTC
                                            Pro</a></strong></li>
                                        <li><strong><a rel="" target="_self" href="#">Bắt đầu
                                            ngay</a></strong></li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><strong>Pháp lý</strong></div>
                                    <ul class="sublinks">
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Chính sách Bảo
                                            mật</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Quy Định Sử
                                            Dụng</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Quy Định Xử
                                            Sự</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Bản Quyền Và Sở Hữu
                                            Trí Tuệ</a></strong></li>
                                        <li><strong><a rel="" target="_self"
                                                       href="#">Tìm hiểu thêm</a></strong>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="legal">
                            <div class="logo"></div>
                            <div class="copyright"><p class="bold"><strong>Bản quyền 2014-2015 HTC Corporation. Mọi
                                quyền được bảo lưu.</strong></p>
                                <ul>
                                    <li><strong><a rel="" target="_self"
                                                   href="#">Điều khoản sử
                                        dụng</a></strong></li>
                                    <li><strong><a rel="" target="_self" href="#">Chính
                                        sách bảo mật</a></strong></li>
                                </ul>
                                <div class="footer-content"><!-- RAW CONTENT BEGIN --><!-- RAW CONTENT END --></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="deep-bottom"></div>
            </div>
            <strong><input name="downToFooter" id="downToFooter" type="hidden"></strong></footer>
    </div>
</div>
<script src="js/modernizr-2.8.3.min.js" class="chrome-plugin-js"></script>
<script src="js/device.js" class="chrome-plugin-js"></script>
<script src="js/jquery.validate.min.js" class="chrome-plugin-js"></script>
<script src="js/hoverIntent-r7.min.js" class="chrome-plugin-js"></script>
<script src="js/jquery.placeholder.min.js" class="chrome-plugin-js"></script>
<script src="js/xregexp-all-min.js" class="chrome-plugin-js"></script>
<script src="js/velocity-1.2.2.js"></script>
<script src="js/velocity-ui-5.0.4.js"></script>
<script src="js/bxslider-4.1.2.js"></script>
<script src="js/watch-2.0.min.js"></script>
<script src="js/all.min.js"></script>
<script src="js/htc.chrome.all.min.js" type="text/javascript" id="chrome-js"></script>
<script type="text/javascript">
    var reviewsWSBaseURL = 'https://ws.htc.com/htc-reviews-1.0/reviews';
</script>
<script src="js/plugins.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/standardproductdetail.js"></script>
<script src="js/velocity-1.1.0.js"></script>


</body>
</html>