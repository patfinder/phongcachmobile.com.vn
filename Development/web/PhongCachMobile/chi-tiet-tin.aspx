﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="chi-tiet-tin.aspx.cs" Inherits="PhongCachMobile.chi_tiet_tin" %>
<%@ Register Src="~/master/uc/ucDigitalToys.ascx" TagName="ucDigitalToys" TagPrefix="ucDigitalToys" %>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        <div class="breadcrumb">
            <div class="content-center">
                <ol>
                    <li><a href="/">Trang chủ</a><i class="fa fa-angle-right"></i></li>
                    <li><a href="<%= articleType.link %>"><%= articleType.title %></a><i class="fa fa-angle-right"></i></li>
                    <li class="active"><%= article.title %></li>
                </ol>
            </div>
        </div>
        <section class="page-new">
            <div class="content-center clearfix">
                <div class="content-main">
                    <h1><%= article.title %></h1>
                    <span class="info-bytt"><%= article.postedDate %> - <strong class="color-red">
                        <%= article.CommentCount %></strong> bình luận - <strong class="color-red">
                        <%= article.viewCount %></strong> lượt xem</span>
                    <%= article.longDesc %>
                </div>

                <ucDigitalToys:ucDigitalToys ID="ucDigitalToys" runat="server" />

            </div>
        </section>
    </div>

</asp:Content>
