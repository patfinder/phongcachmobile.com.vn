﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-faq.aspx.cs" Inherits="PhongCachMobile.m_faq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $('ul#faqs li').each(function () {
                var tis = $(this), state = false, answer = tis.next('div').hide().css('height', 'auto').slideUp();
                tis.click(function () {
                    state = !state;
                    answer.slideToggle(state);
                    tis.toggleClass('active', state);
                });
            });
        });
    </script>
    <style type="text/css">
        #faqs 
        {
            display: inline-block;
            margin-top: -5px;
        }
        #faqs li
        {
            cursor: pointer;
            padding: 5px 0 0 12px;
            background: url(../images/bullet_alt.png) 0 10px no-repeat;
            font-weight:bold;
            color:#515151;
        }
        #faqs li.active
        {
            color: #d74646;
        }
        #faqs div
        {
            height: 0;
            overflow: hidden;
            position: relative;
              padding: 5px 0 0 12px;
        }
        #faqs div p
        {
            padding: 0;
            margin-bottom: 5px;
            line-height: 18px;
            color: black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litFAQ" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content" style="background: white; padding: 25px 30px 16px 30px;">
                                    <asp:ListView ID="lstFaq" runat="server">
                                        <LayoutTemplate>
                                            <ul id="faqs">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <%#Eval("Ask")%></li>
                                            <div>
                                                <p>
                                                    <%#Eval("Answer")%></p>
                                            </div>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
