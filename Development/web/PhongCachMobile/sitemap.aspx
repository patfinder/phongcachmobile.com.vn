﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="sitemap.aspx.cs" Inherits="PhongCachMobile.sitemap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litSitemap" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content" style="background: white; padding: 25px 30px 16px 30px;">
                                    <div id="sitemap_content">
                                        <div class="sitemap_block">
                                            <h3>
                                                <asp:Literal ID="litProduct" runat="server"></asp:Literal></h3>
                                            <ul>
                                                <li><a href="gia-hot">
                                                   Giá hot</a></li>
                                                <li><a href="kho-may-cu">
                                                    Kho máy cũ</a></li>
                                                <li><a href="dtdd">
                                                    Điện thoại di động</a></li>
                                                <li><a href="may-tinh-bang">
                                                    Máy tính bảng</a></li>
                                            </ul>
                                        </div>
                                        <div class="sitemap_block">
                                            <h3>
                                                <asp:Literal ID="litNews" runat="server"></asp:Literal></h3>
                                            <ul>
                                                <li><a href="tin-thi-truong">
                                                    <asp:Literal ID="litLastestNews" runat="server"></asp:Literal></a></li>
                                                <li><a href="tin-khuyen-mai">
                                                    <asp:Literal ID="litPromotionNews" runat="server"></asp:Literal></a></li>
                                            </ul>
                                        </div>
                                        <div class="sitemap_block">
                                            <h3>
                                                <asp:Literal ID="litInfo" runat="server"></asp:Literal></h3>
                                            <ul>
                                                <li><a href="gioi-thieu">
                                                    <asp:Literal ID="litIntroduce" runat="server"></asp:Literal></a></li>
                                                <li><a href="lien-he">
                                                    <asp:Literal ID="litContact" runat="server"></asp:Literal></a></li>
                                                <li><a href="chat-luong-hang-hoa">
                                                    <asp:Literal ID="litProductQuality" runat="server"></asp:Literal></a></li>
                                                <li><a href="hop-tac-kinh-doanh">
                                                    <asp:Literal ID="litJoinBusiness" runat="server"></asp:Literal></a></li>
                                            </ul>
                                        </div>
                                        <div class="sitemap_block">
                                            <h3>
                                                <asp:Literal ID="litSupport" runat="server"></asp:Literal></h3>
                                            <ul>
                                                <li><a href="faq">
                                                    <asp:Literal ID="litFAQ" runat="server"></asp:Literal></a></li>
                                                <li><a href="mua-hang-tu-xa">
                                                    <asp:Literal ID="litDistanceBuying" runat="server"></asp:Literal></a></li>
                                                <li><a href="quy-dinh-bao-hanh">
                                                    <asp:Literal ID="litGuaranteeRule" runat="server"></asp:Literal></a></li>
                                                <li><a href="sua-chua-cai-dat">
                                                    <asp:Literal ID="litFixSetup" runat="server"></asp:Literal></a></li>
                                            </ul>
                                        </div>
                                        <div class="clearblock">
                                        </div>
                                    </div>
                                    <div class="categTree">
                                        <h3>Điện thoại di động</h3>
                                        <div class="tree_top">
                                            <a href="portal">Portal</a></div>
                                        <asp:Literal ID="litMobile" runat="server"></asp:Literal>
                                    </div>
                                     <div class="categTree">
                                        <h3>Máy tính bảng</h3>
                                        <div class="tree_top">
                                            <a href="portal">Portal</a></div>
                                        <asp:Literal ID="litTablet" runat="server"></asp:Literal>
                                    </div>
                                      <div class="categTree">
                                        <h3>Ứng dụng & game</h3>
                                        <div class="tree_top">
                                            <a href="portal">Portal</a></div>
                                        <asp:Literal ID="litApplication" runat="server"></asp:Literal>
                                    </div>
                                     <div class="categTree" style="padding-right:0px;">
                                        <h3>Phụ kiện</h3>
                                        <div class="tree_top">
                                            <a href="portal">Portal</a></div>
                                        <asp:Literal ID="litAccessories" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
