﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="hop-tac-kinh-doanh.aspx.cs" Inherits="PhongCachMobile.hop_tac_kinh_doanh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litJoinBusiness" runat="server"></asp:Literal></span></strong>
                                </div>
                                <asp:Literal ID="litJoinBusinessContent" runat="server"></asp:Literal>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
