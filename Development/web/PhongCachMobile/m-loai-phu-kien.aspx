﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-loai-phu-kien.aspx.cs" Inherits="PhongCachMobile.m_loai_phu_kien" %>

<%@ Register Src="~/master/uc/ucAccessoryList.ascx" TagName="ucAccessoryList" TagPrefix="ucAccessoryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="css/category.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="content">

        <% 
            for (int i = 0; i < SectionsCategories.Count; i++ )
            {
               List<PhongCachMobile.model.Article> group = SectionsCategories[i];
            %>
        <div class="fittings">
            <div class="content-center">
                <div class="menu">
                    <h2><%= SectionList[i].Title %></h2>
                    <div class="menu-other dropdown">
                        <a id="dLabel" data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-th-list"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <% int j = 0;
                            foreach (var article in group)
                            {
                                if (j++ >= 6)
                                    break;
                            %>
                            <li><a href="<%= article.link %>"><%= article.title %></a></li>
                            <% } %>
                        </ul>
                    </div>
                </div>
                <div class="list-fittins clearfix" id="list-fittins-<%= i + 1 %>">

                    <% if(i == 0) { %>
                <asp:PlaceHolder ID="PlaceHolder_1" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 1) {  %>
                <asp:PlaceHolder ID="PlaceHolder_2" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 2) {  %>
                <asp:PlaceHolder ID="PlaceHolder_3" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 3) {  %>
                <asp:PlaceHolder ID="PlaceHolder_4" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 4) {  %>
                <asp:PlaceHolder ID="PlaceHolder_5" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 5) {  %>
                <asp:PlaceHolder ID="PlaceHolder_6" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 6) {  %>
                <asp:PlaceHolder ID="PlaceHolder_7" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 7) {  %>
                <asp:PlaceHolder ID="PlaceHolder_8" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 8) {  %>
                <asp:PlaceHolder ID="PlaceHolder_9" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
                    <% } else if(i == 9) {  %>
                <asp:PlaceHolder ID="PlaceHolder_10" ClientIDMode="Static" runat="server" Visible="false"></asp:PlaceHolder>
        <% } %>

                </div>
            </div>
        </div>
        <% } %>

        <input type="hidden" id="sectionCount" value="<%= SectionsCategories.Count %>" />
    </div>

    <script type="text/javascript" >

        $(document).ready(function () {
            //$ = jQuery;

            console.log("Show n section and move to corresponding place.");
             
            // Show n section and move to corresponding place.
            count = parseInt($("#sectionCount").val());
             
            for (i = 1; i <= count; i++) {
                holder = $("#PlaceHolder_" + i);
                // holder.show();
                $("#list-fittins-" + i).append(holder);
            }
        });

    </script>

</asp:Content>
