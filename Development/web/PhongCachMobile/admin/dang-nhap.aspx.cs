﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.admin
{
    public partial class dang_nhap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadLanguages();
        }


        private void loadLanguages()
        {
            this.litLogIn.Text = this.btnLogin.Text = this.Title =  LangController.getLng("litLogIn.Text", "Đăng nhập");
            this.litUsername.Text = LangController.getLng("litUsername.Text", "Tài khoản");
            this.litPassword.Text = LangController.getLng("litPassword.Text", "Mật khẩu");
            this.reqValUsername.ErrorMessage = this.reqValPassword.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");

            this.litHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litHelpLogin.Text = LangMsgController.getLngMsg("litHelpLogin.Desc", "Những thông tin giải thích về quá trình đăng nhập");
            this.litCopyright.Text = "<a target='_blank' href='../gia-hot'>PhongCachMobile</a>" + " © " + DateTime.Now.Year;
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            LogInUserResult result = SysUserController.logIn(txtUsername.Text, txtPassword.Text);
            if (result.statusCode == LogInUserResult.STATUS_INVALID_CREDENTIAL_CODE)
            {
                litFrmResponse_Login.Text = LangController.getLng("litInvalidAccount.Text", "Sai tài khoản / mật khẩu");
                frmResponse.Visible = true;
            }
            else if (result.statusCode == LogInUserResult.STATUS_INACTIVATED_USER_CODE)
            {
                this.litFrmResponse_Login.Text = result.statusText;
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
                frmResponse.Visible = true;
            }
            else if (result.statusCode == LogInUserResult.STATUS_DISABLE_USER_ERROR_CODE)
            {
                this.litFrmResponse_Login.Text = result.statusText;
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
                frmResponse.Visible = true;
            }
            else if (result.statusCode == LogInUserResult.STATUS_GENERAL_ERROR_CODE)
            {
                this.litFrmResponse_Login.Text = result.statusText;
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
                frmResponse.Visible = true;
            }
            else
            {
                
                DataTable dt = SysUserController.getUserByEmail(txtUsername.Text);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Session["Email"] = txtUsername.Text;
                    Session["UserId"] = dt.Rows[0]["Id"].ToString();
                    Session["GroupId"] = dt.Rows[0]["GroupId"].ToString();
                    Response.Redirect("authorized", true);
                }
            }
        }
    }
}