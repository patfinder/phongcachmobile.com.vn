﻿using PhongCachMobile.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class landing_page_1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            s2p1Title.Text = "Thiết kế 1";
        }

        int page = 1;

        protected void LoadPage()
        {
            Dictionary<string, string> pageValues = new Dictionary<string,string>();

            var dt = LandingPageController.GetPage(page);
            foreach(DataRow dr in dt.Rows)
            {
                pageValues.Add(dr["Name"] as string, dr["Value"] as string);
            }

            // Section 1
            s1Title.Text = pageValues.Vx("s1Title");
            s1Image.ImageUrl = pageValues.Vx("s1Image");

            // Section 2 - P1
            s2p1Title.Text = pageValues.Vx("s2p1Title");
            s1Image.ImageUrl = pageValues.Vx("s1Title");

            s2p1DescTitle.Text = pageValues.Vx("s2p1DescTitle");
            s2p1Desc.Text = pageValues.Vx("s2p1Desc");

            // Section 2 - P2
            s2p2Image1.ImageUrl = pageValues.Vx("s2p2Image1");
            s2p2Image2.ImageUrl = pageValues.Vx("s2p2Image2");
            s2p2Image3.ImageUrl = pageValues.Vx("s2p2Image3");

            // Section 2 - P3
            s2p3Title.Text = pageValues.Vx("s2p3Title");
            s2p3Image1.ImageUrl = pageValues.Vx("s2p3Image1");
            s2p3Image2.ImageUrl = pageValues.Vx("s2p3Image2");
            s2p3Image3.ImageUrl = pageValues.Vx("s2p3Image3");
            s2p3Image4.ImageUrl = pageValues.Vx("s2p3Image4");
            s2p3Image5.ImageUrl = pageValues.Vx("s2p3Image5");
            s2p3Image6.ImageUrl = pageValues.Vx("s2p3Image6");

            // Section 3
            s3Title.Text = pageValues.Vx("s3Title");
            s3Image.ImageUrl = pageValues.Vx("s3Image");

            s3DescTitle.Text = pageValues.Vx("s3DescTitle");
            s3Desc.Text = pageValues.Vx("s3Desc");

            // Section 4
            s4Title.Text = pageValues.Vx("s4Title");
            s4Image.ImageUrl = pageValues.Vx("s4Image");

            // Section 5 - P1
            s5p1Title.Text = pageValues.Vx("s5p1Title");
            s5p1Image.ImageUrl = pageValues.Vx("s5p1Image");

            s5p1DescTitle.Text = pageValues.Vx("s5p1DescTitle");
            s5p1Desc.Text = pageValues.Vx("s5p1Desc");

            // Section 5 - P2
            s5p2Image.ImageUrl = pageValues.Vx("s5p2Image");
            s5p2Desc.Text = pageValues.Vx("s5p2Desc");

            // Section 5 - P3
            s5p3Image.ImageUrl = pageValues.Vx("s5p3Image");
            s5p3Desc.Text = pageValues.Vx("s5p3Desc");
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> pageValues = new Dictionary<string, string>();

            // Section 1
            pageValues["s1Title"] = s1Title.Text;
            pageValues["s1Image"] = s1Image.ImageUrl;

            // Section 2 - P1
            pageValues["s2p1Title"] = s2p1Title.Text;
            pageValues["s1Image"] = s1Image.ImageUrl;

            pageValues["s2p1DescTitle"] = s2p1DescTitle.Text;
            pageValues["s2p1Desc"] = s2p1Desc.Text;

            // Section 2 - P2
            pageValues["s2p2Image1"] = s2p2Image1.ImageUrl;
            pageValues["s2p2Image2"] = s2p2Image2.ImageUrl;
            pageValues["s2p2Image3"] = s2p2Image3.ImageUrl;

            // Section 2 - P3
            pageValues["s2p3Title"] = s2p3Title.Text;
            pageValues["s2p3Image1"] = s2p3Image1.ImageUrl;
            pageValues["s2p3Image2"] = s2p3Image2.ImageUrl;
            pageValues["s2p3Image3"] = s2p3Image3.ImageUrl;
            pageValues["s2p3Image4"] = s2p3Image4.ImageUrl;
            pageValues["s2p3Image5"] = s2p3Image5.ImageUrl;
            pageValues["s2p3Image6"] = s2p3Image6.ImageUrl;

            // Section 3
            pageValues["s3Title"] = s3Title.Text;
            pageValues["s3Image"] = s3Image.ImageUrl;

            pageValues["s3DescTitle"] = s3DescTitle.Text;
            pageValues["s3Desc"] = s3Desc.Text;

            // Section 4
            pageValues["s4Title"] = s4Title.Text;
            pageValues["s4Image"] = s4Image.ImageUrl;

            // Section 5 - P1
            pageValues["s5p1Title"] = s5p1Title.Text;
            pageValues["s5p1Image"] = s5p1Image.ImageUrl;

            pageValues["s5p1DescTitle"] = s5p1DescTitle.Text;
            pageValues["s5p1Desc"] = s5p1Desc.Text;

            // Section 5 - P2
            pageValues["s5p2Image"] = s5p2Image.ImageUrl;
            pageValues["s5p2Desc"] = s5p2Desc.Text;

            // Section 5 - P3
            pageValues["s5p3Image"] = s5p3Image.ImageUrl;
            pageValues["s5p3Desc"] = s5p3Desc.Text;

            LandingPageController.UpdatePage(page, pageValues);

            Response.Write("DONE");
            Response.End();
        }
    }
}