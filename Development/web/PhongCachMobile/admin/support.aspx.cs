﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class support : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litSupportManager.Text", "Quản lý hỗ trợ online");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLanguages();
                loadGroup();
                loadAllSupport(ddlGroup.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadLanguages()
        {
            this.litSupport.Text = LangController.getLng("litSupport.Text", "Support");
            this.litAddSupport.Text = LangController.getLng("litAddSupport.Text", "Thêm / sửa Support");

            this.litGroupChoosing.Text = LangController.getLng("litGroupChoosing.Text", "Nhóm");
            this.btnAddSupport.Text = LangController.getLng("litAdd.Text", "Thêm");
            this.btnDeleteSupport.Text = LangController.getLng("litDelete.Text", "Xóa");
            this.btnAddSupportSave.Text = LangController.getLng("litSave.Text", "Lưu");
            this.btnCancel.Text = LangController.getLng("litCancel.Text", "Hủy");

            this.reqValName.Text = this.reqValPhone.Text = this.reqValSkypeId.Text = this.reqValYM.Text = LangController.getLng("litRequired.Text", "(*)");
        }

        private void loadGroup()
        {
            DataTable dt = GroupController.getGroupsByFilter("SUPPORT_TYPE");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DataTextField = "Name";
                ddlGroup.DataValueField = "Id";
                ddlGroup.DataBind();

                ddlAnotherGroup.DataSource = dt;
                ddlAnotherGroup.DataTextField = "Name";
                ddlAnotherGroup.DataValueField = "Id";
                ddlAnotherGroup.DataBind();
            }
        }

        protected void btnDeleteSupport_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrSupport.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrSupport.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrSupport.Items[i].Cells[1].Text;
                    //delete images?
                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = SupportController.deleteSupport(gvIDs);

                if (result)
                {
                    loadAllSupport(ddlGroup.SelectedValue);
                    lblFrmResponseSupport.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponseSupport.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                    frmResponseSupport.Visible = true;
                }
                else
                {
                    lblFrmResponseSupport.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseSupport.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseSupport.Visible = true;
                }
            }

            resetAddSupport();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadAllSupport(ddlGroup.SelectedValue);
            frmResponseSupport.Visible = false;
        }

        private void resetAddSupport()
        {
            txtName.Text = txtPhone.Text = txtSkype.Text = txtYM.Text =  "";
            ddlAnotherGroup.SelectedIndex = 0;
            dhfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddSupport.Attributes["style"] = "display:none";
        }

        private void loadAllSupport(string groupId)
        {
            DataTable dt = SupportController.getSupportByGroupId(groupId);

            dgrSupport.DataSource = dt;
            dgrSupport.DataBind();
        }

      
        protected void btnAddSupportSave_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.dhfId.Value != "")
            {
                rs = SupportController.updateSupport(new Support{
                    id = int.Parse(dhfId.Value),
                    groupId = int.Parse(ddlAnotherGroup.SelectedValue),
                    name = txtName.Text,
                    skype = txtSkype.Text,
                    yahooMessenger = txtYM.Text,
                    phone = txtPhone.Text});
            }
            else
            {
                rs = SupportController.createSupport(new Support
                {
                    groupId = int.Parse(ddlAnotherGroup.SelectedValue),
                    name = txtName.Text,
                    skype = txtSkype.Text,
                    yahooMessenger = txtYM.Text,
                    phone = txtPhone.Text
                });
            }

            if (rs == true)
            {
                loadAllSupport(ddlGroup.SelectedValue);
                lblFrmResponseSupport.ForeColor = System.Drawing.Color.Green;
                if (this.dhfId.Value != "")
                {
                    lblFrmResponseSupport.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                else
                {
                    lblFrmResponseSupport.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                frmResponseSupport.Visible = true;
            }
            else
            {
                lblFrmResponseSupport.ForeColor = System.Drawing.Color.Red;
                if (this.dhfId.Value != "")
                {
                    // lblFrmResponseSupport.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                    lblFrmResponseSupport.Text = "Cập nhật thất bại";
                }
                else
                {
                    lblFrmResponseSupport.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }

                frmResponseSupport.Visible = true;
            }
            resetAddSupport();
        }


        protected void dgrSupport_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            loadAllSupport(ddlGroup.SelectedValue);
            dgrSupport.CurrentPageIndex = e.NewPageIndex;
            dgrSupport.DataBind();

            //Disable add Support
            resetAddSupport();

            frmResponseSupport.Visible = false;

        }

        protected void dgrSupport_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllSupport(ddlGroup.SelectedValue);
            DataTable dt = (DataTable)dgrSupport.DataSource;
            DataRow dr = dt.Rows[iIndex];

            this.txtName.Text = dr["Name"].ToString();
            this.txtYM.Text = dr["YahooMessenger"].ToString();
            this.txtSkype.Text = dr["Skype"].ToString();
            this.txtPhone.Text = dr["Phone"].ToString();
            this.ddlAnotherGroup.SelectedValue = dr["GroupId"].ToString();
            this.dhfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddSupport.Attributes["style"] = "display:block";

            frmResponseSupport.Visible = false;
        }
    }
}