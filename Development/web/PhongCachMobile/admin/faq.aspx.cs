﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class faq : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litFAQManager.Text", "Quản lý FAQ");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadFAQ();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadFAQ()
        {
            DataTable dt = FaqController.getFaqs();
            if (dt != null && dt.Rows.Count > 0)
            {
                dgrFAQ.DataSource = dt;
                dgrFAQ.DataBind();
            }
        }

        private void loadLang()
        {
            btnAddFAQ.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeleteFAQ.Text = LangController.getLng("litDelete.Text", "Xóa");
            litAddFAQ.Text = LangController.getLng("litAddSlider.Text", "Thêm / sửa slider");
            btnAddSaveFAQ.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelFAQ.Text = LangController.getLng("litCancel.Text", "Hủy");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrFAQ.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrFAQ.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrFAQ.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = FaqController.deleteFaq(gvIDs);

                if (result)
                {
                    loadFAQ();
                    try
                    {
                        lblFrmResponseFAQ.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseFAQ.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseFAQ.Visible = true;
                    }
                    catch
                    {
                        lblFrmResponseFAQ.ForeColor = System.Drawing.Color.Red;
                        lblFrmResponseFAQ.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                        frmResponseFAQ.Visible = true;
                    }
                }
                else
                {
                    lblFrmResponseFAQ.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseFAQ.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseFAQ.Visible = true;
                }
            }
            resetAddFAQ();
        }

        private void resetAddFAQ()
        {
            txtAsk.Text = txtAnswer.Text = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddFAQ.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveFAQ_Click(object sender, EventArgs e)
        {
            bool rs;

            if (this.hdfId.Value != "")
            {
                rs = FaqController.updateFaq(new FAQ
                {
                    id = int.Parse(hdfId.Value),
                    ask = txtAsk.Text,
                    answer = txtAnswer.Text
                });
            }
            else
            {
                rs = FaqController.createFaq(new FAQ
                {
                    ask = txtAsk.Text,
                    answer = txtAnswer.Text
                });
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfId.Value == "" ? FaqController.getCurrentId() : hdfId.Value;

                loadFAQ();
                lblFrmResponseFAQ.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseFAQ.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseFAQ.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseFAQ.Visible = true;
            }
            else
            {
                lblFrmResponseFAQ.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseFAQ.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseFAQ.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseFAQ.Visible = true;
            }
            resetAddFAQ();
        }


        protected void dgrFAQ_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadFAQ();
            DataTable dt = (DataTable)dgrFAQ.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtAsk.Text = dr["Ask"].ToString();
            txtAnswer.Text = dr["Answer"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddFAQ.Attributes["style"] = "display:block";
            frmResponseFAQ.Visible = false;
        }

        protected void dgrFAQ_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrFAQ.CurrentPageIndex = e.NewPageIndex;
            loadFAQ();
            frmResponseFAQ.Visible = false;
            resetAddFAQ();
        }
    }
}