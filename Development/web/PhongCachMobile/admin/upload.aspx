﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="upload.aspx.cs" Inherits="PhongCachMobile.admin.upload" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function check_uncheck(Val) {
        var ValChecked = Val.checked;
        var ValId = Val.id;
        var frm = document.forms[0];
        // Loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for Header Template's Checkbox
            //As we have not other control other than checkbox we just check following statement
            if (this != null) {
                if (ValId.indexOf('CheckAll') != -1) {
                    // Check if main checkbox is checked,
                    // then select or deselect datagrid checkboxes
                    //alert(frm.elements[i].id);
                    // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                    if (frm.elements[i].id.indexOf('deleteRec') != -1)
                        if (ValChecked)
                            frm.elements[i].checked = true;
                        else
                            frm.elements[i].checked = false;
                }
                else if (ValId.indexOf('deleteRec') != -1) {
                    //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                    // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                    document.getElementById('content_dgrImage_CheckAll').checked = false;

                    if (frm.elements[i].checked == false)
                        frm.elements[1].checked = false;
                }
            } // if
        } // for
    } // function</PRE>

    function confirmMsg(frm) {
        // loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for our checkboxes only
            if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                // If any are checked then confirm alert, otherwise nothing happens
                if (frm.elements[i].checked)
                    return confirm('Bạn có chắc muốn xóa?')
            }
        }
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            Upload hình ảnh</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponse" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponse" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmApp" runat="server">
                                    <p class="field">
                                        Hình ảnh &nbsp; 
                                        <asp:FileUpload ID="fulImage" runat="server" /> &nbsp; 
                                        <asp:LinkButton ID="lbtUpload" runat="server" onclick="lbtUpload_Click">Upload</asp:LinkButton> </p>
                                       
                                    <asp:DataGrid ID="dgrImage" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" 
                                        onpageindexchanged="dgrImage_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Name" HeaderText="Tên" >
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                    Width="40%" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("Image") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("Name")%>' AlternateText='<%#Eval("Name")%>'  ImageUrl='<%# Eval("Image") %>' Width="30%" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="Url" HeaderText="Liên kết" Visible="True"></asp:BoundColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" 
                                            HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnDeleteImage" runat="server" CssClass="wpcf7-submit"
                                            CausesValidation="False" OnClientClick="return confirmMsg(this.form)"
                                            ClientIDMode="Static"  Text="Xóa" OnClick="btnDeleteImage_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
