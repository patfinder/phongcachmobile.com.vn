﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="banner.aspx.cs" Inherits="PhongCachMobile.admin.banner" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function addBanner() {
        jQuery('#content_divAddBanner').show();
        jQuery('#content_divAddDeleteBanner').hide();
    }

    function cancelAddSaveBanner() {
        jQuery('#content_divAddBanner').hide();
        jQuery('#content_divAddDeleteBanner').show();
    }

    function check_uncheck(Val) {
        var ValChecked = Val.checked;
        var ValId = Val.id;
        var frm = document.forms[0];
        // Loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for Header Template's Checkbox
            //As we have not other control other than checkbox we just check following statement
            if (this != null) {
                if (ValId.indexOf('CheckAll') != -1) {
                    // Check if main checkbox is checked,
                    // then select or deselect datagrid checkboxes
                    //alert(frm.elements[i].id);
                    // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                    if (frm.elements[i].id.indexOf('deleteRec') != -1)
                        if (ValChecked)
                            frm.elements[i].checked = true;
                        else
                            frm.elements[i].checked = false;
                }
                else if (ValId.indexOf('deleteRec') != -1) {
                    //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                    // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                    document.getElementById('content_dgrBanner_CheckAll').checked = false;
                    if (frm.elements[i].checked == false)
                        frm.elements[1].checked = false;
                }
            } // if
        } // for
    } // function</PRE>

    function confirmMsg(frm) {
        // loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for our checkboxes only
            if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                // If any are checked then confirm alert, otherwise nothing happens
                if (frm.elements[i].checked)
                    return confirm('Bạn có chắc muốn xóa?')
            }
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litBanner" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseBanner" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseBanner" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmBanner">
                                  <p class="field">
                                         <asp:Literal ID="litGroupChoosing" runat="server"></asp:Literal> &nbsp; 
                                            <asp:DropDownList ID="ddlGroup" runat="server" 
                                             onselectedindexchanged="ddlGroup_SelectedIndexChanged" 
                                             AutoPostBack="True">
                                            </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrBanner" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" 
                                        oneditcommand="dgrBanner_EditCommand" 
                                        onpageindexchanged="dgrBanner_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                               <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("DisplayImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("Text")%>' AlternateText='<%#Eval("Text")%>' ImageUrl='<%# Eval("DisplayImage") %>' Width="50" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                             <%--<asp:TemplateColumn HeaderText="Ảnh thumb">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("ThumbImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image2" ToolTip='<%#Eval("Text")%>' AlternateText='<%#Eval("Text")%>' ImageUrl='<%# Eval("ThumbImage") %>' Width="50" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>--%>
                                            <asp:BoundColumn DataField="ACTIONURL" HeaderText="Liên kết">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                              <asp:BoundColumn DataField="POSTEDDATE" HeaderText="Ngày đăng">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật">
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                            Font-Strikeout="False" Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnAddBanner" runat="server" CssClass="wpcf7-submit" OnClientClick="return addBanner();"
                                            UseSubmitBehavior="false" ClientIDMode="Static"  />
                                        <asp:Button ID="btnDeleteBanner" runat="server" CssClass="wpcf7-submit"
                                            Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return confirmMsg(this.form)"
                                            ClientIDMode="Static" OnClick="btnDelete_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddBanner" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litAddBanner" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            <asp:Literal ID="litText" runat="server"></asp:Literal>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtText" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="1000"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Tiêu đề
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtTitle" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="1000"></asp:TextBox></span>
                                        </p>
                                          <p class="field">
                                            <asp:Literal ID="litActionUrl" runat="server"></asp:Literal>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtActionUrl" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                          <p class="field">
                                            <asp:Literal ID="litDisplayImage" runat="server"></asp:Literal>
                                            <br />
                                            <asp:HyperLink ID="hplDisplayImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgDisplay" runat="server" style="max-width:30%;" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulDisplayImage" runat="server" />
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litThumbImage" runat="server"></asp:Literal>
                                            <br />
                                            <asp:HyperLink ID="hplThumbImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgThumb" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulThumbImage" runat="server" />
                                        </p>
                                   
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveBanner" runat="server" class="wpcf7-submit" OnClick="btnAddSaveBanner_Click" />
                                            <asp:Button ID="btnCancelBanner" runat="server" CssClass="wpcf7-submit"
                                                Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return cancelAddSaveBanner();"
                                                UseSubmitBehavior="false" ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div2" class="widget">
                            <h2>
                                <asp:Literal ID="litAddBannerHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div3" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddBannerHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
