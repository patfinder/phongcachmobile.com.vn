﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFooter.ascx.cs" Inherits="PhongCachMobile.admin.master.uc.ucFooter" %>
<div id="footer">
    <div class="container_12 clearfix">
        <div class="grid_12">
            <div class="copy">
                <asp:Literal ID="litCopyright" runat="server"></asp:Literal> &nbsp; | &nbsp; <a target='_blank' href='upload'>Upload</a><br/>
                <!-- {%FOOTER_LINK} -->
            </div>
            <dl class="phone">
                <dt>
                    </dt>
                <dd>
                    <asp:Literal ID="litPhoneNumber" runat="server"></asp:Literal></dd>
            </dl>
        </div>
    </div>
    <!--.container-->
</div>
