﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile.admin.master.uc
{
    public partial class ucFooter : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadLanguages();
        }

        private void loadLanguages()
        {
            litCopyright.Text = "<a target='_blank' href='../gia-hot'>PhongCachMobile</a>" + " © " + DateTime.Now.Year;
            litPhoneNumber.Text = LangController.getLng("litPhoneNumber.Text", "A.Vũ - 09 3897 3858<br/>Lân - 012 136 44445");
        }
    }
}