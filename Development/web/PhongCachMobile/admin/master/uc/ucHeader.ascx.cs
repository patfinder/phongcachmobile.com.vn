﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Configuration;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile.admin.master.uc
{
    public partial class ucHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkSession();
            constructHomeMenu();
        }

        private void checkSession()
        {
            if (Session["UserId"] != null)
            {
                DataTable dt = SysUserController.getUserById(Session["UserId"].ToString());
                if (dt != null && dt.Rows.Count > 0)
                {

                    if (dt.Rows[0]["GroupId"].ToString() != "32" &&
                        dt.Rows[0]["GroupId"].ToString() != "54" 
                        //dt.Rows[0]["GroupId"].ToString() != "16" &&
                        //dt.Rows[0]["GroupId"].ToString() != "15"
                        )
                        Response.Redirect("dang-nhap", true);
                }
                else
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap",true);
            }
        }

        private void constructHomeMenu()
        {
            litHomeMenu.Text = "";
            litHomeMenu.Text += "<ul id='topnav' class='sf-menu sf-js-enabled sf-shadow'>";

            DataTable dt = MenuController.getMenus(Session["GroupId"].ToString(), null);

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string menuId = dt.Rows[i]["Id"].ToString();
                    string groupId = dt.Rows[i]["GroupId"].ToString();
                    string action_url = dt.Rows[i]["ActionUrl"].ToString();
                    string parent_id = dt.Rows[i]["ParentId"].ToString();
                    string text = dt.Rows[i]["Text"].ToString();

                    litHomeMenu.Text += "<li class='menu-item menu-item-type-post_type'><a href='" + action_url.Split('.')[0] + "'> " + text + "</a>";
                    constructSubMenuHome(groupId, menuId);
                    litHomeMenu.Text += "</li>";

                }
            }
            litHomeMenu.Text += "</ul>";
        }

        private void constructSubMenuHome(string groupId, string parentId)
        {
            DataTable dt = MenuController.getMenus(groupId, parentId);

            if (dt != null && dt.Rows.Count >0)
            {
                litHomeMenu.Text += "<ul id='sub-menu' class='sf-menu sf-js-enabled sf-shadow'>";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string subMenuId = dt.Rows[i]["Id"].ToString();
                    string subGroupId = dt.Rows[i]["GroupId"].ToString();
                    string action_url = dt.Rows[i]["ActionUrl"].ToString();
                    string parent_id = dt.Rows[i]["ParentId"].ToString();
                    string text = dt.Rows[i]["Text"].ToString();
                    if (action_url == "truyen.aspx")
                        litHomeMenu.Text += "<li class='menu-item menu-item-type-post_type'><a href='" + action_url + "'> " + text + "</a>";
                    else
                        litHomeMenu.Text += "<li class='menu-item menu-item-type-post_type'><a href='" + action_url.Split('.')[0] + "'> " + text + "</a>";
                    constructSubMenuHome(subGroupId, subMenuId);
                    litHomeMenu.Text += "</li>";

                }
                litHomeMenu.Text += "</ul>";
            }
        }
    }
}