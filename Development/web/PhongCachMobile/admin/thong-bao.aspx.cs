﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class thong_bao : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litMsgManager.Text", "Quản lý ngôn ngữ thông báo");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadAllMsg();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }


        private void loadAllMsg()
        {
            DataTable dt = LangMsgController.getLngMsgs();
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("DisplayValue", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    string displayValue = HTMLRemoval.StripTagsRegex(dr["Value"].ToString());
                    if (displayValue.Length > 100)
                    {
                        displayValue = displayValue.Substring(0, 100);
                        displayValue = displayValue.Substring(0, displayValue.LastIndexOf(' ')) + "...";
                    }
                    dr["DisplayValue"] = displayValue;
                }
                dgrMsg.DataSource = dt;
                dgrMsg.DataBind();
            }
        }

        private void loadLang()
        {
            litMsg.Text = LangController.getLng("litLanguageMsg.Text", "Ngôn ngữ thông báo");
            btnDeleteMsg.Text = LangController.getLng("litDelete.Text", "Xóa");
            litEditMsg.Text = LangController.getLng("litEditMsg.Text", "Chỉnh sửa ngôn ngữ thông báo");
            litValue.Text = LangController.getLng("litValue.Text", "Giá trị");
            btnAddSaveMsg.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelMsg.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddMsgHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litAddMsgHelpDesc.Text = LangMsgController.getLngMsg("litAddMsgHelp.Desc", "Những thông tin giúp đỡ về cách chỉnh sửa ngôn ngữ thông báo");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrMsg.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrMsg.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrMsg.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = LangMsgController.deleteLangMsg(gvIDs);

                if (result)
                {
                    loadAllMsg();
                    try
                    {
                        lblFrmResponseMsg.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseMsg.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseMsg.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseMsg.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseMsg.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseMsg.Visible = true;
                }
            }
            resetAddMsg();
        }

        private void resetAddMsg()
        {
            txtValue.Text = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddMsg.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveMsg_Click(object sender, EventArgs e)
        {
            bool rs = false;
            if (this.hdfId.Value != "")
            {
                rs = LangMsgController.updateLngMsgById(hdfId.Value, txtValue.Text);
            }
            else
            {
                //rs = ServerController.createServer(new Server(txtName.Text, txtCode.Text));
            }
            if (rs)
            {

                loadAllMsg();
                lblFrmResponseMsg.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseMsg.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseMsg.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseMsg.Visible = true;
            }
            else
            {
                lblFrmResponseMsg.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseMsg.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseMsg.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseMsg.Visible = true;
            }
            resetAddMsg();
        }


        protected void dgrMsg_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllMsg();
            DataTable dt = (DataTable)dgrMsg.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtValue.Text = dr["Value"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddMsg.Attributes["style"] = "display:block";
            frmResponseMsg.Visible = false;
        }

        protected void dgrMsg_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrMsg.CurrentPageIndex = e.NewPageIndex;
            loadAllMsg();
            frmResponseMsg.Visible = false;
            resetAddMsg();
        }
    }
}