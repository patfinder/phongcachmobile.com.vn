﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class binh_luan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litCommentManager.Text", "Quản lý bình luận");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                dgrComment.DataBind();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadAllComment(string from, string to)
        {
            string timeFrom = "";
            string timeTo = "";
            if (to == "")
                timeTo = DateTime.Now.ToString("yyyyMMddHHmmsss");
            else
                timeTo = Common.strToFormatDateTime(to, "dd/MM/yyyy HH:mm").ToString("yyyyMMddHHmmss"); ;
            timeFrom = Common.strToFormatDateTime(from, "dd/MM/yyyy HH:mm").ToString("yyyyMMddHHmmss");

            DataTable dt = CommentController.getComments(timeFrom, timeTo);
            if (dt != null && dt.Rows.Count > 0)
            {
                dgrComment.DataSource = dt;
                dt.Columns.Add("Target", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                    if (dr["ProductId"].ToString() != "")
                        dr["Target"] = "../sp-" + dr["ProductId"].ToString() + "#_comment";
                    else if (dr["ArticleId"].ToString() != "")
                        dr["Target"] = "../tt-" + dr["ArticleId"].ToString() + "#_comment";
                }
                dgrComment.DataBind();
            }
        }

        private void loadLang()
        {
            litComment.Text = LangController.getLng("litComment.Text", "Bình luận");
            btnDeleteComment.Text = LangController.getLng("litDelete.Text", "Xóa");
            litEditComment.Text = LangController.getLng("litEditComment.Text", "Chỉnh sửa bình luận");
            litDescription.Text = LangController.getLng("litDescription.Text", "Mô tả");
            btnAddSaveComment.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelComment.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddCommentHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litFrom.Text = LangController.getLng("litFromTime.Text", "Thời gian bất đầu");
            litTo.Text = LangController.getLng("litToTime.Text", "Thời gian kết thúc");
            litAddCommentHelpDesc.Text = LangMsgController.getLngMsg("litAddCommentHelp.Desc", "Những thông tin giúp đỡ về cách thêm / chỉnh sửa bình luận.");
            lbtFind.Text = LangController.getLng("litSearch.Text", "Tìm kiếm");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrComment.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrComment.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrComment.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = CommentController.deleteComment(gvIDs);
                loadAllComment(txtFrom.Text, txtTo.Text);

                if (result)
                {
                    try
                    {
                        lblFrmResponseComment.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseComment.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseComment.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseComment.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseComment.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseComment.Visible = true;
                }
            }
            resetAddComment();
        }

        private void resetAddComment()
        {
            txtDescription.Text = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddComment.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveComment_Click(object sender, EventArgs e)
        {
            bool rs = false;
            if (this.hdfId.Value != "")
            {
                rs = CommentController.updateComment(new Comment(int.Parse(hdfId.Value), txtDescription.Text));
            }
            else
            {
                //rs = ServerController.createServer(new Server(txtName.Text, txtCode.Text));
            }
            if (rs)
            {
                string id = hdfId.Value;
                loadAllComment(txtFrom.Text, txtTo.Text);

                lblFrmResponseComment.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseComment.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseComment.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseComment.Visible = true;
            }
            else
            {
                lblFrmResponseComment.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseComment.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseComment.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseComment.Visible = true;
            }
            resetAddComment();
        }


        protected void dgrComment_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllComment(txtFrom.Text, txtTo.Text);

            DataTable dt = (DataTable)dgrComment.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtDescription.Text = dr["Description"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddComment.Attributes["style"] = "display:block";
            frmResponseComment.Visible = false;
        }

        protected void dgrComment_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrComment.CurrentPageIndex = e.NewPageIndex;
            loadAllComment(txtFrom.Text, txtTo.Text);
            frmResponseComment.Visible = false;
            resetAddComment();
        }

        protected void lbtFind_Click(object sender, EventArgs e)
        {
            loadAllComment(txtFrom.Text, txtTo.Text);
        }

        protected void btnApprovedComment_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrComment.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrComment.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrComment.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = CommentController.approveComment(gvIDs);
                loadAllComment(txtFrom.Text, txtTo.Text);

                if (result)
                {
                    try
                    {
                        lblFrmResponseComment.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseComment.Text = LangController.getLng("litApproveOk.Text", "Cho phép hiển thị thành công");
                        frmResponseComment.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseComment.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseComment.Text = LangController.getLng("litApproveError.Text", "Cho phép hiển thị thất bại");
                    frmResponseComment.Visible = true;
                }
            }
            resetAddComment();
        }
    }
}