﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dang-nhap.aspx.cs" Inherits="PhongCachMobile.admin.dang_nhap" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!-- The HTML5 Shim is required for older browsers, mainly older versions IE -->
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <!--[if lt IE 7]>
    <div style=' clear: both; text-align:center; position: relative;'>
    	<a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0" &nbsp;alt="" /></a>
    </div>
  <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
     <link rel="icon" href="../../images/fav.ico"  type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="all" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/prettyPhoto.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="css/grid.css"/>
    <link rel="stylesheet" href="css/styles.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="css/pagenavi-css.css" type="text/css" media="all"/>
    <script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="js/superfish.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="js/jquery.loader.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript">
        // initialise plugins
        jQuery(function () {
            // main navigation init
            jQuery('ul.sf-menu').superfish({
                delay: 1000, 		// one second delay on mouseout 
                animation: { opacity: 'show', height: 'show' }, // fade-in and slide-down animation 
                speed: 'normal',  // faster animation speed 
                autoArrows: false,        // generation of arrow mark-up (for submenu) 
                dropShadows: true    // drop shadows (for submenu)
            });

            // prettyphoto init
            jQuery("#gallery .portfolio a[rel^='prettyPhoto']").prettyPhoto({
                animationSpeed: 'slow',
                theme: 'facebook',
                slideshow: false,
                autoplay_slideshow: false,
                show_title: true,
                overlay_gallery: false
            });

            // box effects
            jQuery(".box").hover(function () {
                jQuery(this).find(".icon").stop().animate({ top: "28" }, { easing: "easeOutElastic", duration: 800 })
            }, function () {
                jQuery(this).find(".icon").stop().animate({ top: "0" }, { easing: "easeOutElastic", duration: 800 })
            });

        });
    </script>

</head>
<body class="">
    <div id="main">
        <!-- this encompasses the entire Web site -->
        <div id="header">
            <div class="container_12">
                <div class="grid_12">
                    <nav class="primary_cp">
                           </nav>
                    <!--.primary-->
                </div>
            </div>
            <!--.container-->
        </div>
        <div class="container_12 primary_content_wrap clearfix">
            <div id="content" class="grid_12">
                <div class="box">
                    <div class="wrapper">
                        <div class="three_fifth">
                            <div id="text-5">
                                <h2>
                                    <asp:Literal ID="litLogIn" runat="server"></asp:Literal></h2>
                                <div class="textwidget">
                                    <div class="wpcf7" id="wpcf7-f1-w1-o1">
                                        <div id="frmResponse" runat="server" visible="false">
                                            <p class="field" style="color:Red">
                                                <asp:Literal ID="litFrmResponse_Login" runat="server"></asp:Literal>
                                            </p>
                                        </div>
                                        <div id="frmLogin" runat="server">
                                            <form id="Form1" runat="server" class="wpcf7-form">
                                            <p class="field">
                                                <asp:Literal ID="litUsername" runat="server"></asp:Literal><small> *
                                                </small>
                                                <asp:RequiredFieldValidator ID="reqValUsername" runat="server" ControlToValidate="txtUsername"
                                                    ForeColor="Red"></asp:RequiredFieldValidator><br />
                                                <span class="wpcf7-form-control-wrap your-name">
                                                    <asp:TextBox ID="txtUsername" runat="server" CssClass="wpcf7-text" Width="300px"
                                                        MaxLength="40"></asp:TextBox></span>
                                            </p>
                                            <p class="field">
                                                <asp:Literal ID="litPassword" runat="server"></asp:Literal><small> * </small>
                                                <asp:RequiredFieldValidator ID="reqValPassword" runat="server" ControlToValidate="txtPassword"
                                                    Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                <br />
                                                <span class="wpcf7-form-control-wrap your-email">
                                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="wpcf7-text" Width="300px"
                                                        MaxLength="50" TextMode="Password"></asp:TextBox></span>
                                            </p>
                                            <p class="submit-wrap">
                                                <asp:Button ID="btnLogin" runat="server" class="wpcf7-submit" OnClick="btnLogin_Click" />
                                            </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two_fifth last">
                            <div id="text-4" class="widget">
                                <h2>
                                    <asp:Literal ID="litHelp" runat="server"></asp:Literal></h2>
                                <div class="textwidget">
                                </div>
                            </div>
                            <div id="text-6" class="widget">
                                <div class="textwidget">
                                    <dl class="address">
                                        <dt>
                                            <asp:Literal ID="litHelpLogin" runat="server"></asp:Literal></dt>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--#content-->
        </div>
        <!--.container-->
        <div id="footer">
    <div class="container_12 clearfix">
        <div class="grid_12">
            <div class="copy">
                <asp:Literal ID="litCopyright" runat="server"></asp:Literal>
                <%--<a href="/aboutus.aspx" class="privacy">
                    <asp:Literal ID="litAboutUsText_Footer" runat="server"></asp:Literal></a>--%><br/>
                <!-- {%FOOTER_LINK} -->
            </div>
            <dl class="phone">
                <dt>
                    <asp:Literal ID="litMakeAnAppointment" runat="server"></asp:Literal></dt>
                <dd>
                    <asp:Literal ID="litPhoneNumber" runat="server"></asp:Literal></dd>
            </dl>
        </div>
    </div>
    <!--.container-->
</div>

    </div>
    <!--#main-->
    <script type="text/javascript" src="js/jquery.form.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    <!-- this is used by many Wordpress features and for plugins to work proporly -->
    <!-- Show Google Analytics -->
</body>
</html>
