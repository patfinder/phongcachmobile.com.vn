﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.IO;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class game : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litGameManager.Text", "Quản lý game");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadCategory();
                loadOS();
                loadAllGame();
            }
        }

        private void loadOS()
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId("5");
            ddlOS.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string id = dr["Id"].ToString();
                    string name = dr["Name"].ToString();
                    ddlOS.Items.Add(new ListItem(name, id));
                }
            }
            ddlOS.DataBind();
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadAllGame()
        {
            DataTable dt = GameController.getGames(ddlCategory.SelectedValue, ddlOS.SelectedValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                string imagePath = "images/game/";
                foreach (DataRow dr in dt.Rows)
                {
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"].ToString();
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                }
            }
            dgrGame.DataSource = dt;
            dgrGame.DataBind();
        }

        private void loadCategory()
        {
            DataTable dt = CategoryController.getCategories("5", null);
            ddlCategory.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string id = dr["Id"].ToString();
                    string name = dr["Name"].ToString();
                    ddlCategory.Items.Add(new ListItem(name, id));
                }
            }
            ddlCategory.DataBind();
        }

        private void loadLang()
        {
            this.litGame.Text = LangController.getLng("litGame.Text", "Trò chơi");
            this.litAddGameHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litAddGameHelpDesc.Text = LangMsgController.getLngMsg("litAddGameHelpDesc.Desc", "Những thông tin giải thích về thêm / sửa trò chơi");

            this.btnAddGame.Text = LangController.getLng("litAdd.Text", "Thêm");
            this.btnDeleteGame.Text = LangController.getLng("litDelete.Text", "Xóa");
            this.btnAddSaveGame.Text = LangController.getLng("litSave.Text", "Lưu");
            this.btnCancelGame.Text = LangController.getLng("litCancel.Text", "Hủy");
        }

        protected void btnDeleteGame_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrGame.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrGame.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrGame.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = GameController.deleteGame(gvIDs);
                if (result)
                {
                    loadAllGame();
                    //Delete images
                    try
                    {
                        string imagePath = "images/game/";

                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');
                        //loi chua delete hinh
                        foreach (FileInfo file in files)
                        {
                            try
                            {
                                if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                                {
                                    File.Delete(file.FullName);
                                }
                            }
                            catch (Exception ex)
                            {
                                string message = ex.Message;
                            }
                        }

                        lblFrmResponseGame.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseGame.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseGame.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseGame.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseGame.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseGame.Visible = true;
                }
            }
            resetAddGame();
        }

        private void resetAddGame()
        {
            txtName.Text = txtDesc.Text = txtCurrentVerion.Text = txtDeveloper.Text = txtRequiredOS.Text = txtFilesize.Text =
                txtLikeCount.Text = txtViewCount.Text = imgDisplay.ImageUrl = hplDisplayImg.NavigateUrl = litFileName.Text = 
                txtFilename.Text = "";

            hdfId.Value = "";
            chbIsFree.Checked = false;
            divAddDelete.Attributes["style"] = "display:block";
            divAddGame.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveGame_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = "images/game/";
            string strGamePath = "game/";
            if (this.hdfId.Value != "")
            {
                if (fulDisplayImg.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgDisplay.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                rs = GameController.updateGame(new Game{
                    id = int.Parse(hdfId.Value),
                    name = txtName.Text,
                    isFree = chbIsFree.Checked,
                    description = txtDesc.Text, 
                    categoryId = int.Parse(ddlCategory.SelectedValue),
                    likeCount = int.Parse(txtLikeCount.Text),
                    viewCount = int.Parse(txtViewCount.Text),
                    filename = txtFilename.Text,
                    currentVersion = txtCurrentVerion.Text,
                    developer = txtDeveloper.Text,
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    requiredOS = txtRequiredOS.Text, 
                    osId = int.Parse(ddlOS.SelectedValue),
                    filesize = txtFilesize.Text,
                });
            }
            else
            {
                rs = GameController.createGame(new Game
                {
                    name = txtName.Text,
                    isFree = chbIsFree.Checked,
                    description = txtDesc.Text,
                    categoryId = int.Parse(ddlCategory.SelectedValue),
                    likeCount = int.Parse(txtLikeCount.Text),
                    viewCount = int.Parse(txtViewCount.Text),
                    filename = txtFilename.Text,
                    currentVersion = txtCurrentVerion.Text,
                    developer = txtDeveloper.Text,
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    requiredOS = txtRequiredOS.Text,
                    osId = int.Parse(ddlOS.SelectedValue),
                    filesize = txtFilesize.Text
                });
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfId.Value == "" ? GameController.getCurrentId() : hdfId.Value;
                try
                {
                    if (fulDisplayImg.PostedFile.FileName != "")
                    {
                        fulDisplayImg.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulDisplayImg.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImg.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update GAME set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }

                    //if (fulFile.PostedFile.FileName != "")
                    //{
                    //    fulFile.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strGamePath) + id + "_" + fulFile.PostedFile.FileName);
                    //    object[,] parameters = { { "@Filename", id + "_" + fulFile.PostedFile.FileName }, 
                    //                           {"@Id", id}};
                    //    DB.exec("update GAME set Filename = @Filename where Id = @Id", parameters);
                    //}
                }
                catch (Exception ex)
                {
                }

                loadAllGame();
                lblFrmResponseGame.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseGame.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseGame.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseGame.Visible = true;
            }
            else
            {
                lblFrmResponseGame.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseGame.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseGame.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseGame.Visible = true;
            }
            resetAddGame();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrGame.CurrentPageIndex = 0;
            loadAllGame();
            frmResponseGame.Visible = false;
        }

        protected void dgrGame_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllGame();
            DataTable dt = (DataTable)dgrGame.DataSource;
            DataRow dr = dt.Rows[iIndex];

            this.txtName.Text = dr["Name"].ToString();
            this.txtDesc.Text = dr["Description"].ToString();
            this.txtCurrentVerion.Text = dr["CurrentVersion"].ToString();
            this.txtDeveloper.Text = dr["Developer"].ToString();
            this.txtRequiredOS.Text = dr["RequiredOS"].ToString();
            this.txtFilesize.Text = dr["Filesize"].ToString();
            this.txtLikeCount.Text = dr["LikeCount"].ToString();
            this.txtViewCount.Text = dr["ViewCount"].ToString();
            this.txtFilename.Text = dr["FileName"].ToString();
            this.imgDisplay.ImageUrl = dr["DisplayImage"].ToString();
            this.hplDisplayImg.NavigateUrl = dr["DisplayImage"].ToString();
            litFileName.Text = dr["Filename"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            chbIsFree.Checked = bool.Parse(dr["IsFree"].ToString());

            divAddDelete.Attributes["style"] = "display:none";
            divAddGame.Attributes["style"] = "display:block";
            frmResponseGame.Visible = false;
        }

        protected void dgrGame_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrGame.CurrentPageIndex = e.NewPageIndex;
            loadAllGame();
            frmResponseGame.Visible = false;
            //Disable add menu
            resetAddGame();
        }

        protected void ddlOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrGame.CurrentPageIndex = 0;
            loadAllGame();
            frmResponseGame.Visible = false;
        }
    }
}