﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using System.IO;
using PhongCachMobile.model;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class cam_nhan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litTestimonialManager.Text", "Quản lý cảm nhận khách hàng");
            if (!this.IsPostBack)
            {
                loadLang();
                loadAllTestimonial();
            }
        }

        private void loadAllTestimonial()
        {
            DataTable dt = TestimonialController.getTestimonial();
            if (dt != null && dt.Rows.Count > 0)
            {
                string imagePath = "images/testimonial/";
                foreach (DataRow dr in dt.Rows)
                {
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"].ToString();

                }
            }
            dgrTest.DataSource = dt;
            dgrTest.DataBind();
        }

        private void loadLang()
        {
            litTest.Text = LangController.getLng("litTestimonial.Text", "Cảm nhận của khách hàng");
            btnAddTest.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeleteTest.Text = LangController.getLng("litDelete.Text", "Xóa");
            btnAddSaveTest.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelTest.Text = LangController.getLng("litCancel.Text", "Hủy");
        }

        protected void btnDeleteTest_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrTest.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrTest.Items[i].FindControl("deleteRecPrd");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrTest.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = TestimonialController.deleteTestimonial(gvIDs);

                if (result)
                {
                    loadAllTestimonial();
                    //Delete images
                    try
                    {
                        string imagePath = "images/testimonial/";
                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');

                        //loi chua delete hinh
                        foreach (FileInfo file in files)
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    string message = ex.Message;
                                }
                            }
                        }

                        lblFrmResponseTest.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseTest.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseTest.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseTest.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseTest.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseTest.Visible = true;
                }
            }
            resetAddTest();
        }

        private void resetAddTest()
        {
            txtName.Text = txtText.Text = txtShortDesc.Text =  "";
            hdfId.Value = "";

            hplDisplayImage.NavigateUrl = "";
            imgDisplay.ImageUrl = "";

            divAddDeleteTest.Attributes["style"] = "display:block";
            divAddTest.Attributes["style"] = "display:none";

        }

        protected void btnAddSaveTest_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = "images/testimonial/";
            if (this.hdfId.Value != "")
            {
                if (fulDisplayImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgDisplay.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                rs = TestimonialController.updateTestimonial(new Testimonial
                {
                    id = int.Parse(hdfId.Value),
                    name = txtName.Text,
                    shortDesc = txtShortDesc.Text,
                    text = txtText.Text,
                });

            }
            else
            {
                rs = TestimonialController.createTestimonial(new Testimonial
                {
                    name = txtName.Text,
                    shortDesc = txtShortDesc.Text,
                    text = txtText.Text,
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss")
                });
            }
            if (rs)
            {
                string id = this.hdfId.Value == "" ? TestimonialController.getCurrentId() : hdfId.Value;

                // upload Image
                try
                {
                    if (fulDisplayImage.PostedFile.FileName != "")
                    {
                        fulDisplayImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulDisplayImage.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update TESTIMONIAL set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }

                }
                catch (Exception ex)
                {
                }

                loadAllTestimonial();
                lblFrmResponseTest.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseTest.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseTest.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseTest.Visible = true;
            }
            else
            {
                lblFrmResponseTest.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseTest.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseTest.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseTest.Visible = true;
            }
            resetAddTest();
        }


        protected void dgrTest_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrTest.CurrentPageIndex = e.NewPageIndex;
            loadAllTestimonial();
            frmResponseTest.Visible = false;
            resetAddTest();
        }

        protected void dgrTest_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllTestimonial();
            DataTable dt = (DataTable)dgrTest.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtName.Text = dr["Name"].ToString();
            txtText.Text = dr["Text"].ToString();
            txtShortDesc.Text = dr["ShortDesc"].ToString();
            this.imgDisplay.ImageUrl = this.hplDisplayImage.NavigateUrl = dr["DisplayImage"].ToString();

            this.hdfId.Value = dr["Id"].ToString();
            divAddDeleteTest.Attributes["style"] = "display:none";
            divAddTest.Attributes["style"] = "display:block";

            frmResponseTest.Visible = false;
        }

    }
}