﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="tin-tuc.aspx.cs" Inherits="PhongCachMobile.admin.tin_tuc" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">
     function addArticle() {
         jQuery('#content_divAddArticle').show();
         jQuery('#content_divAddDelete').hide();
     }

     function cancelAddSaveArticle() {
         jQuery('#content_divAddArticle').hide();
         jQuery('#content_divAddDelete').show();
     }

     function check_uncheck(Val) {
         var ValChecked = Val.checked;
         var ValId = Val.id;
         var frm = document.forms[0];
         // Loop through all elements
         for (i = 0; i < frm.length; i++) {
             // Look for Header Template's Checkbox
             //As we have not other control other than checkbox we just check following statement
             if (this != null) {
                 if (ValId.indexOf('CheckAll') != -1) {
                     // Check if main checkbox is checked,
                     // then select or deselect datagrid checkboxes
                     //alert(frm.elements[i].id);
                     // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                     if (frm.elements[i].id.indexOf('deleteRec') != -1)
                         if (ValChecked)
                             frm.elements[i].checked = true;
                         else
                             frm.elements[i].checked = false;
                 }
                 else if (ValId.indexOf('deleteRec') != -1) {
                     //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                     // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                     document.getElementById('content_dgrArticle_CheckAll').checked = false;

                     if (frm.elements[i].checked == false)
                         frm.elements[1].checked = false;
                 }
             } // if
         } // for
     } // function</PRE>

     function confirmMsg(frm) {
         // loop through all elements
         for (i = 0; i < frm.length; i++) {
             // Look for our checkboxes only
             if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                 // If any are checked then confirm alert, otherwise nothing happens
                 if (frm.elements[i].checked)
                     return confirm('Bạn có chắc muốn xóa?')
             }
         }
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litArticle" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseArticle" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseArticle" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmArticle" runat="server">
                                    <p class="field">
                                        <asp:Literal ID="litCategoryChoosing" runat="server"> </asp:Literal> &nbsp; 
                                        <asp:DropDownList
                                            ID="ddlCategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrArticle" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" 
                                        oneditcommand="dgrArticle_EditCommand" 
                                        onpageindexchanged="dgrArticle_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="TITLE" HeaderText="Tiêu đề" >
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                    Width="40%" />
                                            </asp:BoundColumn>
                                            <%--<asp:BoundColumn DataField="SHORTDESC" HeaderText="Nội dung ngắn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="GROUPID" HeaderText="GROUPID" Visible="False">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("DisplayImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("Title")%>' AlternateText='<%#Eval("Title")%>'  ImageUrl='<%# Eval("DisplayImage") %>' Width="30%" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="LONGDESC" HeaderText="Nội dung" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="POSTEDDATE" HeaderText="Ngày đăng">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CATEGORYID" HeaderText="CATID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="VIEWCOUNT" HeaderText="Lượt xem" DataFormatString="{0:#,###}"></asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật">
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" 
                                            HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnAddArticle" runat="server" CssClass="wpcf7-submit" OnClientClick="return addArticle();"
                                            UseSubmitBehavior="false" ClientIDMode="Static"  />
                                        <asp:Button ID="btnDeleteArticle" runat="server" CssClass="wpcf7-submit"
                                            Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return confirmMsg(this.form)"
                                            ClientIDMode="Static" OnClick="btnDeleteArticle_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddArticle" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litAddArticle" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseAddArticle" runat="server" visible="false">
                                        <p class="field" style="color: Red">
                                            <asp:Literal ID="litFrmResponseAddArticle" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            <asp:Literal ID="litTitle" runat="server"></asp:Literal><small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValTitle" runat="server" ControlToValidate="txtTitle"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtTitle" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litShortDesc" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValShortDesc" runat="server" ControlToValidate="txtShortDesc"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtShortDesc" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litLongDesc" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValLongDesc" runat="server" ControlToValidate="txtLongDesc"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtLongDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                         <p class="field">
                                            <asp:Literal ID="litViewCount" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValViewCount" runat="server" ControlToValidate="txtViewCount"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtViewCount" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litDisplayImage" runat="server"></asp:Literal>
                                            <br />
                                            <asp:HyperLink ID="hplDisplayImg" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgDisplay" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulDisplayImg" runat="server" />
                                        </p>

                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveArticle" runat="server" class="wpcf7-submit" OnClick="btnAddSaveArticle_Click" />
                                            <asp:Button ID="btnCancelArticle" runat="server" CssClass="wpcf7-submit"
                                                Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return cancelAddSaveArticle();"
                                                UseSubmitBehavior="false" ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div2" class="widget">
                            <h2>
                                <asp:Literal ID="litAddArticleHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div3" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddArticleHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
