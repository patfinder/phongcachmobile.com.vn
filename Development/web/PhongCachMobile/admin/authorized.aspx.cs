﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile.admin
{
    public partial class authorized : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
            }
        }

        private void loadLang()
        {
            this.litAuthorized.Text = this.Title = LangController.getLng("litAuthorized.Text", "Hệ thống phân quyền");
            if (Session["Email"] != null && Session["Email"].ToString() != "")
                this.litAuthorizedText.Text = "Quản trị viên <font style='font-weight:bold;color:red;'>" + Session["Email"].ToString() + 
                    "</font> bắt đầu phiên làm việc vào lúc <font style='font-weight:bold;'>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</font>.";

        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
               
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }
    }
}