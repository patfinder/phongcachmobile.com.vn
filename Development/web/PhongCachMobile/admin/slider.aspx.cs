﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.model;
using System.IO;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class slider : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litSliderManager.Text", "Quản lý slider");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadGroup();
                loadAllSlider(ddlGroup.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadGroup()
        {
            ddlGroup.Items.Clear();
            DataTable dt = GroupController.getGroupsByFilter("SLIDER_TYPE");
            if (dt != null && dt.Rows.Count >0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    // V2 - skip old banner
                    if ((int)dr["ID"] < 55)
                        continue;

                    ddlGroup.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }
                
            }

          
            ddlGroup.DataBind();
            
        }


        private void loadAllSlider(string groupId)
        {
            DataTable dt = SliderController.getSliders(groupId);
            if (dt != null && dt.Rows.Count > 0)
            {
                dgrSlider.DataSource = dt;
                string imagePath = "images/slide/";
               
                foreach (DataRow dr in dt.Rows)
                {
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"];
                    dr["ThumbImage"] = "../" + imagePath + dr["ThumbImage"];
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                }
                dgrSlider.DataBind();
            }
        }

        private void loadLang()
        {
            litSlider.Text = LangController.getLng("litSlider.Text", "Slider");
            btnAddSlider.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeleteSlider.Text = LangController.getLng("litDelete.Text", "Xóa");
            litAddSlider.Text = LangController.getLng("litAddSlider.Text", "Thêm / sửa slider");
            litText.Text = LangController.getLng("litText.Text", "Text");
            litActionUrl.Text = LangController.getLng("litActionUrl.Text", "Liên kết");
            litDisplayImage.Text = LangController.getLng("litDisplayImage.Text", "Ảnh hiển thị");
            litGroupChoosing.Text = LangController.getLng("litGroup.Text", "Nhóm");
            litThumbImage.Text = LangController.getLng("litThumbImage", "Ảnh thumb");
            btnAddSaveSlider.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelSlider.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddSliderHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litAddSliderHelpDesc.Text = LangMsgController.getLngMsg("litAddSliderHelp.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa slider");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrSlider.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrSlider.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrSlider.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = SliderController.deleteSlider(gvIDs);

                if (result)
                {
                    loadAllSlider(ddlGroup.SelectedValue);
                    try
                    {
                        string imagePath = "";
                        if (ddlGroup.SelectedValue == "1")
                            imagePath = SysIniController.getSetting("Slider_ImagePath", "images/slider/");
                        else
                            if (ddlGroup.SelectedValue == "13")
                                imagePath = SysIniController.getSetting("VidSlider_ImagePath", "images/vidslider/");

                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');
                        foreach (FileInfo file in files)
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    string message = ex.Message;
                                }
                            }
                        }

                        lblFrmResponseSlider.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseSlider.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseSlider.Visible = true;
                    }
                    catch
                    {
                        lblFrmResponseSlider.ForeColor = System.Drawing.Color.Red;
                        lblFrmResponseSlider.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                        frmResponseSlider.Visible = true;
                    }
                }
                else
                {
                    lblFrmResponseSlider.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseSlider.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseSlider.Visible = true;
                }
            }
            resetAddSlider();
        }

        private void resetAddSlider()
        {
            txtActionUrl.Text = txtText.Text = imgDisplay.ImageUrl = imgThumb.ImageUrl = hplDisplayImage.NavigateUrl = hplThumbImage.NavigateUrl = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddSlider.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveSlider_Click(object sender, EventArgs e)
        {
            bool rs;
            string imagePath = "images/slide/";

            if (this.hdfId.Value != "")
            {
                rs = SliderController.updateSlider(new Slider(int.Parse(hdfId.Value), "", "", txtText.Text, txtActionUrl.Text, DateTime.Now.ToString("yyyyMMddHHmmss"),
                    true, int.Parse(ddlGroup.SelectedValue),txtTitle.Text));
            }
            else
            {
                rs = SliderController.createSlider(new Slider("", "", txtText.Text, txtActionUrl.Text, DateTime.Now.ToString("yyyyMMddHHmmss"),
                    true, int.Parse(ddlGroup.SelectedValue), txtTitle.Text));
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfId.Value == "" ? SliderController.getCurrentId() : hdfId.Value;

                try
                {
                    if (fulDisplayImage.PostedFile.FileName != "")
                    {
                        fulDisplayImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + imagePath) + id + "_" + fulDisplayImage.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update SLIDER set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                }

                try
                {
                    if (fulThumbImage.PostedFile.FileName != "")
                    {
                        fulThumbImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + imagePath) + id + "_t_" + fulThumbImage.PostedFile.FileName);
                        object[,] parameters = { { "@ThumbImage", id + "_t_" + fulThumbImage.PostedFile.FileName },
                                                 {"@Id", id}};
                        DB.exec("update SLIDER set ThumbImage = @ThumbImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                }

                loadAllSlider(ddlGroup.SelectedValue);
                lblFrmResponseSlider.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseSlider.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseSlider.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseSlider.Visible = true;
            }
            else
            {
                lblFrmResponseSlider.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseSlider.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseSlider.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseSlider.Visible = true;
            }
            resetAddSlider();
        }


        protected void dgrSlider_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllSlider(ddlGroup.SelectedValue);
            DataTable dt = (DataTable)dgrSlider.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtActionUrl.Text = dr["ActionUrl"].ToString();
            txtText.Text = dr["Text"].ToString();
            txtTitle.Text = dr["Title"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            imgDisplay.ImageUrl = this.hplDisplayImage.NavigateUrl = dr["DisplayImage"].ToString();
            imgThumb.ImageUrl = this.hplThumbImage.NavigateUrl = dr["ThumbImage"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddSlider.Attributes["style"] = "display:block";
            frmResponseSlider.Visible = false;
        }

        protected void dgrSlider_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrSlider.CurrentPageIndex = e.NewPageIndex;
            loadAllSlider(ddlGroup.SelectedValue);
            frmResponseSlider.Visible = false;
            resetAddSlider();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrSlider.CurrentPageIndex = 0;
            loadAllSlider(ddlGroup.SelectedValue);
            frmResponseSlider.Visible = false;
            resetAddSlider();
        }
    }
}