﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="nguoi-dung.aspx.cs" Inherits="PhongCachMobile.admin.nguoi_dung" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <link href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        jQuery(document).ready(function () {
            $('#content_txtDOB').datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1900:2020",
                dateFormat: "dd/mm/yy",
                monthNamesShort: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                dayNamesMin: ["Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy", "CN"]
            });
        });

        function cancelAddSaveUser() {
            jQuery('#content_divAddUser').hide();
            jQuery('#content_divAddDeleteUser').show();
        }

        function check_uncheck(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAll') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRec') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRec') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrUser_CheckAll').checked = false;
                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsg(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn vô hiệu?')
                }
            }
        }
    </script>
    <style>
           name
{
    padding: 10px 0px 5px 0px;
    height: 20px;
    position: relative;
    width: 100px;
    display: inline-block;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litUser" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseUser" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseUser" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmServer">
                                 <p class="field">
                                        <asp:Literal ID="litText" runat="server"> </asp:Literal> <small> * </small>: &nbsp; 
                                        <asp:TextBox ID="txtText" runat="server" CssClass="wpcf7-text" Width="150px" MaxLength="30"></asp:TextBox>
                                        <asp:Literal ID="litOn" runat="server"> </asp:Literal>: &nbsp; 
                                     <asp:DropDownList ID="ddlOn" runat="server">
                                     </asp:DropDownList>
                                        &nbsp; &nbsp; 
                                        <asp:LinkButton ID="lbtFind" Font-Bold="true" runat="server" onclick="lbtFind_Click"></asp:LinkButton>
                                        </p>
                                    <asp:DataGrid ID="dgrUser" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnPageIndexChanged="dgrUser_PageIndexChanged"
                                        OnEditCommand="dgrUser_EditCommand"    PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="ID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ALIAS" HeaderText="ALIAS" Visible="False">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="FirstName" HeaderText="FNAME" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="LastName" HeaderText="LNAME" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="Tên">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ADDRESS" HeaderText="Địa chỉ" Visible="True">
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="MOBILE" HeaderText="Mobile">
                                                <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="TEL" HeaderText="Tel" Visible="True"></asp:BoundColumn>
                                              <asp:BoundColumn DataField="EMAIL" HeaderText="Email" Visible="True"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="FLAG" HeaderText="FLAG" Visible="False"></asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Cancel" EditText="Edit" UpdateText="Update"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnDisable" runat="server" CssClass="wpcf7-submit" CausesValidation="False" OnClientClick="return confirmMsg(this.form)"
                                            ClientIDMode="Static" OnClick="btnDisable_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddUser" style="display:none;"  runat="server">
                <div class="wrapper">
                    <div>
                        <div>
                            <h2>
                                <asp:Literal ID="litAddUserText_User" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseAddUser" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseAddUser" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="frmInput" runat="server">

                                        <p class="title"><font style='font-weight:bold; text-transform:uppercase'><asp:Literal ID="litPersonalInfo" runat="server"></asp:Literal></font></p>
                                         <name style="float:left;"><asp:Literal ID="litEnable"  runat="server"></asp:Literal></name>
                                            <table style="width:150px;">
                                            <tr>
                                                <td>    <asp:RadioButton Text="Có" ID="rbtEnableYes" runat="server" Checked="true" GroupName="enable" /></td>
                                                <td><asp:RadioButton Text="Không" ID="rbtEnableNo" GroupName="enable"
                                                 runat="server" /></td>
                                            </tr>
                                            </table>
                                             <name><asp:Literal ID="litRole"  runat="server"></asp:Literal></name>
                             <asp:DropDownList ID="ddlRole" runat="server"></asp:DropDownList><br />

                            <name><asp:Literal ID="litAlias" runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtAlias" MaxLength="30" Width="250px" runat="server"  ></asp:TextBox> <br />
                            <name><asp:Literal ID="litFirstName"  runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtFirstName" MaxLength="10" Width="100px" runat="server"></asp:TextBox> &nbsp; &nbsp; &nbsp; 
                             <name style="width:30px;"><asp:Literal ID="litLastName"  runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtLastName" MaxLength="50" Width="250px" runat="server"></asp:TextBox> <br />
                            <name><asp:Literal ID="litDOB"  runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtDOB" Width="120px" runat="server"></asp:TextBox> <br />
                            <name style="float:left;"><asp:Literal ID="litGender"  runat="server"></asp:Literal></name>
                             <table style="width:150px;">
                                            <tr>
                                                <td>    <asp:RadioButton Text="Nam" ID="rbtMale" runat="server" Checked="true" GroupName="sex" /></td>
                                                <td><asp:RadioButton Text="Nữ" ID="rbtFemale" GroupName="sex"
                                                 runat="server" /></td>
                                            </tr>
                                            </table>
                                <br />

                            <p class="title"><font style='font-weight:bold;text-transform:uppercase'><asp:Literal ID="litContactInfo" runat="server"></asp:Literal></font></p>
                            <name ><asp:Literal ID="litAddress" runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtAddress" MaxLength="200" Width="350px" runat="server"  ></asp:TextBox> <br />
                            <name><asp:Literal ID="litState"  runat="server"></asp:Literal></name>
                             <asp:DropDownList ID="ddlState" runat="server"></asp:DropDownList><br />
                            <name><asp:Literal ID="litEmail"  runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtEmail" Enabled="false" Width="250px" runat="server"></asp:TextBox> <br /> 
                             <name><asp:Literal ID="litMobile"  runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtMobile" MaxLength="20" Width="120px" runat="server"></asp:TextBox> <br />
                            <name><asp:Literal ID="litTel"  runat="server"></asp:Literal></name>
                            <asp:TextBox ID="txtTel" MaxLength="20" Width="120px" runat="server"></asp:TextBox> <br />
                      
                      <br /><br/>

                       <p class="title"><font style='font-weight:bold;text-transform:uppercase'><asp:Literal ID="litCertifiedInfo" runat="server"></asp:Literal></font></p>
                            <name>Mã tài khoản</name>
                            <asp:TextBox ID="txtId" Enabled="false" Width="50px" runat="server"  ></asp:TextBox> &nbsp; <name style='width:auto;'><i><font style='font-weight:bold'><asp:Literal ID="litIdDesc" runat="server"></asp:Literal></font></i></name><br />
                            <br /><br />
                             <p class="submit-wrap">
                                    <asp:Button ID="btnAddUserSave" runat="server" class="wpcf7-submit" OnClick="btnAddUserSave_Click" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                        CausesValidation="False" OnClientClick="return cancelAddSaveUser();" UseSubmitBehavior="false"
                                        ClientIDMode="Static" />
                                        </p>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
