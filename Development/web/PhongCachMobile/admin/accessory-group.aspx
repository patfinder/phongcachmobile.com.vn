﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="accessory-group.aspx.cs" Inherits="PhongCachMobile.admin.accessory_group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="container_12 primary_content_wrap clearfix">
        <form id="form1" action="/admin/accessory-group" method="post" class="wpcf7-form" enctype="multipart/form-data" >
            <div id="content" class="grid_12">

                <% if(ShowResult) { %>
                <div><h3 style="color: red;"><%= ResultMessage %></h3></div>
                <% } else { %>

                <h2>Định nghĩa các nhóm phụ kiện</h2>

                <!-- --------------------- List of Sections --------------------- -->

                <div class="section-list">

                    <%
                        foreach (PhongCachMobile.admin.SectionInfo section in SectionList) {
                            %>

                    <hr />

                    <!-- --------------------- Section Info --------------------- -->
                    <div id="access-div-<%= section.ID %>" <%= section.ID == 0 ? "style='display: none;'" : "" %> >
                        

                          <table class="table">
                            <tbody>
                              <tr>
                                <td><b>Tên nhóm phụ kiện (*):</b></td>
                                <%--<input type="button" class="link-btn section-delete-button" data-id="<%= section.ID %>" /><br />--%>
                                <td><input type="text" name="title-<%= section.ID %>" value="<%= section.Title %>" /></td>
                              </tr>
                              <tr>
                                <td><b>Hình ảnh (*):</b></td>
                                <td>
                                    <img src="/images/product/<%= section.Image %>" alt="section image" />
                                    <input type="file" name="image-<%= section.ID %>" />
                                </td>
                              </tr>
                              <tr>
                                <td><b>Link hình ảnh:</b></td>
                                <td><input type="text" name="link-<%= section.ID %>" value="<%= section.Link %>" /></td>
                              </tr>
                              <tr>
                                <td><b>Số sản phẩm hiển thị (*):</b></td>
                                <td><input type="text" name="showitemcount-<%= section.ID %>" value="<%= section.ShowItemCount %>" /></td>
                              </tr>
                              <tr>
                                <td><b>Các loại phụ kiện trong nhóm:</b></td>
                                <td>
                                    <select name="select-<%= section.ID %>" class="access-select" multiple="multiple" style="height: 100%" >
                                        <% foreach (PhongCachMobile.admin.CategoryInfo cat in AccessCategories) { %>
                                        <%--id="category-<%= cat.ID %>" --%>
                                        <option class="access-option access-option-<%= cat.ID %>" value="<%= cat.ID %>" data-id="<%= cat.ID %>" 
                                            <%= section.Categories.Any(c => c.ID == cat.ID) ? "selected='selected'" : "" %> >
                                            <%= cat.Name %></option>
                                        <% } %>
                                    </select>
                                </td>
                              </tr>

                            <%--Show delete button--%>
                            <% if(section.ID > 0) { %>
                              <tr>
                                <td>
                                    <a href="/admin/accessory-group/?command=delete&id=<%= section.ID %>" >Xóa nhóm phụ kiện</a>
                                </td>
                              </tr>
                            <% } %>

                            </tbody>
                          </table>
                    </div>
                    <% } %>
                </div>

                <!-- --------------------- Common Buttons --------------------- -->
                <% for (int i = 0; i < SectionList.Count; i++ ) { %>
                <input type="hidden" name="SectionID-<%= i %>" value="<%= SectionList[i].ID %>" />
                <% } %>
                <input type="submit" name="submit" value="Submit" class="wpcf7-submit" />
                <input type="submit" name="submit" value="Cancel" class="wpcf7-submit" />
                <input type="button" id="Add" name="Add" value="Add" class="wpcf7-submit" />

            </div>
        </form>
    </div>


    <script type="text/javascript">
        jQuery(document).ready(function () {
            $ = jQuery;

            // Uncheck a category from other section when being selected from one section
            $(".access-select").on('change', function (event) {

                var parentID = $(this).attr("id");
                var options = $(this).children("option:selected");
                options.each(function () {

                    //var parentID = $(this).parent().attr("id");
                    var thisID = $(this).attr("data-id");
                    var thisOfEvent = this;

                    // Unselect other "access-option-ID"
                    $(".access-option-" + thisID).each(function () {
                        // Check if of the same section
                        if (this === thisOfEvent)
                            return;

                        $(this).attr('selected', false);
                    })
                });
            });

            $("input#Add").on('click', function () {
                $("#access-div-0").css("display", "block");
                $(this).hide();
            });
        });
    </script>

    <% } %>

</asp:Content>
