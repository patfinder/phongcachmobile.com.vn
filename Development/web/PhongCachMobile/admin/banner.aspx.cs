﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.IO;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class banner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litBannerManager.Text", "Quản lý Banner");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadGroup();
                loadAllBanner(ddlGroup.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadGroup()
        {
            ddlGroup.Items.Clear();
            DataTable dt = GroupController.getGroupsByFilter("BANNER_TYPE");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    // V2 - skip old banner
                    if ((int)dr["ID"] < 55)
                        continue;

                    ddlGroup.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }

            }
            ddlGroup.DataBind();

        }


        private void loadAllBanner(string groupId)
        {
            DataTable dt = SliderController.getSliders(groupId);
            if (dt != null && dt.Rows.Count > 0)
            {
                dgrBanner.DataSource = dt;
                string imagePath = "images/banner/";

                foreach (DataRow dr in dt.Rows)
                {
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"];
                    dr["ThumbImage"] = "../" + imagePath + dr["ThumbImage"];
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                }
                dgrBanner.DataBind();
            }
        }

        private void loadLang()
        {
            litBanner.Text = LangController.getLng("litBanner.Text", "Banner");
            btnAddBanner.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeleteBanner.Text = LangController.getLng("litDelete.Text", "Xóa");
            litAddBanner.Text = LangController.getLng("litAddSlider.Text", "Thêm / sửa slider");
            litText.Text = LangController.getLng("litText.Text", "Text");
            litActionUrl.Text = LangController.getLng("litActionUrl.Text", "Liên kết");
            litDisplayImage.Text = LangController.getLng("litDisplayImage.Text", "Ảnh hiển thị");
            litGroupChoosing.Text = LangController.getLng("litGroup.Text", "Nhóm");
            litThumbImage.Text = LangController.getLng("litThumbImage", "Ảnh thumb");
            btnAddSaveBanner.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelBanner.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddBannerHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litAddBannerHelpDesc.Text = LangMsgController.getLngMsg("litAddBannerHelp.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa slider");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrBanner.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrBanner.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrBanner.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = SliderController.deleteSlider(gvIDs);

                if (result)
                {
                    loadAllBanner(ddlGroup.SelectedValue);
                    try
                    {
                        string imagePath = "images/banner/";
                        
                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');
                        foreach (FileInfo file in files)
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    string message = ex.Message;
                                }
                            }
                        }

                        lblFrmResponseBanner.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseBanner.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseBanner.Visible = true;
                    }
                    catch
                    {
                        lblFrmResponseBanner.ForeColor = System.Drawing.Color.Red;
                        lblFrmResponseBanner.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                        frmResponseBanner.Visible = true;
                    }
                }
                else
                {
                    lblFrmResponseBanner.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseBanner.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseBanner.Visible = true;
                }
            }
            resetAddBanner();
        }

        private void resetAddBanner()
        {
            txtActionUrl.Text = txtText.Text = imgDisplay.ImageUrl = imgThumb.ImageUrl = hplDisplayImage.NavigateUrl = hplThumbImage.NavigateUrl = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddBanner.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveBanner_Click(object sender, EventArgs e)
        {
            bool rs;
            string imagePath = "images/banner/";

            if (this.hdfId.Value != "")
            {
                rs = SliderController.updateSlider(new Slider(int.Parse(hdfId.Value), "", "", txtText.Text, txtActionUrl.Text, DateTime.Now.ToString("yyyyMMddHHmmss"),
                    true, int.Parse(ddlGroup.SelectedValue), txtTitle.Text));
            }
            else
            {
                rs = SliderController.createSlider(new Slider("", "", txtText.Text, txtActionUrl.Text, DateTime.Now.ToString("yyyyMMddHHmmss"),
                    true, int.Parse(ddlGroup.SelectedValue), txtTitle.Text));
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfId.Value == "" ? SliderController.getCurrentId() : hdfId.Value;

                try
                {
                    if (fulDisplayImage.PostedFile.FileName != "")
                    {
                        fulDisplayImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + imagePath) + id + "_" + fulDisplayImage.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update SLIDER set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                }

                try
                {
                    if (fulThumbImage.PostedFile.FileName != "")
                    {
                        fulThumbImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + imagePath) + id + "_t_" + fulThumbImage.PostedFile.FileName);
                        object[,] parameters = { { "@ThumbImage", id + "_t_" + fulThumbImage.PostedFile.FileName },
                                                 {"@Id", id}};
                        DB.exec("update SLIDER set ThumbImage = @ThumbImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                }

                loadAllBanner(ddlGroup.SelectedValue);
                lblFrmResponseBanner.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseBanner.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseBanner.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseBanner.Visible = true;
            }
            else
            {
                lblFrmResponseBanner.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseBanner.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseBanner.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseBanner.Visible = true;
            }
            resetAddBanner();
        }


        protected void dgrBanner_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllBanner(ddlGroup.SelectedValue);
            DataTable dt = (DataTable)dgrBanner.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtActionUrl.Text = dr["ActionUrl"].ToString();
            txtText.Text = dr["Text"].ToString();
            txtTitle.Text = dr["Title"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            imgDisplay.ImageUrl = this.hplDisplayImage.NavigateUrl = dr["DisplayImage"].ToString();
            imgThumb.ImageUrl = this.hplThumbImage.NavigateUrl = dr["ThumbImage"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddBanner.Attributes["style"] = "display:block";
            frmResponseBanner.Visible = false;
        }

        protected void dgrBanner_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrBanner.CurrentPageIndex = e.NewPageIndex;
            loadAllBanner(ddlGroup.SelectedValue);
            frmResponseBanner.Visible = false;
            resetAddBanner();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrBanner.CurrentPageIndex = 0;
            loadAllBanner(ddlGroup.SelectedValue);
            frmResponseBanner.Visible = false;
            resetAddBanner();
        }
    }
}