﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using System.IO;
using PhongCachMobile.model;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class may_cu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litOldPrdManager.Text", "Quản lý kho máy cũ");
            if (!this.IsPostBack)
            {
                loadLang();
                loadAllPrd();
            }
        }

        private void loadAllPrd()
        {
            DataTable dt = OldProductController.getOldProducts();
            if (dt != null && dt.Rows.Count > 0)
            {
                string imagePath = "images/oldproduct/";
                foreach (DataRow dr in dt.Rows)
                {
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"].ToString();

                }
            }
            dgrPrd.DataSource = dt;
            dgrPrd.DataBind();
        }

        private void loadLang()
        {
            litPrd.Text = LangController.getLng("litOldPrd.Text", "Máy cũ");
            btnAddPrd.Text =  LangController.getLng("litAdd.Text", "Thêm");
            btnDeletePrd.Text =  LangController.getLng("litDelete.Text", "Xóa");
            btnAddSavePrd.Text =  LangController.getLng("litSave.Text", "Lưu");
            btnCancelPrd.Text =  LangController.getLng("litCancel.Text", "Hủy");
        }

        protected void btnDeletePrd_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrPrd.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrPrd.Items[i].FindControl("deleteRecPrd");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrPrd.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = OldProductController.deleteOldProduct(gvIDs);

                if (result)
                {
                    loadAllPrd();
                    //Delete images
                    try
                    {
                        string imagePath = "images/oldproduct/";

                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');

                        //loi chua delete hinh
                        foreach (FileInfo file in files)
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    string message = ex.Message;
                                }
                            }
                        }

                        lblFrmResponsePrd.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponsePrd.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponsePrd.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponsePrd.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponsePrd.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponsePrd.Visible = true;
                }
            }
            resetAddPrd();
        }

        private void resetAddPrd()
        {
            txtName.Text = txtLongDesc.Text = txtCurrentPrice.Text = txtOriginalPrice.Text = "";
            hdfId.Value = "";

            hplDisplayImage.NavigateUrl = "";
            imgDisplay.ImageUrl = "";

            divAddDeletePrd.Attributes["style"] = "display:block";
            divAddPrd.Attributes["style"] = "display:none";

        }

        protected void btnAddSavePrd_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = "images/oldproduct/";
            if (this.hdfId.Value != "")
            {
                if (fulDisplayImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgDisplay.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                rs = OldProductController.updateOldProduct(new OldProduct
                {
                    id = int.Parse(hdfId.Value),
                    name = txtName.Text,
                    description = txtLongDesc.Text,
                    currentPrice = int.Parse(txtCurrentPrice.Text),
                    originalPrice = int.Parse(txtOriginalPrice.Text)
                });

            }
            else
            {
                rs = OldProductController.createOldProduct(new OldProduct
                {
                    name = txtName.Text,
                    description = txtLongDesc.Text,
                    currentPrice = int.Parse(txtCurrentPrice.Text),
                    originalPrice = int.Parse(txtOriginalPrice.Text)
                });
            }
            if (rs)
            {
                string id = this.hdfId.Value == "" ? OldProductController.getCurrentId() : hdfId.Value;

                // upload Image
                try
                {
                    if (fulDisplayImage.PostedFile.FileName != "")
                    {
                        fulDisplayImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulDisplayImage.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update OLDPRODUCT set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }

                }
                catch (Exception ex)
                {
                }

                loadAllPrd();
                lblFrmResponsePrd.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponsePrd.Visible = true;
            }
            else
            {
                lblFrmResponsePrd.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponsePrd.Visible = true;
            }
            resetAddPrd();
        }


        protected void dgrPrd_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrPrd.CurrentPageIndex = e.NewPageIndex;
            loadAllPrd();
            frmResponsePrd.Visible = false;
            resetAddPrd();
        }

        protected void dgrPrd_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllPrd();
            DataTable dt = (DataTable)dgrPrd.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtName.Text = dr["Name"].ToString();
            txtLongDesc.Text = dr["Description"].ToString();
            txtCurrentPrice.Text = dr["CurrentPrice"].ToString();
            txtOriginalPrice.Text = dr["OriginalPrice"].ToString();
            this.imgDisplay.ImageUrl = this.hplDisplayImage.NavigateUrl = dr["DisplayImage"].ToString();

            this.hdfId.Value = dr["Id"].ToString();
            divAddDeletePrd.Attributes["style"] = "display:none";
            divAddPrd.Attributes["style"] = "display:block";

            frmResponsePrd.Visible = false;
        }

    }
}