﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="cau-hinh.aspx.cs" Inherits="PhongCachMobile.admin.cau_hinh" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <link href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#content_txtCDDateTime").datetimepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <%--Begin Global Section--%>
            <%--<div class="box">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litDisplay" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseGlobal" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseGlobal" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="frmGlobal" runat="server">
                                        <p class="field">
                                            <asp:Literal ID="litMaxLengthext_Config" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValMaxLength_Config" runat="server" ControlToValidate="txtMaxLength"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtMaxLength" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litPageLengthText_Config" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValPageLength_Config" runat="server" ControlToValidate="txtPageLength"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtPageLength" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litStartPageNrText_Config" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValStartPageNr_Config" runat="server" ControlToValidate="txtStartPageNr"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtStartPageNr" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnGlobalSave_Config" runat="server" class="wpcf7-submit" OnClick="btnGlobalSave_Config_Click" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="text-4" class="widget">
                            <h2>
                                <asp:Literal ID="litGlobalHelpText_Config" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="text-6" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litGlobalHelp_Config" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <%-- End Global Section--%>
            <%-- <br />--%>

            <%--Begin Under Construction Section--%>
            <div class="box">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litUpgrade" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseUpgrade" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseUpgrade" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="frmUpgrade" runat="server">
                                        <p class="field">
                                            <asp:Literal ID="litUpgradeActive" runat="server"></asp:Literal><small> *
                                            </small>
                                            <br />
                                                <asp:RadioButtonList ID="rblUpgardeActive" runat="server" 
                                                RepeatDirection="Horizontal" Width="100px">
                                                    <asp:ListItem Value="true">Mở</asp:ListItem>
                                                    <asp:ListItem Value="false">Tắt</asp:ListItem>
                                                </asp:RadioButtonList>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litUpgradeCountDown" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValCDDateTime" runat="server" ControlToValidate="txtCDDateTime"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                  <asp:RegularExpressionValidator Display="Dynamic" ID="rgeValCDDateTime"
                                        ControlToValidate="txtCDDateTime"  ForeColor="Red" runat="server" 
                                                ValidationExpression="^([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?$"></asp:RegularExpressionValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCDDateTime" runat="server" CssClass="wpcf7-text" Width="200px"
                                                    MaxLength="30"></asp:TextBox> </span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnUpgradeSave" runat="server" class="wpcf7-submit" OnClick="btnUpgradeSave_Click" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div9" class="widget">
                            <h2>
                                <asp:Literal ID="litUpgradeHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div10" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litUpgradeHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- End Under Construction Section--%>
            <br />
            <%--Begin Email Section--%>
            <div class="box">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litEmail" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseEmail" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseEmail" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="Div2" runat="server">
                                        <p class="field">
                                            <asp:Literal ID="litAdminEmail" runat="server"></asp:Literal><small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValAdminEmail" runat="server" ControlToValidate="txtAdminEmail"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="rgeValAdminEmail" runat="server" 
                                                ControlToValidate="txtAdminEmail" Display="Dynamic" ForeColor="Red" 
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtAdminEmail" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="50"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litForwardEmail" runat="server"></asp:Literal><small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValForwardEmail" runat="server" ControlToValidate="txtForwardEmail"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="rgeValForwardEmail" runat="server" 
                                                ControlToValidate="txtForwardEmail" Display="Dynamic" ForeColor="Red" 
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtForwardEmail" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="50"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnEmailSave" runat="server" class="wpcf7-submit" OnClick="btnEmailSave_Click" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div3" class="widget">
                            <h2>
                                <asp:Literal ID="litEmailHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div4" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litEmailHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- End Email Section--%>
            <br />
            
            <%--Begin Display Section--%>
            <div class="box">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litDisplay" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseDisplay" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseDisplay" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="Div5" runat="server">
                                        <p class="field">
                                            Số sản phẩm Giá hot mỗi trang<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValRandomSpecialNumber" runat="server" ControlToValidate="txtGiaHotPageSize"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtGiaHotPageSize" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Số sản phẩm load thêm mỗi trang<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValRandomFeatureNumber" runat="server" ControlToValidate="txtDTDDPageSize"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDTDDPageSize" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <%--<p class="field">
                                           Số lượng sản phẩm mới<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValNewProductNumber" runat="server" ControlToValidate="txtNewProductNumber"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtNewProductNumber" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>--%>


                                        <p class="field">
                                            Số tin quảng cáo<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValArticleHomeNumber" runat="server" ControlToValidate="txtHotPriceHotNewsCount"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtHotPriceHotNewsCount" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Số tin thị trường<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValNewAccessoriesNumber" runat="server" ControlToValidate="txtHotPriceMarketNewsCount"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtHotPriceMarketNewsCount" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Mức chênh lệch của SP cùng tầm giá<small> * </small>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSamePriceRange"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtSamePriceRange" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <%--<p class="field">
                                           Số lượng sản phẩm khác<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValOtherProductNumber" runat="server" ControlToValidate="txtOtherProductNumber"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtOtherProductNumber" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>--%>
                                       
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnDisplaySave" runat="server" class="wpcf7-submit" OnClick="btnDisplaySave_Click" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div6" class="widget">
                            <h2>
                                <asp:Literal ID="litDisplayHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div7" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litDisplayHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- End Email Section--%>
            <br />

            <%--Begin Statistic Section--%>
            <div class="box">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litStatistic" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseStatistic" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseStatistic" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="Div8" runat="server">
                                        <p class="field">
                                            <asp:Literal ID="litExtraNumOfUser" runat="server"></asp:Literal><small> * </small>
                                            <asp:RequiredFieldValidator ID="reqExtraNumOfUser" runat="server" ControlToValidate="txtExtraNumOfUser"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtExtraNumOfUser" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            <asp:Literal ID="litExtraNumOfVisitor" runat="server"></asp:Literal><small> * </small>
                                            <asp:RequiredFieldValidator ID="reqExtraNumOfVisitor" runat="server" ControlToValidate="txtExtraNumOfVisitor"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtExtraNumOfVisitor" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnStatistic" runat="server" class="wpcf7-submit" OnClick="btnStatisticSave_Click" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div11" class="widget">
                            <h2>
                                <asp:Literal ID="litStatisticHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div12" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litStatisticHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%-- End Statistic Section--%>

        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
