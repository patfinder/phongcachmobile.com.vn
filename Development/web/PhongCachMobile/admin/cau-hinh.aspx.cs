﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class cau_hinh : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litConfigurationManager.Text", "Quản lý cấu hình");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLanguages();
                loadSetting();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }
        private void loadSetting()
        {
            txtGiaHotPageSize.Text = SysIniController.getSetting("Gia_Hot_So_SP_Filter_Page", "16");
            txtDTDDPageSize.Text = SysIniController.getSetting("DTDD_So_SP_Filter_Page", "20");
            txtHotPriceHotNewsCount.Text = SysIniController.getSetting("Gia_Hot_So_Tin_Hot", "10");
            txtHotPriceMarketNewsCount.Text = SysIniController.getSetting("Gia_Hot_So_Tin_Thi_Truong", "10");
            txtSamePriceRange.Text = SysIniController.getSetting("Number_Interval_Price", "1000000");

            //txtNewProductNumber.Text = SysIniController.getSetting("Number_New_Product", "6");
            //txtOtherProductNumber.Text = SysIniController.getSetting("Number_Other_Product", "3");

            txtAdminEmail.Text = SysIniController.getSetting("Global_UserEmail", "bdo@netsservices.dk");
            txtForwardEmail.Text = SysIniController.getSetting("Global_FromEmail", "game.relax24h@gmail.com");

            if (SysIniController.getSetting("Upgrade_IsActive", "false") == "true")
            {
                rblUpgardeActive.Items[0].Selected = true;
                rblUpgardeActive.Items[1].Selected = false;
            }
            else
            {
                rblUpgardeActive.Items[0].Selected = false;
                rblUpgardeActive.Items[1].Selected = true;
            }
            txtCDDateTime.Text = Common.strToFormatDateTime(
                SysIniController.getSetting("Upgrade_DateTime", "20200622021500"), "yyyyMMddHHmmss").ToString("dd/MM/yyyy HH:mm").Replace('-', '/');

            txtExtraNumOfUser.Text = SysIniController.getSetting("OnlineUsers", "200");
            txtExtraNumOfVisitor.Text = SysIniController.getSetting("TotalVisit", "1275980");
        }

        private void loadLanguages()
        {
            this.litDisplay.Text = LangController.getLng("litDisplay.Text", "Hiển thị");
            this.litDisplayHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litDisplayHelpDesc.Text = LangMsgController.getLngMsg("litDisplayHelp.Desc", "Giải thích về những thiết lập có liên quan đến thông tin hiển thị");

            this.litStatisticHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litStatisticHelpDesc.Text = LangMsgController.getLngMsg("litStatisticHelp.Desc", "Giải thích về những thiết lập có liên quan đến thông tin thống kê");

            //this.reqValNewProductNumber.ErrorMessage = this.reqValOtherProductNumber.ErrorMessage = 
            this.reqValRandomSpecialNumber.ErrorMessage = this.reqValRandomFeatureNumber.ErrorMessage = this.reqValNewAccessoriesNumber.ErrorMessage =
                this.reqValCDDateTime.ErrorMessage =
                LangController.getLng("litRequired.Text", "(*)");

            this.btnUpgradeSave.Text = this.btnEmailSave.Text = this.btnDisplaySave.Text = this.btnStatistic.Text =  LangController.getLng("litSave.Text", "Lưu");

            this.litEmail.Text = LangController.getLng("litEmail.Text", "Email");
            this.litAdminEmail.Text = LangController.getLng("litAdminEmail.Text", "Email quản trị viên");
            this.litForwardEmail.Text = LangController.getLng("litForwardEmail.Text", "Email chuyển tiếp");

            this.litEmailHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litEmailHelpDesc.Text = LangMsgController.getLngMsg("litEmailHelp.Desc", "Thông tin giải thích về email của quản trị viên");

            this.rgeValAdminEmail.ErrorMessage = this.rgeValForwardEmail.ErrorMessage = LangController.getLng("litInvalidFormat.Text", "Không đúng định dạng");

            this.litUpgradeActive.Text = LangController.getLng("litActive.Text", "Kích hoạt");
            this.litUpgradeHelpDesc.Text = LangMsgController.getLngMsg("litUpgradeHelp.Desc", "Những thông tin giải thích về tính năng bảo trì, nâng cấp");
            this.litUpgradeHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");

            this.litUpgrade.Text = LangController.getLng("litUpgrade.Text", "Bảo trì - nâng cấp");
            this.rgeValCDDateTime.ErrorMessage = LangController.getLng("litInvalidFormat.Text", "Không đúng định dạng");
            this.litUpgradeCountDown.Text = LangController.getLng("litUpgradeCountDown.Text", "Thời gian hoạt động lại");

            this.litStatistic.Text = LangController.getLng("litStatistic.Text", "Thống kê");
            this.litExtraNumOfUser.Text = LangController.getLng("litExtraNumOfUser.Text", "Số thành viên được thêm");
            this.litExtraNumOfVisitor.Text = LangController.getLng("litExtraNumOfVisitor.Text", "Lượt truy cập được thêm");
        }

        private void closeFrm()
        {
            frmResponseDisplay.Visible = false;
            frmResponseEmail.Visible = false;
            frmResponseUpgrade.Visible = false;
        }

        protected void btnGlobalSave_Config_Click(object sender, EventArgs e)
        {
            //int MaxLen = int.Parse(ConfigurationManager.AppSettings["MaxLengthSetting"]);
            //StringBuilder OData = new StringBuilder(MaxLen);

            //int iResult = 0;

            //iResult = ApolloUtils.apolloUpdate(ConfigurationManager.AppSettings["FirmId"], false, Application["Delimiter"].ToString(),
            //    "NAME", "VALUE", txtMaxLength.Text, "NAME", "SYSINI.DBF", "Global_MaxLength", OData, MaxLen);


            //if (iResult != 0)
            //{
            //    lblFrmResponseGlobal_Config.ForeColor = System.Drawing.Color.Red;
            //    lblFrmResponseGlobal_Config.Text = getLanguage("litUpdatedError.Text", "DFUpdated fail");
            //    frmResponseGlobal.Visible = true;
            //    return;
            //}


            //iResult = ApolloUtils.apolloUpdate(ConfigurationManager.AppSettings["FirmId"], false, Application["Delimiter"].ToString(),
            //    "NAME", "VALUE", txtPageLength.Text, "NAME", "SYSINI.DBF", "Global_PageLength", OData, MaxLen);


            //if (iResult != 0)
            //{
            //    lblFrmResponseGlobal_Config.ForeColor = System.Drawing.Color.Red;
            //    lblFrmResponseGlobal_Config.Text = getLanguage("litUpdatedError.Text", "DFUpdated fail");
            //    frmResponseGlobal.Visible = true;
            //    return;
            //}

            //iResult = ApolloUtils.apolloUpdate(ConfigurationManager.AppSettings["FirmId"], false, Application["Delimiter"].ToString(),
            //"NAME", "VALUE", txtStartPageNr.Text, "NAME", "SYSINI.DBF", "Global_StartPageNr", OData, MaxLen);


            //if (iResult != 0)
            //{
            //    lblFrmResponseGlobal_Config.ForeColor = System.Drawing.Color.Red;
            //    lblFrmResponseGlobal_Config.Text = getLanguage("litUpdatedError.Text", "DFUpdated fail");
            //    frmResponseGlobal.Visible = true;
            //    return;
            //}

            //frmResponseGlobal.Visible = true;
            //lblFrmResponseGlobal_Config.ForeColor = System.Drawing.Color.Green;
            //lblFrmResponseGlobal_Config.Text = getLanguage("litUpdatedOK.Text", "DFUpdated successfully");
        }

        protected void btnEmailSave_Click(object sender, EventArgs e)
        {
            bool rsUserEmail = SysIniController.updateSetting("Global_UserEmail", txtAdminEmail.Text);
            bool rsForwardEmail = SysIniController.updateSetting("Global_FromEmail", txtForwardEmail.Text);

            frmResponseEmail.Visible = true;

            if (rsUserEmail == false || rsForwardEmail == false)
            {
                lblFrmResponseEmail.ForeColor = System.Drawing.Color.Red;
                lblFrmResponseEmail.Text = LangController.getLng("litSaveError.Text", "Lưu thất bại");
                return;
            }
            closeFrm();
            frmResponseEmail.Visible = true;
            lblFrmResponseEmail.ForeColor = System.Drawing.Color.Green;
            lblFrmResponseEmail.Text = LangController.getLng("litSaveOk.Text", "Lưu thành công");
        }

        protected void btnStatisticSave_Click(object sender, EventArgs e)
        {
            bool rsExtraNumOfUser = SysIniController.updateSetting("OnlineUsers", txtExtraNumOfUser.Text);
            bool rsExtraNumOfVisitor = SysIniController.updateSetting("TotalVisit", txtExtraNumOfVisitor.Text);

            if (rsExtraNumOfUser == false || rsExtraNumOfVisitor == false)
            {
                lblFrmResponseStatistic.ForeColor = System.Drawing.Color.Red;
                lblFrmResponseStatistic.Text = LangController.getLng("litSaveError.Text", "Lưu thất bại");
                return;
            }
            closeFrm();
            frmResponseStatistic.Visible = true;
            lblFrmResponseStatistic.ForeColor = System.Drawing.Color.Green;
            lblFrmResponseStatistic.Text = LangController.getLng("litSaveOk.Text", "Lưu thành công");
        }

        protected void btnDisplaySave_Click(object sender, EventArgs e)
        {
            bool rsRandomSpecialNumber = SysIniController.updateSetting("Number_Random_Special", txtGiaHotPageSize.Text);
            bool rsRandomFeatureNumber = SysIniController.updateSetting("Number_Random_Featured", txtDTDDPageSize.Text);
            //bool rsNewProductNumber = SysIniController.updateSetting("Number_New_Product", txtNewProductNumber.Text);
            bool rsArticleHomeNumber = SysIniController.updateSetting("Number_Article_Home", txtHotPriceHotNewsCount.Text);
            bool rsNewAccessoriesNumber = SysIniController.updateSetting("Number_New_Accessories", txtHotPriceMarketNewsCount.Text);
            bool rsSamePriceRange = SysIniController.updateSetting("Number_Interval_Price", txtSamePriceRange.Text);
            //bool rsOtherProductNumber = SysIniController.updateSetting("Number_Other_Product", txtOtherProductNumber.Text);

            //rsNewProductNumber == false || rsOtherProductNumber == falsersOtherProductNumber == false ||
            if (rsRandomSpecialNumber == false || rsRandomFeatureNumber == false || 
                rsArticleHomeNumber == false || rsNewAccessoriesNumber == false  || 
                rsSamePriceRange == false)
            {
                lblFrmResponseDisplay.ForeColor = System.Drawing.Color.Red;
                lblFrmResponseDisplay.Text = LangController.getLng("litSaveError.Text", "Lưu thất bại");
                return;
            }
            closeFrm();
            frmResponseDisplay.Visible = true;
            lblFrmResponseDisplay.ForeColor = System.Drawing.Color.Green;
            lblFrmResponseDisplay.Text = LangController.getLng("litSaveOk.Text", "Lưu thành công");
        }

        protected void btnUpgradeSave_Click(object sender, EventArgs e)
        {
            bool rsIsActive = SysIniController.updateSetting("Upgrade_IsActive", rblUpgardeActive.SelectedValue);
            bool rsDateTime = SysIniController.updateSetting("Upgrade_DateTime", Common.strToFormatDateTime(txtCDDateTime.Text, "dd/MM/yyyy HH:mm").ToString("yyyyMMddHHmmss"));

            if ( rsIsActive == false || rsDateTime == false)
            {
                lblFrmResponseUpgrade.ForeColor = System.Drawing.Color.Red;
                lblFrmResponseUpgrade.Text = LangController.getLng("litSaveError.Text", "Lưu thất bại");
                return;
            }
            closeFrm();
            frmResponseUpgrade.Visible = true;
            lblFrmResponseUpgrade.ForeColor = System.Drawing.Color.Green;
            lblFrmResponseUpgrade.Text = LangController.getLng("litSaveOk.Text", "Lưu thành công");

        }
    }
}