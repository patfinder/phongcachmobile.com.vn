﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.IO;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class ung_dung : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litAppManager.Text", "Quản lý ứng dụng");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadCategory();
                loadOS();
                loadAllApp();
            }
        }

        private void loadOS()
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId("6");
            ddlOS.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string id = dr["Id"].ToString();
                    string name = dr["Name"].ToString();
                    ddlOS.Items.Add(new ListItem(name, id));
                }
            }
            ddlOS.DataBind();
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadAllApp()
        {
            DataTable dt = ApplicationController.getApps(ddlCategory.SelectedValue, ddlOS.SelectedValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                string imagePath = "images/application/";
                foreach (DataRow dr in dt.Rows)
                {
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"].ToString();
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                }
            }
            dgrApp.DataSource = dt;
            dgrApp.DataBind();
        }

        private void loadCategory()
        {
            DataTable dt = CategoryController.getCategories("6", null);
            ddlCategory.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string id = dr["Id"].ToString();
                    string name = dr["Name"].ToString();
                    ddlCategory.Items.Add(new ListItem(name, id));
                }
            }
            ddlCategory.DataBind();
        }

        private void loadLang()
        {
            this.litApp.Text = LangController.getLng("litApp..Text", "Ứng dụng");
            this.litAddAppHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litAddAppHelpDesc.Text = LangMsgController.getLngMsg("litAddGameHelpDesc.Desc", "Những thông tin giải thích về thêm / sửa ứng dụng");

            this.btnAddApp.Text = LangController.getLng("litAdd.Text", "Thêm");
            this.btnDeleteApp.Text = LangController.getLng("litDelete.Text", "Xóa");
            this.btnAddSaveApp.Text = LangController.getLng("litSave.Text", "Lưu");
            this.btnCancelApp.Text = LangController.getLng("litCancel.Text", "Hủy");
        }

        protected void btnDeleteApp_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrApp.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrApp.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrApp.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = ApplicationController.deleteApplication(gvIDs);
                if (result)
                {
                    loadAllApp();
                    //Delete images
                    try
                    {
                        string imagePath = "images/application/";

                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');
                        //loi chua delete hinh
                        foreach (FileInfo file in files)
                        {
                            try
                            {
                                if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                                {
                                    File.Delete(file.FullName);
                                }
                            }
                            catch (Exception ex)
                            {
                                string message = ex.Message;
                            }
                        }

                        lblFrmResponseApp.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseApp.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseApp.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseApp.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseApp.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseApp.Visible = true;
                }
            }
            resetAddApp();
        }

        private void resetAddApp()
        {
            txtName.Text = txtDesc.Text = txtCurrentVerion.Text = txtDeveloper.Text = txtRequiredOS.Text = txtFilesize.Text =
                txtLikeCount.Text = txtViewCount.Text = imgDisplay.ImageUrl = hplDisplayImg.NavigateUrl = litFileName.Text = 
                txtFilename.Text = "";

            hdfId.Value = "";
            chbIsFree.Checked = false;
            divAddDelete.Attributes["style"] = "display:block";
            divAddApp.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveApp_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = "images/application/";
            string strAppPath = "app/";
            if (this.hdfId.Value != "")
            {
                if (fulDisplayImg.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgDisplay.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                rs = ApplicationController.updateApp(new Application
                {
                    id = int.Parse(hdfId.Value),
                    name = txtName.Text,
                    isFree = chbIsFree.Checked,
                    description = txtDesc.Text,
                    categoryId = int.Parse(ddlCategory.SelectedValue),
                    likeCount = int.Parse(txtLikeCount.Text),
                    viewCount = int.Parse(txtViewCount.Text),
                    filename = txtFilename.Text,
                    currentVersion = txtCurrentVerion.Text,
                    developer = txtDeveloper.Text,
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    requiredOS = txtRequiredOS.Text,
                    osId = int.Parse(ddlOS.SelectedValue),
                    filesize = txtFilesize.Text,
                });
            }
            else
            {
                rs = ApplicationController.createApp(new Application
                {
                    name = txtName.Text,
                    isFree = chbIsFree.Checked,
                    description = txtDesc.Text,
                    categoryId = int.Parse(ddlCategory.SelectedValue),
                    likeCount = int.Parse(txtLikeCount.Text),
                    viewCount = int.Parse(txtViewCount.Text),
                    filename = txtFilename.Text,
                    currentVersion = txtCurrentVerion.Text,
                    developer = txtDeveloper.Text,
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    requiredOS = txtRequiredOS.Text,
                    osId = int.Parse(ddlOS.SelectedValue),
                    filesize = txtFilesize.Text
                });
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfId.Value == "" ? ApplicationController.getCurrentId() : hdfId.Value;
                try
                {
                    if (fulDisplayImg.PostedFile.FileName != "")
                    {
                        fulDisplayImg.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulDisplayImg.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImg.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update APPLICATION set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }

                    //if (fulFile.PostedFile.FileName != "")
                    //{
                    //    fulFile.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strAppPath) + id + "_" + fulFile.PostedFile.FileName);
                    //    object[,] parameters = { { "@Filename", id + "_" + fulFile.PostedFile.FileName }, 
                    //                           {"@Id", id}};

                    //    DB.exec("update APPLICATION set Filename = @Filename where Id = @Id", parameters);
                    //}
                }
                catch (Exception ex)
                {
                }

                loadAllApp();
                lblFrmResponseApp.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseApp.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseApp.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseApp.Visible = true;
            }
            else
            {
                lblFrmResponseApp.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseApp.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseApp.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseApp.Visible = true;
            }
            resetAddApp();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrApp.CurrentPageIndex = 0;
            loadAllApp();
            frmResponseApp.Visible = false;
        }

        protected void dgrApp_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllApp();
            DataTable dt = (DataTable)dgrApp.DataSource;
            DataRow dr = dt.Rows[iIndex];

            this.txtName.Text = dr["Name"].ToString();
            this.txtDesc.Text = dr["Description"].ToString();
            this.txtCurrentVerion.Text = dr["CurrentVersion"].ToString();
            this.txtDeveloper.Text = dr["Developer"].ToString();
            this.txtRequiredOS.Text = dr["RequiredOS"].ToString();
            this.txtFilesize.Text = dr["Filesize"].ToString();
            this.txtLikeCount.Text = dr["LikeCount"].ToString();
            this.txtViewCount.Text = dr["ViewCount"].ToString();
            this.txtFilename.Text = dr["FileName"].ToString();
            this.imgDisplay.ImageUrl = dr["DisplayImage"].ToString();
            this.hplDisplayImg.NavigateUrl = dr["DisplayImage"].ToString();
            litFileName.Text = dr["Filename"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            chbIsFree.Checked = bool.Parse(dr["IsFree"].ToString());

            divAddDelete.Attributes["style"] = "display:none";
            divAddApp.Attributes["style"] = "display:block";
            frmResponseApp.Visible = false;
        }

        protected void dgrApp_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrApp.CurrentPageIndex = e.NewPageIndex;
            loadAllApp();
            frmResponseApp.Visible = false;
            //Disable add menu
            resetAddApp();
        }

        protected void ddlOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrApp.CurrentPageIndex = 0;
            loadAllApp();
            frmResponseApp.Visible = false;
        }
    }
}