﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="game.aspx.cs" Inherits="PhongCachMobile.admin.game" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script type="text/javascript">
     function addGame() {
         jQuery('#content_divAddGame').show();
         jQuery('#content_divAddDelete').hide();
     }

     function cancelAddSaveGame() {
         jQuery('#content_divAddGame').hide();
         jQuery('#content_divAddDelete').show();
     }

     function check_uncheck(Val) {
         var ValChecked = Val.checked;
         var ValId = Val.id;
         var frm = document.forms[0];
         // Loop through all elements
         for (i = 0; i < frm.length; i++) {
             // Look for Header Template's Checkbox
             //As we have not other control other than checkbox we just check following statement
             if (this != null) {
                 if (ValId.indexOf('CheckAll') != -1) {
                     // Check if main checkbox is checked,
                     // then select or deselect datagrid checkboxes
                     //alert(frm.elements[i].id);
                     // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                     if (frm.elements[i].id.indexOf('deleteRec') != -1)
                         if (ValChecked)
                             frm.elements[i].checked = true;
                         else
                             frm.elements[i].checked = false;
                 }
                 else if (ValId.indexOf('deleteRec') != -1) {
                     //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                     // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                     document.getElementById('content_dgrGame_CheckAll').checked = false;

                     if (frm.elements[i].checked == false)
                         frm.elements[1].checked = false;
                 }
             } // if
         } // for
     } // function</PRE>

     function confirmMsg(frm) {
         // loop through all elements
         for (i = 0; i < frm.length; i++) {
             // Look for our checkboxes only
             if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                 // If any are checked then confirm alert, otherwise nothing happens
                 if (frm.elements[i].checked)
                     return confirm('Bạn có chắc muốn xóa?')
             }
         }
     }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litGame" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseGame" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseGame" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmGame" runat="server">
                                    <p class="field">
                                        Category &nbsp; 
                                        <asp:DropDownList
                                            ID="ddlCategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                        </asp:DropDownList> &nbsp; |  &nbsp;  Hệ điều hành &nbsp; 
                                        <asp:DropDownList
                                            ID="ddlOS" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOS_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrGame" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" 
                                        oneditcommand="dgrGame_EditCommand" 
                                        onpageindexchanged="dgrGame_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Name" HeaderText="Tên" >
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                    Width="40%" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("DisplayImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("Name")%>' AlternateText='<%#Eval("Name")%>'  ImageUrl='<%# Eval("DisplayImage") %>' Width="30%" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="CURRENTVERSION" HeaderText="Phiên bản" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="POSTEDDATE" HeaderText="Ngày đăng">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="VIEWCOUNT" HeaderText="Lượt xem" DataFormatString="{0:#,###}"></asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật">
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" 
                                            HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnAddGame" runat="server" CssClass="wpcf7-submit" OnClientClick="return addGame();"
                                            UseSubmitBehavior="false" ClientIDMode="Static"  />
                                        <asp:Button ID="btnDeleteGame" runat="server" CssClass="wpcf7-submit"
                                            Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return confirmMsg(this.form)"
                                            ClientIDMode="Static" OnClick="btnDeleteGame_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddGame" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>Thêm / chỉnh sửa game</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseAddGame" runat="server" visible="false">
                                        <p class="field" style="color: Red">
                                            <asp:Literal ID="litFrmResponseAddGame" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            Tên<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Giới thiệu<small> *
                                            </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDesc" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                           Phiên bản hiện tại
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCurrentVerion" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="50"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Nhà phát triển
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDeveloper" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="150"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Yêu cầu HDH<small> *
                                            </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtRequiredOS" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="50"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                           Dung lượng
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtFilesize" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="150"></asp:TextBox></span>
                                        </p>
                                         <p>
                                         <br />
                                            <asp:CheckBox ID="chbIsFree" runat="server" />
                                            là game miễn phí
                                        </p>
                                        <p class="field">
                                            Lượt thích<small> *
                                            </small>
                                            <br />
                                              <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtLikeCount" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Lượt xem
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtViewCount" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị
                                            <br />
                                            <asp:HyperLink ID="hplDisplayImg" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgDisplay" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulDisplayImg" runat="server" />
                                        </p>
                                          <p class="field">
                                            App URL
                                            <br />
                                              <asp:Literal ID="litFileName" runat="server"></asp:Literal><br />
                                            <%--<asp:FileUpload ID="fulFile" runat="server" />--%>
                                            <asp:TextBox ID="txtFilename" runat="server" CssClass="wpcf7-text" Width="500px" ></asp:TextBox>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveGame" runat="server" class="wpcf7-submit" OnClick="btnAddSaveGame_Click" />
                                            <asp:Button ID="btnCancelGame" runat="server" CssClass="wpcf7-submit"
                                                Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return cancelAddSaveGame();"
                                                UseSubmitBehavior="false" ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div2" class="widget">
                            <h2>
                                <asp:Literal ID="litAddGameHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div3" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddGameHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
