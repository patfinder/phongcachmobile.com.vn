﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="authorized.aspx.cs" Inherits="PhongCachMobile.admin.authorized" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                        <div>
                            <h2>
                                <asp:Literal ID="litAuthorized" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseAuthorized" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseAuthorized" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="frmUpgrade" runat="server">
                                        <p class="field">
                                            <asp:Literal ID="litAuthorizedText" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <%-- End Under Construction Section--%>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
