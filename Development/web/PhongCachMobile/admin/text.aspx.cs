﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;

namespace PhongCachMobile.admin
{
    public partial class text : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litTextManager.Text", "Quản lý ngôn ngữ text");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadAllText();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadAllText()
        {
            DataTable dt = LangController.getLngs();
            if (dt != null && dt.Rows.Count > 0)
            {
                dgrText.DataSource = dt;
                dgrText.DataBind();
            }
        }

        private void loadLang()
        {
            litText.Text = LangController.getLng("litLanguageText.Text", "Ngôn ngữ text");
            btnDeleteText.Text = LangController.getLng("litDelete.Text", "Xóa");
            litEditText.Text = LangController.getLng("litEditText.Text", "Chỉnh sửa text");
            litValue.Text = LangController.getLng("litValue.Text", "Giá trị");
            btnAddSaveText.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelText.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddTextHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litAddTextHelpDesc.Text = LangMsgController.getLngMsg("litAddTextHelp.Desc", "Những thông tin giúp đỡ về cách chỉnh sửa ngôn ngữ text");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrText.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrText.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrText.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = LangController.deleteLang(gvIDs);

                if (result)
                {
                    loadAllText();
                    try
                    {
                        lblFrmResponseText.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseText.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseText.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseText.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseText.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseText.Visible = true;
                }
            }
            resetAddText();
        }

        private void resetAddText()
        {
            txtValue.Text = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddText.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveText_Click(object sender, EventArgs e)
        {
            bool rs = false;
            if (this.hdfId.Value != "")
            {
                rs = LangController.updateLngById(hdfId.Value,txtValue.Text);
            }
            else
            {
                //rs = ServerController.createServer(new Server(txtName.Text, txtCode.Text));
            }
            if (rs)
            {
              
                loadAllText();
                lblFrmResponseText.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseText.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseText.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseText.Visible = true;
            }
            else
            {
                lblFrmResponseText.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseText.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseText.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseText.Visible = true;
            }
            resetAddText();
        }


        protected void dgrText_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllText();
            DataTable dt = (DataTable)dgrText.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtValue.Text = dr["Value"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddText.Attributes["style"] = "display:block";
            frmResponseText.Visible = false;
        }

        protected void dgrText_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrText.CurrentPageIndex = e.NewPageIndex;
            loadAllText();
            frmResponseText.Visible = false;
            resetAddText();
        }
    }
}