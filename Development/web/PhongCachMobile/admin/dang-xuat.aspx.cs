﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.admin
{
    public partial class dang_xuat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["UserId"] = null;
            Session["Email"] = null;
            Session["GroupId"] = null;
            Response.Redirect("dang-nhap", true);
        }
    }
}