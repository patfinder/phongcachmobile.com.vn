﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class bieu_mau : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litTemplateManager.Text", "Quản lý biểu mẫu");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadAllTemplate();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadAllTemplate()
        {
            DataTable dt = TemplateController.getTemplates();
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("DisplayValue", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    string displayValue = HTMLRemoval.StripTagsRegex(dr["Value"].ToString());
                    if (displayValue.Length > 100)
                    {
                        displayValue = displayValue.Substring(0, 100);
                        displayValue = displayValue.Substring(0, displayValue.LastIndexOf(' ')) + "...";
                    }
                    dr["DisplayValue"] = displayValue;
                }
                dgrTemplate.DataSource = dt;
                dgrTemplate.DataBind();
            }
        }

        private void loadLang()
        {
            litTemplate.Text = LangController.getLng("litLanguageTemplate.Text", "Biểu mẫu");
            btnDeleteTemplate.Text = LangController.getLng("litDelete.Text", "Xóa");
            litEditTemplate.Text = LangController.getLng("litEditTemplate.Text", "Chỉnh sửa biểu mẫu");
            litValue.Text = LangController.getLng("litValue.Text", "Giá trị");
            btnAddSaveTemplate.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelTemplate.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddTemplateHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litAddTemplateHelpDesc.Text = LangMsgController.getLngMsg("litAddTemplateHelp.Desc", "Những thông tin giúp đỡ về cách chỉnh sửa biểu mẫu");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrTemplate.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrTemplate.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrTemplate.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = LangMsgController.deleteLangMsg(gvIDs);

                if (result)
                {
                    loadAllTemplate();
                    try
                    {
                        lblFrmResponseTemplate.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseTemplate.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseTemplate.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseTemplate.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseTemplate.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseTemplate.Visible = true;
                }
            }
            resetAddTemplate();
        }

        private void resetAddTemplate()
        {
            txtValue.Text = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddTemplate.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveTemplate_Click(object sender, EventArgs e)
        {
            bool rs = false;
            if (this.hdfId.Value != "")
            {
                rs = TemplateController.updateTemplateById(hdfId.Value, txtValue.Text);
            }
            else
            {
                //rs = ServerController.createServer(new Server(txtName.Text, txtCode.Text));
            }
            if (rs)
            {

                loadAllTemplate();
                lblFrmResponseTemplate.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseTemplate.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseTemplate.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseTemplate.Visible = true;
            }
            else
            {
                lblFrmResponseTemplate.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseTemplate.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseTemplate.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseTemplate.Visible = true;
            }
            resetAddTemplate();
        }


        protected void dgrTemplate_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllTemplate();
            DataTable dt = (DataTable)dgrTemplate.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtValue.Text = dr["Value"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddTemplate.Attributes["style"] = "display:block";
            frmResponseTemplate.Visible = false;
        }

        protected void dgrTemplate_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrTemplate.CurrentPageIndex = e.NewPageIndex;
            loadAllTemplate();
            frmResponseTemplate.Visible = false;
            resetAddTemplate();
        }
    }
}