﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="menu.aspx.cs" Inherits="PhongCachMobile.admin.menu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function addMenu() {
        jQuery('#content_divAddMenu').show();
        jQuery('#content_divAddDelete').hide();
    }

    function cancelAddSaveMenu() {
        jQuery('#content_divAddMenu').hide();
        jQuery('#content_divAddDelete').show();
    }

    function check_uncheck(Val) {
        var ValChecked = Val.checked;
        var ValId = Val.id;
        var frm = document.forms[0];
        // Loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for Header Template's Checkbox
            //As we have not other control other than checkbox we just check following statement
            if (this != null) {
                if (ValId.indexOf('CheckAll') != -1) {
                    // Check if main checkbox is checked,
                    // then select or deselect datagrid checkboxes
                    //alert(frm.elements[i].id);
                    // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                    if (frm.elements[i].id.indexOf('deleteRec') != -1)
                        if (ValChecked)
                            frm.elements[i].checked = true;
                        else
                            frm.elements[i].checked = false;
                }
                else if (ValId.indexOf('deleteRec') != -1) {
                    //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                    // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                    document.getElementById('content_dgrMenu_CheckAll').checked = false;

                    if (frm.elements[i].checked == false)
                        frm.elements[1].checked = false;
                }
            } // if
        } // for
    } // function</PRE>

    function confirmMsg(frm) {
        // loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for our checkboxes only
            if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                // If any are checked then confirm alert, otherwise nothing happens
                if (frm.elements[i].checked)
                    return confirm('Bạn có chắc muốn xóa?')
            }
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litMenu" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseMenu" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseMenu" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmMenu" runat="server">
                                    <p class="field">
                                        <asp:Literal ID="litGroupChoosing" runat="server"> </asp:Literal>&nbsp; :<asp:DropDownList
                                            ID="ddlGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrMenu" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnPageIndexChanged="dgrMenu_PageIndexChanged"
                                        OnEditCommand="dgrMenu_EditCommand" OnItemCommand="dgrMenu_ItemCommand" 
                                        onitemdatabound="dgrMenu_ItemDataBound" PageSize="20" >
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="TEXT" HeaderText="TEXT"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="GROUPID" HeaderText="GroupId" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="PARENTID" HeaderText="ParentId" Visible="False">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ACTIONURL" HeaderText="Liên kết">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="INORDER" HeaderText="Thứ tự" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Thay đổi vị trí">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                               <ItemTemplate>
                                                    <asp:LinkButton CommandName="Up" ID="hplUp" Text="Lên" runat="server" CausesValidation="False" />
                                                    <asp:LinkButton CommandName="Down" ID="hplDown" Text="Xuống" runat="server" CausesValidation="False" />
                                               </ItemTemplate>
                                            </asp:TemplateColumn>
                                            
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnAddMenu" runat="server" CssClass="wpcf7-submit" OnClientClick="return addMenu();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteMenu" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsg(this.form)" OnClick="btnDeleteMenu_Click"
                                            ClientIDMode="Static" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddMenu" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litAddMenu" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseAddMenu" runat="server" visible="false">
                                        <p class="field" style="color: Red">
                                            <asp:Literal ID="litFrmResponseAddMenu" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                    <div id="Div1" runat="server">
                                     <p class="field">
                                         <asp:HiddenField ID="dhfId" runat="server" />
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litText" runat="server"></asp:Literal><small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValText" runat="server" ControlToValidate="txtText"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtText" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="30"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litPage" runat="server"></asp:Literal><small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:DropDownList ID="ddlPage" runat="server">
                                                </asp:DropDownList></span>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litParentMenu" runat="server"></asp:Literal>
                                            <br />
                                            <asp:DropDownList ID="ddlMenu" runat="server">
                                            </asp:DropDownList>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddMenuSave" runat="server" class="wpcf7-submit" OnClick="btnAddMenuSave_Click" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSaveMenu();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div2" class="widget">
                            <h2>
                                <asp:Literal ID="litAddMenuHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div3" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddMenuHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
