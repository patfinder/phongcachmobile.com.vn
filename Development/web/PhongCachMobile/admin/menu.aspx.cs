﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using System.IO;

namespace PhongCachMobile.admin
{
    public partial class menu : System.Web.UI.Page
    {
        DataTable dtMenu = new DataTable();
        string strLevel = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litMenuManager.Text", "Quản lý Menu");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLanguages();
                loadGroup();
                loadPage();
                loadAllMenu(ddlGroup.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadPage()
        {
            DirectoryInfo dir = new DirectoryInfo(MapPath("../"));
            FileInfo[] files = dir.GetFiles();
            ddlPage.Items.Clear();
            string front = LangController.getLng("litFrontEnd.Text", "Trang hiển thị");
            string back = LangController.getLng("litBackEnd.Text", "Trang quản lý");
            foreach (FileInfo file in files)
            {
                if (file.Extension == ".aspx")
                {
                    ddlPage.Items.Add(new ListItem(front + "|" + file.Name, file.Name));
                }
            }

            dir = new DirectoryInfo(MapPath(""));
            files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                if (file.Extension == ".aspx")
                {
                    ddlPage.Items.Add(new ListItem(back + "|" + file.Name, file.Name));
                }
            }
            ddlPage.DataBind();
        }

        private void loadLanguages()
        {
            this.litMenu.Text = LangController.getLng("litMenu.Text", "Menu");
            this.litAddMenuHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litAddMenu.Text = LangController.getLng("litAddMenu.Text", "Thêm / sửa menu");
            this.litAddMenuHelpDesc.Text = LangMsgController.getLngMsg("litAddMenuHelp.Desc", "Thông tin giải thích về thêm / cập nhật một menu");

            this.litGroupChoosing.Text = LangController.getLng("litGroupChoosing.Text", "Nhóm");
            this.btnAddMenu.Text = LangController.getLng("litAdd.Text", "Thêm");
            this.btnDeleteMenu.Text = LangController.getLng("litDelete.Text", "Xóa");
            this.btnAddMenuSave.Text = LangController.getLng("litSave.Text", "Lưu");
            this.btnCancel.Text = LangController.getLng("litCancel.Text", "Hủy");

            this.litText.Text = LangController.getLng("litText.Text", "Text");
            this.litPage.Text = LangController.getLng("litPage.Text", "Trang web");
            this.litParentMenu.Text = LangController.getLng("litParent.Text", "Là sub(con) của");

            this.reqValText.Text = LangController.getLng("litRequired.Text", "(*)");
        }

        private void loadGroup()
        {
            DataTable dt = GroupController.getGroupsByFilter("USER_ROLE");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlGroup.DataSource = dt;
                ddlGroup.DataTextField = "Name";
                ddlGroup.DataValueField = "Id";
                ddlGroup.DataBind();
            }
        }

        protected void btnDeleteMenu_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrMenu.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrMenu.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrMenu.Items[i].Cells[1].Text;
                    //delete images?
                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = MenuController.deleteMenu(gvIDs);

                if (result)
                {
                    loadAllMenu(ddlGroup.SelectedValue);
                    lblFrmResponseMenu.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponseMenu.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                    frmResponseMenu.Visible = true;
                }
                else
                {
                    lblFrmResponseMenu.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseMenu.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseMenu.Visible = true;
                }
            }

            resetAddMenu();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadAllMenu(ddlGroup.SelectedValue);
            frmResponseMenu.Visible = false;
        }

        private void resetAddMenu()
        {
            txtText.Text = ""; 
            ddlMenu.SelectedValue = "";
            ddlPage.SelectedIndex = 0;
            dhfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddMenu.Attributes["style"] = "display:none";
        }

        private void loadAllMenu(string groupId)
        {
            DataTable dt = MenuController.getMenus(groupId, null);
            dtMenu = dt.Clone();
            dtMenu.Clear();

            if (dt != null && dt.Rows.Count > 0)
            {
                ddlMenu.Items.Clear();
                ddlMenu.Items.Add(new ListItem("", ""));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dtMenu.NewRow();
                    dr["Id"] = dt.Rows[i]["Id"].ToString();
                    dr["GroupId"] = dt.Rows[i]["GroupId"].ToString();
                    dr["ParentId"] = dt.Rows[i]["ParentId"].ToString();
                    dr["Text"] = dt.Rows[i]["Text"].ToString();
                    dr["ActionUrl"] = dt.Rows[i]["ActionUrl"].ToString();
                    dr["InOrder"] = dt.Rows[i]["InOrder"].ToString();

                    dtMenu.Rows.Add(dr);
                    ddlMenu.Items.Add(new ListItem(dt.Rows[i]["Text"].ToString(), dt.Rows[i]["Id"].ToString()));
                    constructSubMenuHome(dt.Rows[i]["GroupId"].ToString(), dt.Rows[i]["Id"].ToString());
                }

                dgrMenu.DataSource = dtMenu;
                dgrMenu.DataBind();

                ddlMenu.DataBind();
            }
        }

        private void constructSubMenuHome(string groupId, string parentId)
        {
             DataTable dt = MenuController.getMenus(groupId, parentId);

            if (dt != null && dt.Rows.Count >0)
            {
                strLevel += SysIniController.getSetting("Global_IndentLevel", "---");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dtMenu.NewRow();

                    dr["Id"] = dt.Rows[i]["Id"].ToString();
                    dr["GroupId"] = dt.Rows[i]["GroupId"].ToString();
                    dr["ParentId"] = dt.Rows[i]["ParentId"].ToString();
                    dr["Text"] = strLevel + dt.Rows[i]["Text"].ToString();
                    dr["ActionUrl"] = dt.Rows[i]["ActionUrl"].ToString();
                    dr["InOrder"] = dt.Rows[i]["InOrder"].ToString();

                    dtMenu.Rows.Add(dr);
                    ddlMenu.Items.Add(new ListItem(strLevel + dt.Rows[i]["Text"].ToString(), dt.Rows[i]["Id"].ToString()));
                    constructSubMenuHome(dt.Rows[i]["GroupId"].ToString(), dt.Rows[i]["Id"].ToString());

                }
                strLevel = strLevel.Substring(SysIniController.getSetting("Global_IndentLevel", "---").Length);
            }
        }

        protected void btnAddMenuSave_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.dhfId.Value != "")
            {
                rs = MenuController.updateMenu(new model.Menu(int.Parse(dhfId.Value),
                      ddlMenu.SelectedValue == "" ? -1 : int.Parse(ddlMenu.SelectedValue),
                    int.Parse(ddlGroup.SelectedValue), txtText.Text, "",
                    ddlPage.SelectedValue, false, -1));
            }
            else
            {
                int parentId = (ddlMenu.SelectedValue == "" ? -1 : int.Parse(ddlMenu.SelectedValue));
                rs = MenuController.createMenu(new model.Menu(parentId, int.Parse(ddlGroup.SelectedValue), txtText.Text,
                    ddlPage.SelectedValue, "", false, getMaxOrderBy(ddlMenu.SelectedValue)));
            }

            if (rs == true)
            {
                loadAllMenu(ddlGroup.SelectedValue);
                lblFrmResponseMenu.ForeColor = System.Drawing.Color.Green;
                if (this.dhfId.Value != "")
                {
                    lblFrmResponseMenu.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                else
                {
                    lblFrmResponseMenu.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                frmResponseMenu.Visible = true;
            }
            else
            {
                lblFrmResponseMenu.ForeColor = System.Drawing.Color.Red;
                if (this.dhfId.Value != "")
                {
                    lblFrmResponseMenu.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }
                else
                {
                    lblFrmResponseMenu.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }

                frmResponseMenu.Visible = true;
            }
            resetAddMenu();
        }

        private int getMaxOrderBy(string menuId)
        {
            if (menuId == "&nbsp;" || menuId == "" )
                menuId = null;
            DataTable dt = MenuController.getMenus(ddlGroup.SelectedValue, menuId);

            if (dt == null)
                return 0;
            return dt.Rows.Count;
            
        }

        protected void dgrMenu_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            loadAllMenu(ddlGroup.SelectedValue);
            dgrMenu.CurrentPageIndex = e.NewPageIndex;
            dgrMenu.DataBind();

            //Disable add menu
            resetAddMenu();

            frmResponseMenu.Visible = false;

        }

        protected void dgrMenu_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllMenu(ddlGroup.SelectedValue);
            DataTable dt = (DataTable)dgrMenu.DataSource;
            DataRow dr = dt.Rows[iIndex];

            this.txtText.Text = dr["Text"].ToString().Replace(SysIniController.getSetting("Global_IndentLevel", "---"), "");
            this.ddlPage.SelectedValue = dr["ActionUrl"].ToString();
            this.ddlMenu.SelectedValue = dr["ParentId"].ToString();
            this.dhfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddMenu.Attributes["style"] = "display:block";

            frmResponseMenu.Visible = false;
        }

        protected void dgrMenu_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Up")
            {
                int iIndex = 0;
                DataTable dtSibling = loadSiblingMenu(e.Item.Cells[4].Text);
                DataRow drCurrent = null;
                for (int i = 0; i < dtSibling.Rows.Count; i++)
                {
                    if (dtSibling.Rows[i]["id"].ToString() == e.Item.Cells[1].Text)
                    {
                        drCurrent = dtSibling.Rows[i];
                        iIndex = i;
                        break;
                    }
                }

                if (iIndex != 0)
                {
                    DataRow drPrevious = dtSibling.Rows[iIndex - 1];

                    string strOrderBy = drCurrent["InOrder"].ToString();

                    bool rsCurrent = MenuController.updateOrderMenu(drCurrent["Id"].ToString(), int.Parse(drPrevious["InOrder"].ToString()));
                    bool rsPrevious = MenuController.updateOrderMenu(drPrevious["Id"].ToString(), int.Parse(strOrderBy));

                    frmResponseAddMenu.Visible = true;

                    if (rsCurrent && rsPrevious)
                    {
                        loadAllMenu(ddlGroup.SelectedValue);
                        lblFrmResponseMenu.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseMenu.Text = LangController.getLng("litMoveOk.Text", "Thay đổi thành công");
                    }
                    else
                    {
                        lblFrmResponseMenu.ForeColor = System.Drawing.Color.Red;
                        lblFrmResponseMenu.Text = LangController.getLng("litMoveError.Text", "Thay đổi thất bại");
                    }
                }
            }
            else
                if (e.CommandName == "Down")
                {
                    int iIndex = 0;
                    DataTable dtSibling = loadSiblingMenu(e.Item.Cells[4].Text);
                    DataRow drCurrent = null;
                    for (int i = 0; i < dtSibling.Rows.Count; i++)
                    {
                        if (dtSibling.Rows[i]["id"].ToString() == e.Item.Cells[1].Text)
                        {
                            drCurrent = dtSibling.Rows[i];
                            iIndex = i;
                            break;
                        }
                    }

                    if (iIndex != dtSibling.Rows.Count - 1)
                    {
                        DataRow drNext = dtSibling.Rows[iIndex + 1];

                        string strOrderBy = drCurrent["InOrder"].ToString();

                        bool rsCurrent = MenuController.updateOrderMenu(drCurrent["Id"].ToString(), int.Parse(drNext["InOrder"].ToString()));
                        bool rsNext = MenuController.updateOrderMenu(drNext["Id"].ToString(), int.Parse(strOrderBy));

                        frmResponseMenu.Visible = true;

                        if (rsCurrent && rsNext)
                        {
                            loadAllMenu(ddlGroup.SelectedValue);
                            lblFrmResponseMenu.ForeColor = System.Drawing.Color.Green;
                            lblFrmResponseMenu.Text = LangController.getLng("litMoveOk.Text", "Thay đổi thành công");
                        }
                        else
                        {
                            lblFrmResponseMenu.ForeColor = System.Drawing.Color.Red;
                            lblFrmResponseMenu.Text = LangController.getLng("litMoveError.Text", "Thay đổi thất bại");
                        }
                    }
                }
        }

        private DataTable loadSiblingMenu(string menuId)
        {
            if (menuId == "&nbsp;")
                menuId = null;
            DataTable dt = MenuController.getMenus(ddlGroup.SelectedValue, menuId);
            return dt;
        }

        protected void dgrMenu_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton btnUp = (LinkButton)e.Item.FindControl("hplUp");

                if (btnUp != null && e.Item.Cells[6].Text == "0")
                {
                    btnUp.Visible = false;
                }

                LinkButton btnDown = (LinkButton)e.Item.FindControl("hplDown");
                int iOrderBy = getMaxOrderBy(e.Item.Cells[4].Text) - 1;
                string strOrderBy = iOrderBy.ToString();
                if (btnDown != null && e.Item.Cells[6].Text == strOrderBy)
                {
                    btnDown.Visible = false;
                }
            }

        }
    }
}