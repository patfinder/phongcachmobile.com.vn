﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PhongCachMobile.admin
{
    /// <summary>
    /// Summary description for AdminServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AdminServices : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetAccessories(string filter)
        {
            string list = "";
            DataTable dt = ProductController.getSimpleProductsByGroupId(7, filter);
            foreach (DataRow dr in dt.Rows)
            {
                list += "<div><input type='checkbox' class='ck-access' id='access-" + dr["ID"] + "' />" + dr["Name"] + "</div>\n";
            }

            return list;
        }

        [WebMethod]
        public string SaveProductAccessories(string productID, string accessories)
        {
            return ProductController.updateProductAccessories(int.Parse(productID), accessories) ? "Thành công" : "Thất bại";
        }
    }
}
