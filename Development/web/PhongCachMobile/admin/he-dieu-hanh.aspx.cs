﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;

namespace PhongCachMobile.admin
{
    public partial class he_dieu_hanh : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litOSManager.Text", "Quản lý Hệ Điều Hành");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadGroup();
                loadAllOS(ddlGroup.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadGroup()
        {
            ddlGroup.Items.Clear();
            ddlGroup.Items.Add(new ListItem("ĐTDĐ", "3"));
            ddlGroup.Items.Add(new ListItem("Máy tính bảng", "4"));
            ddlGroup.Items.Add(new ListItem("Trò chơi", "5"));
            ddlGroup.Items.Add(new ListItem("Ứng dụng", "6"));
            ddlGroup.DataBind();

        }


        private void loadAllOS(string groupId)
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId(groupId);
            if (dt != null && dt.Rows.Count > 0)
            {
                dgrOS.DataSource = dt;
                dgrOS.DataBind();
            }
        }

        private void loadLang()
        {
            litOS.Text = LangController.getLng("litOS.Text", "Hệ điều hành");
            btnAddOS.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeleteOS.Text = LangController.getLng("litDelete.Text", "Xóa");
            litAddOS.Text = LangController.getLng("litAddOS.Text", "Thêm / sửa hệ điều hành");
            litName.Text = LangController.getLng("litName.Text", "Tên");
            litGroupChoosing.Text = LangController.getLng("litGroup.Text", "Nhóm");
            btnAddSaveOS.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelOS.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddOSHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            litAddOSHelpDesc.Text = LangMsgController.getLngMsg("litAddOSHelpDesc.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa hệ điều hành");

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrOS.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrOS.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrOS.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = OperatingSystemController.deleteOS(gvIDs);

                if (result)
                {
                    loadAllOS(ddlGroup.SelectedValue);
                    try
                    {
                        lblFrmResponseOS.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseOS.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseOS.Visible = true;
                    }
                    catch
                    {
                        lblFrmResponseOS.ForeColor = System.Drawing.Color.Red;
                        lblFrmResponseOS.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                        frmResponseOS.Visible = true;
                    }
                }
                else
                {
                    lblFrmResponseOS.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseOS.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseOS.Visible = true;
                }
            }
            resetAddOS();
        }

        private void resetAddOS()
        {
            txtName.Text = "";
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddOS.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveOS_Click(object sender, EventArgs e)
        {
            bool rs;

            if (this.hdfId.Value != "")
            {
                rs = OperatingSystemController.updateOS(new model.OperatingSystem{
                    id = int.Parse(hdfId.Value),
                    name =  txtName.Text,
                    groupId = int.Parse(ddlGroup.SelectedValue)
                });
            }
            else
            {
                rs = OperatingSystemController.createOS(new model.OperatingSystem
                {
                    name = txtName.Text,
                    groupId = int.Parse(ddlGroup.SelectedValue)
                });
            }
            if (rs)
            {
                loadAllOS(ddlGroup.SelectedValue);
                lblFrmResponseOS.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseOS.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseOS.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseOS.Visible = true;
            }
            else
            {
                lblFrmResponseOS.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseOS.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseOS.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseOS.Visible = true;
            }
            resetAddOS();
        }


        protected void dgrOS_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllOS(ddlGroup.SelectedValue);
            DataTable dt = (DataTable)dgrOS.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtName.Text = dr["Name"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddOS.Attributes["style"] = "display:block";
            frmResponseOS.Visible = false;
        }

        protected void dgrOS_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrOS.CurrentPageIndex = e.NewPageIndex;
            loadAllOS(ddlGroup.SelectedValue);
            frmResponseOS.Visible = false;
            resetAddOS();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrOS.CurrentPageIndex = 0;
            loadAllOS(ddlGroup.SelectedValue);
            frmResponseOS.Visible = false;
            resetAddOS();
        }
    }
}