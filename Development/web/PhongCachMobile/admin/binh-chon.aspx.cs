﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class binh_chon : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litPollManager.Text", "Quản lý bình chọn");
            if (!this.IsPostBack)
            {
                loadLang();
                loadAllPoll();
            }
        }

        private void loadAllPoll()
        {
            DataTable dt = PollController.getPolls();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    dr["StartDate"] = Common.strToDateTime(dr["StartDate"].ToString()).ToShortDateString();
                    dr["EndDate"] = Common.strToDateTime(dr["EndDate"].ToString()).ToShortDateString();

                }
            }
            dgrPoll.DataSource = dt;
            dgrPoll.DataBind();
        }

        private void loadLang()
        {
            btnAddPoll.Text = btnAddDetail.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeletePoll.Text = btnDeleteDetail.Text = LangController.getLng("litDelete.Text", "Xóa");
            btnCloseDetail.Text = LangController.getLng("litClose.Text", "Đóng");
            btnAddSavePoll.Text = btnAddSaveDetail.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelPoll.Text = btnCancelSaveDetail.Text = LangController.getLng("litCancel.Text", "Hủy");
        }

        protected void dgrPoll_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Detail")
            {
                int iIndex = e.Item.DataSetIndex;
                loadAllPoll();
                DataTable dt = (DataTable)dgrPoll.DataSource;
                DataRow dr = dt.Rows[iIndex];
                hdfPollId.Value = dr["Id"].ToString();
                loadAllDetail(dr["Id"].ToString());

                divAddDeletePoll.Attributes["style"] = "display:none";
                divAddPoll.Attributes["style"] = "display:none";
                divDetail.Attributes["style"] = "display:block";
                divAddDetail.Attributes["style"] = "display:none";

                frmResponseDetail.Visible = false;
                frmResponsePoll.Visible = false;
            }
        }

        private void loadAllDetail(string pollId)
        {
            DataTable dt = PollDetailController.getPollDetailByPollId(pollId);
            dgrDetail.DataSource = dt;
            dgrDetail.DataBind();
        }

        protected void btnDeletePoll_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrPoll.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrPoll.Items[i].FindControl("deleteRecPoll");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrPoll.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = ProductController.deleteProduct(gvIDs);

                if (result)
                {
                    loadAllPoll();
                    //Delete images
                    try
                    {
                        lblFrmResponsePoll.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponsePoll.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponsePoll.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponsePoll.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponsePoll.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponsePoll.Visible = true;
                }
            }
            resetAddPoll();
        }

        private void resetAddPoll()
        {
            txtName.Text = txtStartDate.Text = txtEndDate.Text = "";
            hdfId.Value = "";

            divAddDeletePoll.Attributes["style"] = "display:block";
            divAddPoll.Attributes["style"] = "display:none";
            divDetail.Attributes["style"] = "display:none";
            divAddDetail.Attributes["style"] = "display:none";

        }

        protected void btnAddSavePoll_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.hdfId.Value != "")
            {
                rs = PollController.updatePoll(new Poll
                {
                    id = int.Parse(hdfId.Value),
                    name = txtName.Text,
                    startDate = Common.strToFormatDateTime(txtStartDate.Text, "dd/MM/yyyy").ToString("yyyyMMddHHmmss"),
                    endDate = Common.strToFormatDateTime(txtEndDate.Text, "dd/MM/yyyy").ToString("yyyyMMddHHmmss")
                });

            }
            else
            {
                rs = PollController.createPoll(new Poll
                {
                    name = txtName.Text,
                    startDate = Common.strToFormatDateTime(txtStartDate.Text, "dd/MM/yyyy").ToString("yyyyMMddHHmmss"),
                    endDate = Common.strToFormatDateTime(txtEndDate.Text, "dd/MM/yyyy").ToString("yyyyMMddHHmmss")
                });
            }
            if (rs)
            {
                string id = this.hdfId.Value == "" ? PollController.getCurrentId() : hdfId.Value;

                loadAllPoll();
                lblFrmResponsePoll.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponsePoll.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponsePoll.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponsePoll.Visible = true;
            }
            else
            {
                lblFrmResponsePoll.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponsePoll.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponsePoll.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponsePoll.Visible = true;
            }
            resetAddPoll();
        }

        protected void dgrPoll_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrPoll.CurrentPageIndex = e.NewPageIndex;
            loadAllPoll();
            frmResponsePoll.Visible = false;
            resetAddPoll();
        }

        protected void dgrPoll_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllPoll();
            DataTable dt = (DataTable)dgrPoll.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtName.Text = dr["Name"].ToString();
            txtStartDate.Text = dr["StartDate"].ToString();
            txtEndDate.Text = dr["EndDate"].ToString();
          
            this.hdfId.Value = dr["Id"].ToString();
            divAddDeletePoll.Attributes["style"] = "display:none";
            divAddPoll.Attributes["style"] = "display:block";
            divDetail.Attributes["style"] = "display:none";
            divAddDetail.Attributes["style"] = "display:none";

            frmResponsePoll.Visible = false;
        }

        protected void dgrDetail_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllDetail(hdfPollId.Value);
            DataTable dt = (DataTable)dgrDetail.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtDetailName.Text = dr["Name"].ToString();
            this.hdfDetailId.Value = dr["Id"].ToString();

            divAddDeletePoll.Attributes["style"] = "display:none";
            divAddPoll.Attributes["style"] = "display:none";
            divDetail.Attributes["style"] = "display:block";
            divAddDeleteDetail.Attributes["style"] = "display:none";
            divAddDetail.Attributes["style"] = "display:block";

            frmResponseDetail.Visible = false;
            frmResponsePoll.Visible = false;
        }

        protected void dgrDetail_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrDetail.CurrentPageIndex = e.NewPageIndex;
            loadAllDetail(hdfPollId.Value);
            frmResponseDetail.Visible = false;
            resetAddDetail();
        }

        private void resetAddDetail()
        {
            txtDetailName.Text = "";
            hdfDetailId.Value = "";

            divDetail.Attributes["style"] = "display:block";
            divAddDetail.Attributes["style"] = "display:none";
            divAddDeleteDetail.Attributes["style"] = "display:block";
        }

        protected void btnDeleteDetail_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrDetail.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrDetail.Items[i].FindControl("deleteRecDetail");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrDetail.Items[i].Cells[1].Text;
                    //delete images?
                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = PollDetailController.deletePollDetail(gvIDs);

                if (result)
                {
                    loadAllDetail(hdfPollId.Value);
                    try
                    {
                        lblFrmResponseDetail.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseDetail.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseDetail.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseDetail.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseDetail.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseDetail.Visible = true;
                }
            }
            resetAddDetail();
        }

        protected void btnAddSaveDetail_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.hdfDetailId.Value != "")
            {
                rs = PollDetailController.updatePollDetail(new PollDetail
                {
                    id = int.Parse(hdfDetailId.Value),
                    name = txtDetailName.Text
                });
            }
            else
            {
                rs = PollDetailController.createPollDetail(new PollDetail
                {
                    name = txtDetailName.Text,
                    pollId = int.Parse(hdfPollId.Value)
                });
            }
            if (rs)
            {
                string id = this.hdfDetailId.Value == "" ? PollDetailController.getCurrentId() : hdfDetailId.Value;

                loadAllDetail(hdfPollId.Value);
                lblFrmResponseDetail.ForeColor = System.Drawing.Color.Green;

                if (this.hdfDetailId.Value == "")
                {
                    lblFrmResponseDetail.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseDetail.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseDetail.Visible = true;
            }
            else
            {
                lblFrmResponseDetail.ForeColor = System.Drawing.Color.Red;
                if (this.hdfDetailId.Value == "")
                {
                    lblFrmResponseDetail.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseDetail.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseDetail.Visible = true;
            }
            resetAddDetail();
        }
    }
}