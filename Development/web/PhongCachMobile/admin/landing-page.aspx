﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="landing-page.aspx.cs" Inherits="PhongCachMobile.admin.landing_page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <link href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#content_txtCDDateTime").datetimepicker({
                dateFormat: 'dd/mm/yy'
            });
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

            <!-- --------------- Block Start --------------- -->
            <div class="box">
                <div class="wrapper">
                    <!-- --------------- Left Pane --------------- -->
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litUpgrade" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseUpgrade" runat="server" visible="false">
                                        <p class="field">
                                            <asp:Label ID="lblFrmResponseUpgrade" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <div id="frmUpgrade" runat="server">
                                        <p class="field">
                                            <asp:Literal ID="litUpgradeActive" runat="server"></asp:Literal><small> *
                                            </small>
                                            <br />
                                                <asp:RadioButtonList ID="rblUpgardeActive" runat="server" 
                                                RepeatDirection="Horizontal" Width="100px">
                                                    <asp:ListItem Value="true">Mở</asp:ListItem>
                                                    <asp:ListItem Value="false">Tắt</asp:ListItem>
                                                </asp:RadioButtonList>
                                        </p>
                                        <p class="field">
                                            <asp:Literal ID="litUpgradeCountDown" runat="server"></asp:Literal><small> *
                                            </small>
                                            <asp:RequiredFieldValidator ID="reqValCDDateTime" runat="server" ControlToValidate="txtCDDateTime"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                  <asp:RegularExpressionValidator Display="Dynamic" ID="rgeValCDDateTime"
                                        ControlToValidate="txtCDDateTime"  ForeColor="Red" runat="server" 
                                                ValidationExpression="^([0-2]\d|[3][0-1])\/([0]\d|[1][0-2])\/([2][01]|[1][6-9])\d{2}(\s([0-1]\d|[2][0-3])(\:[0-5]\d){1,2})?$"></asp:RegularExpressionValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCDDateTime" runat="server" CssClass="wpcf7-text" Width="200px"
                                                    MaxLength="30"></asp:TextBox> </span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnUpgradeSave" runat="server" class="wpcf7-submit" OnClick="btnUpgradeSave_Click" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- --------------- Right Pane --------------- -->
                    <div class="two_fifth">
                        <div id="Div9" class="widget">
                            <h2>
                                <asp:Literal ID="litUpgradeHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div10" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litUpgradeHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- --------------- Block End --------------- -->

</asp:Content>
