﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using System.IO;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class tin_tuc : System.Web.UI.Page
    {
        string strLevel = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litNewsManager.Text", "Quản lý tin tức");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadCategory();
                loadAllArticle(ddlCategory.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadAllArticle(string categoryId)
        {
            string groupId = SysIniController.getSetting("Group_News_Id", "22");
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(-1, groupId, categoryId);    
       
            if (dt != null && dt.Rows.Count >0)
            {
                string imagePath = SysIniController.getSetting("News_ImagePath","images/news/");
                
                foreach (DataRow dr in dt.Rows)
                {
                    
                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"].ToString();
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                }
            }
            dgrArticle.DataSource = dt;
            dgrArticle.DataBind();
        }

        private void loadCategory()
        {
            string groupId = SysIniController.getSetting("Group_News_Id", "22");
            DataTable dt = CategoryController.getCategories(groupId, null);
            ddlCategory.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    
                    string id = dr["Id"].ToString();
                    string name = dr["Name"].ToString();

                    if (id != "59")
                    {
                        ddlCategory.Items.Add(new ListItem(name, id));
                        loadSubCategories(groupId, id);
                    }
                }
            }
            ddlCategory.DataBind();
        }

        private void loadSubCategories(string groupId, string parentId)
        {
            DataTable dt = CategoryController.getCategories(groupId, parentId);

            if (dt != null && dt.Rows.Count > 0)
            {
                strLevel += SysIniController.getSetting("Global_IndentLevel", "---");
                foreach (DataRow dr in dt.Rows)
                {
                    string id = dr["Id"].ToString();
                    string name = strLevel + dr["Name"].ToString();

                    ddlCategory.Items.Add(new ListItem(name, id));
                    loadSubCategories(groupId, id);
                }
                strLevel = strLevel.Substring(SysIniController.getSetting("Global_IndentLevel", "---").Length);
            }
        }

        private void loadLang()
        {
            this.litArticle.Text = LangController.getLng("litArticle.Text", "Bài viết");
            this.litAddArticleHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litAddArticle.Text = LangController.getLng("litAddArticle.Text", "Thêm / sửa bài viết");
            this.litAddArticleHelpDesc.Text = LangMsgController.getLngMsg("litAddArticleHelp.Desc", "Những thông tin giải thích về thêm / sửa bài viết");

            this.litCategoryChoosing.Text = LangController.getLng("litCategoryChoosing.Text", "Thể loại");
            this.btnAddArticle.Text = LangController.getLng("litAdd.Text", "Thêm");
            this.btnDeleteArticle.Text = LangController.getLng("litDelete.Text", "Xóa");
            this.btnAddSaveArticle.Text = LangController.getLng("litSave.Text", "Lưu");
            this.btnCancelArticle.Text = LangController.getLng("litCancel.Text", "Hủy");

            this.litViewCount.Text = LangController.getLng("litViewCount.Text", "Lượt xem");
            this.litTitle.Text = LangController.getLng("litTitle.Text", "Tiêu đề");
            this.litShortDesc.Text = LangController.getLng("litShortDesc.Text", "Nội dung ngắn");
            this.litLongDesc.Text = LangController.getLng("litLongDesc.Text", "Nội dung chi tiết");
            this.litDisplayImage.Text = LangController.getLng("litDisplayImage.Text", "Ảnh hiển thị");
        }

        protected void btnDeleteArticle_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrArticle.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrArticle.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrArticle.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = ArticleController.deleteArticle(gvIDs);
                if (result)
                {
                    loadAllArticle(ddlCategory.SelectedValue);
                    //Delete images
                    try
                    {
                        string imagePath = SysIniController.getSetting("News_ImagePath", "images/news/");

                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');
                        //loi chua delete hinh
                        foreach (FileInfo file in files)
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    string message = ex.Message;
                                }
                            }
                        }

                        lblFrmResponseArticle.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseArticle.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseArticle.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseArticle.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseArticle.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseArticle.Visible = true;
                }
            }
            resetAddArticle();
        }

        private void resetAddArticle()
        {
            txtTitle.Text = txtViewCount.Text = txtShortDesc.Text = txtLongDesc.Text = imgDisplay.ImageUrl = hplDisplayImg.NavigateUrl = "";
            if (ddlCategory.Items.Count > 0)
                ddlCategory.SelectedIndex = 0;
            hdfId.Value = "";
            divAddDelete.Attributes["style"] = "display:block";
            divAddArticle.Attributes["style"] = "display:none";
        }

        protected void btnAddSaveArticle_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = SysIniController.getSetting("News_ImagePath", "images/news/");
            string groupId = SysIniController.getSetting("Group_News_Id", "4");
            if (this.hdfId.Value != "")
            {
                if (fulDisplayImg.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgDisplay.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                    }
                }

                rs = ArticleController.updateArticle(new Article(int.Parse(hdfId.Value), txtTitle.Text, txtShortDesc.Text, txtLongDesc.Text, "", int.Parse(txtViewCount.Text), true,
                    DateTime.Now.ToString("yyyyMMddHHmmss"), int.Parse(groupId), int.Parse(ddlCategory.SelectedValue), -1));
            }
            else
            {
                rs = ArticleController.createArticle(new Article(txtTitle.Text, txtShortDesc.Text, txtLongDesc.Text, "", int.Parse(txtViewCount.Text), true,
                    DateTime.Now.ToString("yyyyMMddHHmmss"), int.Parse(groupId), int.Parse(ddlCategory.SelectedValue), -1));
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfId.Value == "" ? ArticleController.getCurrentId() : hdfId.Value;
                try
                {
                    if (fulDisplayImg.PostedFile.FileName != "")
                    {
                        fulDisplayImg.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulDisplayImg.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImg.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update ARTICLE set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                }

                loadAllArticle(ddlCategory.SelectedValue);
                lblFrmResponseArticle.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseArticle.Visible = true;
            }
            else
            {
                lblFrmResponseArticle.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseArticle.Visible = true;
            }
            resetAddArticle();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrArticle.CurrentPageIndex = 0;
            loadAllArticle(ddlCategory.SelectedValue);
            frmResponseArticle.Visible = false;
        }

        protected void dgrArticle_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllArticle(ddlCategory.SelectedValue);
            DataTable dt = (DataTable)dgrArticle.DataSource;
            DataRow dr = dt.Rows[iIndex];

            this.txtTitle.Text = dr["Title"].ToString();
            this.txtShortDesc.Text = dr["ShortDesc"].ToString();
            this.txtLongDesc.Text = dr["LongDesc"].ToString();
            this.txtViewCount.Text = dr["ViewCount"].ToString();
            this.imgDisplay.ImageUrl = dr["DisplayImage"].ToString();
            this.hplDisplayImg.NavigateUrl = dr["DisplayImage"].ToString();
            this.hdfId.Value = dr["Id"].ToString();
            divAddDelete.Attributes["style"] = "display:none";
            divAddArticle.Attributes["style"] = "display:block";
            frmResponseArticle.Visible = false;
        }

        protected void dgrArticle_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrArticle.CurrentPageIndex = e.NewPageIndex;
            loadAllArticle(ddlCategory.SelectedValue);
            frmResponseArticle.Visible = false;
            //Disable add menu
            resetAddArticle();
        }
    }
}