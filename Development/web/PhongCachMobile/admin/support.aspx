﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="support.aspx.cs" Inherits="PhongCachMobile.admin.support" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    function addSupport() {
        jQuery('#content_divAddSupport').show();
        jQuery('#content_divAddDelete').hide();
    }

    function cancelAddSaveSupport() {
        jQuery('#content_divAddSupport').hide();
        jQuery('#content_divAddDelete').show();
    }

    function check_uncheck(Val) {
        var ValChecked = Val.checked;
        var ValId = Val.id;
        var frm = document.forms[0];
        // Loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for Header Template's Checkbox
            //As we have not other control other than checkbox we just check following statement
            if (this != null) {
                if (ValId.indexOf('CheckAll') != -1) {
                    // Check if main checkbox is checked,
                    // then select or deselect datagrid checkboxes
                    //alert(frm.elements[i].id);
                    // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                    if (frm.elements[i].id.indexOf('deleteRec') != -1)
                        if (ValChecked)
                            frm.elements[i].checked = true;
                        else
                            frm.elements[i].checked = false;
                }
                else if (ValId.indexOf('deleteRec') != -1) {
                    //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                    // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                    document.getElementById('content_dgrSupport_CheckAll').checked = false;

                    if (frm.elements[i].checked == false)
                        frm.elements[1].checked = false;
                }
            } // if
        } // for
    } // function</PRE>

    function confirmMsg(frm) {
        // loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for our checkboxes only
            if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                // If any are checked then confirm alert, otherwise nothing happens
                if (frm.elements[i].checked)
                    return confirm('Bạn có chắc muốn xóa?')
            }
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litSupport" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseSupport" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseSupport" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmSupport" runat="server">
                                    <p class="field">
                                        <asp:Literal ID="litGroupChoosing" runat="server"> </asp:Literal>&nbsp; :<asp:DropDownList
                                            ID="ddlGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrSupport" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnPageIndexChanged="dgrSupport_PageIndexChanged"
                                        OnEditCommand="dgrSupport_EditCommand" PageSize="10" >
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="NAME"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="YahooMessenger" HeaderText="Yahoo ID">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Skype" HeaderText="Skype ID">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                             <asp:BoundColumn DataField="Phone" HeaderText="Phone">
                                              <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                    </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnAddSupport" runat="server" CssClass="wpcf7-submit" OnClientClick="return addSupport();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteSupport" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsg(this.form)" OnClick="btnDeleteSupport_Click"
                                            ClientIDMode="Static" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddSupport" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litAddSupport" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="frmResponseAddSupport" runat="server" visible="false">
                                        <p class="field" style="color: Red">
                                            <asp:Literal ID="litFrmResponseAddSupport" runat="server"></asp:Literal>
                                        </p>
                                    </div>
                                    <div id="Div1" runat="server">
                                     <p class="field">
                                         <asp:HiddenField ID="dhfId" runat="server" />
                                        </p>
                                        <p class="field">
                                            Tên<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValName" runat="server" ControlToValidate="txtName"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="30"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Skype ID<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValSkypeId" runat="server" ControlToValidate="txtSkype"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtSkype" runat="server" CssClass="wpcf7-text" Width="200px" MaxLength="30"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Yahoo Messenger ID<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValYM" runat="server" ControlToValidate="txtYM"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtYM" runat="server" CssClass="wpcf7-text" Width="200px" MaxLength="30"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Phone<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValPhone" runat="server" ControlToValidate="txtPhone"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtPhone" runat="server" CssClass="wpcf7-text" Width="300px" MaxLength="30"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Nhóm<small> * </small>
                                            <br />
                                            <asp:DropDownList ID="ddlAnotherGroup" runat="server">
                                            </asp:DropDownList>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSupportSave" runat="server" class="wpcf7-submit" OnClick="btnAddSupportSave_Click" />
                                            <asp:Button ID="btnCancel" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSaveSupport();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
