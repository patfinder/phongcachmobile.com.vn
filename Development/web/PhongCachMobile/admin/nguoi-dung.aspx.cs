﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.model;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class nguoi_dung : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLang();
                loadState();
                loadRole();
                loadOn();
                divAddDelete.Visible = false;
            }
            this.Title = LangController.getLng("litUserManager.Text", "Quản lý người dùng");
        }

        private void loadRole()
        {
            DataTable dt = GroupController.getGroupsByFilter("USER_ROLE");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlRole.DataSource = dt;
                ddlRole.DataTextField = "Name";
                ddlRole.DataValueField = "Id";
                ddlRole.DataBind();
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        protected void dgrUser_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllUser(txtText.Text, ddlOn.SelectedValue);

            DataTable dt = (DataTable)dgrUser.DataSource;
            DataRow dr = dt.Rows[iIndex];

            //Personal info
            txtAlias.Text = dr["Alias"].ToString();
            txtFirstName.Text = dr["FirstName"].ToString();
            txtLastName.Text = dr["LastName"].ToString();
            string dateOfBirth = dr["DOB"].ToString();
            try
            {
                if (dateOfBirth != "")
                    txtDOB.Text = Common.parseDate(dateOfBirth, "yyyyMMddHHmmss").ToShortDateString();
            }
            catch
            {
            }
            if (dr["Gender"].ToString() == "True")
            {
                rbtMale.Checked = true;
                rbtFemale.Checked = false;
            }
            else
            {
                rbtMale.Checked = false;
                rbtFemale.Checked = true;
            }

            if (dr["Flag"].ToString() == "True")
            {
                rbtEnableYes.Checked = true;
                rbtEnableNo.Checked = false;
            }
            else
            {
                rbtEnableYes.Checked = false;
                rbtEnableNo.Checked = true;
            }

            ddlRole.SelectedValue = dr["GroupId"].ToString();

            // Contact info
            txtAddress.Text = dr["Address"].ToString();
            txtEmail.Text = dr["Email"].ToString();
            txtMobile.Text = dr["Mobile"].ToString();
            txtTel.Text = dr["Tel"].ToString();
            ddlState.SelectedValue = dr["StateId"].ToString();

            // Budget info
            txtId.Text = dr["Id"].ToString();
            //txtIdentityName.Text = dt.Rows[0]["IdentityName"].ToString();
            //txtIdentityNumber.Text = dt.Rows[0]["IdentityNumber"].ToString();

            //txtBudget.Text = dt.Rows[0]["Budget"].ToString();
            //txtPoint.Text = dt.Rows[0]["Point"].ToString();

            divAddDelete.Attributes["style"] = "display:none";
            divAddUser.Attributes["style"] = "display:block";
            frmResponseUser.Visible = false;
        }

        private void loadAllUser(string text, string sOn)
        {

            DataTable dt = null;
            if (sOn == "0")
            {
                dt = SysUserController.getUserById(text);
            }
            else if (sOn == "1")
            {
                dt = SysUserController.getUserByPrefixEmail(text);
            }
            else // "2"
            {
                dt =  SysUserController.getUserByAlias(text);
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("Name", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["Name"] = dr["FirstName"].ToString() + " " +  dr["LastName"].ToString();
                }
                dgrUser.DataSource = dt;
                dgrUser.DataBind();
            }

        }


        protected void dgrUser_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrUser.CurrentPageIndex = e.NewPageIndex;
            loadAllUser(txtText.Text, ddlOn.SelectedValue);
            frmResponseUser.Visible = false;
            
        }

        private void loadOn()
        {
            ddlOn.Items.Clear();
            ddlOn.Items.Add(new ListItem("Mã tài khoản", "0"));
            ddlOn.Items.Add(new ListItem("Email", "1"));
            ddlOn.Items.Add(new ListItem("Tên hiển thị", "2"));
            ddlOn.DataBind();
        }

        private void loadState()
        {
            DataTable dt = StateController.getStates();
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlState.Items.Clear();
                ddlState.DataSource = dt;
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "Id";
                ddlState.DataBind();
            }
        }

        protected void btnDisable_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrUser.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrUser.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrUser.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = SysUserController.disableUser(gvIDs);
                loadAllUser(txtText.Text, ddlOn.SelectedValue);

                if (result)
                {
                    try
                    {
                        lblFrmResponseUser.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseUser.Text = LangController.getLng("litDisableOk.Text", "Vô hiệu hóa thành công");
                        frmResponseUser.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseUser.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseUser.Text = LangController.getLng("litDisableError.Text", "Vô hiệu hóa thất bại");
                    frmResponseUser.Visible = true;
                }
            }
            //resetAddUser();
        }

        //private void loadUserInfo()
        //{
        //    if (Session["Email"] != null && Session["Email"].ToString() != "")
        //    {
        //        DataTable dt = SysUserController.getUserByEmail(Session["Email"].ToString());

        //        // Personal info
        //        txtAlias.Text = dt.Rows[0]["Alias"].ToString();
        //        txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
        //        txtLastName.Text = dt.Rows[0]["LastName"].ToString();
        //        string dateOfBirth = dt.Rows[0]["DOB"].ToString();
        //        try
        //        {
        //            if (dateOfBirth != "")
        //                txtDOB.Text = Common.parseDate(dateOfBirth, "yyyyMMddHHmmss").ToShortDateString();
        //        }
        //        catch
        //        {
        //        }
        //        if (dt.Rows[0]["Gender"].ToString() == "False")
        //            rblSex.SelectedIndex = 1;
        //        else
        //            rblSex.SelectedIndex = 0;

        //        // Contact info
        //        txtAddress.Text = dt.Rows[0]["Address"].ToString();
        //        txtEmail.Text = dt.Rows[0]["Email"].ToString();
        //        txtMobile.Text = dt.Rows[0]["Mobile"].ToString();
        //        txtTel.Text = dt.Rows[0]["Tel"].ToString();
        //        ddlState.SelectedValue = dt.Rows[0]["StateId"].ToString();

        //        // Certified info
        //        txtId.Text = dt.Rows[0]["Id"].ToString();
        //        txtIdentityName.Text = dt.Rows[0]["IdentityName"].ToString();
        //        txtIdentityNumber.Text = dt.Rows[0]["IdentityNumber"].ToString();

        //        checkStatusCertifiedInfo();
        //    }


        //}

        protected void lbtFind_Click(object sender, EventArgs e)
        {
            loadAllUser(txtText.Text, ddlOn.SelectedValue);
            divAddDelete.Visible = true;
        }

        private void loadLang()
        {
            // Personal info
            this.litPersonalInfo.Text = LangController.getLng("litPersonalInfo.Text", "Thông tin cá nhân");

            this.litAlias.Text = LangController.getLng("litAlias.Text", "Tên hiển thị");
            this.litFirstName.Text = LangController.getLng("litFirstName.Text", "Tên");
            this.litLastName.Text = LangController.getLng("litLastName.Text", "Họ");
            this.litDOB.Text = LangController.getLng("litDOB.Text", "Ngày sinh");
            this.litGender.Text = LangController.getLng("litGender.Text", "Giới tính");

            // Contact info
            this.litContactInfo.Text = LangController.getLng("litContactInfo.Text", "Thông tin liên hệ");
            this.litAddress.Text = LangController.getLng("litAddress.Text", "Địa chỉ");
            this.litEmail.Text = LangController.getLng("litEmail.Text", "Email");
            this.litMobile.Text = LangController.getLng("litMobile.Text", "Mobile");
            this.litTel.Text = LangController.getLng("litTel.Text", "Tel");
            this.litState.Text = LangController.getLng("litState.Text", "Tỉnh / Thành phố");

            // Certified info
            //this.litCertifiedInfo.Text = LangController.getLng("litCertifiedInfo.Text", "Thông tin chứng thực");
            //this.litId.Text = LangController.getLng("litId.Text", "Mã tài khoản");
            //this.litIdDesc.Text = LangController.getLng("litIdDesc.Text", "Sử dụng để nạp xu qua SMS");
            //this.litIdentityName.Text = LangController.getLng("litIdentityName.Text", "Tên trên CMND");
            //this.litIdentityNumber.Text = LangController.getLng("litIdentityNumber.Text", "Số CMND");

            this.btnAddUserSave.Text = LangController.getLng("litSave.Text", "Lưu");
            this.litOn.Text = LangController.getLng("litOn.Text", "theo");
            this.litUser.Text = LangController.getLng("litUser.Text", "Người dùng");
            this.litText.Text = LangController.getLng("litText.Text", "Text");

            this.litEnable.Text = LangController.getLng("litIsAllowable.Text", "Cho phép?");
            lbtFind.Text = LangController.getLng("litSearch.Text", "Tìm kiếm");
            btnCancel.Text = LangController.getLng("litCancel.Text", "Hủy");
            btnDisable.Text = LangController.getLng("litDiable.Text", "Vô hiệu");

            //Budget info
            //this.litBudgetInfo.Text = LangController.getLng("litBudgetInfo.Text", "Thông tin xu");
            //this.litBudget.Text = LangController.getLng("litBudget.Text", "Xu");
            //this.litPoint.Text = LangController.getLng("litPoint.Text", "Điểm tích lũy");

            this.litRole.Text = LangController.getLng("litRole.Text", "Vai trò");

        }

        protected void btnAddUserSave_Click(object sender, EventArgs e)
        {
            SysUser user = new SysUser(txtAlias.Text, txtFirstName.Text, txtLastName.Text, rbtMale.Checked,
                txtAddress.Text, txtDOB.Text == "" ? "" : Common.parseDate(txtDOB.Text, "dd/MM/yyyy").ToString("yyyyMMddHHmmss"), txtEmail.Text, 
                txtMobile.Text, txtTel.Text, ddlState.SelectedValue, rbtEnableYes.Checked, 
                int.Parse(ddlRole.SelectedValue));
            user.id = int.Parse(txtId.Text);

            bool result = SysUserController.updateUser(user);

            if (result)
            {
                lblFrmResponseUser.ForeColor = System.Drawing.Color.Green;
                lblFrmResponseUser.Text = LangController.getLng("litSave.Ok", "Lưu thành công");
                frmResponseUser.Visible = true;
            }
            else
            {
                lblFrmResponseUser.ForeColor = System.Drawing.Color.Red;
                lblFrmResponseUser.Text = LangController.getLng("litSave.Error", "Có lỗi xảy ra. Bạn hãy thử lại sau.");
                frmResponseUser.Visible = true;
            }

        }
    }
}