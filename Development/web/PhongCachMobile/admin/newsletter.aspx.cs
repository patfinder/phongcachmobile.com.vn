﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Net.Mail;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class newsletter : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "E-Newsletter";
            if (!this.IsPostBack)
            {
                loadActivatedUser();
                loadContent();
            }
        }

        private void loadContent()
        {
              DataTable dt = ProductController.getDiscountProducts();
              if (dt != null && dt.Rows.Count > 3)
              {
                  //load enewsletter
                  using (StreamReader reader = File.OpenText(Server.MapPath("../enewsletter/index.html"))) // Path to your 
                  {
                      string body = reader.ReadToEnd();  // Load the content from your file...
                      body = body.Replace("{0}", txtTitle.Text);
                      body = body.Replace("{1}", txtTitle.Text);
                      body = body.Replace("{2}", "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[0]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[0]["Id"].ToString());
                      body = body.Replace("{3}", "http://phongcachmobile.com.vn/images/product/" + dt.Rows[0]["FirstImage"].ToString()); // 3 - link hinh sp 1
                      body = body.Replace("{4}", "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[0]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[0]["Id"].ToString()); // 4 - link sp 1
                      body = body.Replace("{5}", dt.Rows[0]["Name"].ToString()); //5 - title sp 1
                      body = body.Replace("{6}", dt.Rows[0]["HotDesc"].ToString() + dt.Rows[0]["DiscountDesc"].ToString()); //6 - hot desc sp 1
                      body = body.Replace("{7}", int.Parse(dt.Rows[0]["DiscountPrice"].ToString() == "" ? dt.Rows[0]["CurrentPrice"].ToString() : dt.Rows[0]["DiscountPrice"].ToString()).ToString("#,##0") + " đ"); //7 - price sp 1
                      body = body.Replace("{8}", "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[0]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[0]["Id"].ToString()); // 8 - link sp 1
                      body = body.Replace("{9}", "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[1]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[1]["Id"].ToString()); // 9 - link sp 2
                      body = body.Replace("{10}", dt.Rows[1]["Name"].ToString()); //10 - title sp 2
                      body = body.Replace("{11}", dt.Rows[1]["HotDesc"].ToString() + dt.Rows[1]["DiscountDesc"].ToString()); //11 - hot desc sp 2
                      body = body.Replace("{12}", int.Parse(dt.Rows[1]["DiscountPrice"].ToString() == "" ? dt.Rows[1]["CurrentPrice"].ToString() : dt.Rows[1]["DiscountPrice"].ToString()).ToString("#,##0") + " đ"); //12 - price sp 2
                      body = body.Replace("{13}", "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[1]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[1]["Id"].ToString()); // 13 - link sp 2
                      body = body.Replace("{14}", "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[1]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[1]["Id"].ToString()); // 14 - link sp 2
                      body = body.Replace("{15}", "http://phongcachmobile.com.vn/images/product/" + dt.Rows[1]["FirstImage"].ToString()); // 15 - link hinh sp 2
                      string bottomPrd = "";
                      for (int i = 2; i < dt.Rows.Count; i++)
                      {
                          if ((i - 2) % 3 == 0)
                              bottomPrd +=
                          "<tr>" +
                              "<td width='650' bgcolor='#FFFFFF'>" +
                                  "<table cellpadding='0' cellspacing='0' border='0' width='650'>" +
                                      "<tbody>" +
                                          "<tr>" +
                                              "<td width='15'>" +
                                              "</td>" +
                                              "<td width='620'>" +
                                                  "<table cellpadding='0' cellspacing='0' border='0' width='620'>" +
                                                      "<tbody>" +
                                                          "<tr>" +
                                                              "<td width='10'>" +
                                                              "</td>" +
                                                              "<!-- Content Bottom Product 3 START -->";

                          if ((i - 2) % 3 == 0)
                              bottomPrd +=
                                                              "<td width='190' align='center' valign='top'>" +
                                                                  "<table cellpadding='0' cellspacing='0' border='0' width='190'>" +
                                                                      "<tbody>" +
                                                                          "<tr>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-top.png'" +
                                                                                      " alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' style='border-top: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-top.png'" +
                                                                                      " alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='5' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product Info 3 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;height:30px'>" +
                                                                                  "<a href='" + "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[i]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[i]["Id"].ToString() + "' style='font-family: Tahoma, Arial, sans-serif; font-size: 11px; color: #313131;" +
                                                                                      "font-weight: 900; text-decoration: none;'>" + dt.Rows[i]["Name"].ToString() + "</a>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product Info 3 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='15' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product IMG 3 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;'>" +
                                                                                  "<a href='" + "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[i]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[i]["Id"].ToString() + "'>" +
                                                                                      "<img width='170' src='" + "http://phongcachmobile.com.vn/images/product/" + dt.Rows[i]["FirstImage"].ToString() + "' " +
                                                                                          "alt='' border='0' style='display: block;'></a>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product IMG 3 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='15' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Top Product Price 3 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;'>" +
                                                                                  "<span style='font-family: Tahoma, Arial, sans-serif; font-size: 13px; color: #ff3708;" +
                                                                                      "font-weight: 900; text-decoration: none;'>" + int.Parse(dt.Rows[i]["DiscountPrice"].ToString() == "" ? dt.Rows[i]["CurrentPrice"].ToString() : dt.Rows[i]["DiscountPrice"].ToString()).ToString("#,##0") + " đ" + "</span>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Top Product Price 3 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='5' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<tr>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-bottom.png' " +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' style='border-bottom: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-bottom.png' " +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                      "</tbody>" +
                                                                  "</table>" +
                                                              "</td>" +
                                                              "<!-- Content Bottom Product 3 END -->";

                          if ((i - 2) % 3 == 1)
                              bottomPrd +=

                                                              "<td width='15'>" +
                                                              "</td>" +
                                                              "<!-- Content Bottom Product 4 START -->" +
                                                              "<td width='190' align='center' valign='top'>" +
                                                                  "<table cellpadding='0' cellspacing='0' border='0' width='190'>" +
                                                                      "<tbody>" +
                                                                          "<tr>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-top.png' " +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' style='border-top: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-top.png'" +
                                                                                     " alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                         "</tr>" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='5' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product Info 4 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;height:30px'>" +
                                                                                  "<a href='" + "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[i]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[i]["Id"].ToString() + "' style='font-family: Tahoma, Arial, sans-serif; font-size: 11px; color: #313131;" +
                                                                                      "font-weight: 900; text-decoration: none;'>" + dt.Rows[i]["Name"].ToString() + "</a>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product Info 4 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='15' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product IMG 4 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;'>" +
                                                                                  "<a href='" + "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[i]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[i]["Id"].ToString() + "'>" +
                                                                                      "<img width='170' src='" + "http://phongcachmobile.com.vn/images/product/" + dt.Rows[i]["FirstImage"].ToString() + "' " +
                                                                                          "alt='' border='0' style='display: block;'></a>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product IMG 4 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='15' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Top Product Price 4 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;'>" +
                                                                                  "<span style='font-family: Tahoma, Arial, sans-serif; font-size: 13px; color: #ff3708;" +
                                                                                       "font-weight: 900; text-decoration: none;'>" + int.Parse(dt.Rows[i]["DiscountPrice"].ToString() == "" ? dt.Rows[i]["CurrentPrice"].ToString() : dt.Rows[i]["DiscountPrice"].ToString()).ToString("#,##0") + " đ" + "</span>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Top Product Price 4 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='5' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<tr>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-bottom.png'" +
                                                                                     " alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' style='border-bottom: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-bottom.png'" +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                      "</tbody>" +
                                                                  "</table>" +
                                                              "</td>" +
                                                              "<!-- Content Bottom Product 4 END -->";
                          if ((i - 2) % 3 == 2)
                              bottomPrd +=
                                                              "<td width='15'>" +
                                                              "</td>" +
                                                              "<!-- Content Bottom Product 5 START -->" +
                                                              "<td width='190' align='center' valign='top'>" +
                                                                  "<table cellpadding='0' cellspacing='0' border='0' width='190'>" +
                                                                      "<tbody>" +
                                                                          "<tr>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-top.png' " +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' style='border-top: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-top.png'" +
                                                                                      " alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              " </td>" +
                                                                              "<td width='10' height='5' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product Info 5 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                             "<td width='170' align='center' style='text-align: center;height:30px'>" +
                                                                                  "<a href='" + "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[i]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[i]["Id"].ToString() + "' style='font-family: Tahoma, Arial, sans-serif; font-size: 11px; color: #313131;" +
                                                                                      "font-weight: 900; text-decoration: none;'>" + dt.Rows[i]["Name"].ToString() + "</a>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product Info 5 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='15' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product IMG 5 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;'>" +
                                                                                  "<a href='" + "http://phongcachmobile.com.vn/sp-" + HTMLRemoval.StripTagsRegex(dt.Rows[i]["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dt.Rows[i]["Id"].ToString() + "'>" +
                                                                                      "<img width='170' src='" + "http://phongcachmobile.com.vn/images/product/" + dt.Rows[i]["FirstImage"].ToString() + "' " +
                                                                                          "alt='' border='0' style='display: block;'></a>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Bottom Product IMG 5 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='15' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Top Product Price 5 START -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' align='center' style='text-align: center;'>" +
                                                                                  "<span style='font-family: Tahoma, Arial, sans-serif; font-size: 13px; color: #ff3708;" +
                                                                                       "font-weight: 900; text-decoration: none;'>" + int.Parse(dt.Rows[i]["DiscountPrice"].ToString() == "" ? dt.Rows[i]["CurrentPrice"].ToString() : dt.Rows[i]["DiscountPrice"].ToString()).ToString("#,##0") + " đ" + "</span>" +
                                                                              "</td>" +
                                                                              "<td width='10' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<!-- Content Top Product Price 5 END -->" +
                                                                          "<tr>" +
                                                                              "<td width='10' style='border-left: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='170'>" +
                                                                              "</td>" +
                                                                              "<td width='10' height='5' style='border-right: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                          "<tr>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-bottom.png' " +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                              "<td width='170' style='border-bottom: #DEDEDE solid 1px;'>" +
                                                                              "</td>" +
                                                                              "<td width='10'>" +
                                                                                  "<img src='http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-bottom.png' " +
                                                                                      "alt='' style='display: block;'>" +
                                                                              "</td>" +
                                                                          "</tr>" +
                                                                      "</tbody>" +
                                                                  "</table>" +
                                                              "</td>" +
                                                              "<!-- Content Bottom Product 5 END -->";

                          if ((i - 2) % 3 == 2 || i + 1 == dt.Rows.Count)
                              bottomPrd +=
                                                              "<td width='10'>" +
                                                              "</td>" +
                                                          "</tr>" +
                                                      "</tbody>" +
                                                  "</table>" +
                                              "</td>" +
                                              "<td width='15'>" +
                                              "</td>" +
                                          "</tr>" +
                                      "</tbody>" +
                                  "</table>" +
                              "</td>" +
                          "</tr>" +
                          "<tr>" +
                          "<td width='650' height='15' bgcolor='#FFFFFF'>" +
                          "</td>" +
                          "</tr>";
                      }
                     txtContent.Text = body = body.Replace("{16}", bottomPrd);
                  }
              }
        }

        private void loadActivatedUser()
        {
            DataTable dt = SysUserController.getActivatedUser();
            string bcc = "";

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    bcc += dr["Email"].ToString() + ",";
                }
            }
            if (bcc.Length > 0)
                txtBcc.Text = bcc.Substring(0, bcc.Length - 1);
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            DataTable dt = ProductController.getDiscountProducts();
            if (dt != null && dt.Rows.Count > 3)
            {
                //load enewsletter
                using (StreamReader reader = File.OpenText(Server.MapPath("../enewsletter/index.html"))) // Path to your 
                {                                                         // HTML file
                    string error = "";
                    bool result = false;
                    bool isError = false;
                    string[] emails = txtBcc.Text.Split(',');
                    foreach (string email in emails)
                    {
                        result = Common.sendEmail(email, "", "", "", txtTitle.Text, txtContent.Text, "");
                        if (!result)
                        {
                            isError = true;
                            error += email + ", ";
                        }
                    }
                    if (!isError)
                    {
                        lblFrmResponseNewsletter.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseNewsletter.Text = "Gửi email thành công";
                        frmResponseNewsletter.Visible = true;
                    }
                    else
                    {
                        lblFrmResponseNewsletter.ForeColor = System.Drawing.Color.Red;
                        if (error.Length > 2)
                            lblFrmResponseNewsletter.Text = "Lỗi xảy ra trong quá trình gửi e-newsletters. Không thể gửi: " + error;
                        else
                            lblFrmResponseNewsletter.Text = "Lỗi xảy ra trong quá trình gửi e-newsletters.";
                        frmResponseNewsletter.Visible = true;
                    }
                }
            }
            else
            {
                lblFrmResponseNewsletter.ForeColor = System.Drawing.Color.Red;
                lblFrmResponseNewsletter.Text = "Ít nhất 3 sản phẩm giá hot";
                frmResponseNewsletter.Visible = true;
            }
        }

        //<tr>
        //    <td width="650" height="15" bgcolor="#FFFFFF">
        //    </td>
        //</tr>


        //<tr>
        //    <td width="650" bgcolor="#FFFFFF">
        //        <table cellpadding="0" cellspacing="0" border="0" width="650">
        //            <tbody>
        //                <tr>
        //                    <td width="15">
        //                    </td>
        //                    <td width="620">
        //                        <table cellpadding="0" cellspacing="0" border="0" width="620">
        //                            <tbody>
        //                                <tr>
        //                                    <td width="10">
        //                                    </td>
        //                                    <!-- Content Bottom Product 3 START -->
        //                                    <td width="190" align="center" valign="top">
        //                                        <table cellpadding="0" cellspacing="0" border="0" width="190">
        //                                            <tbody>
        //                                                <tr>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-top.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                    <td width="170" style="border-top: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-top.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="5" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product Info 3 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <a href="#" style="font-family: Tahoma, Arial, sans-serif; font-size: 11px; color: #313131;
        //                                                            font-weight: 900; text-decoration: none;">Lorem ipsum dolor sit ametLorem ipsum
        //                                                            dolor</a>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product Info 3 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="15" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product IMG 3 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <a href="#">
        //                                                            <img src="http://advolocaru.com/themeforest/enewsletter/images/product-img-3.png"
        //                                                                alt="" border="0" style="display: block;"></a>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product IMG 3 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="15" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Top Product Price 3 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <span style="font-family: Tahoma, Arial, sans-serif; font-size: 13px; color: #ff3708;
        //                                                            font-weight: 900; text-decoration: none;">5400.95 Euro</span>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Top Product Price 3 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="5" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-bottom.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                    <td width="170" style="border-bottom: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-bottom.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                </tr>
        //                                            </tbody>
        //                                        </table>
        //                                    </td>
        //                                    <!-- Content Bottom Product 3 END -->
        //                                    <td width="15">
        //                                    </td>
        //                                    <!-- Content Bottom Product 4 START -->
        //                                    <td width="190" align="center" valign="top">
        //                                        <table cellpadding="0" cellspacing="0" border="0" width="190">
        //                                            <tbody>
        //                                                <tr>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-top.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                    <td width="170" style="border-top: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-top.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="5" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product Info 4 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <a href="#" style="font-family: Tahoma, Arial, sans-serif; font-size: 11px; color: #313131;
        //                                                            font-weight: 900; text-decoration: none;">Lorem ipsum dolor sit ametLorem ipsum
        //                                                            dolor</a>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product Info 4 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="15" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product IMG 4 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <a href="#">
        //                                                            <img src="http://advolocaru.com/themeforest/enewsletter/images/product-img-4.png"
        //                                                                alt="" border="0" style="display: block;"></a>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product IMG 4 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="15" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Top Product Price 4 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <span style="font-family: Tahoma, Arial, sans-serif; font-size: 13px; color: #ff3708;
        //                                                            font-weight: 900; text-decoration: none;">5400.95 Euro</span>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Top Product Price 4 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="5" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-bottom.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                    <td width="170" style="border-bottom: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-bottom.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                </tr>
        //                                            </tbody>
        //                                        </table>
        //                                    </td>
        //                                    <!-- Content Bottom Product 4 END -->
        //                                    <td width="15">
        //                                    </td>
        //                                    <!-- Content Bottom Product 5 START -->
        //                                    <td width="190" align="center" valign="top">
        //                                        <table cellpadding="0" cellspacing="0" border="0" width="190">
        //                                            <tbody>
        //                                                <tr>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-top.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                    <td width="170" style="border-top: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-top.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="5" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product Info 5 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <a href="#" style="font-family: Tahoma, Arial, sans-serif; font-size: 11px; color: #313131;
        //                                                            font-weight: 900; text-decoration: none;">Lorem ipsum dolor sit ametLorem ipsum
        //                                                            dolor</a>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product Info 5 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="15" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product IMG 5 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <a href="#">
        //                                                            <img src="http://advolocaru.com/themeforest/enewsletter/images/product-img-5.png"
        //                                                                alt="" border="0" style="display: block;"></a>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Bottom Product IMG 5 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="15" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Top Product Price 5 START -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170" align="center" style="text-align: center;">
        //                                                        <span style="font-family: Tahoma, Arial, sans-serif; font-size: 13px; color: #ff3708;
        //                                                            font-weight: 900; text-decoration: none;">5400.95 Euro</span>
        //                                                    </td>
        //                                                    <td width="10" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <!-- Content Top Product Price 5 END -->
        //                                                <tr>
        //                                                    <td width="10" style="border-left: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="170">
        //                                                    </td>
        //                                                    <td width="10" height="5" style="border-right: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                </tr>
        //                                                <tr>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-left-bottom.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                    <td width="170" style="border-bottom: #DEDEDE solid 1px;">
        //                                                    </td>
        //                                                    <td width="10">
        //                                                        <img src="http://phongcachmobile.com.vn/images/enewsletter/black-white/corner-product-bottom-right-bottom.png"
        //                                                            alt="" style="display: block;">
        //                                                    </td>
        //                                                </tr>
        //                                            </tbody>
        //                                        </table>
        //                                    </td>
        //                                    <!-- Content Bottom Product 5 END -->
        //                                    <td width="10">
        //                                    </td>
        //                                </tr>
        //                            </tbody>
        //                        </table>
        //                    </td>
        //                    <td width="15">
        //                    </td>
        //                </tr>
        //            </tbody>
        //        </table>
        //    </td>
        //</tr>
    }
}