﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile.admin
{
    public partial class sitemap : System.Web.UI.Page
    {
        private const string xmlFormatString = @"<?xml version=""1.0"" encoding=""UTF-8""?>";
        //private const string xmlStylesheetString = @"<?xml-stylesheet type=""text/xsl"" href=""http://www.intelligiblebabble.com/files/style.xsl""?>";
        private const string xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
        private const string xmlns_xsi = "http://www.w3.org/2001/XMLSchema-instance";
        private const string xsi_schemaLocation = "http://www.sitemaps.org/schemas/sitemap/0.9\nhttp://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd";

        //Product
        private const string productBaseURL = "http://phongcachmobile.com.vn/sp-";
        private const string dtddCategoryBaseURL = "http://phongcachmobile.com.vn/dtdd-";
        private const string mtbCategoryBaseURL = "http://phongcachmobile.com.vn/mtb-";
        private const string pkdtddCategoryBaseURL = "http://phongcachmobile.com.vn/pkdtdd-";
        private const string pkmtbCategoryBaseURL = "http://phongcachmobile.com.vn/pkmtb-";

        //Application & Game
        private const string appCategoryBaseURL = "http://phongcachmobile.com.vn/ud-";
        private const string gameCategoryBaseURL = "http://phongcachmobile.com.vn/game-";

        //News & Guides
        private const string newsURL = "http://phongcachmobile.com.vn/tt-";
        private const string guidesURL = "http://phongcachmobile.com.vn/tt-";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "Sitemap";
        }

        private void WriteTag(string Priority, string freq, string Navigation, string mod, XmlWriter MyWriter)
        {
            MyWriter.WriteStartElement("url");

            MyWriter.WriteStartElement("loc");
            MyWriter.WriteValue(Navigation);
            MyWriter.WriteEndElement();

            MyWriter.WriteStartElement("lastmod");
            //DateTime time = DateTime.Now;
            //string format = "yyyy-MM-ddThh:mm:ss+00:00";
            MyWriter.WriteValue(mod);
            MyWriter.WriteEndElement();

            MyWriter.WriteStartElement("changefreq");
            MyWriter.WriteValue(freq);
            MyWriter.WriteEndElement();

            MyWriter.WriteStartElement("priority");
            MyWriter.WriteValue(Priority);
            MyWriter.WriteEndElement();

            MyWriter.WriteEndElement();
        }

        protected void CreateXML(string path, string rootUrl, DataTable dt, string priority, string freq)
        {
            XmlWriter writer = null;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            writer = XmlWriter.Create(Server.MapPath("~/xml/" + path + ".xml"));
            //writer.WriteStartDocument();
            writer.WriteStartElement("urlset", "http://www.sitemaps.org/schemas/sitemap/0.9");

            writer.WriteAttributeString("xmlns", "xsi", null, xmlns_xsi);
            writer.WriteAttributeString("xsi", "schemaLocation", null, xsi_schemaLocation);

            Random rnd = new Random();
            int ss = 0;
            int addSS = 0;

            string format = "yyyy-MM-ddThh:mm:ss+00:00";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ss = rnd.Next(1, 1000);
                addSS = addSS + ss;
                DateTime time = DateTime.Now;
                string modify = time.AddSeconds(addSS).ToString(format);
                string url = "";
                url = rootUrl + dt.Rows[i]["Url"].ToString().ToLower().Replace(' ', '-');
                WriteTag(priority, freq, url, modify, writer);
            }

            // writer.WriteEndDocument();

            writer.Close();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            // Product
            DataTable dtProduct = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from PRODUCT;");
            CreateXML("product", productBaseURL, dtProduct, "0.2", "Monthly");
            lblFrmResponseText.Text = "SẢN PHẨM" + "<br/>" + "http://phongcachmobile.com.vn/xml/product.xml";

            DataTable dtDtddCategory = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from CATEGORY where GroupId = 3;");
            CreateXML("dtddCategory", dtddCategoryBaseURL, dtDtddCategory, "0.2", "Daily");
            lblFrmResponseText.Text += "<br/>" + "http://phongcachmobile.com.vn/xml/dtddCategory.xml";

            DataTable dtMtbCategory = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from CATEGORY where GroupId = 4;");
            CreateXML("mtbCategory", mtbCategoryBaseURL, dtMtbCategory, "0.3", "Monthly");
            lblFrmResponseText.Text += "<br/>" + "http://phongcachmobile.com.vn/xml/mtbCategory.xml";

            DataTable dtPkdtddCategory = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from CATEGORY where GroupId = 7;");
            CreateXML("pkdtddCategory", pkdtddCategoryBaseURL, dtPkdtddCategory, "0.3", "Monthly");
            lblFrmResponseText.Text += "<br/>" + "http://phongcachmobile.com.vn/xml/pkdtddCategory.xml";

            DataTable dtPkmtbCategory = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from CATEGORY where GroupId = 8;");
            CreateXML("pkmtbCategory", pkmtbCategoryBaseURL, dtPkmtbCategory, "0.3", "Monthly");
            lblFrmResponseText.Text += "<br/>" + "http://phongcachmobile.com.vn/xml/pkmtbCategory.xml";
            lblFrmResponseText.Text += "<br/>";


            // Application & Game
            DataTable dtAppCategory = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from CATEGORY where GroupId = 6;");
            CreateXML("appCategory", appCategoryBaseURL, dtAppCategory, "0.2", "Monthly");
            lblFrmResponseText.Text += "ỨNG DỤNG & GAME" + "<br/>" + "http://phongcachmobile.com.vn/xml/appCategory.xml";

            DataTable dtGameCategory = DB.getData("select name + '-' + CONVERT(varchar(10), id) as Url  from CATEGORY where GroupId = 5;");
            CreateXML("gameCategory", gameCategoryBaseURL, dtGameCategory, "0.4", "Monthly");
            lblFrmResponseText.Text += "<br/>" + "http://phongcachmobile.com.vn/xml/gameCategory.xml";
            lblFrmResponseText.Text += "<br/>";


            // News & Guides
            DataTable dtNewsUrl = DB.getData("select title + '-' + CONVERT(varchar(10), id) as Url  from ARTICLE where ProductId is null;");
            CreateXML("news", newsURL, dtNewsUrl, "0.2", "Monthly");
            lblFrmResponseText.Text += "TIN TỨC VÀ TƯ VẤN SỬ DỤNG" + "<br/>" + "http://phongcachmobile.com.vn/xml/news.xml";

            DataTable dtGuidesURL = DB.getData("select title + '-' + CONVERT(varchar(10), id) as Url  from ARTICLE where ProductId is not null;");
            CreateXML("guides", guidesURL, dtGuidesURL, "0.4", "Monthly");
            lblFrmResponseText.Text += "<br/>" + "http://phongcachmobile.com.vn/xml/guides.xml" + "<br/>";

            frmResponseText.Visible = true;

        }
    }
}