﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using PhongCachMobile.controller;

namespace PhongCachMobile.admin
{
    public partial class upload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadImage();
            }
        }

        private void loadImage()
        {
            string strImgCache = Server.MapPath("../images/misc/");
            string[] staImgEntry = Directory.GetFiles(strImgCache, "*.*");
            if (staImgEntry.Length > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("name", typeof(string));
                dt.Columns.Add("date", typeof(string));
                dt.Columns.Add("image", typeof(string));
                dt.Columns.Add("url", typeof(string));

                foreach (string image in staImgEntry)
                {
                    DataRow dr = dt.NewRow();
                    dr["name"] = Path.GetFileName(image);
                    dr["image"] = "../images/misc/" + dr["name"];
                    dr["url"] = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/') - 5) + "images/misc/" + dr["name"];
                    dr["date"] = File.GetLastWriteTime(image).ToString("yyyyMMddHHmmss");
                    dt.Rows.Add(dr);
                }

                dt = dt.AsEnumerable().OrderByDescending(x => x.Field<string>("date")).CopyToDataTable();
                dgrImage.DataSource = dt;
                dgrImage.DataBind();
            }
        }

        protected void dgrImage_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrImage.CurrentPageIndex = e.NewPageIndex;
            loadImage();
        }

        protected void btnDeleteImage_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrImage.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrImage.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrImage.Items[i].Cells[1].Text;
                }
            }
            gvIDs += ")";

            if (chkBox)
            {


                DirectoryInfo dir = new DirectoryInfo(MapPath("../images/misc"));
                FileInfo[] files = dir.GetFiles();
                //loi chua delete hinh
                foreach (FileInfo file in files)
                {
                    try
                    {
                        if (gvIDs.Contains(file.Name))
                        {
                            File.Delete(file.FullName);
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = ex.Message;
                    }
                }

                loadImage();

                try
                {
                    lblFrmResponse.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponse.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                    frmResponse.Visible = true;
                }
                catch
                {
                }
            }
            else
            {
                lblFrmResponse.ForeColor = System.Drawing.Color.Red;
                lblFrmResponse.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                frmResponse.Visible = true;
            }
        }

        protected void lbtUpload_Click(object sender, EventArgs e)
        {
            if (fulImage.PostedFile.FileName != "")
            {
                try
                {
                    fulImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../images/misc/") + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + fulImage.PostedFile.FileName);
                }
                catch (Exception ex)
                {
                }
                loadImage();
            }
        }
    }
}