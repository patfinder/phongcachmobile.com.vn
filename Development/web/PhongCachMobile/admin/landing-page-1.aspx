﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="landing-page-1.aspx.cs" Inherits="PhongCachMobile.admin.landing_page_1" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">


    <div class="container_12 primary_content_wrap clearfix">
        <%--<asp:FormView >

        </asp:FormView>--%>

        <form id="form1" method="post" class="wpcf7-form" runat="server" enctype="multipart/form-data" >
            <div id="content" class="grid_12">

                <table>
                    <tbody>
                        <!-- ----------------------------- Section 1 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h1>Phần 1 - Tên Sản phẩm</h1><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề</td>
                            <td><asp:TextBox ID="s1Title" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s1Image" runat="server" /><br />
                                <asp:FileUpload ID="s1ImageUpload" runat="server" /></td>
                        </tr>
                        <!-- ----------------------------- Section 2 - Page 1 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h1>Phần 2 - Thiết kế</h1><hr /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><h2>Trang 1</h2></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề</td>
                            <td><asp:TextBox ID="s2p1Title" runat="server" value="Thiết kế" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s2p1Image" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" runat="server" /><br />
                                <asp:FileUpload ID="s2p1ImageUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề mô tả</td>
                            <td><asp:TextBox ID="s2p1DescTitle" runat="server" value="NHỎ GỌN, HOÀN HẢO" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Mô tả</td>
                            <td><CKEditor:CKEditorControl ID="s2p1Desc" runat="server"></CKEditor:CKEditorControl></td>
                        </tr>
                        <!-- ----------------------------- Section 2 - Page 2 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h2>Trang 2</h2><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 1</td>
                            <td><asp:Image ID="s2p2Image1" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <input type="file" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 2</td>
                            <td><asp:Image ID="s2p2Image2" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <input type="file" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 3</td>
                            <td><asp:Image ID="s2p2Image3" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <input type="file" /></td>
                        </tr>
                        <!-- ----------------------------- Section 2 - Page 3 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h2>Trang 3</h2><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề</td>
                            <td><asp:TextBox ID="s2p3Title" runat="server" Text="HOÀN HẢO Ở MỌI GÓC ĐỘ" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 1</td>
                            <td><asp:Image ID="s2p3Image1" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <asp:FileUpload type="s2p3Image1Upload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 2</td>
                            <td><asp:Image ID="s2p3Image2" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <asp:FileUpload type="s2p3Image2Upload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 3</td>
                            <td><asp:Image ID="s2p3Image3" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <asp:FileUpload type="s2p3Image3Upload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 4</td>
                            <td><asp:Image ID="s2p3Image4" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <asp:FileUpload ID="s2p3Image4Upload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 5</td>
                            <td><asp:Image ID="s2p3Image5" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <asp:FileUpload ID="s2p3Image5Upload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình SP 6</td>
                            <td><asp:Image ID="s2p3Image6" runat="server" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" /><br />
                                <asp:FileUpload ID="s2p3Image6Upload" runat="server" /></td>
                        </tr>
                        <!-- ----------------------------- Section 3 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h1>Phần 3 - Máy ảnh</h1><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề</td>
                            <td><asp:TextBox ID="s3Title" runat="server" value="Thiết kế" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s3Image" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" runat="server" /><br />
                                <asp:FileUpload ID="s3ImageUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề mô tả</td>
                            <td><asp:TextBox ID="s3DescTitle" runat="server" value="NHỎ GỌN, HOÀN HẢO" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Mô tả</td>
                            <td><CKEditor:CKEditorControl ID="s3Desc" runat="server"></CKEditor:CKEditorControl></td>
                        </tr>
                        <!-- ----------------------------- Section 4 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h1>Phần 4 - Âm thanh</h1><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề</td>
                            <td><asp:TextBox ID="s4Title" runat="server" value="Thiết kế" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s4Image" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" runat="server" /><br />
                                <asp:FileUpload ID="s4ImageUpload" runat="server" /></td>
                        </tr>
                        <!-- ----------------------------- Section 5 - Page 1 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h1>Phần 5 - Ứng dụng</h1><hr /></td>
                        </tr>
                        <tr>
                            <td colspan="2"><h2>Trang 1</h2><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề</td>
                            <td><asp:TextBox ID="s5p1Title" runat="server" value="Thiết kế" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s5p1Image" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" runat="server" /><br />
                                <asp:FileUpload ID="s5p1ImageUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề mô tả</td>
                            <td><asp:TextBox ID="s5p1DescTitle" runat="server" value="NHỎ GỌN, HOÀN HẢO" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Mô tả</td>
                            <td><CKEditor:CKEditorControl ID="s5p1Desc" runat="server"></CKEditor:CKEditorControl></td>
                        </tr>
                        <!-- ----------------------------- Section 5 - Page 2 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h2>Trang 2</h2><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s5p2Image" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" runat="server" /><br />
                                <asp:FileUpload ID="s5p2ImageUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Tiêu đề mô tả</td>
                            <td><asp:TextBox ID="s5p2DescTitle" runat="server" value="NHỎ GỌN, HOÀN HẢO" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Mô tả</td>
                            <td><CKEditor:CKEditorControl ID="s5p2Desc" runat="server"></CKEditor:CKEditorControl></td>
                        </tr>
                        <!-- ----------------------------- Section 5 - Page 3 ----------------------------- -->
                        <tr>
                            <td colspan="2"><h2>Trang 3</h2><hr /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Hình nền</td>
                            <td><asp:Image ID="s5p3Image" style="width: 50%;" ImageUrl="http://localhost:8060/landing-pages/images/slide-hero-1.jpg" alt="htc one" runat="server" /><br />
                                <asp:FileUpload ID="s5p3ImageUpload" runat="server" /></td>
                        </tr>
                        <tr>
                            <td class="label label-primary">Mô tả</td>
                            <td><asp:TextBox ID="s5p3Desc" runat="server" value="NHỎ GỌN, HOÀN HẢO" /></td>
                        </tr>
                    </tbody>
                </table>

            </div>

            <div>
                <asp:Button ID="btnSave" Text="Lưu" runat="server" CausesValidation="false" class="wpcf7-submit" OnClick="btnSave_Click"/>
            </div>
        </form>
    </div>


    <script type="text/javascript">
        jQuery(document).ready(function () {
            $ = jQuery;

            // Uncheck a category from other section when being selected from one section
            $(".access-select").on('change', function (event) {

                var parentID = $(this).attr("id");
                var options = $(this).children("option:selected");
                options.each(function () {

                    //var parentID = $(this).parent().attr("id");
                    var thisID = $(this).attr("data-id");
                    var thisOfEvent = this;

                    // Unselect other "access-option-ID"
                    $(".access-option-" + thisID).each(function () {
                        // Check if of the same section
                        if (this === thisOfEvent)
                            return;

                        $(this).attr('selected', false);
                    })
                });
            });

            $("input#Add").on('click', function () {
                $("#access-div-0").css("display", "block");
                $(this).hide();
            });
        });
    </script>

</asp:Content>
