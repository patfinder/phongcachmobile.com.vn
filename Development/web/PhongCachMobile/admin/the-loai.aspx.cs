﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class the_loai : System.Web.UI.Page
    {
        DataTable dtCategory = new DataTable();
        string strLevel = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litCategoryManager.Text", "Quản lý thể loại");
            if (!this.IsPostBack)
            {
                checkAuthorization();
                loadLanguages();
                loadGroup();
                loadAllCategory(ddlGroup.SelectedValue);
            }
        }

        private void checkAuthorization()
        {
            if (Session["GroupId"] != null && Session["GroupId"].ToString() != "")
            {
                if (!MenuController.authorize(Session["GroupId"].ToString(), this.GetType().BaseType.Name.Replace('_', '-') + ".aspx"))
                {
                    Response.Redirect("dang-nhap", true);
                }
            }
            else
            {
                Response.Redirect("dang-nhap", true);
            }
        }

        private void loadLanguages()
        {
            this.litCategory.Text = LangController.getLng("litCategory.Text", "Thể loại");
            this.litAddCategoryHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            this.litAddCategory.Text = LangController.getLng("litAddCategory.Text", "Thêm / sửa thể loại");
            this.litAddCategoryHelpDesc.Text = LangMsgController.getLngMsg("litAddCategoryHelp.Desc", "Thông tin giải thích về thêm / cập nhật một thể loại");

            this.litGroupChoosing.Text = LangController.getLng("litGroupChoosing.Text", "Nhóm");
            this.btnAddCategory.Text = LangController.getLng("litAdd.Text", "Thêm");
            this.btnDeleteCategory.Text = LangController.getLng("litDelete.Text", "Xóa");
            this.btnAddCategorySave.Text = LangController.getLng("litSave.Text", "Lưu");
            this.btnCancel.Text = LangController.getLng("litCancel.Text", "Hủy");

            this.litName.Text = LangController.getLng("litName.Text", "Tên");
            this.litEnable.Text = LangController.getLng("litEnable.Text", "Kích hoạt");
            this.litDescription.Text = LangController.getLng("litDescription.Text", "Mô tả");
            this.litParentCategory.Text = LangController.getLng("litParent.Text", "Là sub(con) của");
        }

        private void loadGroup()
        {
            DataTable dt = GroupController.getGroupsByFilter("OBJECT_TYPE");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                    ddlGroup.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
            }

            dt = GroupController.getGroupsByFilter("NEWS_TYPE");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                    ddlGroup.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
            }

            dt = GroupController.getGroupsByFilter("FAQ_TYPE");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                    ddlGroup.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
            }

            ddlGroup.DataBind();
        }

        protected void btnDeleteCategory_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrCategory.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrCategory.Items[i].FindControl("deleteRec");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrCategory.Items[i].Cells[1].Text;
                    //delete images?
                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = CategoryController.deleteCategory(gvIDs);

                if (result)
                {
                    loadAllCategory(ddlGroup.SelectedValue);
                    lblFrmResponseCategory.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponseCategory.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                    frmResponseCategory.Visible = true;
                }
                else
                {
                    lblFrmResponseCategory.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseCategory.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseCategory.Visible = true;
                }
            }

            resetAddCategory();
        }

        protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgrCategory.CurrentPageIndex = 0;
            loadAllCategory(ddlGroup.SelectedValue);
            frmResponseCategory.Visible = false;
        }

        private void resetAddCategory()
        {
            txtName.Text = txtDescription.Text = "";
            ddlCategory.SelectedValue = "";
            dhfId.Value = "";
            rbtYes.Checked = true;
            rbtNo.Checked = false;
            divAddDelete.Attributes["style"] = "display:block";
            divAddCategory.Attributes["style"] = "display:none";
        }

        private void loadAllCategory(string groupId)
        {
            DataTable dt = CategoryController.getCategories(groupId, null);
            dtCategory = dt.Clone();
            dtCategory.Clear();

            if (dt != null && dt.Rows.Count > 0)
            {
                ddlCategory.Items.Clear();
                ddlCategory.Items.Add(new ListItem("", ""));

                dtCategory.Columns.Add("ENABLE", typeof(string));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dtCategory.NewRow();
                    dr["Id"] = dt.Rows[i]["Id"].ToString();
                    dr["GroupId"] = dt.Rows[i]["GroupId"].ToString();
                    dr["ParentId"] = dt.Rows[i]["ParentId"].ToString();
                    dr["Name"] = dt.Rows[i]["Name"].ToString();
                    dr["Description"] = dt.Rows[i]["Description"].ToString();
                    dr["Display"] = dt.Rows[i]["Display"].ToString();
                    dr["InOrder"] = dt.Rows[i]["InOrder"].ToString();
                    dr["Enable"] = bool.Parse(dt.Rows[i]["Display"].ToString()) == true ? "Có" : "Không";

                    dtCategory.Rows.Add(dr);
                    ddlCategory.Items.Add(new ListItem(dt.Rows[i]["Name"].ToString(), dt.Rows[i]["Id"].ToString()));
                    constructSubCategoryHome(dt.Rows[i]["GroupId"].ToString(), dt.Rows[i]["Id"].ToString());
                }
                dgrCategory.DataSource = dtCategory;
                dgrCategory.DataBind();

                ddlCategory.DataBind();
            }
        }

        private void constructSubCategoryHome(string groupId, string parentId)
        {
            DataTable dt = CategoryController.getCategories(groupId, parentId);

            if (dt != null && dt.Rows.Count > 0)
            {
                strLevel += SysIniController.getSetting("Global_IndentLevel", "---");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dtCategory.NewRow();

                    dr["Id"] = dt.Rows[i]["Id"].ToString();
                    dr["GroupId"] = dt.Rows[i]["GroupId"].ToString();
                    dr["ParentId"] = dt.Rows[i]["ParentId"].ToString();
                    dr["Name"] = strLevel + dt.Rows[i]["Name"].ToString();
                    dr["Description"] = dt.Rows[i]["Description"].ToString();
                    dr["Display"] = dt.Rows[i]["Display"].ToString();
                    dr["InOrder"] = dt.Rows[i]["InOrder"].ToString();
                    dr["Enable"] = bool.Parse(dt.Rows[i]["Display"].ToString()) == true ? "Có" : "Không";

                    dtCategory.Rows.Add(dr);
                    ddlCategory.Items.Add(new ListItem(strLevel + dt.Rows[i]["Name"].ToString(), dt.Rows[i]["Id"].ToString()));
                    constructSubCategoryHome(dt.Rows[i]["GroupId"].ToString(), dt.Rows[i]["Id"].ToString());

                }
                strLevel = strLevel.Substring(SysIniController.getSetting("Global_IndentLevel", "---").Length);
            }
        }

        protected void btnAddCategorySave_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.dhfId.Value != "")
            {
                rs = CategoryController.updateCategory(new Category(int.Parse(dhfId.Value), txtName.Text, txtDescription.Text, "",
                    ddlCategory.SelectedValue == "" ? -1 : int.Parse(ddlCategory.SelectedValue),
                    int.Parse(ddlGroup.SelectedValue), -1, rbtYes.Checked));
            }
            else
            {
                int parentId = (ddlCategory.SelectedValue == "" ? -1 : int.Parse(ddlCategory.SelectedValue));
                rs = CategoryController.createCategory(new Category(txtName.Text, txtDescription.Text, "",
                     ddlCategory.SelectedValue == "" ? -1 : int.Parse(ddlCategory.SelectedValue),
                    int.Parse(ddlGroup.SelectedValue), getMaxOrderBy(ddlCategory.SelectedValue), rbtYes.Checked));
            }

            if (rs == true)
            {
                loadAllCategory(ddlGroup.SelectedValue);
                lblFrmResponseCategory.ForeColor = System.Drawing.Color.Green;
                if (this.dhfId.Value == "")
                {
                    lblFrmResponseCategory.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseCategory.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseCategory.Visible = true;
            }
            else
            {
                lblFrmResponseCategory.ForeColor = System.Drawing.Color.Red;
                if (this.dhfId.Value == "")
                {
                    lblFrmResponseCategory.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseCategory.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseCategory.Visible = true;
            }
            resetAddCategory();
        }

        private int getMaxOrderBy(string categoryId)
        {
            if (categoryId == "&nbsp;" || categoryId == "")
                categoryId = null;
            DataTable dt = CategoryController.getCategories(ddlGroup.SelectedValue, categoryId);

            if (dt == null)
                return 0;
            return dt.Rows.Count;

        }

        protected void dgrCategory_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            loadAllCategory(ddlGroup.SelectedValue);
            dgrCategory.CurrentPageIndex = e.NewPageIndex;
            dgrCategory.DataBind();

            //Disable add menu
            resetAddCategory();

            frmResponseCategory.Visible = false;

        }

        protected void dgrCategory_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllCategory(ddlGroup.SelectedValue);
            DataTable dt = (DataTable)dgrCategory.DataSource;
            DataRow dr = dt.Rows[iIndex];

            this.txtName.Text = dr["Name"].ToString().Replace(SysIniController.getSetting("Global_IndentLevel", "---"), "");
            this.txtDescription.Text = dr["Description"].ToString();
            this.ddlCategory.SelectedValue = dr["ParentId"].ToString();
            this.dhfId.Value = dr["Id"].ToString();
            this.rbtYes.Checked = bool.Parse(dr["Display"].ToString());
            this.rbtNo.Checked = !bool.Parse(dr["Display"].ToString());
            divAddDelete.Attributes["style"] = "display:none";
            divAddCategory.Attributes["style"] = "display:block";

            frmResponseCategory.Visible = false;
        }

        protected void dgrCategory_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Up")
            {
                int iIndex = 0;
                DataTable dtSibling = loadSiblingCategory(e.Item.Cells[4].Text);
                DataRow drCurrent = null;
                for (int i = 0; i < dtSibling.Rows.Count; i++)
                {
                    if (dtSibling.Rows[i]["id"].ToString() == e.Item.Cells[1].Text)
                    {
                        drCurrent = dtSibling.Rows[i];
                        iIndex = i;
                        break;
                    }
                }

                if (iIndex != 0)
                {
                    DataRow drPrevious = dtSibling.Rows[iIndex - 1];

                    string strOrderBy = drCurrent["InOrder"].ToString();

                    bool rsCurrent = CategoryController.updateOrderCategory(drCurrent["Id"].ToString(), int.Parse(drPrevious["InOrder"].ToString()));
                    bool rsPrevious = CategoryController.updateOrderCategory(drPrevious["Id"].ToString(), int.Parse(strOrderBy));

                    frmResponseCategory.Visible = true;

                    if (rsCurrent && rsPrevious)
                    {
                        loadAllCategory(ddlGroup.SelectedValue);
                        lblFrmResponseCategory.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseCategory.Text = LangController.getLng("litMoveOk.Text", "Thay đổi thành công");
                    }
                    else
                    {
                        lblFrmResponseCategory.ForeColor = System.Drawing.Color.Red;
                        lblFrmResponseCategory.Text = LangController.getLng("litMoveError.Text", "Thay đổi thất bại");
                    }
                }
            }
            else
                if (e.CommandName == "Down")
                {
                    int iIndex = 0;
                    DataTable dtSibling = loadSiblingCategory(e.Item.Cells[4].Text);
                    DataRow drCurrent = null;
                    for (int i = 0; i < dtSibling.Rows.Count; i++)
                    {
                        if (dtSibling.Rows[i]["id"].ToString() == e.Item.Cells[1].Text)
                        {
                            drCurrent = dtSibling.Rows[i];
                            iIndex = i;
                            break;
                        }
                    }

                    if (iIndex != dtSibling.Rows.Count - 1)
                    {
                        DataRow drNext = dtSibling.Rows[iIndex + 1];

                        string strOrderBy = drCurrent["InOrder"].ToString();

                        bool rsCurrent = CategoryController.updateOrderCategory(drCurrent["Id"].ToString(), int.Parse(drNext["InOrder"].ToString()));
                        bool rsNext = CategoryController.updateOrderCategory(drNext["Id"].ToString(), int.Parse(strOrderBy));

                        frmResponseCategory.Visible = true;

                        if (rsCurrent && rsNext)
                        {
                            loadAllCategory(ddlGroup.SelectedValue);
                            lblFrmResponseCategory.ForeColor = System.Drawing.Color.Green;
                            lblFrmResponseCategory.Text = LangController.getLng("litMoveOk.Text", "Thay đổi thành công");
                        }
                        else
                        {
                            lblFrmResponseCategory.ForeColor = System.Drawing.Color.Red;
                            lblFrmResponseCategory.Text = LangController.getLng("litMoveError.Text", "Thay đổi thất bại");
                        }
                    }
                }
        }

        private DataTable loadSiblingCategory(string categoryId)
        {
            if (categoryId == "&nbsp;")
                categoryId = null;
            DataTable dt = CategoryController.getCategories(ddlGroup.SelectedValue, categoryId);
            return dt;
        }

        protected void dgrCategory_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                LinkButton btnUp = (LinkButton)e.Item.FindControl("hplUp");

                if (btnUp != null && e.Item.Cells[7].Text == "0")
                {
                    btnUp.Visible = false;
                }

                LinkButton btnDown = (LinkButton)e.Item.FindControl("hplDown");
                int iOrderBy = getMaxOrderBy(e.Item.Cells[4].Text) - 1;
                string strOrderBy = iOrderBy.ToString();
                if (btnDown != null && e.Item.Cells[7].Text == strOrderBy)
                {
                    btnDown.Visible = false;
                }
            }

        }
    }
}