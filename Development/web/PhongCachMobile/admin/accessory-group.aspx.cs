﻿using PhongCachMobile.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.admin
{
    public partial class accessory_group : System.Web.UI.Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProductController).FullName);

        protected void Page_Load(object sender, EventArgs e)
        {
            string strImagePath = "images/product/";

            if (!string.IsNullOrEmpty(Request["submit"]) && Request["submit"] != "Cancel")
            {
                // Update
                for (int i = 0; i < 100; i++)
                {
                    // Check no more
                    if (string.IsNullOrEmpty(Request["SectionID-" + i]))
                        break;

                    int ID = int.Parse(Request["SectionID-" + i]);
                    string Title = Request["title-" + ID] ?? "";
                    string Link = Request["link-" + ID] ?? "";
                    //string Image = Request["image-" + ID] ?? "";
                    string imageControl = "image-" + ID;
                    int ShowItemCount = int.Parse(Request["showitemcount-" + ID] ?? "8");
                    List<string> categories = (Request["select-" + ID] ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                    // New section defined
                    bool newSection = ID == 0 && (!string.IsNullOrEmpty(Title) || categories.Count > 0);

                    // Invalid input
                    if (string.IsNullOrEmpty(Title) && categories.Count > 0)
                    {
                        ShowResult = true;
                        ResultMessage = "Tên nhóm phụ kiện không được để trống.";
                        return;
                    }

                    if (newSection && (Request.Files[imageControl] == null || string.IsNullOrEmpty(Request.Files[imageControl].FileName)))
                    {
                        ShowResult = true;
                        ResultMessage = "Hình không thể trống";
                        return;
                    }

                    // Empty group
                    if (categories.Count <= 0)
                    {
                        // Remove empty group
                        if (ID > 0)
                        {
                            AccessoryGroupController.deleteGroup(ID);
                            continue;
                        }

                        // Skip new group with nothing defined
                        if (!string.IsNullOrEmpty(Title))
                        {
                            ShowResult = true;
                            ResultMessage = "Bạn phải chọn ít nhất một loại trong nhóm phụ kiện.";
                            return;
                        }

                        continue;
                    }

                    SectionInfo sectionInfo = ID == 0 ? null : AccessoryGroupController.getGroup(ID);
                    string fileName = sectionInfo == null ? "" : sectionInfo.Image;

                    try
                    {
                        // Save image if any
                        if(Request.Files[imageControl] != null && !string.IsNullOrEmpty(Request.Files[imageControl].FileName))
                        {
                            // Save file, and update if submitted
                            string currentImage = HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName;
                            if (File.Exists(currentImage))
                                File.Delete(currentImage);

                            // Save new file
                            fileName = Request.Files[imageControl].FileName;

                            if (File.Exists(fileName))
                                fileName += new Random().Next();

                            // Save new image
                            Request.Files[imageControl].SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("accessory-group.aspx error", ex);

                        ShowResult = true;
                        ResultMessage = ex.Message;
                        return;
                    }

                    // Final work
                    SectionInfo section = new SectionInfo
                    {
                        ID = ID,
                        Title = Title,
                        Image = fileName,
                        Link = Link,
                        ShowItemCount = ShowItemCount,
                        Categories = categories.Select(c => new CategoryInfo
                        {
                            ID = int.Parse(c),
                        }).ToList(),
                    };

                    AccessoryGroupController.updateGroup(section);
                }

                ShowResult = true;
                ResultMessage = "Cập nhật thành công danh sách nhóm phụ kiện.";
                return;
            }

            // Delete a group
            if (Request["command"] == "delete" || Request["?command"] == "delete")
            {
                int ID = int.Parse(Request["ID"]);
                AccessoryGroupController.deleteGroup(ID);

                Response.Redirect("/admin/accessory-group");
                return;
            }

            doInit();
        }

        public bool ShowResult = false;
        public string ResultMessage = "";

        /// <summary>
        /// List of accessory categories
        /// </summary>
        public List<CategoryInfo> AccessCategories = new List<CategoryInfo>();

        /// <summary>
        /// List of sections
        /// </summary>
        public List<SectionInfo> SectionList = new List<SectionInfo>();

        protected void doInit()
        {
            // Load accessory list
            var dt = CategoryController.getCategories(ProductController.ProductGroupAccessory, "");
            foreach (DataRow dr in dt.Rows)
            {
                AccessCategories.Add(new CategoryInfo
                {
                    ID = (int)dr["ID"],
                    Name = (string)dr["Name"],
                });
            }

            // Load Sections from db
            var groups = AccessoryGroupController.getGroups();
            foreach (var group in groups)
            {
                group.Categories.ForEach(c =>
                {
                    var acc = AccessCategories.FirstOrDefault(a => a.ID == c.ID);
                    if (acc != default(CategoryInfo))
                        c.Name = acc.Name;
                });
            }
            SectionList = groups;

            SectionList.Add(new SectionInfo
            {
                ID = 0,
                Title = "",
            });
        }
    }

    public class SectionInfo
    {
        public int ID = 0;
        public string Title = "";
        public string Description = null;
        public string Image = "";
        public string Link = "";
        public int ShowItemCount = 8;
        public List<CategoryInfo> Categories = new List<CategoryInfo>();
    }

    public class CategoryInfo
    {
        public int ID = 0;
        public string Name = "";
    }
}