﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using System.IO;
using PhongCachMobile.model;
using System.Web.Services;

namespace PhongCachMobile.admin
{
    public partial class san_pham : System.Web.UI.Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProductController).FullName);

        protected void Page_Load(object sender, EventArgs e)
        {
            // ScriptManager.RegisterStartupScript(dgrPrd, this.GetType(), "update_access", 
            //     "function updateSelectedAccess() {" +
            //     "	var listAccess = $('currentAccess').val().split(',');" +
            //     "	$.each(listAccess, function (index, value) {" +
            //     "		$('.ck-access#access-' + value).attr('checked', true);" +
            //     "	});" +
            //     "}; updateSelectedAccess();", true);

            this.Title = LangController.getLng("litPrdManager.Text", "Quản lý sản phẩm");
            
            if (!this.IsPostBack)
            {
                loadType();
                loadCategory();
                loadOS();
                loadFeature();
                loadLang();
                loadAllPrd();
            }

            //loadOS();

            loadAccessories();
        }

        protected bool ShowSuggestedAccessories = true;
        protected List<Product> Accessories = new List<Product>();

        private void loadAccessories()
        {
            // Load accessory list
            DataTable dt = ProductController.getSimpleProductsByGroupId(7, "");
            foreach (DataRow dr in dt.Rows)
            {
                //Accessories.Add(ProductController.rowToSimpleProduct(dr));
                Accessories.Add(new Product {
                    id = (int)dr["id"],
                    name = dr["name"] as string,
                });
            }
        }

        [WebMethod]
        public static string GetDate()
        {
            return DateTime.Now.ToString();
        }

        private void loadFeature()
        {
            DataTable dt = FeatureController.getFeaturesByGroupId("4");
            cblFeature.Items.Clear();
            if (dt != null && dt.Rows.Count >0)
            {
                foreach(DataRow dr in dt.Rows)
                {
                    cblFeature.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }
                cblFeature.DataBind();
            }
        }

        private void loadAllArticle(string productId)
        {
            DataTable dt = ArticleController.getArticleByProductId("22", productId);

            if (dt != null && dt.Rows.Count > 0)
            {
                string imagePath = "images/news/";

                foreach (DataRow dr in dt.Rows)
                {

                    dr["DisplayImage"] = "../" + imagePath + dr["DisplayImage"].ToString();
                    dr["PostedDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                }
            }
            dgrArticle.DataSource = dt;
            dgrArticle.DataBind();
        }

        private void loadOS()
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId(ddlType.SelectedValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlOS.DataSource = dt;
                ddlOS.DataValueField = "Id";
                ddlOS.DataTextField = "Name";
                ddlOS.DataBind();
            }
        }

        private void loadAllPrd()
        {
            DataTable dt = ProductController.getProducts(ddlType.SelectedValue, ddlCateogry.SelectedValue);
            if (dt != null && dt.Rows.Count > 0)
            {
                string imagePath = "images/product/";
                foreach (DataRow dr in dt.Rows)
                {
                    dr["FirstImage"] = "../" + imagePath + dr["FirstImage"].ToString();

                }
            }
            dgrPrd.DataSource = dt;
            dgrPrd.DataBind();
        }

        private void loadLang()
        {
            litPrd.Text = LangController.getLng("litPrd.Text", "Sản phẩm");
            btnAddPrd.Text = btnAddArticle.Text = btnAddVideo.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeletePrd.Text = btnDeleteArticle.Text = btnDeleteVideo.Text = LangController.getLng("litDelete.Text", "Xóa");
            btnCloseArticle.Text = btnCloseVideo.Text = LangController.getLng("litClose.Text", "Đóng");
            btnAddSavePrd.Text = btnAddSaveArticle.Text = btnAddSaveVideo.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelPrd.Text = btnCancelArticle.Text = btnCancelSaveVideo.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddArticleHelp.Text = litAddArticleHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            //litAddPrdHelpDesc.Text = LangMsgController.getLngMsg("litAddPrdHelp.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa sản phẩm.");
            litAddArticleHelpDesc.Text = LangMsgController.getLngMsg("litAddArticleHelpDesc.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa tư vấn.");
        }

        private void loadType()
        {
            DataTable dt = GroupController.getGroupsByFilter("OBJECT_TYPE");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlType.DataSource = dt;
                ddlType.DataValueField = "Id";
                ddlType.DataTextField = "Name";
                ddlType.DataBind();
            }
        }

        private void loadCategory()
        {
            DataTable dt = CategoryController.getCategories(ddlType.SelectedValue, "");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlCateogry.DataSource = ddlAnotherCategory.DataSource = dt;
                ddlCateogry.DataValueField = ddlAnotherCategory.DataValueField = "Id";
                ddlCateogry.DataTextField = ddlAnotherCategory.DataTextField = "Name";
                ddlCateogry.DataBind();
                ddlAnotherCategory.DataBind();
            }
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reload page if product chosen to edit
            //if (!string.IsNullOrWhiteSpace(ContentBlockOpenned.Value))
            //{
            //    Response.Redirect(Request.RawUrl, true);
            //    return;
            //}

            dgrPrd.CurrentPageIndex = 0;

            loadCategory();
            loadOS();
            loadAllPrd();
        }

        protected void ddlCateogry_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Reload page if product chosen to edit
            //if (!string.IsNullOrWhiteSpace(ContentBlockOpenned.Value))
            //{
            //    Response.Redirect(Request.RawUrl, true);
            //    return;
            //}

            loadAllPrd();
        }

        protected void dgrPrd_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Article")
            {
                int iIndex = e.Item.DataSetIndex;
                loadAllPrd();
                DataTable dt = (DataTable)dgrPrd.DataSource;
                DataRow dr = dt.Rows[iIndex];
                hdfPrdId.Value = dr["Id"].ToString();
                loadAllArticle(dr["Id"].ToString());

                divAddDeletePrd.Attributes["style"] = "display:none";
                divAddPrd.Attributes["style"] = "display:none";
                divArticle.Attributes["style"] = "display:block";
                divAddArticle.Attributes["style"] = "display:none";

                frmResponseArticle.Visible = false;
                frmResponsePrd.Visible = false;
            } 
            else if (e.CommandName == "Video")
            {
                int iIndex = e.Item.DataSetIndex;
                loadAllPrd();
                DataTable dt = (DataTable)dgrPrd.DataSource;
                DataRow dr = dt.Rows[iIndex];
                hdfProductId.Value = dr["Id"].ToString();
                loadAllVideo(dr["Id"].ToString());

                divAddDeletePrd.Attributes["style"] = "display:none";
                divAddPrd.Attributes["style"] = "display:none";
                divVideo.Attributes["style"] = "display:block";
                divAddVideo.Attributes["style"] = "display:none";

                frmResponseVideo.Visible = false;
                frmResponsePrd.Visible = false;
            }
            else if (e.CommandName == "Feature")
            {
                int iIndex = e.Item.DataSetIndex;
                loadAllPrd();
                DataTable dt = (DataTable)dgrPrd.DataSource;
                DataRow dr = dt.Rows[iIndex];
                hdfAnotherPrdId.Value = dr["Id"].ToString();

                DataTable dtFeature = ProductFeatureController.getFeatureByProductId(dr["Id"].ToString());

                Dictionary<int, string> dictFeature = new Dictionary<int, string>();
                foreach (DataRow drFeature in dtFeature.Rows)
                {
                    dictFeature.Add(int.Parse(drFeature["FeatureId"].ToString()), drFeature["Value"].ToString());
                }

                if (dtFeature.Rows.Count > 0)
                {
                    hdfFeatureId.Value = "1";

                    txt2G.Text = dictFeature[6] == null ? "" : dictFeature[6];
                    txt3G.Text = dictFeature[7] == null ? "" : dictFeature[7];
                    txt4G.Text = dictFeature[8] == null ? "" : dictFeature[8];
                    txtCommingOut.Text = dictFeature[9] == null ? "" : dictFeature[9];
                    txtSize.Text = dictFeature[10] == null ? "" : dictFeature[10];
                    txtWeight.Text = dictFeature[11] == null ? "" : dictFeature[11];
                    txtDisplayType.Text = dictFeature[12] == null ? "" : dictFeature[12];
                    txtDisplaySize.Text = dictFeature[13] == null ? "" : dictFeature[13];
                    txtDisplayOther.Text = dictFeature[14] == null ? "" : dictFeature[14];
                    txtRingType.Text = dictFeature[15] == null ? "" : dictFeature[15];
                    txtOutSound.Text = dictFeature[16] == null ? "" : dictFeature[16];
                    txtOtherSound.Text = dictFeature[17] == null ? "" : dictFeature[17];
                    txtNumberBook.Text = dictFeature[18] == null ? "" : dictFeature[18];
                    txtCalledNumber.Text = dictFeature[19] == null ? "" : dictFeature[19];
                    txtInsideMemory.Text = dictFeature[20] == null ? "" : dictFeature[20];
                    txtCardSlot.Text = dictFeature[21] == null ? "" : dictFeature[21];
                    txtGPRS.Text = dictFeature[22] == null ? "" : dictFeature[22];
                    txtEDGE.Text = dictFeature[23] == null ? "" : dictFeature[23];
                    txtDataLoad3G.Text = dictFeature[24] == null ? "" : dictFeature[24];
                    txtWLAN.Text = dictFeature[25] == null ? "" : dictFeature[25];
                    txtBluetooth.Text = dictFeature[26] == null ? "" : dictFeature[26];
                    txtNFC.Text = dictFeature[27] == null ? "" : dictFeature[27];
                    txtUSB.Text = dictFeature[28] == null ? "" : dictFeature[28];
                    txtMainCamera.Text = dictFeature[29] == null ? "" : dictFeature[29];
                    txtCameraFeature.Text = dictFeature[30] == null ? "" : dictFeature[30];
                    txtVideoRecorder.Text = dictFeature[31] == null ? "" : dictFeature[31];
                    txtSubCamera.Text = dictFeature[32] == null ? "" : dictFeature[32];
                    txtOS.Text = dictFeature[33] == null ? "" : dictFeature[33];
                    txtChip.Text = dictFeature[34] == null ? "" : dictFeature[34];
                    txtSMS.Text = dictFeature[35] == null ? "" : dictFeature[35];
                    txtBrowser.Text = dictFeature[36] == null ? "" : dictFeature[36];
                    txtRadio.Text = dictFeature[37] == null ? "" : dictFeature[37];
                    txtGame.Text = dictFeature[38] == null ? "" : dictFeature[38];
                    txtColor.Text = dictFeature[39] == null ? "" : dictFeature[39];
                    txtLanguage.Text = dictFeature[40] == null ? "" : dictFeature[40];
                    txtGPS.Text = dictFeature[41] == null ? "" : dictFeature[41];
                    txtJava.Text = dictFeature[42] == null ? "" : dictFeature[42];
                    txtOtherFeature.Text = dictFeature[43] == null ? "" : dictFeature[43];
                    txtPin.Text = dictFeature[44] == null ? "" : dictFeature[44];
                }
                else
                {
                    hdfFeatureId.Value = "";

                    txt2G.Text = txt3G.Text = txt4G.Text = txtCommingOut.Text = txtSize.Text = txtWeight.Text = txtDisplayType.Text = txtDisplaySize.Text = txtDisplayOther.Text =
                    txtRingType.Text = txtOutSound.Text = txtOtherSound.Text = txtNumberBook.Text = txtCalledNumber.Text = txtInsideMemory.Text = txtCardSlot.Text = txtGPRS.Text = txtEDGE.Text =
                    txtDataLoad3G.Text = txtWLAN.Text = txtBluetooth.Text = txtNFC.Text = txtUSB.Text = txtMainCamera.Text = txtCameraFeature.Text = txtVideoRecorder.Text = txtSubCamera.Text =
                    txtOS.Text = txtChip.Text = txtSMS.Text = txtBrowser.Text = txtRadio.Text = txtGame.Text = txtColor.Text = txtLanguage.Text = txtGPS.Text = txtJava.Text =
                    txtOtherFeature.Text = txtPin.Text = "";
                }


                divAddDeletePrd.Attributes["style"] = "display:none";
                divAddPrd.Attributes["style"] = "display:none";
                divArticle.Attributes["style"] = "display:none";
                divAddArticle.Attributes["style"] = "display:none";
                divFeature.Attributes["style"] = "display:block";

                frmResponseArticle.Visible = false;
                frmResponsePrd.Visible = false;
            }
        }

        private void loadAllVideo(string prdId)
        {
            DataTable dt = VideoController.getVideoByProductId(prdId);
            dgrVideo.DataSource = dt;
            dgrVideo.DataBind();
        }

        protected void btnDeletePrd_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrPrd.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrPrd.Items[i].FindControl("deleteRecPrd");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrPrd.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = ProductController.deleteProduct(gvIDs);

                if (result)
                {
                    loadAllPrd();
                    //Delete images
                    try
                    {
                        string imagePath = "images/product/";

                        DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                        FileInfo[] files = dir.GetFiles();
                        string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');

                        //loi chua delete hinh
                        foreach (FileInfo file in files)
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                try
                                {
                                    File.Delete(file.FullName);
                                }
                                catch (Exception ex)
                                {
                                    log.Error("btnDeletePrd_Click", ex);
                                }
                            }
                        }

                        lblFrmResponsePrd.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponsePrd.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponsePrd.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponsePrd.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponsePrd.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponsePrd.Visible = true;
                }
            }
            resetAddPrd();
        }

        private void resetAddPrd()
        {
            txtName.Text = txtShortDesc.Text = txtLongDesc.Text = txtInclude.Text = txtRemark.Text = txtOutstanding.Text = txtDiscountDesc.Text = txtVideo.Text =
                txtCurrentPrice.Text = txtCurrentPrice2.Value = txtDiscountPrice.Text = txtViewCount.Text = txtRate.Text = txtHotDesc.Text = "";
            chbIsFeatured.Checked = chbIsSpeedySold.Checked = chbIsCompanyProduct.Checked =
                chbIsGifted.Checked = chbIsComingSoon.Checked = chbIsOutOfStock.Checked = chkLightBackground.Checked = false;

            foreach (ListItem item in cblFeature.Items)
            {
                item.Selected = false;
            }
            ddlOS.SelectedIndex = ddlAnotherCategory.SelectedIndex = 0;
            hdfId.Value = "";
            rbtEnableYes.Checked = true;
            rbtEnableNo.Checked = false;
            hplFirstImage.NavigateUrl = hplSecondImage.NavigateUrl = hplThirdImage.NavigateUrl = hplFourthImage.NavigateUrl = hplFifthImage.NavigateUrl = "";
            imgFirst.ImageUrl = imgSecond.ImageUrl = imgThird.ImageUrl = imgFourth.ImageUrl = imgFifth.ImageUrl = imgBackground.ImageUrl = "";
            txtImageDesc1.Text = txtImageDesc2.Text = txtImageDesc3.Text = txtImageDesc4.Text = txtImageDesc5.Text = "";


            divAddDeletePrd.Attributes["style"] = "display:block";
            divAddPrd.Attributes["style"] = "display:none";
            divArticle.Attributes["style"] = "display:none";
            divAddArticle.Attributes["style"] = "display:none";

        }

        protected void btnAddSavePrd_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = "images/product/";
            if (this.hdfId.Value != "")
            {
                if (fulFirstImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgFirst.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                        log.Error("btnAddSavePrd_Click", ex);
                    }
                }

                if (fulSecondImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgSecond.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                        log.Error("btnAddSavePrd_Click", ex);
                    }
                }

                if (fulThirdImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgThird.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                        log.Error("btnAddSavePrd_Click", ex);

                    }
                }

                if (fulFourthImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgFourth.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                        log.Error("btnAddSavePrd_Click", ex);
                    }
                }

                if (fulFifthImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgFifth.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                        log.Error("btnAddSavePrd_Click", ex);
                    }
                }

                if (fulBackgroundImage.PostedFile.FileName != "")
                {
                    string fileName = Path.GetFileName(imgBackground.ImageUrl);
                    try
                    {
                        File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                    }
                    catch (Exception ex)
                    {
                        log.Error("btnAddSavePrd_Click", ex);
                    }
                }

                DateTime? lastPriceUpdate = null;
                if (!string.IsNullOrWhiteSpace(txtCurrentPrice2.Value) && !string.IsNullOrWhiteSpace(txtCurrentPrice.Text))
                {
                    if (int.Parse(txtCurrentPrice.Text) < int.Parse(txtCurrentPrice2.Value))
                        lastPriceUpdate = DateTime.Now;
                }

                rs = ProductController.updateProduct(new Product
                {
                    id = int.Parse(hdfId.Value),
                    name = txtName.Text,
                    shortDesc = txtShortDesc.Text,
                    longDesc = txtLongDesc.Text,
                    include = txtInclude.Text,
                    remark = txtRemark.Text,
                    outstanding = txtOutstanding.Text,
                    viewCount = !string.IsNullOrWhiteSpace(txtViewCount.Text) ? int.Parse(txtViewCount.Text) : 0,
                    rate = !string.IsNullOrWhiteSpace(txtRate.Text) ? double.Parse(txtRate.Text) : 0,
                    categoryId = !string.IsNullOrWhiteSpace(ddlAnotherCategory.SelectedValue) ? int.Parse(ddlAnotherCategory.SelectedValue) : 0,
                    isFeatured = chbIsFeatured.Checked,
                    isSpeedySold = chbIsSpeedySold.Checked,
                    isGifted = chbIsGifted.Checked,
                    isComingSoon = chbIsComingSoon.Checked,
                    isOutOfStock = chbIsOutOfStock.Checked,
                    isCompanyProduct = chbIsCompanyProduct.Checked,
                    currentPrice = !string.IsNullOrWhiteSpace(txtCurrentPrice.Text) ? int.Parse(txtCurrentPrice.Text) : 0,
                    discountPrice = !string.IsNullOrWhiteSpace(txtDiscountPrice.Text) ? int.Parse(txtDiscountPrice.Text) : 0,
                    osId = !string.IsNullOrWhiteSpace(ddlOS.SelectedValue) ? int.Parse(ddlOS.SelectedValue) : 1,
                    video = txtVideo.Text,
                    groupId = !string.IsNullOrWhiteSpace(ddlType.SelectedValue) ? int.Parse(ddlType.SelectedValue) : 0,
                    discountDesc = txtDiscountDesc.Text,
                    hotDesc = txtHotDesc.Text,
                    flag = rbtEnableYes.Checked,
                    useDarkBackground = !chkLightBackground.Checked,
                    firstImageDesc = txtImageDesc1.Text,
                    secondImageDesc = txtImageDesc2.Text,
                    thirdImageDesc = txtImageDesc3.Text,
                    fourthImageDesc = txtImageDesc4.Text,
                    fifthImageDesc = txtImageDesc5.Text,
                    lastPriceUpdate = lastPriceUpdate,
                });

                foreach (ListItem item in cblFeature.Items)
                {
                    if (ProductFeatureController.getFeatureByProductId(hdfId.Value,item.Value).Rows.Count <=0)
                    {
                        bool result = ProductFeatureController.createProductFeature(new ProductFeature
                        {
                            productId = int.Parse(hdfId.Value),
                            featureId = int.Parse(item.Value),
                            value = item.Selected.ToString()
                        });
                    }
                    else
                    {
                        bool result = ProductFeatureController.updateProductFeature(new ProductFeature
                        {
                            productId = int.Parse(hdfId.Value),
                            featureId = int.Parse(item.Value),
                            value = item.Selected.ToString()
                        });
                    }
                }

            }
            else
            {
                rs = ProductController.createProduct(new Product
                {
                    name = txtName.Text,
                    shortDesc = txtShortDesc.Text,
                    longDesc = txtLongDesc.Text,
                    include = txtInclude.Text,
                    remark = txtRemark.Text,
                    outstanding = txtOutstanding.Text,
                    viewCount = !string.IsNullOrWhiteSpace(txtViewCount.Text) ? int.Parse(txtViewCount.Text) : 0,
                    rate = !string.IsNullOrWhiteSpace(txtRate.Text) ? double.Parse(txtRate.Text) : 0,
                    categoryId = !string.IsNullOrWhiteSpace(ddlAnotherCategory.SelectedValue) ? int.Parse(ddlAnotherCategory.SelectedValue) : 0,
                    isFeatured = chbIsFeatured.Checked,
                    isSpeedySold = chbIsSpeedySold.Checked,
                    isGifted = chbIsGifted.Checked,
                    isComingSoon = chbIsComingSoon.Checked,
                    isOutOfStock = chbIsOutOfStock.Checked,
                    isCompanyProduct = chbIsCompanyProduct.Checked,
                    currentPrice = !string.IsNullOrWhiteSpace(txtCurrentPrice.Text) ? int.Parse(txtCurrentPrice.Text) : 0,
                    discountPrice = !string.IsNullOrWhiteSpace(txtDiscountPrice.Text) ? int.Parse(txtDiscountPrice.Text) : 0,
                    osId = !string.IsNullOrWhiteSpace(ddlOS.SelectedValue) ? int.Parse(ddlOS.SelectedValue) : 1,
                    video = txtVideo.Text,
                    groupId = !string.IsNullOrWhiteSpace(ddlType.SelectedValue) ? int.Parse(ddlType.SelectedValue) : 0,
                    discountDesc = txtDiscountDesc.Text,
                    hotDesc = txtHotDesc.Text,
                    flag = rbtEnableYes.Checked,
                    useDarkBackground = !chkLightBackground.Checked,
                    firstImageDesc = txtImageDesc1.Text,
                    secondImageDesc = txtImageDesc2.Text,
                    thirdImageDesc = txtImageDesc3.Text,
                    fourthImageDesc = txtImageDesc4.Text,
                    fifthImageDesc = txtImageDesc5.Text,
                });

                string id = this.hdfId.Value == "" ? ProductController.getCurrentId() : hdfId.Value;

                foreach (ListItem item in cblFeature.Items)
                {
                    bool result = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(id),
                        featureId = int.Parse(item.Value),
                        value = item.Selected.ToString()
                    });
                }
            }
            if (rs)
            {
                string id = this.hdfId.Value == "" ? ProductController.getCurrentId() : hdfId.Value;

                // upload Image
                try
                {
                    if (fulFirstImage.PostedFile.FileName != "")
                    {
                        fulFirstImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulFirstImage.PostedFile.FileName);
                        object[,] parameters = { { "@FirstImage", id + "_" + fulFirstImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update PRODUCT set FirstImage = @FirstImage where Id = @Id", parameters);
                    }

                    if (fulSecondImage.PostedFile.FileName != "")
                    {
                        fulSecondImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulSecondImage.PostedFile.FileName);
                        object[,] parameters = { { "@SecondImage", id + "_" + fulSecondImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update PRODUCT set SecondImage = @SecondImage where Id = @Id", parameters);
                    }

                    if (fulThirdImage.PostedFile.FileName != "")
                    {
                        fulThirdImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulThirdImage.PostedFile.FileName);
                        object[,] parameters = { { "@ThirdImage", id + "_" + fulThirdImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update PRODUCT set ThirdImage = @ThirdImage where Id = @Id", parameters);
                    }

                    if (fulFourthImage.PostedFile.FileName != "")
                    {
                        fulFourthImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulFourthImage.PostedFile.FileName);
                        object[,] parameters = { { "@FourthImage", id + "_" + fulFourthImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update PRODUCT set FourthImage = @FourthImage where Id = @Id", parameters);
                    }

                    if (fulFifthImage.PostedFile.FileName != "")
                    {
                        fulFifthImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulFifthImage.PostedFile.FileName);
                        object[,] parameters = { { "@FifthImage", id + "_" + fulFifthImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update PRODUCT set FifthImage = @FifthImage where Id = @Id", parameters);
                    }

                    if (fulBackgroundImage.PostedFile.FileName != "")
                    {
                        fulBackgroundImage.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulBackgroundImage.PostedFile.FileName);
                        object[,] parameters = { { "@backgroundImage", id + "_" + fulBackgroundImage.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update PRODUCT set backgroundImage = @backgroundImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("btnAddSavePrd_Click", ex);
                }

                loadAllPrd();
                lblFrmResponsePrd.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponsePrd.Visible = true;
            }
            else
            {
                lblFrmResponsePrd.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponsePrd.Visible = true;
            }
            resetAddPrd();
        }


        protected void btnSaveFeature_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.hdfFeatureId.Value != "")
            {
                rs = ProductFeatureController.updateProductFeature(new ProductFeature
                {
                    productId = int.Parse(hdfAnotherPrdId.Value),
                    featureId = 6,
                    value = txt2G.Text
                });

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 7,
                        value = txt3G.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 8,
                        value = txt4G.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 9,
                        value = txtCommingOut.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 10,
                        value = txtSize.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 11,
                        value = txtWeight.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 12,
                        value = txtDisplayType.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 13,
                        value = txtDisplaySize.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 14,
                        value = txtDisplayOther.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 15,
                        value = txtRingType.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 16,
                        value = txtOutSound.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 17,
                        value = txtOtherSound.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 18,
                        value = txtNumberBook.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 19,
                        value = txtCalledNumber.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 20,
                        value = txtInsideMemory.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 21,
                        value = txtCardSlot.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 22,
                        value = txtGPRS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 23,
                        value = txtEDGE.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 24,
                        value = txtDataLoad3G.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 25,
                        value = txtWLAN.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 26,
                        value = txtBluetooth.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 27,
                        value = txtNFC.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 28,
                        value = txtUSB.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 29,
                        value = txtMainCamera.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 30,
                        value = txtCameraFeature.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 31,
                        value = txtVideoRecorder.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 32,
                        value = txtSubCamera.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 33,
                        value = txtOS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 34,
                        value = txtChip.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 35,
                        value = txtSMS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 36,
                        value = txtBrowser.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 37,
                        value = txtRadio.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 38,
                        value = txtGame.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 39,
                        value = txtColor.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 40,
                        value = txtLanguage.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 41,
                        value = txtGPS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 42,
                        value = txtJava.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 43,
                        value = txtOtherFeature.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.updateProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 44,
                        value = txtPin.Text
                    });
                }


            }
            else
            {
                rs = ProductFeatureController.createProductFeature(new ProductFeature
                {
                    productId = int.Parse(hdfAnotherPrdId.Value),
                    featureId = 6,
                    value = txt2G.Text
                });

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 7,
                        value = txt3G.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 8,
                        value = txt4G.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 9,
                        value = txtCommingOut.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 10,
                        value = txtSize.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 11,
                        value = txtWeight.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 12,
                        value = txtDisplayType.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 13,
                        value = txtDisplaySize.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 14,
                        value = txtDisplayOther.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 15,
                        value = txtRingType.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 16,
                        value = txtOutSound.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 17,
                        value = txtOtherSound.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 18,
                        value = txtNumberBook.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 19,
                        value = txtCalledNumber.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 20,
                        value = txtInsideMemory.Text
                    });
                }

                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 21,
                        value = txtCardSlot.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 22,
                        value = txtGPRS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 23,
                        value = txtEDGE.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 24,
                        value = txtDataLoad3G.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 25,
                        value = txtWLAN.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 26,
                        value = txtBluetooth.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 27,
                        value = txtNFC.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 28,
                        value = txtUSB.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 29,
                        value = txtMainCamera.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 30,
                        value = txtCameraFeature.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 31,
                        value = txtVideoRecorder.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 32,
                        value = txtSubCamera.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 33,
                        value = txtOS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 34,
                        value = txtChip.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 35,
                        value = txtSMS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 36,
                        value = txtBrowser.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 37,
                        value = txtRadio.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 38,
                        value = txtGame.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 39,
                        value = txtColor.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 40,
                        value = txtLanguage.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 41,
                        value = txtGPS.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 42,
                        value = txtJava.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 43,
                        value = txtOtherFeature.Text
                    });
                }
                if (rs)
                {
                    rs = ProductFeatureController.createProductFeature(new ProductFeature
                    {
                        productId = int.Parse(hdfAnotherPrdId.Value),
                        featureId = 44,
                        value = txtPin.Text
                    });
                }
            }
            if (rs)
            {
                lblFrmResponsePrd.ForeColor = System.Drawing.Color.Green;

                if (this.hdfAnotherPrdId.Value == "")
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponsePrd.Visible = true;
            }
            else
            {
                lblFrmResponsePrd.ForeColor = System.Drawing.Color.Red;
                if (this.hdfAnotherPrdId.Value == "")
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponsePrd.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponsePrd.Visible = true;
            }
            resetUpdatePrdFeature();
        }

        private void resetUpdatePrdFeature()
        {
            hdfFeatureId.Value = hdfAnotherPrdId.Value = "";
            divAddDeletePrd.Attributes["style"] = "display:block";
            divAddPrd.Attributes["style"] = "display:none";
            divArticle.Attributes["style"] = "display:none";
            divAddArticle.Attributes["style"] = "display:none";
            divFeature.Attributes["style"] = "display:none";
        }


        protected void dgrPrd_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrPrd.CurrentPageIndex = e.NewPageIndex;
            loadAllPrd();
            frmResponsePrd.Visible = false;
            resetAddPrd();
        }

        protected List<int> currentAccessList = new List<int>();

        protected void dgrPrd_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllPrd();
            DataTable dt = (DataTable)dgrPrd.DataSource;
            DataRow dr = dt.Rows[iIndex];

            if (dr["Flag"].ToString() == "True")
            {
                rbtEnableYes.Checked = true;
                rbtEnableNo.Checked = false;
            }
            else
            {
                rbtEnableYes.Checked = false;
                rbtEnableNo.Checked = true;
            }

            txtName.Text = dr["Name"].ToString();
            txtShortDesc.Text = dr["ShortDesc"].ToString();
            txtLongDesc.Text = dr["LongDesc"].ToString();
            txtInclude.Text = dr["Include"].ToString();
            txtRemark.Text = dr["Remark"].ToString();
            txtOutstanding.Text = dr["Outstanding"].ToString();
            txtDiscountDesc.Text = dr["DiscountDesc"].ToString();
            txtHotDesc.Text = dr["HotDesc"].ToString();
            txtVideo.Text = dr["Video"].ToString();
            txtViewCount.Text = dr["ViewCount"].ToString();
            txtCurrentPrice.Text = dr["CurrentPrice"].ToString();
            txtCurrentPrice2.Value = dr["CurrentPrice"].ToString();
            txtDiscountPrice.Text = dr["DiscountPrice"].ToString();
            //ddlOS.SelectedValue = dr["OSId"].ToString();
            ddlOS.Text = dr["OSId"].ToString();
            ddlAnotherCategory.Text = dr["CategoryId"].ToString();
            txtRate.Text = dr["Rate"].ToString();
            chkLightBackground.Checked = !(dr["useDarkBackground"] as bool? ?? false);

            txtImageDesc1.Text = dr["FirstImageDesc"].ToString();
            txtImageDesc2.Text = dr["SecondImageDesc"].ToString();
            txtImageDesc3.Text = dr["ThirdImageDesc"].ToString();
            txtImageDesc4.Text = dr["FourthImageDesc"].ToString();
            txtImageDesc5.Text = dr["FifthImageDesc"].ToString();

            this.imgFirst.ImageUrl = this.hplFirstImage.NavigateUrl = dr["FirstImage"].ToString();
            this.imgSecond.ImageUrl = this.hplSecondImage.NavigateUrl = "../images/product/" + dr["SecondImage"].ToString();
            this.imgThird.ImageUrl = this.hplThirdImage.NavigateUrl = "../images/product/" + dr["ThirdImage"].ToString();
            this.imgFourth.ImageUrl = this.hplFourthImage.NavigateUrl = "../images/product/" + dr["FourthImage"].ToString();
            this.imgFifth.ImageUrl = this.hplFifthImage.NavigateUrl = "../images/product/" + dr["FifthImage"].ToString();
            this.imgBackground.ImageUrl = this.hplBackgroundImage.NavigateUrl = "../images/product/" + dr["BackgroundImage"].ToString();

            DataTable dtFeature = ProductFeatureController.getFeatureByProductId(dr["Id"].ToString(),"1");
            cblFeature.Items[0].Selected = dtFeature.Rows.Count > 0 ? bool.Parse(dtFeature.Rows[0]["Value"].ToString()) : false;

            dtFeature = ProductFeatureController.getFeatureByProductId(dr["Id"].ToString(), "2");
            cblFeature.Items[1].Selected = dtFeature.Rows.Count > 0 ? bool.Parse(dtFeature.Rows[0]["Value"].ToString()) : false;

            dtFeature = ProductFeatureController.getFeatureByProductId(dr["Id"].ToString(), "3");
            cblFeature.Items[2].Selected = dtFeature.Rows.Count > 0 ? bool.Parse(dtFeature.Rows[0]["Value"].ToString()) : false;

            this.hdfId.Value = dr["Id"].ToString();
            this.chbIsFeatured.Checked = bool.Parse(dr["isFeatured"].ToString() == "" ? "False" : dr["isFeatured"].ToString());
            this.chbIsSpeedySold.Checked = bool.Parse(dr["isSpeedySold"].ToString() == "" ? "False" : dr["isSpeedySold"].ToString());
            this.chbIsCompanyProduct.Checked = bool.Parse(dr["isCompanyProduct"].ToString() == "" ? "False" : dr["isCompanyProduct"].ToString());
            this.chbIsGifted.Checked = bool.Parse(dr["isGifted"].ToString() == "" ? "False" : dr["isGifted"].ToString());
            this.chbIsOutOfStock.Checked = bool.Parse(dr["isOutOfStock"].ToString() == "" ? "False" : dr["isOutOfStock"].ToString());
            this.chbIsComingSoon.Checked = bool.Parse(dr["isComingSoon"].ToString() == "" ? "False" : dr["isComingSoon"].ToString());
            divAddDeletePrd.Attributes["style"] = "display:none";
            divAddPrd.Attributes["style"] = "display:block";
            divArticle.Attributes["style"] = "display:none";
            divAddArticle.Attributes["style"] = "display:none";
            currentAccess.Value = dr["suggestedAccessories"].ToString();
            currentAccessList = dr["suggestedAccessories"].ToString()
                .Split(new char[] { ',' }).Where(s => !string.IsNullOrWhiteSpace(s)).Select(s => int.Parse(s.Trim())).ToList();

            frmResponsePrd.Visible = false;
        }

        protected void dgrArticle_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllArticle(hdfPrdId.Value);
            DataTable dt = (DataTable)dgrArticle.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtArticleTitle.Text = dr["Title"].ToString();
            txtArticleShortDesc.Text = dr["ShortDesc"].ToString();
            txtArticleLongDesc.Text = dr["LongDesc"].ToString();
            txtArticleViewCount.Text = dr["ViewCount"].ToString();

            this.imgDisplay.ImageUrl = this.hplDisplayImg.NavigateUrl = dr["DisplayImage"].ToString();
            this.hdfArticleId.Value = dr["Id"].ToString();

            divAddDeletePrd.Attributes["style"] = "display:none";
            divAddPrd.Attributes["style"] = "display:none";
            divArticle.Attributes["style"] = "display:block";
            divAddDeleteArticle.Attributes["style"] = "display:none";
            divAddArticle.Attributes["style"] = "display:block";

            frmResponseArticle.Visible = false;
            frmResponsePrd.Visible = false;
        }

        protected void dgrArticle_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrArticle.CurrentPageIndex = e.NewPageIndex;
            loadAllArticle(hdfPrdId.Value);
            frmResponseArticle.Visible = false;
            resetAddArticle();
        }

        private void resetAddArticle()
        {
            txtArticleTitle.Text = txtArticleShortDesc.Text = txtArticleLongDesc.Text = txtArticleViewCount.Text = imgDisplay.ImageUrl = hplDisplayImg.NavigateUrl = "";
            hdfArticleId.Value = "";

            divArticle.Attributes["style"] = "display:block";
            divAddArticle.Attributes["style"] = "display:none";
            divAddDeleteArticle.Attributes["style"] = "display:block";
        }

        protected void btnDeleteArticle_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrArticle.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrArticle.Items[i].FindControl("deleteRecArticle");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrArticle.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = ArticleController.deleteArticle(gvIDs);

                if (result)
                {
                    loadAllArticle(hdfPrdId.Value);

                    string imagePath = "images/news/";

                    DirectoryInfo dir = new DirectoryInfo(MapPath("../" + imagePath));
                    FileInfo[] files = dir.GetFiles();
                    string[] gvId = gvIDs.Substring(1, gvIDs.Length - 2).Split(',');
                    //loi chua delete hinh
                    foreach (FileInfo file in files)
                    {
                        try
                        {
                            if (gvId.Contains(file.Name.Substring(0, file.Name.IndexOf('_'))))
                            {
                                File.Delete(file.FullName);
                            }
                        }
                        catch (Exception ex)
                        {
                            log.Error("btnDeleteArticle_Click", ex);
                        }
                    }

                    try
                    {
                        lblFrmResponseArticle.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseArticle.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseArticle.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseArticle.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseArticle.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseArticle.Visible = true;
                }
            }
            resetAddArticle();
        }

        protected void btnAddSaveArticle_Click(object sender, EventArgs e)
        {
            bool rs;
            string strImagePath = "images/news/";
            string groupId = "22";
            if (this.hdfArticleId.Value != "")
            {
                try
                {
                    if (fulDisplayImg.PostedFile.FileName != "")
                    {
                        string fileName = Path.GetFileName(imgDisplay.ImageUrl);
                        try
                        {
                            File.Delete(HttpContext.Current.Request.MapPath("../" + strImagePath) + fileName);
                        }
                        catch (Exception ex)
                        {
                            log.Error("btnAddSaveArticle_Click", ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.Error("btnAddSaveArticle_Click", ex);
                }

                rs = ArticleController.updateArticle(new Article(int.Parse(hdfArticleId.Value), txtArticleTitle.Text, txtArticleShortDesc.Text, txtArticleLongDesc.Text, "", int.Parse(txtArticleViewCount.Text), true,
                    DateTime.Now.ToString("yyyyMMddHHmmss"), int.Parse(groupId), 59, int.Parse(hdfPrdId.Value)));
            }
            else
            {
                rs = ArticleController.createArticle(new Article(txtArticleTitle.Text, txtArticleShortDesc.Text, txtArticleLongDesc.Text, "", int.Parse(txtArticleViewCount.Text), true,
                    DateTime.Now.ToString("yyyyMMddHHmmss"), int.Parse(groupId), 59, int.Parse(hdfPrdId.Value)));
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfArticleId.Value == "" ? ArticleController.getCurrentId() : hdfArticleId.Value;
                try
                {
                    if (fulDisplayImg.PostedFile.FileName != "")
                    {
                        fulDisplayImg.PostedFile.SaveAs(HttpContext.Current.Request.MapPath("../" + strImagePath) + id + "_" + fulDisplayImg.PostedFile.FileName);
                        object[,] parameters = { { "@DisplayImage", id + "_" + fulDisplayImg.PostedFile.FileName }, 
                                               {"@Id", id}};
                        DB.exec("update ARTICLE set DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }
                }
                catch (Exception ex)
                {
                    log.Error("btnAddSaveArticle_Click", ex);
                }

                loadAllArticle(hdfPrdId.Value);
                lblFrmResponseArticle.ForeColor = System.Drawing.Color.Green;

                if (this.hdfArticleId.Value == "")
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseArticle.Visible = true;
            }
            else
            {
                lblFrmResponseArticle.ForeColor = System.Drawing.Color.Red;
                if (this.hdfArticleId.Value == "")
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseArticle.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseArticle.Visible = true;
            }
            resetAddArticle();
        }

        protected void dgrVideo_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllVideo(hdfProductId.Value);
            DataTable dt = (DataTable)dgrVideo.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtVideoCode.Text = dr["Video"].ToString();
            txtVideoTitle.Text = dr["Title"].ToString();
            this.hdfVideoId.Value = dr["Id"].ToString();

            divAddDeletePrd.Attributes["style"] = "display:none";
            divAddPrd.Attributes["style"] = "display:none";
            divVideo.Attributes["style"] = "display:block";
            divAddDeleteVideo.Attributes["style"] = "display:none";
            divAddVideo.Attributes["style"] = "display:block";

            frmResponseVideo.Visible = false;
            frmResponsePrd.Visible = false;
        }

        protected void dgrVideo_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrVideo.CurrentPageIndex = e.NewPageIndex;
            loadAllVideo(hdfProductId.Value);
            frmResponseVideo.Visible = false;
            resetAddVideo();
        }

        private void resetAddVideo()
        {
            txtVideoCode.Text = txtVideoTitle.Text = "";
            hdfVideoId.Value = "";

            divVideo.Attributes["style"] = "display:block";
            divAddVideo.Attributes["style"] = "display:none";
            divAddDeleteVideo.Attributes["style"] = "display:block";
        }

        protected void btnDeleteVideo_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrVideo.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrVideo.Items[i].FindControl("deleteRecVideo");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrVideo.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = VideoController.deleteVideo(gvIDs);

                if (result)
                {
                    loadAllVideo(hdfProductId.Value);
                    try
                    {
                        lblFrmResponseVideo.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponseVideo.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponseVideo.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponseVideo.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseVideo.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseVideo.Visible = true;
                }
            }
            resetAddVideo();
        }

        protected void btnAddSaveVideo_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.hdfVideoId.Value != "")
            {
                rs = VideoController.updateVideo(new Video{
                    id = int.Parse(hdfVideoId.Value), 
                    title = txtVideoTitle.Text,
                    video = txtVideoCode.Text});
            }
            else
            {
                rs = VideoController.createVideo(new Video{
                    productId = int.Parse(hdfProductId.Value),
                    title = txtVideoTitle.Text,
                    video = txtVideoCode.Text});
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfVideoId.Value == "" ? VideoController.getCurrentId() : hdfVideoId.Value;
               
                loadAllVideo(hdfProductId.Value);
                lblFrmResponseVideo.ForeColor = System.Drawing.Color.Green;

                if (this.hdfVideoId.Value == "")
                {
                    lblFrmResponseVideo.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseVideo.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseVideo.Visible = true;
            }
            else
            {
                lblFrmResponseVideo.ForeColor = System.Drawing.Color.Red;
                if (this.hdfVideoId.Value == "")
                {
                    lblFrmResponseVideo.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseVideo.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseVideo.Visible = true;
            }
            resetAddVideo();
        }
    }
}