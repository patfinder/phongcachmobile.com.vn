﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.IO;
using PhongCachMobile.model;

namespace PhongCachMobile.admin
{
    public partial class diem_tich_luy : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = LangController.getLng("litCardPointManager.Text", "Quản lý thẻ điểm tích lũy");
            if (!this.IsPostBack)
            {
                loadStatus();
                loadLang();
                loadAllCard();
                loadAllPoint("01/01/2013 00:00", DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                if (Session["GroupId"].ToString() == "32")
                {
                    btnApproveClose.Visible = true;
                    divUnApprovedPoint.Visible = true;
                }
                else
                {
                    btnApproveClose.Visible = false;
                    divUnApprovedPoint.Visible = false;
                }

            }
        }

        private void loadStatus()
        {
            ddlStatus.Items.Clear();
            ddlStatus.Items.Add(new ListItem("", "-1"));
            ddlStatus.Items.Add(new ListItem("ĐANG SỬ DỤNG", "0"));
            ddlStatus.Items.Add(new ListItem("ĐANG ĐỢI XỬ LÝ", "1"));
            ddlStatus.Items.Add(new ListItem("ĐÃ ĐÓNG", "2"));
            ddlStatus.DataBind();
            
        }

        private void loadAllPoint(string cardId)
        {
            DataTable dt = PointController.getPointsByCardId(cardId);

            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("User", typeof(string));
                dt.Columns.Add("sDate", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["User"] = SysUserController.getFullnameById(dr["UserId"].ToString());
                    dr["sDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                    string status = "";
                    if (bool.Parse(dr["IsApproved"].ToString()) == false)
                        status = "ĐANG ĐỢI KIỂM TRA";
                    else
                        status = "ĐÃ KIỂM TRA";
                    dr["Status"] = status;
                }
            }
            dgrPoint.DataSource = dt;
            dgrPoint.DataBind();
        }

        private void loadAllCard()
        {
            DataTable dt = CardController.getCards(int.Parse(ddlStatus.SelectedValue));
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("sDate", typeof(string));
                dt.Columns.Add("sStatus", typeof(string));
                dt.Columns.Add("Point", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["sDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                    string status = "";
                    if (dr["Status"].ToString() == "2")
                        status = "ĐÃ ĐÓNG";
                    else if (dr["Status"].ToString() == "1")
                        status = "ĐANG ĐỢI XỬ LÝ";
                    else
                        status = "ĐANG SỬ DỤNG";
                    dr["sStatus"] = status;

                    dr["Point"] = PointController.getTotalPointByCardId(dr["Id"].ToString());
                }
            }
            dgrCard.DataSource = dt;
            dgrCard.DataBind();
        }

        private void loadLang()
        {
            litCard.Text = LangController.getLng("litCard.Text", "Thẻ tích lũy");
            btnAddCard.Text = btnAddPoint.Text = LangController.getLng("litAdd.Text", "Thêm");
            btnDeleteCard.Text = btnDeletePoint.Text = LangController.getLng("litDelete.Text", "Xóa");
            btnClosePoint.Text = LangController.getLng("litClose.Text", "Đóng");
            btnAddSaveCard.Text = btnAddSavePoint.Text = LangController.getLng("litSave.Text", "Lưu");
            btnCancelCard.Text = btnCancelPoint.Text = LangController.getLng("litCancel.Text", "Hủy");
            litAddPointHelp.Text = litAddPointHelp.Text = LangController.getLng("litHelp.Text", "Giúp đỡ");
            //litAddPrdHelpDesc.Text = LangMsgController.getLngMsg("litAddPrdHelp.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa sản phẩm.");
            litAddPointHelpDesc.Text = LangMsgController.getLngMsg("litAddPointHelpDesc.Desc", "Những thông tin giúp đỡ về cách thêm mới / chỉnh sửa điểm tích lúy.");
            litFrom.Text = LangController.getLng("litFromTime.Text", "Thời gian bất đầu");
            litTo.Text = LangController.getLng("litToTime.Text", "Thời gian kết thúc");
        }


        protected void dgrCard_ItemCommand(object source, DataGridCommandEventArgs e)
        {
            if (e.CommandName == "Point")
            {
                int iIndex = e.Item.DataSetIndex;
                if (txtCode.Text == "")
                {
                    loadAllCard();
                    DataTable dtCard = (DataTable)dgrCard.DataSource;
                    if (dtCard.Rows[iIndex]["Status"].ToString() == "2")
                    {
                        btnAddPoint.Visible = false;
                        btnDeletePoint.Visible = false;
                    }
                    else
                    {
                        btnAddPoint.Visible = true;
                        btnDeletePoint.Visible = true;
                    }
                }
                else
                {
                    DataTable dtCard = CardController.getCardByCode(txtCode.Text);
                    if (dtCard != null && dtCard.Rows.Count <= 0)
                    {
                        loadAllCard();
                        if (dtCard.Rows[0]["Status"].ToString() == "2")
                        {
                            btnAddPoint.Visible = false;
                            btnDeletePoint.Visible = false;
                        }
                        else
                        {
                            btnAddPoint.Visible = true;
                            btnDeletePoint.Visible = true;
                        }

                    }
                }

                DataTable dt = (DataTable)dgrCard.DataSource;
                DataRow dr = dt.Rows[iIndex];
                hdfCardId.Value = dr["Id"].ToString();
                loadAllPoint(dr["Id"].ToString());

                divAddDeleteCard.Attributes["style"] = "display:none";
                divAddCard.Attributes["style"] = "display:none";
                divPoint.Attributes["style"] = "display:block";
                divAddPoint.Attributes["style"] = "display:none";

                frmResponsePoint.Visible = false;
                frmResponseCard.Visible = false;
            }
               
        }

        protected void btnDeleteCard_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrCard.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrCard.Items[i].FindControl("deleteRecCard");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrCard.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = CardController.deleteCard(gvIDs);

                if (result)
                {
                    loadAllCard();

                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponseCard.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                    frmResponseCard.Visible = true;
                  
                }
                else
                {
                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseCard.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponseCard.Visible = true;
                }
            }
            resetAddCard();
        }

        private void resetAddCard()
        {
            txtCardCode.Text = txtBrandNumber.Text =  "";
            hdfId.Value = "";

            divAddDeleteCard.Attributes["style"] = "display:block";
            divAddCard.Attributes["style"] = "display:none";
            divPoint.Attributes["style"] = "display:none";
            divAddPoint.Attributes["style"] = "display:none";

        }

        protected void btnAddSaveCard_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.hdfId.Value != "")
            {
                rs = CardController.updateCard(new Card
                {
                    id = int.Parse(hdfId.Value),
                    code = txtCardCode.Text,
                    brandNumber = txtBrandNumber.Text
                });

            }
            else
            {
                rs = CardController.createCard(new Card
                {
                    code = txtCardCode.Text,
                    brandNumber = txtBrandNumber.Text,
                    status = 0,
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss")
                });
            }
            if (rs)
            {
                string id = this.hdfId.Value == "" ? CardController.getCurrentId() : hdfId.Value;

                loadAllCard();
                lblFrmResponseCard.ForeColor = System.Drawing.Color.Green;

                if (this.hdfId.Value == "")
                {
                    lblFrmResponseCard.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    lblFrmResponseCard.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                frmResponseCard.Visible = true;
            }
            else
            {
                lblFrmResponseCard.ForeColor = System.Drawing.Color.Red;
                if (this.hdfId.Value == "")
                {
                    lblFrmResponseCard.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                }
                else
                {
                    lblFrmResponseCard.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                frmResponseCard.Visible = true;
            }
            resetAddCard();
        }

        protected void dgrCard_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrCard.CurrentPageIndex = e.NewPageIndex;
            loadAllCard();
            frmResponseCard.Visible = false;
            resetAddCard();
        }

        protected void dgrCard_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            if (txtCode.Text != "")
            {
                loadCardByCode(txtCode.Text);
            }
            else
            {
                loadAllCard();
            }
            DataTable dt = (DataTable)dgrCard.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtCardCode.Text = dr["Code"].ToString();
            txtBrandNumber.Text = dr["BrandNumber"].ToString();

            this.hdfId.Value = dr["Id"].ToString();
            divAddDeleteCard.Attributes["style"] = "display:none";
            divAddCard.Attributes["style"] = "display:block";
            divPoint.Attributes["style"] = "display:none";
            divAddPoint.Attributes["style"] = "display:none";

            frmResponseCard.Visible = false;

            if (dt.Rows[iIndex]["Status"].ToString() == "2")
            {
                btnAddSaveCard.Visible = false;
            }
            else
            {
                btnAddSaveCard.Visible = true;
            }

        }

        protected void dgrPoint_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            loadAllPoint(hdfCardId.Value);
            DataTable dt = (DataTable)dgrPoint.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtAnotherCardCode.Text = dr["Code"].ToString();

            this.hdfPointId.Value = dr["Id"].ToString();

            divAddDeleteCard.Attributes["style"] = "display:none";
            divAddCard.Attributes["style"] = "display:none";
            divPoint.Attributes["style"] = "display:block";
            divAddDeletePoint.Attributes["style"] = "display:none";
            divAddPoint.Attributes["style"] = "display:block";

            frmResponsePoint.Visible = false;
            frmResponseCard.Visible = false;
        }

        protected void dgrPoint_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrPoint.CurrentPageIndex = e.NewPageIndex;
            loadAllPoint(hdfCardId.Value);
            frmResponsePoint.Visible = false;
            resetAddPoint();
        }

        private void resetAddPoint()
        {
            txtAnotherCardCode.Text = "";
            hdfPointId.Value =  "";
            if (hdfUpdateFromUnapproved.Value != "1")
                divPoint.Attributes["style"] = "display:block";
            else
                divPoint.Attributes["style"] = "display:none";
            hdfUpdateFromUnapproved.Value = "";
            divAddPoint.Attributes["style"] = "display:none";
            divAddDeletePoint.Attributes["style"] = "display:block";
        }

        protected void btnDeletePoint_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrPoint.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrPoint.Items[i].FindControl("deleteRecPoint");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrPoint.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = PointController.deletePoint(gvIDs);

                if (result)
                {
                    loadAllPoint(hdfCardId.Value);
                    try
                    {
                        lblFrmResponsePoint.ForeColor = System.Drawing.Color.Green;
                        lblFrmResponsePoint.Text = LangController.getLng("litDeleteOk.Text", "Xóa thành công");
                        frmResponsePoint.Visible = true;
                    }
                    catch
                    {
                    }
                }
                else
                {
                    lblFrmResponsePoint.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponsePoint.Text = LangController.getLng("litDeleteError.Text", "Xóa thất bại");
                    frmResponsePoint.Visible = true;
                }
            }
            resetAddPoint();
        }

        protected void btnAddSavePoint_Click(object sender, EventArgs e)
        {
            bool rs;
            if (this.hdfPointId.Value != "")
            {
                rs = PointController.updatePoint(new Point{
                    id = int.Parse(hdfPointId.Value),
                    code = txtAnotherCardCode.Text
                });
            }
            else
            {
                rs = PointController.createPoint(new Point
                {
                    userId = int.Parse(Session["UserId"].ToString()),
                    code = txtAnotherCardCode.Text,
                    cardId = int.Parse(hdfCardId.Value),
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    isApproved = false
                });
            }
            if (rs)
            {
                // upload Image
                string id = this.hdfPointId.Value == "" ? PointController.getCurrentId() : hdfPointId.Value;

                loadAllPoint(hdfCardId.Value);
                if (txtFrom.Text != "" && txtTo.Text != "")
                    loadAllPoint(txtFrom.Text, txtTo.Text);
                else
                    loadAllPoint("01/01/2013 00:00", DateTime.Now.ToString("dd/MM/yyyy HH:mm"));

                if (hdfUpdateFromUnapproved.Value != "1")
                    lblFrmResponsePoint.ForeColor = System.Drawing.Color.Green;
                else
                    lblResponseUnapprovedPoint.ForeColor = System.Drawing.Color.Green;

                

                if (this.hdfPointId.Value == "")
                {
                    if (hdfUpdateFromUnapproved.Value != "1")
                        lblFrmResponsePoint.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                    else
                        lblResponseUnapprovedPoint.Text = LangController.getLng("litAddOk.Text", "Thêm thành công");
                }
                else
                {
                    
                    if (hdfUpdateFromUnapproved.Value != "1")
                        lblFrmResponsePoint.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                    else
                        lblResponseUnapprovedPoint.Text = LangController.getLng("litUpdateOk.Text", "Cập nhật thành công");
                }
                if (hdfUpdateFromUnapproved.Value != "1")
                    frmResponsePoint.Visible = true;
                else
                    frmResponseUnapprovedPoint.Visible = true;
            }
            else
            {
                if (hdfUpdateFromUnapproved.Value != "1")
                    lblFrmResponsePoint.ForeColor = System.Drawing.Color.Red;
                else
                    lblResponseUnapprovedPoint.ForeColor = System.Drawing.Color.Red;

                if (this.hdfPointId.Value == "")
                {
                    if (hdfUpdateFromUnapproved.Value != "1")
                        lblFrmResponsePoint.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");
                    else
                        lblResponseUnapprovedPoint.Text = LangController.getLng("litAddError.Text", "Thêm thất bại");                  
                }
                else
                {
                    if (hdfUpdateFromUnapproved.Value != "1")
                        lblFrmResponsePoint.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                    else
                        lblResponseUnapprovedPoint.Text = LangController.getLng("litUpdateError.Text", "Cập nhật thất bại");
                }

                if (hdfUpdateFromUnapproved.Value != "1")
                    frmResponsePoint.Visible = true;
                else
                    frmResponseUnapprovedPoint.Visible = true;

                
            }
            resetAddPoint();
        }

        protected void lbtSearch_Click(object sender, EventArgs e)
        {
            if (txtCode.Text != "")
                loadCardByCode(txtCode.Text);
            else
                loadAllCard();
        }

        private void loadCardByCode(string code)
        {
            DataTable dt = CardController.getCardByCode(code);
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("sDate", typeof(string));
                dt.Columns.Add("sStatus", typeof(string));
                dt.Columns.Add("Point", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["sDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                    string status = "";
                    if (dr["Status"].ToString() == "2")
                        status = "ĐÃ ĐÓNG";
                    else if (dr["Status"].ToString() == "1")
                        status = "ĐANG ĐỢI XỬ LÝ";
                    else
                        status = "ĐANG SỬ DỤNG";
                    dr["sStatus"] = status;

                    dr["Point"] = PointController.getTotalPointByCardId(dr["Id"].ToString());
                }

            }
            dgrCard.DataSource = dt;
            dgrCard.DataBind();
        }

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtCode.Text = "";
            loadAllCard();
        }


        protected void btnRequestClose_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrCard.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrCard.Items[i].FindControl("deleteRecCard");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrCard.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool resultCheck = PointController.getUnapprovedPoint(gvIDs);
                if (!resultCheck)
                {
                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseCard.Text = LangController.getLng("litUnApprovedPointError.Text", "Tồn tại thẻ có điểm chưa được xác nhận. Hãy chắc chắn toàn bộ điểm đã được xác nhận trước khi gửi yêu cầu đóng thẻ.");
                    frmResponseCard.Visible = true;
                    return;
                }

                bool result = CardController.requestToApproveCard(gvIDs);

                if (result)
                {
                    loadAllCard();

                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponseCard.Text = LangController.getLng("litRequestToApproveCardOk.Text", "Gửi yêu cầu đóng card thành công. Xin hãy đợi xác minh.");
                    frmResponseCard.Visible = true;

                }
                else
                {
                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseCard.Text = LangController.getLng("litRequestToApproveCardError.Text", "Gửi yêu cầu đóng card thất bại. Bạn hãy thử lại sau.");
                    frmResponseCard.Visible = true;
                }
            }
            resetAddCard();
        }

        protected void btnApproveClose_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrCard.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrCard.Items[i].FindControl("deleteRecCard");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrCard.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = CardController.approveCard(gvIDs);

                if (result)
                {
                    if (txtCode.Text != "")
                        loadCardByCode(txtCode.Text);
                    else
                        loadAllCard();

                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Green;
                    lblFrmResponseCard.Text = LangController.getLng("litApproveCardOk.Text", "Cho phép đóng card thành công");
                    frmResponseCard.Visible = true;

                }
                else
                {
                    lblFrmResponseCard.ForeColor = System.Drawing.Color.Red;
                    lblFrmResponseCard.Text = LangController.getLng("litApproveCardError.Text", "Cho phép đóng card thất bại");
                    frmResponseCard.Visible = true;
                }
            }
            resetAddCard();
        }

        protected void btnApprovePoint_Click(object sender, EventArgs e)
        {
            string gvIDs = "(-1";
            bool chkBox = false;
            //'Navigate through each row in the GridView for checkbox items
            for (int i = 0; i < dgrUnApprovedPoint.Items.Count; i++)
            {
                CheckBox deleteChkBxItem = (CheckBox)dgrUnApprovedPoint.Items[i].FindControl("deleteRecUnApprovedPoint");
                if (deleteChkBxItem.Checked)
                {
                    chkBox = true;
                    // Concatenate GridView items with comma for SQL Delete
                    gvIDs += "," + dgrUnApprovedPoint.Items[i].Cells[1].Text;
                    //delete images?

                }
            }
            gvIDs += ")";

            if (chkBox)
            {
                bool result = PointController.approvePoint(gvIDs);

                if (result)
                {
                    if (txtFrom.Text != "" && txtTo.Text != "")
                        loadAllPoint(txtFrom.Text, txtTo.Text);
                    else
                        loadAllPoint("01/01/2013 00:00", DateTime.Now.ToString("dd/MM/yyyy HH:mm"));

                    if (txtCode.Text != "")
                        loadCardByCode(txtCode.Text);
                    else
                        loadAllCard();


                    lblResponseUnapprovedPoint.ForeColor = System.Drawing.Color.Green;
                    lblResponseUnapprovedPoint.Text = LangController.getLng("litApprovePointOk.Text", "Xác nhận điểm thành công");
                    frmResponseUnapprovedPoint.Visible = true;

                }
                else
                {
                    lblResponseUnapprovedPoint.ForeColor = System.Drawing.Color.Red;
                    lblResponseUnapprovedPoint.Text = LangController.getLng("litApprovePointError.Text", "Xác nhận điểm thất bại");
                    frmResponseUnapprovedPoint.Visible = true;
                }
            }
        }

        protected void lbtFind_Click(object sender, EventArgs e)
        {
            loadAllPoint(txtFrom.Text, txtTo.Text);
        }

        private void loadAllPoint(string from, string to)
        {
            string timeFrom = "";
            string timeTo = "";
            if (to == "")
                timeTo = DateTime.Now.ToString("yyyyMMddHHmmsss");
            else
                timeTo = Common.strToFormatDateTime(to, "dd/MM/yyyy HH:mm").ToString("yyyyMMddHHmmss"); ;
            timeFrom = Common.strToFormatDateTime(from, "dd/MM/yyyy HH:mm").ToString("yyyyMMddHHmmss");

            DataTable dt = PointController.getUnApprovedPoint(timeFrom, timeTo);
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("User", typeof(string));
                dt.Columns.Add("sDate", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["User"] = SysUserController.getFullnameById(dr["UserId"].ToString());
                    dr["sDate"] = Common.strToDateTime(dr["PostedDate"].ToString()).ToShortDateString();
                    string status = "";
                    if (bool.Parse(dr["IsApproved"].ToString()) == false)
                        status = "ĐANG ĐỢI KIỂM TRA";
                    else
                        status = "ĐÃ KIỂM TRA";
                    dr["Status"] = status;
                }
            }
            dgrUnApprovedPoint.DataSource = dt;
            dgrUnApprovedPoint.DataBind();
        }

        protected void dgrUnApprovedPoint_EditCommand(object source, DataGridCommandEventArgs e)
        {
            int iIndex = e.Item.DataSetIndex;
            if(txtFrom.Text != "" && txtTo.Text != "")
                loadAllPoint(txtFrom.Text, txtTo.Text);
            else
                loadAllPoint("01/01/2013 00:00", DateTime.Now.ToString("dd/MM/yyyy HH:mm"));

            DataTable dt = (DataTable)dgrUnApprovedPoint.DataSource;
            DataRow dr = dt.Rows[iIndex];

            txtAnotherCardCode.Text = dr["Code"].ToString();

            this.hdfPointId.Value = dr["Id"].ToString();
            this.hdfUpdateFromUnapproved.Value = "1";
            divAddDeleteCard.Attributes["style"] = "display:none";
            divAddCard.Attributes["style"] = "display:none";
            divAddDeletePoint.Attributes["style"] = "display:none";
            divAddPoint.Attributes["style"] = "display:block";

            frmResponsePoint.Visible = false;
            frmResponseCard.Visible = false;
        }

        protected void dgrUnApprovedPoint_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgrUnApprovedPoint.CurrentPageIndex = e.NewPageIndex;
            loadAllPoint(txtFrom.Text, txtTo.Text);
            frmResponsePoint.Visible = false;
            resetAddPoint();
        }
    }
}