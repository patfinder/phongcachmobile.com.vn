﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class m_loai_phu_kien_mtb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadTitle();
        }

        private void loadTitle()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                litMobile.Text = this.Title = "Phụ kiện MTB " + CategoryController.getCategoryNameById(id);
            }
            else
                litMobile.Text = this.Title = "Phụ kiện MTB";
        }

        private void loadProductsByCategory(int categoryId)
        {
            DataTable dt = ProductController.getAccessoriesByCategory(categoryId);

            DataTable dtNew = dt;
            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("UrlName", typeof(string));
                dtNew.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-');

                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                        long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }
            }

            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadProductsByCategory(int.Parse(id));
            }
            else
            {
                loadProductsByCategory();
            }
        }

        private void loadProductsByCategory()
        {
            DataTable dt = ProductController.getAccessoriesByGroupId(8);
            DataTable dtNew = dt;

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("UrlName", typeof(string));
                dtNew.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).Replace(' ', '-');
                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                        long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }
                lstItem.DataSource = dtNew;
                lstItem.DataBind();
            }
        }
    }
}