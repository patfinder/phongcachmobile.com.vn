﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class m_quy_dinh_bao_hanh : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            this.Title = "Quy định bảo hành";
            litMGuaranteeAgreement.Text = LangMsgController.getLngMsg("litMGuaranteeAgreement.Text", "<span style='color:#be2603;font-weight:bold'>PHONGCACHMOBILE</span> chịu trách nhiệm đối với những sản phẩm chất lượng cao do chúng tôi phân phối, trường hợp hàng nhái / hàng giả, chúng tôi cam kết hoàn tiền <span style='color:#be2603;font-weight:bold'>GẤP ĐÔI</span> cho quý khách.<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>1. ĐỊA ĐIỂM BẢO HÀNH</span><br/>" +
                "Trung tâm Bảo hành PHONGCACHMOBILE: 58 Trần Quang Khải, P.Tân Định, Quận 1, Tp.HCM.<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>2. THỜI HẠN BẢO HÀNH</span><br />" +
                "- Nhóm 1 (máy phiên bản quốc tế): 12 tháng cho Mainboard.<br />" +
                "- Nhóm 2 (máy dành cho thị trường nội của Nhật Bản và Hàn Quốc): 03 tháng cho Mainboard.<br />" +
                "- Nhóm 3 (một số máy đặc biệt như model mới, model không có linh kiện sửa chữa tại Việt Nam...): Hỗ trợ 10% - 30% phí sửa chữa và thay thế linh kiện (linh kiện zin / linh kiện loại 1) trong 01 năm.<br />" +
                "- Phụ kiện kèm máy (pin, sạc, cáp, tai nghe): bảo hành 01 tháng<br />" +
                "<span style='color:#be2603;font-weight:bold'>ĐẶC BIỆT TRONG 7 NGÀY ĐẦU SỬ DỤNG ĐỐI VỚI MÁY THUỘC TẤT CẢ CÁC NHÓM:</span><br />" +
                "Trong 7 ngày đầu sử dụng, khách hàng sẽ được đổi máy mới trong trường hợp máy đã mua bị lỗi mainboard (hay còn gọi là lỗi phần cứng), thời gian đổi máy trong vòng 48h sau khi xác định chính xác lỗi thuộc về phần cứng. Khách hàng mua hàng từ xa sẽ được ưu đãi đặc biệt lên tới 15 ngày kể từ ngày chuyển phát.<br />" +
                "Riêng màn hình, cảm ứng và camera nếu trong 7 ngày đầu bị lỗi thì sẽ được thay thế miễn phí chứ không được áp dụng chính sách đổi máy.<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>3. HẠNG MỤC NGOẠI LỆ KHÔNG ĐƯỢC BẢO HÀNH</span><br />" +
                "Màn hình, Camera, Dây nguồn và Ổ cứng (bộ nhớ trong của máy) sẽ không được bảo hành miễn phí vì nguyên nhân hư hỏng thường bị tác động bởi các yếu tố bên ngoài như nhiệt độ, độ ẩm, nguồn điện, virus và cách sử dụng.<br />" +
                "Đối với các hạng mục không bảo hành miễn phí: Chúng tôi vẫn hỗ trợ 10% phí linh kiện thay thế và hoàn toàn không tính tiền công sửa chữa (với điều kiện hư hỏng không phải do lỗi sử dụng của khách hàng).<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>4. NHỮNG TRƯỜNG HỢP BỊ TỪ CHỐI BẢO HÀNH</span> <br />" +
                "- Máy không sử dụng đúng cách.<br />" +
                "- Máy mất nguồn (không lên nguồn), treo logo.<br />" +
                "- Máy có dấu hiệu của sự va chạm như vỏ và thân máy có vết cấn, vết nứt, vỡ, gãy, biến dạng.<br />" +
                "- Máy có dấu hiệu đã từng đặt trong trong môi trường nhiệt độ cao (ngoài nắng, gần lửa, các nguồn nhiệt…).<br />" +
                "- Máy có dấu hiệu sử dụng sai điện áp theo quy định của nhà sản xuất.<br />" +
                "- Lỗi phát sinh do virus tin học.<br />" +
                "- Máy có dấu hiệu bị ướt mưa, rơi vào nước, bị ẩm.<br />" +
                "- Khách hàng tự ý can thiệp vào bên trong máy như tự ý ci đặt & nâng cấp ROM, RAM và Firmware, tự ý bung tem mở máy hoặc nhờ nơi khác mở máy.<br />" +
                "- Máy trong tình trạng Security Code hoặc Sim Card (bị khoá máy).<br />" +
                "- Dán keo trên thân máy làm máy không toả nhiệt trong quá trình hoạt động (đặc biệt là trong quá trình dán keo, máy sẽ bị hơ lửa làm ảnh hưởng đến các thiết bị phía bên trong).<br />" +
                "- Không có phiếu bảo hành, phiếu bảo hành bị phai màu chữ hoặc có dấu hiệu tẩy xóa.<br />" +
                "- Máy không có tem của PHONGCACHMOBILE.<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>5. CÁC LƯU Ý KHÁC VỀ VẤN ĐỀ BẢO HÀNH</span> <br />" +
                "Để tránh những chuyện ngoài ý muốn, xin quý khách kiểm tra máy & phụ kiện trước khi rời khỏi showroom. Sau khi quý khách rời khỏi showroom, chúng tôi hoàn toàn không chịu trách nhiệm đối với việc thất lạc hoặc thiếu phụ kiện.<br />" +
                "Vấn đề điểm chết trên màn hình không được xem là lỗi kỹ thuật nếu như số điểm chết trên màn hình không vượt quá quy định cho phép do hãng sản xuất đã công bố (ít hơn hoặc bằng 3 điểm chết).<br />" +
                "Các lỗi về phần mềm chỉ được hỗ trợ khắc phục, không thuộc phạm vi bảo hành.<br />" +
                "Chỉ có các sản phẩm xách tay được hưởng chính sách bảo hành 1 năm mới được cung cấp thẻ bảo hành ngay khi khách hàng mua máy.<br />" +
                "Riêng sản phẩm Máy tính bảng và một vài model ĐTDĐ 'đặc biệt' sẽ được áp dụng chính sách bảo hành bị hạn chế: đổi máy mới trong 7 ngày sử dụng đầu tiên nếu có lỗi phần cứng và hỗ trợ 30% chi phí sửa chữa trong 1 năm (không có chính sách bảo hành miễn phí).<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>6. QUÝ KHÁCH Ở XA</span><br />" +
                "Cách thức gửi Bảo hành: Trong quá trình sử dụng nếu máy điện thoại bị lỗi, hỏng (trong điều khoản Bảo hành), Quý khách vui lòng giữ nguyên hiện trạng gửi về PHONGCACHMOBILE, ngay sau khi nhận được máy, chúng tôi sẽ cố gắng hết sức khắc phục trong thời gian nhanh nhất. Trước khi gửi xin vui lòng gọi điện thoại liên hệ trước với bộ phận Kỹ thuật & Bảo hành của PHONGCACHMOBILE.<br />" +
                "Chi phí vận chuyển: Quý khách không cần thanh toán chi phí vận chuyển máy bảo hành.<br />" +
                "Hỗ trợ trực tuyến: Trong quá trình sử dụng nếu máy có lỗi do cài đặt phần mềm, quý khách chỉ cần gọi điện tới bộ phận Kỹ thuật & Bảo hành của PHONGCACHMOBILE để được hướng dẫn xử lý trực tiếp.<br /><br />" +
                "<span style='color:#be2603;font-weight:bold'>7. ĐỐI VỚI HÀNG CÔNG TY VIỆT NAM</span><br />" +
                "Quý khách vui lòng mang đến các Trung tâm bảo hành chính hãng tại Việt Nam (Thông tin địa chỉ của các Trung tâm bảo hành được thể hiện trên các sổ bảo hành, sổ hướng dẫn và trên website của PHONGCACHMOBILE trong mục Hỗ Trợ).<br />" +
                "Đổi máy mới trong 24 giờ sử dụng đầu tiên nếu có lỗi phần cứng (với điều kiện máy không được tháo warranty và không trầy xước).");
        }
    }
}