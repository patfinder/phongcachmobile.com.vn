﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-loai-ud.aspx.cs" Inherits="PhongCachMobile.m_loai_ud" %>

<%@ Register Src="~/master/uc/ucTripleBanner.ascx" TagName="ucTripleBanner" TagPrefix="ucTripleBanner" %>
<%@ Register Src="~/master/uc/ucApplicationListBare.ascx" TagName="ucApplicationListBare" TagPrefix="ucApplicationListBare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <link href="css/category.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        
        <% if(!hideBanners) { %>
        <ucTripleBanner:ucTripleBanner ID="ucTripleBanner" bannerLeftItems="<%# topBannerLeftItems %>" 
            bannerRightItem1s="<%# topBannerRightItem1s %>" bannerRightItem2s="<%# topBannerRightItem2s %>" runat="server" />
        <% } %>

        <div class="product-list">
            <div class="product-other content-center">

                <div class="filter">
                    <div  class="label"><span>Tìm theo: </span></div>
                    <div class="dropdown" id="ddAppType">
                        <a data-target="#" value="<%= activeAppTypeID %>" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span><%= activeAppType %></span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <% foreach(PhongCachMobile.model.Article article in appTypes) { %>
                            <li><a href="javascript:;" value="<%= article.shortDesc %>"><%= article.title %></a></li>
                            <% } %>
                        </ul>
                    </div>
                    <div class="dropdown" id="ddSortView">
                        <a data-target="#" value="0" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Ngày cập nhật</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="javascript:;" value="1">Lượt xem</a></li>
                        </ul>
                    </div>
                    <div class="promotion raBrand" id="raiOS" value="iOS">
                        <i class="fa fa-circle"></i> <span>iOS (iPhone - iPAD) (<%= appleCount %>)</span>
                    </div>
                    <div class="promotion raBrand" id="raAndroid" value="Android">
                        <i class="fa fa-circle"></i> <span>Android (<%= samsungCount %>)</span>
                    </div>
                    <div class="promotion raBrand" id="raWindowsPhone" value="WindowsPhone">
                        <i class="fa fa-circle"></i> <span>Windows Phone (<%= windowsPhoneCount %>)</span>
                    </div>
                </div>
                <ul class="list-game clearfix">

                    <ucApplicationListBare:ucApplicationListBare ID="ucApplicationListBare" applications="<%# applications %>" runat="server" />

                </ul>
                <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= pageSize ? pageSize : remainCount %></span> sản phẩm</a>
            </div>
        </div>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">
        
    <% // ---------------------------- Footer ---------------------------- %>

    <script type="text/javascript">

        var pageSize = <%= pageSize %>;

        $(document).ready(function () {

            window.filterIDs = { ddAppType: "appType", ddSortView: "sortView" };

            // Apply filters handling
            applyFilterHandling("ddAppType", false, function(){
                loadMoreApps("GetApps", true, pageSize);
            });
            applyFilterHandling("ddSortView", false, function(){
                loadMoreApps("GetApps", true, pageSize);
            });

            var brandIDs = ["raiOS", "raAndroid", "raWindowsPhone"];

            // Brand filters
            $("div.raBrand").on("click", function () {

                var curID = $(this).attr("id");
                paramValues.os = $(this).attr("value");
                $(this).addClass("active");

                // clear "active" of all other than current
                $.each(brandIDs, function (index, id) {
                    if (id != curID) {
                        $("#" + id).removeClass("active");
                    }
                });

                loadMoreApps("GetApps", true, pageSize);
            });

            $(".read-more").on("click", function () {
                loadMoreApps("GetApps", false, pageSize);
            });

            paramValues.appType = $("#ddAppType>a").attr("value");
        });

        var paramValues = { appType: <%= activeAppTypeID %>, os: "Unknown", sortView: 0, page: 0 };

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.slide-banner').slick({
                slidesToShow: 1,
                variableWidth: false,
                autoplay: true,
                autoplaySpeed: 7000
            });
        });

    </script>

</asp:Content>
