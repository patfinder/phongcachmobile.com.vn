﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="gia-hot.aspx.cs" Inherits="PhongCachMobile.gia_hot" %>
<%@ Register Src="~/master/uc/ucBrandsLogoList.ascx" TagName="ucBrandsLogoList" TagPrefix="ucBrandsLogoList" %>
<%@ Register Src="~/master/uc/ucProductList.ascx" TagName="ucProductList" TagPrefix="ucProductList" %>
<%@ Register Src="~/master/uc/ucProductListBare.ascx" TagName="ucProductListBare" TagPrefix="ucProductListBare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Giá Hot</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        <div class="slide-banner-top">
            <% foreach (var article in topBannerItems) { %>
            <div class="items"><a href="<%= article.link %>"><img src="images/slide/<%= article.displayImg %>" alt="" /></a></div>
            <% } %>
        </div>
        
        <ucBrandsLogoList:ucBrandsLogoList ID="idUCBrandsLogoList" runat="server" />

        <div class="product-list">
            <div class="product-hot">
                <div class="content-center">
                    <%--<h2>Giá hot <span>(Sản phẩm giảm mạnh so với giá thị trường)</span>--%>
                    <h2>Giá hot <span>(Sản phẩm đang có giá khuyến mãi)</span>
                        <span class="sologan"><i class="fa fa-hand-o-right"></i><span id="slogan-hot-news" style="white-space:pre;"></span></span></h2>
                    <ucProductList:ucProductList ID="hotProductList" ProductList="<%# hotProducts %>" runat="server" />
                </div>
            </div>
            <div class="product-other list-5-items content-center">
                <h2>Vừa giảm giá
                    <span class="sologan"><i class="fa fa-hand-o-right"></i><span id="slogan-market-news" style="white-space:pre;"></span></span></h2>

                <ul>
                    <ucProductListBare:ucProductListBare ID="ucFilterPhoneList" HideHotProduct="true" ProductList="<%# discountProducts %>" runat="server" />
                </ul>

                <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= pageSize ? pageSize : remainCount %></span> sản phẩm</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">
        
    <% // ---------------------------- Footer ---------------------------- %>
        
    <script type="text/javascript">

        var pageSize = <%= pageSize %>;

        $(document).ready(function () {

            $(".read-more").on("click", function () {
                loadMoreProducts("GetDiscountProducts", false, pageSize);
            });
        });

        var paramValues = { page: 0 };

    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('.slide-banner-top').slick({
                slidesToShow: 1,
                variableWidth: false,
                autoplay: true,
                autoplaySpeed: 7000
            });
            $("#slogan-hot-news").typed({
                strings: [<%= string.Join(",", promotionArticles.Select(article => "\"<a href='" + article.link + "'>" + article.title + "</a>\"")) %>],
                typeSpeed: 70,
                backDelay: 5000,
                loop: true,
                contentType: 'html',
                loopCount: 100
            });
            $("#slogan-market-news").typed({
                strings: [<%= string.Join(",", marketNews.Select(article => "\"<a href='" + article.link + "'>" + article.title + "</a>\"")) %>],
                typeSpeed: 50,
                backDelay: 5000,
                loop: true,
                contentType: 'html',
                loopCount: 100
            });
        });

    </script>

</asp:Content>
