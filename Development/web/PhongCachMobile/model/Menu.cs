﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Menu
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public int groupId { get; set; }
        public string text { get; set; }
        public string actionUrl { get; set; }
        public string displayImage { get; set; }
        public bool flag { get; set; }
        public int inOrder { get; set; }

        public Menu(int parentId, int groupId, string text, string actionUrl, string displayImage, bool flag, int inOrder)
        {
            this.parentId = parentId;
            this.groupId = groupId;
            this.text = text;
            this.actionUrl = actionUrl;
            this.displayImage = displayImage;
            this.flag = flag;
            this.inOrder = inOrder;
        }

        public Menu(int id, int parentId, int groupId, string text, string actionUrl, string displayImage, bool flag, int inOrder)
        {
            this.id = id;
            this.parentId = parentId;
            this.groupId = groupId;
            this.text = text;
            this.actionUrl = actionUrl;
            this.displayImage = displayImage;
            this.flag = flag;
            this.inOrder = inOrder;
        }
    }
}