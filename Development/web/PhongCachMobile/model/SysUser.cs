﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class SysUser
    {
        public int id { get; set; }
        public string alias { get; set; }
        public string avatar { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public bool gender { get; set; }
        public string address { get; set; }
        public string DOB { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string tel { get; set; }
        public string stateId { get; set; }
        public bool flag { get; set; }
        public string remark { get; set; }
        public string password { get; set; }
        public string activatedToken { get; set; }
        public string registeredDate { get; set; }
        public string activatedDate { get; set; }

        public int groupId { get; set; }
      
        public SysUser()
        {
        }

        public SysUser(string alias, string firstName, string lastName, bool gender, string address, string DOB, string email, string mobile, string tel, string stateId,
            bool flag, int groupId)
        {
            this.alias = alias;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
            this.address = address;
            this.DOB = DOB;
            this.email = email;
            this.mobile = mobile;
            this.tel = tel;
            this.stateId = stateId;
            this.flag = flag;
            this.groupId = groupId;
        }

        public SysUser(string alias, string firstName, string lastName, bool gender, string address, string DOB, string email, string mobile, string tel, string stateId,
          string identityName, string identityNumber)
        {
            this.alias = alias;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
            this.address = address;
            this.DOB = DOB;
            this.email = email;
            this.mobile = mobile;
            this.tel = tel;
            this.stateId = stateId;
        }
    }
}