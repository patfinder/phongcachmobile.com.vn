﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Slider
    {
        public int id { get; set; }
        public string displayImg { get; set; }
        public string thumbImg { get; set; }
        public string text { get; set; }
        public string actionUrl { get; set; }
        public string postedDate { get; set; }
        public bool flag { get; set; }
        public int groupId { get; set; }
        public string title { get; set; }

        public Slider()
        {
        }

        public Slider(int id, string displayImg, string thumbImg, string text, string actionUrl, string postedDate, bool flag, int groupId, string title)
        {
            this.id = id;
            this.displayImg = displayImg;
            this.thumbImg = thumbImg;
            this.text = text;
            this.actionUrl = actionUrl;
            this.postedDate = postedDate;
            this.flag = flag;
            this.groupId = groupId;
            this.title = title;
        }

        public Slider(string displayImg, string thumbImg, string text, string actionUrl, string postedDate, bool flag, int groupId, string title)
        {
            this.displayImg = displayImg;
            this.thumbImg = thumbImg;
            this.text = text;
            this.actionUrl = actionUrl;
            this.postedDate = postedDate;
            this.flag = flag;
            this.groupId = groupId;
            this.title = title;
        }

        public Article toArticle()
        {
            return new Article
            {
                id = this.id,
                title = this.title,
                //shortDesc = this.shortDesc,
                longDesc = this.text,
                displayImg = this.displayImg,
                //viewCount = this.viewCount,
                flag = this.flag,
                postedDate = this.postedDate,
                groupId = this.groupId,
                //categoryId = this.categoryId,
                //productId = this.productId,
                //remark = this.remark,
                link = this.actionUrl,
                //CommentCount = this.CommentCount,
            };
        }
    }
}