﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Point
    {
        public int id { get; set; }
        public int userId { get; set; }
        public string code { get; set; }
        public string postedDate { get; set; }
        public bool isApproved { get; set; }
        public int cardId { get; set; }
    }
}