﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Testimonial
    {
        public int id { get; set; }
        public string name { get; set; }
        public string text { get; set; }
        public string postedDate { get; set; }
        public string shortDesc { get; set; }
        public string displayImage { get; set; }

    }
}