﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Group
    {
        public int id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string description { get; set; }
        public string filter { get; set; }
        public string imagePath { get; set; }

        public Group(string name, string value, string description, string filter, string imagePath)
        {
            this.name = name;
            this.value = value;
            this.description = description;
            this.filter = filter;
            this.imagePath = imagePath;
        }
    }
}