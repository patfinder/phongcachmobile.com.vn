﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Video
    {
        public int id { get; set; }
        public string video { get; set; }
        public int productId { get; set; }
        public string title { get; set; }
    }
}