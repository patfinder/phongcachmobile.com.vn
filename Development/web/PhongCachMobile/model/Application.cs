﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Application
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool isFree { get; set; }
        public string description { get; set; }
        public int categoryId { get; set; }
        public int likeCount { get; set; }
        public int viewCount { get; set; }
        public string displayImage { get; set; }
        public string currentVersion { get; set; }
        public string developer { get; set; }
        public string postedDate { get; set; }
        public string requiredOS { get; set; }
        public int osId { get; set; }
        // Filename now become file URL
        public string filename { get; set; }
        public string filesize { get; set; }
        // Detail link
        public string link { get; set; }
        // Download link
        public string download { get; set; }
    }
}