﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Comment
    {
        public int id { get; set; }
        public int parentId { get; set; }
        public string description { get; set; }
        public string postedDate { get; set; }
        public int likeCount { get; set; }
        public int userId { get; set; }
        public int articleId { get; set; }
        public int productId { get; set; }

        public Comment(int id, string description)
        {
            this.id = id;
            this.description = description;
        }


        public Comment(int parentId, string description, string postedDate, int likeCount, int userId, int articleId, int productId)
        {
            this.parentId = parentId;
            this.description = description;
            this.postedDate = postedDate;
            this.likeCount = likeCount;
            this.userId = userId;
            this.articleId = articleId;
            this.productId = productId;
        }

        public Comment(int id, int parentId, string description, string postedDate, int likeCount, int userId,  int articleId, int productId)
        {
            this.id = id;
            this.parentId = parentId;
            this.description = description;
            this.postedDate = postedDate;
            this.likeCount = likeCount;
            this.userId = userId;
            this.articleId = articleId;
            this.productId = productId;
        }
    }
}