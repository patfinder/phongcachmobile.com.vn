﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Lang
    {
        public int id {get; set;}
        public string name { get; set; }
        public string value { get; set; }

        public Lang(string name, string value)
        {
            this.name = name;
            this.value = value;
        }
    }
   

}