﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string displayImg { get; set; }
        public int parentId { get; set; }
        public int groupId { get; set; }
        public int inOrder { get; set; }
        public bool display { get; set; }

        public Category(int id, string name, string description, string displayImg, int parentId, int groupId, int inOrder, bool display)
        {
            this.id = id;
            this.name = name;
            this.description = description;
            this.displayImg = displayImg;
            this.parentId = parentId;
            this.groupId = groupId;
            this.inOrder = inOrder;
            this.display = display;
        }

        public Category(string name, string description, string displayImg, int parentId, int groupId, int inOrder, bool display)
        {
            this.name = name;
            this.description = description;
            this.displayImg = displayImg;
            this.parentId = parentId;
            this.groupId = groupId;
            this.inOrder = inOrder;
            this.display = display;
        }
    }
}