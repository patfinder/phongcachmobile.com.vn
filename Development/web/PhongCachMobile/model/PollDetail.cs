﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class PollDetail
    {
        public int id { get; set; }
        public int pollId { get; set; }
        public string name { get; set; }
        public int value { get; set; }
    }
}