﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class LogInUserResult
    {
        public static int STATUS_VALID_CREDENTIAL_CODE = 0;
        public static string STATUS_VALID_CREDENTIAL_TEXT = "Ok";

        public static int STATUS_INACTIVATED_USER_CODE = STATUS_VALID_CREDENTIAL_CODE + 1;
        public static string STATUS_INACTIVATED_USER_TEXT = "Tài khoản chưa được kích hoạt.";

        public static int STATUS_INVALID_CREDENTIAL_CODE = STATUS_INACTIVATED_USER_CODE + 1;
        public static string STATUS_INVALID_CREDENTIAL_TEXT = "Sai email / mật khẩu.";

        public static int STATUS_GENERAL_ERROR_CODE = STATUS_INVALID_CREDENTIAL_CODE + 1;
        public static string STATUS_GENERAL_ERROR_TEXT = "Lỗi xảy ra trong quá trình đăng nhập tài khoản.";

        public static int STATUS_DISABLE_USER_ERROR_CODE = STATUS_GENERAL_ERROR_CODE + 1;
        public static string STATUS_DISABLE_USER_ERROR_TEXT = "Tài khoản bị chặn";

        public int statusCode { get; set; }
        public string statusText { get; set; }

        public LogInUserResult(int statusCode, string statusText)
        {
            this.statusCode = statusCode;
            this.statusText = statusText;
        }
    }
}