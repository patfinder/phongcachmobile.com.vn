﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Answer
    {
         public int id { get; set; }
        public int parentId { get; set; }
        public string description { get; set; }
        public string postedDate { get; set; }
        public int likeCount { get; set; }
        public int userId { get; set; }
        public int askId { get; set; }

        public Answer(int id, string description)
        {
            this.id = id;
            this.description = description;
        }


        public Answer(int parentId, string description, string postedDate, int likeCount, int userId, int askId)
        {
            this.parentId = parentId;
            this.description = description;
            this.postedDate = postedDate;
            this.likeCount = likeCount;
            this.userId = userId;
            this.askId = askId;
        }

        public Answer(int id, int parentId, string description, string postedDate, int likeCount, int userId, int askId)
        {
            this.id = id;
            this.parentId = parentId;
            this.description = description;
            this.postedDate = postedDate;
            this.likeCount = likeCount;
            this.userId = userId;
            this.askId = askId;
        }
    }
}