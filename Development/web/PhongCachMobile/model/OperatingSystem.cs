﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class OperatingSystem
    {
        public int id { get; set; }
        public string name { get; set; }
        public int groupId { get; set; }

    }
}