﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class FAQ
    {
        public int id { get; set; }
        public string ask { get; set; }
        public string answer { get; set; }
    }
}