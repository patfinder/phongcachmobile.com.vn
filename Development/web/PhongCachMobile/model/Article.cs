﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Article
    {
        public int id { get; set; }
        public string title { get; set; }
        public string shortDesc { get; set; }
        public string longDesc { get; set; }
        public string displayImg { get; set; }
        public int viewCount { get; set; }
        public bool flag { get; set; }
        public string postedDate { get; set; }
        public int groupId { get; set; }
        public int categoryId { get; set; }
        public int productId { get; set; }
        public string remark { get; set; }
        public string link { get; set; }
        public int CommentCount { get; set; }

        public Article()
        {
        }

        public Article(int id, string title, string shortDesc, string longDesc, string displayImg, int viewCount, bool flag, string postedDate, int groupId, int categoryId, int productId)
        {
            this.id = id;
            this.title = title;
            this.shortDesc = shortDesc;
            this.longDesc = longDesc;
            this.displayImg = displayImg;
            this.viewCount = viewCount;
            this.flag = flag;
            this.postedDate = postedDate;
            this.groupId = groupId;
            this.categoryId = categoryId;
            this.productId = productId;
        }

        public Article(string title, string shortDesc, string longDesc, string displayImg, int viewCount, bool flag, string postedDate, int groupId, int categoryId, int productId)
        {
            this.title = title;
            this.shortDesc = shortDesc;
            this.longDesc = longDesc;
            this.displayImg = displayImg;
            this.viewCount = viewCount;
            this.flag = flag;
            this.postedDate = postedDate;
            this.groupId = groupId;
            this.categoryId = categoryId;
            this.productId = productId;
        }
    }
}