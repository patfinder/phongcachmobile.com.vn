﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Ask
    {
        public int id { get; set; }
        public string ask { get; set; }
        public int userId { get; set; }
        public int viewCount { get; set; }
        public string title { get; set; }
        public bool isSolved { get; set; }
        public string postedDate { get; set; }

    }
}