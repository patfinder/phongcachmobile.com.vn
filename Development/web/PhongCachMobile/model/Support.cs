﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Support
    {
        public int id { get; set; }
        public string name { get; set; }
        public string yahooMessenger { get; set; }
        public string skype { get; set; }
        public int groupId { get; set; }
        public string phone { get; set; }
    }
}