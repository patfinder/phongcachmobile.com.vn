﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Product
    {
        public int id { get; set; }
        public string name { get; set; }
        public string firstImage { get; set; }
        public string secondImage { get; set; }
        public string thirdImage { get; set; }
        public string fourthImage { get; set; }
        public string fifthImage { get; set; }
        public string firstImageDesc { get; set; }
        public string secondImageDesc { get; set; }
        public string thirdImageDesc { get; set; }
        public string fourthImageDesc { get; set; }
        public string fifthImageDesc { get; set; }
        public List<string> allImages { get; set; }
        public List<string> allImageDescs { get; set; }
        public string shortDesc { get; set; }
        public string longDesc { get; set; }
        public string include { get; set; }
        public string outstanding { get; set; }
        public string remark { get; set; }
        public bool flag { get; set; }
        public bool display { get; set; }
        public int viewCount { get; set; }
        public double rate { get; set; }
        public int categoryId { get; set; }
        public bool isFeatured { get; set; }
        public bool isSpeedySold { get; set; }
        public bool isCompanyProduct { get; set; }
        public bool isComingSoon { get; set; }
        public bool isOutOfStock { get; set; }
        public bool isGifted { get; set; }
        public int currentPrice { get; set; }
        public string currentPriceStr { get; set; }
        public int discountPrice { get; set; }
        public string discountPriceStr { get; set; }
        public int osId { get; set; }
        public int groupId { get; set; }
        public string video { get; set; }
        public string discountDesc { get; set; }
        public string hotDesc { get; set; }
        public string link { get; set; }
        public bool oldProduct { get; set; }
        public string suggestedAccessories { get; set; }
        public bool useDarkBackground { get; set; }
        public string backgroundImage { get; set; }
        public DateTime? lastPriceUpdate { get; set; }
    }
}