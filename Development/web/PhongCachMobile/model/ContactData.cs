﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class ContactData
    {
        public int id { get; set; }
        public string senderName { get; set; }
        public string senderEmail { get; set; }
        public string description { get; set; }
        public string subject { get; set; }
        public string postedDate { get; set; }

        public ContactData(int id, string senderName, string senderEmail, string description, string subject, string postedDate)
        {
            this.id = id;
            this.senderName = senderName;
            this.senderEmail = senderEmail;
            this.description = description;
            this.subject = subject;
            this.postedDate = postedDate;
        }

        public ContactData(string senderName, string senderEmail, string description, string subject, string postedDate)
        {
            this.senderName = senderName;
            this.senderEmail = senderEmail;
            this.description = description;
            this.subject = subject;
            this.postedDate = postedDate;
        }
    }
}