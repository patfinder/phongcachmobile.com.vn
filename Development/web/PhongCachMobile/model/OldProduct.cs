﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class OldProduct
    {
        public int id { get; set; }
        public string name { get; set; }
        public string displayImage { get; set; }
        public string description { get; set; }
        public int originalPrice { get; set; }
        public int currentPrice { get; set; }
       
    }
}