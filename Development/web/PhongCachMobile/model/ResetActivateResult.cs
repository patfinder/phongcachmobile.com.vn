﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class ResetActivateResult
    {
        public static int STATUS_VALID_RESET_CODE = 0;
        public static string STATUS_VALID_RESET_TEXT = "Ok";

        public static int STATUS_ALREADY_ACTIVATED_USER_CODE = STATUS_VALID_RESET_CODE + 1;
        public static string STATUS_ALREADY_ACTIVATED_USER_TEXT = "Tài khoản đã được kích hoạt.";

        public static int STATUS_GENERAL_ERROR_CODE = STATUS_ALREADY_ACTIVATED_USER_CODE + 1;
        public static string STATUS_GENERAL_ERROR_TEXT = "Lỗi xảy ra trong quá trình gửi lại mã kích hoạt.";

        public int statusCode { get; set; }
        public string statusText { get; set; }

        public ResetActivateResult(int statusCode, string statusText)
        {
            this.statusCode = statusCode;
            this.statusText = statusText;
        }
    }
}