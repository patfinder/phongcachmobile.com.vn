﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class ActivateUserResult
    {
        public static int STATUS_VALID_TOKEN_CODE = 0;
        public static string STATUS_VALID_TOKEN_TEXT = "Ok";

        public static int STATUS_INVALID_TOKEN_CODE = STATUS_VALID_TOKEN_CODE + 1;
        public static string STATUS_INVALID_TOKEN_TEXT = "Sai mã kích hoạt tài khoản.";

        public static int STATUS_ALREADY_ACTIVATED_CODE = STATUS_INVALID_TOKEN_CODE + 1;
        public static string STATUS_ALREADY_ACTIVATED_TEXT = "Tài khoản đã được kích hoạt.";

        public static int STATUS_EXPIRED_TIME_CODE = STATUS_ALREADY_ACTIVATED_CODE + 1;
        public static string STATUS_EXPIRED_TIME_TEXT = "Thời gian kích hoạt đã kết thúc.";

        public static int STATUS_GENERAL_ERROR_CODE = STATUS_EXPIRED_TIME_CODE + 1;
        public static string STATUS_GENERAL_ERROR_TEXT = "Lỗi xảy ra trong quá trình kích hoạt tài khoản.";

        public int statusCode { get; set; }
        public string statusText { get; set; }

        public ActivateUserResult(int statusCode, string statusText)
        {
            this.statusCode = statusCode;
            this.statusText = statusText;
        }
    }
}