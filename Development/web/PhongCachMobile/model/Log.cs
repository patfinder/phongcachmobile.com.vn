﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Log
    {
        public int id { get; set; }
        public string time { get; set; }
        public string description { get; set; }
        public int status { get; set; }
        public string text { get; set; }

        public Log(string time, string description, int status, string text)
        {
            // TODO: Complete member initialization
            this.time = time;
            this.description = description;
            this.status = status;
            this.text = text;
        }
    }
}