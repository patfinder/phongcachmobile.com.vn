﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class ParallaxSection
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Introduction { get; set; }
        public string Background { get; set; }
    }
}