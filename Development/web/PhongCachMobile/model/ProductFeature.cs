﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class ProductFeature
    {
        public int id { get; set; }
        public int productId { get; set; }
        public int featureId { get; set; }
        public string value { get; set; }
    }
}