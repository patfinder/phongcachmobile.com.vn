﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhongCachMobile.model
{
    public class Card
    {
        public int id { get; set; }
        public string code { get; set; }
        public string brandNumber { get; set; }
        public int status { get; set; }
        public string postedDate { get; set; }

    }
}