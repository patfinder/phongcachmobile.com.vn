﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using System.Text.RegularExpressions;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class m_chi_tiet_tin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Request.QueryString["id"]))
                id = int.Parse(Request.QueryString["id"].Split(new[] { '-' }).Last());

            if (!this.IsPostBack)
            {
                updateCounter();
                loadNews();
            }

            // Promotion / Market News
            var isPromotionNews = article.categoryId == 56;
            articleType = new Article()
            {
                title = isPromotionNews ? "Tin PR - Khuyến mại" : "Tin thị trường",
                link = Common.m() + (isPromotionNews ? "tin-khuyen-mai" : "tin-thi-truong"),
            };

            this.Title += " - " + article.title;
        }

        protected int id;
        protected Article articleType = new Article();

        protected Article article = new Article();
        protected void loadNews()
        {
            DataTable dt = ArticleController.getArticleById(id.ToString());
            if (dt != null && dt.Rows.Count > 0)
            {
                article = ArticleController.rowToArticle(dt.Rows[0]);
                dt = CommentController.getNumberOfCommentsByArticleIDs(new[] { article.id }.ToList());
                if (dt != null && dt.Rows.Count > 0)
                    article.CommentCount = (int)dt.Rows[0]["CommentCount"];
            }
        }

        private void updateCounter()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);

                // update counter
                if (Session["CurrentArticleId"] == null || Session["CurrentArticleId"].ToString() != id)
                {
                    ArticleController.addViewCount(id);
                    Session["CurrentArticleId"] = id;
                }
            }
        }
    }
}