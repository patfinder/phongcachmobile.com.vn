﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-tin-khuyen-mai.aspx.cs" Inherits="PhongCachMobile.m_tin_khuyen_mai" %>

<%@ Register Src="~/master/uc/ucNewsListBare.ascx" TagName="ucNewsListBare" TagPrefix="ucNewsListBare" %>
<%@ Register Src="~/master/uc/ucDigitalToys.ascx" TagName="ucDigitalToys" TagPrefix="ucDigitalToys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="content">

        <div class="content">
            <div class="breadcrumb">
                <div class="content-center">
                    <ol>
                        <li><a href="/">Trang chủ</a><i class="fa fa-angle-right"></i></li>
                        <li>Tin PR - Khuyến mại</li>
                    </ol>
                </div>
            </div>
            <section class="page-new">
                <div class="group-infor content-center clearfix">
                    <div class="product-other content-main">
                        <ul class="clearfix" id="newsList">

                            <ucNewsListBare:ucNewsListBare ID="ucNewsListBare" NewsList="<%# newsList %>" runat="server" />

                        </ul>
                        <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= 10 ? 10 : remainCount %></span> tin</a>
                    </div>

                    <%--<ucDigitalToys:ucDigitalToys ID="ucDigitalToys" runat="server" />--%>

                </div>
            </section>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">

    <script type="text/javascript">

        $(document).ready(function () {

            $(".read-more").on("click", function () {
                loadMoreArticles("GetPromotions", false, 10);
            });
        });

        var paramValues = { page: 0, pageSize: 10 };

    </script>

</asp:Content>