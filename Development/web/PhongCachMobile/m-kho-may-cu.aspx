﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-kho-may-cu.aspx.cs" Inherits="PhongCachMobile.m_kho_may_cu" %>

<%@ Register Src="~/master/uc/ucTripleBanner.ascx" TagName="ucTripleBanner" TagPrefix="ucTripleBanner" %>
<%@ Register Src="~/master/uc/ucProductListBare.ascx" TagName="ucProductListBare" TagPrefix="ucProductListBare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="content">
        
        <ucTripleBanner:ucTripleBanner ID="ucTripleBanner" bannerLeftItems="<%# topBannerLeftItems %>" 
            bannerRightItem1s="<%# topBannerRightItem1s %>" bannerRightItem2s="<%# topBannerRightItem2s %>" runat="server" />

        <div class="product-list">
            <div class="product-other content-center">

                <div class="filter">
                    <div  class="label"><span>Tìm theo: </span></div>
                    <div class="dropdown" id="ddBrand">
                        <a data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Hãng sản xuất</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <% foreach(KeyValuePair<string, int> kv in brands) { %>
                            <li><a href="javascript:;" value="<%= kv.Value %>"><%= kv.Key %></a></li>
                            <% } %>
                        </ul>
                    </div>
                    <div class="dropdown" id="ddPrice">
                        <a data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Mức giá</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <% foreach(KeyValuePair<string, int> kv in prices) { %>
                            <li><a href="javascript:;" value="<%= kv.Value %>"><%= kv.Key %></a></li>
                            <% } %>
                        </ul>
                    </div>
                    <div class="dropdown sort" id="ddSortPrice">
                        <a data-target="#" value="1" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Giá từ Cao đến Thấp</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="javascript:;" value="0">Giá từ Thấp đến Cao</a></li>
                        </ul>
                    </div>
                </div>
                <ul>
                    <ucProductListBare:ucProductListBare ID="ucFilterPhoneList" ProductList="<%# filterPhoneList %>" OldProduct="true" runat="server" />
                </ul>
                <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= pageSize ? pageSize : remainCount %></span> sản phẩm</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">
        
    <% // ---------------------------- Footer ---------------------------- %>

    <script type="text/javascript">

        var pageSize = <%= pageSize %>;

        $(document).ready(function () {
            $('.slide-banner').slick({
                        slidesToShow: 1,
                        variableWidth:false,
                        autoplay: true,
                        autoplaySpeed: 7000,
                        arrows: false,
                        dots: false

                    });

            window.filterIDs = { ddBrand: "brand", ddPrice: "price", ddSortPrice: "sortPrice" };

            // Apply filters handling
            applyFilterHandling("ddBrand", false, function(){
                loadMoreProducts("GetOldProducts", true, pageSize);
            });
            applyFilterHandling("ddPrice", false, function(){
                loadMoreProducts("GetOldProducts", true, pageSize);
            });
            applyFilterHandling("ddSortPrice", true, function(){
                loadMoreProducts("GetOldProducts", true, pageSize);
            });

            $(".read-more").on("click", function () {
                loadMoreProducts("GetOldProducts", false, pageSize);
            });
        });

        var paramValues = { group: -1, brand: -1, price: -1, os: -1, promotion: 0, sortPrice: 1, page: 0 };

    </script>

</asp:Content>
