﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class gia_hot_event : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadTopSlider();
                //loadHotPrd();
                loadPrdNews();
                loadPoll();
                loadDiscountNews();

            }
        }

        private void loadDiscountNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "56");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.StripTagsRegex(dr["Title"].ToString()).ToLower().Replace(' ', '-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = " cách đây " + ts.Hours + " giờ.";
                    //else
                    //    dr["TimeSpan"] = " cách đây " + ts.Days + " ngày";
                }
                lstDiscountNews.DataSource = dt;
                lstDiscountNews.DataBind();
            }

        }

        private void loadPrdNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "55");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.StripTagsRegex(dr["Title"].ToString()).ToLower().Replace(' ', '-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = " cách đây " + ts.Hours + " giờ.";
                    //else
                    //    dr["TimeSpan"] = " cách đây " + ts.Days + " ngày";
                }
                lstPrdNews.DataSource = dt;
                lstPrdNews.DataBind();
            }
        }

        private void loadPoll()
        {
            DataTable dtPoll = PollController.getLastest(DateTime.Now.ToString("yyyyMMddHHmmss"));
            if (dtPoll != null && dtPoll.Rows.Count > 0)
            {
                litPollName.Text = dtPoll.Rows[0]["Name"].ToString();

                DataTable dtPollDetail = PollDetailController.getPollDetailByPollId(dtPoll.Rows[0]["Id"].ToString());
                if (dtPollDetail != null && dtPollDetail.Rows.Count > 0)
                {
                    int sum = 0;
                    foreach (DataRow drPollDetail in dtPollDetail.Rows)
                    {
                        sum += int.Parse(drPollDetail["Value"].ToString());
                    }

                    dtPollDetail.Columns.Add("Percent", typeof(double));
                    foreach (DataRow drPollDetail in dtPollDetail.Rows)
                    {
                        rblPoll.Items.Add(new ListItem(drPollDetail["Name"].ToString(), drPollDetail["Id"].ToString()));
                        drPollDetail["Percent"] = Math.Round(int.Parse(drPollDetail["Value"].ToString()) * 100.0 / sum, 0);
                    }

                    rblPoll.SelectedIndex = 0;

                    lstPollAnswer.DataSource = dtPollDetail;
                    lstPollAnswer.DataBind();
                }

                if (Session["Poll"] == null || Session["Poll"].ToString() == "false")
                {
                    lstPollAnswer.Visible = false;
                    rblPoll.Visible = true;
                }
                else
                {
                    lstPollAnswer.Visible = true;
                    rblPoll.Visible = false;
                    btnVote.Visible = false;
                }
            }
        }

        private void loadHotPrd()
        {
            //int number = int.Parse(SysIniController.getSetting("Number_New_Product", "6"));
            //DataTable dt = ProductController.getDiscountProducts();
            //litHotPrd.Text = "";
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    dt.Columns.Add("UrlName", typeof(string));
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        DataRow dr = dt.Rows[i];
            //        if (i % 6 == 0)
            //            litHotPrd.Text += "<ul class='products-grid'>";
            //        litHotPrd.Text += "<li style='width:250px;min-height:564px;' class='" + (i % 3 == 2 ? "last" : "") + "'>" +
            //            "<div class='topbg2'>" +
            //                                "<div class='title'>" +
            //                            "<p style='text-align:left;'><span style='font-family:Open Sans;font-size:16px;color:white;'><a class='link1' href='sp-" + HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString() + "' title='" + dr["Name"].ToString() + "'>" +
            //                            (dr["Name"].ToString().Length > 24 ? dr["Name"].ToString().Substring(0, 22) + "..." : dr["Name"].ToString()) + "</a></span></p>" +
            //                            "</div>" +
            //                            "<div class='subtitle'>" +
            //                            "<p style='text-align:left;'><span style='font-family:Open Sans;font-size:14px;color:white;'>Giá gốc: </span><span style='font-family:Open Sans;font-size:14px;color:white;text-decoration:line-through;'>" +
            //                            int.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") + "đ</span></p>" +
            //                            "</div>" +

            //                            "</div>" +
            //        "<div class='item'>" + "<a href='sp-" + HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString() +
            //            "' title='" + dr["Name"].ToString() + "' class='product-image'>";
            //        litHotPrd.Text += "<img src='images/product/" + dr["FirstImage"].ToString() + "' width='203px' style='max-height:203px' alt='" + dr["Name"].ToString() + "'/></a>";
            //        litHotPrd.Text += "<div class='price-box'><span class='regular-price'><span class='price'>" +
            //            int.Parse(dr["DiscountPrice"].ToString() == "" ? dr["CurrentPrice"].ToString() : dr["DiscountPrice"].ToString()).ToString("#,##0") + " đ</span>" +
            //            "</span></div>";
            //        //litHotPrd.Text += "<h3 class='product-name'>" +
            //        //    "<a href='sp-" + dr["Name"].ToString().ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString() + "' title='" + dr["Name"].ToString() + "'>" +
            //        //    dr["Name"].ToString() + "</a></h3>";
            //        litHotPrd.Text += "<div class='hotdesc'>" + dr["HotDesc"].ToString() + dr["DiscountDesc"].ToString() + "</div>";
            //        litHotPrd.Text += "<div class='clear'></div>";
            //        litHotPrd.Text += "<div class='actions'>" +
            //            "<a href='sp-" + HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString() + "' " +
            //            "class='button'><span><span>Chi tiết</span></span></a> " +
            //            "</div></div></li>";
            //        if (i % 6 == 5)
            //            litHotPrd.Text += "</ul>";

            //    }
            //}
        }

        private void loadTopSlider()
        {
            litTopSlider.Text = litTopSliderCaption.Text = "";
            DataTable dt = SliderController.getSliders("35"); //GroupId Top Slider
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    litTopSlider.Text += "<a href='" + dr["ActionUrl"].ToString() + "'><img src='images/slide/" + dr["DisplayImage"].ToString() + "' alt='' title='#sliderCaption" + i + "' /></a>";
                    litTopSliderCaption.Text += "<div id='" + "sliderCaption" + i + "' class='nivo-html-caption'>" + dr["Text"].ToString() + "</div>";
                }
            }
        }

        private void loadLang()
        {
            //this.Title = litSubtitle.Text = string.Format(LangController.getLng("litHotProduct.Subtitle", "Sản phẩm giá hot chỉ có trong ngày <font color='red'>{0}</font>"));
            litYourVote.Text = LangController.getLng("litYourVote.Text", "Ý kiến từ người dùng");
            //litLastestNews.Text = LangController.getLng("litLastestNews.Text", "Tin tức mới nhất");
            //litToday.Text = DateTime.Now.ToShortDateString();
            //this.Title = "Giá hot trong ngày " + litToday.Text;
        }

        protected void btnVote_Click(object sender, EventArgs e)
        {
            Session["Poll"] = "true";
            PollDetailController.increaseById(rblPoll.SelectedValue);
            loadPoll();
        }
    }
}