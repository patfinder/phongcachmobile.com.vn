﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="sua-chua-cai-dat.aspx.cs" Inherits="PhongCachMobile.sua_chua_cai_dat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content page-satic">
        <div class="content-banner product-list">
            <div class="content-center clearfix">
                <h2>Giới thiệu</h2>
                <p>Được sự lựa chọn tuyển dụng và sàng lọc gắt gao, kỹ thuật viên của <span class="color-red">PHONGCACHMOBILE</span> - đội ngũ Kỹ Thuật với bề dày trên 10 năm kinh nghiệm kết hợp với tinh thần học hỏi và nghiên cứu, đặc biệt, được sự công tác và hỗ trợ tối đa từ các Trung tâm bảo hành ủy quyền chính hãng Sony, Samung, HTC ... đã giúp cho PHONGCACHMOBILE có thể tự hào đảm nhận việc sửa chữa được những 'bệnh' khó của di động đặc biệt là những dòng cao cấp hiện nay của các hãng danh tiếng như iPhone, HTC, Nokia, Sony, Samsung, LG, Motorola, Blackberry...</p>

                <p>Ngoài ra, được sự hậu thuẫn từ hãng Pantech Vega (Hàn Quốc) cùng các nhà phân phối ủy quền tại VN, PHONGCACHMOBILE luôn sẵng sàng những linh kiện sửa chữa chính hãng phục vụ việc bảo hành và sửa chữa các dòng máy cao cấp của Vega.</p>

                <p>Đặc biệt, PHONGCACHMOBILE là đơn vị duy nhất có thể nhận <span class="color-red">sửa chữa các dòng tai nghe bluetooth cao cấp</span> của các thương hiệu nổi tiếng toàn cầu.</p>

                <p>Chúng tôi hân hạnh được phục vụ tất cả quý khách với tinh thần và trách nhiệm cao nhất.</p>
            </div>
        </div>


        <div class="product-list">
            <div class="content-center">
                <h2>Bảng giá apple</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>iPhone 4/4s</td>
                            <td>250.000đ</td>
                            <td>750.000đ</td> 
                        </tr>
                        <tr>
                            <td>iPhone 5/5s</td>
                            <td>350.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>iPhone 6</td>
                            <td>850.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>iPhone 6 Plus</td>
                            <td>950.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>iPad 2,3,4</td>
                            <td>900.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>iPad Air</td>
                            <td>1.200.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>iPad Air 2</td>
                            <td>1.650.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>iPad Mini 1,2</td>
                            <td>850.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <h2>Bảng giá samsung</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>S3</td>
                            <td>550.000đ</td>
                            <td>2.400.000đ</td> 
                        </tr>
                        <tr>
                            <td>S4</td>
                            <td>650.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>S5</td>
                            <td>750.000đ</td>
                            <td>3.400.000đ</td>
                        </tr>
                        <tr>
                            <td>S6</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>S6 Plus</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Note 3</td>
                            <td>650.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Note 4</td>
                            <td>1.250.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Note 5</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá sony</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Z</td>
                            <td>900.000đ</td>
                            <td>1.500.000đ</td> 
                        </tr>
                        <tr>
                            <td>Z Ultra</td>
                            <td>1.850.000đ</td>
                            <td>2.450.000đ</td>
                        </tr>
                        <tr>
                            <td>Z1</td>
                            <td>1.000.000đ</td>
                            <td>2.450.000đ</td>
                        </tr>
                        <tr>
                            <td>Z1 Compact</td>
                            <td>1.250.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Z2</td>
                            <td>1.350.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Z3</td>
                            <td>1.450.000đ</td>
                            <td>2.950.000đ</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá nokia</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>520</td>
                            <td>500.000đ</td>
                            <td>750.000đ</td> 
                        </tr>
                        <tr>
                            <td>720</td>
                            <td>850.000đ</td>
                            <td>1.350.000đ</td>
                        </tr>
                        <tr>
                            <td>800</td>
                            <td>600.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>820</td>
                            <td>900.000đ</td>
                            <td>1.650.000đ</td>
                        </tr>
                        <tr>
                            <td>830</td>
                            <td>900.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>925</td>
                            <td>700.000đ</td>
                            <td>1.550.000đ</td>
                        </tr>
						<tr>
                            <td>1020</td>
                            <td>650.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá LG</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>G</td>
                            <td>700.000đ</td>
                            <td>Call</td> 
                        </tr>
                        <tr>
                            <td>G2</td>
                            <td>850.000đ</td>
                            <td>1.450.000đ</td>
                        </tr>
                        <tr>
                            <td>G Pro</td>
                            <td>950.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>GK</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>G3</td>
                            <td>1.450.000đ</td>
                            <td>1.950.000đ</td>
                        </tr>
						<tr>
                            <td>G4</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá HTC</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>M7</td>
                            <td>850.000đ</td>
                            <td>Call</td> 
                        </tr>
                        <tr>
                            <td>M8</td>
                            <td>1.100.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>M9</td>
                            <td>1.350.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá ZenFone</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ZenFone 5</td>
                            <td>500.000đ</td>
                            <td>900.000đ</td> 
                        </tr>
                        <tr>
                            <td>ZenFone 6</td>
                            <td>750.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>FonePad</td>
                            <td>Call</td>
                            <td>1.250.000đ</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá Sky Vega</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>860</td>
                            <td>1.450.000đ</td>
                        </tr>
                        <tr>
                            <td>870</td>
                            <td>1.350.000đ</td>
                        </tr>
                        <tr>
                            <td>890</td>
                            <td>1.750.000đ</td>
                        </tr>
						<tr>
                            <td>900</td>
                            <td>1.450.000đ</td>
                        </tr>
                        <tr>
                            <td>910</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="notice">
                    <h5 class="color-red">Lưu ý: Tất cả các hạng mục màn hình , cảm ứng bao test 01 tuần</h5>
                </div>
                <h2>PIN:</h2>
                <div class="content-unlock">
                    <p>Apple:<br />+ Iphone 4,4s 250k<br />+ Iphone 5, 5c  300k<br />+ Iphone 5s 300k</p>
                    <p>Sony:<br />+ Z 600K<br />+ Z1 650K<br />+ Z2 750k<br />+ Z3 850K</p>
                    <p>* Các hạng mục thay pin bảo hành 01 tháng, Các dòng máy pin rời tham khảo giá pin</p>
                </div>
				<h2>LOA, RUNG, MIC, CAMERA:</h2>
                <div class="content-unlock">
                    <p>Apple:<br />+ Loa : 250k-350k<br />+ Rung : 250k-300k<br />+ Mic : 250k – 400k<br/>+ Camera : iphone 4,4s  250k. ip 5 350k, ip5s 450k</p>
                    <p>Sony:<br />+ Loa : 250k-350k<br />+ Rung : 200k-350k<br />+ Camera : Gọi kiểm tra</p>
					<p>Samsung:<br />+ Loa : 250k-350k<br />+ Mic, camera : Gọi kiểm tra</p>
					<p>HTC:<br />+ Loa, rung, mic : Gọi kiểm tra</p>
					<p>LG:<br />+ Loa : 250k-350k</p>
					<p>Asus:<br />+ Loa : 200k-400k</p>
					<p>Nokia:<br />+ Loa : 200k-300k</p>
					<p>Sky:<br />+ Gọi kiểm tra</p>
                </div>
				<h2>HẠNG MỤC KHÁC:</h2>
                <div class="content-unlock">
                    <p>Apple:<br />+ Nắp lưng 4,4s 120k<br />+ Nút home 4,4s,5 250k<br />+ Vỏ iphone 5 750k<br />+ Vỏ iphone 5s cho iphone 5 850k<br />+ Vỏ iphone 5s 800k</p>
                    <p>Sony:<br />+ Nắp lưng Z, Z1 300k ( có keo chống nước )<br />+ Zắp lưng Z2, Z3 350k ( có keo chống nước )<br />+ Ép keo nắp lưng các model chống nước 200k</p>
                </div>
				

            </div>

        </div>
    </div>

</asp:Content>
