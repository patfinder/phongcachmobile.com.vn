﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class loai_dtdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadSlider();
                loadBanner();
                loadOS();
                loadNews();
                //loadFeature();
                loadSidebarBanner();
                loadBrand();
            }

            loadTitle();
        }

        private void loadBrand()
        {
            //<a href="#"><img alt="" src="images/logo_apple.jpg"></a>
            DataTable dt = CategoryController.getCategories("3", "");
            if (dt != null && dt.Rows.Count > 0)
            {
                litBrand.Text = "";
                for (int i=0; i< dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    if (i != dt.Rows.Count - 1)
                    {
                        litBrand.Text += "<a href='dtdd-" + dr["Name"].ToString().ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString() + "'>";
                    }
                    else
                    {
                        litBrand.Text += "<a style='margin-right:0px;' href='dtdd-" + dr["Name"].ToString().ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString() + "'>";
                    }
                    litBrand.Text += "<img alt='" + dr["Name"].ToString() + "' src='images/" + dr["DisplayImage"] + "'></a>";
                }
            }
        }

        private void loadSidebarBanner()
        {
            //Phone
            //DataTable dt = SliderController.getSliders("36");
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    int index = DateTime.Now.Second % dt.Rows.Count;
            //    imgPhone.ImageUrl = "images/banner/" + dt.Rows[index]["DisplayImage"].ToString();
            //}

            //Tablet
            DataTable dt = SliderController.getSliders("37");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litTablet.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>";
            }

            //Accessory
            dt = SliderController.getSliders("38");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litAccessory.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>";
            }

            //Application
            dt = SliderController.getSliders("39");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litApplication.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>";
            }

            //Game
            dt = SliderController.getSliders("40");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litGame.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>";
            }
        }

     
        private void loadNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "56");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.StripTagsRegex(dr["Title"].ToString()).ToLower().Replace(' ','-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = " cách đây " + ts.Hours + " giờ.";
                    //else
                    //    dr["TimeSpan"] = " cách đây " + ts.Days + " ngày";
                }
                lstNews.DataSource = dt;
                lstNews.DataBind();
            }
        }

        //private void loadFeature()
        //{
        //    DataTable dt = FeatureController.getFeaturesByGroupId("3");
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        ddlFeatures.Items.Clear();
        //        ddlFeatures.Items.Add(new ListItem("Tất cả", "-1"));
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            ddlFeatures.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
        //        }
        //        ddlFeatures.DataBind();
        //    }
        //}

        private void loadOS()
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId("3");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlOS.Items.Clear();
                ddlOS.Items.Add(new ListItem("Tất cả", "-1"));
                foreach (DataRow dr in dt.Rows)
                {
                    ddlOS.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }
                ddlOS.DataBind();
            }
        }

        private void loadTitle()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    litMobile.Text = this.Title = "Điện thoại sử dụng hệ điều hành " + OperatingSystemController.getOSNameById(id);
                }
                else if (ids[0].Equals("tn"))
                {
                    litMobile.Text = this.Title = "Điện thoại với tính năng " + FeatureController.getFeatureNameById(id);
                }
                else
                {
                    litMobile.Text = this.Title = "Điện thoại " + CategoryController.getCategoryNameById(id);
                }
            }
            else
                litMobile.Text = this.Title = "Điện thoại di động";
        }

        private void loadBanner()
        {
            //DataTable dt = SliderController.getSliders(2,"24");
            //if (dt != null && dt.Rows.Count >0)
            //{
            //    litBanner.Text = "";
            //    foreach(DataRow dr in dt.Rows)
            //    {
            //        litBanner.Text += "<li class='item'><a href='" + dr["ActionUrl"].ToString() + "'>" + 
            //            "<img src='images/banner/" + dr["DisplayImage"].ToString() + "' />";
            //        litBanner.Text += "<div class='contentDiv'>" + dr["Text"].ToString() + "</div>";
            //        litBanner.Text += "</a></li>";
            //    }
            //}
        }

        private void loadSlider()
        {
            //litSlider.Text = "<div id='slider' class='nivoSlider' style='margin-bottom: 0; display: inline-block;'>";

            //DataTable dt = SliderController.getSliders("23");
            //if (dt != null && dt.Rows.Count >0)
            //{
            //    foreach(DataRow dr in dt.Rows)
            //    {
            //        litSlider.Text += "<a   href='" + dr["ActionUrl"].ToString() + "'>" + 
            //            "<img  src='images/slide/" + dr["DisplayImage"].ToString() + "' class='slider_image' /></a>";
            //    }
            //}
            //litSlider.Text += "</div>";

            litSlider.Text = "<ul class='splash' title=' '>";

            DataTable dt = SliderController.getSliders("23");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    litSlider.Text += "<li><a   href='" + dr["ActionUrl"].ToString() + "'>" +
                        "<img style='border: 1px solid #dcdcdc;' src='images/slide/" + dr["DisplayImage"].ToString() + "' /></a></li>";
                }
            }
            litSlider.Text += "</ul>";

        }


        private void loadProductsByCategory(int categoryId)
        {
            DataTable dt = null;
            if (ddlTopSort.SelectedValue.Equals("posteddate", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.getProductsByCategory(categoryId);
            else if (ddlTopSort.SelectedValue.Equals("hightolowprice", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.getHighToLowPriceProductsByCategory(categoryId);
            else if (ddlTopSort.SelectedValue.Equals("lowtohighprice", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.getLowToHighPriceProductsByCategory(categoryId);
            else //viewcount
                dt = ProductController.getHighViewProductsByCategory(categoryId);

            EnumerableRowCollection<DataRow> temp = null;
            //temp = dt.AsEnumerable().Where(row => (row.Field<bool>("isOutOfStock") == false));

            //if (temp != null && temp.Count() > 0)
            //    dt = temp.CopyToDataTable();

            DataTable dtNew = dt;

            //Price filter

            if (!ddlPrice.SelectedValue.Equals("tat-ca"))
            {
                if (ddlPrice.SelectedValue.Equals("duoi-1-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 1000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-2-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 2000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-3-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 3000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-5-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 5000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-8-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 8000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-10-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 10000000);
                else if (ddlPrice.SelectedValue.Equals("tren-10-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") >= 1000000);

                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }
           

            //OS filter
            if (!ddlOS.SelectedValue.Equals("-1") && dtNew != null)
            {
                temp = dtNew.AsEnumerable().Where(row => row.Field<int>("OSId") == int.Parse(ddlOS.SelectedValue));

                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }

            //Features filter

            //if (!ddlFeatures.SelectedValue.Equals("-1") && dtNew != null)
            //{
            //    DataTable dtFeaturedProduct = FeatureController.getProductsByFeatureId(ddlFeatures.SelectedValue);
            //    List<int> ids = new List<int>();
            //    foreach(DataRow drFeaturedProduct in dtFeaturedProduct.Rows)
            //    {
            //        ids.Add(int.Parse(drFeaturedProduct["ProductId"].ToString()));
            //    }
            //    temp = dtNew.AsEnumerable().Where(row => ids.Contains(row.Field<int>("Id")));

            //    if (temp != null && temp.Count() > 0)
            //        dtNew = temp.CopyToDataTable();
            //    else
            //        dtNew = null;
            //}

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("UrlName", typeof(string));
                dtNew.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-');

                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                        long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }
            }
            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }
            }
            else
            {
                loadProductsByCategory();
            }

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
                " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";

        }

  
        private void loadProductsByCategory()
        {
            DataTable dt = null;
            if (ddlTopSort.SelectedValue.Equals("posteddate", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.getProductsByGroupId(3);
            else if (ddlTopSort.SelectedValue.Equals("hightolowprice", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.getHighToLowPriceProductsByGroupId(3);
            else if (ddlTopSort.SelectedValue.Equals("lowtohighprice", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.getLowToHighPriceProductsByGroupId(3);
            else //viewcount
                dt = ProductController.getHighViewProductsByGroupId(3);

            EnumerableRowCollection<DataRow> temp = null;

            //temp = dt.AsEnumerable().Where(row => (row.Field<bool>("isOutOfStock") == false));

            //if (temp != null && temp.Count() > 0)
            //    dt = temp.CopyToDataTable();

            DataTable dtNew = dt;


            if (!ddlPrice.SelectedValue.Equals("tat-ca"))
            {
                if (ddlPrice.SelectedValue.Equals("duoi-1-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 1000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-2-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 2000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-3-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 3000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-5-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 5000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-8-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 8000000);
                else if (ddlPrice.SelectedValue.Equals("duoi-10-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") < 10000000);
                else if (ddlPrice.SelectedValue.Equals("tren-10-trieu"))
                    temp = dt.AsEnumerable().Where(row => row.Field<int>("CurrentPrice") >= 1000000);

                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }


            //OS filter
            if (!ddlOS.SelectedValue.Equals("-1") && dtNew != null)
            {
                temp = dtNew.AsEnumerable().Where(row => row.Field<int>("OSId") == int.Parse(ddlOS.SelectedValue));

                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }

            //Features filter

            //if (!ddlFeatures.SelectedValue.Equals("-1") && dtNew != null)
            //{
            //    DataTable dtFeaturedProduct = FeatureController.getProductsByFeatureId(ddlFeatures.SelectedValue);
            //    List<int> ids = new List<int>();
            //    foreach (DataRow drFeaturedProduct in dtFeaturedProduct.Rows)
            //    {
            //        ids.Add(int.Parse(drFeaturedProduct["ProductId"].ToString()));
            //    }
            //    temp = dtNew.AsEnumerable().Where(row => ids.Contains(row.Field<int>("Id")));

            //    if (temp != null && temp.Count() > 0)
            //        dtNew = temp.CopyToDataTable();
            //    else
            //        dtNew = null;
            //}


            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("UrlName", typeof(string));
                dtNew.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-');

                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                        long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }
            }

            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }

        protected void ddlTopDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);
            //dpgLstItemTop.StartRowIndex =dpgLstItemBot. = 0;
            ddlBotDisplay.SelectedIndex = ddlTopDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlBotDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);

            ddlTopDisplay.SelectedIndex = ddlBotDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();

            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
                " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlTopSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                ddlBotSort.SelectedIndex = ddlTopSort.SelectedIndex;
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlBotSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                ddlTopSort.SelectedIndex = ddlBotSort.SelectedIndex;

                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlPrice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlFeatures_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                if (ids[0].Equals("hdh"))
                {
                    ddlOS.SelectedValue = id;
                    loadProductsByCategory();
                }
                //else if (ids[0].Equals("tn"))
                //{
                //    ddlFeatures.SelectedValue = id;
                //    loadProductsByCategory();
                //}
                else
                {
                    loadProductsByCategory(int.Parse(id));
                }

            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }
    }
}