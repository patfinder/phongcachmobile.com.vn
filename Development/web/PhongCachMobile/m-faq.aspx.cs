﻿using PhongCachMobile.controller;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile
{
    public partial class m_faq : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadFAQ();
            }
        }

        private void loadFAQ()
        {
            DataTable dt = FaqController.getFaqs();
            if (dt != null && dt.Rows.Count > 0)
            {
                lstFaq.DataSource = dt;
                lstFaq.DataBind();
            }
        }

        private void loadLang()
        {
            litFAQ.Text = LangController.getLng("litFAQ.Text", "Câu hỏi thường gặp");

        }
    }
}