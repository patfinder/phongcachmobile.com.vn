﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using PhongCachMobile.model;
using PhongCachMobile.master;
using PhongCachMobile.admin;
using PhongCachMobile.master.uc;

namespace PhongCachMobile
{
    public partial class loai_phu_kien_dtdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.Home)this.Master).TemplateType = TemplateType.NewType;

            if (!this.IsPostBack)
            {
                loadAccessoryGroups();
            }
        }

        private void loadAccessoryGroups()
        {
            List<CategoryInfo> AccessCategories = new List<CategoryInfo>();

            var dt = CategoryController.getCategories(ProductController.ProductGroupAccessory, "");
            foreach (DataRow dr in dt.Rows)
            {
                AccessCategories.Add(new CategoryInfo
                {
                    ID = (int)dr["ID"],
                    Name = (string)dr["Name"],
                });
            }

            SectionList = AccessoryGroupController.getGroups();
            SectionList.ForEach(g => 
                g.Categories.ForEach(c =>
                {
                    var ax = AccessCategories.FirstOrDefault(a => a.ID == c.ID);
                    if(ax != default(CategoryInfo))
                        c.Name = ax.Name;
                }));

            foreach (var group in SectionList)
            {
                loadGroupContent(group);
            }

            InitControls();
        }

        protected List<SectionInfo> SectionList = new List<SectionInfo>();
        protected List<List<Article>> SectionsCategories = new List<List<Article>>();
        protected List<List<Article>> GroupBanners = new List<List<Article>>();
        protected List<List<Product>> GroupProducts = new List<List<Product>>();

        private void loadGroupContent(SectionInfo sectionInfo)
        {
            var group = new List<Article>();

            foreach (CategoryInfo cat in sectionInfo.Categories)
            {
                group.Add(new Article
                {
                    id = cat.ID,
                    title = cat.Name,
                    link = Common.m() + "sub-phu-kien-" + cat.Name.ToLower().Replace(" ", "-") + "-" + cat.ID,
                });
            }
            SectionsCategories.Add(group);

            var banners = new List<Article>();
            banners.Add(new Article { title = sectionInfo.Title, displayImg = sectionInfo.Image, link = sectionInfo.Link });
            GroupBanners.Add(banners);

            // Load group products
            int noRows = int.Parse(SysIniController.getSetting("Phu_Kien_TaiNghe_Loa", "8"));
            DataTable dt = ProductController.getProductsByCategoryIDs(sectionInfo.Categories.Select(c => c.ID).ToArray(), noRows);

            List<Product> products = new List<Product>();
            foreach (DataRow row in dt.Rows)
            {
                products.Add(ProductController.rowToProduct(row));
            }

            GroupProducts.Add(products);
        }

        protected void InitControls()
        {
            PlaceHolder[] placeHolders = new PlaceHolder[]{
                PlaceHolder_1, PlaceHolder_2, PlaceHolder_3,
                PlaceHolder_4, PlaceHolder_5, PlaceHolder_6,
                PlaceHolder_7, PlaceHolder_8, PlaceHolder_9,
                PlaceHolder_10, 
            };

            for (int i = 0; i < SectionsCategories.Count; i++)
            {
                //ucAccessoryList ucAccessoryList = new ucAccessoryList();
                ucAccessoryList ucAccessoryList = LoadControl("~/master/uc/ucAccessoryList.ascx") as ucAccessoryList;

                ucAccessoryList.ID = "ucAccessoryList_" + i;
                ucAccessoryList.Banners = GroupBanners[i];
                ucAccessoryList.ProductList = GroupProducts[i];
                //ucAccessoryList.DataBind();

                if (i < 10)
                {
                    placeHolders[i].Visible = true;
                    placeHolders[i].Controls.Add(ucAccessoryList);
                    //placeHolders[i].Controls.Add(new Literal() { Text = "Hello" });
                }
            }
        }
    }
}