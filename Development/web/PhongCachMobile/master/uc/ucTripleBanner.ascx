﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucTripleBanner.ascx.cs" Inherits="PhongCachMobile.master.uc.ucTripleBanner" %>

<div class="content-banner">
    <div class="content-center clearfix">
        <div class="slide-banner">
            <% foreach(var item in bannerLeftItems) { %>
            <div class="items"><a href="<%= item.link %>" ><img src="images/slide/<%= item.displayImg %>" alt="<%= item.displayImg %>" /></a></div>
            <% } %>
        </div>
        <div class="section-sale">
            <div class="slide-right">
                <% for(int i=0; i<bannerRightItem1s.Count; i++) {
                        var item1 = bannerRightItem1s[i];
                        var item2 = bannerRightItem2s[i];
                    %>
                <div class="items">
                    <a href="<%= item1.link %>"><img src="images/slide/<%= item1.displayImg %>" alt="<%= item1.displayImg %>" /></a>
                    <a href="<%= item2.link %>"><img src="images/slide/<%= item2.displayImg %>" alt="<%= item2.displayImg %>" /></a>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</div>
