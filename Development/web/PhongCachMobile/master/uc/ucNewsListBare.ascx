﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucNewsListBare.ascx.cs" Inherits="PhongCachMobile.master.uc.ucNewsListBare" %>

<!--News Count: <%= NewsList.Count %> -->
<% foreach(PhongCachMobile.model.Article article in NewsList) { %>
<li >
    <h2><a href="<%= article.link %>"><%= article.title %></a></h2>
    <span class="info-by"><%= article.postedDate %> - <strong class="color-red"><%= article.CommentCount %></strong> bình luận - <strong class="color-red"><%= article.viewCount %></strong> lượt xem</span>
    <a href="<%= article.link %>" class="img-box"><img src="images/news/<%= article.displayImg %>" alt=""></a>
</li>
<% } %>