﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucHeader.ascx.cs" Inherits="PhongCachMobile.master.uc.ucHeader" %>

<div id="header">
    <header class="header <%= HeaderType == PhongCachMobile.master.uc.HeaderType.Home ? "clearfix" : "" %>">

        <!-- Openning Header tags -->

        <% if(HeaderType == PhongCachMobile.master.uc.HeaderType.Home) { %>

            <div class="logo"><a href="/" class="logo">PhongCachMobile</a></div>

        <% } else { %>

                <nav class="nav-top">
                    <div class="content-center clearfix">
                        <ul class="content-left">
                            <li><a href="tin-khuyen-mai">Tin PR - Khuyến mại</a></li>
                            <li><a href="tin-thi-truong">Tin thị trường</a></li>
                            <li><a href="faq">FAQ</a></li>
                            <li><a href="lien-he">Liên hệ</a></li>
                            <!-- <li><a href="/landing-pages/landing-page1.aspx">HTC One</a></li>
                            <li><a href="/landing-pages2/landing-page2.aspx">HTC One Plus</a></li> -->
                            <!-- <li><a href="http://phongcachmobile.com.vn:81">Website cũ phongcachmobile.com.vn</a></li> -->
                        </ul>
                        <% if(!loggedIn) { %>
                        <ul class="content-right">
                            <li>Chào mừng bạn đến với PhongCachMobile</li>
                            <li><i class="fa fa-user"></i><a href="dang-nhap">ĐĂNG NHẬP</a></li>
                            <li><a href="dang-ky">ĐĂNG KÝ</a></li>
                        </ul>
                        <% } else { %>
                        <ul class="content-right logged-in">
                            <li>Chào mừng <span class="color-red"><%= user.lastName + " " + user.firstName %></span>
                                <div class="dropdown">
                                    <a id="dLabel" data-target="#" href="http://example.com" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-sort-desc"></i>
                                    </a>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <%--<li><a href="<%= PhongCachMobile.util.Common.m() %>nguoi-dung">My Profile</a></li>--%>
                                        <li><a href="<%= PhongCachMobile.util.Common.m() %>dang-xuat">Log out</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <% } %>
                    </div>
                </nav>
                <div class="logo-adv">
                    <div class="content-center clearfix">
                        <div class="logo"><a href="/" class="logo">PhongCachMobile</a></div>
                        <div class="adv">
                            <a href="<%= bannerArticle.link %>"><img src="/images/banner/<%= bannerArticle.displayImg %>" alt="adv"></a>
                        </div>
                    </div>
                </div>
                <div class="menu-bar">
                    <div class="content-center clearfix">

        <% } %>

        <nav>
            <ul class="nav navbar-nav">
                <% if(HeaderType == PhongCachMobile.master.uc.HeaderType.MainPage) { %><li><a href="/trang-chu"><span class="icon-home"></span></a></li> <% } %>
                <li <%= activeMenu == "gia-hot" ? "class='active'" : "" %>><a href="gia-hot">GIÁ HOT</a></li>
                <li <%= activeMenu == "dtdd" ? "class='active'" : "" %>><a href="dtdd">ĐIỆN THOẠI <i class="fa fa-sort-desc"></i></a>
                    <ul class="sub-menu clearfix">
                        <li>
                            <% foreach(var item in MenuPhoneCategories1)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                            <% } %>
                        </li>
                        <li>
                            <% foreach(var item in MenuPhoneCategories2)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                            <% } %>
                        </li>
                        <li>
                            <% foreach(var item in MenuPhoneCategories3)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                            <% } %>
                        </li>

                        <%  bool first = true;
                            foreach(var product in PhoneProducts) { %>
                        <li class="items-product <%= first ? "first" : "" %>">
                            <% first = false; %>
                            <span class="notice">MỚI</span>
                            <figure>
                                <div class="box-img">
                                    <img src="images/product/<%= product.firstImage %>" alt="">
                                </div>
                                <figcaption>
                                    <h2><%= product.name %></h2><br />
                                    <span class="price"><%= product.currentPriceStr %></span>
                                </figcaption>
                            </figure>
                            <a href="<%= product.link %>" class="read-more">Xem chi tiết</a>
                        </li>
                        <% } %>
                    </ul>
                </li>
                <li <%= activeMenu == "macbook" ? "class='active'" : "" %>><a href="macbook">MACBOOK<i class="fa fa-sort-desc"></i></a>
                <ul class="sub-menu list-5-items clearfix">
                    <li>
                        <% foreach(var item in MenuMacbookCategories1)
                            {
                                // <a href="#">Apple</a><br /> %>
                                <a href="<%= item[2] %>"><%= item[1] %></a><br />
                        <% } %>
                    </li>
                    <li>
                        <% foreach(var item in MenuMacbookCategories2)
                            {
                                // <a href="#">Apple</a><br /> %>
                                <a href="<%= item[2] %>"><%= item[1] %></a><br />
                        <% } %>
                    </li>
                    <%  first = true;
                    foreach(var product in MacbookProducts) { %>
                    <li class="items-product <%= first ? "first" : "" %>">
                    <% first = false; %>
                    <span class="notice">MỚI</span>
                    <figure>
                        <div class="box-img">
                            <img src="images/product/<%= product.firstImage %>" alt="">
                        </div>
                        <figcaption>
                            <h2><%= product.name %></h2><br />
                            <span class="price"><%= product.currentPriceStr %></span>
                        </figcaption>
                    </figure>
                    <a href="<%= product.link %>" class="read-more">Xem chi tiết</a>
                    </li>
                    <% } %>
                </ul>
                </li>
                <li <%= activeMenu == "may-tinh-bang" ? "class='active'" : "" %>><a href="may-tinh-bang">MÁY TÍNH BẢNG <i class="fa fa-sort-desc"></i></a>
                    <ul class="sub-menu clearfix">
                        <li>
                            <%
                                foreach(var item in MenuTabletCategories1)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>
                        <li>
                            <%
                                foreach (var item in MenuTabletCategories2)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>
                        <li>
                            <%
                                foreach(var item in MenuTabletCategories3)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>
                        <%  first = true;
                            foreach(var product in TabletProducts) { %>
                        <li class="items-product <%= first ? "first" : "" %>">
                            <% first = false; %>
                            <span class="notice">MỚI</span>
                            <figure>
                                <div class="box-img">
                                    <img src="images/product/<%= product.firstImage %>" alt="">
                                </div>
                                <figcaption>
                                    <h2><%= product.name %></h2><br />
                                    <span class="price"><%= product.currentPriceStr %></span>
                                </figcaption>
                            </figure>
                            <a href="<%= product.link %>" class="read-more">Xem chi tiết</a>
                        </li>
                        <% } %>
                    </ul>
                </li>
                <li <%= activeMenu == "phu-kien" ? "class='active'" : "" %>><a href="phu-kien">PHỤ KIỆN <i class="fa fa-sort-desc"></i></a>
                    <ul class="sub-menu one-product clearfix">
                        <li>
                            <%
                                foreach (var item in MenuAccessoryCategories1)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>
                        <li>
                            <%
                                foreach (var item in MenuAccessoryCategories2)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>
                        <li>
                            <%
                                foreach (var item in MenuAccessoryCategories3)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>
                        <%  first = true;
                            foreach(var product in AccessoryProducts) { %>
                        <li class="items-product <%= first ? "first" : "" %>">
                            <% first = false; %>
                            <span class="notice">MỚI</span>
                            <figure>
                                <div class="box-img">
                                    <img src="images/product/<%= product.firstImage %>" alt="">
                                </div>
                                <figcaption>
                                    <h2><%= product.name %></h2><br />
                                    <span class="price"><%= product.currentPriceStr %></span>
                                </figcaption>
                            </figure>
                            <a href="<%= product.link %>" class="read-more">Xem chi tiết</a>
                        </li>
                        <% } %>

                    </ul>
                </li>
                <li <%= activeMenu == "kho-may-cu" ? "class='active'" : "" %>><a href="kho-may-cu">MÁY CŨ</a></li>
                <li <%= activeMenu == "ung-dung" ? "class='active'" : "" %> class="line-2"><a href="ung-dung">ỨNG DỤNG <br />- GAME <i class="fa fa-sort-desc"></i></a>
                    <ul class="sub-menu game-product clearfix">
                        <li>
                            <%
                                foreach (var item in MenuGameCategories)
                                {
                                    // <a href="#">Apple</a><br /> %>
                                    <a href="<%= item[2] %>"><%= item[1] %></a><br />
                                <% }
                            %>
                        </li>

                        <%  first = true;
                            foreach(var product in SoftwareProducts) { %>
                        <li class="items-product <%= first ? "first" : "" %>">
                            <% first = false; %>
                            <span class="notice">MỚI</span>
                            <figure>
                                <div class="box-img">
                                    <img src="images/<%= (product is PhongCachMobile.model.Game ? 
                                        "Game" : "Application") + "/" + product.displayImage %>" alt="">
                                </div>
                                <figcaption>
                                    <h2><%= product.name %></h2><br />
                                </figcaption>
                            </figure>
                            <a href="<%= /* loggedIn ? product.download : PhongCachMobile.util.Common.m() + "dang-nhap" */ product.filename %>" class="read-more">Tải về</a>
                        </li>
                        <% } %>
                    </ul>
                </li>
                <li <%= activeMenu == "sua-chua-cai-dat" ? "class='active'" : "" %> class="line-2"><a href="sua-chua-cai-dat">SỮA CHỮA <br />- CÀI ĐẶT</a></li>
            </ul>
        </nav>

        <div class="account">
            <form id="search-frm" class="navbar-form navbar-left" role="search" action="tim-kiem">
                <div class="form-group">
                    <input id="search-value" type="text" class="form-control" placeholder="" name="id">
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </form>
            <% if(HeaderType == PhongCachMobile.master.uc.HeaderType.Home) 
               {
                   if (!loggedIn)
                   { %><a href="dang-nhap"><i class="fa fa-user"></i></a><% }
                   else
                   { %>
                    <div class="logged dropdown">
                        <a id="dLabel" data-target="#" href="trang-chu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <img src="images/avatar/<%= user.avatar %>">
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <%--<li><a href="gia-hot">My Profile</a></li>--%>
                            <li><a href="dang-xuat">Log out</a></li>
                        </ul>
                    </div>
            <% } } %>
        </div>

        <!-- Closing Header tags -->
        <% if(HeaderType == PhongCachMobile.master.uc.HeaderType.MainPage) { %>
                    </div>
                </div>
        <% } %>

    </header>
</div>
