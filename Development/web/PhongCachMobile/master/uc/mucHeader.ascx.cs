﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.master.uc
{
    public partial class mucHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadMenuPhoneCategory();
            loadUserProfile();
        }

        protected SysUser user = new SysUser();

        public HeaderType HeaderType = HeaderType.MainPage;
        public bool loggedIn = false;

        public string activeMenu = Common.m() + "gia-hot";

        private void loadMenuPhoneCategory()
        {
            // Check active menu
            var path = HttpContext.Current.Request.RawUrl.StripLeadingFSlash();

            // Strip params (if any)
            if (path.IndexOf("?") <= 0)
                activeMenu = path;
            else
                activeMenu = path.Substring(path.IndexOf("?"));

            activeMenu = activeMenu.StripLeadingM();

            // Check typical patterns
            if (activeMenu.StartsWith(Common.m() + "dtdd-"))
                activeMenu = "dtdd";
            if (activeMenu.StartsWith(Common.m() + "macbook-"))
                activeMenu = "macbook";
            else if (activeMenu.StartsWith(Common.m() + "mtb-"))
                activeMenu = "may-tinh-bang";
            else if (activeMenu.StartsWith(Common.m() + "ud-") || activeMenu.StartsWith(Common.m() + "game-"))
                activeMenu = "ung-dung";
            else if (activeMenu.StartsWith(Common.m() + "pkdtdd-") || activeMenu.StartsWith(Common.m() + "sub-phu-kien-"))
                activeMenu = "phu-kien";
        }

        private void loadUserProfile()
        {
            loggedIn = Common.IsLoggedIn();

            if (!loggedIn)
                return;

            DataTable dt = SysUserController.getUserByEmail(Session["Email"] as string);
            user.email = dt.Rows[0]["email"] as string;

            user.firstName = dt.Rows[0]["firstName"] as string;
            if (string.IsNullOrWhiteSpace(user.firstName))
                user.firstName = user.email;

            user.lastName = dt.Rows[0]["lastName"] as string;

            user.avatar = dt.Rows[0]["avatar"] as string;
            if (string.IsNullOrEmpty(user.avatar))
                user.avatar = "1.png";
        }
    }
}