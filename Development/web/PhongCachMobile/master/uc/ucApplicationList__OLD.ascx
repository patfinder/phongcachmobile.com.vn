﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucApplicationList__OLD.ascx.cs" Inherits="PhongCachMobile.master.uc.ucApplicationList" %>

<ul class="list-game clearfix">
    <% foreach(PhongCachMobile.model.Application app in applications) { %>
    <li>
        <figure>
            <div class="box-img"><a href="<%= (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.filename %>">
                <img src="images/<%= (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.displayImage %>">
            </a></div>
            <figcaption>
                <div class="content-left">
                    <h2><%= app.name %></h2>
                    <p class="intro"><%= app.description %></p>
                    <div class="information">
                        <p><span class="color-red">|</span> <strong>Cập nhật:</strong> <%= app.postedDate %>  <span class="color-red">|</span>  <strong class="color-red"><%= app.viewCount %></strong> lượt xem<br />
                        <span class="color-red">|</span> <strong>Yêu cầu:</strong> <%= app.requiredOS %>  <span class="color-red">|</span>  <strong>Dung lượng:</strong> <%= app.filesize %><br />
                        <span class="color-red">|</span> <strong>Nhà sản xuất:</strong> <%= app.developer %> <span class="color-red">|</span>  <strong>Phiên bản:</strong> <%= app.currentVersion %></p>
                    </div>
                </div>
                <a href="<%= (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.filename %>" class="download">Tải về</a>

            </figcaption>
        </figure>
    </li>
    <% } %>
</ul>
