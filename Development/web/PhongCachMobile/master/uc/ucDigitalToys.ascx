﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucDigitalToys.ascx.cs" Inherits="PhongCachMobile.master.uc.ucDigitalToys" %>

<aside class="aside-right">
    <div class="content-right">
        <h2>Đồ chơi số</h2>
        <% foreach(PhongCachMobile.model.Article article in bannerItems) { %>
        <a href="<%= article.link %>"><img src="images/banner/<%= article.displayImg %>" alt="" /></a>
        <% } %>
    </div>
</aside>
