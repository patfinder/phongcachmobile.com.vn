﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;

namespace PhongCachMobile.master.uc
{
    public partial class mucRightPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadPromotionNews();
            }
        }

        private void loadPromotionNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "56");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.StripTagsRegex(dr["Title"].ToString()).ToLower().Replace(' ', '-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = " cách đây " + ts.Hours + " giờ.";
                    //else
                    //    dr["TimeSpan"] = " cách đây " + ts.Days + " ngày";
                }
                lstNews.DataSource = dt;
                lstNews.DataBind();
            }
        }
    }
}