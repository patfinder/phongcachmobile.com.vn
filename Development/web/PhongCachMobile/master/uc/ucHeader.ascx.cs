﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Text;

namespace PhongCachMobile.master.uc
{
    public enum HeaderType
    {
        Unknown,
        Home,
        MainPage,
        DetailPage,
    };
    
    public partial class ucHeader : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!this.IsPostBack)
            {
                loadMenuPhoneCategory();
                loadMenuMacbookCategory();
                loadMenuTabletCategory();
                loadMenuGameAppCategory();
                loadMenuAccessoriesCategory();
                loadUserProfile();
            }
        }

        protected SysUser user = new SysUser();

        public HeaderType HeaderType = HeaderType.MainPage;
        public bool loggedIn = false;

        protected string RemoveLeaderSlash(string path)
        {
            if (path.StartsWith("/"))
                return path.Substring(1);

            return path;
        }

        public string activeMenu = "gia-hot";
        public Article bannerArticle = new Article { displayImg = "adv.jpg" };

        public List<string[]> MenuPhoneCategories1 = new List<string[]>();
        public List<string[]> MenuPhoneCategories2 = new List<string[]>();
        public List<string[]> MenuPhoneCategories3 = new List<string[]>();

        public List<Product> PhoneProducts = new List<Product>();
        private void loadMenuPhoneCategory()
        {
            // Load site banner
            // DTDD_Top_Banner_Left_Items // V2-Old Phone - Sliders Left
            DataTable dt = SliderController.getSliders("1075");
            if (dt != null && dt.Rows.Count > 0)
            {
                var rand = new Random().Next(dt.Rows.Count);
                var slider = SliderController.rowToSlider(dt.Rows[rand]);

                bannerArticle = new Article
                {
                    displayImg = slider.displayImg,
                    link = slider.actionUrl
                };
            }

            // Check active menu
            var path = RemoveLeaderSlash(HttpContext.Current.Request.RawUrl);

            // Strip params (if any)
            if (path.IndexOf("?") <= 0)
                activeMenu = path;
            else
                activeMenu = path.Substring(path.IndexOf("?"));

            // Check typical patterns
            if (activeMenu.StartsWith("dtdd-"))
                activeMenu = "dtdd";
            if (activeMenu.StartsWith("macbook-"))
                activeMenu = "macbook";
            else if (activeMenu.StartsWith("mtb-"))
                activeMenu = "may-tinh-bang";
            else if (activeMenu.StartsWith("ud-") || activeMenu.StartsWith("game-"))
                activeMenu = "ung-dung";
            else if (activeMenu.StartsWith("pkdtdd-") || activeMenu.StartsWith("sub-phu-kien-"))
                activeMenu = "phu-kien";

            // Get menu phone categories
            DataTable dtSub = CategoryController.getCategories(ProductController.ProductGroupPhone, "");
            int groupCount = dtSub.Rows.Count / 2 + (dtSub.Rows.Count % 2 > 0 ? 1 : 0);

            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                string id = dtSub.Rows[i]["Id"].ToString();
                string name = dtSub.Rows[i]["Name"].ToString();
                string url = "dtdd-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;

                if (i < groupCount)
                    MenuPhoneCategories1.Add(new string[] { id, name, url });
                else
                    MenuPhoneCategories2.Add(new string[] { id, name, url });
            }

            // Phones categories on menu
            DataTable dtOS = OperatingSystemController.getOperatingSystemsByGroupId(ProductController.ProductGroupPhone);
            foreach (DataRow drOS in dtOS.Rows)
            {
                string id = drOS["Id"].ToString();
                string name = drOS["Name"].ToString();
                string url = "dtdd-hdh-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;
                name = "Hệ điều hành " + name;

                MenuPhoneCategories3.Add(new string[] { id, name, url });
            }

            // Featured Phones on menu
            var devices = ProductController.getRandomFeatured(2, ProductController.ProductGroupPhone);
            foreach(DataRow row in devices.Rows)
            {
                PhoneProducts.Add(ProductController.rowToSimpleProduct(row));
            }
        }

        public List<string[]> MenuMacbookCategories1 = new List<string[]>();
        public List<string[]> MenuMacbookCategories2 = new List<string[]>();

        public List<Product> MacbookProducts = new List<Product>();
        private void loadMenuMacbookCategory()
        {
            // Get menu Macbook categories
            DataTable dtSub = CategoryController.getCategories(ProductController.ProductGroupMacbook, "");
            List<Article> tmpList = new List<Article>();

            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                string id = dtSub.Rows[i]["Id"].ToString();
                string name = dtSub.Rows[i]["Name"].ToString();
                string url = "macbook-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;

                tmpList.Add(new Article() { id = int.Parse(id), title = name, link = url });
            }

            tmpList.Sort(FunctionalComparer<Article>.Create(new Func<Article, Article, int>((a1, a2) =>
            {
                return a1.title.Length.CompareTo(a1.title.Length);
            })));

            int len2 = tmpList.Count / 2 + (tmpList.Count % 2);
            MenuMacbookCategories1.AddRange(tmpList.Take(len2).Select(a => new string[] { a.id.ToString(), a.title, a.link }));
            MenuMacbookCategories2.AddRange(tmpList.Skip(len2).Select(a => new string[] { a.id.ToString(), a.title, a.link }));

            // Featured Macbooks on menu
            var devices = ProductController.getRandomFeatured(2, ProductController.ProductGroupMacbook);
            foreach (DataRow row in devices.Rows)
            {
                MacbookProducts.Add(ProductController.rowToSimpleProduct(row));
            }
        }

        public List<string[]> MenuTabletCategories1 = new List<string[]>();
        public List<string[]> MenuTabletCategories2 = new List<string[]>();
        public List<string[]> MenuTabletCategories3 = new List<string[]>();

        public List<Product> TabletProducts = new List<Product>();

        private void loadMenuTabletCategory()
        {
            DataTable dtSub = CategoryController.getCategories(ProductController.ProductGroupTablet, "");
            foreach (DataRow drOS in dtSub.Rows)
            {
                string id = drOS["Id"].ToString();
                string name = drOS["Name"].ToString();
                string url = "mtb-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;

                MenuTabletCategories1.Add(new string[] { id, name, url });
            }

            DataTable dtOS = OperatingSystemController.getOperatingSystemsByGroupId(ProductController.ProductGroupTablet);
            foreach (DataRow drOS in dtOS.Rows)
            {
                string id = drOS["Id"].ToString();
                string name = drOS["Name"].ToString();
                string url = "mtb-hdh-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;
                name = "Hệ điều hành " + name;

                MenuTabletCategories2.Add(new string[] { id, name, url });
            }

            DataTable dtFeature = FeatureController.getFeaturesByGroupId(ProductController.ProductGroupTablet);
            foreach (DataRow drOS in dtFeature.Rows)
            {
                string id = drOS["Id"].ToString();
                string name = drOS["Name"].ToString();
                string url = "mtb-tn-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;

                MenuTabletCategories3.Add(new string[] { id, name, url });
            }

            DataTable devices = ProductController.getRandomFeatured(2, ProductController.ProductGroupTablet);
            if (devices.Rows.Count <= 0)
                devices = ProductController.getRandomProductsByGroupId(ProductController.ProductGroupTabletInt, 2);

            foreach (DataRow row in devices.Rows)
            {
                TabletProducts.Add(ProductController.rowToSimpleProduct(row));
            }
        }

        public List<string[]> MenuAccessoryCategories1 = new List<string[]>();
        public List<string[]> MenuAccessoryCategories2 = new List<string[]>();
        public List<string[]> MenuAccessoryCategories3 = new List<string[]>();

        public List<Product> AccessoryProducts = new List<Product>();

        private void loadMenuAccessoriesCategory()
        {
            DataTable dtSub = CategoryController.getCategories(ProductController.ProductGroupAccessory, "");
            int groupCount = dtSub.Rows.Count / 3 + (dtSub.Rows.Count % 3 > 0 ? 1 : 0);

            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                string id = dtSub.Rows[i]["Id"].ToString();
                string name = dtSub.Rows[i]["Name"].ToString();
                //string url = "pkdtdd-" + name.ToLower().Replace(" ", "-") + "-" + id;
                string url = "sub-phu-kien-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;

                if (i < groupCount)
                    MenuAccessoryCategories1.Add(new string[] { id, name, url });
                else if (i < 2 * groupCount)
                    MenuAccessoryCategories2.Add(new string[] { id, name, url });
                else
                    MenuAccessoryCategories3.Add(new string[] { id, name, url });
            }

            var devices = ProductController.getRandomFeatured(1, ProductController.ProductGroupAccessory);
            foreach (DataRow row in devices.Rows)
            {
                AccessoryProducts.Add(ProductController.rowToSimpleProduct(row));
            }
        }

        public List<string[]> MenuGameCategories = new List<string[]>();
        public List<Application> SoftwareProducts = new List<Application>();

        private void loadMenuGameAppCategory()
        {
            // Game categories
            DataTable dtSub = CategoryController.getCategories(ProductController.ProductGroupGame, "");
            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                string id = dtSub.Rows[i]["Id"].ToString();
                string name = dtSub.Rows[i]["Name"].ToString();
                string url = "game-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;
                name = "[Game] " + name;

                MenuGameCategories.Add(new string[] { id, name, url });
            }

            // Application categories
            dtSub = CategoryController.getCategories(ProductController.ProductGroupApplication, "");
            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                string id = dtSub.Rows[i]["Id"].ToString();
                string name = dtSub.Rows[i]["Name"].ToString();
                string url = "ud-" + name.NormalizeDiacriticalCharacters().ToLower().Replace(" ", "-") + "-" + id;
                name = "[Ứng dụng] " + name;

                MenuGameCategories.Add(new string[] { id, name, url });
            }

            // App / Game
            var apps = ApplicationController.getRandomApplications(-1, 3);
            foreach (DataRow row in apps.Rows)
            {
                var app = ApplicationController.rowToApplication(row);
                SoftwareProducts.Add(app);
                //app.filename = "application";
            }
            apps = GameController.getRandomGames(-1, 3);
            foreach (DataRow row in apps.Rows)
            {
                var app = ApplicationController.rowToApplication(row);
                SoftwareProducts.Add(app);
                //app.filename = "game";
            }

            // Sample Apps
            var rand = new Random();
            SoftwareProducts = SoftwareProducts.OrderBy(item => rand.Next()).ToList();
            if (SoftwareProducts.Count > 3)
                SoftwareProducts = SoftwareProducts.GetRange(0, 3);

            //SoftwareProducts.ForEach((app) => { app.remark = "ung-dung"; });
        }

        private void loadUserProfile()
        {
            loggedIn = Common.IsLoggedIn();

            if (!loggedIn)
                return;

            DataTable dt = SysUserController.getUserByEmail(Session["Email"] as string);
            user.email = dt.Rows[0]["email"] as string;

            user.firstName = dt.Rows[0]["firstName"] as string;
            if (string.IsNullOrWhiteSpace(user.firstName))
                user.firstName = user.email;

            user.lastName = dt.Rows[0]["lastName"] as string;

            user.avatar = dt.Rows[0]["avatar"] as string;
            if (string.IsNullOrEmpty(user.avatar))
                user.avatar = "1.png";
        }
    }
}