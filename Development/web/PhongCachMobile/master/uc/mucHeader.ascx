﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mucHeader.ascx.cs" Inherits="PhongCachMobile.master.uc.mucHeader" %>

<div data-role="header">
    <div class="menu-bar clearfix">
        <ul class="content-left">
            <li><a href="m-tin-khuyen-mai" data-ajax="false">Tin PR - Khuyến mại</a></li>
            <li><a href="m-tin-thi-truong" data-ajax="false">Tin thị trường</a></li>
            <li><a href="m-faq" data-ajax="false">FAQ</a></li>
            <li><a href="m-lien-he" data-ajax="false">Liên hệ</a></li>
        </ul>
        <% if(!loggedIn) { %>
        <ul class="content-right">
            <li><a href="m-dang-nhap"><i class="fa fa-user"></i></a></li>
        </ul>
        <% } else { %>
        <div class="logged dropdown">
            <a id="dLabel" data-target="#" href="m-trang-chu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="../images/home/avatar.jpg">
            </a>
            <ul class="dropdown-menu" aria-labelledby="dLabel">
                <%--<li><a href="#">My Profile</a></li>--%>
                <li><a href="dang-xuat">Log out</a></li>
            </ul>
        </div>
        <% } %>
    </div>
    <div class="menu-main">
        <div class="logo">
            <a href="/" >PhongCachMobile</a>
        </div>
        <div class="nav">
            <ul>
                <li><a href="#form-search" data-ajax="false" ><i class="fa fa-search"></i></a>
                    <div data-role="panel" data-position="left" data-position-fixed="true" data-display="overlay" data-theme="b" id="form-search">
                        <form id="search-frm" action="m-tim-kiem">
                            <input id="search-value" type="text" name="id">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>            
                    </div><!-- /panel -->
                </li>
                <li><a href="#nav-menu" data-ajax="false" data-iconpos="notext"><i class="fa fa-bars"></i></a>
                    <div data-role="panel" data-position="right" data-position-fixed="true" data-display="overlay" data-theme="a" id="nav-menu">
                            <ul>
                        <li <%= activeMenu == "gia-hot" ? "class='active'" : "" %>><a href="m-gia-hot" data-ajax="false">GIÁ HOT</a></li>
                        <li <%= activeMenu == "dtdd" ? "class='active'" : "" %> ><a href="m-dtdd" data-ajax="false">ĐIỆN THOẠI</a></li>
                        <li <%= activeMenu == "macbook" ? "class='active'" : "" %> ><a href="m-macbook" data-ajax="false">Macbook</a></li>
                        <li <%= activeMenu == "may-tinh-bang" ? "class='active'" : "" %> ><a href="m-may-tinh-bang" data-ajax="false">MÁY TÍNH BẢNG</a></li>
                        <li <%= activeMenu == "phu-kien" ? "class='active'" : "" %> ><a href="m-phu-kien" data-ajax="false">PHỤ KIỆN</a></li>
                        <li <%= activeMenu == "kho-may-cu" ? "class='active'" : "" %> ><a href="m-kho-may-cu" data-ajax="false">MÁY CŨ</a></li>
                        <li <%= activeMenu == "ung-dung" ? "class='active'" : "" %> ><a href="m-ung-dung" data-ajax="false">ỨNG DỤNG - GAME</a></li>
                        <li <%= activeMenu == "sua-chua-cai-dat" ? "class='active'" : "" %> ><a href="m-sua-chua-cai-dat" data-ajax="false">SỮA CHỮA - CÀI ĐẶT</a></li>
                        </ul>
                        </div><!-- /panel -->
                </li>
            </ul>
        </div>
    </div>
</div><!-- /header -->
