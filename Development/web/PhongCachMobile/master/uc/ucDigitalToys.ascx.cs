﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.master.uc
{
    public partial class ucDigitalToys : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadBanner();
        }

        protected List<Article> bannerItems = new List<Article>();
        protected void loadBanner()
        {
            // Do_Choi_So_ItemImages1 // V2-SideBar - Toys
            foreach (DataRow dr in SliderController.getSliders("71").Rows)
            {
                bannerItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
        }
    }
}