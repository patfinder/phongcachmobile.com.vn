﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProductListBare.ascx.cs" Inherits="PhongCachMobile.master.uc.ucProductListBare" %>
<% foreach(var product in ProductList) { %>
<li class="<%= product.isOutOfStock ? "over-product" : "" %>" <%= product.isComingSoon ? "comming-product" : "" %>">
    <% if (product.isOutOfStock) { %><img src="images/icon-het.png" class="icon-hethang"> <% } %>
    <% if (product.isComingSoon) { %><img src="images/icon-sapco.png" class="icon-sapco"> <% } %>
    <figure>
        <a href="<%= product.link  %>" class="img-box">
            <img src="images/<%= OldProduct ? "oldproduct" : "product" %>/<%= product.firstImage %>" alt="">
            <div class="group">
                <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                <%= product.discountPrice > 0 && !HideHotProduct ? "<span class='hot'></span>" : "" %>
            </div>
        </a>
        <figcaption>
            <h3><a href="<%= product.link  %>"><%= product.name %></a></h3>
            <span class="price"><%= product.currentPriceStr %></span>
            <% if(product.discountPrice > 0) { %><span class="price-regular"><strike>(<%= product.discountPriceStr %>)</strike></span> <% } %>
            <p class="intro"><%= product.shortDesc  %></p>
        </figcaption>
    </figure>
    <% if(!product.oldProduct) { %>
    <a href="<%= product.link  %>" class="read-more">Xem chi tiết</a>
    <% } %>
</li>
<% } %>