﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucApplicationListBare.ascx.cs" Inherits="PhongCachMobile.master.uc.ucApplicationListBare" %>

<% foreach(PhongCachMobile.model.Application app in applications) { %>
<li>
    <figure>

        <% 
       string filePath = app.filename;
       if (!string.IsNullOrWhiteSpace(app.filename) && app.filename.StartsWith("http"))
           filePath = app.filename;
       else
           filePath = (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.filename;
             %>

        <div class="box-img"><a href="<%= filePath %>">
            <img src="images/<%= (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.displayImage %>">
        </a></div>
        <figcaption>
            <div class="content-left">
                <h2><%= app.name %></h2>
                <p class="intro"><%= app.description %></p>
                <div class="information">
                    <p><span class="color-red">|</span> <strong>Cập nhật:</strong> <%= app.postedDate %>  <span class="color-red">|</span>  <strong class="color-red"><%= app.viewCount %></strong> lượt xem<br />
                    <span class="color-red">|</span> <strong>Yêu cầu:</strong> <%= app.requiredOS %>  <span class="color-red">|</span>  <strong>Dung lượng:</strong> <%= app.filesize %><br />
                    <span class="color-red">|</span> <strong>Nhà sản xuất:</strong> <%= app.developer %> <span class="color-red">|</span>  <strong>Phiên bản:</strong> <%= app.currentVersion %></p>
                </div>
            </div>
            <a href="<%= /* PhongCachMobile.util.Common.IsLoggedIn() ? app.download : PhongCachMobile.util.Common.m() + "dang-nhap" */
                filePath %>" class="download">Tải về</a>
        </figcaption>
    </figure>
</li>
<% } %>
