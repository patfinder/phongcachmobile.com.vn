﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.master.uc
{
    public partial class mucFooter : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadSetting();
                loadPromotion();
                loadMarketNews();
            }
        }

        protected List<Article> promotionArticles = new List<Article>();
        private void loadPromotion()
        {
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(3, "22", "56");
            foreach (DataRow row in dt.Rows)
            {
                promotionArticles.Add(ArticleController.rowToSimpleArticle(row));
            }
        }

        protected List<Article> marketNews = new List<Article>();
        private void loadMarketNews()
        {
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(3, "22", "55");
            foreach (DataRow row in dt.Rows)
            {
                marketNews.Add(ArticleController.rowToSimpleArticle(row));
            }
        }

        private void loadSetting()
        {
            litTotalVisit.Text = int.Parse(SysIniController.getSetting("TotalVisit", "151250")).ToString("#,##0");
            int onlineUser = int.Parse(SysIniController.getSetting("OnlineUsers", "200")) + int.Parse(Application["OnlineUsers"].ToString());
            litOnline.Text = onlineUser.ToString("#,##0");
        }
    }
}