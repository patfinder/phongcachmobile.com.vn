﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucBrandsLogoList.ascx.cs" Inherits="PhongCachMobile.master.uc.ucBrandsLogoList" %>
<div class="breadcrumb logo-list">
    <div class="content-center">
        <ol>
        <% foreach (PhongCachMobile.model.Article article in brandLogoes)
                   { %>
            <li><a href="<%= article.link %>">
                <img src="images/banner/<%= article.displayImg %>" alt="<%= article.displayImg %>" >
                </a></li>
        <% } %>
        </ol>
    </div>
</div>