﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.master.uc
{
    public partial class ucBrandsLogoList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            loadLogoes();
        }

        public List<Article> brandLogoes = new List<Article>();
        private void loadLogoes()
        {
            // Hot_Price_Logo_Urls1 // V2-Logos - Banners
            foreach (DataRow dr in SliderController.getSliders("70").Rows)
            {
                var article = SliderController.rowToSlider(dr).toArticle();
                article.link = Common.m() + article.link;
                brandLogoes.Add(article);
            }
        }
    }
}