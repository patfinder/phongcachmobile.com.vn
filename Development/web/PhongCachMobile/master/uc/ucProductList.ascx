﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucProductList.ascx.cs" Inherits="PhongCachMobile.master.uc.ucProductList" %>
<ul>
    <% foreach(var product in ProductList) { %>
    <li>
        <figure>
            <a href="<%= product.link  %>" class="img-box"><img src="images/product/<%= product.firstImage %>" alt="">
                <div class="group">
                    <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                    <%= product.discountPrice > 0 ? "<span class='hot'></span>" : "" %>
                </div>
            </a>
            <figcaption>
                <h3><a href="<%= product.link  %>"><%= product.name %></a></h3>
                <span class="price"><%= product.currentPriceStr %></span>
                <% if(product.discountPrice > 0) { %><span class="price-regular"><strike>(<%= product.discountPriceStr %>)</strike></span> <% } %>
                <p class="intro"><%= product.shortDesc  %></p>
            </figcaption>
        </figure>
        <a href="<%= product.link  %>" class="read-more">Xem chi tiết</a>
    </li>
    <% } %>
</ul>