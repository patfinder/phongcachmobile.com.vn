﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAccessoryGroup.ascx.cs" Inherits="PhongCachMobile.master.uc.ucAccessoryGroup" %>

<%@ Register Src="~/master/uc/ucAccessoryList.ascx" TagName="ucAccessoryList" TagPrefix="ucAccessoryList" %>

<div class="fittings">
    <div class="content-center">
        <div class="menu">
            <h2>Tai nghe - Loa</h2>
            <ul>
                <% foreach (var article in group)
                    { %>
                <li><a href="<%= article.link%>"><%= article.title%></a></li>
                <% } %>
            </ul>
        </div>
        <div class="list-fittins clearfix">
            <asp:PlaceHolder ID="PlaceHolder_aa" runat="server"></asp:PlaceHolder>
            <ucAccessoryList:ucAccessoryList ID="ucPhoneSpeakerProducts"
                ProductList="<%# products %>" Banners="<%# banners %>" runat="server" />
        </div>
    </div>
</div>
