﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAccessoryList.ascx.cs" Inherits="PhongCachMobile.master.uc.ucAccessoryList" %>
<ul>
    <li class="slide-home">
        <% foreach(PhongCachMobile.model.Article banner in Banners) { %>
        <div class="items"><a href="<%= banner.link %>"><img src="images/product/<%= banner.displayImg %>" alt=""></a></div>
        <% } %>
    </li>
    <% foreach(var product in ProductList) { %>
    <li>
        <figure>
            <div class="box-img"><a href="<%= product.link  %>"><img src="images/product/<%= product.firstImage %>" alt=""></a></div>
            <figcaption>
                <h2><a href="<%= product.link  %>"><%= product.name %></a></h2>
                <span class="price"><%= product.currentPriceStr %></span>
                <% if(product.discountPrice != 0) { %><span class="price-regular"><strike>(<%= product.discountPriceStr %>)</strike></span> <% } %>
                <p class="intro"><%= product.shortDesc  %></p>
            </figcaption>
        </figure>
    </li>
    <% } %>
</ul>
