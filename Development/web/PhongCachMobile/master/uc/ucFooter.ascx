﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFooter.ascx.cs" Inherits="PhongCachMobile.master.uc.ucFooter" %>

<div id="footer">
<footer class="footer">
    <div class="footer-top clearfix">
        <div class="col-3">
            <h3>TIN KHUYẾN MÃI</h3>
            <ul class="promotion">
            <% foreach(var article in promotionArticles) { %>
                <li><i class="fa fa-chevron-circle-right"></i><a href="<%= article.link %>"><%= HttpUtility.HtmlEncode(article.title) %></a></li>
            <% } %>
            </ul>

            <h3>HỆ THỐNG PHONGCACHMOBILE</h3>
            <ul>
                <li><i class="fa fa-chevron-circle-right"></i>58 Trần Quang Khải, P. Tân Định, Quận 1, Tp. HCM.</li>
                <%--<li><i class="fa fa-chevron-circle-right"></i>697 Lê Hồng Phong, Phường 10, Quận 10, Tp. HCM.</li>--%>
                <li><i class="fa fa-chevron-circle-right"></i>35A Xô Viết Nghệ Tĩnh, P. An Cư, Q. Ninh Kiều, Tp. Cần Thơ.</li>
            </ul>
        </div>
        <div class="col-3">
            <h3>TIN THỊ TRƯỜNG</h3>
            <ul>
            <% foreach (var article in marketNews)
               { %>
                <li><i class="fa fa-chevron-circle-right"></i><a href="<%= article.link %>"><%= HttpUtility.HtmlEncode(article.title) %></a></li>
            <% } %>
            </ul>

            <h3>HỖ TRỢ  <span>(09:00 - 21:00)</span></h3>
            <ul>
                <li class="clearfix"><span class="text-left"><i class="fa fa-chevron-circle-right"></i>[Tp. HCM]</span>  	<span class="text-right"><span>Bán hàng</span>	: (08) 3526 8254 - 3526 8255<br />
                    <span>Kỹ thuật</span>	: (08) 3526 8255. Ext 103 - 104</span> </li>
                <li  class="clearfix"><span class="text-left"><i class="fa fa-chevron-circle-right"></i>[Cần Thơ]</span>	<span class="text-right"><span>Bán hàng</span>	: (071) 0381 9477 - 03819478<br />
                    <span>Kỹ thuật</span>	: (071) 0381 9476</span></li>
            </ul>
        </div>
        <div class="col-3">
            <div class="content-facebook">
                <div id="fb-root"></div>
                <script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=862852357144009";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
            </div>

            <div class="fb-page" data-href="http://facebook.com/phongcachmobile.com.vn" data-small-header="true"
                data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"
                data-adapt-container-width="true" data-show-posts="true">
            <div class="fb-xfbml-parse-ignore"><blockquote cite="http://facebook.com/phongcachmobile.com.vn">
                <a href="http://facebook.com/phongcachmobile.com.vn">Phongcachmobile</a></blockquote></div></div>

            <%--<div class="content-facebook">
                <img src="images/fb.jpg">
            </div>--%>

        </div>
    </div>
    <div class="footer-bottom clearfix">
        <ul class="content-left">
            <li><a href="#"><img src="images/mastercard.jpg" alt="Master Card"></a></li>
            <li><a href="#"><img src="images/visa.jpg" alt="Visa"></a></li>
            <li>© Copyright 2015 by <a href="/">PhongCachMobile.com.vn</a></li>
        </ul>
        <ul class="content-right">
            <li>Đang online: <span class="munber"><asp:Literal ID="litOnline" runat="server"></asp:Literal></span>   |   Lượt truy cập: <span class="munber"><asp:Literal ID="litTotalVisit" runat="server"></asp:Literal></span></li>
            <li><a href="lien-he" class="button">LIÊN HỆ</a></li>
        </ul>
    </div>
</footer>

<!-- [[%FOOTER_LINK]] -->	</div>