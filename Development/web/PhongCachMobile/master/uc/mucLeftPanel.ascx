﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mucLeftPanel.ascx.cs" Inherits="PhongCachMobile.master.uc.mucLeftPanel" %>
<div data-role="panel" id="left-panel" data-theme="g">
                <!-- Social Icons Starts -->
                <div class="l-social-icons">
                    <ul>
                        <li><a href='https://www.facebook.com/phongcachmobile.com.vn' target="_blank"><img src="m/images/icons/social-icons/fb.png" width="36" height="36"/></a></li>
                        <li><a href='https://www.youtube.com/user/phongcachmobile' target="_blank"><img src="m/images/icons/social-icons/youtube.png" width="36" height="36"/></a></li>
                    </ul>
                </div>
                <!-- Social Icons Ends -->
                <!-- Main Menu Starts --> 
                <div id="accordion">
                    <asp:Literal ID="litMenu" runat="server"></asp:Literal>
                </div>
                <!-- Main Menu Ends -->
            </div>