﻿using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.master.uc
{
    public partial class ucAccessoryGroup : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [Bindable(true)]
        public List<Article> group { get; set; }

        [Bindable(true)]
        public List<Article> banners { get; set; }

        [Bindable(true)]
        public List<Product> products { get; set; }
    }
}