﻿using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile.master.uc
{
    public partial class ucProductListBare : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public bool HideHotProduct { get; set; }
        public bool OldProduct { get; set; }

        [Bindable(true)]
        public List<Product> ProductList { get; set; }
    }
}