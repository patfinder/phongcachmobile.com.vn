﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;

namespace PhongCachMobile.master.uc
{
    public partial class mucLeftPanel : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadMenu();
            }
        }

        private void loadMenu()
        {
            litMenu.Text = "<ul id='items'>";
            DataTable dt = MenuController.getMenus("2", "");
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    string text = dr["Text"].ToString();
                    string actionUrl = dr["ActionUrl"].ToString();
                    string inOrd = dr["InOrder"].ToString();

                    string filename = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]).ToLower();
                    string icon = "";

                    if (("m-" + actionUrl).ToLower().Contains("m-gia-hot.aspx"))
                        icon = "<i class='fa fa-thumbs-up fa-2x'></i>";
                    if (("m-" + actionUrl).ToLower().Contains("m-dtdd.aspx"))
                        icon = "<i class='fa fa-mobile fa-2x'></i>";
                    if (("m-" + actionUrl).ToLower().Contains("m-may-tinh-bang.aspx"))
                        icon = "<i class='fa fa-tablet fa-2x'></i>";
                    if (i == 3)
                        icon = "<i class='fa fa-headphones fa-2x'></i>";
                    if (("m-" + actionUrl).ToLower().Contains("m-kho-may-cu.aspx"))
                        icon = "<i class='fa fa-exchange fa-2x'></i>";
                    if (i == 5)
                        icon = "<i class='fa fa-gamepad fa-2x'></i>";

                    if (("m-" + actionUrl).ToLower().Contains(filename) ||
                      (i == 1 && filename.Contains("m-loai-dtdd.aspx")) ||
                      (i == 2 && filename.Contains("m-loai-mtb.aspx")) ||
                      (i == 3 && (filename.Contains("m-loai-phu-kien-dtdd.aspx") || filename.Contains("m-loai-phu-kien-mtb.aspx"))) ||
                      (i == 5 && (filename.Contains("m-loai-game.aspx") || filename.Contains("loai-ud.aspx"))))
                    {
                       
                        litMenu.Text += "<li><a rel='external' class='activated' href='m-" + actionUrl.Split('.')[0] + "'>" + icon +  text + "</a>";
                    }
                    else
                    {
                        litMenu.Text += "<li><a rel='external' href='m-" + actionUrl.Split('.')[0] + "'>" + icon + text + "</a>";
                    }

                    if (actionUrl == "dtdd.aspx") //Dien thoai di dong
                        loadMenuPhoneCategory();
                    else if (actionUrl == "may-tinh-bang.aspx") //May tinh bang
                        loadMenuTabletCategory();
                    else if (inOrd == "5") //
                        loadMenuGameAppCategory();
                    else if (inOrd == "3") //
                        loadMenuAccessoriesCategory();

                    litMenu.Text += "</li>";

                }

                //string url = System.IO.Path.GetFileName(Request.ServerVariables["SCRIPT_NAME"]).ToLower();

                //if (url.ToLower().Contains("m-tin-khuyen-mai.aspx"))
                //    litMenu.Text += "<li><a rel='external' class='activated' href='m-tin-khuyen-mai" + "'><i class='fa fa-gift fa-2x'></i>TIN KHUYẾN MÃI</a>";
                //else
                //    litMenu.Text += "<li><a rel='external' href='m-tin-khuyen-mai" + "'><i class='fa fa-gift fa-2x'></i>TIN KHUYẾN MÃI</a>";

                //if (url.ToLower().Contains("m-tin-thi-truong.aspx"))
                //    litMenu.Text += "<li><a rel='external' class='activated' href='m-tin-thi-truong" + "'><i class='fa fa-credit-card fa-2x'></i>TIN THỊ TRƯỜNG</a>";
                //else
                //    litMenu.Text += "<li><a rel='external'  href='m-tin-thi-truong" + "'><i class='fa fa-credit-card fa-2x'></i>TIN THỊ TRƯỜNG</a>";

                
            }
            litMenu.Text += "</ul>";


            //<ul id="items">
            //    <li>
            //        <a href="index.html" rel="external" class="activated"><i class="fa fa-home fa-2x"></i>HOME</a>
            //    </li>
            //    <li><a href="products.html" rel="external"><i class="fa fa-suitcase fa-2x"></i>PRODUCTS</a></li>
            //    <li>
            //        <a href="#"><i class="fa fa-video-camera fa-2x"></i>GALLERY</a>
            //        <ul class="sub-items">
            //            <li>
            //                <a href="photo-gallery.html" rel="external">- PHOTO GALLERY</a>
            //            </li>
            //            <li>
            //                <a href="video-gallery.html" rel="external">- VIDEO GALLERY</a>
            //            </li>
            //            <li>
            //                <a href="2-columns-portfolio.html" rel="external">- PORTFOLIO GALLERY</a>
            //            </li>
            //        </ul>
            //    </li>
            //</ul>
        }

        private void loadMenuAccessoriesCategory()
        {
            DataTable dtSub = CategoryController.getCategories("7", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litMenu.Text += "<ul class='sub-items'>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {

                    DataRow drSub = dtSub.Rows[i - 1];
                    litMenu.Text += "<li><a  rel='external' href='m-pkdtdd-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + drSub["Name"].ToString() + "</a></li>";
                }

                DataTable dtApp = CategoryController.getCategories("8", "");
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtApp.Rows.Count; i++)
                    {

                        DataRow drApp = dtApp.Rows[i - 1];
                        litMenu.Text += "<li><a  rel='external' href='m-pkmtb-" + drApp["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drApp["Id"].ToString() + "'>" + drApp["Name"].ToString() + "</a></li>";
                    }
                }
                litMenu.Text += "</ul>";
            }
        }

        private void loadMenuGameAppCategory()
        {
            DataTable dtSub = CategoryController.getCategories("5", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litMenu.Text += "<ul class='sub-items'>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {

                    DataRow drSub = dtSub.Rows[i - 1];
                    litMenu.Text += "<li><a  rel='external' href='m-game-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>Game " + drSub["Name"].ToString() + "</a></li>";
                }

                DataTable dtApp = CategoryController.getCategories("6", "");
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtApp.Rows.Count; i++)
                    {

                        DataRow drApp = dtApp.Rows[i - 1];
                        litMenu.Text += "<li><a  rel='external' href='m-ud-" + drApp["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drApp["Id"].ToString() + "'>Ứng dụng " + drApp["Name"].ToString() + "</a></li>";
                    }
                }
                litMenu.Text += "</ul>";
            }
        }

        private void loadMenuTabletCategory()
        {
            DataTable dtSub = CategoryController.getCategories("4", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litMenu.Text += "<ul class='sub-items'>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {

                    DataRow drSub = dtSub.Rows[i - 1];
                    litMenu.Text += "<li><a  rel='external' href='m-mtb-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + drSub["Name"].ToString() + "</a></li>";
                }

                //DataTable dtOS = OperatingSystemController.getOperatingSystemsByGroupId("4");
                //if (dtOS != null && dtOS.Rows.Count > 0)
                //{
                //    foreach (DataRow drOS in dtOS.Rows)
                //        litMenu.Text += "<li><a href='m-mtb-hdh-" + drOS["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drOS["Id"].ToString() + "'>" + "Hệ điều hành " + drOS["Name"].ToString() + "</a></li>";

                //}

                //DataTable dtFeature = FeatureController.getFeaturesByGroupId("4");
                //if (dtFeature != null && dtFeature.Rows.Count > 0)
                //{
                //    foreach (DataRow drFeature in dtFeature.Rows)
                //        litMenu.Text += "<li><a href='m-mtb-tn-" + drFeature["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drFeature["Id"].ToString() + "'>" + drFeature["Name"].ToString() + "</a></li>";
                //}

                litMenu.Text += "</ul>";
            }
        }

        private void loadMenuPhoneCategory()
        {
            DataTable dtSub = CategoryController.getCategories("3", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litMenu.Text += "<ul class='sub-items'>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {

                    DataRow drSub = dtSub.Rows[i - 1];
                    litMenu.Text += "<li><a rel='external' href='m-dtdd-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + drSub["Name"].ToString() + "</a></li>";
                }

                //DataTable dtOS = OperatingSystemController.getOperatingSystemsByGroupId("3");
                //if (dtOS != null && dtOS.Rows.Count > 0)
                //{
                //    foreach (DataRow drOS in dtOS.Rows)
                //        litMenu.Text += "<li><a href='m-dtdd-hdh-" + drOS["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drOS["Id"].ToString() + "'>" + "Hệ điều hành " + drOS["Name"].ToString() + "</a></li>";
                //}
                litMenu.Text += "</ul>";
            }
        }
    }
}