﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.util;
using PhongCachMobile.master.uc;

namespace PhongCachMobile.master
{
    public partial class MHome : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.TemplateType = master.TemplateType.OldType;

            //frm.Action = Request.RawUrl;
            if (!this.IsPostBack)
            {
               checkMobile();
               // if (Session["App"] == null)
               // {
               //     Session["App"] = true;
               //     Response.Redirect("m-pre-portal");
               // }
            }
        }

        public TemplateType TemplateType
        {
            get;
            set;
        }

        public HeaderType HeaderType
        {
            get
            {
                return idUCHeader.HeaderType;
            }
            set
            {
                idUCHeader.HeaderType = value;
            }
        }

        private void checkMobile()
        {
            if (!Common.fBrowserIsMobile())
            {
                Response.Redirect("gia-hot");
            }
        }
    }
}