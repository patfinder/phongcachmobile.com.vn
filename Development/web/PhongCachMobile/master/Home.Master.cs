﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.master.uc;

namespace PhongCachMobile.master
{
    public enum TemplateType
    {
        Unknown,
        OldType,
        NewType,
    };

    public partial class Home : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.TemplateType == master.TemplateType.Unknown)
                this.TemplateType = master.TemplateType.OldType;

            checkMobile();
            //frm.Action = Request.RawUrl;
            //loadSupport();

            Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile");
        }

        public string Title = "Phong Cách Mobile";

        public TemplateType TemplateType
        {
            get;
            set;
        }

        public HeaderType HeaderType
        {
            get
            {
                return idUCHeader.HeaderType;
            }
            set
            {
                idUCHeader.HeaderType = value;
            }
        }

        private void checkMobile()
        {
            if (Common.fBrowserIsMobile())
            {
                // Check if product page, return
                if (Request.RawUrl.StartsWith("sp-") ||
                    Request.RawUrl.StartsWith("/sp-"))
                {
                    Response.Redirect("m-" + Request.RawUrl.StripLeadingFSlash());

                    return;
                }

                // Home page
                if (string.IsNullOrEmpty(Request.RawUrl) || Request.RawUrl == "/")
                    Response.Redirect("m-trang-chu");

                // Any page
                var newUrl = "m-" + Request.RawUrl.StripLeadingFSlash();
                if(Common.UrlExists(Request, newUrl))
                    Response.Redirect(newUrl);
                else
                    Response.Redirect("m-trang-chu");
            }
        }
    }

}