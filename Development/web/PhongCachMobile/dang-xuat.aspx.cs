﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile
{
    public partial class dang_xuat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                checkLogout();
            }
        }

        private void checkLogout()
        {
            if (Session["Email"] != null && Session["Email"].ToString() != "")
            {
                Session.Abandon();
                Session.Clear();
                Session.RemoveAll();
                Response.Redirect("gia-hot");
            }
        }
    }
}