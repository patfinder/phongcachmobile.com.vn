$(document).ready(function () {

    // ------------------------------- Desktop Sliders -------------------------------

    $('.slide-home').slick({
        dots: false,
        infinite: true,
        slidesToShow: 1,
        variableWidth:true,
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: true
    });
    $('.slide-banner').slick({
        infinite: true,
        slidesToShow: 1,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 7000,
        arrows: false,
        dots: true,
        asNavFor: '.slide-right'
    });
    $('.slide-right').slick({
        variableWidth: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slide-banner',
        dots: false,
        arrows: false
    });
    var w_secify = $('.specify').width();
    var h_box;
    $('.slide-home .items').css({'width': w_secify + 'px'});
    setInterval(function(){
        h_box = $('.home-top .specify').height()/2;
        $('.home-top figcaption').css({'height': h_box + 'px'});
        $('.home-top .tb-middle').css({'height': h_box + 'px'});
        $('.home-top .container-img > div').css({'height': h_box + 'px'});
    }, 2000);

    $(window).resize( function(){
        w_secify = $('.specify').width();
        $('.slide-home .items').css({'width': w_secify + 'px'});
        h_box = $('.home-top .specify').height()/2;
        $('.home-top figcaption').css({'height': h_box + 'px'});
        $('.home-top .tb-middle').css({'height': h_box + 'px'});
        $('.home-top .container-img > div').css({'height': h_box + 'px'});
    });

    // ------------------------------- Mobile Sliders -------------------------------

    // ------------------------------- ~Mobile Sliders~ -------------------------------


    var top_footer = $('.footer').offset().top;
    if($('.aside-right').size()){
        var top_right = $('.aside-right').offset().top;
    }

    if($('.technology').size()){
        top_right = $('.technology').offset().top;
    }

    if($('.group-infor').size()) {
        $('.aside-right').css({'height': $('.group-infor').height(), 'overflow':'hidden'});
    }
    else{
        $('.aside-right').css({'height': $('.content').height(), 'overflow':'hidden'});
    }
    $sidebar = $(".aside-right .content-right");
    $window = $(window);
    var sidebarOffset = $sidebar.offset(),
        height_html_stop = $('html, body').height();
    if($('.group-infor').size()){
        sidebarOffset = $('.group-infor').offset();
    }
    $(window).scroll( function(){
        var top_scroll = $(this).scrollTop();

        var w_window = $(this).width();
        //scroll vertical
        var currentLeft = $(this).scrollLeft();
        if(currentLeft >  w_window - 20) {
            //handler
        }
        //console.log(top_right, top_scroll);
        //console.log(sidebarOffset.top, $window.scrollTop(), $('.group-infor').offset().top);
        if($('.group-infor').size()){
            sidebarOffset = $('.group-infor').offset();
        }
        if($('.aside-right').size()) {
            if ($window.scrollTop() > sidebarOffset.top) {
                $sidebar.addClass('fixTop');
                $sidebar.stop().animate({
                    marginTop: ($window.scrollTop() - sidebarOffset.top)
                });
            } else {
                $sidebar.removeClass('fixTop');
                $sidebar.stop().animate({
                    marginTop: 0
                });
            }

        }
        if($('#wrapper3').hasClass('subpage')){
            if($(this).scrollTop() > 138){
                $('.menu-bar').addClass('fixTop');
            }
            if($(this).scrollTop() < 138){
                $('.menu-bar').removeClass('fixTop');
            }
            if($(this).scrollLeft() > 100){
                $('.menu-bar').addClass('fixRight');
            }
            if($(this).scrollLeft() < 100){
                $('.menu-bar').removeClass('fixRight');
            }
        }
        else{
            if($(this).scrollTop() > 67){
                $('#header').addClass('fixTop');
            }
            if($(this).scrollTop() < 67){
                $('#header').removeClass('fixTop');
            }

            if($(this).scrollLeft() > 100){
                $('#header').addClass('fixRight');
            }
            if($(this).scrollLeft() < 100){
                $('#header').removeClass('fixRight');
            }
        }

    });

    jQuery('.dropdown').hover(function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    }, function() {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });
    $('#faqs li').click( function(){
        $('#faqs div').removeClass('open');
        $(this).next().addClass('open');
    });

    //click open or close content on detail page
    $('.technology .read-more.open').click(function () {
        $('.technology-full').addClass('open');
        $('.technology .close').css({ 'display': 'block' });
        $(this).css({ 'display': 'none' });
        //$('.group-infor .aside-right').css({'height': $('.group-infor').height(), 'overflow':'hidden'});

    });
    $('.technology .close').click(function () {
        $('.technology-full').removeClass('open');
        $('.technology .close').css({ 'display': 'none' });
        $('.technology .read-more.open').css({ 'display': 'block' });
        $('body').animate({scrollTop: $(".technology .read-more.open").offset().top}, 0);
        //$('.group-infor .aside-right').css({'height': $('.group-infor').height(), 'overflow':'hidden'});
    });
    $('.article-details .read-more.open').click(function () {
        $('.article-details .content-text').addClass('open');
        $('.article-details .close').css({ 'display': 'block' });
        $(this).css({ 'display': 'none' });
        $('.group-infor .aside-right').css({'height': $('.group-infor .aside-right').height() + $('.article-details .content-text .content-more').height(), 'overflow':'hidden'});
    });
    $('.article-details .close').click(function () {
        $('.article-details .content-text').removeClass('open');
        $('.article-details .close').css({ 'display': 'none' });
        $('.article-details .read-more.open').css({ 'display': 'block' });
        $('body').animate({scrollTop: $(".article-details").offset().top}, 0);
        $('.group-infor .aside-right').css({'height': $('.group-infor').height() - $('.article-details .content-text .content-more').height(), 'overflow':'hidden'});

    });


});
