﻿
jQuery(document).ready(function () {

	// hide #back-top first
    jQuery("#back-top").hide();

	// fade in #back-top
	jQuery(function () {
		jQuery(window).scroll(function () {
			if (jQuery(this).scrollTop() > 100) {
				jQuery('#back-top').fadeIn();
			} else {
				jQuery('#back-top').fadeOut();
			}
            if (jQuery(this).scrollTop() > 37) {
                jQuery('.ui-header').addClass('ui-header-fixed');
                jQuery('#page').addClass('ui-page-header-fixed');
            } else {
                jQuery('.ui-header').removeClass('ui-header-fixed');
                jQuery('#page').removeClass('ui-page-header-fixed');
            }
		});

		// scroll body to 0px on click
		jQuery('#back-top a').click(function () {
			jQuery('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
});

// Apply handling of filter
function applyFilterHandling(divID, swap, callback) {

    var divSel = "div#" + divID + ">ul a";
    $(divSel).on("click", function () {

        // Clear feature filter
        paramValues.featureID = -1;

        console.log("filter-click: " + $(this).text() + ", swap: " + swap);

        var field = filterIDs[divID];
        paramValues[field] = $(this).attr("value");

        if (swap) {
            // swap value
            var value = 1 - parseInt($(this).attr("value"));
            $(this).attr("value", value);

            // swap caption
            var tmp = $(this).parent().parent().parent().find("span").text();
            $(this).parent().parent().parent().find("span").text($(this).text());

            $(this).text(tmp);
        }
        else {
            $(this).parent().parent().parent().find("span").text($(this).text());
        }

        if (callback)
            callback();
    });
}

function loadMoreProducts(apiName, clear, pageSize) {
	doLoadMore("ProductService.asmx", apiName, clear, pageSize);
};

function loadMoreApps(apiName, clear, pageSize) {
	doLoadMore("ApplicationService.asmx", apiName, clear, pageSize);
};

function loadMoreArticles(apiName, clear, pageSize) {
	doLoadMore("ArticleService.asmx", apiName, clear, pageSize, false);
};

/*
    isProduct: Default True. True if this is product, otherwise this is article.
*/
function doLoadMore(serviceName, apiName, clear, pageSize, isProduct) {

    isProduct = typeof isProduct !== 'undefined' ? isProduct : true;
	pageSize = typeof pageSize !== 'undefined' ? pageSize : 20;

	if (clear) {
	    paramValues.page = 0;
	    $("div>a.read-more").html("Xem thêm <span class='number'>" + pageSize + "</span> " + (isProduct ? "sản phẩm" : "tin"));
	} else {
	    paramValues.page += 1;
	}

	console.log("paramValues: " + JSON.stringify(paramValues));

	$.ajax({
	    type: "POST",
	    url: "/" + serviceName + "/" + apiName,
	    async: true,
	    data: paramValues,
	    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
	    dataType: "XML",
	    success: function (xdoc) {
	        // console.log("success: -----------------");

	        // clear current list
	        if (clear) {
	            $(".product-other>ul").text("");
	        }

	        xml = $(xdoc);
	        // <string>
	        //   <count/>
	        //   <items> ... </items>
	        // </string>

	        // get inner text
	        //html = xml.find("string").text();
	        jsonResult = JSON.parse(xml.find("string").text());

	        remainCount = jsonResult.remainCount;
	        html = jsonResult.items;
	        if (/\S/.test(html)) {
	            $(".product-other>ul").append(html);
	        }

	        $("div>a.read-more").html("Xem thêm <span class='number'>" + (remainCount > pageSize ? pageSize : remainCount) + "</span> " + (isProduct ? "sản phẩm" : "tin"));
	    },
	    error: function () {
	        console.log("error: -----------------");
	    }
	});
};
