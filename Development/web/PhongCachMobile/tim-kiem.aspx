﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home2.Master" AutoEventWireup="true" CodeBehind="tim-kiem.aspx.cs" Inherits="PhongCachMobile.tim_kiem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="css/category.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready(function () {
            jQuery(".link-compare").easyTooltip();
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="page-title category-title">
                                <h2>
                                    <asp:Literal ID="litSearch" runat="server"></asp:Literal></h2>
                            </div>
                            <div class="col-main">
                                <div class="category-products">
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litTopItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị </label>
                                                <asp:DropDownList ID="ddlTopDisplay" runat="server" OnSelectedIndexChanged="ddlTopDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                     <asp:ListItem Text="9" Value="9" />
                                                    <asp:ListItem Text="15" Value="15" />
                                                    <asp:ListItem Text="30" Value="30" />
                                                </asp:DropDownList>
                                                <label>
                                                    &nbsp; &nbsp; &nbsp; Sắp xếp </label>
                                                <asp:DropDownList ID="ddlTopSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Mức giá giảm" Value="hightolowprice" />
                                                    <asp:ListItem Text="Mức giá tăng" Value="lowtohighprice" />
                                                    <asp:ListItem Text="Ngày đăng" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemTop" PageSize="9" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lstItem" runat="server">
                                        <LayoutTemplate>
                                            <ul class="products-grid" style="display: inline-block;">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="item <%# (((ListViewDataItem)Container).DataItemIndex) % 3 == 2 ? "last" : "" %> ">
                                                <div style="height:80px;">
                                                <h2 class="product-name product-name-height">
                                                    <a href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                       <%#Eval("Name").ToString().Length > 70 ? Eval("Name").ToString().Substring(0, 70) + "..." : Eval("Name").ToString()%></a></h2>
                                                <div class="desc std">
                                                    <%#Eval("ShortDesc")%>
                                                </div></div>
                                                <div class="grid-inner">
                                                    <a href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" class="product-image">
                                                        <img class="pr-img" src="images/product/<%#Eval("FirstImage")%>" width="150" style="max-height:203px;"
                                                            alt="<%#Eval("Name")%>"></a>
                                                </div>
                                                <div class="product-box-2">
                                                    <div class="product-atr-height">
                                                        <div class="price-box">
                                                              <span class="regular-price" id="product-price-26"><span class="price">
                                                                <%# Eval("sPrice").ToString() == "-1.00" ? "Available on amazon" : "$ " + Eval("sPrice")%></span>
                                                                 <%# (Eval("sPrice").ToString() != "-1.00" && Eval("DiscountPrice").ToString() != "" )? "<span class='oldprice'>$ " + double.Parse(Eval("CurrentPrice").ToString()) + "</span>" : ""%>
                                                                 </span>
                                                        </div>
                                                    </div>
                                                    <div class="actions">
                                                        <a class="btnAddCart" href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                            <span>Chi tiết</span></a>  &nbsp; <%# (Eval("DiscountPrice").ToString() != "") ? "<img src='images/hotprice.png' style='margin-top:-10px;' height='40px' alt='' />" : ((Eval("IsGifted").ToString() == "" || Eval("IsGifted").ToString() == "False") ? "" : "<img src='images/giftpack.png' style='margin-top:-10px;' height='40px' alt='' />")%> 
                                                      <%--  <ul class="add-to-links">
                                                            <li>
                                                                <asp:LinkButton ToolTip="Thêm vào so sánh"  ID="lbtCompare" CommandName="addCompare" CssClass="link-compare" runat="server">
                                                                Thêm vào so sánh</asp:LinkButton>
                                                            </li>
                                                        </ul>--%>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litBotItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị </label>
                                                <asp:DropDownList ID="ddlBotDisplay" runat="server" OnSelectedIndexChanged="ddlBotDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="9" Value="9" />
                                                    <asp:ListItem Text="15" Value="15" />
                                                    <asp:ListItem Text="30" Value="30" />
                                                </asp:DropDownList>
                                                <label>
                                                    &nbsp; &nbsp; &nbsp; Sắp xếp </label>
                                                <asp:DropDownList ID="ddlBotSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBotSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Mức giá giảm" Value="hightolowprice" />
                                                    <asp:ListItem Text="Mức giá tăng" Value="lowtohighprice" />
                                                    <asp:ListItem Text="Ngày đăng" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemBot" PageSize="9" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>
                                           Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                      <asp:ListView ID="lstNews" runat="server">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                             <li>
                                                <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>" width="60px" class="newsImg" />
                                                </a>
                                                <div class="product-name"><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>"><%# Eval("Title")%></a> <%# Eval("TimeSpan")%></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>