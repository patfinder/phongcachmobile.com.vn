﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile
{
    public partial class portal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!this.IsPostBack)
            {
                checkMobile();
                loadSlider();
                loadLang();
            }
        }

            

        private void loadSlider()
        {
            DataTable dt = SliderController.getSliders("52");
            if (dt != null && dt.Rows.Count >0)
            {
                litNavigation.Text = "<nav class='controller js-home-controller'>";
                litSlider.Text = "";

                for(int i=0; i< dt.Rows.Count; i++)
                {
                    litNavigation.Text += "<a class='slide-trigger active ga-click-track' href='#' data-hero-slide='" + i + "'>";
                    litNavigation.Text += "<span class='blue-dot'></span>";
                    litNavigation.Text += "<span class='slide-trigger'>" + (i+1) + "</span>";
                    litNavigation.Text += "</a>";

                    litSlider.Text += "<article class='active hero-slide js-home-slide' data-slide='" + i + "'>";
                    litSlider.Text += "<div class='headline'>";
                    litSlider.Text += "<h1>" + dt.Rows[i]["Title"].ToString() + "</h1>";
                    litSlider.Text += "<p>" + dt.Rows[i]["Text"].ToString() + "</p>";
                    litSlider.Text += "</div>";
                    litSlider.Text += "<img src='images/slide/" +  dt.Rows[i]["DisplayImage"].ToString() + "' class='js-hisrc-preloader' />";
                    litSlider.Text += "<img class='hisrc' src='images/slide/" + dt.Rows[i]["DisplayImage"].ToString() + "' data-1x='images/slide/" + dt.Rows[i]["DisplayImage"].ToString() +"' style='height: auto; width: 100%;' />";
                    litSlider.Text += "</article>";
                }

                litNavigation.Text += "</nav>";
            }

        }

        private void checkMobile()
        {
            if (Common.fBrowserIsMobile())
            {
                Response.Redirect("m-pre-portal");
            }
        }

        private void loadLang()
        {
            this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile - Phong cách riêng cho chính bạn!");
        }
    }
}