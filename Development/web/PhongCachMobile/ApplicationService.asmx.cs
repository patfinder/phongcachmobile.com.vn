﻿using Newtonsoft.Json;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PhongCachMobile
{
    /// <summary>
    /// Summary description for ApplicationService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ApplicationService : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetApps(string appType, string os, string sortView, string page)
        {
            // appType is actually App Category
            return DoGetApps(appType, os, sortView, page);
        }

        protected string DoGetApps(string appCat, string os, string sortView, string page)
        {
            int iAppCat = string.IsNullOrEmpty(appCat) ? -1 : int.Parse(appCat);
            OSType osType = string.IsNullOrWhiteSpace(os) ? OSType.Unknown : (OSType)Enum.Parse(typeof(OSType), os);

            bool bSortView = string.IsNullOrEmpty(sortView) ? false : int.Parse(sortView) == 1;
            int iPage = string.IsNullOrEmpty(page) ? 0 : int.Parse(page);

            if (iPage < 0)
                iPage = 0;

            string result = "";

            int iPageSize = int.Parse(SysIniController.getSetting("Ung_Dung_So_SP_Filter_Page", "8"));
            int remainCount;
            DataTable dt = ApplicationController.getAppWithFilter(ApplicationController.appCategoryTypeMap[iAppCat],
                iAppCat, osType, bSortView, out remainCount, iPage, iPageSize);

            foreach (DataRow row in dt.Rows)
            {
                var app = ApplicationController.rowToApplication(row);

                string itemPattern =
                "    <li>" +
                "        <figure>" +
                // (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.filename
                "            <div class='box-img'><a href='" + app.filename + "'>" +
                "                <img src='images/" + (app is PhongCachMobile.model.Game ? "game/" : "application/") + app.displayImage + "'>" +
                "            </a></div>" +
                "            <figcaption>" +
                "                <div class='content-left'>" +
                "                    <h2>" + app.name + "</h2>" +
                "                    <p class='intro'>" + app.description + "</p>" +
                "                    <div class='information'>" +
                "                        <p><span class='color-red'>|</span> <strong>Cập nhật:</strong> " + app.postedDate + " <span class='color-red'>|</span>  <strong class='color-red'>" + app.viewCount + "</strong> lượt xem<br />" +
                "                        <span class='color-red'>|</span> <strong>Yêu cầu:</strong> " + app.requiredOS + " <span class='color-red'>|</span>  <strong>Dung lượng:</strong> " + app.filesize + "<br />" +
                "                        <span class='color-red'>|</span> <strong>Nhà sản xuất:</strong> " + app.developer + " <span class='color-red'>|</span>  <strong>Phiên bản:</strong> " + app.currentVersion + "</p>" +
                "                    </div>" +
                "                </div>" +
                "                <a href='" + /* (Common.IsLoggedIn() ? app.download : Common.m() + "dang-nhap") */ app.filename + "' class='download'>Tải về</a>" +
                "            </figcaption>" +
                "        </figure>" +
                "    </li>";

                result += itemPattern;
            }

            return JsonConvert.SerializeObject(new { remainCount = remainCount, items = result });
        }
    }
}
