﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class m_quen_mat_khau : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
            this.Title = "Quên mật khẩu?";
        }

        private void loadLang()
        {
            litRegister.Text =  "Quên mật khẩu?";
            this.lbtSend.Text = "<span><span>" + LangController.getLng("litSend.Text", "Gửi") + "</span></span>";
            this.reqValEmail.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
            this.rgeValEmail.ErrorMessage = LangController.getLng("litInvalidFormat.Text", "Sai định dạng");
        }

        protected void lbtSend_Click(object sender, EventArgs e)
        {
            if (!SysUserController.checkAvailableUser(txtEmail.Text))
            {
                this.lblEmailResponse.ForeColor = System.Drawing.Color.Red;
                this.lblEmailResponse.Text = LangController.getLng("litNonExistAccount.Text", "Email không tồn tại.");
            }
            else
            {
                forgetPasswordResponse.Visible = true;
                frmInput.Visible = false;

                string randomPassword = Common.getRandomString(8);
                bool result = SysUserController.resetPassword(txtEmail.Text, randomPassword);
                if (result)
                {
                    bool sendResult = sendResetPasswordEmail(txtEmail.Text, randomPassword);
                    if (sendResult)
                    {
                        this.litForgetPasswordResponse.Text = String.Format(LangMsgController.getLngMsg("litForgetPasswordResponse.OK", "Một email chứa thông tin về mật khẩu mới đã được gửi đến địa chỉ <font style='font-weight:bold;color:red'><u>{0}</u></font>. Hãy kiểm tra email của bạn và đăng nhập bằng mật khẩu mới."), txtEmail.Text);
                        this.Title = LangController.getLng("litForgetPasswordResponse.OK", "Tạo mật khẩu mới thành công");
                    }
                    else
                    {
                        this.litForgetPasswordResponse.Text = LangMsgController.getLngMsg("litForgetPasswordResponse.Error", "Đã có lỗi xảy ra trong quá trình tạo mật khẩu mới.<br/>Bạn vui lòng thử lại sau.");
                        this.Title = LangController.getLng("litForgetPasswordResponse.Error", "Lỗi xảy ra trong quá trình tạo mật khẩu mới");
                    }
                }
                else
                {
                    this.litForgetPasswordResponse.Text = LangMsgController.getLngMsg("litForgetPasswordResponse.Error", "Đã có lỗi xảy ra trong quá trình tạo mật khẩu mới.<br/>Bạn vui lòng thử lại sau.");
                    this.Title = LangController.getLng("litForgetPasswordResponse.Error", "Lỗi xảy ra trong quá trình tạo mật khẩu mới");
                }
            }
        }

        private bool sendResetPasswordEmail(string email, string randomPassword)
        {
            try
            {
                string resetPasswordEmailTemplate = TemplateController.getTemplate("EMAIL_RESETPASSWORD_TEMP", "<p><font style='font-weight:bold;color:red'>PhongCachMobile</font> - Thông tin mật khẩu mới cho tài khoản của bạn</p>" +
                        "<p><strong>Email:</strong> {0}</p>" +
                        "<p><strong>Mật khẩu mới:</strong> {1}</p>" +
                        "<p>Nội dung trên được chuyển đến từ website <font style='font-weight:bold;color:red'>PhongCachMobile</font></p>" +
                        "<p><font style='font-weight:bold'>PhongCachMobile Team</font><br/>http://phongcachmobile.com.vn/</p>");
                Common.sendEmail(txtEmail.Text, "", "", "", LangController.getLng("litResetPasswordEmail.Subject", "[PhongCachMobile] Thông tin mật khẩu mới"),
                    String.Format(resetPasswordEmailTemplate, email, randomPassword), "");

                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error sendResetPasswordEmail "));
                return false;
            }
        }
    }
}