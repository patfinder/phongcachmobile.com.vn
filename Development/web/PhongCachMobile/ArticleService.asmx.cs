﻿using Newtonsoft.Json;
using PhongCachMobile.controller;
using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PhongCachMobile
{
    /// <summary>
    /// Summary description for ArticleService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ArticleService : System.Web.Services.WebService
    {

        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetPromotions(string page = null, string pageSize = null)
        {
            return DoGetArticles("56", page, pageSize);
        }
        [WebMethod]
        public string GetMarketNews(string page = null, string pageSize = null)
        {
            return DoGetArticles("55", page, pageSize);
        }

        protected string DoGetArticles(string category, string pageIndex, string pageSize)
        {
            int iCategory = string.IsNullOrEmpty(category) ? -1 : int.Parse(category);

            int iPageIndex = string.IsNullOrEmpty(pageIndex) ? 0 : int.Parse(pageIndex);
            if (iPageIndex < 0)
                iPageIndex = 0;

            int iPageSize = string.IsNullOrEmpty(pageSize) ? 0 : int.Parse(pageSize);
            if (iPageSize <= 0)
                iPageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "10"));

            string result = "";

            int remainCount;
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(out remainCount, iCategory, iPageIndex, iPageSize);
            if (dt != null && dt.Rows.Count > 0)
            {
                List<Article> newsList = new List<Article>();

                foreach (DataRow dr in dt.Rows)
                {
                    newsList.Add(ArticleController.rowToArticle(dr));
                }

                dt = CommentController.getNumberOfCommentsByArticleIDs(newsList.Select(a => a.id).ToList());
                foreach (DataRow dr in dt.Rows)
                {
                    newsList.First(a => a.id == (int)dr["articleID"]).CommentCount = (int)dr["CommentCount"];
                }

                foreach(Article article in newsList)
                {
                    result +=
                        "    <li > " +
                        "        <h2><a href='" + article.link + "'>" + article.title + "</a></h2> " +
                        "        <span class='info-by'>" + article.postedDate + " - <strong class='color-red'>" + article.CommentCount + "</strong> bình luận - <strong class='color-red'>" + article.viewCount + "</strong> lượt xem</span> " +
                        "        <a href='" + article.link + "' class='img-box'><img src='images/news/" + article.displayImg + "' alt=''></a> " +
                        "    </li>";
                }
            }

            return JsonConvert.SerializeObject(new { remainCount = remainCount, items = result });
        }
    }
}
