﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.model;
using PhongCachMobile.controller;
using System.Data;

namespace PhongCachMobile
{
    public partial class kich_hoat : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                checkActivateAccount();
            }
        }

        private void checkActivateAccount()
        {
            if (Request.QueryString["idtoken"] != null && Request.QueryString["idtoken"].ToString() != "")
            {
                string[] idtoken = Request.QueryString["idtoken"].ToString().Split('-');
                string id = idtoken[0];
                string token = idtoken[1];
                ActivateUserResult result = SysUserController.activateUser(id, token);

                DataTable dt = SysUserController.getUserById(id);
                if (result.statusCode == ActivateUserResult.STATUS_VALID_TOKEN_CODE)
                {
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        Session["Email"] = dt.Rows[0]["Email"].ToString();
                        Session["UserId"] = dt.Rows[0]["Id"].ToString();
                        string url = "";
                        if (Session["RefUrl"] != null && Session["RefUrl"].ToString() != "")
                        {
                            if (Session["RefUrl"].ToString().Contains("thong-tin-"))
                                url = Session["RefUrl"].ToString() + "#_comment";
                            else
                                url = Session["RefUrl"].ToString();
                        }
                        else
                            url = "gia-hot";
                        string activateResponse = String.Format(LangMsgController.getLngMsg("litActivateResponse.OK", "Tài khoản <font style='font-weight:bold;color:red'><u>{0}</font></u> đã được kích hoạt thành công. " +
                            "Ấn vào <a class='color-1 link1' href='{1}'>đây</a> để tiếp tục."), dt.Rows[0]["Email"].ToString(), url);
                        this.litActivateResponse.Text = activateResponse;
                    }
                }
                else
                {
                    if (result.statusCode != ActivateUserResult.STATUS_ALREADY_ACTIVATED_CODE)
                        this.litActivateResponse.Text = String.Format(LangMsgController.getLngMsg("litActivateResponse.Error", "Có lỗi xảy ra trong quá trình kích hoạt tài khoản <font style='font-weight:bold;color:red'><u>{0}</font></u>. <br/>" +
                            "<font style='font-weight:bold'>Lỗi</font>: {1} <br/>Bạn hãy thử lại hoặc <a class='color-1 link1' href='gui-lai-ma-xac-nhan'>yêu cầu</a> hệ thống gửi lại mã xác nhận."), dt.Rows[0]["Email"].ToString(), result.statusText);
                    else
                        this.litActivateResponse.Text = String.Format(LangMsgController.getLngMsg("litAlreadyActivateResponse.Error", "Có lỗi xảy ra trong quá trình kích hoạt tài khoản <font style='font-weight:bold;color:red'><u>{0}</font></u>. <br/>" +
                            "<font style='font-weight:bold'>Lỗi</font>: {1}"), dt.Rows[0]["Email"].ToString(), result.statusText);
                }
            }
        }

        private void loadLang()
        {
            this.Title = "Kích hoạt tài khoản";
        }
    }
}