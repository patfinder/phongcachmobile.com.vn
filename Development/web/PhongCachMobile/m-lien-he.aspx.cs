﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile
{
    public partial class m_lien_he : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            this.litContact.Text = LangController.getLng("litContact.Text", "Liên hệ với PhongCachMobile");
            this.btnSubmit.Text = LangController.getLng("litSend.Text", "Gửi");
            this.reqValEmail.ErrorMessage = this.reqValMessage.ErrorMessage = this.reqValSubject.ErrorMessage =
                this.reqValSecuredCode.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
            this.rgeValEmail.ErrorMessage = LangController.getLng("litInvalidFormat.Text", "Sai định dạng");
            this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + litContact.Text;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            ccSecuredCode.ValidateCaptcha(txtCaptcha.Text);
            if (ccSecuredCode.UserValidated)
            {
                string template = TemplateController.getTemplate("EMAIL_CONTACT_TEMP", "<p>PhongCachMobile Contact</p>" +
                   "<p><strong>Email:</strong> {0}</p>" +
                   "<p><strong>Nội dung:</strong> {1}</p>" +
                   "<p>Nội dung trên được chuyển đến từ website PhongCachMobile</p>");

                ContactDataController.createContactData(new ContactData("", txtEmail.Text, txtMessage.Text, txtSubject.Text, DateTime.Now.ToString("yyyyMMddHHmmss")));

                string value = String.Format(template, txtEmail.Text, txtMessage.Text);

                string contactEmail = SysIniController.getSetting("Global_ContactEmail", "bdo@netsservices.dk");

                bool result = Common.sendEmail(contactEmail, "", "", "", "[PCMB][Liên hệ]" + txtSubject.Text, value, "");

                if (result)
                {
                    lblFrmResponse_Contact.Text = LangMsgController.getLngMsg("litContactResponse.OK", "Cảm ơn bạn đã liên hệ với chúng tôi.<br/>Ban quản trị sẽ trả lời bạn trong thời gian sớm nhất.");
                }
                else
                {
                    lblFrmResponse_Contact.Text = LangMsgController.getLngMsg("litContactResponse.Error", "Có lỗi xảy ra. Nội dung liên hệ của bạn đã được lưu vào hệ thống. Chúng tôi sẽ trả lời bạn trong thời gian sớm nhất.");
                }
                this.contact_form.Visible = false;
                this.contact_form.Attributes["style"] = "display:none;";
                this.frmResponse.Attributes["style"] = "display:block;";
                this.frmResponse.Visible = true;
            }
            else
            {
                lblCaptchaResponse_Contact.ForeColor = System.Drawing.Color.Red;
                lblCaptchaResponse_Contact.Text = LangController.getLng("litCaptchaError.Text", "Sai mã xác nhận");
            }

        }
    }
}