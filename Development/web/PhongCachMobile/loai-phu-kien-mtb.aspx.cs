﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class loai_phu_kien_mtb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadSlider();
                loadBanner();
                loadNews();
                loadSidebarBanner();
            }

            loadTitle();
        }

        private void loadSidebarBanner()
        {
            //Phone
            DataTable dt = SliderController.getSliders("36");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litPhone.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Tablet
            dt = SliderController.getSliders("37");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litTablet.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Accessory
            //dt = SliderController.getSliders("38");
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    int index = DateTime.Now.Second % dt.Rows.Count;
            //    imgAccessory.ImageUrl = "images/banner/" + dt.Rows[index]["DisplayImage"].ToString();
            //}

            //Application
            dt = SliderController.getSliders("39");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litApplication.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Game
            dt = SliderController.getSliders("40");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litGame.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }
        }


        private void loadNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "56");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.StripTagsRegex(dr["Title"].ToString()).ToLower().Replace(' ', '-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = " cách đây " + ts.Hours + " giờ.";
                    //else
                    //    dr["TimeSpan"] = " cách đây " + ts.Days + " ngày";
                }
                lstNews.DataSource = dt;
                lstNews.DataBind();
            }
        }

        private void loadTitle()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                litMobile.Text = this.Title = "Phụ kiện máy tính bảng - " + CategoryController.getCategoryNameById(id);

            }
            else
                litMobile.Text = this.Title = "Phụ kiện máy tính bảng";
        }

        private void loadBanner()
        {
            DataTable dt = SliderController.getSliders(2,"30");
            if (dt != null && dt.Rows.Count > 0)
            {
                litBanner.Text = "";
                foreach (DataRow dr in dt.Rows)
                {
                    litBanner.Text += "<li class='item'><a href='" + dr["ActionUrl"].ToString() + "'>" +
                        "<img  src='images/banner/" + dr["DisplayImage"].ToString() + "' />";
                    litBanner.Text += "<div class='contentDiv'>" + dr["Text"].ToString() + "</div>";
                    litBanner.Text += "</a></li>";
                }
            }
        }

        private void loadSlider()
        {
            litSlider.Text = "<div id='slider' class='nivoSlider' style='margin-bottom: 0; display: inline-block;'>";

            DataTable dt = SliderController.getSliders("29");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    litSlider.Text += "<a href='" + dr["ActionUrl"].ToString() + "'>" +
                        "<img src='images/slide/" + dr["DisplayImage"].ToString() + "' class='slider_image' /></a>";
                }
            }
            litSlider.Text += "</div>";
        }


        private void loadProductsByCategory(int categoryId)
        {
            DataTable dt = ProductController.getAccessoriesByCategory(categoryId);

            EnumerableRowCollection<DataRow> temp = null;

            //temp = dt.AsEnumerable().Where(row => (row.Field<bool>("isOutOfStock") == false));

            //if (temp != null && temp.Count() > 0)
            //    dt = temp.CopyToDataTable();

            DataTable dtNew = dt;


            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("UrlName", typeof(string));
                dtNew.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-');

                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                        long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }
            }

            lstItem.DataSource = dtNew;
            lstItem.DataBind();

        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadProductsByCategory(int.Parse(id));
            }
            else
            {
                loadProductsByCategory();
            }

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
                " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";

        }


        private void loadProductsByCategory()
        {
            DataTable dt = ProductController.getAccessoriesByGroupId(8);
            EnumerableRowCollection<DataRow> temp = null;
            //temp = dt.AsEnumerable().Where(row => (row.Field<bool>("isOutOfStock") == false));

            //if (temp != null && temp.Count() > 0)
            //    dt = temp.CopyToDataTable();

            DataTable dtNew = dt;

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("UrlName", typeof(string));
                dtNew.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-');

                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                        long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }
                lstItem.DataSource = dtNew;
                lstItem.DataBind();

            }
        }

        protected void ddlTopDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);
            //dpgLstItemTop.StartRowIndex =dpgLstItemBot. = 0;
            ddlBotDisplay.SelectedIndex = ddlTopDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadProductsByCategory(int.Parse(id));
            }
            else
            {
                loadProductsByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
               " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlBotDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);
            ddlTopDisplay.SelectedIndex = ddlBotDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];
                loadProductsByCategory(int.Parse(id));

            }
            else
            {
                loadProductsByCategory();

            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm thứ " + (dpgLstItemTop.StartRowIndex + 1) +
                " đến " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

    }
}