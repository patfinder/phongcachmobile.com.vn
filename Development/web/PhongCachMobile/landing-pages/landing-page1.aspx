﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="landing-page1.aspx.cs" Inherits="PhongCachMobile.landing_pages.landing_page1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>HTC One mini 2 Thông số kỹ thuật và đánh giá | HTC Việt Nam</title>
    <meta charset="UTF-8">
    <meta content="#" name="description">
    <meta content="vi" http-equiv="content-language">
    <meta content="#" property="og:title">
    <meta content="#"property="og:description">
    <meta content="width=device-w idth, initial-scale=1" name="viewport">
    <meta content="HTC One mini 2" name="product_name">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!<![endif]-->
    <!--[if IE]><! -->
    <script type="text/javascript">
        /*@cc_on
         @if (@_jscript_version == 10)
         document.documentElement.className += ' ie10';
         @elif (@_jscript_version == 9)
         document.documentElement.className += ' ie9';
         @elif (@_jscript_version == 5.8)
         document.documentElement.className += ' ie8';
         @else
         document.documentElement.className += ' lt-ie8';
         @end
         @*/
    </script>
    <!--<![endif] -->
    <script type="text/javascript">


        var regionType = '#';

    </script>
    <link href="css/productdetail.css" type="text/css" rel="stylesheet">
    <link href="css/htc.chrome.ltr.css" type="text/css" rel="stylesheet">
    <style>
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#htc-one .content .intro .logo {
            background-image: url('images/htc-one-mini-2-logo.png');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#htc-one {
            background-image: url('images/slide-hero-1.jpg');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#design .panel#design-one {
            background-image: url('images/slide-design-1-2.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-design-1-2.jpg', sizingMethod='scale');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#design .panel#design-two {
            background-image: url('images/slide-design-2-bg.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-design-2-bg.jpg', sizingMethod='scale');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#camera {
            background-image: url('images/slide-camera-1.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-camera-1.jpg', sizingMethod='scale');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#sound {
            background-image: url('images/slide-sound-1.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-sound-1.jpg', sizingMethod='scale');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#apps #apps-one {
            background-image: url('images/slide-apps-1.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-apps-1.jpg', sizingMethod='scale');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#apps #apps-two {
            background-image: url('images/slide-apps-2.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-apps-2.jpg', sizingMethod='scale');
        }
        body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#apps #apps-three {
            background-image: url('images/slide-apps-3.jpg');
            filter: progid: DXImageTransform.Microsoft.AlphaImageLoader(src='images/slide-apps-3.jpg', sizingMethod='scale');
        }
        @media only screen and (max-width: 720px) {
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#htc-one {
                background-image: url('images/slide-hero-1-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#design .panel#design-one {
                background-image: url('images/slide-design-1-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#design .panel#design-two {
                background-image: url('images/slide-design-2-bg-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#camera {
                background-image: url('images/slide-camera-1-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#sound {
                background-image: url('images/slide-sound-1-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#apps #apps-one {
                background-image: url('images/slide-apps-1-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#apps #apps-two {
                background-image: url('images/slide-apps-2-mobile.jpg');
            }
            body.productdetail.template_m8.template_m8_mini .main .slide-wrapper section#apps #apps-three {
                background-image: url('images/slide-apps-3-mobile.jpg');
            }
        }
    </style>
    <script src="js/jquery-2.1.4.min.js" class="chrome-plugin-js"></script>
    <script src="js/modernizr-2.8.3.min.js" class="chrome-plugin-js"></script>
    <script src="js/device.js" class="chrome-plugin-js"></script>
    <script src="js/jquery.validate.min.js" class="chrome-plugin-js"></script>
    <script src="js/hoverIntent-r7.min.js" class="chrome-plugin-js"></script>
    <script src="js/jquery.placeholder.min.js" class="chrome-plugin-js"></script>
    <script src="js/xregexp-all-min.js" class="chrome-plugin-js"></script>
    <script src="js/velocity-1.2.2.js"></script>
    <script src="js/velocity-ui-5.0.4.js"></script>
    <script src="js/bxslider-4.1.2.js"></script>
    <script src="js/watch-2.0.min.js"></script>
    <script src="js/all.min.js"></script>
    <script src="js/htc.chrome.all.min.js" type="text/javascript" id="chrome-js"></script>
    <script src="js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="js/touchwipe.js"></script>
    <script src="js/www-widgetapi.js"></script>
    <!-- Adobe Marketing Cloud Tag Management code Copyright 1996-2013 Adobe, Inc. All Rights Reserved More info available at http://www.adobe.com -->
    <script src="js/satellite-54c15d5a6166620015030900.js"></script>
    <script src="js/s-code-contents-9141f308cbb207913a2dfc8ff0c09b2547a80da0.js"></script>
    <script src="js/mbox-contents-92772e6e78b8ab7a53db7e0a77986c6d160fd114.js"></script>

    <script src="js/target.js"></script>
    <script src="js/standard" language="JavaScript"></script>
    <style>
        body.productdetail.htc-one-mini-2 .main .slide-wrapper section#htc-one .content .intro .buy-button {
            display: none;
        }
    </style>

</head>
<body  class="vn productdetail htc-one-mini-2 template_m8_mini template_m8 ltr">
<div class="mboxDefault" id="mbox-target-global-mbox-1435642235885-392752" style="visibility: visible; display: block;"></div>
<div class="htc-chrome" id="htc-site">
    <div class="htc-header" id="htc-header">
        <header>
            <div class="header-wrapper">
                <div class="d-header">
                    <div class="logo-block">
                        <a href="#"></a>
                    </div>
                    <!--
                    <div class="top-navi-block">
                        <ul class="top-navi-wrapper">
                            <li class="top-item signup"><a rel="follow" target="_self" href="##" class="top-title">ĐĂNG KÝ</a>
                                <div class="popover signup-popover">
                                    <div class="signup-wrapper">
                                        <div class="form-block">
                                            <div class="title">NHẬN THÔNG TIN MỚI NHẤT</div>
                                            <div class="context">Ưu đãi, thông tin và mọi vấn đề bạn quan tâm về HTC.</div>
                                            <form action="" method="get" id="HtcSignup" novalidate="novalidate">
                                                <input data-msg-specialchars="Vui lòng nhập địa chỉ email hợp lệ" data-msg-required="Vui lòng nhập địa chỉ email hợp lệ" data-rule-specialchars="true" data-rule-required="true" aria-label="Địa chỉ email"
                                                       placeholder="Địa chỉ email" type="text" maxlength="100" class="input-text" id="emailtext" name="emailtext" aria-required="true">
                                                <div class="question-wrapper">
                                                    <div class="context">Bạn đã từng sở hữu một chiếc HTC chưa?</div>
                                                    <div class="YN-wrapper">
                                                        <div class="hidden-area">
                                                            <input data-msg-required="Thông tin bắt buộc" data-rule-required="true" name="ownedHtc" id="ownedHtc" type="radio" aria-required="true">
                                                        </div>
                                                        <input value="Có" aria-label="Có" class="btnYN Y" name="yes" type="button">
                                                        <input value="Không" aria-label="Không" class="btnYN N" name="no" type="button">
                                                    </div>
                                                </div>
                                                <div class="btn-yn-error error"></div>
                                                <div class="checker-wrapper">
                                                    <div class="checker">
                                                        <input aria-label="Có" name="subscriptchecker" id="subscriptchecker" value="y" type="checkbox">
                                                        <label for="subscriptchecker"></label>
                                                    </div>
                                                    <div class="context">Email cho tôi về các ưu đãi chọn lọc từ đối tác và ưu đãi đặc biệt của HTC, thông tin sản phẩm và bản tin.</div>
                                                </div>
                                                <div class="privacy-wrapper">
                                                    <div class="context">
                                                        Để xem chính sách bảo mật của chúng tôi, vui lòng truy cập <a href="http://www.htc.com/vn/terms/privacy/" target="_blank">nhấp vào đây</a>.

                                                    </div>
                                                </div>
                                                <div class="submit-wrapper">
                                                    <input aria-label="Gửi" value="Gửi" class="poper-submit" name="signup-submit" type="submit">
                                                </div>
                                            </form>
                                        </div>
                                        <div class="thank-you-wrapper">
                                            <div class="signup-closer"></div>
                                            <div class="envolope-wrapper">
                                                <div class="image-holder-email"></div>
                                                <div class="title">Cảm ơn bạn!</div>
                                            </div>
                                            <div class="context">Chúng tôi đã nhận được thông tin của bạn và sẽ sớm gửi cập nhật về HTC cho bạn.</div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div> -->
                    <div class="main-navi-block">
                        <ul class="menu-list">
                            <!--<li class="has-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="#">SẢN PHẨM</a>
                                </div>
                                <div class="sub-menu">
                                    <div class="column col-1 permanent-link">
                                        <div class="align-center"><a rel="" target="_self" href="#">__ SẢN PHẨM
                                        </a><a rel="" target="_self" href="#">__ RE CAMERA
                                        </a>
                                        </div>
                                    </div>
                                    <div class="column col-1 standard-product">
                                        <div class="align-center">
                                            <a href="#">
                                                <img src="images/htc-one-m9-global-2v-listing.png">
                                                <div class="product-name">
                                                    HTC One M9
                                                </div>
                                                <div class="product-desc">Hãy tận hưởng từng phút giây của cuộc sống</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="column col-1 standard-product">
                                        <div class="align-center">
                                            <a href="#">
                                                <img src="images/htc-desire-eye-global-2v-listing-white-red.png">
                                                <div class="product-name">
                                                    HTC Desire EYE
                                                </div>
                                                <div class="product-desc">Thay đổi quan điểm của bạn</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="column col-2 product-story">
                                        <a href="#">
                                            <div class="align-center">
                                                <img src="images/recamera-story.png">
                                            </div>
                                            <div class="product-name">
                                                Máy ảnh RE
                                            </div>
                                            <div class="product-desc">
                                                Chiếc máy ảnh nhỏ xinh đặc biệt
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="#">PHỤ KIỆN</a>
                                </div>
                            </li>
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="#">PHẦN MỀM +ỨNG DỤNG</a>
                                </div>
                            </li>
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="#">HỖ TRỢ</a>
                                </div>
                            </li>-->
                            <li class="no-sub-menu">
                                <div class="title"><a rel="follow" target="_self" href="../">HOME</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <!--<div class="search-block">
                        <div class="align-center">
                            <form class="htc-search-form d-search-form" method="get" name="search" action="#" id="HtcSearchForm">
                                <input class="submit" type="submit" name="submit" value="">
                                <input placeholder="Tìm kiếm" aria-label="Tìm kiếm" class="query" type="text" maxlength="100" name="q">
                            </form>
                        </div>
                    </div>-->
                </div>
                <div class="m-header">
                    <div class="logo-block">
                        <a href="http://www.htc.com/vn/"></a>
                    </div>
                    <div class="main-navi-block">
                        <ul class="menu-list">
                            <li class="has-sub-menu product-tab">
                                <!--<div class="title">SẢN PHẨM</div>
                                <div class="sub-menu"><a rel="follow" target="_self" href="#" class="sub-item">SẢN PHẨM</a><a rel="follow" target="_self" href="http://www.htc.com/vn/re/re-camera/" class="sub-item">RE CAMERA</a>
                                </div> -->
                            </li>
                            <li class="has-sub-menu has-icon search-tab">
                                 <!--<div class="title"></div>
                                <div class="sub-menu">
                                    <div class="align-center">
                                        <form class="htc-search-form m-search-form" method="get" name="search" action="#">
                                            <input class="submit" type="submit" name="submit" value="">
                                            <input placeholder="Tìm kiếm" class="query" type="text" maxlength="100" name="q">
                                        </form>
                                    </div>
                                </div>-->
                            </li>
                            <li class="has-sub-menu has-icon menu-tab">
                                <div class="title"></div>
                                <div class="sub-menu">
                                    <!-- header menu -->
                                    <!--<div class="sub-item"><a rel="follow" target="_self" href="#"> PHỤ KIỆN</a>
                                    </div>
                                    <div class="sub-item"><a rel="follow" target="_self" href="#"> PHẦN MỀM +ỨNG DỤNG</a>
                                    </div>
                                    <div class="sub-item"><a rel="follow" target="_self" href="#"> HỖ TRỢ</a>
                                    </div> -->
                                    <div class="sub-item"><a rel="follow" target="_self" href="../">HOME</a>
                                                                        </div>
                                    <!--   Top navigation link -->
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <section class="htc-main" id="htc-main">
        <div itemtype="http://schema.org/Product" itemscope="itemscope" class="main">
            <img style="display:none;" itemprop="image" src="images/htc-one-mini-2-en-angled-listing.png">
            <div class="intro-overlay" style="display: none;">
                <div class="loader-animation-container">
                    <div class="icon-animation-container">
                        <ul>
                            <li style="display: list-item;">
                                <img src="images/phone.png">
                            </li>
                            <li style="display: none;">
                                <img src="images/cam.png">
                            </li>
                            <li style="display: none;">
                                <img src="images/note.png">
                            </li>
                            <li style="display: none;">
                                <img src="images/blink.png">
                            </li>
                            <li style="display: none;">
                                <img src="images/chat.png">
                            </li>
                            <li style="display: none;">
                                <img src="images/grid.png">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="mobile-detector"></div>
            <div class="hash-detector"></div>
            <!-- navigation and more -->
            <div class="extra-info nomore">
                <div class="specs" style="width: 100%;"><span>Thông số kỹ thuật</span>
                </div>
            </div>
            <!-- end of navigation and more -->
            <nav class="slide-nav nomore">
                <ul class="spaced">
                    <li data-hash="htc-one" data-slide="1" class="active" id="nav-phone">
                        <div class="wrapper">
                            <div class="circle"></div>
                            <div class="label">HTC One mini 2</div>
                        </div>
                    </li>
                    <li data-hash="design" data-slide="2" id="nav-design" class="">
                        <div class="wrapper">
                            <div class="circle"></div>
                            <div class="label">Thiết kế</div>
                        </div>
                    </li>
                    <li data-hash="camera" data-slide="3" id="nav-camera">
                        <div class="wrapper">
                            <div class="circle"></div>
                            <div class="label">Máy ảnh</div>
                        </div>
                    </li>
                    <li data-hash="sound" data-slide="4" id="nav-sound">
                        <div class="wrapper">
                            <div class="circle"></div>
                            <div class="label">Âm thanh</div>
                        </div>
                    </li>
                    <li data-hash="apps" data-slide="5" id="nav-apps">
                        <div class="wrapper">
                            <div class="circle"></div>
                            <div class="label">Các ứng dụng</div>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="sound-toggle">
                <!-- input type range for volume slider -->
            </div>
            <div class="scroll-prompt">
                <div class="label">Cuộn xuống</div>
                <div class="arrow"></div>
            </div>
            <div class="slide-container">
                <div class="slide-wrapper">
                    <section class="slide" id="htc-one">
                        <div data-position="" class="header-button"><span>Hiện đầu trang</span>
                        </div><a href="#" class="support-icon">
                        CẦN TRỢ GIÚP?
                    </a>
                        <video class="no-scale" id="video-hero" style="display: none;">
                            <source type="video/mp4" src="images/Mini-Hero.mp4">
                            <source type="video/webm" src="images/mini-hero.webm">
                            <img src="images/slide-hero-1.jpg" class="video-fallback no-scale">
                        </video>
                        <div class="content anchored">
                            <div class="hotspot-container">
                                <div class="intro">
                                    <div class="lead-in"></div>
                                    <div class="logo"></div><a class="buy-button jump-to-section" href="#" data-section="buy">MUA NGAY</a>
                                </div>
                                <div data-id="mini-storage" class="hotspot" id="mini-storage-hotspot"></div>
                                <div data-id="mini-storage" class="hotspot-caption" id="mini-storage-caption">
                                    <div class="connector one"></div>
                                    <div class="copy">
                                        <h4>DUNG LƯỢNG LƯU TRỮ LỚN</h4>
                                        <p>Bộ nhớ trong 16GB với khe cắm thẻ SD mở rộng lên tới 128GB.</p>
                                    </div>
                                </div>
                                <div data-id="mini-camera" class="hotspot" id="mini-camera-hotspot"></div>
                                <div data-id="mini-camera" class="hotspot-caption" id="mini-camera-caption">
                                    <div class="connector one"></div>
                                    <div class="copy">
                                        <h4>TỰ CHỤP CHÂN DUNG VỚI CHẤT LƯỢNG TUYỆT VỜI </h4>
                                        <p>Máy ảnh trước 5MP với phần mềm Touch Up tự động giúp hoàn thiện tông màu cho da và đồng hồ đếm ngược ngay trên màn hình hiển thị mang đến cho bạn những bức ảnh “tự sướng” đẹp đến bất ngờ.</p>
                                    </div>
                                </div>
                                <div data-id="mini-power" class="hotspot" id="mini-power-hotspot"></div>
                                <div data-id="mini-power" class="hotspot-caption" id="mini-power-caption">
                                    <div class="connector one"></div>
                                    <div class="copy">
                                        <h4>HIỆU SUẤT XỬ LÝ MẠNH MẼ</h4>
                                        <p>Tích hợp bộ vi xử lý Qualcomm Snapdragon 400 mạnh mẽ giúp mang lại: hiệu suất xử lý cực nhanh, chơi game, xem phim mượt mà và quản lý năng lượng sử dụng pin hiệu quả, giúp kéo dài thời lượng sử dụng pin.</p>
                                    </div>
                                </div>
                                <div data-id="mini-speakers" class="hotspot" id="mini-speakers-hotspot"></div>
                                <div data-id="mini-speakers" class="hotspot-caption" id="mini-speakers-caption">
                                    <div class="connector one"></div>
                                    <div class="copy">
                                        <h4>ÂM THANH TUYỆT VỜI ĐẾN KINH NGẠC</h4>
                                        <p>HTC BoomSound™: loa stereo kép mặt trước, bộ khuếch đại tích hợp và phần mềm tăng hiệu ứng âm nhạc.</p>
                                    </div>
                                </div>
                            </div>
                            <div style="display:none;" class="intro">
                                <div class="lead-in"></div>
                                <div class="logo"></div><a href="#" class="buy-button">Đăng ký</a>
                            </div>
                        </div>
                    </section>
                    <!-- section#htc-one -->
                    <section class="slide" id="design">
                        <div data-position="1" class="panel first" id="design-one">
                            <div class="overlay"></div>
                            <div class="content expand anchored">
                                <div class="description-block">
                                    <h3>NHỎ GỌN, HOÀN HẢO </h3>
                                    <p>Giới thiệu một thiết bị Android nhỏ gọn hoàn hảo nhất từ trước đến nay. Kích thước màn hình 4,5’’ nhỏ gọn hơn, lưng máy cong và các cạnh thon gọn để bạn có thể sử dụng bằng một tay thật dễ dàng. Giúp bạn kết nối
                                        với cả thế giới nhờ những thông tin cập nhật tuỳ chọn ngay trên màn hình chính và với cả thế giới âm nhạc cùng loa stereo kép mặt trước. Máy ảnh trước 5MP với phần mềm Touch Up tự động cho những bức ảnh chân
                                        dung tự chụp tuyệt vời và bộ nhớ mở rộng lên đến 128GB cho bạn khả năng lưu trữ tất cả những bức ảnh của mình.</p>
                                </div>
                            </div>
                        </div>
                        <div data-position="2" class="panel second collapse" id="design-two">
                            <div class="overlay"></div>
                            <div class="mobile-webgl">
                                <div class="close"></div>
                            </div>
                            <div class="content expand anchored">
                                <div class="header-wrapper">
                                    <h2>DÁNG THON GỌN ĐẸP HOÀN HẢO</h2>
                                    <!--<a class="call-to-action after-title" href="design/">TÌM HIỂU THÊM</a>-->
                                </div>
                                <ul class="color-tabs">
                                    <li data-color="gray" class="gray active">GUNMETAL GRAY</li>
                                    <li data-color="silver" class="silver ">GLACIAL SILVER</li>
                                    <li data-color="gold" class="gold ">AMBER GOLD</li>
                                </ul>
                            </div>
                            <div class="content alt anchored">
                                <div class="spacer"></div>
                                <div style="background-image:url(images/gray-us.png);" data-color="gray" data-section="design-two" class="phone-model jump-scroll gray active">
                                    <div class="glass"></div>
                                </div>
                                <div style="background-image:url(images/silver-us.png);" data-color="silver" data-section="design-two" class="phone-model jump-scroll silver "></div>
                                <div style="background-image:url(images/gold-us.png);" data-color="gold" data-section="design-two" class="phone-model jump-scroll gold "></div>
                                <div class="maximize">
                                    <div class="corner top-left"></div>
                                    <div class="corner top-right"></div>
                                    <div class="corner bottom-left"></div>
                                    <div class="corner bottom-right"></div>
                                </div>
                                <div data-section="design-two" class="experience-label jump-scroll">Khám phá
                                    <br>
                                    <span>TƯƠNG TÁC TRỰC TIẾP VỚI SẢN PHẨM</span>
                                </div>
                            </div>
                        </div>
                        <div data-position="3" class="panel third collapse" id="design-three">
                            <div class="overlay"></div>
                            <div class="image-area">
                            <img data-position="1" src="images/device1.jpg" class="desktop">
                            <img data-position="2" src="images/device2.jpg" class="desktop">
                            <img data-position="3" src="images/device3.jpg" class="desktop">
                            <img data-position="4" src="images/device4.jpg" class="desktop">
                            <img data-position="5" src="images/device5.jpg" class="desktop">
                            <img data-position="6" src="images/device6.jpg" class="desktop">
                            </div>
                            <div class="content expand anchored">
                                <div class="header-wrapper">
                                    <h2>HOÀN HẢO Ở MỌI GÓC ĐỘ</h2>
                                    <!--<a class="call-to-action after-title" href="design/">LEARN MORE</a>-->
                                </div>
                                <div class="filmstrip-wrapper">
                                    <ul class="filmstrip">
                                        <li data-position="1" class="active">
                                            <div class="cover"></div>
                                            <img src="images/mini_device_thumb1.jpg">
                                        </li>
                                        <li data-position="2">
                                            <div class="cover"></div>
                                            <img src="images/mini_device_thumb2.jpg">
                                        </li>
                                        <li data-position="3">
                                            <div class="cover"></div>
                                            <img src="images/mini_device_thumb3.jpg">
                                        </li>
                                        <li data-position="4">
                                            <div class="cover"></div>
                                            <img src="images/mini_device_thumb4.jpg">
                                        </li>
                                        <li data-position="5">
                                            <div class="cover"></div>
                                            <img src="images/mini_device_thumb5.jpg">
                                        </li>
                                        <li data-position="6">
                                            <div class="cover"></div>
                                            <img src="images/mini_device_thumb6.jpg">
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- section#design -->
                    <section class="slide" id="camera">
                        <div class="content expand anchored">
                            <div class="description-block">
                                <h3>TỰ CHỤP CHÂN DUNG CỰC THÍCH</h3>
                                <p>Máy ảnh trước 5MP với phần mềm Touch Up tự động giúp hoàn thiện độ sáng và tông màu cho da. Đồng hồ đếm ngược nằm ngay trên màn hình hiển thị và thiết kế vừa tay cho bạn chụp những bức ảnh "tự sướng" tuyệt đẹp, mọi
                                    lúc mọi nơi.</p>
                            </div>
                        </div>
                    </section>
                    <!-- section#camera -->
                    <section class="slide" id="sound">
                        <video data-section="4" muted="muted" loop="loop" class="scale">
                            <source type="video/mp4" src="images/Mini-Boomsound.mp4">
                            <source type="video/webm" src="images/Mini-Boomsound.webm">
                            <img src="images/slide-sound-1.jpg" class="video-fallback scale">
                        </video>
                        <div class="content expand anchored">
                            <div class="description-block">
                                <h3>ÂM THANH CỰC KHỦNG </h3>
                                <p>HTC BoomSound™ ấn tượng nhất với: loa stereo nhỏ đến khó tin nhưng mang lại âm thanh đặc sắc, sống động đáng kinh ngạc.</p>
                                <p>Nhờ có bộ khuếch đại tích hợp, phần mềm giúp cân bằng âm trầm và âm bổng cùng với các ngăn loa ngoại cỡ được thiết kế kiểu xếp chồng lên nhau mà loa stereo kép mặt trước có thể tăng âm lượng đáng kể. Tận hưởng âm nhạc
                                    và âm thanh êm dịu tuyệt vời.</p>
                            </div>
                        </div>
                    </section>
                    <!-- section#sound -->
                    <section class="slide" id="apps">
                        <div data-position="1" class="panel first" id="apps-one">
                            <div class="overlay"></div>
                            <div class="content expand anchored">
                                <div class="description-block">
                                    <h3>TRUY CẬP MỌI NGUỒN THÔNG TIN NGAY TRÊN ĐẦU NGÓN TAY </h3>
                                    <p>Với HTC BlinkFeed™, chỉ cần rê nhẹ ngón tay là bạn đã truy cập được vào các nội dung trực tuyến mà mình yêu thích.</p>
                                    <p>Từ các trạng thái trên mạng xã hội đến các tin nóng hổi và nhiều thông tin hữu ích khác, toàn bộ đều được cập nhật liên tục với HTC BlinkFeed™ giúp bạn luôn kết nối với thế giới bên ngoài.</p>
                                </div>
                            </div>
                        </div>
                        <div data-position="2" class="panel second collapse" id="apps-two">
                            <div class="overlay"></div>
                            <div class="content expand anchored">
                                <div class="description-block">
                                    <h3>BLINKFEED: CẢ THẾ GIỚI HIỆN RA CHỈ TRONG CHỚP MẮT</h3>
                                    <p>HTC BlinkFeed™ cập nhật mới nội dung suốt cả ngày, cung cấp cho bạn tất cả thông tin mới nóng hổi nhất. Với hàng ngàn đối tác cung cấp nội dung và thêm nhiều nội dung hấp dẫn nữa được thêm vào mỗi tháng, chắc chắn
                                        bạn sẽ tìm được những trang web và nguồn thông tin mà bạn yêu thích để tuỳ chọn hiển thị lên màn hình BlinkFeed của riêng mình. Luôn cập nhật thông tin nóng hổi và các trạng thái từ mạng xã hội chỉ trong chớp
                                        mắt.</p>
                                </div>
                            </div>
                        </div>
                        <div data-position="3" class="panel third collapse" id="apps-three">
                            <div class="overlay"></div>
                            <div class="content anchored">
                                <div>
                                    <h2>CÁC ỨNG DỤNG CỦA HTC</h2>

                                </div>
                                <ul class="apps-list">
                                    <li>
                                        <div class="icon blinkfeed"></div>
                                        <div class="label">HTC BLINKFEED™</div>
                                        <p>Cập nhật ngay trên màn hình chính. Bài viết yêu thích, tin tức, thể thao và nhiều thứ khác.</p>
                                    </li>
                                    <li>
                                        <div class="icon calendar"></div>
                                        <div class="label">LỊCH</div>
                                        <p>Lịch biểu hàng ngày, các cuộc hẹn, cuộc họp, lời nhắc và nhiều thứ khác.</p>

                                    </li>
                                </ul>
                                <h2 class="second">HỆ ĐIỀU HÀNH ANDROID TIỆN DỤNG </h2>
                                <ul class="apps-list alt">
                                    <li>
                                        <div class="icon maps"></div>
                                        <div class="label">GOOGLE MAPS</div>
                                    </li>
                                    <li>
                                        <div class="icon now"></div>
                                        <div class="label">GOOGLE NOW</div>
                                    </li>
                                    <li>
                                        <div class="icon voice"></div>
                                        <div class="label">ĐIỀU KHIỂN BẰNG GIỌNG NÓI</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <!-- section#apps -->
                    <section class="slide" id="buy">
                        <div class="content anchored">
                            <div class="mobile-section top">
                                <div class="background-cover top"></div>
                                <div class="mobile-section-inner"></div>
                            </div>
                            <div class="mobile-section bottom">
                                <div class="background-cover bottom"></div>
                                <div class="mobile-section-inner">
                                    <div class="bottom-info">
                                        <div class="accessories-area">
                                            <h5>chính hãng HTC</h5>
                                            <div class="accessories-list">
                                                <div class="accessories-scroll">
                                                    <div data-movement="1" class="accessories-container">
                                                        <a target="_blank" href="#">
                                                            <img alt="HTC Mini+" src="images/htc-mini-plus-angled-listing.png"><span>HTC Mini+</span>
                                                        </a>
                                                        <a target="_blank" href="#">
                                                            <img alt="HTC Media Link HD" src="images/htc-media-link-hd-angled-listing.png"><span>HTC Media Link HD</span>
                                                        </a>
                                                        <a target="_blank" href="#">
                                                            <img alt="Translucent Case" src="images/htc-accessory-c942-m8-angled-listing.png"><span>Translucent Case</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="accessories-nav" style="display: none;"></div>
                                            </div>
                                        </div>
                                        <div class="reviews-area ">
                                            <h5>Đánh giá</h5>
                                            <div class="meta" id="starWidth">16</div>
                                            <div class="meta" id="starGapWidth">3</div>
                                            <div class="reviews-list">
                                                <div class="stars-insert">
                                                    <!-- contents will be inserted into each rating category via JS -->
                                                    <div class="green-bar"></div>
                                                    <div class="star first"></div>
                                                    <div class="star second"></div>
                                                    <div class="star third"></div>
                                                    <div class="star fourth"></div>
                                                    <div class="star fifth"></div>
                                                </div>
                                                <div class="ratings-stars">
                                                    <div class="rating-property"><span>Máy ảnh</span>
                                                        <div class="stars">
                                                            <div class="score">4.4074</div>
                                                            <!-- contents will be inserted into each rating category via JS -->
                                                            <div class="green-bar" style="width: 83px;"></div>
                                                            <div class="star first"></div>
                                                            <div class="star second"></div>
                                                            <div class="star third"></div>
                                                            <div class="star fourth"></div>
                                                            <div class="star fifth"></div>
                                                        </div>
                                                    </div>
                                                    <div class="rating-property"><span>Chất lượng âm thanh</span>
                                                        <div class="stars">
                                                            <div class="score">4.7778</div>
                                                            <!-- contents will be inserted into each rating category via JS -->
                                                            <div class="green-bar" style="width: 88px;"></div>
                                                            <div class="star first"></div>
                                                            <div class="star second"></div>
                                                            <div class="star third"></div>
                                                            <div class="star fourth"></div>
                                                            <div class="star fifth"></div>
                                                        </div>
                                                    </div>
                                                    <div class="rating-property"><span>Màn hình</span>
                                                        <div class="stars">
                                                            <div class="score">4.5185</div>
                                                            <!-- contents will be inserted into each rating category via JS -->
                                                            <div class="green-bar" style="width: 84px;"></div>
                                                            <div class="star first"></div>
                                                            <div class="star second"></div>
                                                            <div class="star third"></div>
                                                            <div class="star fourth"></div>
                                                            <div class="star fifth"></div>
                                                        </div>
                                                    </div>
                                                    <div class="rating-property"><span>Dễ sử dụng</span>
                                                        <div class="stars">
                                                            <div class="score">4.1481</div>
                                                            <!-- contents will be inserted into each rating category via JS -->
                                                            <div class="green-bar" style="width: 78px;"></div>
                                                            <div class="star first"></div>
                                                            <div class="star second"></div>
                                                            <div class="star third"></div>
                                                            <div class="star fourth"></div>
                                                            <div class="star fifth"></div>
                                                        </div>
                                                    </div>
                                                    <div class="rating-property"><span>Thời gian dùng pin</span>
                                                        <div class="stars">
                                                            <div class="score">4.1481</div>
                                                            <!-- contents will be inserted into each rating category via JS -->
                                                            <div class="green-bar" style="width: 78px;"></div>
                                                            <div class="star first"></div>
                                                            <div class="star second"></div>
                                                            <div class="star third"></div>
                                                            <div class="star fourth"></div>
                                                            <div class="star fifth"></div>
                                                        </div>
                                                    </div>
                                                    <div class="rating-property"><span>Chiêm ngưỡng &amp; Cảm nhận</span>
                                                        <div class="stars">
                                                            <div class="score">4.6842</div>
                                                            <!-- contents will be inserted into each rating category via JS -->
                                                            <div class="green-bar" style="width: 87px;"></div>
                                                            <div class="star first"></div>
                                                            <div class="star second"></div>
                                                            <div class="star third"></div>
                                                            <div class="star fourth"></div>
                                                            <div class="star fifth"></div>
                                                        </div>
                                                    </div>
                                                    <div class="overall-rating">Tổng thể <span class="average">4.1</span> / 5</div>
                                                </div>
                                                <div class="reviews-info">
                                                    <div class="label">88 % NGƯỜI DÙNG <b>GIỚI THIỆU</b>
                                                    </div><a href="#" class="more-reviews">[ Đọc đánh giá ]</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-position="" class="footer-button" style="display: none;"><span>Hiện chân trang</span>
                        </div>
                    </section>
                    <!-- section#buy -->
                </div>
                <!-- .slide-wrapper -->
            </div>
            <!-- .slide-container -->
            <div class="model-container">
                <div class="help-container">
                    <div class="tip left-button">Xoay</div>
                    <div class="tip right-button">Quét</div>
                    <div class="tip middle-button">Thu phóng</div>
                </div>
                <div class="white-overlay"></div>
                <div class="close"></div>
                <div class="help">?</div>
                <ul class="color-options">
                    <li data-color="gray" data-model="375ae80dc7e34b229cc9739b1ce92e2b" class="gray active"></li>
                    <li data-color="silver" data-model="1b863d2813a149d2a339e6f11a039c2e" class="silver "></li>
                    <li data-color="gold" data-model="8c5579e0ccf7439bba4add8b18d04388" class="gold "></li>
                </ul>
            </div>
            <div class="chrome-overlay"></div>
            <div class="specs-spacer top"></div>
            <div class="specs-spacer bottom"></div>
            <div class="specs-container">
                <div class="content">
                    <div class="columns">
                        <div class="close-specs"></div>
                        <div class="column one">
                            <ul>
                                <li class="">
                                    <h4>Kích thước<sup></sup></h4><span><p>137,43 x 65,04 x 10,6 mm</p></span>
                                </li>
                                <li class="">
                                    <h4>Tốc độ chip xử lý<sup></sup></h4><span><p>Bộ vi xử lý lõi tứ 1,2 GHz, Qualcomm® Snapdragon™ 400</p></span>
                                </li>
                                <li class="hide">
                                    <h4>Mạng<sup>1</sup></h4><span><p>2G/2,5G - GSM/GPRS/EDGE:</p>
                                    <ul>
                                        <li>850/900/1800/1900 MHz</li>
                                    </ul>
                                        <p>3G - WCDMA:</p>
                                    <ul>
                                        <li>850/900/1900/2100 MHz với HSPA+ lên đến 42 Mbps</li>
                                    </ul>
                                        <p>4G - LTE:</p>
                                    <ul>
                                        <li>EMEA: Dải băng tần 3,7,8,20</li>
                                        <li>CHÂU Á: Dải băng tần 1,3,7,8,28</li>
                                    </ul></span>
                                </li>
                                <li class="hide">
                                    <h4>Cảm biến<sup></sup></h4><span><p>Cảm biến ánh sáng</p>
                                    <p>Cảm biến không gian gần</p>
                                    <p>Gia tốc kế</p></span>
                                </li>
                                <li class="hide">
                                    <h4>Máy ảnh<sup></sup></h4><span><ul>
                                    <li>Máy ảnh Chính: 13MP, cảm biến BSI, f/2.2, quay phim 1080p</li>
                                    <li>Máy ảnh phụ: 5MP, cảm biến BSI, quay phim 1080p</li>
                                </ul></span>
                                </li>
                                <li class="hide">
                                    <h4>Bộ sạc pin AC<sup></sup></h4>
                                    <span><p>Khoảng điện áp/tần số: 100 ~ 240V AC, 50/60 Hz</p>
                                        <p>Đầu ra dòng DC: 5 V / 1A</p></span>
                                </li>
                            </ul>
                        </div>
                        <div class="column two">
                            <ul>
                                <li class="">
                                    <h4>Trọng lượng<sup></sup></h4><span><p>137 g</p></span>
                                </li>
                                <li class="">
                                    <h4>Hệ điều hành Android<sup></sup></h4><span><p>Android™ với HTC Sense™</p>
                                    <p>HTC BlinkFeed™</p></span>
                                </li>
                                <li class="hide">
                                    <h4>Bộ nhớ<sup>2</sup></h4><span><p>Tổng dung lượng bộ nhớ:  16GB</p>
                                    <p>RAM: 1GB</p>
                                    <p>Hỗ trợ khe cắm thẻ nhớ mở rộng microSD™ lên đến 128GB (không bao gồm thẻ trong hộp máy)</p></span>
                                </li>
                                <li class="hide">
                                    <h4>Kết nối<sup></sup></h4><span><ul>
                                    <li>Giắc âm thanh stereo 3,5 mm</li>
                                    <li>NFC<sup>1</sup></li>
                                    <li>Bluetooth® 4.0 hỗ trợ aptX™ </li>
                                    <li>Wifi®: IEEE 802,11 a/b/g/n (2,4 &amp; 5 GHz)</li>
                                    <li>DLNA® để truyền phát không dây nội dung đa phương tiện từ điện thoại sang TV hoặc máy tính tương thích</li>
                                    <li>HTC Connect™</li>
                                    <li>cổng micro-USB 2,0</li>
                                </ul></span>
                                </li>
                                <li class="hide">
                                    <h4>Đa phương tiện<sup></sup></h4><span>Định dạng âm thanh được hỗ trợ:<br> <strong>Phát lại:</strong> .aac, .amr, .ogg, .m4a, .mid, .mp3, .flac, .wav, .wma (Windows Media Audio 10)<br> <strong>Ghi âm:</strong> .aac<br> <br> Định dạng phim được hỗ trợ:<br> <strong>Phát lại:</strong>.3gp, .3g2, .mp4, .mkv, .wmv (Windows Media Video 10), .avi (MP4 ASP and MP3)<br> <strong>Ghi âm:</strong> .mp4<br></span>
                                </li>
                            </ul>
                        </div>
                        <div class="column three">
                            <ul>
                                <li class="">
                                    <h4>Màn hình<sup></sup></h4><span><p>4,5 inch, độ phân giải HD720</p></span>
                                </li>
                                <li class="">
                                    <h4>Loại Thẻ SIM<sup></sup></h4><span><p>nano SIM</p></span>
                                </li>
                                <li class="hide">
                                    <h4>GPS<sup></sup></h4><span><p>Ăngten GPS tích hợp + GLONASS</p>
                                    <p>La bàn số</p></span>
                                </li>
                                <li class="hide">
                                    <h4>Âm thanh<sup></sup></h4>
                                    <ul>
                                        <li>HTC BoomSound™</li>
                                        <li>Loa stereo kép mặt trước với bộ khuếch đại âm thanh tích hợp</li>
                                        <li>Sense Voice</li>
                                    </ul>
                                </li>
                                <li class="hide">
                                    <h4>Pin<sup>3</sup></h4><span><p>Dung lượng: 2100 mAh</p>
                                    <p>Pin polyme Li-ion liền máy, sạc lại được</p>
                                    <p>Thời gian đàm thoại:</p>
                                    <ul>
                                        <li>Tối đa 16.2 giờ trên nền 3G</li>
                                    </ul>
                                    <p>Thời gian chờ:</p>
                                    <ul>
                                        <li>Tối đa 556 giờ trên nền 3G</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="footnote">
                            <ol>
                                <li>
                                    <p>Băng tần mạng ở các khu vực có thể khác nhau, tùy thuộc vào nhà khai thác dịch vụ điện thoại di động và vị trí của bạn. 4G LTE &amp; NFC chỉ có sẵn ở một số quốc gia đã chọn. Tốc độ tải lên và tải xuống cũng tùy
                                        thuộc vào nhà khai thác dịch vụ điện thoại di động.</p>
                                </li>
                                <li>
                                    <p>Bộ nhớ trống có thể ít hơn tùy thuộc vào phần mềm hiện đang sử dụng của máy. Người dùng có khoảng 10.57 GB bộ nhớ để lưu trữ nội dung. Dung lượng bộ nhớ khả dụng có thể thay đổi tùy theo bản cập nhật phần mềm điện
                                        thoại và lưu lượng sử dụng của ứng dụng.</p>
                                </li>
                                <li>
                                    <p>Thời gian sử dụng pin (thời gian đàm thoại, thời gian chờ và nhiều thông số khác) tùy thuộc vào mạng và cách sử dụng điện thoại.
                                        <br>
                                        <br>Thông số kỹ thuật Thời gian chờ ("thông số kỹ thuật") là một tiêu chuẩn ngành chỉ dùng để cho phép so sánh các thiết bị di động khác nhau trong những điều kiện như nhau. Mức tiêu hao năng lượng trong chế độ
                                        chờ còn phụ thuộc vào nhiều yếu tố bao gồm nhưng không hạn chế ở mạng điện thoại, chế độ cài đặt, vị trí, sự di chuyển, cường độ sóng và số lượng thuê bao tập trung tại khu vực đó. Do đó, việc so sánh thời gian
                                        chờ của các thiết bị di động khác nhau chỉ có thể được thực hiện trong điều kiện kiểm soát chặt chẽ của phòng thí nghiệm. Khi sử dụng bất kỳ thiết bị di động nào trong điều kiện thực tế, thời gian chờ sẽ có
                                        thể thấp hơn và phụ thuộc rất nhiều vào các yếu tố đã nêu trên.</p>
                                </li>
                            </ol>
                        </div>
                        <div class="specs-footnote">Lưu ý: Thông số kỹ thuật có thể thay đổi mà không cần thông báo trước.</div>
                    </div>
                    <div class="rule">
                        <div class="toggle-specs">
                            <div class="expand-collapse"></div>
                            <div class="label default">XEM TẤT CẢ THÔNG SỐ KỸ THUẬT</div>
                            <div class="label alt">XEM TỪNG PHẦN THÔNG SỐ KỸ THUẬT</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="specs-overlay"></div>
        </div>
    </section>
    <div class="footer-space"></div>
    <div class="htc-footer" id="htc-footer">
        <footer>
            <div class="footer-wrapper">
                <div class="main-desk-modern">
                    <div class="footer-align-center">
                        <div class="region-block">
                            <div class="click-area">
                                <div class="region-text-wrapper">
                                    <div class="region-text">Việt Nam</div>
                                </div>
                                <div class="globe"></div>
                            </div>
                            <div class="left-declaration-block">© 2014-2015 HTC Corporation</div>
                        </div>
                        <div class="arrow-block">
                            <div class="arrow-back"></div>
                            <div class="arrow-icon"></div>
                        </div>
                        <div class="connect-block">
                            <div class="right-declaration-block">
                                <ul>
                                    <li><a rel="follow" target="_self" href="#">Chính sách Bảo mật</a>
                                    </li>
                                    <li><a rel="follow" target="_self" href="#">Bảo mật sản phẩm</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="click-area">
                                <div class="default-area">
                                    <div class="dot-grey">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="connect-wording">
                                        KẾT NỐI
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="popover region-popover">
                            <div class="region-list">
                                <div class="popover-wrapper">
                                    <div class="title-wrapper">
                                        <div class="title">Quốc gia</div>
                                    </div>
                                    <div class="list-wrapper desktop-wrapper">
                                        <ul>
                                            <li><a href="http://www.htc.com/au/">Australia</a>
                                            </li>
                                            <li><a href="http://www.htc.com/at/">Österreich</a>
                                            </li>
                                            <li><a href="http://www.htc.com/be-nl/">België</a>
                                            </li>
                                            <li><a href="http://www.htc.com/be-fr/">Belgique</a>
                                            </li>
                                            <li><a href="http://www.htc.com/br/support/">Brazil</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ca/">Canada - English</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ca-fr/">Canada - Français</a>
                                            </li>
                                            <li><a href="http://www.htc.com/cz/">Česká republika</a>
                                            </li>
                                            <li><a href="http://www.htc.com/cn/">中国</a>
                                            </li>
                                            <li><a href="http://www.htc.com/dk/">Danmark</a>
                                            </li>
                                            <li><a href="http://www.htc.com/de/">Deutschland</a>
                                            </li>
                                            <li><a href="http://www.htc.com/es/">España</a>
                                            </li>
                                            <li><a href="http://www.htc.com/fi/">Suomi</a>
                                            </li>
                                            <li><a href="http://www.htc.com/fr/">France</a>
                                            </li>
                                            <li><a href="http://www.htc.com/gr/">Ελλάδα</a>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><a href="http://www.htc.com/hk-en/">Hong Kong</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hk-tc/">香港</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hr/">Hrvatska</a>
                                            </li>
                                            <li><a href="http://www.htc.com/in/">India</a>
                                            </li>
                                            <li><a href="http://www.htc.com/id/">Indonesia</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ie/">Ireland</a>
                                            </li>
                                            <li><a href="http://www.htc.com/it/">Italia</a>
                                            </li>
                                            <li><a href="http://www.htc.com/jp/">日本</a>
                                            </li>
                                            <li><a href="http://www.htc.com/kr/">한국</a>
                                            </li>
                                            <li><a href="http://www.htc.com/kz/">Казахстан</a>
                                            </li>
                                            <li><a href="http://www.htc.com/latam/">América Latina</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hu/">Magyarország</a>
                                            </li>
                                            <li><a href="http://www.htc.com/mea-en/">Middle East</a>
                                            </li>
                                            <li><a href="http://www.htc.com/mea-sa/">الشرق الأوسط</a>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><a href="http://www.htc.com/mm/">Myanmar</a>
                                            </li>
                                            <li><a href="http://www.htc.com/nl/">Nederland</a>
                                            </li>
                                            <li><a href="http://www.htc.com/nz/">New Zealand</a>
                                            </li>
                                            <li><a href="http://www.htc.com/no/">Norge</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ru/">Россия и СНГ</a>
                                            </li>
                                            <li><a href="http://www.htc.com/pl/">Polska</a>
                                            </li>
                                            <li><a href="http://www.htc.com/pt/">Portugal</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ro/">România</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ch-de/">die Schweiz</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ch-it/">Svizzera</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ch-fr/">Suisse</a>
                                            </li>
                                            <li><a href="http://www.htc.com/rs/">Србија</a>
                                            </li>
                                            <li><a href="http://www.htc.com/sk/">Slovensko</a>
                                            </li>
                                            <li><a href="http://www.htc.com/sea/">Singapore</a>
                                            </li>
                                            <li><a href="http://www.htc.com/se/">Sverige</a>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><a href="http://www.htc.com/tw/">台灣</a>
                                            </li>
                                            <li><a href="http://www.htc.com/th/">ประเทศไทย</a>
                                            </li>
                                            <li><a href="http://www.htc.com/tr/">Türkiye</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ua/">Україна</a>
                                            </li>
                                            <li><a href="http://www.htc.com/uk/">United Kingdom</a>
                                            </li>
                                            <li><a href="http://www.htc.com/us/">United States</a>
                                            </li>
                                            <li><a href="#">Việt Nam</a>
                                            </li>
                                            <li><a href="http://www.htc.com/us/contact/additional_support.html">Additional Support</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="list-wrapper mobile-wrapper">
                                        <ul>
                                            <li><a href="http://www.htc.com/au/">Australia</a>
                                            </li>
                                            <li><a href="http://www.htc.com/at/">Österreich</a>
                                            </li>
                                            <li><a href="http://www.htc.com/be-nl/">België</a>
                                            </li>
                                            <li><a href="http://www.htc.com/be-fr/">Belgique</a>
                                            </li>
                                            <li><a href="http://www.htc.com/br/support/">Brazil</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ca/">Canada - English</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ca-fr/">Canada - Français</a>
                                            </li>
                                            <li><a href="http://www.htc.com/cz/">Česká republika</a>
                                            </li>
                                            <li><a href="http://www.htc.com/cn/">中国</a>
                                            </li>
                                            <li><a href="http://www.htc.com/dk/">Danmark</a>
                                            </li>
                                            <li><a href="http://www.htc.com/de/">Deutschland</a>
                                            </li>
                                            <li><a href="http://www.htc.com/es/">España</a>
                                            </li>
                                            <li><a href="http://www.htc.com/fi/">Suomi</a>
                                            </li>
                                            <li><a href="http://www.htc.com/fr/">France</a>
                                            </li>
                                            <li><a href="http://www.htc.com/gr/">Ελλάδα</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hk-en/">Hong Kong</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hk-tc/">香港</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hr/">Hrvatska</a>
                                            </li>
                                            <li><a href="http://www.htc.com/in/">India</a>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><a href="http://www.htc.com/id/">Indonesia</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ie/">Ireland</a>
                                            </li>
                                            <li><a href="http://www.htc.com/it/">Italia</a>
                                            </li>
                                            <li><a href="http://www.htc.com/jp/">日本</a>
                                            </li>
                                            <li><a href="http://www.htc.com/kr/">한국</a>
                                            </li>
                                            <li><a href="http://www.htc.com/kz/">Казахстан</a>
                                            </li>
                                            <li><a href="http://www.htc.com/latam/">América Latina</a>
                                            </li>
                                            <li><a href="http://www.htc.com/hu/">Magyarország</a>
                                            </li>
                                            <li><a href="http://www.htc.com/mea-en/">Middle East</a>
                                            </li>
                                            <li><a href="http://www.htc.com/mea-sa/">الشرق الأوسط</a>
                                            </li>
                                            <li><a href="http://www.htc.com/mm/">Myanmar</a>
                                            </li>
                                            <li><a href="http://www.htc.com/nl/">Nederland</a>
                                            </li>
                                            <li><a href="http://www.htc.com/nz/">New Zealand</a>
                                            </li>
                                            <li><a href="http://www.htc.com/no/">Norge</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ru/">Россия и СНГ</a>
                                            </li>
                                            <li><a href="http://www.htc.com/pl/">Polska</a>
                                            </li>
                                            <li><a href="http://www.htc.com/pt/">Portugal</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ro/">România</a>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li><a href="http://www.htc.com/ch-de/">die Schweiz</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ch-it/">Svizzera</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ch-fr/">Suisse</a>
                                            </li>
                                            <li><a href="http://www.htc.com/rs/">Србија</a>
                                            </li>
                                            <li><a href="http://www.htc.com/sk/">Slovensko</a>
                                            </li>
                                            <li><a href="http://www.htc.com/sea/">Singapore</a>
                                            </li>
                                            <li><a href="http://www.htc.com/se/">Sverige</a>
                                            </li>
                                            <li><a href="http://www.htc.com/tw/">台灣</a>
                                            </li>
                                            <li><a href="http://www.htc.com/th/">ประเทศไทย</a>
                                            </li>
                                            <li><a href="http://www.htc.com/tr/">Türkiye</a>
                                            </li>
                                            <li><a href="http://www.htc.com/ua/">Україна</a>
                                            </li>
                                            <li><a href="http://www.htc.com/uk/">United Kingdom</a>
                                            </li>
                                            <li><a href="http://www.htc.com/us/">United States</a>
                                            </li>
                                            <li><a href="http://www.htc.com/vn/">Việt Nam</a>
                                            </li>
                                            <li><a href="http://www.htc.com/us/contact/additional_support.html">Additional Support</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="list-closer"></div>
                                </div>
                            </div>
                            <div class="extra-space"></div>
                        </div>
                        <div class="popover connect-popover">
                            <div class="social-list">
                                <div class="align-center">
                                    <a rel="follow" title="Facebook" href="#">
                                        <img alt="Facebook" src="images/connect-fb.png">
                                    </a>
                                    <a rel="follow" title="Twitter" href="#">
                                        <img alt="Twitter" src="images/connect-twitter.png">
                                    </a>
                                    <a rel="follow" title="Google+" href="#">
                                        <img alt="Google+" src="images/connect-googleplus.png">
                                    </a>
                                    <a rel="follow" title="Youtube" href="#">
                                        <img alt="Youtube" src="images/connect-youtube.png">
                                    </a>
                                </div>
                            </div>
                            <div class="extra-space"></div>
                        </div>
                    </div>
                </div>
                <div class="main-desk">
                    <div class="footer-align-center">
                        <div class="level-one">
                            <div class="region-block">
                                <div class="align-center click-area">Việt Nam</div>
                                <div class="globe click-area"></div>
                            </div>
                            <div class="popover region-popover">
                                <div class="region-list">
                                    <div class="popover-wrapper">
                                        <div class="title-wrapper">
                                            <div class="title">Quốc gia</div>
                                        </div>
                                        <div class="list-wrapper desktop-wrapper">
                                            <ul>
                                                <li><a href="http://www.htc.com/au/">Australia</a>
                                                </li>
                                                <li><a href="http://www.htc.com/at/">Österreich</a>
                                                </li>
                                                <li><a href="http://www.htc.com/be-nl/">België</a>
                                                </li>
                                                <li><a href="http://www.htc.com/be-fr/">Belgique</a>
                                                </li>
                                                <li><a href="http://www.htc.com/br/support/">Brazil</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ca/">Canada - English</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ca-fr/">Canada - Français</a>
                                                </li>
                                                <li><a href="http://www.htc.com/cz/">Česká republika</a>
                                                </li>
                                                <li><a href="http://www.htc.com/cn/">中国</a>
                                                </li>
                                                <li><a href="http://www.htc.com/dk/">Danmark</a>
                                                </li>
                                                <li><a href="http://www.htc.com/de/">Deutschland</a>
                                                </li>
                                                <li><a href="http://www.htc.com/es/">España</a>
                                                </li>
                                                <li><a href="http://www.htc.com/fi/">Suomi</a>
                                                </li>
                                                <li><a href="http://www.htc.com/fr/">France</a>
                                                </li>
                                                <li><a href="http://www.htc.com/gr/">Ελλάδα</a>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li><a href="http://www.htc.com/hk-en/">Hong Kong</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hk-tc/">香港</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hr/">Hrvatska</a>
                                                </li>
                                                <li><a href="http://www.htc.com/in/">India</a>
                                                </li>
                                                <li><a href="http://www.htc.com/id/">Indonesia</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ie/">Ireland</a>
                                                </li>
                                                <li><a href="http://www.htc.com/it/">Italia</a>
                                                </li>
                                                <li><a href="http://www.htc.com/jp/">日本</a>
                                                </li>
                                                <li><a href="http://www.htc.com/kr/">한국</a>
                                                </li>
                                                <li><a href="http://www.htc.com/kz/">Казахстан</a>
                                                </li>
                                                <li><a href="http://www.htc.com/latam/">América Latina</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hu/">Magyarország</a>
                                                </li>
                                                <li><a href="http://www.htc.com/mea-en/">Middle East</a>
                                                </li>
                                                <li><a href="http://www.htc.com/mea-sa/">الشرق الأوسط</a>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li><a href="http://www.htc.com/mm/">Myanmar</a>
                                                </li>
                                                <li><a href="http://www.htc.com/nl/">Nederland</a>
                                                </li>
                                                <li><a href="http://www.htc.com/nz/">New Zealand</a>
                                                </li>
                                                <li><a href="http://www.htc.com/no/">Norge</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ru/">Россия и СНГ</a>
                                                </li>
                                                <li><a href="http://www.htc.com/pl/">Polska</a>
                                                </li>
                                                <li><a href="http://www.htc.com/pt/">Portugal</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ro/">România</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ch-de/">die Schweiz</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ch-it/">Svizzera</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ch-fr/">Suisse</a>
                                                </li>
                                                <li><a href="http://www.htc.com/rs/">Србија</a>
                                                </li>
                                                <li><a href="http://www.htc.com/sk/">Slovensko</a>
                                                </li>
                                                <li><a href="http://www.htc.com/sea/">Singapore</a>
                                                </li>
                                                <li><a href="http://www.htc.com/se/">Sverige</a>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li><a href="http://www.htc.com/tw/">台灣</a>
                                                </li>
                                                <li><a href="http://www.htc.com/th/">ประเทศไทย</a>
                                                </li>
                                                <li><a href="http://www.htc.com/tr/">Türkiye</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ua/">Україна</a>
                                                </li>
                                                <li><a href="http://www.htc.com/uk/">United Kingdom</a>
                                                </li>
                                                <li><a href="http://www.htc.com/us/">United States</a>
                                                </li>
                                                <li><a href="http://www.htc.com/vn/">Việt Nam</a>
                                                </li>
                                                <li><a href="http://www.htc.com/us/contact/additional_support.html">Additional Support</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="list-wrapper mobile-wrapper">
                                            <ul>
                                                <li><a href="http://www.htc.com/au/">Australia</a>
                                                </li>
                                                <li><a href="http://www.htc.com/at/">Österreich</a>
                                                </li>
                                                <li><a href="http://www.htc.com/be-nl/">België</a>
                                                </li>
                                                <li><a href="http://www.htc.com/be-fr/">Belgique</a>
                                                </li>
                                                <li><a href="http://www.htc.com/br/support/">Brazil</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ca/">Canada - English</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ca-fr/">Canada - Français</a>
                                                </li>
                                                <li><a href="http://www.htc.com/cz/">Česká republika</a>
                                                </li>
                                                <li><a href="http://www.htc.com/cn/">中国</a>
                                                </li>
                                                <li><a href="http://www.htc.com/dk/">Danmark</a>
                                                </li>
                                                <li><a href="http://www.htc.com/de/">Deutschland</a>
                                                </li>
                                                <li><a href="http://www.htc.com/es/">España</a>
                                                </li>
                                                <li><a href="http://www.htc.com/fi/">Suomi</a>
                                                </li>
                                                <li><a href="http://www.htc.com/fr/">France</a>
                                                </li>
                                                <li><a href="http://www.htc.com/gr/">Ελλάδα</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hk-en/">Hong Kong</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hk-tc/">香港</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hr/">Hrvatska</a>
                                                </li>
                                                <li><a href="http://www.htc.com/in/">India</a>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li><a href="http://www.htc.com/id/">Indonesia</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ie/">Ireland</a>
                                                </li>
                                                <li><a href="http://www.htc.com/it/">Italia</a>
                                                </li>
                                                <li><a href="http://www.htc.com/jp/">日本</a>
                                                </li>
                                                <li><a href="http://www.htc.com/kr/">한국</a>
                                                </li>
                                                <li><a href="http://www.htc.com/kz/">Казахстан</a>
                                                </li>
                                                <li><a href="http://www.htc.com/latam/">América Latina</a>
                                                </li>
                                                <li><a href="http://www.htc.com/hu/">Magyarország</a>
                                                </li>
                                                <li><a href="http://www.htc.com/mea-en/">Middle East</a>
                                                </li>
                                                <li><a href="http://www.htc.com/mea-sa/">الشرق الأوسط</a>
                                                </li>
                                                <li><a href="http://www.htc.com/mm/">Myanmar</a>
                                                </li>
                                                <li><a href="http://www.htc.com/nl/">Nederland</a>
                                                </li>
                                                <li><a href="http://www.htc.com/nz/">New Zealand</a>
                                                </li>
                                                <li><a href="http://www.htc.com/no/">Norge</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ru/">Россия и СНГ</a>
                                                </li>
                                                <li><a href="http://www.htc.com/pl/">Polska</a>
                                                </li>
                                                <li><a href="http://www.htc.com/pt/">Portugal</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ro/">România</a>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li><a href="http://www.htc.com/ch-de/">die Schweiz</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ch-it/">Svizzera</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ch-fr/">Suisse</a>
                                                </li>
                                                <li><a href="http://www.htc.com/rs/">Србија</a>
                                                </li>
                                                <li><a href="http://www.htc.com/sk/">Slovensko</a>
                                                </li>
                                                <li><a href="http://www.htc.com/sea/">Singapore</a>
                                                </li>
                                                <li><a href="http://www.htc.com/se/">Sverige</a>
                                                </li>
                                                <li><a href="http://www.htc.com/tw/">台灣</a>
                                                </li>
                                                <li><a href="http://www.htc.com/th/">ประเทศไทย</a>
                                                </li>
                                                <li><a href="http://www.htc.com/tr/">Türkiye</a>
                                                </li>
                                                <li><a href="http://www.htc.com/ua/">Україна</a>
                                                </li>
                                                <li><a href="http://www.htc.com/uk/">United Kingdom</a>
                                                </li>
                                                <li><a href="http://www.htc.com/us/">United States</a>
                                                </li>
                                                <li><a href="http://www.htc.com/vn/">Việt Nam</a>
                                                </li>
                                                <li><a href="http://www.htc.com/us/contact/additional_support.html">Additional Support</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="list-closer"></div>
                                    </div>
                                </div>
                                <div class="extra-space"></div>
                            </div>
                            <div class="arrow-block"></div>
                            <div class="connect-block">
                                <div class="default-area click-area">
                                    <div class="dot-grey">
                                        <div class="circle"></div>
                                    </div>
                                    KẾT NỐI
                                </div>
                            </div>
                            <div class="popover connect-popover">
                                <div class="social-list">
                                    <div class="align-center">
                                        <a rel="follow" title="Facebook" href="#">
                                            <img alt="Facebook" src="images/connect-fb.png">
                                        </a>
                                        <a rel="follow" title="Twitter" href="#">
                                            <img alt="Twitter" src="images/connect-twitter.png">
                                        </a>
                                        <a rel="follow" title="Google+" href="#">
                                            <img alt="Google+" src="images/connect-googleplus.png">
                                        </a>
                                        <a rel="follow" title="Youtube" href="#">
                                            <img alt="Youtube" src="images/connect-youtube.png">
                                        </a>
                                    </div>
                                </div>
                                <div class="extra-space"></div>
                            </div>
                        </div>
                        <div class="level-two">
                            <div class="left-col">
                                <div class="copyright">
                                    © 2014-2015 HTC Corporation
                                    <ul>
                                        <li><a rel="follow" target="_self" href="#">Chính sách Bảo mật</a>
                                        </li>
                                        <li><a rel="follow" target="_self" href="#">Bảo mật sản phẩm</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="right-col">
                                <div class="list-of-links">
                                    <ul>
                                        <li><a rel="" target="_self" href="#">Điều khoản sử dụng</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Chính sách bảo mật</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-list">
                    <div class="footer-align-center">
                        <div class="links">
                            <div class="links-col1 m-group-col-1">
                                <ul>
                                    <li>
                                        <div class="title">SẢN PHẨM</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">SẢN PHẨM</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">RE CAMERA</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title">HỖ TRỢ</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">Trung tâm hỗ trợ</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Hỗ trợ bảo hành HTC</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title">Pháp lý</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">Chính sách Bảo mật</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Quy Định Sử Dụng</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Quy Định Xử Sự</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Bản Quyền Và Sở Hữu Trí Tuệ</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Tìm hiểu thêm</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="links-col2 m-group-col-1">
                                <ul>
                                    <li>
                                        <div class="title"><a href="#">Phụ kiện</a>
                                        </div>
                                        <ul class="sublinks"></ul>
                                    </li>
                                    <li>
                                        <div class="title">PHÒNG TIN TỨC</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">Phòng tin tức</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Tổng quan về Công ty</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Lãnh đạo</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Trách nhiệm của Doanh nghiệp</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="links-col3 m-group-col-2">
                                <ul>
                                    <li>
                                        <div class="title">Công nghệ cải tiến</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">Công nghệ cải tiến</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Máy ảnh ultrapixel</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">HTC Transfer</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Power to Give</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">HTC Connect</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title">Nghề nghiệp</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">Làm việc tại HTC</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="links-col4 m-group-col-2">
                                <ul>
                                    <li>
                                        <div class="title">LIÊN HỆ VỚI CHÚNG TÔI</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">GỌI ĐIỆN</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">EMAIL HỖ TRỢ</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">LÊN LỊCH HẸN</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <div class="title">Sites</div>
                                        <ul class="sublinks">
                                            <li><a rel="" target="_self" href="#">HTC Blog</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">HTC Dev</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">HTC Pro</a>
                                            </li>
                                            <li><a rel="" target="_self" href="#">Bắt đầu ngay</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="m-links">
                            <ul>
                                <li>
                                    <div class="title">SẢN PHẨM</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">SẢN PHẨM</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">RE CAMERA</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title"><a href="#">Phụ kiện</a>
                                    </div>
                                    <ul class="sublinks"></ul>
                                </li>
                                <li>
                                    <div class="title">Công nghệ cải tiến</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">Công nghệ cải tiến</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Máy ảnh ultrapixel</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">HTC Transfer</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Power to Give</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">HTC Connect</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title">LIÊN HỆ VỚI CHÚNG TÔI</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">GỌI ĐIỆN</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">EMAIL HỖ TRỢ</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">LÊN LỊCH HẸN</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title">HỖ TRỢ</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">Trung tâm hỗ trợ</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Hỗ trợ bảo hành HTC</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title">PHÒNG TIN TỨC</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">Phòng tin tức</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Tổng quan về Công ty</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Lãnh đạo</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Trách nhiệm của Doanh nghiệp</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title">Nghề nghiệp</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">Làm việc tại HTC</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title">Sites</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">HTC Blog</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">HTC Dev</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">HTC Pro</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Bắt đầu ngay</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <div class="title">Pháp lý</div>
                                    <ul class="sublinks">
                                        <li><a rel="" target="_self" href="#">Chính sách Bảo mật</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Quy Định Sử Dụng</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Quy Định Xử Sự</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#/">Bản Quyền Và Sở Hữu Trí Tuệ</a>
                                        </li>
                                        <li><a rel="" target="_self" href="#">Tìm hiểu thêm</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="legal">
                            <div class="logo"></div>
                            <div class="copyright">
                                <p class="bold">Bản quyền 2014-2015 HTC Corporation. Mọi quyền được bảo lưu.</p>
                                <ul>
                                    <li><a rel="" target="_self" href="#">Điều khoản sử dụng</a>
                                    </li>
                                    <li><a rel="" target="_self" href="#">Chính sách bảo mật</a>
                                    </li>
                                </ul>
                                <div class="footer-content">
                                    <!-- RAW CONTENT BEGIN -->
                                    <!-- RAW CONTENT END -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="deep-bottom show"></div>
            </div>
            <input name="downToFooter" id="downToFooter" type="hidden">
        </footer>
    </div>
</div>
</body>

</html>
