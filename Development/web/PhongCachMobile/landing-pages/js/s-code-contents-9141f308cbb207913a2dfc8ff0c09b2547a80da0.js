/*
 * GLOBAL EMBED
 */

/*
 * BEGIN SiteCatalyst.js
 */

/*
 * START Helper function
 */
//var browserVersion = new String(navigator.sayswho);

s_getLoadTime();


 // Get a section of a URL
function getUrlParameter(name, url) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var pattern = /SCRIPT|OBJECT|APPLET|FORM|EMBED|%(7b|7d|7c|5c|5e|7e|5b|5d|60|25|27|3b|2f|3f|3a|40|3d|26|3c|3e|22|23)|;|\/|\?|:|@|=|&|<|>|"|#|\{|\}|\||\\|\^|~|\[|\]|`|%|'/ig;
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(url);
	if(results === null) {
		return "";	
	} else {
		var result = results[1];
		result = result.replace(pattern, "");
		return result;
	}
}

function parsePhoneFromURL(URL) {
	var link = URL;
	var parts = link.split("/");
	var phone= "Cant resolve phone name";

	for (var i = 0; i < parts.length; i++) {
		if (parts[i] == "smartphones") {
			if (parts[i+1]) {
				if (parts[i+1] != "") {
					phone = parts[i+1];
				}
			}
		}
	}

	return phone;
}

// END Helper function

/*
 * Determine URL, Region, and site
 */

var URL = window.location.hostname.toLowerCase() + location.pathname.toLowerCase();

// Any microsite.htc.com
var isSubdomainOfHTC = false;
// Any Test Link of HTC
var isTestLink = false;
// A htc.com site but doesnt conform has a special case
var isSpecialCase = false;
// Any site that uses tag Manager but is not htc.com
var isOther = false;
// An already known "other" site
var isKnownOther = false;
// Omniture Page Name
var pageName = "";
// Omniture Channel
var channel = "";
// The microsite identifier ie microsite.htc.com
var htcSubdomain = "";
// The microsite follows the same format as htc.com ex: eclub
var isStandardSubdomain = false;
// Check the flag set by the errorPage module on the site
var isError = false;
if (typeof errorPage != "undefined") {
	if (errorPage) {
		isError = true;
	}
}

/*
 * Standard HTC.com values
 */

// part after htc.com/
var region = "";
// part after region, ex: htc.com/www/smartphones
var section = "";
// part after section if it exists, ex: htc.com/www/smartphones/htc-one-x
var subsection = "";

// The tagging Account
var accountURL = "";

/*
 * All of the options that determine if this is a standard HTC.com link or a test
 * link of a standard link or "other".
 *
 * HTC Test Links www1 www2 etc
 * AG Test Links 12.130.xx.xx
 */

if((/htc\.com|12\.130|htc-staging|preview-prod|razorfishsf.com/).test(URL)) {
	isOther = false;
	//alert("First False");
	// HTC Test Link
	if((/(www\d)\.htc\.com|12\.130|qa-www|stage-www.htc.com|razorfishsf.com|preview-prod/).test(URL)) {
		//alert("This is a test link!");
		if (true) {
		}
		isTestLink = true;
	}

	// Exclude www or www1 from servers
	if (!(/(www|www\d)\.htc\.com/).test(URL)) {
		if ((/(.+)\.htc\.com/).test(URL)) {
			//alert("This is a subdomain!");
			isSubdomainOfHTC = true;
			htcSubdomain = RegExp.$1;
		}

		// Some subdomains follow the same pattern as htc.com ex: eclub.htc.com/us
		if (htcSubdomain == "eclub") {
			isStandardSubdomain = true;
		} else if (htcSubdomain == "support") {
			isStandardSubdomain = true;
		// Preview Prod is not a subdomain
		} else if (htcSubdomain == "preview-prod") {
			isSubdomainOfHTC = false;
		}
	}

	if (!/(www|www\d)\.razorfishsf\.com/.test(URL)) {
		if ((/(.+)\.razorfishsf\.com/).test(URL)) {
			isSubdomainOfHTC = true;
			htcSubdomain = RegExp.$1;
		}
	}

	/*
	 * Set up any special cases here, by setting isSpecialCase to true
	 * this will bypass the regular pageName determination logic.
	 */

	// The signup form has the region in a different place
	if ((/signup\.form/).test(URL)) {
		isSpecialCase = true;
	// The contact us email form has the region in a different place
	// also requires overriding of pagename
	} else if ((/about_htc_bymail_iframe/).test(URL)) {
		isSpecialCase = true;
	} else if (isError) {
		isSpecialCase = true;
	} else if ((/htc-sync-manager\/.+\/download.html/).test(URL)) {
		isSpecialCase = true;
	} else if ((/mindtouch/).test(URL)) {
		isSpecialCase = true;
	}
} else {
	isOther = true;
	//alert("Is Other is TRUE");
}

if(!isOther && !isSpecialCase) {

	// This will be null if it was not set above
	if (isSubdomainOfHTC) {
		// Some subdomains will be identified but requested to be removed
		if ((/eclub/).test(htcSubdomain)) {
			// dont add subdomain part to pagename
		} else {
			pageName += htcSubdomain;
		}
	}

	// replace the last "/" with "" then split the url on "/"
	var urlParts = URL.replace(/\/$/, "").split("/");

	if(!urlParts[2] && !isSubdomainOfHTC) {
		region = urlParts[1];
		//check some one offs
		if (region == "legend" || region == "smartphones") {
			region = "www";
		}
	} else {

		var iterator = 0;

		// If we have determined this is a subdomain, we cannot guarantee
		// that urlParts[1] is the region, in order to skip it
		if (isSubdomainOfHTC && !isStandardSubdomain) {
			iterator = 1;
		} else {
			iterator = 2;
		}

		// Set Standard HTC Variables
		if(!isSubdomainOfHTC) {
			if(urlParts[1]) {
				region = urlParts[1];
				//check for some www one-offs
				if (region == "member") {
					region = "www";
				}
				
			}
			if(urlParts[2]) {
				section = urlParts[2];
				channel = section.replace(/\..+/, "");
			}
			if(urlParts[3]) {
				subsection = urlParts[3];
			}
		// Set Standard HTC Variables of standard subdomains
		} else if (isStandardSubdomain) {
			region = urlParts[1];
			if (urlParts[2]) {
				section = urlParts[2];
				channel = section.replace(/\..+/, "");
			}
			
			if (htcSubdomain == "support") {
				
				region = urlParts[1];
				
				channel = "support";
			}
		} else {
			/*
			 * Set region or pagename manually for servers here like below.
			 */
			
			if((/tw-investors/).test(htcSubdomain)) {
				region = "tw";
			} else if((/investors/).test(htcSubdomain)) {
				region = "www";
			} else if ((/one/).test(htcSubdomain)) {
				region = 'htconeexperience';
			} else if (/htc-m7-qa/.test(htcSubdomain)) {
				region = 'htconeexperience';
			} else if (/shop\.us/.test(htcSubdomain)) {
				region = 'bestbuy_us_shop';
				channel = "shop.us";
			} else if (/store\.us/.test(htcSubdomain)) {
				region = 'us';
				channel = 'digital river store';
				accountURL = region;
			} else if (/eshop/.test(htcSubdomain)) {
				region = "china-eshop";
				// channel = "china eshop";
			} else if (/changethegame/.test(htcSubdomain)  || /fbapps/.test(htcSubdomain)) {
				channel = "UEFA Hub";
				var whatHub = document.URL.split("?l=")[1];
					if (whatHub != undefined) {
						whatHub = whatHub.split("&fb=")[0].toLowerCase();
						switch (whatHub) {
							case 'en_gb':
								region = "uk";
							break;
							
							case 'es_es':
								region = "es";
							break;
							
							case 'tr_tr':
								region = "tr";
							break;
							
							case 'en_ie':
								region = "ie";
							break;
							
							case 'de_de':
								region = "de";
							break;
							
							case 'fr_fr':
								region = "fr";
							break;
							
							case 'pl_pl':
								region = "pl";
							break;
							
							default:
							break;
						}
					} else {
						region = "us";	
					}
			} else if (/shop.emea/.test(htcSubdomain)) {
				channel = "EMEA shop";
				region = urlParts[1];
			} else if (/community/.test(htcSubdomain)) {
				if (urlParts[1] == "tw") {
				channel = "community";
				region = "twcom";
				}
			}else if (/estore/.test(htcSubdomain) || /promotion-estore/.test(htcSubdomain)) {
				if (urlParts[1] == "tw") {
						channel = "Taiwan Eshop";
						region = "tweshop";
				} else {
					channel = urlParts[1] + " Eshop";
					region = urlParts[1];
				}
			} else if (/in.store-test/.test(htcSubdomain)) {
					channel = urlParts[1] + " Eshop";
					region = urlParts[1];
			}else if (/bbs/.test(htcSubdomain)) {
				channel = "community";
				region = "cncom";
			} else if (/m/.test(htcSubdomain)) {
				region = urlParts[1];
			} else if (/careers/.test(htcSubdomain)) {	
				channel = "careers";
				region = "careers";
			} else {
				// We have an outlier
				_satellite.notify("Last Else, we have an outlier.",1);
				region = "unknown";
				channel = "unknown";
			}
		}
	}
} else if (isSpecialCase) {

	/*
	 * Add any special case data as needed here
	 */

	// Signup form iframe
	if ((/signup\.form/).test(URL)) {
		pageName = "signup: iframe";
		channel = "signup";
		region = getUrlParameter("site", window.location.href.toLowerCase());
	// Contact Us email form iframe
	} else if ((/about_htc_bymail_iframe/).test(URL)) {
		pageName = "contact: email: iframe";
		channel = "contact";
		var urlParts = URL.replace(/\/$/, "").split("/");
		region = urlParts[2];
	} else if ((/htc-sync-manager\/.+\/download.html/).test(URL)) {
		pageName = "support: htc-sync-manager: download";
		channel = "support";
		var urlParts = URL.replace(/\/$/, "").split("/");
		region = urlParts[2];
	} else if (isError) {
		pageName = "404 Error: " + window.location.href.toLowerCase();
		channel = "error";
		var urlParts = URL.replace(/\/$/, "").split("/");
		region = urlParts[1];
	} else if ((/mindtouch/).test(URL)) {
		pageName = "mindtouch";
		channel = "support";
		region = "us";
	}
} else if(isOther) {

	/*
	 * Set Each "other" site's pagename here, make sure to define isKnownOther as true
	 */

	//UK Store:
if ((/officialhtcstore/).test(URL)) {
		//alert("Setting region to uk");
		region = "uk";
		channel = "shop";
		isOther = false;
	}
	 
	// For taleo we only care about the final page ex apply.jsp
	if ((/taleo\.net/).test(URL)) {

		var urlParts = URL.replace(/\/$/, "").split("/");

		pageName = "taleo";

		for (var i = 1; i < urlParts.length; i++) {
			if ((/.jsp|careers/).test(urlParts[i])) {
				var curr = urlParts[i];
				// replace any extension with "" ex: .aspx, .page
				var part = curr.replace(/\..+/, "");

				if(pageName !== "") {
					pageName += ": ";
				}

				pageName += part;
			}
		}

		if(getUrlParameter("rid", window.location.href.toLowerCase())) {
			sectionURLParam = getUrlParameter("rid", window.location.href.toLowerCase());

			if(pageName !== "") {
				pageName += ": ";
			}

			pageName += sectionURLParam;
		}

		region = "us";
		isKnownOther = true;
	} else if ((/six\.surveys\.com/).test(URL)) {
		isKnownOther = true;
		pageName = "six.surveys.com";
	} else if ((/htchange\.de/).test(URL)) {
		isKnownOther = true;
		pageName = "Germany UEFA Hub";
		region = "de";
		channel = "UEFA";
		accountURL = region;
	} else if ((/htchange\.ch/).test(URL)) {
		isKnownOther = true;
		pageName = "Switzerland UEFA Hub";
		region = "ch-de";
		channel = "UEFA";
		accountURL = region;
	} else if ((/htchange\.at/).test(URL)) {
		isKnownOther = true;
		pageName = "Austria UEFA Hub";
		region = "at";
		channel = "UEFA";
		accountURL = region;		
	} else if ((/htclaunch\.com/).test(URL)) {
		isKnownOther = true;
		region = "www";
		channel = "htclaunch";
		accountURL = region;
	} else if (/htc.cust-serv/.test(URL)) {	
		isKnownOther = true;
		region = "us";		
		channel = "US Chat Serv";
		accountURL = region;
		pageName = "contact: chat: live chat";
	} else if (/recamera\.com/.test(URL)) {
		isKnownOther = true;
		region = document.URL.split(".com/")[1].split("\/")[0];
		channel = "recamera";
		accountURL = "recamera-prod";
		pageName = document.title;
	} else if (/htcvr\.com/.test(URL)) {
		isKnownOther = true;
		region = "us"	; //document.URL.split(".com/")[1].split("\/")[0];
		channel = "recamera";
		accountURL = "htcvr";
		pageName = document.title;
	}
	
	// This is not a known Other
	if (!isKnownOther) {
		pageName = window.location.href.toLowerCase();
	}

	// If anything lands in this section its channel is not set it should be other
	if (channel == '') {
		channel = "other";
	}
	
} else {
	// Something Broke, the above logic should have caught all options
	pageName = window.location.href.toLowerCase();
	// If anything lands in this section its channel should be other
	channel = "other";
}




/*
 * Set the account based on our URL matches. Last resort is test.
 */

if (!isOther && !isTestLink) {
	accountURL = region;
} else if (isTestLink) {
	
	accountURL = "test";
} else if (isOther) {
	/*
	 * Add each known "other" sites account here like below.
	 */
	
	if (isKnownOther) {

		if (pageName === "taleo.net") {
			accountURL = "us";
		} else if (pageName === "six.surveys.com") {
			accountURL = "six.surveys.com";
		}
	// unknown "other"
	} else {
		accountURL = "catchall";
	}
} else {
	// Fallback case
	accountURL = "catchall";
}

// END Set Account

/*
 * Custom Page Load / Event Code
 *
 *  Add any custom page load code below by matching a combination of pagename and URL as needed.
 */

// Search Page Dynamic Search on Results
var searchPageEvent = false;

var fireCustomEvents = false;
var customFlag = "";
var customEvents = "";
var customVariables = "";
var customValue = "";

// We do not want these to fire if we are on an "other"
if (!isOther) {
	
	var compareURLParam = "";
	if (getUrlParameter("1", window.location.href.toLowerCase())) {
		compareURLParam = getUrlParameter("1", window.location.href.toLowerCase());
	}

	// We are on a product page under smartphones but not on a tab, or compare
	if ((/smartphones: .+: index/).test(pageName) && !(/compare/g).test(pageName) && subsection != "" && subsection != "find-a-store" && !(/smartphones\/htc-one\/signup/).test(window.location.href.toLowerCase())) {
		customFlag = "prodDetailEvents";
		customEvents = ",event14,prodView";
		customValue = ";" + subsection;
		fireCustomEvents = true;
	} else if ((/arb/).test(pageName)) {

			//var currentProduct = $('.device-filter-selected-device');
			//var prodLink = currentProduct.find("img").attr("data-link");

			//console.log(prodLink);

			//customFlag = "arbProducts";
			//customValue = parsePhoneFromURL(prodLink);
	// We are on support, and we are on a product page because we have a p_id URL param
	} else if ((/support/).test(pageName)) {
		// if "p_name" exists IE we are on a product
		if (getUrlParameter("p_name", window.location.href.toLowerCase())) {

			// Save the p_name, since it used twice below
			var supportPName = getUrlParameter("p_name", window.location.href.toLowerCase());
			// Add the p_name to the pagename
			pageName += ": " + supportPName + ": index";

			// If we are on faq, we will have a cat, use this as a flag to grab the text from the dropdown
			if (getUrlParameter("cat", window.location.href.toLowerCase())) {
				var selector = $("#selectFaqCategory option:selected").text();

				// If there was a dropdown selected (ie default "Top 10" will not work) add it to the pagename
				if (selector) {
					pageName += ": " + selector;
				}
			}

			customFlag = "supportDetailEvents";
			customVariables = "prop35";
			customValue = "D=pageName";
			fireCustomEvents = true;
		// We removed index above, to allow the product to be first, if there is no product add index back in
		} else {
			pageName += ": index";
		}
	}
}

// END Custom Page Load / Event Code

/*
 * Define Tagging Account
 */

var account_def = {
	"www" : "htcww-en-prod,htcglobal-prod",
	// Mindtouch URL for www
	"en" : "htcww-en-prod,htcglobal-prod",
	"europe" : "htceurope-prod,htcglobal-prod",
	// Mindtouch URL for uk
	"uk" : "htcuk-prod,htcglobal-prod",
	"en-uk" : "htcuk-prod,htcglobal-prod",
	"sea" : "htcsea-prod,htcglobal-prod",
	"tw" : "htctw-prod,htcglobal-prod",
	"tweshop" : "htctweshop-prod,htcglobal-prod",
	"mideast" : "htcmea-prod,htcglobal-prod",
	"de" : "htcde-prod,htcglobal-prod",
	"fr" : "htcfr-prod,htcglobal-prod",
	"es" : "htces-prod,htcglobal-prod",
	"at" : "htcat-prod,htcglobal-prod",
	"be-fr" : "htcbefr-prod,htcglobal-prod",
	"be-nl" : "htcbenl-prod,htcglobal-prod",
	"sk" : "htcsk-prod,htcglobal-prod",
	"si" : "htcsi-prod,htcglobal-prod",
	"ch" : "htcchde-prod,htcglobal-prod",
	"ch-de" : "htcchde-prod,htcglobal-prod",
	"ch-fr" : "htcchfr-prod,htcglobal-prod",
	"ch-it" : "htcchit-prod,htcglobal-prod",
	"au" : "htcau-prod,htcglobal-prod",
	"nz" : "htcnz-prod,htcglobal-prod",
	"in" : "htcin-prod,htcglobal-prod",
	"asia" : "htcasia-prod,htcglobal-prod",
	"pt" : "htcpt-prod,htcglobal-prod",
	"dk" : "htcdk-prod,htcglobal-prod",
	"tr" : "htctr-prod,htcglobal-prod",
	"se" : "htcse-prod,htcglobal-prod",
	"nl" : "htcnl-prod,htcglobal-prod",
	"mea-en" : "htcmea-en,htcglobal-prod",
	"mea-fr" : "htcmeafr-prod,htcglobal-prod",
	"mea-sa" : "htcmeasa-prod,htcglobal-prod",
	"africa" : "htcafrica-prod,htcglobal-prod",
	"vn" : "htcvn-prod,htcglobal-prod",
	"no" : "htcno-prod,htcglobal-prod",
	"ro" : "htcro-prod,htcglobal-prod",
	"ru" : "htcru-prod,htcglobal-prod",
	"th" : "htcth-prod,htcglobal-prod",
	"hk-en" : "htchken-prod,htcglobal-prod",
	"hk-tc" : "htchktc-prod,htcglobal-prod",
	"it" : "htcit-prod,htcglobal-prod",
	"cz" : "htccz-prod,htcglobal-prod",
	"pl" : "htcpl-prod,htcglobal-prod",
	"gr" : "htcgr-prod,htcglobal-prod",
	"ua" : "htcua-prod,htcglobal-prod",
	"hu" : "htchu-prod,htcglobal-prod",
	"rs" : "htcrs-prod,htcglobal-prod",
	"arabic" : "htcarabic-prod,htcglobal-prod",
	"jp" : "htcjp-prod,htcglobal-prod",
	"cn" : "htccn-prod,htcglobal-prod",
	"china-eshop" : "htccneshop-prod,htcglobal-prod",
	"cncom" : "htccncom-prod,htcglobal-prod",
	"kr" : "htckr-prod,htcglobal-prod",
	"fi" : "htcfi-prod,htcglobal-prod",
	"mx" : "htcmx-prod,htcglobal-prod",
	"br" : "htcbr-prod,htcglobal-prod",
	"ie" : "htcie-prod,htcglobal-prod",
	"twcom" : "htctwcom-prod,htcglobal-prod",
	// Mindtouch URL for IE
	"en-ie" : "htcie-prod,htcglobal-prod",
	"cn2" : "htc-testing-reportsuite",
	"us" : "htcus-prod,htcglobal-prod",
	"en-us" : "htcus-prod,htcglobal-prod",
	"es-us" : "htcus-prod,htcglobal-prod",
	"ca-fr" : "htcca-prod,htcglobal-prod",
	"ca" : "htcca-prod,htcglobal-prod",
	"mm" : "htcmm-prod,htcglobal-prod",
	"six.surveys.com" : "htcww-en-prod,htcglobal-prod",
	"htconeexperience" : "htcglobal-prod",
	"latam": "htclatam-region-prod,htcglobal-prod",
	"test" : "htc-testing-reportsuite",
	"id" : "htcid-prod,htcglobal-prod",
	"kz" : "htckz-prod,htcglobal-prod",
	"hr" : "htchr-prod,htcglobal-prod",
	"bestbuy_us_shop" : "htcus-prod,htcglobal-prod",
	"catchall" : "htc-catchall",
	"recamera-prod" : "htc-recameraglobal-prod",
	"careers" : "htccareers-prod",
	"htcvr" : "htc-viveprod"
};

// Set the s_account variable to the account def that matches accountURL
if (account_def[accountURL]) {
	var s_account = account_def[accountURL];
// Provide a failsafe if accountURL determined is not on the def_account list
} else {
	var s_account = "htc-catchall";
}

// END Define Account

/*
* EXIT URL Arrays for exitURL (watch list) exitURLsToIgnore (ignore list)
*/

//Instantiate and populate the exitURL Array
var exitURLs = new Array(8);

//array format:  URL, events, linkTrackVars, linkTrackEvents, linkName, linktype
// NOTE: URL has to be exact
// reminder, length will be 1 higher than the last index, Array(num+1) - [num]
exitURLs[0] = "htcdev.com|event1|events|event1|DeveloperHTC.com";
exitURLs[1] = "htcpro.com|event1|events|event1|HTCPro.com";
exitURLs[2] = "htc.com/www|event1|events|event1|HTC Worldwide";
exitURLs[3] = "freeurecycle.com|event1|events|event1|HTC Recycling Program";
exitURLs[4] = "htcsense.com|event1|events|event1|HTCSense.com";
exitURLs[5] = "youtube.com|event1|events|event1|HTC on YouTube";
//exitURLs[6] = "facebook.com|event1|events|event1|HTC on Facebook";
//exitURLs[7] = "twitter.com|event1|events|event1|HTC on Twitter";
exitURLs[6] = "youtu.be|event1|events|event1|HTC-RDJ on YouTube";
exitURLs[7] = "htc-connect.com|event1|events|event1|htc-connect.com";

// split the array out to be a multidimensional array since we use it split below
for ( i = 0; i < exitURLs.length; i++) {
	exitURLs[i] = exitURLs[i].split("|");
}

// reminder, length will be 1 higher than the last index, Array(num+1) - [num]
var exitURLsToIgnore = new Array(51);

exitURLsToIgnore[0] = "youku.com";
exitURLsToIgnore[1] = "htctime.com";
exitURLsToIgnore[2] = "weibo.com";
exitURLsToIgnore[3] = "dropbox.com";
exitURLsToIgnore[4] = "renren.com";
exitURLsToIgnore[5] = "219.80.249.25";
exitURLsToIgnore[6] = "p_htc.aspx";
exitURLsToIgnore[7] = "t.qq.com";
exitURLsToIgnore[8] = "dropbox.com";
exitURLsToIgnore[9] = "htcbeats.nl";
exitURLsToIgnore[10] = "taleo.net";
exitURLsToIgnore[11] = "htc-recommended-by.ru";
exitURLsToIgnore[12] = "/cn/smartphones";
exitURLsToIgnore[13] = "adobe.com";
exitURLsToIgnore[14] = ".htc.com";
exitURLsToIgnore[15] = ".htcaccessories.";
exitURLsToIgnore[16] = ".htcinternational.nl";
exitURLsToIgnore[17] = "icims.com";
exitURLsToIgnore[18] = "htcsense.";
exitURLsToIgnore[19] = "htc.mapintime.com";
exitURLsToIgnore[20] = "htc.papago.com.cn";
exitURLsToIgnore[21] = "microsoft.com/downloads";
exitURLsToIgnore[22] = "apple.com/quicktime";
exitURLsToIgnore[23] = "pro-service.su";
exitURLsToIgnore[24] = "chacha.com";
exitURLsToIgnore[25] = "google";
exitURLsToIgnore[26] = "ustream.tv";
exitURLsToIgnore[27] = "htceurope";
exitURLsToIgnore[28] = "twitter.com";
exitURLsToIgnore[29] = "facebook.com";
exitURLsToIgnore[30] = "htchange.";
exitURLsToIgnore[31] = "adcash.com";
exitURLsToIgnore[32] = "expansys.com";
exitURLsToIgnore[33] = "dhl.co.uk";

// Evidon URL's
exitURLsToIgnore[34] = "betrad";
exitURLsToIgnore[35] = "evidon";

//Adhoc links to block (mostly bit.ly specific links)
exitURLsToIgnore[36] = "techradar.com";
exitURLsToIgnore[37] = "sketchfab.com";
exitURLsToIgnore[38] = "steampowered.com";
exitURLsToIgnore[39] = "htcvr.com";

exitURLsToIgnore[40] = "bit.ly/1FHsJvU";
exitURLsToIgnore[41] = "bit.ly/1e7ZQ3M";
exitURLsToIgnore[42] = "bit.ly/1eoFcar";
exitURLsToIgnore[43] = "bit.ly/1iqaVZO";

exitURLsToIgnore[44] = "http://bit.ly/1hIisIm";
exitURLsToIgnore[45] = "http://bit.ly/1eEOq8m";
exitURLsToIgnore[46] = "htcnews.no";
exitURLsToIgnore[47] = "etmgup.com";
exitURLsToIgnore[48] = "recamera.com";
exitURLsToIgnore[49] = "qq.com";
exitURLsToIgnore[50] = "securecheckout.billmelater.com";



// END Exit Link Setup

/*
 * Setup Omniture
 */

var s = s_gi(s_account);
s.debugTracking = false;
s.charSet = "UTF-8";

/* Link and ClickMap tracking */
s.trackDownloadLinks = true;
s.trackExternalLinks = true;
s.trackInlineStats = true;
s.linkDownloadFileTypes = "exe,zip,wav,mp3,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx,dmg";
s.linkInternalFilters = "javascript:,razorfishsf.com,htc.com,12.130,63.241.29.4,localhost,htcsense.com,mailto,taleo.net,officialhtcstore.com";
s.linkLeaveQueryString = false;
s.linkTrackVars = "None";
s.linkTrackEvents = "None";

/* DynamicObjectIDs config */
/*
function s_getObjectID(o) {
	var ID = o.href;
	return ID;
}

s.getObjectID = s_getObjectID;
*/

/* Page Name Plugin Config */
s.siteID = "";
// leftmost value in pagename
s.pageName = "";
// reset pagename
s.defaultPage = "index";
// filename to add when none exists
s.queryVarsList = "";
// query parameters to keep
s.pathExcludeDelim = ";";
// portion of the path to exclude
s.pathConcatDelim = ": ";
// page name component separator
s.pathExcludeList = "";
// elements to exclude from the path
s.usePlugins = true;

// END Setup Omniture

function s_doPlugins(s) {

//if(browserVersion.indexOf("old")==-1)
//{
	var loadTime = s_getLoadTime();
	loadTime = loadTime/10;

	s.eVar53=s.prop53=loadTime;
	if(s_getLoadTime())s.events=s.apl(s.events,'event53='+loadTime,',',1);

	/* To setup Dynamic Object IDs */
	//s.setupDynamicObjectIDs();

	s.prop1 = s.getNewRepeat();
	s.eVar1 = "D=c1";
	//sets eVar1 to prop1
	s.prop2 = s.getVisitNum();
	s.eVar2 = "D=c2";
	//sets eVar2 to prop2
	s.events = s.apl(s.events, "event1", ",", 1);
	s.eVar8 = "D=pageName";

	//File Download tracking
	try {
		s.durl = s.downloadLinkHandler();
	} catch (e) {
		s.durl = "";
	}

    if (s.durl) {
        //Track FileName
        s.eVar35 = s.durl;
        s.linkTrackVars = "eVar35";
    }

	/*
	 * Fire Custom page load data, set fireCustomEvents to false after
	 */

	if (fireCustomEvents) {

		switch (customFlag) {
			case 'prodDetailEvents':
				s.events += customEvents;
				s.products = customValue;
				s.eVar20 = s.products;
				break;
			case 'supportDetailEvents':
				s.prop35 = customValue;
				break;
			case 'arbProducts':
				s.products = customValue;
				break;
			default:
				break;
		}

		fireCustomEvents = false;
	}

	//sets pageName to eVar7
	s.prop13 = "D=g";
	//sets prop24 to current URL
	s.eVar13 = "D=g";
	//sets eVar24 to current URL
	s.prop4 = 'D=c9+": "+pageName';
	//concatenates language (prop9) with pageName
	s.prop28 = s.eVar28 = "D=oid";

	if (!s.campaign) {
		s.campaign = s.getQueryParam('cid,extcid,/extcid');
	}
	s.eVar4 = "D=v0";
	s.eVar5 = "D=v0";

	s.eVar16 = s.getValOnce(s.getQueryParam('icid,intcid'), 's_ev16', 5, 'm');
	//sets the Internal Campaign ID based on the ICID URL Parameter. Also intcid for /us/
	//getValOnce guarantees that the same value isn't called more than once per 5 minutes to handle the URL Parameter surviving through a tab click and multiple tag calls on a page - ie: product detail page

	s.eVar12 = s.getPreviousValue(s.pageName, 'gpv_pn');
	s.prop12 = "D=v12";

	// Get search query and save as evar10, and send event9
	// override this if we are on the search page, typing in the search box
	if (!searchPageEvent) {
		if (s.getQueryParam('q')) {
			s.eVar10 = "KEYWORD: " + s.getQueryParam('q');
			s.events = s.apl(s.events, "event9", ",", 1);
			s.prop10 = "D=v10";
		}
	} else {
		searchPageEvent = false;

	}

	// Calculate the time and save it in prop3
	calculateTime();

	/*
	 * Exit URL Handling
	 */

	try {
		s.exitURL = s.exitLinkHandler();
	} catch (e) {
		s.exitURL = "";
	}

	if (s.exitURL) {
		handleExitURL();
	}
}

s.doPlugins = s_doPlugins;

/*
 * Helper function for doPlugins, caluclates the time.
 */
function calculateTime() {
	/*
	 * Date/Weekday array to manage time variables in omniture
	 */

	var weekday = new Array(7);
	for ( i = 0; i < weekday.length; ++i) {
		weekday[i] = new Array(2);
	}

	weekday[0][0] = "Sunday";
	weekday[0][1] = "Weekend";
	weekday[1][0] = "Monday";
	weekday[1][1] = "Weekday";
	weekday[2][0] = "Tuesday";
	weekday[2][1] = "Weekday";
	weekday[3][0] = "Wednesday";
	weekday[3][1] = "Weekday";
	weekday[4][0] = "Thursday";
	weekday[4][1] = "Weekday";
	weekday[5][0] = "Friday";
	weekday[5][1] = "Weekday";
	weekday[6][0] = "Saturday";
	weekday[6][1] = "Weekend";

	//Determine the time stamp in blocks of 30 minutes
	var TSoutput = "";
	var currentTime = new Date();
	var hours = currentTime.getHours();
	var minutes = currentTime.getMinutes();
	var meridianState = "AM";
	if (minutes < 30) {
		minutes = "00";
	} else {
		minutes = "30";
	}
	if (hours > 11) {
		meridianState = "PM";
	}
	if (hours > 12) {
		hours = hours - 12;
	}
	TSoutput += (hours + ":" + minutes + " " + meridianState);

	var tempday = weekday[currentTime.getDay()][0];
	var tempweek = weekday[currentTime.getDay()][1];

	if (TSoutput) {
		s.prop3 = tempweek + " : " + tempday + " : " + TSoutput;
		s.eVar3 = "D=c3";
	}
}

// END Calculate time function

/*
 * Handle Exit Links
 */
function handleExitURL() {
	//set default exitIndex to zero
	var exitLinkIndex = -1;
	var exitIgnoreIndex = -1;
	// check each array index [0]th element (the url to see if it is inside of the s.exitURL)
	for ( i = 0; i < exitURLs.length; i++) {
		if (s.exitURL.match(exitURLs[i][0])) {
			exitLinkIndex = i;
		}
	}

	//check if s.exitURL is in exitURLsToIgnore array and identify the index (i)
	for ( i = 0; i < exitURLsToIgnore.length; i++) {
		if (s.exitURL.match(exitURLsToIgnore[i])) {
			exitIgnoreIndex = i;
		}
	}

	// check if the exit URL was on the watched list
	if (exitLinkIndex !== -1) {
		//set the outgoing exit variables using the temp array
		s.events = exitURLs[exitLinkIndex][1];
		s.linkTrackVars = exitURLs[exitLinkIndex][2];
		s.linkTrackEvents = exitURLs[exitLinkIndex][3];
		s.linkName = exitURLs[exitLinkIndex][4];
		//youtube
		if (exitLinkIndex == 5) { 
			s.linkName = s.exitURL;
		}
	} else if (exitIgnoreIndex !== -1 || document.URL.indexOf("us/go/htc-buzz") > -1) {

		// fire normal exit for any link on the ignore list
		s.events = "event1";
		s.linkTrackVars = "events";
		s.linkTrackEvents = "event1";
		s.linkName = "Ignored: " + s.exitURL;
	} else {

		// fire buy exit for any link not on the ignore array
		s.linkTrackVars = "prop30,eVar30";
		s.events = "event1,event30";
		s.linkTrackEvents = "event1,event30";
		s.prop30 = "Buy Exit";
		s.eVar30 = "Buy Exit";
		s.linkName = "Buy Exit (Default): " + s.exitURL;

		// Fire default exit link covario pixel
		//dla.CV.buyExit("default");
	}
}

// END Exit Link Handlers

/*
 * Plugin: s_getCartOpen
 */
s.getCartOpen=new Function("c",""
+"var s=this,t=new Date,e=s.events?s.events:'',i=0;t.setTime(t.getTim"
+"e()+1800000);if(s.c_r(c)||e.indexOf('scOpen')>-1){if(!s.c_w(c,1,t))"
+"{s.c_w(c,1,0)}}else{if(e.indexOf('scAdd')>-1){if(s.c_w(c,1,t)){i=1}"
+"else if(s.c_w(c,1,0)){i=1}}}if(i){e=e+',scOpen'}return e");

/*
 * Plugin: s_resetGetCartOpen
 */
s.resetGetCartOpen=new Function(""
+"var s=this,t=new Date,e=s.events?s.events:'';t.setTime(t.getTime()+"
+"10000);if(e.indexOf('purchase')>-1){if(s.c_r('s_scOpen')||e.indexOf"
+"('scOpen')>-1){if(!s.c_w('s_scOpen','',t)){s.c_w('s_scOpen','',0);}"
+"}}return e");


/*
 * Plugin: getValOnce_v1.1
 */
s.getValOnce = new Function("v", "c", "e", "t", "" + "var s=this,a=new Date,v=v?v:'',c=c?c:'s_gvo',e=e?e:0,i=t=='m'?6000" + "0:86400000;k=s.c_r(c);if(v){a.setTime(a.getTime()+e*i);s.c_w(c,v,e" + "==0?0:a);}return v==k?'':v");

/*
 * Plugin: getPageName v2.1 - parse URL and return
 */
s.getPageName = new Function("u", "" + "var s=this,v=u?u:''+s.wd.location,x=v.indexOf(':'),y=v.indexOf('/'," + "x+4),z=v.indexOf('?'),c=s.pathConcatDelim,e=s.pathExcludeDelim,g=s." + "queryVarsList,d=s.siteID,n=d?d:'',q=z<0?'':v.substring(z+1),p=v.sub" + "string(y+1,q?z:v.length);z=p.indexOf('#');p=z<0?p:s.fl(p,z);x=e?p.i" + "ndexOf(e):-1;p=x<0?p:s.fl(p,x);p+=!p||p.charAt(p.length-1)=='/'?s.d" + "efaultPage:'';y=c?c:'/';while(p){x=p.indexOf('/');x=x<0?p.length:x;" + "z=s.fl(p,x);if(!s.pt(s.pathExcludeList,',','p_c',z))n+=n?y+z:z;p=p." + "substring(x+1)}y=c?c:'?';while(g){x=g.indexOf(',');x=x<0?g.length:x" + ";z=s.fl(g,x);z=s.pt(q,'&','p_c',z);if(z){n+=n?y+z:z;y=c?c:'&'}g=g.s" + "ubstring(x+1)}return n");

/* DEUTSCH: This is used for eVar12
 * Plugin: getPreviousValue_v1.0 - return previous value of designated
 *   variable (requires split utility)
 */
s.getPreviousValue = new Function("v", "c", "el", "" + "var s=this,t=new Date,i,j,r='';t.setTime(t.getTime()+1800000);if(el" + "){if(s.events){i=s.split(el,',');j=s.split(s.events,',');for(x in i" + "){for(y in j){if(i[x]==j[y]){if(s.c_r(c)) r=s.c_r(c);v?s.c_w(c,v,t)" + ":s.c_w(c,'no value',t);return r}}}}}else{if(s.c_r(c)) r=s.c_r(c);v?" + "s.c_w(c,v,t):s.c_w(c,'no value',t);return r}");

/* DEUTSCH: This plugin does not seem to be used at all
 * Utility: inList v1.0 - find out if a value is in a list
 */
s.inList = new Function("v", "l", "d", "" + "var s=this,ar=Array(),i=0,d=(d)?d:',';if(typeof(l)=='string'){if(s." + "split)ar=s.split(l,d);else if(l.split)ar=l.split(d);else return-1}e" + "lse ar=l;while(i<ar.length){if(v==ar[i])return true;i++}return fals" + "e;");


/*DEUTSCH: used to for file download tracking
/*
 * Plugin: downloadLinkHandler 0.7 - identify and report download links
 */
s.downloadLinkHandler=new Function("p","o",""
+"var s=this,h=s.p_gh(),n='linkDownloadFileTypes',i,t;if(!h||(s.linkT"
+"ype&&(h||s.linkName)))return'';i=h.href.indexOf('?');t=s[n];s[n]=p?"
+"p:t;if(s.lt(h.href)=='d')s.linkType='d';else h='';s[n]=t;return o?h"
+":h.href;");

/*DEUTSCH: used to for site exits and buy now exits
 * Plugin: exitLinkHandler 0.7 - identify and report exit links
 */
s.exitLinkHandler = new Function("p", "o", "" + "var s=this,h=s.p_gh(),n='linkInternalFilters',i,t;if(!h||(s.linkTyp" + "e&&(h||s.linkName)))return'';i=h.href.indexOf('?');t=s[n];s[n]=p?p:" + "t;h.ref=s.linkLeaveQueryString||i<0?h.href:h.href.substring(0,i);if" + "(s.lt(h.href)=='e')s.linkType='e';else h='';s[n]=t;return o?h:h.hre" + "f;");
s.p_gh = new Function("", "" + "var s=this;if(!s.eo&&!s.lnk)return'';var o=s.eo?s.eo:s.lnk,y=s.ot(o" + "),n=s.oid(o),x=o.s_oidt;if(s.eo&&o==s.eo){while(o&&!n&&y!='BODY'){o" + "=o.parentElement?o.parentElement:o.parentNode;if(!o)return'';y=s.ot" + "(o);n=s.oid(o);x=o.s_oidt;}}return o?o:'';");

/* DEUTSCH: This plugin does not seem to be used at all
 * Plugin: detectRIA v0.1 - detect and set Flash, Silverlight versions
 */
s.detectRIA = new Function("cn", "fp", "sp", "mfv", "msv", "sf", "" + "cn=cn?cn:'s_ria';msv=msv?msv:2;mfv=mfv?mfv:10;var s=this,sv='',fv=-" + "1,dwi=0,fr='',sr='',w,mt=s.n.mimeTypes,uk=s.c_r(cn),k=s.c_w('s_cc'," + "'true',0)?'Y':'N';fk=uk.substring(0,uk.indexOf('|'));sk=uk.substrin" + "g(uk.indexOf('|')+1,uk.length);if(k=='Y'&&s.p_fo('detectRIA')){if(u" + "k&&!sf){if(fp){s[fp]=fk;}if(sp){s[sp]=sk;}return false;}if(!fk&&fp)" + "{if(s.pl&&s.pl.length){if(s.pl['Shockwave Flash 2.0'])fv=2;x=s.pl['" + "Shockwave Flash'];if(x){fv=0;z=x.description;if(z)fv=z.substring(16" + ",z.indexOf('.'));}}else if(navigator.plugins&&navigator.plugins.len" + "gth){x=navigator.plugins['Shockwave Flash'];if(x){fv=0;z=x.descript" + "ion;if(z)fv=z.substring(16,z.indexOf('.'));}}else if(mt&&mt.length)" + "{x=mt['application/x-shockwave-flash'];if(x&&x.enabledPlugin)fv=0;}" + "if(fv<=0)dwi=1;w=s.u.indexOf('Win')!=-1?1:0;if(dwi&&s.isie&&w&&exec" + "Script){result=false;for(var i=mfv;i>=3&&result!=true;i--){execScri" + "pt('on error resume next: result = IsObject(CreateObject(\"Shockwav" + "eFlash.ShockwaveFlash.'+i+'\"))','VBScript');fv=i;}}fr=fv==-1?'flas" + "h not detected':fv==0?'flash enabled (no version)':'flash '+fv;}if(" + "!sk&&sp&&s.apv>=4.1){var tc='try{x=new ActiveXObject(\"AgControl.A'" + "+'gControl\");for(var i=msv;i>0;i--){for(var j=9;j>=0;j--){if(x.is'" + "+'VersionSupported(i+\".\"+j)){sv=i+\".\"+j;break;}}if(sv){break;}'" + "+'}}catch(e){try{x=navigator.plugins[\"Silverlight Plug-In\"];sv=x'" + "+'.description.substring(0,x.description.indexOf(\".\")+2);}catch('" + "+'e){}}';eval(tc);sr=sv==''?'silverlight not detected':'silverlight" + " '+sv;}if((fr&&fp)||(sr&&sp)){s.c_w(cn,fr+'|'+sr,0);if(fr)s[fp]=fr;" + "if(sr)s[sp]=sr;}}");
s.p_fo = new Function("n", "" + "var s=this;if(!s.__fo){s.__fo=new Object;}if(!s.__fo[n]){s.__fo[n]=" + "new Object;return 1;}else {return 0;}");

/*DEUTSCH: Used for other plugins, should stay
 * Plugin Utility: apl v1.1
 */
s.apl = new Function("l", "v", "d", "u", "" + "var s=this,m=0;if(!l)l='';if(u){var i,n,a=s.split(l,d);for(i=0;i<a." + "length;i++){n=a[i];m=m||(u==1?(n==v):(n.toLowerCase()==v.toLowerCas" + "e()));}}if(!m)l=l?l+d+v:v;return l");

s.repl = new Function("x", "o", "n", "" + "var i=x.indexOf(o),l=n.length;while(x&&i>=0){x=x.substring(0,i)+n+x." + "substring(i+o.length);i=x.indexOf(o,i+l)}return x");

s.join = new Function("v", "p", "" + "var s = this;var f,b,d,w;if(p){f=p.front?p.front:'';b=p.back?p.back" + ":'';d=p.delim?p.delim:'';w=p.wrap?p.wrap:'';}var str='';for(var x=0" + ";x<v.length;x++){if(typeof(v[x])=='object' )str+=s.join( v[x],p);el" + "se str+=w+v[x]+w;if(x<v.length-1)str+=d;}return f+str+b;");

/*DEUTSCH:Used for Campaign identidication amongst other things
 * Plugin: getQueryParam 2.3
 */
s.getQueryParam = new Function("p", "d", "u", "" + "var s=this,v='',i,t;d=d?d:'';u=u?u:(s.pageURL?s.pageURL:s.wd.locati" + "on);if(u=='f')u=s.gtfs().location;while(p){i=p.indexOf(',');i=i<0?p" + ".length:i;t=s.p_gpv(p.substring(0,i),u+'');if(t){t=t.indexOf('#')>-" + "1?t.substring(0,t.indexOf('#')):t;}if(t)v+=v?d+t:t;p=p.substring(i=" + "=p.length?i:i+1)}return v");
s.p_gpv = new Function("k", "u", "" + "var s=this,v='',i=u.indexOf('?'),q;if(k&&i>-1){q=u.substring(i+1);v" + "=s.pt(q,'&','p_gvf',k)}return v");
s.p_gvf = new Function("t", "k", "" + "if(t){var s=this,i=t.indexOf('='),p=i<0?t:t.substring(0,i),v=i<0?'T" + "rue':t.substring(i+1);if(p.toLowerCase()==k.toLowerCase())return s." + "epa(v)}return ''");

/*DEUTSCH: used for prop1
 * Plugin: getNewRepeat 1.2 - Returns whether user is new or repeat
 */
s.getNewRepeat = new Function("d", "cn", "" + "var s=this,e=new Date(),cval,sval,ct=e.getTime();d=d?d:30;cn=cn?cn:" + "'s_nr';e.setTime(ct+d*24*60*60*1000);cval=s.c_r(cn);if(cval.length=" + "=0){s.c_w(cn,ct+'-New',e);return'New';}sval=s.split(cval,'-');if(ct" + "-sval[0]<30*60*1000&&sval[1]=='New'){s.c_w(cn,ct+'-New',e);return'N" + "ew';}else{s.c_w(cn,ct+'-Repeat',e);return'Repeat';}");

/*DEUTSCH: useful for other plugins
 * Utility Function: split v1.5 - split a string (JS 1.0 compatible)
 */
s.split = new Function("l", "d", "" + "var i,x=0,a=new Array;while(l){i=l.indexOf(d);i=i>-1?i:l.length;a[x" + "++]=l.substring(0,i);l=l.substring(i+d.length);}return a");

/*DEUTSCH: used for prop2
 * Plugin: Visit Number By Month 2.0 - Return the user visit number
 */
s.getVisitNum = new Function("" + "var s=this,e=new Date(),cval,cvisit,ct=e.getTime(),c='s_vnum',c2='s" + "_invisit';e.setTime(ct+30*24*60*60*1000);cval=s.c_r(c);if(cval){var" + " i=cval.indexOf('&vn='),str=cval.substring(i+4,cval.length),k;}cvis" + "it=s.c_r(c2);if(cvisit){if(str){e.setTime(ct+30*60*1000);s.c_w(c2,'" + "true',e);return str;}else return 'unknown visit number';}else{if(st" + "r){str++;k=cval.substring(0,i);e.setTime(k);s.c_w(c,k+'&vn='+str,e)" + ";e.setTime(ct+30*60*1000);s.c_w(c2,'true',e);return str;}else{s.c_w" + "(c,ct+30*24*60*60*1000+'&vn=1',e);e.setTime(ct+30*60*1000);s.c_w(c2" + ",'true',e);return 1;}}");

/*DEUTSCH: see about removing as it can hinder download linktracking
 * DynamicObjectIDs
 */
/*
 s.setupDynamicObjectIDs = new Function("" + "var s=this;if(!s.doi){s.doi=1;if(s.apv>3&&(!s.isie||!s.ismac||s.apv" + ">=5)){if(s.wd.attachEvent)s.wd.attachEvent('onload',s.setOIDs);else" + " if(s.wd.addEventListener)s.wd.addEventListener('load',s.setOIDs,fa" + "lse);else{s.doiol=s.wd.onload;s.wd.onload=s.setOIDs}}s.wd.s_semapho" + "re=1}");
s.setOIDs = new Function("e", "" + "var s=s_c_il[" + s._in + "],b=s.eh(s.wd,'onload'),o='onclick',x,l,u,c,i" + ",a=new Array;if(s.doiol){if(b)s[b]=s.wd[b];s.doiol(e)}if(s.d.links)" + "{for(i=0;i<s.d.links.length;i++){l=s.d.links[i];c=l[o]?''+l[o]:'';b" + "=s.eh(l,o);z=l[b]?''+l[b]:'';u=s.getObjectID(l);if(u&&c.indexOf('s_" + "objectID')<0&&z.indexOf('s_objectID')<0){u=s.repl(u,'\"','');u=s.re" + "pl(u,'\\n','').substring(0,97);l.s_oc=l[o];a[u]=a[u]?a[u]+1:1;x='';" + "if(c.indexOf('.t(')>=0||c.indexOf('.tl(')>=0||c.indexOf('s_gs(')>=0" + ")x='var x=\".tl(\";';x+='s_objectID=\"'+u+'_'+a[u]+'\";return this." + "s_oc?this.s_oc(e):true';if(s.isns&&s.apv>=5)l.setAttribute(o,x);l[o" + "]=new Function('e',x)}}}s.wd.s_semaphore=0;return true");
*/

/* RAZORFISH: Captures page load time to the tenths of a second
 * getLoadTime
 */
function s_getLoadTime(){if(!window.s_loadT){var b=new Date().getTime(),o=window.performance?performance.timing:0,a=o?o.requestStart:window.inHeadTS||0;s_loadT=a?Math.round((b-a)/100):''}return s_loadT}

/* RAZORFISH: Captures browser and browser version
 * get browser and browser version
 */
navigator.sayswho= (function(){
    var ua= navigator.userAgent, 
    N= navigator.appName, tem, 
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*([\d\.]+)/i) || [];
    M= M[2]? [M[1], M[2]]:[N, navigator.appVersion, '-?'];
    if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
    return M.join(' ');
})();
/* WARNING: Changing any of the below variables will cause drastic
 changes to how your visitor data is collected.  Changes should only be
 made when instructed to do so by your account manager.*/
s.visitorNamespace = "htc";
s.trackingServer = "metrics.htc.com";
s.trackingServerSecure = "smetrics.htc.com";
//s.visitorMigrationKey = "51366207";
//s.visitorMigrationServer = "htc.com.122.2o7.net";
//s.visitorMigrationServerSecure = "htc.com.102.122.2o7.net";


/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code='',s_objectID;function s_gi(un,pg,ss){var c="s.version='H.26.1';s.an=s_an;s.logDebug=function(m){var s=this,tcf=new Function('var e;try{console.log(\"'+s.rep(s.rep(s.rep(m,\"\\\\\",\"\\\\"
+"\\\\\"),\"\\n\",\"\\\\n\"),\"\\\"\",\"\\\\\\\"\")+'\");}catch(e){}');tcf()};s.cls=function(x,c){var i,y='';if(!c)c=this.an;for(i=0;i<x.length;i++){n=x.substring(i,i+1);if(c.indexOf(n)>=0)y+=n}retur"
+"n y};s.fl=function(x,l){return x?(''+x).substring(0,l):x};s.co=function(o){return o};s.num=function(x){x=''+x;for(var p=0;p<x.length;p++)if(('0123456789').indexOf(x.substring(p,p+1))<0)return 0;ret"
+"urn 1};s.rep=s_rep;s.sp=s_sp;s.jn=s_jn;s.ape=function(x){var s=this,h='0123456789ABCDEF',f=\"+~!*()'\",i,c=s.charSet,n,l,e,y='';c=c?c.toUpperCase():'';if(x){x=''+x;if(s.em==3){x=encodeURIComponent("
+"x);for(i=0;i<f.length;i++) {n=f.substring(i,i+1);if(x.indexOf(n)>=0)x=s.rep(x,n,\"%\"+n.charCodeAt(0).toString(16).toUpperCase())}}else if(c=='AUTO'&&('').charCodeAt){for(i=0;i<x.length;i++){c=x.su"
+"bstring(i,i+1);n=x.charCodeAt(i);if(n>127){l=0;e='';while(n||l<4){e=h.substring(n%16,n%16+1)+e;n=(n-n%16)/16;l++}y+='%u'+e}else if(c=='+')y+='%2B';else y+=escape(c)}x=y}else x=s.rep(escape(''+x),'+"
+"','%2B');if(c&&c!='AUTO'&&s.em==1&&x.indexOf('%u')<0&&x.indexOf('%U')<0){i=x.indexOf('%');while(i>=0){i++;if(h.substring(8).indexOf(x.substring(i,i+1).toUpperCase())>=0)return x.substring(0,i)+'u00"
+"'+x.substring(i);i=x.indexOf('%',i)}}}return x};s.epa=function(x){var s=this,y,tcf;if(x){x=s.rep(''+x,'+',' ');if(s.em==3){tcf=new Function('x','var y,e;try{y=decodeURIComponent(x)}catch(e){y=unesc"
+"ape(x)}return y');return tcf(x)}else return unescape(x)}return y};s.pt=function(x,d,f,a){var s=this,t=x,z=0,y,r;while(t){y=t.indexOf(d);y=y<0?t.length:y;t=t.substring(0,y);r=s[f](t,a);if(r)return r"
+";z+=y+d.length;t=x.substring(z,x.length);t=z<x.length?t:''}return ''};s.isf=function(t,a){var c=a.indexOf(':');if(c>=0)a=a.substring(0,c);c=a.indexOf('=');if(c>=0)a=a.substring(0,c);if(t.substring("
+"0,2)=='s_')t=t.substring(2);return (t!=''&&t==a)};s.fsf=function(t,a){var s=this;if(s.pt(a,',','isf',t))s.fsg+=(s.fsg!=''?',':'')+t;return 0};s.fs=function(x,f){var s=this;s.fsg='';s.pt(x,',','fsf'"
+",f);return s.fsg};s.mpc=function(m,a){var s=this,c,l,n,v;v=s.d.visibilityState;if(!v)v=s.d.webkitVisibilityState;if(v&&v=='prerender'){if(!s.mpq){s.mpq=new Array;l=s.sp('webkitvisibilitychange,visi"
+"bilitychange',',');for(n=0;n<l.length;n++){s.d.addEventListener(l[n],new Function('var s=s_c_il['+s._in+'],c,v;v=s.d.visibilityState;if(!v)v=s.d.webkitVisibilityState;if(s.mpq&&v==\"visible\"){whil"
+"e(s.mpq.length>0){c=s.mpq.shift();s[c.m].apply(s,c.a)}s.mpq=0}'),false)}}c=new Object;c.m=m;c.a=a;s.mpq.push(c);return 1}return 0};s.si=function(){var s=this,i,k,v,c=s_gi+'var s=s_gi(\"'+s.oun+'\")"
+";s.sa(\"'+s.un+'\");';for(i=0;i<s.va_g.length;i++){k=s.va_g[i];v=s[k];if(v!=undefined){if(typeof(v)!='number')c+='s.'+k+'=\"'+s_fe(v)+'\";';else c+='s.'+k+'='+v+';'}}c+=\"s.lnk=s.eo=s.linkName=s.li"
+"nkType=s.wd.s_objectID=s.ppu=s.pe=s.pev1=s.pev2=s.pev3='';\";return c};s.c_d='';s.c_gdf=function(t,a){var s=this;if(!s.num(t))return 1;return 0};s.c_gd=function(){var s=this,d=s.wd.location.hostnam"
+"e,n=s.fpCookieDomainPeriods,p;if(!n)n=s.cookieDomainPeriods;if(d&&!s.c_d){n=n?parseInt(n):2;n=n>2?n:2;p=d.lastIndexOf('.');if(p>=0){while(p>=0&&n>1){p=d.lastIndexOf('.',p-1);n--}s.c_d=p>0&&s.pt(d,'"
+".','c_gdf',0)?d.substring(p):d}}return s.c_d};s.c_r=function(k){var s=this;k=s.ape(k);var c=' '+s.d.cookie,i=c.indexOf(' '+k+'='),e=i<0?i:c.indexOf(';',i),v=i<0?'':s.epa(c.substring(i+2+k.length,e<"
+"0?c.length:e));return v!='[[B]]'?v:''};s.c_w=function(k,v,e){var s=this,d=s.c_gd(),l=s.cookieLifetime,t;v=''+v;l=l?(''+l).toUpperCase():'';if(e&&l!='SESSION'&&l!='NONE'){t=(v!=''?parseInt(l?l:0):-6"
+"0);if(t){e=new Date;e.setTime(e.getTime()+(t*1000))}}if(k&&l!='NONE'){s.d.cookie=k+'='+s.ape(v!=''?v:'[[B]]')+'; path=/;'+(e&&l!='SESSION'?' expires='+e.toGMTString()+';':'')+(d?' domain='+d+';':''"
+");return s.c_r(k)==v}return 0};s.eh=function(o,e,r,f){var s=this,b='s_'+e+'_'+s._in,n=-1,l,i,x;if(!s.ehl)s.ehl=new Array;l=s.ehl;for(i=0;i<l.length&&n<0;i++){if(l[i].o==o&&l[i].e==e)n=i}if(n<0){n=i"
+";l[n]=new Object}x=l[n];x.o=o;x.e=e;f=r?x.b:f;if(r||f){x.b=r?0:o[e];x.o[e]=f}if(x.b){x.o[b]=x.b;return b}return 0};s.cet=function(f,a,t,o,b){var s=this,r,tcf;if(s.apv>=5&&(!s.isopera||s.apv>=7)){tc"
+"f=new Function('s','f','a','t','var e,r;try{r=s[f](a)}catch(e){r=s[t](e)}return r');r=tcf(s,f,a,t)}else{if(s.ismac&&s.u.indexOf('MSIE 4')>=0)r=s[b](a);else{s.eh(s.wd,'onerror',0,o);r=s[f](a);s.eh(s"
+".wd,'onerror',1)}}return r};s.gtfset=function(e){var s=this;return s.tfs};s.gtfsoe=new Function('e','var s=s_c_il['+s._in+'],c;s.eh(window,\"onerror\",1);s.etfs=1;c=s.t();if(c)s.d.write(c);s.etfs=0"
+";return true');s.gtfsfb=function(a){return window};s.gtfsf=function(w){var s=this,p=w.parent,l=w.location;s.tfs=w;if(p&&p.location!=l&&p.location.host==l.host){s.tfs=p;return s.gtfsf(s.tfs)}return "
+"s.tfs};s.gtfs=function(){var s=this;if(!s.tfs){s.tfs=s.wd;if(!s.etfs)s.tfs=s.cet('gtfsf',s.tfs,'gtfset',s.gtfsoe,'gtfsfb')}return s.tfs};s.mrq=function(u){var s=this,l=s.rl[u],n,r;s.rl[u]=0;if(l)fo"
+"r(n=0;n<l.length;n++){r=l[n];s.mr(0,0,r.r,r.t,r.u)}};s.flushBufferedRequests=function(){};s.mr=function(sess,q,rs,ta,u){var s=this,dc=s.dc,t1=s.trackingServer,t2=s.trackingServerSecure,tb=s.trackin"
+"gServerBase,p='.sc',ns=s.visitorNamespace,un=s.cls(u?u:(ns?ns:s.fun)),r=new Object,l,imn='s_i_'+s._in+'_'+un,im,b,e;if(!rs){if(t1){if(t2&&s.ssl)t1=t2}else{if(!tb)tb='2o7.net';if(dc)dc=(''+dc).toLow"
+"erCase();else dc='d1';if(tb=='2o7.net'){if(dc=='d1')dc='112';else if(dc=='d2')dc='122';p=''}t1=un+'.'+dc+'.'+p+tb}rs='http'+(s.ssl?'s':'')+'://'+t1+'/b/ss/'+s.un+'/'+(s.mobile?'5.1':'1')+'/'+s.vers"
+"ion+(s.tcn?'T':'')+'/'+sess+'?AQB=1&ndh=1'+(q?q:'')+'&AQE=1';if(s.isie&&!s.ismac)rs=s.fl(rs,2047)}if(s.d.images&&s.apv>=3&&(!s.isopera||s.apv>=7)&&(s.ns6<0||s.apv>=6.1)){if(!s.rc)s.rc=new Object;if"
+"(!s.rc[un]){s.rc[un]=1;if(!s.rl)s.rl=new Object;s.rl[un]=new Array;setTimeout('if(window.s_c_il)window.s_c_il['+s._in+'].mrq(\"'+un+'\")',750)}else{l=s.rl[un];if(l){r.t=ta;r.u=un;r.r=rs;l[l.length]"
+"=r;return ''}imn+='_'+s.rc[un];s.rc[un]++}if(s.debugTracking){var d='AppMeasurement Debug: '+rs,dl=s.sp(rs,'&'),dln;for(dln=0;dln<dl.length;dln++)d+=\"\\n\\t\"+s.epa(dl[dln]);s.logDebug(d)}im=s.wd["
+"imn];if(!im)im=s.wd[imn]=new Image;im.s_l=0;im.onload=new Function('e','this.s_l=1;var wd=window,s;if(wd.s_c_il){s=wd.s_c_il['+s._in+'];s.bcr();s.mrq(\"'+un+'\");s.nrs--;if(!s.nrs)s.m_m(\"rr\")}');"
+"if(!s.nrs){s.nrs=1;s.m_m('rs')}else s.nrs++;im.src=rs;if(s.useForcedLinkTracking||s.bcf){if(!s.forcedLinkTrackingTimeout)s.forcedLinkTrackingTimeout=250;setTimeout('if(window.s_c_il)window.s_c_il['"
+"+s._in+'].bcr()',s.forcedLinkTrackingTimeout);}else if((s.lnk||s.eo)&&(!ta||ta=='_self'||ta=='_top'||ta=='_parent'||(s.wd.name&&ta==s.wd.name))){b=e=new Date;while(!im.s_l&&e.getTime()-b.getTime()<"
+"500)e=new Date}return ''}return '<im'+'g sr'+'c=\"'+rs+'\" width=1 height=1 border=0 alt=\"\">'};s.gg=function(v){var s=this;if(!s.wd['s_'+v])s.wd['s_'+v]='';return s.wd['s_'+v]};s.glf=function(t,a"
+"){if(t.substring(0,2)=='s_')t=t.substring(2);var s=this,v=s.gg(t);if(v)s[t]=v};s.gl=function(v){var s=this;if(s.pg)s.pt(v,',','glf',0)};s.rf=function(x){var s=this,y,i,j,h,p,l=0,q,a,b='',c='',t;if("
+"x&&x.length>255){y=''+x;i=y.indexOf('?');if(i>0){q=y.substring(i+1);y=y.substring(0,i);h=y.toLowerCase();j=0;if(h.substring(0,7)=='http://')j+=7;else if(h.substring(0,8)=='https://')j+=8;i=h.indexO"
+"f(\"/\",j);if(i>0){h=h.substring(j,i);p=y.substring(i);y=y.substring(0,i);if(h.indexOf('google')>=0)l=',q,ie,start,search_key,word,kw,cd,';else if(h.indexOf('yahoo.co')>=0)l=',p,ei,';if(l&&q){a=s.s"
+"p(q,'&');if(a&&a.length>1){for(j=0;j<a.length;j++){t=a[j];i=t.indexOf('=');if(i>0&&l.indexOf(','+t.substring(0,i)+',')>=0)b+=(b?'&':'')+t;else c+=(c?'&':'')+t}if(b&&c)q=b+'&'+c;else c=''}i=253-(q.l"
+"ength-c.length)-y.length;x=y+(i>0?p.substring(0,i):'')+'?'+q}}}}return x};s.s2q=function(k,v,vf,vfp,f){var s=this,qs='',sk,sv,sp,ss,nke,nk,nf,nfl=0,nfn,nfm;if(k==\"contextData\")k=\"c\";if(v){for(s"
+"k in v)if((!f||sk.substring(0,f.length)==f)&&v[sk]&&(!vf||vf.indexOf(','+(vfp?vfp+'.':'')+sk+',')>=0)&&(!Object||!Object.prototype||!Object.prototype[sk])){nfm=0;if(nfl)for(nfn=0;nfn<nfl.length;nfn"
+"++)if(sk.substring(0,nfl[nfn].length)==nfl[nfn])nfm=1;if(!nfm){if(qs=='')qs+='&'+k+'.';sv=v[sk];if(f)sk=sk.substring(f.length);if(sk.length>0){nke=sk.indexOf('.');if(nke>0){nk=sk.substring(0,nke);n"
+"f=(f?f:'')+nk+'.';if(!nfl)nfl=new Array;nfl[nfl.length]=nf;qs+=s.s2q(nk,v,vf,vfp,nf)}else{if(typeof(sv)=='boolean'){if(sv)sv='true';else sv='false'}if(sv){if(vfp=='retrieveLightData'&&f.indexOf('.c"
+"ontextData.')<0){sp=sk.substring(0,4);ss=sk.substring(4);if(sk=='transactionID')sk='xact';else if(sk=='channel')sk='ch';else if(sk=='campaign')sk='v0';else if(s.num(ss)){if(sp=='prop')sk='c'+ss;els"
+"e if(sp=='eVar')sk='v'+ss;else if(sp=='list')sk='l'+ss;else if(sp=='hier'){sk='h'+ss;sv=sv.substring(0,255)}}}qs+='&'+s.ape(sk)+'='+s.ape(sv)}}}}}if(qs!='')qs+='&.'+k}return qs};s.hav=function(){va"
+"r s=this,qs='',l,fv='',fe='',mn,i,e;if(s.lightProfileID){l=s.va_m;fv=s.lightTrackVars;if(fv)fv=','+fv+','+s.vl_mr+','}else{l=s.va_t;if(s.pe||s.linkType){fv=s.linkTrackVars;fe=s.linkTrackEvents;if(s"
+".pe){mn=s.pe.substring(0,1).toUpperCase()+s.pe.substring(1);if(s[mn]){fv=s[mn].trackVars;fe=s[mn].trackEvents}}}if(fv)fv=','+fv+','+s.vl_l+','+s.vl_l2;if(fe){fe=','+fe+',';if(fv)fv+=',events,'}if ("
+"s.events2)e=(e?',':'')+s.events2}for(i=0;i<l.length;i++){var k=l[i],v=s[k],b=k.substring(0,4),x=k.substring(4),n=parseInt(x),q=k;if(!v)if(k=='events'&&e){v=e;e=''}if(v&&(!fv||fv.indexOf(','+k+',')>"
+"=0)&&k!='linkName'&&k!='linkType'){if(k=='timestamp')q='ts';else if(k=='dynamicVariablePrefix')q='D';else if(k=='visitorID')q='vid';else if(k=='pageURL'){q='g';if(v.length>255){s.pageURLRest=v.subs"
+"tring(255);v=v.substring(0,255);}}else if(k=='pageURLRest')q='-g';else if(k=='referrer'){q='r';v=s.fl(s.rf(v),255)}else if(k=='vmk'||k=='visitorMigrationKey')q='vmt';else if(k=='visitorMigrationSer"
+"ver'){q='vmf';if(s.ssl&&s.visitorMigrationServerSecure)v=''}else if(k=='visitorMigrationServerSecure'){q='vmf';if(!s.ssl&&s.visitorMigrationServer)v=''}else if(k=='charSet'){q='ce';if(v.toUpperCase"
+"()=='AUTO')v='ISO8859-1';else if(s.em==2||s.em==3)v='UTF-8'}else if(k=='visitorNamespace')q='ns';else if(k=='cookieDomainPeriods')q='cdp';else if(k=='cookieLifetime')q='cl';else if(k=='variableProv"
+"ider')q='vvp';else if(k=='currencyCode')q='cc';else if(k=='channel')q='ch';else if(k=='transactionID')q='xact';else if(k=='campaign')q='v0';else if(k=='resolution')q='s';else if(k=='colorDepth')q='"
+"c';else if(k=='javascriptVersion')q='j';else if(k=='javaEnabled')q='v';else if(k=='cookiesEnabled')q='k';else if(k=='browserWidth')q='bw';else if(k=='browserHeight')q='bh';else if(k=='connectionTyp"
+"e')q='ct';else if(k=='homepage')q='hp';else if(k=='plugins')q='p';else if(k=='events'){if(e)v+=(v?',':'')+e;if(fe)v=s.fs(v,fe)}else if(k=='events2')v='';else if(k=='contextData'){qs+=s.s2q('c',s[k]"
+",fv,k,0);v=''}else if(k=='lightProfileID')q='mtp';else if(k=='lightStoreForSeconds'){q='mtss';if(!s.lightProfileID)v=''}else if(k=='lightIncrementBy'){q='mti';if(!s.lightProfileID)v=''}else if(k=='"
+"retrieveLightProfiles')q='mtsr';else if(k=='deleteLightProfiles')q='mtsd';else if(k=='retrieveLightData'){if(s.retrieveLightProfiles)qs+=s.s2q('mts',s[k],fv,k,0);v=''}else if(s.num(x)){if(b=='prop'"
+")q='c'+n;else if(b=='eVar')q='v'+n;else if(b=='list')q='l'+n;else if(b=='hier'){q='h'+n;v=s.fl(v,255)}}if(v)qs+='&'+s.ape(q)+'='+(k.substring(0,3)!='pev'?s.ape(v):v)}}return qs};s.ltdf=function(t,h"
+"){t=t?t.toLowerCase():'';h=h?h.toLowerCase():'';var qi=h.indexOf('?'),hi=h.indexOf('#');if(qi>=0){if(hi>=0&&hi<qi)qi=hi;}else qi=hi;h=qi>=0?h.substring(0,qi):h;if(t&&h.substring(h.length-(t.length+"
+"1))=='.'+t)return 1;return 0};s.ltef=function(t,h){t=t?t.toLowerCase():'';h=h?h.toLowerCase():'';if(t&&h.indexOf(t)>=0)return 1;return 0};s.lt=function(h){var s=this,lft=s.linkDownloadFileTypes,lef"
+"=s.linkExternalFilters,lif=s.linkInternalFilters;lif=lif?lif:s.wd.location.hostname;h=h.toLowerCase();if(s.trackDownloadLinks&&lft&&s.pt(lft,',','ltdf',h))return 'd';if(s.trackExternalLinks&&h.inde"
+"xOf('#')!=0&&h.indexOf('about:')!=0&&h.indexOf('javascript:')!=0&&(lef||lif)&&(!lef||s.pt(lef,',','ltef',h))&&(!lif||!s.pt(lif,',','ltef',h)))return 'e';return ''};s.lc=new Function('e','var s=s_c_"
+"il['+s._in+'],b=s.eh(this,\"onclick\");s.lnk=this;s.t();s.lnk=0;if(b)return this[b](e);return true');s.bcr=function(){var s=this;if(s.bct&&s.bce)s.bct.dispatchEvent(s.bce);if(s.bcf){if(typeof(s.bcf"
+")=='function')s.bcf();else if(s.bct&&s.bct.href)s.d.location=s.bct.href}s.bct=s.bce=s.bcf=0};s.bc=new Function('e','if(e&&e.s_fe)return;var s=s_c_il['+s._in+'],f,tcf,t,n,nrs,a,h;if(s.d&&s.d.all&&s."
+"d.all.cppXYctnr)return;if(!s.bbc)s.useForcedLinkTracking=0;else if(!s.useForcedLinkTracking){s.b.removeEventListener(\"click\",s.bc,true);s.bbc=s.useForcedLinkTracking=0;return}else s.b.removeEvent"
+"Listener(\"click\",s.bc,false);s.eo=e.srcElement?e.srcElement:e.target;nrs=s.nrs;s.t();s.eo=0;if(s.nrs>nrs&&s.useForcedLinkTracking&&e.target){a=e.target;while(a&&a!=s.b&&a.tagName.toUpperCase()!="
+"\"A\"&&a.tagName.toUpperCase()!=\"AREA\")a=a.parentNode;if(a){h=a.href;if(h.indexOf(\"#\")==0||h.indexOf(\"about:\")==0||h.indexOf(\"javascript:\")==0)h=0;t=a.target;if(e.target.dispatchEvent&&h&&("
+"!t||t==\"_self\"||t==\"_top\"||t==\"_parent\"||(s.wd.name&&t==s.wd.name))){tcf=new Function(\"s\",\"var x;try{n=s.d.createEvent(\\\\\"MouseEvents\\\\\")}catch(x){n=new MouseEvent}return n\");n=tcf("
+"s);if(n){tcf=new Function(\"n\",\"e\",\"var x;try{n.initMouseEvent(\\\\\"click\\\\\",e.bubbles,e.cancelable,e.view,e.detail,e.screenX,e.screenY,e.clientX,e.clientY,e.ctrlKey,e.altKey,e.shiftKey,e.m"
+"etaKey,e.button,e.relatedTarget)}catch(x){n=0}return n\");n=tcf(n,e);if(n){n.s_fe=1;e.stopPropagation();if (e.stopImmediatePropagation) {e.stopImmediatePropagation();}e.preventDefault();s.bct=e.tar"
+"get;s.bce=n}}}}}');s.oh=function(o){var s=this,l=s.wd.location,h=o.href?o.href:'',i,j,k,p;i=h.indexOf(':');j=h.indexOf('?');k=h.indexOf('/');if(h&&(i<0||(j>=0&&i>j)||(k>=0&&i>k))){p=o.protocol&&o.p"
+"rotocol.length>1?o.protocol:(l.protocol?l.protocol:'');i=l.pathname.lastIndexOf('/');h=(p?p+'//':'')+(o.host?o.host:(l.host?l.host:''))+(h.substring(0,1)!='/'?l.pathname.substring(0,i<0?0:i)+'/':''"
+")+h}return h};s.ot=function(o){var t=o.tagName;if(o.tagUrn||(o.scopeName&&o.scopeName.toUpperCase()!='HTML'))return '';t=t&&t.toUpperCase?t.toUpperCase():'';if(t=='SHAPE')t='';if(t){if((t=='INPUT'|"
+"|t=='BUTTON')&&o.type&&o.type.toUpperCase)t=o.type.toUpperCase();else if(!t&&o.href)t='A';}return t};s.oid=function(o){var s=this,t=s.ot(o),p,c,n='',x=0;if(t&&!o.s_oid){p=o.protocol;c=o.onclick;if("
+"o.href&&(t=='A'||t=='AREA')&&(!c||!p||p.toLowerCase().indexOf('javascript')<0))n=s.oh(o);else if(c){n=s.rep(s.rep(s.rep(s.rep(''+c,\"\\r\",''),\"\\n\",''),\"\\t\",''),' ','');x=2}else if(t=='INPUT'"
+"||t=='SUBMIT'){if(o.value)n=o.value;else if(o.innerText)n=o.innerText;else if(o.textContent)n=o.textContent;x=3}else if(o.src&&t=='IMAGE')n=o.src;if(n){o.s_oid=s.fl(n,100);o.s_oidt=x}}return o.s_oi"
+"d};s.rqf=function(t,un){var s=this,e=t.indexOf('='),u=e>=0?t.substring(0,e):'',q=e>=0?s.epa(t.substring(e+1)):'';if(u&&q&&(','+u+',').indexOf(','+un+',')>=0){if(u!=s.un&&s.un.indexOf(',')>=0)q='&u="
+"'+u+q+'&u=0';return q}return ''};s.rq=function(un){if(!un)un=this.un;var s=this,c=un.indexOf(','),v=s.c_r('s_sq'),q='';if(c<0)return s.pt(v,'&','rqf',un);return s.pt(un,',','rq',0)};s.sqp=function("
+"t,a){var s=this,e=t.indexOf('='),q=e<0?'':s.epa(t.substring(e+1));s.sqq[q]='';if(e>=0)s.pt(t.substring(0,e),',','sqs',q);return 0};s.sqs=function(un,q){var s=this;s.squ[un]=q;return 0};s.sq=functio"
+"n(q){var s=this,k='s_sq',v=s.c_r(k),x,c=0;s.sqq=new Object;s.squ=new Object;s.sqq[q]='';s.pt(v,'&','sqp',0);s.pt(s.un,',','sqs',q);v='';for(x in s.squ)if(x&&(!Object||!Object.prototype||!Object.pro"
+"totype[x]))s.sqq[s.squ[x]]+=(s.sqq[s.squ[x]]?',':'')+x;for(x in s.sqq)if(x&&(!Object||!Object.prototype||!Object.prototype[x])&&s.sqq[x]&&(x==q||c<2)){v+=(v?'&':'')+s.sqq[x]+'='+s.ape(x);c++}return"
+" s.c_w(k,v,0)};s.wdl=new Function('e','var s=s_c_il['+s._in+'],r=true,b=s.eh(s.wd,\"onload\"),i,o,oc;if(b)r=this[b](e);for(i=0;i<s.d.links.length;i++){o=s.d.links[i];oc=o.onclick?\"\"+o.onclick:\""
+"\";if((oc.indexOf(\"s_gs(\")<0||oc.indexOf(\".s_oc(\")>=0)&&oc.indexOf(\".tl(\")<0)s.eh(o,\"onclick\",0,s.lc);}return r');s.wds=function(){var s=this;if(s.apv>3&&(!s.isie||!s.ismac||s.apv>=5)){if(s"
+".b&&s.b.attachEvent)s.b.attachEvent('onclick',s.bc);else if(s.b&&s.b.addEventListener){if(s.n&&((s.n.userAgent.indexOf('WebKit')>=0&&s.d.createEvent)||(s.n.userAgent.indexOf('Firefox/2')>=0&&s.wd.M"
+"ouseEvent))){s.bbc=1;s.useForcedLinkTracking=1;s.b.addEventListener('click',s.bc,true)}s.b.addEventListener('click',s.bc,false)}else s.eh(s.wd,'onload',0,s.wdl)}};s.vs=function(x){var s=this,v=s.vi"
+"sitorSampling,g=s.visitorSamplingGroup,k='s_vsn_'+s.un+(g?'_'+g:''),n=s.c_r(k),e=new Date,y=e.getYear();e.setYear(y+10+(y<1900?1900:0));if(v){v*=100;if(!n){if(!s.c_w(k,x,e))return 0;n=x}if(n%10000>"
+"v)return 0}return 1};s.dyasmf=function(t,m){if(t&&m&&m.indexOf(t)>=0)return 1;return 0};s.dyasf=function(t,m){var s=this,i=t?t.indexOf('='):-1,n,x;if(i>=0&&m){var n=t.substring(0,i),x=t.substring(i"
+"+1);if(s.pt(x,',','dyasmf',m))return n}return 0};s.uns=function(){var s=this,x=s.dynamicAccountSelection,l=s.dynamicAccountList,m=s.dynamicAccountMatch,n,i;s.un=s.un.toLowerCase();if(x&&l){if(!m)m="
+"s.wd.location.host;if(!m.toLowerCase)m=''+m;l=l.toLowerCase();m=m.toLowerCase();n=s.pt(l,';','dyasf',m);if(n)s.un=n}i=s.un.indexOf(',');s.fun=i<0?s.un:s.un.substring(0,i)};s.sa=function(un){var s=t"
+"his;if(s.un&&s.mpc('sa',arguments))return;s.un=un;if(!s.oun)s.oun=un;else if((','+s.oun+',').indexOf(','+un+',')<0)s.oun+=','+un;s.uns()};s.m_i=function(n,a){var s=this,m,f=n.substring(0,1),r,l,i;i"
+"f(!s.m_l)s.m_l=new Object;if(!s.m_nl)s.m_nl=new Array;m=s.m_l[n];if(!a&&m&&m._e&&!m._i)s.m_a(n);if(!m){m=new Object,m._c='s_m';m._in=s.wd.s_c_in;m._il=s._il;m._il[m._in]=m;s.wd.s_c_in++;m.s=s;m._n="
+"n;m._l=new Array('_c','_in','_il','_i','_e','_d','_dl','s','n','_r','_g','_g1','_t','_t1','_x','_x1','_rs','_rr','_l');s.m_l[n]=m;s.m_nl[s.m_nl.length]=n}else if(m._r&&!m._m){r=m._r;r._m=m;l=m._l;f"
+"or(i=0;i<l.length;i++)if(m[l[i]])r[l[i]]=m[l[i]];r._il[r._in]=r;m=s.m_l[n]=r}if(f==f.toUpperCase())s[n]=m;return m};s.m_a=new Function('n','g','e','if(!g)g=\"m_\"+n;var s=s_c_il['+s._in+'],c=s[g+\""
+"_c\"],m,x,f=0;if(s.mpc(\"m_a\",arguments))return;if(!c)c=s.wd[\"s_\"+g+\"_c\"];if(c&&s_d)s[g]=new Function(\"s\",s_ft(s_d(c)));x=s[g];if(!x)x=s.wd[\\'s_\\'+g];if(!x)x=s.wd[g];m=s.m_i(n,1);if(x&&(!m"
+"._i||g!=\"m_\"+n)){m._i=f=1;if((\"\"+x).indexOf(\"function\")>=0)x(s);else s.m_m(\"x\",n,x,e)}m=s.m_i(n,1);if(m._dl)m._dl=m._d=0;s.dlt();return f');s.m_m=function(t,n,d,e){t='_'+t;var s=this,i,x,m,"
+"f='_'+t,r=0,u;if(s.m_l&&s.m_nl)for(i=0;i<s.m_nl.length;i++){x=s.m_nl[i];if(!n||x==n){m=s.m_i(x);u=m[t];if(u){if((''+u).indexOf('function')>=0){if(d&&e)u=m[t](d,e);else if(d)u=m[t](d);else u=m[t]()}"
+"}if(u)r=1;u=m[t+1];if(u&&!m[f]){if((''+u).indexOf('function')>=0){if(d&&e)u=m[t+1](d,e);else if(d)u=m[t+1](d);else u=m[t+1]()}}m[f]=1;if(u)r=1}}return r};s.m_ll=function(){var s=this,g=s.m_dl,i,o;i"
+"f(g)for(i=0;i<g.length;i++){o=g[i];if(o)s.loadModule(o.n,o.u,o.d,o.l,o.e,1);g[i]=0}};s.loadModule=function(n,u,d,l,e,ln){var s=this,m=0,i,g,o=0,f1,f2,c=s.h?s.h:s.b,b,tcf;if(n){i=n.indexOf(':');if(i"
+">=0){g=n.substring(i+1);n=n.substring(0,i)}else g=\"m_\"+n;m=s.m_i(n)}if((l||(n&&!s.m_a(n,g)))&&u&&s.d&&c&&s.d.createElement){if(d){m._d=1;m._dl=1}if(ln){if(s.ssl)u=s.rep(u,'http:','https:');i='s_s"
+":'+s._in+':'+n+':'+g;b='var s=s_c_il['+s._in+'],o=s.d.getElementById(\"'+i+'\");if(s&&o){if(!o.l&&s.wd.'+g+'){o.l=1;if(o.i)clearTimeout(o.i);o.i=0;s.m_a(\"'+n+'\",\"'+g+'\"'+(e?',\"'+e+'\"':'')+')}"
+"';f2=b+'o.c++;if(!s.maxDelay)s.maxDelay=250;if(!o.l&&o.c<(s.maxDelay*2)/100)o.i=setTimeout(o.f2,100)}';f1=new Function('e',b+'}');tcf=new Function('s','c','i','u','f1','f2','var e,o=0;try{o=s.d.cre"
+"ateElement(\"script\");if(o){o.type=\"text/javascript\";'+(n?'o.id=i;o.defer=true;o.onload=o.onreadystatechange=f1;o.f2=f2;o.l=0;':'')+'o.src=u;c.appendChild(o);'+(n?'o.c=0;o.i=setTimeout(f2,100)':"
+"'')+'}}catch(e){o=0}return o');o=tcf(s,c,i,u,f1,f2)}else{o=new Object;o.n=n+':'+g;o.u=u;o.d=d;o.l=l;o.e=e;g=s.m_dl;if(!g)g=s.m_dl=new Array;i=0;while(i<g.length&&g[i])i++;g[i]=o}}else if(n){m=s.m_i"
+"(n);m._e=1}return m};s.voa=function(vo,r){var s=this,l=s.va_g,i,k,v,x;for(i=0;i<l.length;i++){k=l[i];v=vo[k];if(v||vo['!'+k]){if(!r&&(k==\"contextData\"||k==\"retrieveLightData\")&&s[k])for(x in s["
+"k])if(!v[x])v[x]=s[k][x];s[k]=v}}};s.vob=function(vo){var s=this,l=s.va_g,i,k;for(i=0;i<l.length;i++){k=l[i];vo[k]=s[k];if(!vo[k])vo['!'+k]=1}};s.dlt=new Function('var s=s_c_il['+s._in+'],d=new Dat"
+"e,i,vo,f=0;if(s.dll)for(i=0;i<s.dll.length;i++){vo=s.dll[i];if(vo){if(!s.m_m(\"d\")||d.getTime()-vo._t>=s.maxDelay){s.dll[i]=0;s.t(vo)}else f=1}}if(s.dli)clearTimeout(s.dli);s.dli=0;if(f){if(!s.dli"
+")s.dli=setTimeout(s.dlt,s.maxDelay)}else s.dll=0');s.dl=function(vo){var s=this,d=new Date;if(!vo)vo=new Object;s.vob(vo);vo._t=d.getTime();if(!s.dll)s.dll=new Array;s.dll[s.dll.length]=vo;if(!s.ma"
+"xDelay)s.maxDelay=250;s.dlt()};s.gfid=function(){var s=this,d='0123456789ABCDEF',k='s_fid',fid=s.c_r(k),h='',l='',i,j,m=8,n=4,e=new Date,y;if(!fid||fid.indexOf('-')<0){for(i=0;i<16;i++){j=Math.floo"
+"r(Math.random()*m);h+=d.substring(j,j+1);j=Math.floor(Math.random()*n);l+=d.substring(j,j+1);m=n=16}fid=h+'-'+l;}y=e.getYear();e.setYear(y+2+(y<1900?1900:0));if(!s.c_w(k,fid,e))fid=0;return fid};s."
+"applyADMS=function(){var s=this,vb=new Object;if(s.wd.ADMS&&!s.visitorID&&!s.admsc){if(!s.adms)s.adms=ADMS.getDefault();if(!s.admsq){s.visitorID=s.adms.getVisitorID(new Function('v','var s=s_c_il['"
+"+s._in+'],l=s.admsq,i;if(v==-1)v=0;if(v)s.visitorID=v;s.admsq=0;if(l){s.admsc=1;for(i=0;i<l.length;i++)s.t(l[i]);s.admsc=0;}'));if(!s.visitorID)s.admsq=new Array}if(s.admsq){s.vob(vb);vb['!visitorI"
+"D']=0;s.admsq.push(vb);return 1}else{if(s.visitorID==-1)s.visitorID=0}}return 0};s.track=s.t=function(vo){var s=this,trk=1,tm=new Date,sed=Math&&Math.random?Math.floor(Math.random()*10000000000000)"
+":tm.getTime(),sess='s'+Math.floor(tm.getTime()/10800000)%10+sed,y=tm.getYear(),vt=tm.getDate()+'/'+tm.getMonth()+'/'+(y<1900?y+1900:y)+' '+tm.getHours()+':'+tm.getMinutes()+':'+tm.getSeconds()+' '+"
+"tm.getDay()+' '+tm.getTimezoneOffset(),tcf,tfs=s.gtfs(),ta=-1,q='',qs='',code='',vb=new Object;if(s.mpc('t',arguments))return;s.gl(s.vl_g);s.uns();s.m_ll();if(!s.td){var tl=tfs.location,a,o,i,x='',"
+"c='',v='',p='',bw='',bh='',j='1.0',k=s.c_w('s_cc','true',0)?'Y':'N',hp='',ct='',pn=0,ps;if(String&&String.prototype){j='1.1';if(j.match){j='1.2';if(tm.setUTCDate){j='1.3';if(s.isie&&s.ismac&&s.apv>"
+"=5)j='1.4';if(pn.toPrecision){j='1.5';a=new Array;if(a.forEach){j='1.6';i=0;o=new Object;tcf=new Function('o','var e,i=0;try{i=new Iterator(o)}catch(e){}return i');i=tcf(o);if(i&&i.next){j='1.7';if"
+"(a.reduce){j='1.8';if(j.trim){j='1.8.1';if(Date.parse){j='1.8.2';if(Object.create)j='1.8.5'}}}}}}}}}if(s.apv>=4)x=screen.width+'x'+screen.height;if(s.isns||s.isopera){if(s.apv>=3){v=s.n.javaEnabled"
+"()?'Y':'N';if(s.apv>=4){c=screen.pixelDepth;bw=s.wd.innerWidth;bh=s.wd.innerHeight}}s.pl=s.n.plugins}else if(s.isie){if(s.apv>=4){v=s.n.javaEnabled()?'Y':'N';c=screen.colorDepth;if(s.apv>=5){bw=s.d"
+".documentElement.offsetWidth;bh=s.d.documentElement.offsetHeight;if(!s.ismac&&s.b){tcf=new Function('s','tl','var e,hp=0;try{s.b.addBehavior(\"#default#homePage\");hp=s.b.isHomePage(tl)?\"Y\":\"N\""
+"}catch(e){}return hp');hp=tcf(s,tl);tcf=new Function('s','var e,ct=0;try{s.b.addBehavior(\"#default#clientCaps\");ct=s.b.connectionType}catch(e){}return ct');ct=tcf(s)}}}else r=''}if(s.pl)while(pn<"
+"s.pl.length&&pn<30){ps=s.fl(s.pl[pn].name,100)+';';if(p.indexOf(ps)<0)p+=ps;pn++}s.resolution=x;s.colorDepth=c;s.javascriptVersion=j;s.javaEnabled=v;s.cookiesEnabled=k;s.browserWidth=bw;s.browserHe"
+"ight=bh;s.connectionType=ct;s.homepage=hp;s.plugins=p;s.td=1}if(vo){s.vob(vb);s.voa(vo)}s.fid=s.gfid();if(s.applyADMS())return '';if((vo&&vo._t)||!s.m_m('d')){if(s.usePlugins)s.doPlugins(s);if(!s.a"
+"bort){var l=s.wd.location,r=tfs.document.referrer;if(!s.pageURL)s.pageURL=l.href?l.href:l;if(!s.referrer&&!s._1_referrer){s.referrer=r;s._1_referrer=1}s.m_m('g');if(s.lnk||s.eo){var o=s.eo?s.eo:s.l"
+"nk,p=s.pageName,w=1,t=s.ot(o),n=s.oid(o),x=o.s_oidt,h,l,i,oc;if(s.eo&&o==s.eo){while(o&&!n&&t!='BODY'){o=o.parentElement?o.parentElement:o.parentNode;if(o){t=s.ot(o);n=s.oid(o);x=o.s_oidt}}if(!n||t"
+"=='BODY')o='';if(o){oc=o.onclick?''+o.onclick:'';if((oc.indexOf('s_gs(')>=0&&oc.indexOf('.s_oc(')<0)||oc.indexOf('.tl(')>=0)o=0}}if(o){if(n)ta=o.target;h=s.oh(o);i=h.indexOf('?');h=s.linkLeaveQuery"
+"String||i<0?h:h.substring(0,i);l=s.linkName;t=s.linkType?s.linkType.toLowerCase():s.lt(h);if(t&&(h||l)){s.pe='lnk_'+(t=='d'||t=='e'?t:'o');s.pev1=(h?s.ape(h):'');s.pev2=(l?s.ape(l):'')}else trk=0;i"
+"f(s.trackInlineStats){if(!p){p=s.pageURL;w=0}t=s.ot(o);i=o.sourceIndex;if(o.dataset&&o.dataset.sObjectId){s.wd.s_objectID=o.dataset.sObjectId;}else if(o.getAttribute&&o.getAttribute('data-s-object-"
+"id')){s.wd.s_objectID=o.getAttribute('data-s-object-id');}else if(s.useForcedLinkTracking){s.wd.s_objectID='';oc=o.onclick?''+o.onclick:'';if(oc){var ocb=oc.indexOf('s_objectID'),oce,ocq,ocx;if(ocb"
+">=0){ocb+=10;while(ocb<oc.length&&(\"= \\t\\r\\n\").indexOf(oc.charAt(ocb))>=0)ocb++;if(ocb<oc.length){oce=ocb;ocq=ocx=0;while(oce<oc.length&&(oc.charAt(oce)!=';'||ocq)){if(ocq){if(oc.charAt(oce)=="
+"ocq&&!ocx)ocq=0;else if(oc.charAt(oce)==\"\\\\\")ocx=!ocx;else ocx=0;}else{ocq=oc.charAt(oce);if(ocq!='\"'&&ocq!=\"'\")ocq=0}oce++;}oc=oc.substring(ocb,oce);if(oc){o.s_soid=new Function('s','var e;"
+"try{s.wd.s_objectID='+oc+'}catch(e){}');o.s_soid(s)}}}}}if(s.gg('objectID')){n=s.gg('objectID');x=1;i=1}if(p&&n&&t)qs='&pid='+s.ape(s.fl(p,255))+(w?'&pidt='+w:'')+'&oid='+s.ape(s.fl(n,100))+(x?'&oi"
+"dt='+x:'')+'&ot='+s.ape(t)+(i?'&oi='+i:'')}}else trk=0}if(trk||qs){s.sampled=s.vs(sed);if(trk){if(s.sampled)code=s.mr(sess,(vt?'&t='+s.ape(vt):'')+s.hav()+q+(qs?qs:s.rq()),0,ta);qs='';s.m_m('t');if"
+"(s.p_r)s.p_r();s.referrer=s.lightProfileID=s.retrieveLightProfiles=s.deleteLightProfiles=''}s.sq(qs)}}}else s.dl(vo);if(vo)s.voa(vb,1);s.abort=0;s.pageURLRest=s.lnk=s.eo=s.linkName=s.linkType=s.wd."
+"s_objectID=s.ppu=s.pe=s.pev1=s.pev2=s.pev3='';if(s.pg)s.wd.s_lnk=s.wd.s_eo=s.wd.s_linkName=s.wd.s_linkType='';return code};s.trackLink=s.tl=function(o,t,n,vo,f){var s=this;s.lnk=o;s.linkType=t;s.li"
+"nkName=n;if(f){s.bct=o;s.bcf=f}s.t(vo)};s.trackLight=function(p,ss,i,vo){var s=this;s.lightProfileID=p;s.lightStoreForSeconds=ss;s.lightIncrementBy=i;s.t(vo)};s.setTagContainer=function(n){var s=th"
+"is,l=s.wd.s_c_il,i,t,x,y;s.tcn=n;if(l)for(i=0;i<l.length;i++){t=l[i];if(t&&t._c=='s_l'&&t.tagContainerName==n){s.voa(t);if(t.lmq)for(i=0;i<t.lmq.length;i++){x=t.lmq[i];y='m_'+x.n;if(!s[y]&&!s[y+'_c"
+"']){s[y]=t[y];s[y+'_c']=t[y+'_c']}s.loadModule(x.n,x.u,x.d)}if(t.ml)for(x in t.ml)if(s[x]){y=s[x];x=t.ml[x];for(i in x)if(!Object.prototype[i]){if(typeof(x[i])!='function'||(''+x[i]).indexOf('s_c_i"
+"l')<0)y[i]=x[i]}}if(t.mmq)for(i=0;i<t.mmq.length;i++){x=t.mmq[i];if(s[x.m]){y=s[x.m];if(y[x.f]&&typeof(y[x.f])=='function'){if(x.a)y[x.f].apply(y,x.a);else y[x.f].apply(y)}}}if(t.tq)for(i=0;i<t.tq."
+"length;i++)s.t(t.tq[i]);t.s=s;return}}};s.wd=window;s.ssl=(s.wd.location.protocol.toLowerCase().indexOf('https')>=0);s.d=document;s.b=s.d.body;if(s.d.getElementsByTagName){s.h=s.d.getElementsByTagN"
+"ame('HEAD');if(s.h)s.h=s.h[0]}s.n=navigator;s.u=s.n.userAgent;s.ns6=s.u.indexOf('Netscape6/');var apn=s.n.appName,v=s.n.appVersion,ie=v.indexOf('MSIE '),o=s.u.indexOf('Opera '),i;if(v.indexOf('Oper"
+"a')>=0||o>0)apn='Opera';s.isie=(apn=='Microsoft Internet Explorer');s.isns=(apn=='Netscape');s.isopera=(apn=='Opera');s.ismac=(s.u.indexOf('Mac')>=0);if(o>0)s.apv=parseFloat(s.u.substring(o+6));els"
+"e if(ie>0){s.apv=parseInt(i=v.substring(ie+5));if(s.apv>3)s.apv=parseFloat(i)}else if(s.ns6>0)s.apv=parseFloat(s.u.substring(s.ns6+10));else s.apv=parseFloat(v);s.em=0;if(s.em.toPrecision)s.em=3;el"
+"se if(String.fromCharCode){i=escape(String.fromCharCode(256)).toUpperCase();s.em=(i=='%C4%80'?2:(i=='%U0100'?1:0))}if(s.oun)s.sa(s.oun);s.sa(un);s.vl_l='timestamp,dynamicVariablePrefix,visitorID,fi"
+"d,vmk,visitorMigrationKey,visitorMigrationServer,visitorMigrationServerSecure,ppu,charSet,visitorNamespace,cookieDomainPeriods,cookieLifetime,pageName,pageURL,referrer,contextData,currencyCode,ligh"
+"tProfileID,lightStoreForSeconds,lightIncrementBy,retrieveLightProfiles,deleteLightProfiles,retrieveLightData';s.va_l=s.sp(s.vl_l,',');s.vl_mr=s.vl_m='timestamp,charSet,visitorNamespace,cookieDomain"
+"Periods,cookieLifetime,contextData,lightProfileID,lightStoreForSeconds,lightIncrementBy';s.vl_t=s.vl_l+',variableProvider,channel,server,pageType,transactionID,purchaseID,campaign,state,zip,events,"
+"events2,products,linkName,linkType';var n;for(n=1;n<=75;n++){s.vl_t+=',prop'+n+',eVar'+n;s.vl_m+=',prop'+n+',eVar'+n}for(n=1;n<=5;n++)s.vl_t+=',hier'+n;for(n=1;n<=3;n++)s.vl_t+=',list'+n;s.va_m=s.s"
+"p(s.vl_m,',');s.vl_l2=',tnt,pe,pev1,pev2,pev3,resolution,colorDepth,javascriptVersion,javaEnabled,cookiesEnabled,browserWidth,browserHeight,connectionType,homepage,pageURLRest,plugins';s.vl_t+=s.vl"
+"_l2;s.va_t=s.sp(s.vl_t,',');s.vl_g=s.vl_t+',trackingServer,trackingServerSecure,trackingServerBase,fpCookieDomainPeriods,disableBufferedRequests,mobile,visitorSampling,visitorSamplingGroup,dynamicA"
+"ccountSelection,dynamicAccountList,dynamicAccountMatch,trackDownloadLinks,trackExternalLinks,trackInlineStats,linkLeaveQueryString,linkDownloadFileTypes,linkExternalFilters,linkInternalFilters,link"
+"TrackVars,linkTrackEvents,linkNames,lnk,eo,lightTrackVars,_1_referrer,un';s.va_g=s.sp(s.vl_g,',');s.pg=pg;s.gl(s.vl_g);s.contextData=new Object;s.retrieveLightData=new Object;if(!ss)s.wds();if(pg){"
+"s.wd.s_co=function(o){return o};s.wd.s_gs=function(un){s_gi(un,1,1).t()};s.wd.s_dc=function(un){s_gi(un,1).t()}}",
w=window,l=w.s_c_il,n=navigator,u=n.userAgent,v=n.appVersion,e=v.indexOf('MSIE '),m=u.indexOf('Netscape6/'),a,i,j,x,s;if(un){un=un.toLowerCase();if(l)for(j=0;j<2;j++)for(i=0;i<l.length;i++){s=l[i];x=s._c;if((!x||x=='s_c'||(j>0&&x=='s_l'))&&(s.oun==un||(s.fs&&s.sa&&s.fs(s.oun,un)))){if(s.sa)s.sa(un);if(x=='s_c')return s}else s=0}}w.s_an='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
w.s_sp=new Function("x","d","var a=new Array,i=0,j;if(x){if(x.split)a=x.split(d);else if(!d)for(i=0;i<x.length;i++)a[a.length]=x.substring(i,i+1);else while(i>=0){j=x.indexOf(d,i);a[a.length]=x.subst"
+"ring(i,j<0?x.length:j);i=j;if(i>=0)i+=d.length}}return a");
w.s_jn=new Function("a","d","var x='',i,j=a.length;if(a&&j>0){x=a[0];if(j>1){if(a.join)x=a.join(d);else for(i=1;i<j;i++)x+=d+a[i]}}return x");
w.s_rep=new Function("x","o","n","return s_jn(s_sp(x,o),n)");
w.s_d=new Function("x","var t='`^@$#',l=s_an,l2=new Object,x2,d,b=0,k,i=x.lastIndexOf('~~'),j,v,w;if(i>0){d=x.substring(0,i);x=x.substring(i+2);l=s_sp(l,'');for(i=0;i<62;i++)l2[l[i]]=i;t=s_sp(t,'');d"
+"=s_sp(d,'~');i=0;while(i<5){v=0;if(x.indexOf(t[i])>=0) {x2=s_sp(x,t[i]);for(j=1;j<x2.length;j++){k=x2[j].substring(0,1);w=t[i]+k;if(k!=' '){v=1;w=d[b+l2[k]]}x2[j]=w+x2[j].substring(1)}}if(v)x=s_jn("
+"x2,'');else{w=t[i]+' ';if(x.indexOf(w)>=0)x=s_rep(x,w,t[i]);i++;b+=62}}}return x");
w.s_fe=new Function("c","return s_rep(s_rep(s_rep(c,'\\\\','\\\\\\\\'),'\"','\\\\\"'),\"\\n\",\"\\\\n\")");
w.s_fa=new Function("f","var s=f.indexOf('(')+1,e=f.indexOf(')'),a='',c;while(s>=0&&s<e){c=f.substring(s,s+1);if(c==',')a+='\",\"';else if((\"\\n\\r\\t \").indexOf(c)<0)a+=c;s++}return a?'\"'+a+'\"':"
+"a");
w.s_ft=new Function("c","c+='';var s,e,o,a,d,q,f,h,x;s=c.indexOf('=function(');while(s>=0){s++;d=1;q='';x=0;f=c.substring(s);a=s_fa(f);e=o=c.indexOf('{',s);e++;while(d>0){h=c.substring(e,e+1);if(q){i"
+"f(h==q&&!x)q='';if(h=='\\\\')x=x?0:1;else x=0}else{if(h=='\"'||h==\"'\")q=h;if(h=='{')d++;if(h=='}')d--}if(d>0)e++}c=c.substring(0,s)+'new Function('+(a?a+',':'')+'\"'+s_fe(c.substring(o+1,e))+'\")"
+"'+c.substring(e+1);s=c.indexOf('=function(')}return c;");
c=s_d(c);if(e>0){a=parseInt(i=v.substring(e+5));if(a>3)a=parseFloat(i)}else if(m>0)a=parseFloat(u.substring(m+10));else a=parseFloat(v);if(a<5||v.indexOf('Opera')>=0||u.indexOf('Opera')>=0)c=s_ft(c);if(!s){s=new Object;if(!w.s_c_in){w.s_c_il=new Array;w.s_c_in=0}s._il=w.s_c_il;s._in=w.s_c_in;s._il[s._in]=s;w.s_c_in++;}s._c='s_c';(new Function("s","un","pg","ss",c))(s,un,pg,ss);return s}
function s_giqf(){var w=window,q=w.s_giq,i,t,s;if(q)for(i=0;i<q.length;i++){t=q[i];s=s_gi(t.oun);s.sa(t.un);s.setTagContainer(t.tagContainerName)}w.s_giq=0}s_giqf()