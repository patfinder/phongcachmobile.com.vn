﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true"
    CodeBehind="m-loai-game.aspx.cs" Inherits="PhongCachMobile.m_loai_game" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                            <asp:Literal ID="litSubtitle" runat="server"></asp:Literal></h2>
                    </div>
                    <div class="toolbar">
                        <div class="pager">
                            <div class="limiter">
                                <label>
                                    &nbsp; &nbsp; Thể loại</label>
                                <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                </asp:DropDownList>
                                <label>
                                    &nbsp; &nbsp; Hệ điều hành</label>
                                <asp:DropDownList ID="ddlOS" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOS_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <asp:ListView ID="lstItem" runat="server"  onitemdatabound="lstItem_ItemDataBound">
                        <LayoutTemplate>
                            <ul class="products-grid" style="text-align:center">
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </ul>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <li class="item">
                                <div class="desc" style="padding-bottom:10px;">
                                    <h2 class="product-name product-name-height">
                                        <a href="#">
                                            <%#Eval("Name")%></a></h2>
                                              <div class="grid-inner">
                                    <div class="product-image">
                                       <img width="150px" src="images/game/<%#Eval("DisplayImage")%>" alt="<%#Eval("Name")%>"></div>
                                </div>
                                    <div class=" std" style="text-align:left;">
                                        <%#Eval("Description")%>
                                    </div>
                                       <p class="compare checkbox">
                                                        <label for="comparator_item_list1">
                                                            Dung lượng:</label>
                                                        <span class="property">
                                                            <%#Eval("Filesize")%></span></p>
                                     <p class="compare checkbox">
                                                                <asp:HiddenField ID="hdfName" Value='<%#Eval("Filename")%>' runat="server" />
                                                                  <asp:HiddenField ID="hdfId" Value='<%#Eval("Id")%>' runat="server" />
                                                                <asp:HyperLink ID="hplDownload" class="exclusive ajax_add_to_cart_button"  runat="server">Tải về</asp:HyperLink>
                                                                </p>

                                </div>
                              
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                    <div class="toolbar">
                        <div class="pager">
                            <div class="pages">
                                <strong>Trang:</strong>
                                <ol>
                                    <asp:DataPager ID="dpgLstItemBot" PageSize="6" PagedControlID="lstItem" runat="server"
                                        OnPreRender="dpgLstItem_PreRender">
                                        <Fields>
                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                        </Fields>
                                    </asp:DataPager>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
