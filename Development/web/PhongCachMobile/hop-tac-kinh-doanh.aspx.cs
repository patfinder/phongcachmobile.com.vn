﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class hop_tac_kinh_doanh : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            litJoinBusiness.Text = this.Title =  LangController.getLng("litJoinBusiness.Text", "Hợp tác kinh doanh");
            litJoinBusinessContent.Text = LangMsgController.getLngMsg("litJoinBusiness.Content", "<div class='block-content' style='background: white; padding: 25px 30px 16px 30px; " + 
                                    "line-height: 18px; color: #515151;'> " + 
                                    "<p style='font-weight: bold;'> " +
                                        "<font style='color: red;'>PhongCachMobile</font> yêu cầu hợp tác chân chính " + 
                                        "để cùng phát triển trên cơ sở đảm bảo quyền lợi cao nhất cho khách hàng!</p> " +
                                    "<div class='box-container'> " + 
                                        "<div class='about-page'> " + 
                                            "<i class='icon-random'>like</i><div class='extra-wrap'> " + 
                                                "<h3> " + 
                                                    "Liên hệ trở thành Đại lý</h3> " + 
                                                "<p style='line-height: 18px;padding-top:8px;'> " + 
                                                    "Các cửa hàng điện thoại di động trên toàn quốc có nhu cầu muốn trở thành đại lý " + 
                                                    "nhập hàng của PhongCachMobile " + 
                                                    "vui lòng liên lạc với <font style='color: red; " + 
                                                        "font-weight: bold;'>Mr. Hồ Hoa Phong</font> - P.Kinh Doanh qua số điện thoại <font style='color: red; " + 
                                                        "font-weight: bold;'>0982.787.585</font> hoặc email <font style='color: red; font-weight: bold;'>" + 
                                                            "hp.hoaphong@gmail.com</font>. Quyền lợi khi trở thành đại lý của PhongCachMobile bao gồm:</p> " + 
                                                "<p> " +
                                                "</p> " + 
                                                "<ul> " + 
                                                    "<li>Được nhập hàng điện thoại di động chất lượng cao nhất với mức giá sỉ tốt nhất để " + 
                                                        "đảm bảo lợi ích kinh doanh tối đa.</li> " + 
                                                    "<li>Được cập nhật thường xuyên các mẫu điện thoại di động mới nhất của thị trường nước " + 
                                                        "ngoài.</li> " + 
                                                    "<li>Chính sách hỗ trợ kỹ thuật và bảo hành tốt nhất và nhanh chóng nhất.</li> " + 
                                                    "<li>...và đặc biệt là một chính sách thưởng cực kỳ hấp dẫn!</li> " + 
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                        "<div class='about-page'> " + 
                                            "<i class='icon-star-empty'>like</i><div class='extra-wrap'>" + 
                                                "<h3>" + 
                                                    "Liên hệ khai thác thương hiệu PHONGCACHMOBILE</h3>" + 
                                                "<p style='line-height: 18px;padding-top:8px;'>" + 
                                                    "Các đại lý muốn sử dụng và khai thác thương hiệu " + 
                                                        "PhongCachMobile trong việc kinh doanh, vui lòng liên hệ với <font style='color: red; font-weight: bold;'>Mr. Vũ</font> - " + 
                                                    "P.Marketing qua số điện thoại <font style='color: red; font-weight: bold;'>0908.430.968</font> " + 
                                                    "hoặc email <font style='color: red; font-weight: bold;'>phongcach_mobile@yahoo.com</font>. " + 
                                                    "Quyền lợi khi được sử dụng thương hiệu PhongCachMobile bao gồm:</p> " + 
                                                "<ul> " + 
                                                    "<li>Đuợc trở thành một chi nhánh chính thức của PhongCachMobile, địa chỉ và thông tin chi tiết của các chi nhánh sẽ được " + 
                                                        "thể hiện trên website www.phongcachmobile.com.vn.</li> " + 
                                                    "<li>Được hỗ trợ trong việc xây dựng mặt bằng: Tư vấn về thiết kế, mua trang thiết bị " + 
                                                        "và đồ dùng, thỏa thuận với nhà cung cấp.</li> " + 
                                                    "<li>Được hỗ trợ Marketing cùng các chương trình bán hàng ưu đãi để phát triển số lượng " + 
                                                        "khách hàng tiềm năng.</li> " + 
                                                    "<li>Hoàn toàn không lo lắng về hàng hoá đầu vào vì được chúng tôi cung cấp toàn phần " + 
                                                        "tất cả ĐTDĐ và phụ kiện chất lượng cao nhất.</li> " + 
                                                    "<li>Chính sách hỗ trợ máy mẫu trưng bày với mức giá đặc biệt.</li> " + 
                                                    "<li>Được cập nhật thường xuyên các mẫu điện thoại di động mới nhất của thị trường nước " + 
                                                        "ngoài.</li> " + 
                                                    "<li>Hỗ trợ và huấn luyện chuyên môn cho đội ngũ nhân viên bán hàng và kỹ thuật trong " + 
                                                        "suốt quá trình kinh doanh. Được cập nhật những thông tin chuyên sâu mới nhất vào " + 
                                                        "mỗi tuần/tháng.</li> " + 
                                                    "<li>...và quan trọng nhất là bạn sẽ có được sự yên tâm về doanh số bán hàng và lợi nhuận.</li> " + 
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                        "<div class='about-page'>" + 
                                            "<i class='icon-truck'>like</i>" + 
                                            "<div class='extra-wrap'>" + 
                                                "<h3>" + 
                                                    "Đối tác cung cấp hàng cho PHONGCACHMOBILE</h3> " + 
                                                "<ul> " + 
                                                    "<li>Quý đối tác trong và ngoài nước nếu có nguồn hàng ĐTDĐ hoặc phụ kiện chất lượng " + 
                                                        "cao (không phải hàng nhái, hàng giả kém chất lượng) muốn cung cấp hàng hoá vào thị " + 
                                                        "trường Việt Nam thông qua những nhà phân phối uý tín như PhongCachMobile có thể " + 
                                                        "liên hệ ngay với <font style='color: red; font-weight: bold;'>Mr. Kiệt</font> - P.Thu Mua qua số điện thoại " + 
                                                        "<font style='color: red;font-weight:bold;'>0907.389.450</font> hoặc email " + 
                                                        "<font style='color: red;font-weight:bold;'>phongcach_mobile@yahoo.com</font> " + 
                                                    "</li>" +
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                    "</div>" + 
                                "</div>");
        }
    }
}