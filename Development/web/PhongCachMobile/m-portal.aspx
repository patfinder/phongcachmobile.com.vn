﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="m-portal.aspx.cs" Inherits="PhongCachMobile.m_portal" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
    <asp:Literal ID="litTitle" runat="server"></asp:Literal></title>
        <!-- All Stylesheets -->
        <link rel="stylesheet"  href="m/css/All-Stylesheets.css">
        <!-- Add 2 Home -->
        <link rel="apple-touch-icon" href="m/images/add-to-home/touch-icon-iphone.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="m/images/add-to-home/touch-icon-ipad.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="m/images/add-to-home/touch-icon-iphone4.png" />
        <script type="text/javascript" src="m/js/add2home.js" charset="utf-8"></script>
        <!-- Javascript File -->
        <script src="m/js/jquery.js"></script>
        <script type="text/javascript" src="m/js/jquery-1.7.1.min.js"></script>
        <!-- Donut Chart -->
        <script type="text/javascript" src="m/js/jquery.donutchart.js"></script>
        <!-- jQuery Mobile -->
        <script src="m/js/jquery.mobile-1.3.2.min.js"></script>
        <!-- Glyphicons -->
        <script type="text/javascript" async src="m/js/glyphicons/ga.js"></script>
    </head>
    <body>
        <!-- Page Contents Starts
            ================================================== -->
        <div data-role="page" id="page" data-theme="">
            <!-- header Starts -->
            <div class="logo"><img src="images/logo-web-light@1x.png" width="70%"> </div>
            <!-- header Ends -->
            <div class="enter"><a href="m-gia-hot" rel="external"><i class="fa fa-chevron-circle-right fa-4x"></i></a></div>
            <div data-role="content">
                <!-- Slider Starts -->
                <div id="homepage">
                    <div class="slider-text">
                        <div id="slidecaption"></div>
                    </div>
                    <!-- Left, Right Arrow Buttons Starts --> 
                    <a id="prevslide" class="load-item"><i class="fa fa-chevron-circle-left fa-2x"></i></a><a id="nextslide" class="load-item"><i class="fa fa-chevron-circle-right fa-2x"></i></a> 
                    <!-- Left, Right Arrow Buttons Starts --> 
                    <!--Time Bar-->
                    <div id="progress-back" class="load-item">
                        <div id="progress-bar"></div>
                    </div>
                </div>
                <!-- Slider Ends -->
            </div>
            <!-- /content -->
        </div>
        <!-- Page Contents Ends
            ================================================== -->
        <!-- Javascript Files
            ================================================== -->   
        <script src="m/owl-carousel/owl-carousel/owl.carousel.js"></script> 
        <!-- Custom JS File -->
        <script src="m/js/custom.js"></script> 
        <script src="m/menu/js/main.js"></script>    
        <!-- Twitter -->
        <script src="m/js/tweets/twitlive-min.js"></script>
        <script>
            $(document).ready(function () {
                $(".customizeWindow").click(function () {
                    $(".customizeMe").css("display", "block");
                });

                $("#close").click(function () {
                    $(".customizeMe").css("display", "none");
                });

                $("#custSearch").click(function () {
                    $(".customizeMe").css("display", "none");
                    $("#tweet_container h1 em").html($("#keyword").val());
                });

                $(".tweets").liveTweets({ operator: "#google", liveTweetsToken: "R8l0mjc5sbfmSB1fRdKB15T19mGfI8mpnnfvWipmo" });
            });
        </script>        
        <!-- Retina Display -->
        <script src="m/js/Retina/retina.js"></script>
        <!-- Supersized Slider
            ================================================== --> 
        <script type="text/javascript" src="m/js/supersized/jquery.easing.min.js"></script> 
        <script type="text/javascript" src="m/js/supersized/supersized.3.2.7.min.js"></script> 
        <script type="text/javascript" src="m/js/supersized/supersized.shutter.min.js"></script>
        <asp:Literal ID="litImage" runat="server"></asp:Literal> 
        
       
       <%-- <script type="text/javascript" src="m/js/supersized/supersized-images.js"></script>--%>
    </body>
</html>