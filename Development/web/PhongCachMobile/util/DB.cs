﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using PhongCachMobile.model;
using PhongCachMobile.controller;

namespace PhongCachMobile.util
{
    public class DB
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProductController).FullName);

        //public static bool connect()
        //{
        //    bool result = true;
        //    try
        //    {
        //        SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString);
        //        SqlConnection cn.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //          LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
        //    }
        //    return result;
        //}

        /// <summary>
        /// Wrap query string in pager, so the query results can be returned in pages
        /// </summary>
        /// <param name="query">Only include field list, no "SELECT" keyword</param>
        /// <param name="pageIndex">Start from 0</param>
        /// <param name="pageSize"></param>
        /// <param name="paramss">Will include @PageStart and @PageEnd</param>
        /// <returns></returns>
        /// 
        int i;

        /// <summary>
        /// Wrap query string in pager, so the query results can be returned in pages
        /// </summary>
        /// <param name="query">Only include field list, no "SELECT" keyword. Query must not include Order expression.</param>
        /// <param name="orderExp"></param>
        /// <param name="pageIndex">Start from 0</param>
        /// <param name="pageSize"></param>
        /// <param name="paramss">Will include @PageStart and @PageEnd</param>
        /// <returns></returns>
        public static string wrapInPager(string query, string orderExp, int pageIndex, int pageSize, out object[,] paramss)
        {
            paramss = new object[,]{
                {"@PageStart", pageIndex * pageSize + 1},
                {"@PageEnd", (pageIndex + 1) * pageSize},
            };

            return
                "SELECT * FROM ( " +
                "    SELECT ROW_NUMBER() OVER(" + orderExp + ") AS Row_Number, " + query +
                ") AS Page_Table WHERE Row_Number BETWEEN @PageStart AND @PageEnd " + orderExp;
        }

        public static string getValue(string queryString)
        {
            string result = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        Object obj = cmd.ExecuteScalar();
                        if (obj == null)
                            result = "";
                        else
                            result = obj.ToString();
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("getValue: sql: {0}", queryString), ex);
            }

            return result;
        }

        public static string getValue(string queryString, object[,] parameters)
        {
            string result = "";
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        if (parameters != null)
                            for (int i = 0; i < parameters.GetLength(0); i++)
                                cmd.Parameters.Add(new SqlParameter(parameters[i, 0].ToString(), parameters[i, 1]));
                        Object obj = cmd.ExecuteScalar();
                        if (obj == null)
                            result = "";
                        else
                            result = obj.ToString();
                    }
                    cn.Close();
                }

            }
            catch (Exception ex)
            {
                string params1 = "";
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        for (int j = 0; j < parameters.GetLength(1); j++)
                            params1 += parameters[i, j] + ", ";

                        params1 += "\n-------------\n";
                    }
                }

                log.Error(string.Format("getValue: sql: {0}, pararmeter: {1}", queryString, params1), ex);
            }
            return result;
        }

        public static DataTable getData(string queryString)
        {
            DataTable dt = null;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt = new DataTable();
                            dt.Load(dr);
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("getData: sql: {0}", queryString), ex);
            }
            return dt;
        }

        public static DataTable getData(string queryString, object[,] parameters)
        {
            DataTable dt = null;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        if (parameters != null)
                            for (int i = 0; i < parameters.GetLength(0); i++)
                                cmd.Parameters.Add(new SqlParameter(parameters[i, 0].ToString(), parameters[i, 1]));
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt = new DataTable();
                            dt.Load(dr);
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string params1 = "";
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        for (int j = 0; j < parameters.GetLength(1); j++)
                            params1 += parameters[i, j] + ", ";

                        params1 += "\n-------------\n";
                    }
                }

                log.Error(string.Format("getData: sql: {0}, pararmeter: {1}", queryString, params1), ex);
            }
            return dt;
        }

        public static DataTable getData(string queryString, object[,] parameters, bool[] isUnicode)
        {
            DataTable dt = null;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        if (parameters != null)
                            for (int i = 0; i < parameters.GetLength(0); i++)
                            {
                                SqlParameter parameter = new SqlParameter();
                                parameter.ParameterName = parameters[i, 0].ToString();
                                parameter.Direction = ParameterDirection.Input;
                                parameter.Value = parameters[i, 1].ToString().Replace('-', ' ');
                                if (isUnicode[i] == true)
                                    parameter.SqlDbType = SqlDbType.NVarChar;
                                else
                                    parameter.SqlDbType = SqlDbType.VarChar;
                                cmd.Parameters.Add(parameter);
                            }
                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            dt = new DataTable();
                            dt.Load(dr);
                        }
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string params1 = "";
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        for (int j = 0; j < parameters.GetLength(1); j++)
                            params1 += parameters[i, j] + ", ";

                        params1 += "\n-------------\n";
                    }
                }

                log.Error(string.Format("getData: sql: {0}, pararmeter: {1}, bool", queryString, params1), ex);
            }
            return dt;
        }

        public static bool exec(string queryString)
        {
            bool success = true;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("exec: sql: {0}", queryString), ex);

                success = false;
            }
            return success;
        }

        public static bool exec(string queryString, object[,] parameters, bool logOnError = true)
        {
            bool success = true;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.CommandType = CommandType.Text;
                        if (parameters != null)
                            for (int i = 0; i < parameters.GetLength(0); i++)
                                cmd.Parameters.Add(new SqlParameter(parameters[i, 0].ToString(), parameters[i, 1]));
                        cmd.ExecuteNonQuery();
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string params1 = "";
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        for (int j = 0; j < parameters.GetLength(1); j++)
                            params1 += parameters[i, j] + ", ";

                        params1 += "\n-------------\n";
                    }
                }

                log.Error(string.Format("exec: sql: {0}, pararmeter: {1}, bool", queryString, params1), ex);

                success = false;
            }
            return success;
        }

        public static bool exec(string queryString, List<object[]> parameters, bool logOnError = true)
        {
            var paramss = parameters.ToDictionary((l1) => l1[0].ToString(), (l1) => l1[1]);

            return exec(queryString, paramss, logOnError);
        }

        public static bool exec(string queryString, Dictionary<string, object> parameters, bool logOnError = true)
        {
            bool success = true;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        cmd.CommandType = CommandType.Text;
                        if (parameters != null)
                        {
                            foreach(KeyValuePair<string, object> kv in parameters)
                                cmd.Parameters.Add(new SqlParameter(kv.Key, kv.Value));
                        }

                        cmd.ExecuteNonQuery();
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string params1 = "";
                if (parameters != null)
                {
                    foreach (var kv in parameters)
                    {
                        params1 += string.Format("{0} = {1}, ", kv.Key, kv.Value);
                        params1 += "\n-------------\n";
                    }
                }

                log.Error(string.Format("exec: sql: {0}, pararmeter list: {1}, bool", queryString, params1), ex);

                success = false;
            }
            return success;
        }

        public static bool exec(string queryString, string commandType)
        {
            bool success = true;
            try
            {

                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        switch (commandType)
                        {
                            case "StoredProcedure":
                                cmd.CommandType = CommandType.StoredProcedure;
                                break;
                            case "TableDirect":
                                cmd.CommandType = CommandType.TableDirect;
                                break;
                            case "Text":
                                cmd.CommandType = CommandType.Text;
                                break;
                        }
                        cmd.ExecuteNonQuery();
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                log.Error(string.Format("exec: sql: {0}, commandType: {1}", queryString, commandType), ex);

                success = false;
            }
            return success;
        }

        public static bool exec(string queryString, object[,] parameters, string commandType)
        {
            bool success = true;
            try
            {
                using (SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstr"].ConnectionString))
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand(queryString, cn))
                    {
                        switch (commandType)
                        {
                            case "StoredProcedure":
                                cmd.CommandType = CommandType.StoredProcedure;
                                break;
                            case "TableDirect":
                                cmd.CommandType = CommandType.TableDirect;
                                break;
                            case "Text":
                                cmd.CommandType = CommandType.Text;
                                break;
                        }
                        if (parameters != null)
                            for (int i = 0; i < parameters.GetLength(0); i++)
                                cmd.Parameters.Add(new SqlParameter(parameters[i, 0].ToString(), parameters[i, 1]));
                        cmd.ExecuteNonQuery();
                    }
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                string params1 = "";
                if (parameters != null)
                {
                    for (int i = 0; i < parameters.Length; i++)
                    {
                        for (int j = 0; j < parameters.GetLength(1); j++)
                            params1 += parameters[i, j] + ", ";

                        params1 += "\n-------------\n";
                    }
                }

                log.Error(string.Format("exec: sql: {0}, pararmeter: {1}, commandType: {2}", queryString, params1, commandType), ex);

                success = false;
            }
            return success;
        }
    }
}