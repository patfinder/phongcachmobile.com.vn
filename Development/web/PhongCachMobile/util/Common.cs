﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using PhongCachMobile.model;
using PhongCachMobile.controller;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Net;

namespace PhongCachMobile.util
{
    public static class Common
    {
        public static string strDateTimeToString(string strDateTime)
        {
            strDateTime = strDateTime.Replace("/", "");
            strDateTime = strDateTime.Replace("-", "");
            strDateTime = strDateTime.Replace(" ", "");
            strDateTime = strDateTime.Replace(":", "");
            return strDateTime;
        }

        public static string getRandomString(int size)
        {
            Random random = new Random((int)DateTime.Now.Ticks);
            StringBuilder builder = new StringBuilder();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString().ToLower();
        }
        public static string generateHash(string input)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
            Byte[] bs = System.Text.Encoding.UTF8.GetBytes(input);
            bs = x.ComputeHash(bs);
            StringBuilder s = new StringBuilder();
            foreach (Byte b in bs)
            {
                s.Append(b.ToString("x2").ToLower());
            }
            string outstr = s.ToString();
            return outstr;
        }

        public static DateTime strToFormatDateTime(string strDateTime, string dtFormat)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            return DateTime.ParseExact(strDateTime, dtFormat, provider);
        }

        public static DateTime parseDate(string s, string format)
        {
            DateTime result;
            if (!DateTime.TryParse(s, out result))
            {
                result = DateTime.ParseExact(s, format, System.Globalization.CultureInfo.InvariantCulture);
            }
            return result;
        }

        public static DateTime strToDateTime(string strDateTime)
        {
            CultureInfo provider = CultureInfo.InvariantCulture;
            return DateTime.ParseExact(strDateTime.Trim(), "yyyyMMddHHmmss", provider);
        }

        public static string getComicRate(string rate)
        {
            double dRate = double.Parse(rate);
            if (dRate <= 1)
                return "0.png";
            if (dRate < 2)
                return "1.png";
            if (dRate < 3)
                return "2.png";
            if (dRate < 4)
                return "3.png";
            if (dRate < 5)
                return "4.png";
            if (dRate >= 5)
                return "5.png";
            return "0.png";
        }


        public static string getRate(string rate)
        {
            double dRate = double.Parse(rate);
            if (dRate < 0.5)
                return "iStar";
             if (dRate <1)
                return "iStar i_s0-5";
            if (dRate <1.5)
                return "iStar i_s1";
            if (dRate <2)
                return "iStar i_s1-5";
            if (dRate <2.5)
                return "iStar i_s2";
            if (dRate <3)
                return "iStar i_s2-5";
            if (dRate <3.5)
                return "iStar i_s3";
            if (dRate <4)
                return "iStar i_s3-5";
            if (dRate <4.5)
                return "iStar i_s4";
            if (dRate <5)
                return "iStar i_s4-5";
            if (dRate >=5)
                return "iStar i_s5";
             return "iStar";
        }

        public static string getLikeRate(string likeRate)
        {
            double dLikeRate = double.Parse(likeRate);
            if (dLikeRate < 0.5)
               return "iHeart";
            if (dLikeRate <1)
               return "iHeart i_h0-5";
            if (dLikeRate <1.5)
               return "iHeart i_h1";
            if (dLikeRate <2)
               return "iHeart i_h1-5";
            if (dLikeRate <2.5)
                return "iHeart i_h2";
            if (dLikeRate <3)
                return "iHeart i_h2-5";
            if (dLikeRate <3.5)
                return "iHeart i_h3";
            if (dLikeRate <4)
                return "iHeart i_h3-5";
            if (dLikeRate <4.5)
                return "iHeart i_h4";
            if (dLikeRate <5)
                return "iHeart i_h4-5";
            if (dLikeRate >=5)
                return "iHeart i_h5";
            return "iHeart";
        }

        public static bool sendEmail(string strTo, string strFrom, string strCc, string strBcc, string strSubject, string strBody, string strAttPath)
        {
            try
            {
                MailMessage mail = new MailMessage();

                string strUsername = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string strPassword = ConfigurationManager.AppSettings["smtpPassword"].ToString();

                
                int iValue = 700;

                if (strTo == "" && strTo != null)
                {
                    LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Empty To-address", 1, "Error sendEmail"));
                    return false;
                }

                string[] arrStrTo = strTo.Split(';');
                foreach (string strItem in arrStrTo)
                {
                    mail.To.Add(strItem.Trim());
                }

                if (strCc != "" && strCc != null)
                {
                    string[] arrStrCc = strCc.Split(';');
                    foreach (string strItem in arrStrCc)
                    {
                        mail.CC.Add(strItem.Trim());
                    }
                }

                if (strBcc != "" && strBcc != null)
                {
                    string[] arrStrBcc = strBcc.Split(';');
                    foreach (string strItem in arrStrBcc)
                    {
                        mail.Bcc.Add(strItem.Trim());
                    }
                }

                if (strSubject == "")
                    strSubject = "(No subject)";
                else
                    mail.Subject = strSubject;
                mail.Body = HttpUtility.HtmlDecode(strBody);
                mail.IsBodyHtml = true;

                if (strAttPath != "" && strAttPath != null)
                {
                    string[] arrStrAttPath = strAttPath.Split(';');
                    foreach (string strItem in arrStrAttPath)
                    {
                        Attachment attachment = new Attachment(strItem.Trim());
                        mail.Attachments.Add(attachment);
                    }
                }

                if (strFrom == "")
                {
                    strFrom = strUsername;
                }

                mail.From = new MailAddress(strFrom, "PHONGCACHMOBILE");

                string[] smtpConfig = ConfigurationManager.AppSettings["smtpServer"].Split(';');

                string strSmtpServer = Convert.ToString(smtpConfig.GetValue(0));
                int iSmtpPort = Convert.ToInt32(smtpConfig.GetValue(1));
                bool isAuthenticated = Convert.ToBoolean(smtpConfig.GetValue(2));
                bool isSSLEnable = Convert.ToBoolean(smtpConfig.GetValue(3));

                SmtpClient SmtpServer = new SmtpClient(strSmtpServer);

                SmtpServer.Port = iSmtpPort;
                if (isAuthenticated)
                    SmtpServer.Credentials = new System.Net.NetworkCredential(strUsername, strPassword);
                SmtpServer.EnableSsl = isSSLEnable;
                SmtpServer.Send(mail);

                return true;
            }
            catch (Exception e)
            {
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), e.ToString(), 1, "Error sendEmail " + e.InnerException == null ? e.InnerException.ToString() : ""));
                return false;

            }
        }

        static Regex MobileCheck = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
        static Regex MobileVersionCheck = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);

        public static bool fBrowserIsMobile()
        {
            Debug.Assert(HttpContext.Current != null);

            if (HttpContext.Current.Request != null && HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"] != null)
            {
                var u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();

                if (u.Length < 4)
                    return false;

                if (MobileCheck.IsMatch(u) || MobileVersionCheck.IsMatch(u.Substring(0, 4)))
                    return true;
            }

            return false;
        }

        public static string m()
        {
            return fBrowserIsMobile() ? "m-" : "";
        }

        public static bool IsLoggedIn()
        {
            return HttpContext.Current.Session != null && 
                !string.IsNullOrWhiteSpace(HttpContext.Current.Session["Email"] as string);
        }

        public static string StripLeadingM(this string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return path;

            return path.StartsWith("m-") ? path.Substring(2) : path;
        }


        public static string StripLeadingFSlash(this string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return path;

            return path.StartsWith("/") ? path.Substring(1) : path;
        }

        public static TValue Vx<TKey, TValue>
            (this IDictionary<TKey, TValue> dictionary, TKey key, TValue defaultValue)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : defaultValue;
        }

        public static string NormalizeDiacriticalCharacters(this string value)
        {
            if (value == null)
                return null;

            var normalised = value.Normalize(NormalizationForm.FormD).ToCharArray();

            return new string(normalised.Where(c => (int)c <= 127).ToArray());
        }

        public static string ReplaceNonAlphaNumSpace(this string value)
        {
            if (value == null)
                return null;
            
            var result = "";
            foreach(var ch in value)
            {
                if (char.IsLetterOrDigit(ch) || ch == ' ')
                    result += ch.ToString();
                else
                    result += " ";
            }

            return result;
        }

        public static string MakeUrl(HttpRequest request, string url)
        {
            var pageUrl = request.Url.Scheme + System.Uri.SchemeDelimiter + request.Url.Host +
                (request.Url.IsDefaultPort ? "" : ":" + request.Url.Port) + "/" + url.StripLeadingFSlash();

            return pageUrl;
        }

        public static bool UrlExists(HttpRequest request, string url)
        {
            try
            {
                if (url == null)
                    return false;

                if (!url.StartsWith("http://"))
                    url = MakeUrl(request, url);

                var request2 = WebRequest.Create(url) as HttpWebRequest;
                if (request2 == null)
                    return false;

                request2.Method = "HEAD";
                using (var response = (HttpWebResponse)request2.GetResponse())
                {
                    return response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (UriFormatException)
            {
                //Invalid Url
                return false;
            }
            catch (WebException)
            {
                //Unable to access url
                return false;
            }
        }

        /// <summary>
        /// Get value or default
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="defaultValueProvider"></param>
        /// <returns></returns>
        public static TValue Vx<TKey, TValue>
            (this IDictionary<TKey, TValue> dictionary, TKey key, Func<TValue> defaultValueProvider)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value
                 : defaultValueProvider();
        }

        /// <summary>
        /// Get value or default
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static TValue Vx<TKey, TValue>
        (this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue);
        }
    }

    public class FunctionalComparer<T> : IComparer<T>
    {
        private Func<T, T, int> comparer;
        public FunctionalComparer(Func<T, T, int> comparer)
        {
            this.comparer = comparer;
        }
        public static IComparer<T> Create(Func<T, T, int> comparer)
        {
            return new FunctionalComparer<T>(comparer);
        }
        public int Compare(T x, T y)
        {
            return comparer(x, y);
        }
    }
}