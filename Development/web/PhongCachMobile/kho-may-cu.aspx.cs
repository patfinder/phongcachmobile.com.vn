﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class kho_may_cu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.Home)this.Master).TemplateType = TemplateType.NewType;

            if (!this.IsPostBack)
            {
                loadBannerItems();
                loadFilterValues();
                loadFilterPhoneList();
            }
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("Kho_May_Cu_So_SP_1_Trang", "20"));

        protected List<Article> topBannerLeftItems = new List<Article>();
        protected List<Article> topBannerRightItem1s = new List<Article>();
        protected List<Article> topBannerRightItem2s = new List<Article>();
        private void loadBannerItems()
        {
            // DTDD_Top_Banner_Left_Items // V2-Old Phone - Sliders Left
            foreach (DataRow dr in SliderController.getSliders("1072").Rows)
            {
                topBannerLeftItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // DTDD_Top_Banner_Right_Items1 // V2-Old Phone - Sliders Right 1
            foreach (DataRow dr in SliderController.getSliders("1073").Rows)
            {
                topBannerRightItem1s.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // DTDD_Top_Banner_Right_Items2 // V2-Old Phone - Sliders Right 2
            foreach (DataRow dr in SliderController.getSliders("1074").Rows)
            {
                topBannerRightItem2s.Add(SliderController.rowToSlider(dr).toArticle());
            }

            ucTripleBanner.DataBind();
        }

        protected Dictionary<string, int> brands = new Dictionary<string, int>();
        protected Dictionary<string, int> prices = new Dictionary<string, int>();

        private void loadFilterValues()
        {
            // Brands
            DataTable dt = CategoryController.getCategories(ProductController.ProductGroupPhone, "");
            if (dt != null && dt.Rows.Count > 0)
            {
                brands.Add("Tất cả", -1);
                foreach (DataRow dr in dt.Rows)
                {
                    brands.Add(dr["Name"].ToString(), int.Parse(dr["Id"].ToString()));
                }
            }

            // Prices
            prices = ProductController.PriceList;
        }

        protected int remainCount = 0; 
        protected List<Product> filterPhoneList = new List<Product>();
        protected bool OldProduct = true;
        private void loadFilterPhoneList()
        {
            int soTin = int.Parse(SysIniController.getSetting("Kho_May_Cu_So_SP_1_Trang", "16"));
            DataTable dt = OldProductController.getOldProducts(new List<int>(), 0, true, out remainCount, 0, soTin);
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row, true);
                // product.shortDesc = HTMLRemoval.StripTagsRegexCompiled(product.shortDesc);
                filterPhoneList.Add(product);
            }

            ucFilterPhoneList.DataBind();
        }
    }
}