﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-sua-chua-cai-dat.aspx.cs" Inherits="PhongCachMobile.m_sua_chua_cai_dat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

<div role="main" class="ui-content subpage">
    <div class="content page-satic">
        <div class="content-banner product-list">
            <div class="content-center clearfix">
                <h2>Giới thiệu</h2>
                <p>Được sự lựa chọn tuyển dụng và sàng lọc gắt gao, kỹ thuật viên của <span class="color-red">PHONGCACHMOBILE</span> - đội ngũ Kỹ Thuật với bề dày trên 10 năm kinh nghiệm kết hợp với tinh thần học hỏi và nghiên cứu, đặc biệt, được sự công tác và hỗ trợ tối đa từ các Trung tâm bảo hành ủy quyền chính hãng Sony, Samung, HTC ... đã giúp cho PHONGCACHMOBILE có thể tự hào đảm nhận việc sửa chữa được những 'bệnh' khó của di động đặc biệt là những dòng cao cấp hiện nay của các hãng danh tiếng như iPhone, HTC, Nokia, Sony, Samsung, LG, Motorola, Blackberry...</p>

                <p>Ngoài ra, được sự hậu thuẫn từ hãng Pantech Vega (Hàn Quốc) cùng các nhà phân phối ủy quền tại VN, PHONGCACHMOBILE luôn sẵng sàng những linh kiện sửa chữa chính hãng phục vụ việc bảo hành và sửa chữa các dòng máy cao cấp của Vega.</p>

                <p>Đặc biệt, PHONGCACHMOBILE là đơn vị duy nhất có thể nhận <span class="color-red">sửa chữa các dòng tai nghe bluetooth cao cấp</span> của các thương hiệu nổi tiếng toàn cầu.</p>

                <p>Chúng tôi hân hạnh được phục vụ tất cả quý khách với tinh thần và trách nhiệm cao nhất.</p>
            </div>
        </div>


        <div class="product-list">
            <div class="content-center">
                <h2>Bảng giá apple</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>iPhone 4/4s</td>
                            <td>250.000đ</td>
                            <td>750.000đ</td> 
                        </tr>
                        <tr>
                            <td>iPhone 5/5s</td>
                            <td>350.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>iPhone 6</td>
                            <td>850.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>iPhone 6 Plus</td>
                            <td>950.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>iPad 2,3,4</td>
                            <td>900.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>iPad Air</td>
                            <td>1.200.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>iPad Air 2</td>
                            <td>1.650.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>iPad Mini 1,2</td>
                            <td>850.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <h2>Bảng giá samsung</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>S3</td>
                            <td>550.000đ</td>
                            <td>2.400.000đ</td> 
                        </tr>
                        <tr>
                            <td>S4</td>
                            <td>650.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>S5</td>
                            <td>750.000đ</td>
                            <td>3.400.000đ</td>
                        </tr>
                        <tr>
                            <td>S6</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>S6 Plus</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Note 3</td>
                            <td>650.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Note 4</td>
                            <td>1.250.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Note 5</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá sony</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Z</td>
                            <td>900.000đ</td>
                            <td>1.500.000đ</td> 
                        </tr>
                        <tr>
                            <td>Z Ultra</td>
                            <td>1.850.000đ</td>
                            <td>2.450.000đ</td>
                        </tr>
                        <tr>
                            <td>Z1</td>
                            <td>1.000.000đ</td>
                            <td>2.450.000đ</td>
                        </tr>
                        <tr>
                            <td>Z1 Compact</td>
                            <td>1.250.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Z2</td>
                            <td>1.350.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>Z3</td>
                            <td>1.450.000đ</td>
                            <td>2.950.000đ</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá nokia</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>520</td>
                            <td>500.000đ</td>
                            <td>750.000đ</td> 
                        </tr>
                        <tr>
                            <td>720</td>
                            <td>850.000đ</td>
                            <td>1.350.000đ</td>
                        </tr>
                        <tr>
                            <td>800</td>
                            <td>600.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>820</td>
                            <td>900.000đ</td>
                            <td>1.650.000đ</td>
                        </tr>
                        <tr>
                            <td>830</td>
                            <td>900.000đ</td>
                            <td>Call</td>
                        </tr>
						<tr>
                            <td>925</td>
                            <td>700.000đ</td>
                            <td>1.550.000đ</td>
                        </tr>
						<tr>
                            <td>1020</td>
                            <td>650.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá LG</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>G</td>
                            <td>700.000đ</td>
                            <td>Call</td> 
                        </tr>
                        <tr>
                            <td>G2</td>
                            <td>850.000đ</td>
                            <td>1.450.000đ</td>
                        </tr>
                        <tr>
                            <td>G Pro</td>
                            <td>950.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>GK</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>G3</td>
                            <td>1.450.000đ</td>
                            <td>1.950.000đ</td>
                        </tr>
						<tr>
                            <td>G4</td>
                            <td>Call</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá HTC</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>M7</td>
                            <td>850.000đ</td>
                            <td>Call</td> 
                        </tr>
                        <tr>
                            <td>M8</td>
                            <td>1.100.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>M9</td>
                            <td>1.350.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá ZenFone</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Kính cảm ứng</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>ZenFone 5</td>
                            <td>500.000đ</td>
                            <td>900.000đ</td> 
                        </tr>
                        <tr>
                            <td>ZenFone 6</td>
                            <td>750.000đ</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>FonePad</td>
                            <td>Call</td>
                            <td>1.250.000đ</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
				<h2>Bảng giá Sky Vega</h2>
                <div class="table">
                    <table>
                        <thead>
                            <tr>
                                <td>Sản phẩm</td>
                                <td>Full bộ</td>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>860</td>
                            <td>1.450.000đ</td>
                        </tr>
                        <tr>
                            <td>870</td>
                            <td>1.350.000đ</td>
                        </tr>
                        <tr>
                            <td>890</td>
                            <td>1.750.000đ</td>
                        </tr>
						<tr>
                            <td>900</td>
                            <td>1.450.000đ</td>
                        </tr>
                        <tr>
                            <td>910</td>
                            <td>Call</td>
                        </tr>
                        <tr>
                            <td>Sản phẩm khác (Kiểm tra báo giá trực tiếp)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="notice">
                    <h5 class="color-red">Lưu ý từ bộ phận kỹ thuật:</h5>
                    <p>+ Các vấn đề hư hỏng và thay thế linh kiện, xin vui lòng trực tiếp mang máy đến để kỹ thuật của chúng tôi có thể kiểm tra và tư vấn sửa chữa cho quý khách hàng được tốt hơn.</p>
                    <p>+ Đặc biệt nhận unlock tất cả những dòng ĐTDĐ nhà mạng xách tay từ nước ngoài.</p>
                    <p>+ Để được tư vấn sữa chữa - cài đặt, quý khách hàng vui lòng liên hệ 08 35.268.255/ nhấn 103 để gặp Kỹ thuật cài đặt, nhấn 104 để gặp Kỹ thuật phần cứng. Thời gian làm việc từ 10h - 18h trừ ngày Chủ Nhật.</p>
                    <p>Trung tâm sửa chữa - bảo hành PHONGCACHMOBILE cam kết chất lượng linh kiện và tính trung thực của toàn thể nhân viên kỹ thuật trong quá trình sửa chữa. Mọi trường hợp trái với cam kết, chúng tôi hoàn tiền gâp đôi cho quý khách hàng</p>
                </div>
                <h2>Bảng giá UNLOCK</h2>
                <div class="content-unlock">
                    <p>Samsung:<br />
                    Các dòng máy từ S4 trở về trước : 150K<br />
                    S5, Note 4, Note 3 ...                       : 400K</p>

                    <p>Nokia:<br />
                    Lumia 102, 520, 521, 820, 900, 920, 1520, 1020 AT&T : 400K  (Từ 1-5 ngày )</p>

                    <p>LG:<br />
                    150K- 200K</p>

                    <p>HTC:<br />
                    - Đời máy 2009 – 2013: 100K<br />
                    - Đời máy 2013-2014 : 200K<br />
                    - Đời máy 2014 : 250- 400K</p>

                    <p>iPhone: (Giá chỉ mang tính chất tham khảo vui lòng gọi trực tiếp kiểm tra)<br />
                    AT&T :<br />
                    - 3GS, 4, 4S, 5, 5S  :  300K ( 1-5 ngày )<br />
                    - 6, 6 PLUS : 500K ( 1- 5 ngày )<br />
                    - 4,4S, 5S, 6, 6+  ( Bablist ) : 1200K ( 1h- 48H)</p>

                    <p>T-mobile:<br />
                    - 4, 4s, 5, 5c, 5s : 2200K ( 1-10 ngày )<br />
                    - 6, 6 Plus : 2500k  ( 1 -10 ngày )</p>

                    <p>Australia :<br />
                    - 3 Australia (Three) for iPhone 3, 3s, 4, 4s ,5 ( Normal ) =  300K<br />
                    - 3 Hutchison Australia for iphone 3, 3s, 4, 4s, 5 ( Not Found )= 500K( 1-3 ngày)<br />
                    - Telstra Australia for iPhone 3, 3s, 4, 4s, 5  = 550k ( 1-48h)<br />
                    - Vodafone Australia iPhone 3, 3s, 4, 4s ,5 (Not Found) = 550k( 1-3 ngày )<br />
                    - Vodafone Australia iPhone 3, 3s, 4, 4s, 5 ( Normal )= 350K ( 1- 24h )<br />
                    - Optus / Virgin Australia for iPhone 3, 3s, 4, 4s, 5 = 700k( 1-3 ngày)</p>

                    <p>Korea :<br />
                    - Korea KT&SK iPhone 3, 3s,4, 4s, 5 = 300K ( 1-5 ngày)<br />
                    - Korea KT&SK iPhone 3, 3s, 4, 4s, 5 ( Badlist) = 1700K  ( 1-5 ngày )</p>

                    <p>France :<br />
                    -  Orange France iPhone 3, 3s, 4, 4s, 5,  5s  = 2100k ( 1-2 ngày )<br />
                    -  SFR France – iPhone 4G, 4S, 5 (Not Found) = 1200k ( 1-2 ngày )<br />
                    -  SFR France for iPhone 3, 3s, 4, 4s =350k ( 1-2 ngày )<br />
                    -  SFR France for iPhone 6, 6 Plus =1500k ( 1-2 ngày )<br />
                   ……</p>
                </div>
            </div>

        </div>
    </div>
</div>

</asp:Content>
