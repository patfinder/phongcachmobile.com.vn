﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile
{
    public partial class m_dang_nhap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            this.Title = LangController.getLng("litSystemLogin.Text", "Đăng nhập vào hệ thống");
            //this.litLogin.Text = LangController.getLng("litLogin.Text", "Đăng nhập");
            //this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + this.litLogin.Text;
            //this.lbtLogin.Text = "<span><span>" + this.litLogin.Text + "</span></span>";
            this.reqValEmail.ErrorMessage = this.reqValPassword.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
            this.rgeValEmail.ErrorMessage = LangController.getLng("litInvalidFormat.Text", "Sai định dạng");
            this.rgePasswordLength.Text = LangController.getLng("litPasswordLength", "Độ dài từ 6 - 30 kí tự.");

        }

        protected void lbtLogin_Click(object sender, EventArgs e)
        {
            LogInUserResult result = SysUserController.logIn(txtEmail.Text, txtPassword.Text);
            if (result.statusCode == LogInUserResult.STATUS_INVALID_CREDENTIAL_CODE)
            {
                this.lblLoginResponse.ForeColor = System.Drawing.Color.Red;
                this.lblLoginResponse.Text = result.statusText;
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
            }
            else if (result.statusCode == LogInUserResult.STATUS_INACTIVATED_USER_CODE)
            {
                this.lblLoginResponse.ForeColor = System.Drawing.Color.Red;
                this.lblLoginResponse.Text = result.statusText + "<br/>" + LangMsgController.getLngMsg("litInActivatedAccount.Text", "<font color='#000'>Hãy kiểm tra email để kích hoạt tài khoản hoặc bạn có thể yêu cầu <a class='color-1 link1' href='gui-lai-ma-xac-nhan'>gửi lại mã xác nhận</a>.</font>");
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
            }
            else if (result.statusCode == LogInUserResult.STATUS_DISABLE_USER_ERROR_CODE)
            {
                this.lblLoginResponse.ForeColor = System.Drawing.Color.Red;
                this.lblLoginResponse.Text = result.statusText + "<br/>";
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
            }
            else if (result.statusCode == LogInUserResult.STATUS_GENERAL_ERROR_CODE)
            {
                this.lblLoginResponse.ForeColor = System.Drawing.Color.Red;
                this.lblLoginResponse.Text = result.statusText;
                this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + result.statusText;
            }
            else
            {
                Session["Email"] = txtEmail.Text;
                DataTable dt = SysUserController.getUserByEmail(txtEmail.Text);
                if (dt != null && dt.Rows.Count > 0)
                    Session["UserId"] = dt.Rows[0]["Id"].ToString();
                if (Session["RefUrl"] != null && Session["RefUrl"].ToString() != "")
                {
                    if (Session["RefUrl"].ToString().Contains("thong-tin-"))
                        Response.Redirect(Session["RefUrl"].ToString() + "#_comment");
                    else
                        Response.Redirect(Session["RefUrl"].ToString());
                }
                else
                    Response.Redirect("gia-hot", true);
            }
        }


    }
}