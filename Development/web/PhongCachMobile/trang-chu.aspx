﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="trang-chu.aspx.cs" Inherits="PhongCachMobile.trang_chu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Trang chủ</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        <section class="home-top clearfix">
            <div class="specify">
                <div class="slide-home">
                    <% foreach(PhongCachMobile.model.Article article in bannerItems) { %>
                    <div class="items"><a href="<%= article.link %>" ><img src="images/slide/<%= article.displayImg %>" alt="" /></a></div>
                    <% } %>
                </div>
            </div>
            <ul class="list-product">
                <% PhongCachMobile.model.Product product = null; %>
                <%-- -------------Hot Products------------- --%>
                <% for(int i=0; i<2; i++) {
                       product = hot2Products[i];
                    %>
                <li <%= i == 0 ? "" : "class='left-text'" %> >
                    <figure <%= i == 0 ? "class='clearfix'" : "" %> >
                        <div class="container-img">
                            <div <%= i == 0 ? "class='tb-middle'" : "" %> ><a href="<%= product.link %>"><img src="images/product/<%= product.firstImage %>" alt="<%= product.name %>" /></a></div>
                        </div>
                        <figcaption class="<%= i == 0 ? "bg-black" : "bg-blue" %>">
                            <h2><%= product.name %></h2>
                            <span class="price"><%= product.currentPriceStr %></span>
                            <% if (product.discountPrice > 0) { %><span class="price-regular">(<strike><%= product.discountPriceStr %></strike>)</span><% } %>
                            <p class="intro"><%= product.shortDesc %></p>
                            <div class="group">
                                <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                                <%= product.discountPrice > 0 ? "<span class='hot'></span>" : "" %>
                            </div>
                            <a href="<%= product.link %>" class="btn-view"><span>Xem chi tiết</span></a>
                        </figcaption>
                    </figure>
                </li>
                <% } %>
            </ul>
        </section>
        <section class="home-top section-second clearfix">
            <ul class="list-product">
                <%-- -------------Middle Random Product------------- --%>
                <% for (int i = 0; i < rand2Products.Count; i++)
                   {
                       product = rand2Products[i];
                    %>
                <li>
                    <figure <%= i == 0 ? "class='clearfix'" : "" %> >
                        <div class="container-img">
                            <div class="tb-middle"><a href="<%= product.link %>"><img src="images/product/<%= product.firstImage %>" alt="<%= product.name %>" /></a></div>
                        </div>
                        <figcaption class="<%= i == 0 ? "bg-blue-drak" : "bg-black" %>">
                            <h2><%= product.name %></h2>
                            <span class="price"><%= product.currentPriceStr %></span>
                            <% if (product.discountPrice > 0) { %><span class="price-regular">(<strike><%= product.discountPriceStr %></strike>)</span><% } %>
                            <p class="intro"><%= product.shortDesc %></p>
                            <div class="group">
                                <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                                <%= product.discountPrice > 0 ? "<span class='hot'></span>" : "" %>
                            </div>
                            <a href="<%= product.link %>" class="btn-view"><span>Xem chi tiết</span></a>
                        </figcaption>
                    </figure>
                </li>
                <% } %>
            </ul>
            <ul class="list-product">
                <li class="left-text">
                    <figure class="clearfix" >
                        <div class="container-img">
                            <div class="tb-middle"><a href="<%= randAccessory.link %>"><img src="images/product/<%= randAccessory.firstImage %>" alt="<%= randAccessory.name %>" /></a></div>
                        </div>
                        <figcaption class="bg-blue-light">
                            <h2><%= randAccessory.name %></h2>
                            <span class="price"><%= randAccessory.currentPriceStr %></span>
                            <p class="intro"><%= randAccessory.shortDesc %></p>
                            <a href="<%= randAccessory.link %>" class="btn-view"><span>Xem chi tiết</span></a>
                        </figcaption>
                    </figure>
                </li>
                <li class="left-text">
                    <figure >
                        <div class="container-img">
                            <div class="tb-middle"><a href="<%= randBottomProduct.link %>"><img src="images/oldproduct/<%= randBottomProduct.firstImage %>" alt="<%= randBottomProduct.name %>" /></a>
                            <a href='kho-may-cu' class="btn-view"><span>Xem tất cả</span></a>
                            </div>
                        </div>
                        <figcaption class="bg-red">
                            <h2><%= randBottomProduct.name %></h2>
                            <span class="price"><%= randBottomProduct.currentPriceStr %></span>
                            <% if (randBottomProduct.discountPrice > 0)
                               { %><span class="price-regular">(Máy mới <%= randBottomProduct.discountPriceStr %>)</span><% } %>
                            <div class="intro"><%= randBottomProduct.shortDesc %></div>

                        </figcaption>
                    </figure>
                </li>
            </ul>
        </section>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">

    <script type="text/javascript">
        $(document).ready( function(){
            $('.slide-home').slick({
                dots: false,
                infinite: true,
                slidesToShow: 1,
                variableWidth:true,
                autoplay: true,
                autoplaySpeed: 7000,
                arrows: true
            });
            var w_secify = $('.specify').width();
            $('.slide-home .items').css({'width': w_secify + 'px'});
            var h_box = $('.home-top .specify').height()/2;
            $('.home-top figcaption').css({'height': h_box + 'px'});
            $('.home-top .tb-middle').css({'height': h_box + 'px'});
            $(window).resize( function(){
                w_secify = $('.specify').width();
                $('.slide-home .items').css({'width': w_secify + 'px'});
                h_box = $('.home-top .specify').height()/2;
                $('.home-top figcaption').css({'height': h_box + 'px'});
                $('.home-top .tb-middle').css({'height': h_box + 'px'});

            });
        });
    </script>

</asp:Content>