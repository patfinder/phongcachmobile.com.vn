﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.controller;
using System.Text;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile.ajax
{
    /// <summary>
    /// Summary description for GetProduct
    /// </summary>
    public class GetProduct : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string productName = context.Request.QueryString["q"].ToString();

            DataTable dt = ProductController.getProductsByName(productName);
            StringBuilder sb = new StringBuilder();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    sb.AppendLine(dr["Name"].ToString() + "|" + HTMLRemoval.removeSpecialChar(HTMLRemoval.StripTagsRegex(dr["Name"].ToString())).ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString());
                }
            }
            context.Response.Write(sb);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}