﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.controller;
using System.Web.SessionState;

namespace PhongCachMobile.ajax
{
    /// <summary>
    /// Summary description for UpdateLastRead
    /// </summary>
    public class UpdateLastRead : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            bool dt = SysUserController.updateLastRead(context.Session["UserId"].ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}