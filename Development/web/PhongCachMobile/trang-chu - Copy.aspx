﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="trang-chu.aspx.cs" Inherits="PhongCachMobile.trang_chu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Trang chủ</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        <section class="home-top clearfix">
            <div class="specify">
                <div class="slide-home">
                    <div class="items"><img src="images/home/lazada.jpg" alt="#"></div>
                    <%--TODO: 3 images--%>
                    <%--<div class="items"><img src="images/home/lazada.jpg" alt="#"></div>
                    <div class="items"><img src="images/home/lazada.jpg" alt="#"></div>--%>
                </div>
            </div>
            <ul class="list-product">
                <% PhongCachMobile.model.Product product = null; %>
                <% for(int i=0; i<2; i++) {
                       product = hot2Products[i];
                    %>
                <li <%= i == 0 ? "" : "class='left-text'" %> >
                    <figure <%= i == 0 ? "class='clearfix'" : "" %> >
                        <div class="container-img">
                            <div <%= i == 0 ? "class='tb-middle'" : "" %> ><img src="images/product/<%= product.firstImage %>" alt="<%= product.name %>" /></div>
                        </div>
                        <figcaption class="<%= i == 0 ? "bg-black" : "bg-blue" %>">
                            <h2><%= product.name %></h2>
                            <span class="price"><%= product.currentPriceStr %>đ</span>
                            <% if (!string.IsNullOrEmpty(product.discountPriceStr)) { %><span class="price-regular">(<strike><%= product.discountPriceStr %>đ</strike>)</span><% } %>
                            <p class="intro"><%= product.shortDesc %></p>
                            <div class="group">
                                <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                                <%= product.isSpeedySold ? "<span class='hot'></span>" : "" %>
                            </div>
                            <a href="<%= product.remark %>" class="btn-view"><span>Xem chi tiết</span></a>
                        </figcaption>
                    </figure>
                </li>
                <% } %>
            </ul>
        </section>
        <section class="home-top section-second clearfix">
            <ul class="list-product">
                <% for(int i=0; i<2; i++) {
                       product = rand2Products[i];
                    %>
                <li>
                    <figure <%= i == 0 ? "class='clearfix'" : "" %> >
                        <div class="container-img">
                            <div class="tb-middle"><img src="images/product/<%= product.firstImage %>" alt="<%= product.name %>" /></div>
                        </div>
                        <figcaption class="<%= i == 0 ? "bg-blue-drak" : "bg-black" %>">
                            <h2><%= product.name %></h2>
                            <span class="price"><%= product.currentPriceStr %>đ</span>
                            <% if (!string.IsNullOrEmpty(product.discountPriceStr)) { %><span class="price-regular">(<strike><%= product.discountPriceStr %>đ</strike>)</span><% } %>
                            <p class="intro"><%= product.shortDesc %></p>
                            <div class="group">
                                <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                                <%= product.isSpeedySold ? "<span class='hot'></span>" : "" %>
                            </div>
                            <a href="<%= product.remark %>" class="btn-view"><span>Xem chi tiết</span></a>
                        </figcaption>
                    </figure>
                </li>
                <% } %>
            </ul>
            <ul class="list-product">
                <li class="left-text">
                    <figure class="clearfix" >
                        <div class="container-img">
                            <div class="tb-middle"><img src="images/game/<%= randBottomGame.displayImage %>" alt="<%= randBottomGame.name %>" /></div>
                        </div>
                        <figcaption class="bg-blue-light">
                            <h2><%= randBottomGame.name %></h2>
                            <span class="price-free">Miễn phí</span>
                            <p class="intro"><%= randBottomGame.description %></p>
                            <a href="<%= randBottomGame.remark %>" class="btn-view"><span>Tải game về</span></a>
                        </figcaption>
                    </figure>
                </li>
                <li class="left-text">
                    <figure >
                        <div class="container-img">
                            <div class="tb-middle"><img src="images/product/<%= randBottomProduct.firstImage %>" alt="<%= randBottomProduct.name %>" /></div>
                        </div>
                        <figcaption class="bg-red">
                            <h2><%= randBottomProduct.name %></h2>
                            <span class="price"><%= randBottomProduct.currentPriceStr %>đ</span>
                            <% if (!string.IsNullOrEmpty(randBottomProduct.discountPriceStr))
                               { %><span class="price-regular">(<strike><%= randBottomProduct.discountPriceStr %>đ</strike>)</span><% } %>
                            <div class="intro"><%= randBottomProduct.shortDesc %></div>
                            <a href="<%= randBottomProduct.remark %>" class="btn-view"><span>Xem chi tiết</span></a>
                        </figcaption>
                    </figure>
                </li>
            </ul>
        </section>
    </div>

</asp:Content>
