﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class m_loai_mtb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.MHome)this.Master).TemplateType = TemplateType.NewType;

            // Brand
            string idStr = Request.QueryString["id"];
            if (!string.IsNullOrWhiteSpace(idStr))
            {
                // OS
                if (idStr.StartsWith(Common.m() + "hdh-"))
                {
                    activeOSID = int.Parse(idStr.Split('-').Last());
                    activeOSName = PhongCachMobile.controller.OperatingSystemController.getOSNameById(activeOSID.ToString());
                }
                // Feature
                else if (idStr.StartsWith(Common.m() + "tn-"))
                {
                    activeFeatureID = int.Parse(idStr.Split('-').Last());
                    activeFeatureName = PhongCachMobile.controller.FeatureController.getFeatureNameById(activeFeatureID.ToString());
                }
                // Brand
                else
                {
                    activeBrandID = int.Parse(idStr.Split('-').Last());
                    activeBrandName = PhongCachMobile.controller.CategoryController.getCategoryNameById(activeBrandID.ToString());
                }

                hideBanners = true;
            }

            if (!this.IsPostBack)
            {
                //loadBannerItems();
                loadTopBannerItems();
                loadFilterValues();
                loadFilterPhoneList();
            }
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "20"));

        // Hide Banner
        protected bool hideBanners = false;

        // Active Brand
        protected int activeBrandID = -1;
        protected string activeBrandName = "Tất cả";
        // Active OS
        protected int activeOSID = -1;
        protected string activeOSName = "Tất cả";
        // Feature
        protected int activeFeatureID = -1;
        protected string activeFeatureName = "Tất cả";

        protected List<Article> topBannerLeftItems = new List<Article>();
        protected List<Article> topBannerRightItem1s = new List<Article>();
        protected List<Article> topBannerRightItem2s = new List<Article>();
        private void loadBannerItems()
        {
            // MTB_Top_Banner_Left_Items // V2-Tablet - Sliders Left
            foreach (DataRow dr in SliderController.getSliders("59").Rows)
            {
                topBannerLeftItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // MTB_Top_Banner_Right_Items1 // V2-Tablet - Sliders Right 1
            foreach (DataRow dr in SliderController.getSliders("60").Rows)
            {
                topBannerRightItem1s.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // MTB_Top_Banner_Right_Items2 // V2-Tablet - Sliders Right 2
            foreach (DataRow dr in SliderController.getSliders("61").Rows)
            {
                topBannerRightItem2s.Add(SliderController.rowToSlider(dr).toArticle());
            }

            //ucTripleBanner.DataBind();
        }

        protected List<Article> topBannerItems = new List<Article>();
        private void loadTopBannerItems()
        {
            // Trang_Chu_Banner_Images // V2-Home Page - Sliders
            foreach (DataRow dr in SliderController.getSliders("59").Rows)
            {
                topBannerItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
        }

        protected Dictionary<string, int> prices = new Dictionary<string, int>();
        protected Dictionary<string, int> oses = new Dictionary<string, int>();
        protected int appleCount = 0;
        protected int samsungCount = 0;
        protected int otherCount = 0;
        protected int promotionCount = 0;

        private void loadFilterValues()
        {
            // Prices
            prices = ProductController.PriceList;

            // OSes
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId(ProductController.ProductGroupTablet);
            if (dt != null && dt.Rows.Count > 0)
            {
                oses.Add("Tất cả", -1);
                foreach (DataRow dr in dt.Rows)
                {
                    oses.Add(dr["Name"].ToString(), int.Parse(dr["Id"].ToString()));
                }
            }

            appleCount = ProductController.getProductCountByCategory(ProductController.ProductGroupTabletInt, ProductController.TabletCategoryApple);
            samsungCount = ProductController.getProductCountByCategory(ProductController.ProductGroupTabletInt, ProductController.TabletCategorySamsung);
            otherCount = ProductController.getProductCountByCategory(ProductController.ProductGroupTabletInt, ProductController.TabletCategoryOther);
            promotionCount = ProductController.getDiscountProductCount(ProductController.ProductGroupTabletInt);
        }

        protected int remainCount = 0;
        protected List<Product> filterPhoneList = new List<Product>();
        private void loadFilterPhoneList()
        {
            DataTable dt = null;

            if (activeFeatureID > 0)
            {
                dt = FeatureController.getProductByFeature(ProductController.ProductGroupTabletInt, -1, activeFeatureID);
            }
            else
            {
                dt = ProductController.getProductsByGroupId(
                     ProductController.ProductGroupTabletInt, activeBrandID, -1, activeOSID, false, true, out remainCount);
            }
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);
                filterPhoneList.Add(product);
            }

            ucFilterPhoneList.DataBind();
        }
    }
}