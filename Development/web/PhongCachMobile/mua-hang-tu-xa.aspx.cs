﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class mua_hang_tu_xa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            litDistanceBuying.Text = LangController.getLng("litDistanceBuying.Text", "Mua hàng từ xa");
            litDistanceBuyingContent.Text = LangMsgController.getLngMsg("litDistanceBuying.Text", "<div class='block-content' style='background: white; padding: 25px 30px 16px 30px;" + 
                                    "line-height: 18px; color: #515151;'>" + 
                                    "<p style='font-weight: bold;'>" + 
                                        "Quý khách hàng liên hệ, trao đổi với chúng tôi trực tiếp từ số điện thoại trên website " + 
                                        "hoặc email: <font style='color: red; font-weight: bold;'>phongcach_mobile@yahoo.com</font> để đặt mua hàng. Sau khi thống nhất, quý " + 
                                        "khách hàng gởi tiền đến tài khoản của PhongCachMobile. Ngay khi nhận được tiền, " +
                                        "chúng tôi sẽ gởi hàng đến tận nơi theo yêu cầu của Quý khách.</p>" + 
                                    "<div class='box-container'>" + 
                                        "<div class='about-page'>" + 
                                            "<i class='icon-shopping-cart'>like</i><div class='extra-wrap'>" + 
                                                "<h3>Phương thức thanh toán</h3>" + 
                                                "<p style='line-height: 18px; padding-top: 8px;'>" + 
                                                    "Quý Khách có thể thanh toán bằng cách nhờ người thân hoặc bạn bè " + 
                                                    "<font style='color: red; font-weight: bold;'>thanh toán trực tiếp</font> tại PhongCachMobile Store hoặc " + 
                                                    "<font style='color: red; font-weight: bold;'>thanh toán chuyển khoản</font>" + 
                                                "<ul style='width:30%'>" + 
                                                    "<li>Tên tài khoản: Lê Hoàng Vũ</li>" +
                                                    "<li>Tài khoản số:  140.22312.808011</li>" + 
                                                    "<li>Ngân hàng: Techcombank - CN Tân Định</li>" + 
                                                "</ul>" + 
                                                 "<ul  style='width:30%'>" + 
                                                    "<li>Tên tài khoản: Lê Hoàng Vũ</li>" + 
                                                    "<li>Tài khoản số:  140.22312.808011</li>" + 
                                                    "<li>Ngân hàng: Techcombank - CN Tân Định</li>" + 
                                                "</ul>" + 
                                                  "<ul  style='width:30%'>" + 
                                                    "<li>Tên tài khoản: Lê Hoàng Vũ</li>" + 
                                                    "<li>Tài khoản số: 19022.0624.6861</li>" + 
                                                    "<li>Ngân hàng: Nông nghiệp & Phát triển Nông thôn Việt Nam - CN Bến Thành</li>" + 
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                        "<div class='about-page'>" + 
                                            "<i class='icon-money'>like</i><div class='extra-wrap'>" + 
                                                "<h3>Chi phí chuyển phát nhanh và mức bồi thường</h3>" + 
                                                "<p style='line-height: 18px; padding-top: 8px;'>" + 
                                                    "Chi phí chuyển phát nhanh hàng hoá trong phạm vi lãnh thổ Việt Nam phụ " + 
                                                    "thuộc vào vị trí địa lý của khách hàng xa hay gần so với PhongCachMobile và giá trị của hàng hoá chuyển phát " + 
                                                    "(Ví dụ: Mức cước chuyển phát nhanh HCM - Hà Nội là 80.000đ áp dụng cho hàng hoá có mức giá trị 10.000.000đ). " + 
                                                    "Để biết mức cước cụ thể, khách hàng vui lòng trao đổi chi tiết với nhân viên bán hàng.</p> " + 
                                                "<p style='line-height: 18px; padding-top: 8px;margin-left:92px;'>Mức bồi thường trong trường hợp thất lạc hoặc hư hỏng do chuyển phát nhanh:</p> " +
                                                "<ul style='margin-left:92px;'> " + 
                                                    "<li>Nếu khách hàng chỉ thanh toán phí chuyển phát nhanh, Công ty chuyển phát nhanh sẽ bồi hoàn 50% giá trị hàng hoá.</li>" + 
                                                    "<li>Nếu khách hàng thanh toán phí chuyển phát nhanh + phí bảo hiểm (phí bảo hiểm = 3% giá trị hàng hoá), Công ty chuyển phát nhanh sẽ bồi hoàn 100% giá trị hàng hoá.</li>" + 
                                                "</ul>" + 
                                                "<p style='line-height: 18px; padding-top: 8px;margin-left:92px;color:red'>Xin quý khách lưu ý:</p>" + 
                                                "<ul style='margin-left:92px;'>" + 
                                                    "<li>Chi phí chuyển hàng đến tận nơi theo yêu cầu của Quý khách hàng ở xa (Chuyển phát nhanh - Bảo đảm) sẽ được thỏa thuận ngay lúc đặt mua hàng.</li>" + 
                                                    "<li>Sau khi chuyển tiền cho chúng tôi đề nghị quý khách thông báo lại bằng tin nhắn SMS hoặc điện thoại " + 
                                                    "cho chúng tôi để chúng tôi đóng gói và chuyển phát nhanh EMS tới địa chỉ của người mua.</li>" + 
                                                    "<li>Khi nhận hàng, rất mong quý khách trình CMND, tháo mở cẩn thận và kiểm tra thật kỹ quy cách hàng hoá và số lượng linh kiện " + 
                                                    "đi kèm trước khi ký biên bản nhận hàng. Mọi khiếu nại về quy cách hàng hoá và linh kiện " + 
                                                    "đi kèm sẽ không được giải quyết sau khi khách đã đồng ý nhận hàng.</li>" + 
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                        "<div class='about-page'>" + 
                                            "<i class='icon-truck'>like</i>" + 
                                            "<div class='extra-wrap'>" + 
                                                "<h3>Phương thức chuyển hàng</h3>" + 
                                                "<ul>" + 
                                                    "<li>Đối với khách hàng ở Thành phố - thị xã: Chúng tôi sẽ chuyển hàng qua dịch vụ chuyển phát nhanh EMS, Quý khách có thể nhận được hàng sau 24h.</li>" + 
                                                    "<li>Đối với khách hàng ở các Huyện, vùng xa: Chúng tôi sẽ chuyển hàng qua dịch vụ chuyển phát nhanh EMS, Quý khách có thể nhận được hàng sau 48h.</li>" + 
                                                    "<li>Đối với khách hàng ở các khu vực không có dịch vụ chuyển phát nhanh EMS chúng tôi sẽ chuyển hàng qua xe khách, hoặc chuyển bảo đảm qua Bưu điện.</li>" + 
                                                "</ul>" + 
                                                "<p style='line-height: 18px; padding-top: 8px;margin-left:92px;'>Tất cả các mặt hàng đều được chúng tôi kiểm tra kỹ càng trước khi gửi đến quý khách.</p>" + 
                                                "<p style='line-height: 18px; padding-top: 8px;margin-left:92px;'>Xin quý khách vui lòng lưu ý chi tiết về tất cả các nội dung trên đặc biệt là  " + 
                                                "<font style='color: red; font-weight: bold;'>mức bồi thường</font> để tránh những trường hợp ngoài ý muốn xảy ra.</p>" + 
                                            "</div>" + 
                                        "</div>" + 
                                    "</div>" + 
                                "</div>");
        }
    }
}