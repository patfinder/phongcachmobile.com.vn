﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="chi-tiet-san-pham.aspx.cs" Inherits="PhongCachMobile.chi_tiet_san_pham" %>

<%@ Register Src="~/master/uc/ucDigitalToys.ascx" TagName="ucDigitalToys" TagPrefix="ucDigitalToys" %>
<%@ Register Src="~/master/uc/ucProductList.ascx" TagName="ucProductList" TagPrefix="ucProductList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title><%= PhongCachMobile.controller.LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + product.name %></title>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        <div class="breadcrumb">
            <div class="content-center">
                <ol>
                    <li><a href="/">Trang chủ</a><i class="fa fa-angle-right"></i></li>
                    <li><a href="<%= breadcrumCategory.link %>"><%= breadcrumCategory.title %></a><i class="fa fa-angle-right"></i></li>
                    <li><a href="<%= category.link %>"><%= category.title %></a><i class="fa fa-angle-right"></i></li>
                    <li class="active"><%= product.name %></li>
                </ol>
            </div>
        </div>
        <section class="page-detail">
            <div class="content-information clearfix" style="background: url('<%= backgroundImage %>') no-repeat top center fixed; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <div class="content-center">
                    <div class="content-detail clearfix">
                        <div class="img">
                            <img src="images/product/<%= product.firstImage %>">
                        </div>
                        <div class="content-text">
                            <h1 <%= DarkBG ? "style='color: #000;'" : "" %> ><%= product.name %></h1>
                            <span class="price" <%= DarkBG ? "style='color: #C72803;'" : "" %> ><%= product.currentPriceStr %></span>
                                <% if(product.discountPrice > 0) { %>
                                    <span class="price-regular">(<strike><%= product.discountPriceStr %>)</strike>
                                    </span><% } %>
                            <div class="group">
                                <%= product.isGifted ? "<span class='gift'></span>" : "" %>
                                <%= product.discountPrice > 0 ? "<span class='hot'></span>" : "" %>
                            </div>
                            <div class="description">
                                <div class="box-padding">
                                    <div <%= DarkBG ? "style='color: #333;'" : "" %> ><%= product.discountDesc %></div>
                                    <% if (!string.IsNullOrWhiteSpace(product.include)) { %>
                                    <div class="ralated">
                                        <h4>Phụ kiện kèm theo máy:</h4>
                                        <%= product.include %>
                                    </div>
                                    <% } %>
                                </div>
                            </div>
                            <div class="social">
                                <div class="share bordercolor">
	                                <!-- AddThis Button BEGIN -->
	                                <div class="addthis_toolbox addthis_default_style ">
		                                <span class='st_facebook_hcount' displaytext='Facebook'></span><span class='st_twitter_hcount'
			                                displaytext='Tweet'></span><span class='st_googleplus_hcount' displaytext='Google +'>
			                                </span><span class='st_email_hcount' displaytext='Email'></span>
	                                </div>
                                </div>
                                <div class="clearblock"></div>
                            </div>
                        </div>
                    </div>
                    <div id="navbar-scroll" class="tabs">
                        <% var color = DarkBG ? "style='color: #333;'" : "style='color: #FFF;'"; %>
                        <ul class="nav navbar-nav" >
                            <li><a href="#highlight" <%= color %> >Điểm nổi bật<span class="caret"><span></span></span></a></li>
                            <% if(new int[]{3, 4}.Contains(product.groupId)){ %>
                            <li><a href="#technology" <%= color %> >Thông số kỹ thuật<span class="caret"><span></span></span></a></li>
                            <% } %>
                            <li><a href="#article-details" <%= color %> >Bài viết chi tiết<span class="caret"><span></span></span></a></li>
                            <%--<li data-class="article-used" id="article-used">Thủ thuật sử dụng<span class="caret"><span></span></span></li>--%>
                            <% if(!string.IsNullOrWhiteSpace(product.video)) { %>
                            <li><a href="#videos" <%= color %> >Video giới thiệu<span class="caret"><span></span></span></a></li>
                            <% } %>
                            <li><a href="#comments" <%= color %> ><span class="fb-comments-count" data-href="<%= PageUrl %>">0</span> Bình luận<span class="caret"><span></span></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="list-details">

                <div class="content-center">
                    <h2 id="highlight" class="highlight">ĐẶC ĐIỂM NỔI BẬT</h2>
                </div>

                <div class="<%= product.allImages.Count <= 1 ? "no-slide" : "slide-show" %>">
                    <% foreach(string image in product.allImages) { %>
                    <div class="items"><img src="images/product/<%= image %>" alt=""></div>
                    <% } %>
                </div>
                <div class="slide-text">
                    <% foreach(string desc in product.allImageDescs) { %>
                      <div class="items"><%= desc %></div>
                    <% } %>
                </div>

                <div class="group-infor content-center clearfix">
                    <div class="content-main">
                        <% if(new int[]{3, 4}.Contains(product.groupId)){ %>
                        <article class="technology">
                            <h2 id="technology">Thông số kỹ thuật</h2>
                            <table>
                                <% foreach(PhongCachMobile.model.Article article in productFeatures1) { %>
                                <tr>
                                    <td style="max-width: 30%"><%= article.title %></td>
                                    <td><p><%= article.longDesc %></p></td>
                                </tr>
                                <% } %>
                            </table>
                            <a href="javascript:void(0);" class="read-more close">Thu nhỏ thông số kỹ thuật</a>
                            <div class="technology-full">
                                <table>
                                <% foreach(PhongCachMobile.model.Article article in productFeatures2) { %>
                                <tr>
                                    <td style="max-width: 30%"><%= article.title %></td>
                                    <td><p><%= article.longDesc %></p></td>
                                </tr>
                                <% } %>
                                </table>
                            </div>
                            <a href="javascript:void(0);" class="read-more open">Xem đầy đủ thông số kỹ thuật</a>
                            <a href="javascript:void(0);" class="read-more close">Thu nhỏ thông số kỹ thuật</a>
                        </article>
                        <% } %>

                        <article id="article-details" class="article-details">
                            <h2>Bài viết chi tiết</h2>
                            <div class="content-short">
                                <%= product.longDesc %>
                            </div>

                            <a href="javascript:void(0);" class="read-more close">Thu nhỏ Bài viết chi tiết</a>
                            <div class="content-text">
                                <div class="content-more">
                                    <%= product.longDesc %>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="read-more open">Xem đầy đủ Bài viết chi tiết</a>
                            <a href="javascript:void(0);" class="read-more close">Thu nhỏ Bài viết chi tiết</a>
                        </article>
                        <%--<article class="article-used">
                            <h2>Thủ thuật sử dụng</h2>
                            <ul>
                                <li><a href="#" class="title">title test</a>
                                    <div class="content-description">
                                        <p>Sony Xperia SP là phiên bản nâng cấp từ mẫu Sony Xperia SL ra mắt năm ngoài. Đây là điện thoại thông minh chú trọng đến thiết kế, tính năng và phần mềm tiện ích bên trong.<br />
                                        Cấu hình của máy gồm : Bộ xử lý lõi kép nhân Krait tốc độ 1,7 GHz, Qualcomm MSM8960T S4 Pro chipset, đồ họa Adreno 320 , bộ nhớ 8 GB và RAM 1 GB và chạy hệ điều hành Android 4.1.2 (Jelly Bean).<br />
                                        - See more at: http://25.144.140.76:8060/sp-sony-xperia-sp-385#sthash.YrSkY6sn.dpuf</p>
                                    </div>
                                </li>
                            </ul>
                        </article>--%>

                        <% if(!string.IsNullOrWhiteSpace(product.video)) { %>
                        <article  id="videos" class="videos">
                            <h2>Video giới thiệu</h2>
                            <div class="content-video">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/<%= product.video %>" frameborder="0" allowfullscreen>
                                    <object width='720' height='400'>
                                        <param name='movie' value='https://www.youtube.com/v/<%= product.video %>?version=3'></param>
                                        <param name='allowFullScreen' value='true'></param>
                                        <param name='allowScriptAccess' value='always'></param>
                                        <embed src='https://www.youtube.com/v/<%= product.video %>?version=3' type='application/x-shockwave-flash' allowfullscreen='true' allowScriptAccess='always' width='720' height='400'></embed></object>
                                </iframe>
                            </div>
                        </article>
                        <% } %>

                        <article id="comments" class="comments">
                            <h2>Bình luận</h2>
                            <!-- ------------------ Comments ------------------ -->
                            <%--<img src="images/product/comments.jpg">--%>
                            
                            <div id="fb-root"></div>
                            <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s); js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=862852357144009"; // 120360814669224 i-want
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>

                            <div class="fb-comments" data-href="<%= PageUrl %>" data-numposts="5"></div>
                            <!-- ------------------ END Comments ------------------ -->
                        </article>
                    </div>
                    
                    <ucDigitalToys:ucDigitalToys ID="ucDigitalToys" runat="server" />

                </div>
                <div class="product-list">
                    <div class="content-center">

                        <% if(accessories.Count > 0) { %>
                        <h2>Phụ kiện đề xuất</h2>

                        <ucProductList:ucProductList ID="ucAccessories" ProductList="<%# accessories %>" runat="server" />
                        <% } %>

                        <% if(similarProducts.Count > 0) { %>
                        <h2><%= IsAccessory ? "Sản phẩm cùng Loại" : "Sản phẩm cùng Giá" %></h2>
                        
                        <ucProductList:ucProductList ID="ucSimilarProducts" ProductList="<%# similarProducts %>" runat="server" />
                        <% } %>
                    </div>
                </div>
            </div>
        </section>
    </div>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {
            var topPadding = 50;
            var top_tabs = $('.tabs').offset().top;
            if($('.highlight').length > 0)
                var top_highlight = $('.highlight').offset().top - topPadding;
            if($('.technology').length > 0)
                var top_technology = $('.technology').offset().top - topPadding;
            if($('.article-details').length > 0)
                var top_details = $('.article-details').offset().top - topPadding;
            //var top_used = $('.article-used').offset().top - topPadding;
            if($('.videos').length > 0)
                var top_videos = $('.videos').offset().top - topPadding;
            if($('.comments').length > 0)
                var top_comments = $('.comments').offset().top - topPadding;
            $('.slide-show').slick({
                //centerMode: true,
                slidesToShow: 1,
                //variableWidth: true,
                autoplay: true,
                autoplaySpeed: 7000,
                centerMode: true,
                variableWidth: true,
                asNavFor: '.slide-text'
            });
            $('.slide-text').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: false,
                asNavFor: '.slide-show',
                dots: false,
                arrows: false
            });

            $('.tabs li a').click(function (e) {
                e.preventDefault();
                $('.tabs li').removeClass('active');
                $(this).parent().addClass('active');
                var id_scroll = $(this).attr('href');
                $('html,body').animate({
                    scrollTop: $(id_scroll).offset().top - 50
                },
                800);
            });
            $('body').scrollspy({ target: '#navbar-scroll' });
            $(window).scroll(function () {
                var top_scroll = $(this).scrollTop();
                if($('.technology').length > 0)
                    top_technology = $('.technology').offset().top - topPadding;
                if($('.article-details').length > 0)
                    top_details = $('.article-details').offset().top - topPadding;
                if($('.videos').length > 0)
                    top_videos = $('.videos').offset().top - topPadding;
                if($('.comments').length > 0)
                    top_comments = $('.comments').offset().top - topPadding;
                if (top_scroll >= top_tabs - 100) {
                    $('.tabs').addClass('fixTop');
                    $('.menu-bar').removeClass('fixTop');
                }
                else {
                    $('.tabs').removeClass('fixTop');
                    $('.menu-bar').addClass('fixTop');
                }
                if($(this).scrollTop() < 138){
                    $('.menu-bar').removeClass('fixTop');
                }


            });

            function cut(n) {
                return function textCutter(i, text) {
                    var short = text.substr(0, n);
                    if (/^\S/.test(text.substr(n)))
                        return short.replace(/\s+\S*$/, "");
                    return short;
                };
            }
            $('.article-details .content-short').text(cut(505));
            var short_text  = $('.article-details .content-short').text();
            $('.article-details .content-short').html("<p>" + short_text + "...</p>");

            //show list thủ thuật sử dụng

            $('.article-used a.title').on('click', function(e){
                e.preventDefault();
                var $this=$(this);
                $('.article-used a.title').removeClass('active');
                $('.article-used .content-description').removeClass('open');
                $this.addClass('active')
                    .next().addClass('open');
            });

        });
    </script>

</asp:Content>