﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class quy_dinh_bao_hanh : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            litGuaranteeRule.Text = this.Title =  LangController.getLng("litGuaranteeRule.Text", "Quy định bảo hành");
            litGuaranteeRuleContent.Text = LangMsgController.getLngMsg("litGuaranteeRule.Text", "<div class='block-content' style='background: white; padding: 15px 30px 16px 30px;" +
                                    "line-height: 18px; color: #515151;'>" +
                                    "<p style='text-transform: uppercase; color: red;' class='block-subtitle'>" +
                                        "Chế độ bảo hành \"5 sao\" đối với sản phẩm điện thoại di động và máy tính bảng duy " +
                                        "nhất chỉ có tại PhongCachMobile</p>" +
                                    "<p style='line-height: 18px;'>" +
                                        "PhongCachMobile chịu trách nhiệm đối với những sản phẩm chất lượng cao do chúng " +
                                        "tôi phân phối, trường hợp hàng nhái / hàng giả, chúng tôi cam kết hoàn tiền GẤP ĐÔI " +
                                        "cho quý khách.</p>" +
                                    "<br/>" +
                                    "<div class='about-padd'>" +
                                        "<div class='wrapper'>" +
                                            "<div class='about-col-8' style='min-height: 110px; margin-bottom: 15px;'>" +
                                                "<h3>" +
                                                    "Địa điểm bảo hành</h3>" +
                                                "<p>" +
                                                    "Trung tâm Bảo hành PhongCachMobile tại địa chỉ: Tầng 1 - 58 Trần Quang Khải, Quận " +
                                                    "1, Tp.HCM.</p>" +
                                            "</div>" +
                                            "<div class='about-col-9' style='min-height: 110px; margin-bottom: 15px;'>" +
                                                "<h3>" +
                                                    "Thời gian bảo hành</h3>" +
                                                "<ul>" +
                                                    "<li>12 tháng cho Mainboard</li>" +
                                                    "<li>01 tháng cho phụ kiện bao gồm Tai nghe, Cáp, Sạc và Pin.</li>" +
                                                    "<li>01 tuần đối với Thẻ nhớ tặng kèm máy.</li>" +
                                                "</ul>" +
                                            "</div>" +
                                            "<div class='about-col-10' style='min-height: 110px; margin-bottom: 15px;'>" +
                                                "<h3>" +
                                                    "Thời gian nhận máy bảo hành</h3>" +
                                                "<p>" +
                                                    "Từ 9h30 - 18h00 vào tất cả các ngày ngoại trừ ngày Chủ Nhật và ngày Lễ.</p>" +
                                            "</div>" +
                                            "<div class='about-col-1' style='min-height: 210px; background: #f9f9f9; padding-top: 20px;'>" +
                                                "<h3>" +
                                                    "Đặc biệt trong 7 ngày đầu sử dụng</h3>" +
                                                "<p>" +
                                                    "Trong 7 ngày đầu sử dụng, khách hàng sẽ được đổi máy mới trong trường hợp máy đã " +
                                                    "mua bị lỗi mainboard (hay còn gọi là lỗi phần cứng), thời gian đổi máy trong vòng " +
                                                    "48h sau khi xác định chính xác lỗi thuộc về phần cứng. Riêng màn hình, cảm ứng và " +
                                                    "camera nếu trong 7 ngày đầu bị lỗi thì sẽ được thay thế miễn phí chứ không được " +
                                                    "áp dụng chính sách đổi máy.</p>" +
                                            "</div>" +
                                            "<div class='about-col-2' style='background: #f9f9f9; padding-top: 20px;'>" +
                                                "<h3>" +
                                                    "Hạng mục ngoại lệ không được bảo hành</h3>" +
                                                "<p>" +
                                                    "Camera, Dây nguồn và Ổ cứng (bộ nhớ trong của máy) sẽ không được bảo hành miễn phí " +
                                                    "vì nguyên nhân hư hỏng thường bị tác động bởi các yếu tố bên ngoài như nhiệt độ, " +
                                                    "độ ẩm, nguồn điện, virus và cách sử dụng.</p>" +
                                                "<p style='padding-top: 5px;'>" +
                                                    "Đối với các hạng mục không bảo hành miễn phí: Chúng tôi vẫn hỗ trợ 10% phí linh " +
                                                    "kiện thay thế và hoàn toàn không tính tiền công sửa chữa (với điều kiện hư hỏng " +
                                                    "không phải do lỗi sử dụng của khách hàng).</p>" +
                                            "</div>" +
                                            "<div class='about-col-11' style='min-height: 518px;'>" +
                                                "<h3>" +
                                                    "Những trường hợp bị từ chối bảo hành</h3>" +
                                                "<ul>" +
                                                    "<li>Máy không sử dụng đúng cách.</li>" +
                                                    "<li>Máy mất nguồn (không lên nguồn), treo logo.</li>" +
                                                    "<li>Máy có dấu hiệu của sự va chạm như vỏ và thân máy có vết cấn, vết nứt, vỡ, gãy, " +
                                                        "biến dạng.</li>" +
                                                    "<li>Máy có dấu hiệu đã từng đặt trong trong môi trường nhiệt độ cao (ngoài nắng, gần " +
                                                        "lửa, các nguồn nhiệt…).</li>" +
                                                    "<li>Máy có dấu hiệu sử dụng sai điện áp theo quy định của nhà sản xuất.</li>" +
                                                    "<li>Lỗi phát sinh do virus tin học.</li>" +
                                                    "<li>Máy có dấu hiệu bị ướt mưa, rơi vào nước, bị ẩm.</li>" +
                                                    "<li>Khách hàng tự ý can thiệp vào bên trong máy như tự ý cài đặt & nâng cấp ROM, RAM " +
                                                        "và Firmware, tự ý bung tem mở máy hoặc nhờ nơi khác mở máy.</li>" +
                                                    "<li>Máy trong tình trạng Security Code hoặc Sim Card (bị khoá máy).</li>" +
                                                    "<li>Dán keo trên thân máy làm máy không toả nhiệt được.</li>" +
                                                    "<li>Không có phiếu bảo hành, phiếu bảo hành bị phai màu chữ hoặc có dấu hiệu tẩy xóa.</li>" +
                                                    "<li>Máy không có tem của PhongCachMobile.</li>" +
                                                "</ul>" +
                                            "</div>" +
                                            "<div class='about-col-3' style='margin-top: -475px; background: #f9f9f9; padding-top: 20px; " +
                                                "width: 610px;'>" +
                                                "<h3>" +
                                                    "Các lưu ý khác về vấn đề bảo hành</h3>" +
                                                "<ul>" +
                                                    "<li>Để tránh những chuyện ngoài ý muốn, xin quý khách kiểm tra máy & phụ kiện trước " +
                                                        "khi rời khỏi showroom. Sau khi quý khách rời khỏi showroom, chúng tôi hoàn toàn " +
                                                        "không chịu trách nhiệm đối với việc thất lạc hoặc thiếu phụ kiện.</li>" +
                                                    "<li>Vấn đề điểm chết trên màn hình không được xem là lỗi kỹ thuật nếu như số điểm chết " +
                                                        "trên màn hình không vượt quá quy định cho phép do hãng sản xuất đã công bố (ít hơn " +
                                                        "hoặc bằng 3 điểm chết).</li>" +
                                                    "<li>Các lỗi về phần mềm chỉ được hỗ trợ khắc phục, không thuộc phạm vi bảo hành.</li>" +
                                                    "<li>Chỉ có các sản phẩm xách tay được hưởng chính sách bảo hành 1 năm mới được cung " +
                                                        "cấp thẻ bảo hành ngay khi khách hàng mua máy.</li>" +
                                                    "<li>Riêng sản phẩm Máy tính bảng và một vài model ĐTDĐ 'đặc biệt' sẽ được áp dụng chính " +
                                                        "sách bảo hành bị hạn chế: đổi máy mới trong 7 ngày sử dụng đầu tiên nếu có lỗi phần " +
                                                        "cứng và hỗ trợ 30% chi phí sửa chữa trong 1 năm.</li>" +
                                                "</ul>" +
                                                "<h3 style='padding-top: 20px;'>" +
                                                    "Quý khách ở xa</h3>" +
                                                "<ul>" +
                                                    "<li>Cách thức gửi Bảo hành: Trong quá trình sử dụng nếu máy điện thoại bị lỗi, hỏng " +
                                                        "(trong điều khoản Bảo hành ), Quý khách vui lòng giữ nguyên hiện trang gửi về PhongCachMobile, " +
                                                        "ngay sau khi nhận được máy, chúng tôi sẽ cố gắng hết sức khắc phục trong thời gian " +
                                                        "nhanh nhất. Trước khi gửi xin vui lòng gọi điện thoại liên hệ trước với bộ phận " +
                                                        "Kỹ thuật & Bảo hành của PhongCachMobile.</li>" +
                                                    "<li><font style='color: red; font-weight: bold;'>Chi phí vận chuyển</font>: Quý " +
                                                        "khách không cần thanh toán chi phí vận chuyển máy bảo hành.</li>" +
                                                    "<li><font style='color: red; font-weight: bold;'>Hỗ trợ trực tuyến</font>: Trong " +
                                                        "quá trình sử dụng nếu máy có lỗi do cài đặt phần mềm, quý khách chỉ cần gọi điện " +
                                                        "tới bộ phận Kỹ thuật & Bảo hành của PhongCachMobile để được hướng dẫn xử lý trực " +
                                                        "tiếp.</li>" +
                                                "</ul>" +
                                            "</div>" +
                                        "</div>" +
                                    "</div>" +
                                    "<div class='about-padd-2'>" +
                                        "<div class='about-col-7'>" +
                                            "<h4>" +
                                                "Đối với hàng công ty Việt Nam</h4>" +
                                            "<p>" +
                                                "Quý khách vui lòng mang đến các Trung tâm bảo hành chính hãng tại Việt Nam (Thông " +
                                                "tin địa chỉ của các Trung tâm bảo hành được thể hiện trên các sổ bảo hành, sổ hướng " +
                                                "dẫn và trên website của PhongCachMobile trong mục Hỗ Trợ)." +
                                            "</p>" +
                                            "<p>" +
                                                "Đổi máy mới trong 24 giờ sử dụng đầu tiên nếu có lỗi phần cứng (với điều kiện máy " +
                                                "không được tháo warranty và không trầy xước).</p>" +
                                        "</div>" +
                                    "</div>" +
                                    "<div class='about-padd-2'>" +
                                        "<div class='about-col-7'>" +
                                            "<h4>" +
                                                "Dịch vụ bảo hành VIP - Thêm yên tâm, thêm lợi ích</h4>" +
                                            "<p>" +
                                                "Sự khác biệt giữa bảo hành thường và dịch vụ bảo hành VIP</p>" +
                                            "<ul>" +
                                                "<li><font style='color: red; font-weight: bold;'>Bảo hành thường</font>: Máy bị " +
                                                    "hư hỏng trên mainboard và không thể sửa chữa -> <font style='color: red; font-weight: bold;'>" +
                                                        "Thay mainboard miễn phí</font></li>" +
                                                "<li><font style='color: red; font-weight: bold;'>Bảo hành VIP</font>: Máy bị hư " +
                                                    "hỏng trên mainboard và không thể sửa chữa -> <font style='color: red; font-weight: bold;'>" +
                                                        "Đổi máy mới</font></li>" +
                                            "</ul>" +
                                            "<p>" +
                                                "Tại sao nên chọn thêm dịch vụ bảo hành VIP</p>" +
                                            "<ul>" +
                                                "<li>Dịch vụ VIP nhưng phí dịch vụ rất thấp: +5% trị giá máy!</li>" +
                                                "<li>Lợi ích gia tăng: suốt 12 tháng sử dụng, dù máy có bị cũ kỹ theo thời gian nhưng " +
                                                    "vẫn được bảo hành đổi máy mới!</li>" +
                                                "<li>Trường hợp ngay tại thời điểm đổi máy mới, nếu máy mới không còn sản xuất hoặc hết " +
                                                    "hàng thì khách được tùy thích đổi sang máy khác có trị giá <= 80% trị giá máy cũ. " +
                                                    "Nếu đổi máy trị giá cao hơn thì có thể bù thêm tiền (80% trị giá máy cũ + tiền bù " +
                                                    "thêm)</li>" +
                                            "</ul>" +
                                            "<p style='color: red; font-weight: bold;'>" +
                                                "LƯU Ý VỀ DỊCH VỤ</p>" +
                                            "<ul>" +
                                                "<li>Không áp dụng cho hàng công ty.</li>" +
                                                "<li>Không áp dụng cho những model đặc biệt có chính sách bảo hành bị hạn chế.</li>" +
                                                "<li>Sau khi mua máy và được cấp thẻ VIP, xin vui lòng đọc kỹ những quy định của VIP Warranty để việc sử dụng được thuận tiện.</li>" +
                                                "<li>PhongCachMobile sẽ từ chối quyền bảo hành VIP nếu khách hàng vi phạm các điều khoản bảo hành.</li>" +
                                            "</ul>" +
                                        "</div>" +
                                    "</div>" +
                                "</div>");

        }
    }
}