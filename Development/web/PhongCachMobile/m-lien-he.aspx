﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome2.Master" AutoEventWireup="true" CodeBehind="m-lien-he.aspx.cs" Inherits="PhongCachMobile.m_lien_he" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litContact" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content" style="background: white; padding: 25px 30px 16px 30px;">
                                <p style="text-transform: uppercase" class="block-subtitle">Thông tin liên hệ</p>
                                <p style="line-height: 18px;">Head Office: 58 Trần Quang Khải, P.Tân Định, Q.1, Tp.HCM. Điện thoại : 08 35268.254 - 08 35268.255</p>
                                <br />
                                <p style="text-transform: uppercase" class="block-subtitle">Gửi email đến chúng tôi</p>
                                 <div id="frmResponse" runat="server" style="display:none;">
                                <p style="line-height:18px;">
                                    <asp:Label ID="lblFrmResponse_Contact" runat="server"></asp:Label>
                                </p>
                            </div>

                                    <div class="std" id="contact_form" runat="server">
                                    <fieldset>
                                      
                                        <p class="text">
                                            <label for="id_contact">
                                                Tiêu đề</label>
                                            <asp:TextBox ID="txtSubject"  MaxLength="50" runat="server"></asp:TextBox> &nbsp; 
                                               <asp:RequiredFieldValidator ID="reqValSubject" Display="Dynamic" runat="server"
                                        ControlToValidate="txtSubject" ForeColor="Red"></asp:RequiredFieldValidator>
                                        </p>
                                        <p class="text">
                                            <label for="email">
                                                Email</label>
                                                  <asp:TextBox ID="txtEmail"  MaxLength="50" runat="server"></asp:TextBox> &nbsp; 
                                              <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                        Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>

                                        </p>
                                        <p class="textarea">
                                            <label for="message">
                                                Nội dung</label>
                                               <asp:TextBox ID="txtMessage" MaxLength="1000" Width="500" Height="200" TextMode="MultiLine" runat="server"></asp:TextBox> &nbsp; 
                            <asp:RequiredFieldValidator ForeColor="Red" ID="reqValMessage" runat="server"
                                ControlToValidate="txtMessage" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </p>
                                         <p class="text"">
                                            <label for="email">
                                                Mã xác nhận</label>
                                                 <cc1:CaptchaControl ID="ccSecuredCode" runat="server" CaptchaBackgroundNoise="Extreme"
                                CaptchaLength="5" CaptchaHeight="60" CaptchaWidth="210" CaptchaLineNoise="None"
                                CaptchaMinTimeout="5" CaptchaMaxTimeout="240" CssClass="captcha" />
                                </p><p class="text" style="padding-top:0px;">  <label for="email">
                                              &nbsp; </label>
                                                  <asp:TextBox ID="txtCaptcha" runat="server" Style="margin-top: 15px;" Width="100px" MaxLength="5"></asp:TextBox>  &nbsp; 
                            <asp:RequiredFieldValidator ForeColor="Red" style="vertical-align:bottom;" ID="reqValSecuredCode" runat="server"
                                ControlToValidate="txtCaptcha" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:Label ID="lblCaptchaResponse_Contact" style="color:Red;vertical-align:bottom;display:none;" runat="server"></asp:Label>
                                        </p>
                                        <p class="submit">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Button"  class="button_large" 
                                                onclick="btnSubmit_Click" />
                                        </p>
                                    </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
