﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class chat_luong_hang_hoa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            litProductQuality.Text = this.Title = LangController.getLng("litProductQuality.Text", "Chất lượng hàng hóa");
            litProdcutQualityContent.Text = LangMsgController.getLngMsg("litProductQuality.Text", "<div class='block-content' style='background: white; padding: 25px 30px 16px 30px; line-height: 18px; color: #515151;'>" + 
                                    "<p>" + 
                                        "<font style='color: red; font-weight: bold;'>PhongCachMobile</font> không chạy " +
                                        "theo cuộc đua cạnh tranh bán hàng với <font style='color: red'>giá rẻ nhất</font> " + 
                                        "hoặc <font style='color: red'>siêu giảm giá</font> (giảm giá bằng cách sử dụng " + 
                                        "hàng hoá kém chất lượng, hàng tân trang \"re-new\", hàng re-furbish, hàng cũ tồn kho, " + 
                                        "hàng có linh kiện lô, sử dụng linh kiện lô trong quá trình sửa chửa bảo hành...). " + 
                                        "Vì vậy, khách hàng muốn tìm mua những sản phẩm với giá rẻ nhất hoặc siêu giảm giá, " + 
                                        "xin vui lòng tìm đến những nơi khác!" + 
                                    "</p>" + 
                                    "<div class='box-container'>" + 
                                        "<div class='about-page'>" + 
                                            "<i class='icon-thumbs-up'>like</i>" + 
                                            "<div class='extra-wrap'>" + 
                                                "<h3>Đối với hàng điện thoại từ các công ty tại Việt Nam</h3>" + 
                                                "<ul>" + 
                                                    "<li>Luôn luôn là hàng mới nhất được xuất trực tiếp từ nhà phân phối uỷ quyền của chính hãng.</li>" + 
                                                    "<li>Đầy đủ phụ kiện zin kèm máy.</li>" + 
                                                    "<li>Mức giá luôn tốt nhất so với thị trường.</li>" + 
                                                    "<li>Đổi máy mới trong vòng 24h nếu có lỗi phần cứng từ nhà sản xuất.</li>" + 
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                        "<div class='about-page'>" + 
                                            "<i class='icon-ok'>like</i>" + 
                                            "<div class='extra-wrap'>" + 
                                                "<h3>Đối với hàng điện thoại & máy tính bảng xách tay</h3>" + 
                                                "<ul>" + 
                                                    "<li>Nguồn hàng xách tay là hàng chính hãng 100%, được chính thức nhập từ các nhà phân " + 
                                                        "phối sỉ uy tín từ nước ngoài, có sự chọn lọc kỹ càng và nghiêm ngặt trước khi nhập " + 
                                                        "kho. Phần lớn hàng hoá được nhập từ các quốc gia tiên tiến nhất và khắc khe nhất " + 
                                                        "về chỉ tiêu chất lượng hàng viễn thông trên thế giới như Mỹ, Canada, Châu Âu (Anh, " + 
                                                        "Pháp, Đức, Nga), Singapore, Hồng Kông, Ấn Độ...</li> " + 
                                                    "<li>Đa số sản phẩm bán ra có imei máy và imei hộp trùng khớp nhau (95% sản phẩm điện " + 
                                                        "thoại xách tay bán ra tại những nơi khác thường không trùng khớp imei máy và imei " + 
                                                        "hộp hoặc imei hộp bị in lại để trùng khớp với imei máy)</li> " + 
                                                    "<li>Đặc biệt đối với hàng iPhone: Hàng mới 100% và hộp còn nguyên seal ZIN, hàng đúng " + 
                                                        "chuẩn quốc tế với dãy băng tần đủ, có thể sử dụng tại tất cả các quốc gia.<br /> " + 
                                                        "<font style='color: red; font-weight: bold;'>Cảnh báo iPhone 4</font>: hiện " + 
                                                        "nay vì chạy theo lợi nhuận nên thị trường đang bán tràn lan iPhone 4 cũ được tân " + 
                                                        "trang, phụ kiện zin bị thay hàng lô, hộp được dán lại imei và được đóng lại seal " + 
                                                        "hệt như máy mới. Ngoài ra, cũng có rất nhiều trường hợp khách bị mua nhầm hàng nhà " + 
                                                        "mạng được mua Code để trở thành hàng quốc tế (điển hình như mã hàng LL - Mỹ và KH " + 
                                                        "- Hàn Quốc).<br /> " + 
                                                        "Uy tín được khẳng định từ người tiêu dùng, <font style='color: red; font-weight: bold;'>" + 
                                                            "PhongCachMobile</font> hiện được xem là nhà bán lẻ uy tín bậc nhất tại HCM về " + 
                                                        "các sản phẩm iPhone/ iPad.</li> " + 
                                                    "<li>Tất cả hàng hoá bán ra mới 100% (brandnew 100%), đầy đủ phụ kiện zin từ nhà sản " + 
                                                        "xuất (khách hàng muốn mua máy cũ, vui lòng tìm tại mục kho máy cũ).</li> " + 
                                                    "<li>Hàng hoá xách tay được chuyển từ nước ngoài về <font style='color: red; font-weight: bold;'>" + 
                                                        "PhongCachMobile</font> thường xuyên mỗi ngày và được bán theo giá thị trường quốc " + 
                                                        "tế để đảm bảo chất lượng và giá luôn luôn tốt nhất tại thị trường Việt Nam.</li> " + 
                                                    "<li>Bảo hành tận tình, đúng nguyên tắc về chất lượng linh kiện bảo hành và thời gian " + 
                                                        "bảo hành.</li> " + 
                                                    "<li>Đổi máy mới trong 07 ngày nếu có lỗi phần cứng từ nhà sản xuất. </li> " + 
                                                    "<li><font style='color: red; font-weight: bold;'>Miễn phí</font> cài đặt ứng dụng " + 
                                                        "và nâng cấp hệ điều hành cho khách hàng trong suốt thời gian 12 tháng sử dụng (áp " + 
                                                        "dụng cho cả hàng chính hãng và hàng xách tay).</li> " + 
                                                "</ul> " + 
                                            "</div> " + 
                                        "</div> " + 
                                        "<div class='about-page'> " + 
                                            "<i class='icon-cogs'>like</i> " + 
                                            "<div class='extra-wrap'> " + 
                                                "<h3> " + 
                                                    "Đối với linh kiện, phụ kiện</h3> " + 
                                                "<ul> " + 
                                                    "<li><font style='color: red; font-weight: bold;'>PhongCachMobile</font> hiện là " + 
                                                        "đối tác với các nhà phân phối độc quyền sản phẩm linh kiện, phụ kiện chất lượng " + 
                                                        "cao tại Việt Nam. Vì vậy, chất lượng linh kiện - phụ kiện bán ra tại hệ thống showroom " + 
                                                        "đều là những sản phẩm có chất lượng tốt nhất trên thị trường. </li> " + 
                                                "</ul> " + 
                                            "</div> " + 
                                        "</div> " + 
                                    "</div> " +
                                "</div>");
        }
    }
}