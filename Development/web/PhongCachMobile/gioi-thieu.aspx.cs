﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class gioi_thieu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            litIntroduce.Text = this.Title = LangController.getLng("litPCMBIntroduce.Text", "Giới thiệu về Phong Cách Mobile");
            litIntroduceContent.Text = LangMsgController.getLngMsg("litPCMBIntroduce.Text", 
                                "<div class='block-content' style='background: white; padding: 15px 30px 16px 30px;line-height:18px;color:#515151;'>" + 
                                    "<p style='text-transform: uppercase' class='block-subtitle'>" + 
                                        "Chúng tôi đang tạo nên cảm hứng cho một trào lưu mới, trào lưu từ \"PhongcachMobile\"!" + 
                                    "</p>" + 
                                    "<p style='line-height: 18px;'>" + 
                                        "Phong cách - một nét riêng biệt được tạo nên từ sự kết hợp của nhiều yếu tố: cá " + 
                                        "tính, thời trang, gu thẩm mỹ, sự tinh tế… Phong cách không có chuẩn mực, mà mỗi " + 
                                        "người có một tỷ lệ kết hợp khác nhau để tạo nên phong cách riêng cho chính mình. " + 
                                        "Có “phong cách” để khẳng định “cái tôi”, có “phong cách” để khẳng định điều \"khác " + 
                                        "biệt\"! Cuộc sống đương đại, ngoài quần áo, phục trang và xe cộ thì mobile đang trở " + 
                                        "thành vai trò trung tâm trong việc thể hiện 'gu' và 'cá tính' của mỗi chúng ta.</p>" + 
                                    "<br />" + 
                                    "<br />" + 
                                    "<div class='about-padd'>" +
                                        "<div class='wrapper'>" + 
                                            "<div class='about-col-1'>" + 
                                                "<h3>Sản phẩm kinh doanh nhập từ nước ngoài</h3>" + 
                                                "<p>" + 
                                                    "Được xách tay trực tiếp từ các quốc gia khu vực Châu Âu, Mỹ, Singapore, Ấn Độ, Hồng " + 
                                                    "Kông, Úc... được tuyển chọn kỹ càng và chỉ tập trung vào những sản phẩm \"thời trang\" " + 
                                                    "nhất, \"công nghệ\" nhất và \"phong cách\" nhất để đáp ứng được gu phong cách khó tính " + 
                                                    "nhất của giới trung và thượng lưu.</p>" + 
                                            "</div>" + 
                                            "<div class='about-col-2'>" + 
                                                "<h3>Phong cách phục vụ nhiệt tình</h3>" + 
                                                "<p>" + 
                                                    "Tiên phong kinh doanh những model mới nhất của thế giới chưa có mặt tại Việt Nam. " +
                                                    "Các showroom được các Tạp chí Công nghệ Thông tin và Tạp chí Thời trang đánh là " + 
                                                    "showroom điện thoại đẹp và tiện nghi tại Việt Nam. Đồng thời PhongCachMobile mang " + 
                                                    "lại sản phẩm chất lượng và phục vụ khách hàng tốt nhất hiện nay.</p> " + 
                                            "</div>" + 
                                            "<div class='about-col-3'>" + 
                                                "<h3>" + 
                                                    "Lấy khách hàng làm trung tâm</h3>" + 
                                                "<p>" + 
                                                    "Liên tục có các hoạt động khuyến mãi và chính sách bán hàng ưu đãi cho khách hàng. " +
                                                    "Thêm vào đó, chúng tôi được sự tin tưởng của Hệ thống rạp Galaxy, Cinebox, Thương " + 
                                                    "xá Tax, Diamond, các thương hiệu mỹ phẩm, thời trang uy tín trong việc liên kết " + 
                                                    "tổ chức sự kiện, triễn lãm và khuyến mãi nhằm phát triển mạnh thương hiệu <font style='color: red; " + 
                                                        "font-weight: bold;'>PhongCachMobile</font> tại Việt Nam!</p>" + 
                                            "</div>" + 
                                        "</div>" + 
                                    "</div>" + 
                                    "<div class='about-padd-2'>" + 
                                        "<div class='wrapper'>" + 
                                            "<div class='about-col-4'>" + 
                                                "<h4>" + 
                                                   "Cam kết về chất lượng kinh doanh" + 
                                                "</h4>" + 
                                                "<p>Sau nhiều năm kinh doanh trong lĩnh vực điện thoại di động, <font style='color: red; " + 
                                                        "font-weight: bold;'>PhongCachMobile</font> có đuợc mối quan hệ tốt với các đối tác là các nhà phân phối và " + 
                                                "các hãng danh tiếng như Vertu, Apple, Nokia, Samsung, Motorola, Sony Ericsson, Samsung, HTC, LG...  Vì vậy chúng tôi đã và đang hiện thực hóa cam kết:</p>" +
                                                "<ul>" + 
                                                    "<li>Đem đến sự thuận tiện và thái độ phục vụ đẳng cấp \"5 sao\" đến khách hàng.</li>" + 
                                                    "<li>Mang lại môi trường làm việc hiện đại, chuyên nghiệp, và cơ hội thăng tiến cho nhân viên.</li>" + 
                                                    "<li>Đặt sự tin tưởng và tôn trọng với đối tác và các nhà đầu tư lên hàng đầu.</li>" + 
                                                "</ul>" + 
                                            "</div>" + 
                                            "<div class='about-col-5'>" + 
                                                "<h4>" + 
                                                    "Website PhongCachMobile</h4>" + 
                                                "<p>" + 
                                                    "Website chính thức của <font style='color: red;" +
                                                        "font-weight: bold;'>Hệ thống PhongCachMobile</font> là <font style='color: red; " + 
                                                        "font-weight: bold;'>www.phongcachmobile.com.vn</font> được thay đổi phiên bản 2.0 và ra mắt vào 01/11/2013 cũng là dịp sinh nhật lần thứ 4. " + 
                                                        "Hoạt động đến hôm nay, website chúng tôi đã tiên phong trong thiết kế và hiển thị thông tin trực quan nhất đến với khách hàng.</p>" + 
                                                "<ul>" + 
                                                    "<li>Website thương mại điện tử lớn nhất Việt Nam</li>" + 
                                                    "<li>Thiết kế theo phong cách nước ngoài</li>" + 
                                                    "<li>Lượt truy cập hơn <font style='color: red;'>300.000</font> mỗi ngày</li>" + 
                                                "</ul>" + 
                                            "</div>" + 
                                            "<div class='about-col-6'>" + 
                                                "<h4>" + 
                                                    "Chuỗi cửa hàng PhongCachMobile</h4>" + 
                                                "<p>Tính cho đến thời điểm hiện tại, không kế đến những cửa hàng hợp tác kinh doanh, <font style='color: red; " +
                                                        "font-weight: bold;'>PhongCachMobile</font> đã triển khai chi nhánh ở TP.HCM và các tỉnh lân cận để phục vụ khách hàng một cách tốt nhất. " +
                                                         "Hầu như mọi thương hiệu điện thoại di động cao cấp được phân phối chính thức đều có mặt tại các cửa hàng của chúng tôi.</p>" + 
                                                "<ul>" + 
                                                    "<li>58 Trần Quang Khải, P.Tân Định, Q.1, TP.HCM</li>" + 
                                                    "<li>675 Lê Hồng Phong, P.10, Q.10, TP.HCM </li>" + 
                                                    "<li>35A Xô Viết Nghệ Tĩnh, P.An Cư, Q.Ninh Kiều, TP.Cần Thơ</li>" + 
                                                    "<li>136 Yersin, P.Hiệp Thành, TP.Thủ Dầu Một, Bình Dương</li>" + 
                                                "</ul>" + 
                                            "</div>" + 
                                        "</div>" + 
                                        "<div class='about-col-7'>" + 
                                            "<h4>Gửi lời tri ân đến khách hàng</h4>" + 
                                            "<p>Với mong muốn các website mà <font style='color: red;font-weight: bold;'>PhongCachMobile</font> đầu tư sẽ là những website hàng đầu trong lĩnh vực kinh doanh hàng công nghệ di động, " + 
                                            "cũng như mong muốn mang lại sự hài lòng cao nhất cho Quý khách, chúng tôi sẽ cố gắng nỗ lực để cập nhật và phát triền không ngừng.</p> " +
                                            "<p>Chân thành cảm ơn Quý khách hàng đã ủng hộ <font style='color: red;font-weight: bold;'>PhongCachMobile</font> trong thời gian vừa qua và ước mong tiếp tục nhận được sự ủng hộ trong thời gian tới." + 
                                            "</p>" + 
                                        "</div>" + 
                                    "</div>" + 
                                "</div>");
            
        }
    }
}