﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class m_loai_game : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadCategory();
                loadOS();
            }
            loadTitle();
        }

        private void loadOS()
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId("5");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlOS.Items.Clear();
                ddlOS.Items.Add(new ListItem("Tất cả", "-1"));
                foreach (DataRow dr in dt.Rows)
                {
                    ddlOS.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }
                ddlOS.DataBind();
            }
        }

        private void loadTitle()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                litSubtitle.Text = this.Title = "Game " + CategoryController.getCategoryNameById(id);
            }
            else
                litSubtitle.Text = this.Title = "Game";
        }

        private void loadCategory()
        {
            DataTable dt = CategoryController.getCategories("5", "");
            ddlCategory.Items.Clear();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    ddlCategory.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }
            }
            ddlCategory.DataBind();
        }

        private void loadGamesByCategory(int categoryId)
        {
            DataTable dt = GameController.getGamesByCategory(categoryId);
            EnumerableRowCollection<DataRow> temp = null;
            DataTable dtNew = dt;

            //OS filter
            if (!ddlOS.SelectedValue.Equals("-1"))
            {
                temp = dtNew.AsEnumerable().Where(row => row.Field<int>("OSId") == int.Parse(ddlOS.SelectedValue));
                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("Free", typeof(string));
                dtNew.Columns.Add("Token", typeof(string));
                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["Free"] = "<span class='free'>Miễn phí</span>";
                    dr["Token"] = dr["Filename"].ToString();
                    dr["PostedDate"] = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss").ToShortDateString();
                }
            }
            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadGamesByCategory(int.Parse(ddlCategory.SelectedValue));
            dpgLstItemBot.SetPageProperties(0, dpgLstItemBot.PageSize, true);
        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            if (lstItem.DataSource == null)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
                {
                    string sId = Request.QueryString["id"].ToString();
                    string[] ids = sId.Split('-');
                    string id = ids[ids.Length - 1];

                    loadGamesByCategory(int.Parse(id));
                }
                else
                {
                    loadGamesByCategory();
                }

                int currentPage = ((dpgLstItemBot.StartRowIndex) / dpgLstItemBot.MaximumRows); // -1
                DataTable dt = (DataTable)lstItem.DataSource;
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = currentPage * dpgLstItemBot.PageSize; i < (currentPage + 1) * dpgLstItemBot.PageSize && i < dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr != null)
                        {
                            GameController.addViewCount(dr["Id"].ToString());
                            dr["ViewCount"] = int.Parse(dr["ViewCount"].ToString()) + 1;
                        }
                    }
                }

            }
        }


        private void loadGamesByCategory()
        {
            DataTable dt = GameController.getGamesByCategory(-1);
            EnumerableRowCollection<DataRow> temp = null;
            DataTable dtNew = dt;

            //OS filter
            if (!ddlOS.SelectedValue.Equals("-1"))
            {
                temp = dtNew.AsEnumerable().Where(row => row.Field<int>("OSId") == int.Parse(ddlOS.SelectedValue));
                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("Free", typeof(string));
                dtNew.Columns.Add("Token", typeof(string));
                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["Free"] = "<span class='free'>Miễn phí</span>";
                    dr["Token"] = dr["Filename"].ToString();
                    dr["PostedDate"] = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss").ToShortDateString();
                }
            }
            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }


        protected void ddlOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));

            }
            else
            {
                loadGamesByCategory();
            }

            dpgLstItemBot.SetPageProperties(0, dpgLstItemBot.PageSize, true);
        }

        protected void lstItem_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            HyperLink hplDownload = (HyperLink)e.Item.FindControl("hplDownload");
            HiddenField hdfName = (HiddenField)e.Item.FindControl("hdfName");
            HiddenField hdfId = (HiddenField)e.Item.FindControl("hdfId");

            if (SysIniController.getSetting("IsGameAppDownloadable", "true") == "true")
            {
                if (Session["Email"] != null && Session["Email"].ToString() != "")
                {
                    if (hdfName.Value != "#" && hdfName.Value != "")
                        hplDownload.NavigateUrl = "game/" + hdfName.Value;
                    else
                    {
                        hplDownload.NavigateUrl = "#";
                        hplDownload.Text = "Cài đặt tại cửa hàng";
                    }
                }
                else
                {
                    hplDownload.NavigateUrl = "m-dang-nhap";
                }
            }
            else
            {
                hplDownload.Visible = false;
            }
        }


    
    }
}