﻿using PhongCachMobile.controller;
using PhongCachMobile.master;
using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile
{
    public partial class phu_kien_sub : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.Home)this.Master).TemplateType = TemplateType.NewType;

            // Category
            string idStr = Request.QueryString["id"];
            if (!string.IsNullOrWhiteSpace(idStr))
            {
                activeCategoryID = int.Parse(idStr.Split('-').Last());
                activeCategoryName = PhongCachMobile.controller.CategoryController.getCategoryNameById(activeCategoryID.ToString());

                hideBanners = true;
            }

            if (!this.IsPostBack)
            {
                loadBannerItems();
                loadFilterValues();
                loadFilterPhoneList();
            }
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "20"));

        // Hide Banner
        protected bool hideBanners = false;

        // Active Category
        protected int activeCategoryID = -1;
        protected string activeCategoryName = "Tất cả";

        protected List<Article> topBannerLeftItems = new List<Article>();
        protected List<Article> topBannerRightItem1s = new List<Article>();
        protected List<Article> topBannerRightItem2s = new List<Article>();
        private void loadBannerItems()
        {
            // DTDD_Top_Banner_Left_Items // V2-Phone - Sliders Left
            foreach (DataRow dr in SliderController.getSliders("56").Rows)
            {
                topBannerLeftItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // DTDD_Top_Banner_Right_Items1 // V2-Phone - Sliders Right 1
            foreach (DataRow dr in SliderController.getSliders("57").Rows)
            {
                topBannerRightItem1s.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // DTDD_Top_Banner_Right_Items2 // V2-Phone - Sliders Right 2
            foreach (DataRow dr in SliderController.getSliders("58").Rows)
            {
                topBannerRightItem2s.Add(SliderController.rowToSlider(dr).toArticle());
            }

            ucTripleBanner.DataBind();
        }

        protected Dictionary<string, int> prices = new Dictionary<string, int>();
        protected int promotionCount = 0;

        private void loadFilterValues()
        {
            // Prices
            prices = ProductController.PriceList;

            promotionCount = ProductController.getDiscountProductCount(ProductController.ProductGroupPhoneInt);
        }

        protected int remainCount = 0;
        protected List<Product> filterPhoneList = new List<Product>();
        private void loadFilterPhoneList()
        {
            DataTable dt = ProductController.getProductsByGroupId(
                ProductController.ProductGroupAccessoryInt, activeCategoryID, -1, -1, false, true, out remainCount);
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);
                filterPhoneList.Add(product);
            }

            ucFilterPhoneList.DataBind();
        }
    }
}