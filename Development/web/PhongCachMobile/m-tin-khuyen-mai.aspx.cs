﻿using PhongCachMobile.controller;
using PhongCachMobile.model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PhongCachMobile
{
    public partial class m_tin_khuyen_mai : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadNews();
            }
        }

        protected int remainCount = 0;
        public List<Article> newsList = new List<Article>();
        protected void loadNews()
        {
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(out remainCount, 56);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    newsList.Add(ArticleController.rowToArticle(dr));
                }

                dt = CommentController.getNumberOfCommentsByArticleIDs(newsList.Select(a => a.id).ToList());
                foreach (DataRow dr in dt.Rows)
                {
                    newsList.First(a => a.id == (int)dr["articleID"]).CommentCount = (int)dr["CommentCount"];
                }
            }

            ucNewsListBare.DataBind();
        }
    }
}