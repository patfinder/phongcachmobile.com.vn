﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class gui_lai_ma_xac_nhan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            this.Title = "Gửi lại mã xác nhận";
            this.lbtSend.Text = "<span><span>" + LangController.getLng("litSend.Text", "Gửi") + "</span></span>";
            this.reqValEmail.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
            this.rgeValEmail.ErrorMessage = LangController.getLng("litInvalidFormat.Text", "Sai định dạng");
        }

        protected void lbtSend_Click(object sender, EventArgs e)
        {
            if (!SysUserController.checkAvailableUser(txtEmail.Text))
            {
                this.lblEmailResponse.ForeColor = System.Drawing.Color.Red;
                this.lblEmailResponse.Text = LangController.getLng("litNonExistAccount.Text", "Email không tồn tại.");
                this.Title = this.lblEmailResponse.Text;
            }
            else
            {

                string randomString = Common.getRandomString(6);
                ResetActivateResult result = SysUserController.resetActivatedToken(txtEmail.Text, randomString);

                if (result.statusCode == ResetActivateResult.STATUS_VALID_RESET_CODE)
                {
                    resetActivatedTokenResponse.Visible = true;
                    frmInput.Visible = false;
                    bool sendResult = sendResetActivatedTokenEmail(txtEmail.Text, randomString);
                    if (sendResult)
                    {
                        this.litResetActivatedTokenResponse.Text = String.Format(LangMsgController.getLngMsg("litResetActivatedTokenResponse.OK", "Một email chứa thông tin kích hoạt tài khoản mới đã được gửi đến địa chỉ <font style='font-weight:bold;color:red'><u>{0}</u></font>. Hãy kiểm tra email của bạn."), txtEmail.Text);
                        this.Title =  LangController.getLng("litResetActivatedTokenResponse.OK", "Gửi lại mã xác nhận thành công");
                    }
                    else
                    {
                        this.litResetActivatedTokenResponse.Text = LangMsgController.getLngMsg("litResetActivatedTokenResponse.Error", "Đã có lỗi xảy ra trong quá trình gửi lại mã xác nhận.<br/>Bạn vui lòng thử lại sau.");
                        this.Title = LangController.getLng("litResetActivatedTokenResponse.Error", "Lỗi xảy ra trong quá trình gửi lại mã xác nhận.");
                    }
                }
                else if (result.statusCode == ResetActivateResult.STATUS_ALREADY_ACTIVATED_USER_CODE)
                {
                    this.lblEmailResponse.ForeColor = System.Drawing.Color.Red;
                    this.lblEmailResponse.Text = result.statusText + "<br/>";
                    this.Title = result.statusText;
                }
                else
                {
                    resetActivatedTokenResponse.Visible = true;
                    frmInput.Visible = false;
                    this.litResetActivatedTokenResponse.Text = LangMsgController.getLngMsg("litResetActivatedTokenResponse.Error", "Đã có lỗi xảy ra trong quá trình gửi lại mã xác nhận.<br/>Bạn vui lòng thử lại sau.") + "<br/>";
                    this.Title = LangController.getLng("litResetActivatedTokenResponse.Error", "Lỗi xảy ra trong quá trình gửi lại mã xác nhận.") ;
                }
            }
        }

        private bool sendResetActivatedTokenEmail(string email, string randomString)
        {
            try
            {
                string resetPasswordEmailTemplate = TemplateController.getTemplate("EMAIL_RESETACTIVATEDTOKEN_TEMP", "<p><font style='font-weight:bold;color:red'>PhongCachMobile</font> - Thông tin mã kích hoạt tài khoản mới</p>" +
                        "<p><strong>Email:</strong> {0}</p>" +
                        "<p>Để kích hoạt tài khoản của bạn tại <font style='font-weight:bold;color:red'>PhongCachMobile</font>, hãy click vào link kích hoạt tài khoản mới bên dưới:<br/>{1}</p><br/>" +
                        "<p>Nội dung trên được chuyển đến từ website <font style='font-weight:bold;color:red'>PhongCachMobile</font></p>" +
                       "<p><font style='font-weight:bold'>PhongCachMobile Team</font><br/>http://phongcachmobile.com.vn/</p>");

                DataTable dt = SysUserController.getUserByEmail(email);

                string newActivatedUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/')) +
                  "/kich-hoat-" + dt.Rows[0]["Id"].ToString() + "-" + Common.generateHash(email + randomString);
                Common.sendEmail(txtEmail.Text, "", "", "", LangController.getLng("litResetActivatedTokenEmail.Subject", "[PhongCachMobile] Thông tin mã kích hoạt mới"),
                    String.Format(resetPasswordEmailTemplate, email, newActivatedUrl), "");

                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error sendResetActivatedTokenEmail "));
                return false;
            }
        }
    }
}