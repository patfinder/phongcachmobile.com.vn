﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.util;
using System.Data;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class m_portal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //checkMobile();
                loadImage();
            }
        }

        private void loadImage()
        {
            litImage.Text = "<script type='text/javascript'>" + 
                "jQuery(function ($) {" + 
                "$.supersized({" + 
                    "slideshow: 1, " + 
                    "autoplay: 1, " + 
                    "start_slide: 1, " + 
                    "stop_loop: 0, " + 
                    "random: 0, " + 
                    "slide_interval: 3000, " + 
                    "transition: 6, " + 
                    "transition_speed: 1000, " + 
                    "new_window: 1, " + 
                    "pause_hover: 0, " + 
                    "keyboard_nav: 1, " + 
                    "performance: 1, " + 
                    "image_protect: 1, " + 
                    
                    "min_width: 0, " + 
                    "min_height: 0,  " + 
                    "vertical_center: 1, " + 
                    "horizontal_center: 1, 	" + 
                    "fit_always: 0,  " + 
                    "fit_portrait: 1, " + 
                    "fit_landscape: 0, " + 

                    "slide_links: 'blank',  " + 
                    "thumb_links: 1, 	" + 
                    "thumbnail_navigation: 0, 	" + 

                    "slides: [	";

            DataTable dt = SliderController.getSliders("53");
            if (dt != null && dt.Rows.Count >0)
            {
                for(int i=0; i< dt.Rows.Count-1; i++)
                {
                    litImage.Text += "{image: 'images/slide/" + dt.Rows[i]["DisplayImage"].ToString() + "', title: '" + dt.Rows[i]["Title"].ToString() + "<div class=slidedescription>" + dt.Rows[i]["Text"].ToString() + "</div>', thumb: '', url: 'images/slide/" + dt.Rows[i]["DisplayImage"].ToString() + "' },";
                    
                }
                litImage.Text += "{image: 'images/slide/" + dt.Rows[dt.Rows.Count - 1]["DisplayImage"].ToString() + "', title: '" + dt.Rows[dt.Rows.Count - 1]["Title"].ToString() + "<div class=slidedescription>" + dt.Rows[dt.Rows.Count - 1]["Text"].ToString() + "</div>', thumb: '', url: 'images/slide/" + dt.Rows[dt.Rows.Count - 1]["DisplayImage"].ToString() + "' }";
            }
          litImage.Text += "], " + 
                    "progress_bar: 1, " + 
                    "mouse_scrub: 0" + 
                    "});" + 
                    "});" + 
                    "</script>";
        }

        private void checkMobile()
        {
            if (!Common.fBrowserIsMobile())
            {
                Response.Redirect("portal");
            }
        }
    }
}