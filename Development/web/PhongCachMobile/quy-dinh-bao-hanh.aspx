﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="quy-dinh-bao-hanh.aspx.cs" Inherits="PhongCachMobile.quy_dinh_bao_hanh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class='block'>
                                <div class='block-title'>
                                    <strong><span>
                                        <asp:Literal ID="litGuaranteeRule" runat="server"></asp:Literal></span></strong>
                                </div>
                                <asp:Literal ID="litGuaranteeRuleContent" runat="server"></asp:Literal>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
