﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.Web.UI.HtmlControls;
using PhongCachMobile.master;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class m_loai_ud : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.MHome)this.Master).TemplateType = TemplateType.NewType;

            if (!string.IsNullOrWhiteSpace(Request.QueryString["id"]))
            {
                // Game IDs
                List<int> gameIDs = new List<int>();
                foreach (DataRow dr in CategoryController.getCategories(ProductController.ProductGroupGame, "").Rows)
                {
                    gameIDs.Add((int)dr["id"]);
                }

                activeAppTypeID = int.Parse(Request.QueryString["id"].Split('-').Last());
                DataRow dr2 = CategoryController.getCategoryById(activeAppTypeID.ToString()).Rows[0];
                activeAppType = (gameIDs.Contains(activeAppTypeID) ? "[Game] " : "[Ứng dụng] ") + dr2["Name"];

                hideBanners = true;
            }

            if (!this.IsPostBack)
            {
                loadBannerItems();
                loadFilterValues();
                loadFilterPhoneList();
            }
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("Ung_Dung_So_SP_Filter_Page", "8"));

        // Hide Banner
        protected bool hideBanners = false;

        protected string activeAppType = "Tất cả";
        protected int activeAppTypeID = -1;

        protected List<Article> appTypes = new List<Article>();
        protected int appleCount = 0;
        protected int samsungCount = 0;
        protected int windowsPhoneCount = 0;

        protected List<Article> topBannerLeftItems = new List<Article>();
        protected List<Article> topBannerRightItem1s = new List<Article>();
        protected List<Article> topBannerRightItem2s = new List<Article>();
        private void loadBannerItems()
        {
            // Ung_Dung_Top_Banner_Left_Items // V2-App - Sliders Left
            foreach (DataRow dr in SliderController.getSliders("66").Rows)
            {
                topBannerLeftItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // Ung_Dung_Top_Banner_Right_Items1 // V2-App - Sliders Right 1
            foreach (DataRow dr in SliderController.getSliders("67").Rows)
            {
                topBannerRightItem1s.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // Ung_Dung_Top_Banner_Right_Items2 // V2-App - Sliders Right 2
            foreach (DataRow dr in SliderController.getSliders("68").Rows)
            {
                topBannerRightItem2s.Add(SliderController.rowToSlider(dr).toArticle());
            }

            ucTripleBanner.DataBind();
        }

        private void loadFilterValues()
        {
            // App/Game types
            appTypes.Add(new Article
            {
                id = -1,
                title = "Tất cả",
                shortDesc = "-1",
                link = "game---1"
            });

            // Game categories
            DataTable dtSub = CategoryController.getCategories(ProductController.ProductGroupGame, "");
            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                appTypes.Add(new Article
                {
                    id = (int)dtSub.Rows[i]["Id"],
                    title = "[Game] " + (string)dtSub.Rows[i]["Name"],
                    shortDesc = dtSub.Rows[i]["Id"].ToString(),
                    link = "game-" + ((string)dtSub.Rows[i]["Name"]).Replace(" ", "-") + "-" + dtSub.Rows[i]["Id"]
                });
            }

            // Application categories
            dtSub = CategoryController.getCategories(ProductController.ProductGroupApplication, "");
            for (int i = 0; i < dtSub.Rows.Count; i++)
            {
                appTypes.Add(new Article
                {
                    id = (int)dtSub.Rows[i]["Id"],
                    title = "[Ứng dụng] " + (string)dtSub.Rows[i]["Name"],
                    shortDesc = dtSub.Rows[i]["Id"].ToString(),
                    link = "ud-" + ((string)dtSub.Rows[i]["Name"]).Replace(" ", "-") + "-" + dtSub.Rows[i]["Id"]
                });
            }

            appleCount = ApplicationController.getAppAndGameCount(OSType.iOS);
            samsungCount = ApplicationController.getAppAndGameCount(OSType.Android);
            windowsPhoneCount = ApplicationController.getAppAndGameCount(OSType.WindowsPhone);
        }

        protected int remainCount = 0;
        protected List<Application> applications = new List<Application>();
        private void loadFilterPhoneList()
        {
            DataTable dt = ApplicationController.getAppWithFilter(ApplicationType.Unknown, activeAppTypeID, OSType.Unknown, false, out remainCount, 0, 0);

            foreach (DataRow row in dt.Rows)
            {
                applications.Add(ApplicationController.rowToApplication(row));
            }

            ucApplicationListBare.DataBind();
        }
    }
}