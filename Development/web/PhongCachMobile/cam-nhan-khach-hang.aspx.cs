﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class cam_nhan_khach_hang : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadTestimonial();
            }
        }

        private void loadTestimonial()
        {
            DataTable dt = TestimonialController.getTestimonial();
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlName", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlName"] = "khach-hang-" + HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString();
                    string text = HTMLRemoval.StripTagsRegex(dr["Text"].ToString());
                    dr["Text"] = (text.Length > 200 ? text.Substring(0, 200) + "..." : text);
                }
                lstTestimonial.DataSource = dt;
                lstTestimonial.DataBind();
            }
        }

        private void loadLang()
        {
            litTestimonial.Text = this.Title = LangController.getLng("litTestimonial.Text", "Cảm nhận từ khách khi mua hàng tại PhongCachMobile");
            litTestimonialContent.Text = LangMsgController.getLngMsg("litTestimonialContent.Text", "Với mong muốn trở thành chuỗi cửa hàng cung cấp sản phẩm điện thoại di động, máy tính bảng, phụ kiện và dịch vụ sửa chửa, nâng cấp tốt nhất cho khách hàng, PhongCachMobile là nơi không chỉ tập trung sản phẩm cao cấp với giá tốt nhất mà còn là nơi quy tụ đội ngũ nhân viên nhiệt tình, giàu kinh nghiệm với môi trường làm việc hàng đầu tại Việt Nam.");

        }
    }
}