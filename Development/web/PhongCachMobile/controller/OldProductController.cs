﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using System.Data;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class OldProductController
    {
        internal static DataTable getOldProducts()
        {
            //get
            try
            {
                return DB.getData("select * from OLDPRODUCT  order by currentPrice desc");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getOldProducts"));
                return null;
            }
        }

        public static DataTable getRandomOldProducts(int number)
        {
            //get
            try
            {
                return DB.getData("select top " + number + " * from OLDPRODUCT order by NEWID();");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getRandomSpecial"));
                return null;
            }
        }
        public static DataTable getOldProducts(List<int> categoryIDs, int price, bool bSortPrice, out int remainCount, int pageIndex, int pageSize)
        {
            remainCount = 0;

            //get
            try
            {
                // Where expression
                string wherePrice = price <= 0 ? "" : 
                    (categoryIDs.Count() <= 0 ? " WHERE " : " AND ") + (price <= 10000000 ? " currentPrice <= " + price : " currentPrice > 10000000");

                // Order Expression
                string orderExp = "ORDER BY currentPrice " + (bSortPrice ? "DESC" : "ASC");
                string query = "* FROM OLDPRODUCT " +
                    // Category
                    (categoryIDs.Count() <= 0 ? " " : " WHERE categoryID IN (" + string.Join(",", categoryIDs) + ")") +
                    // Price
                    wherePrice;

                // Get count
                remainCount = int.Parse(DB.getValue("SELECT COUNT(ID) " + query.Substring(2))) - (pageIndex * pageSize + pageSize);
                if (remainCount < 0)
                    remainCount = 0;

                object[,] paramss;
                query = DB.wrapInPager(query, orderExp, pageIndex, pageSize, out paramss);
                return DB.getData(query, paramss);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getRandomSpecial"));
                return null;
            }
        }

        internal static bool updateOldProduct(OldProduct oldProduct)
        {
            try
            {
                object[,] parameters =  {   { "@name", oldProduct.name },
                                            { "@id", oldProduct.id},
                                            { "@description", oldProduct.description},
                                            { "@currentPrice", oldProduct.currentPrice},
                                            { "@originalPrice", oldProduct.originalPrice}
                                         };

                return DB.exec("update OLDPRODUCT set name = @name, [description] = @description, currentPrice = @currentPrice, originalPrice = @originalPrice where Id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateOldProduct"));
                return false;
            }
        }

        internal static bool createOldProduct(OldProduct oldProduct)
        {
            try
            {
                object[,] parameters =  {   { "@name", oldProduct.name },
                                            { "@description", oldProduct.description},
                                            { "@currentPrice", oldProduct.currentPrice},
                                            { "@originalPrice", oldProduct.originalPrice}
                                         };

                return DB.exec("insert into OLDPRODUCT (name, [description], currentPrice, originalPrice) " +
                    "values (@name, @description, @currentPrice, @originalPrice) ;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createOldProduct"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('OLDPRODUCT')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentOldProductId"));
                return "";
            }
        }

        internal static bool deleteOldProduct(string gvIDs)
        {
            try
            {
                return DB.exec("delete from OLDPRODUCT where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteOldProduct"));
                return false;
            }
        }
    }
}