﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class PollDetailController
    {
        public static DataTable getPollDetailByPollId(string pollId)
        {
            //get
            try
            {
                object [,] parameter = {{"@pollId", pollId}};
                return DB.getData("select * from PollDetail where pollId = @pollId;", parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getPollDetailByPollId"));
                return null;
            }
        }

        public static bool increaseById(string id)
        {
            //get
            try
            {
                object[,] parameter = { { "@id", id } };
                return DB.exec("update PollDetail set Value = value+1 where id = @id;", parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error increaseById"));
                return false;
            }
        }

        internal static bool deletePollDetail(string gvIDs)
        {
            try
            {
                return DB.exec("delete from POLLDETAIL where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deletePollDetail"));
                return false;
            }
        }

        internal static bool updatePollDetail(PollDetail pollDetail)
        {
            try
            {
                object[,] parameters =  {   { "@name", pollDetail.name },
                                        { "@id", pollDetail.id }
                                    };
                return DB.exec("update PollDetail set name = @name where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updatePollDetail"));
                return false;
            }
        }

        internal static bool createPollDetail(PollDetail pollDetail)
        {
            try
            {
                object[,] parameters =  {{ "@name", pollDetail.name },
                                         {"@pollId", pollDetail.pollId }
                                    };
                return DB.exec("insert into PollDetail (name, pollId, value) " +
                    "values (@name, @pollId, 0);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createPollDetail"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('POLLDETAIL')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentPollDetailId"));
                return "";
            }
        }
    }
}