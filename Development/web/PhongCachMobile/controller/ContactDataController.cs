﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile.controller
{
    public class ContactDataController
    {
        // Get ContactData by Id
        public static DataTable getContactDataById(int id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                DataTable dt = new DataTable();
                dt = DB.getData("select * from CONTACTDATA where id = @id;", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getContactDataById"));
                return null;
            }
        }

        // Get ContactData by SenderEmail
        public static DataTable getContactDataBySenderEmail(string senderName)
        {
            //get
            try
            {
                object[,] parameters = { { "@senderName", senderName } };
                DataTable dt = new DataTable();
                dt = DB.getData("select * from CONTACTDATA where senderName = @senderName;", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getContactDataBySenderEmail"));
                return null;
            }
        }

        // Create ContactData 
        public static bool createContactData(ContactData contactData)
        {
            //create
            try
            {
                object[,] parameters = { 
                                        {"@senderName", contactData.senderName},  
                                        {"@senderEmail", contactData.senderEmail}, 
                                        {"@description", contactData.description}, 
                                        {"@subject", contactData.subject}, 
                                        {"@postedDate", contactData.postedDate}, 
                                       };
                bool result = DB.exec("insert into CONTACTDATA(senderName, senderEmail, description, subject, postedDate)" +
                                        " values(@senderName ,@senderEmail ,@description ,@subject ,@postedDate);", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createContactData"));
                return false;
            }
        }

        // Update ContactData 
        public static bool updateContactData(ContactData contactData)
        {
            //get
            try
            {
                object[,] parameters = {{"@id", contactData.id},  
                                        {"@senderName", contactData.senderName},  
                                        {"@senderEmail", contactData.senderEmail}, 
                                        {"@description", contactData.description}, 
                                        {"@subject", contactData.subject}, 
                                        {"@postedDate", contactData.postedDate}, 
                                       };
                bool result = DB.exec("update CONTACTDATA " +
                                        "set senderName = @senderName, senderEmail = @senderEmail, description = @description, subject = @subject, postedDate = @postedDate " +
                                        "where id = @id;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateContactData"));
                return false;
            }
        }
    }
}