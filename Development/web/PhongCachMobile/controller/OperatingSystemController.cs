﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class OperatingSystemController
    {
        public static DataTable getOperatingSystemsByGroupId(string groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from OPERATINGSYSTEM where groupId = @groupId;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getOperatingSystems"));
                return null;
            }
        }

        public static string getOSNameById(string id)
        {
            //get
            try
            {
                object[,] parameters =  {{ "@id", id }
                                        };
                return DB.getValue("select Name from OPERATINGSYSTEM where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getOSNameById"));
                return "";
            }
        }

        internal static bool deleteOS(string gvIDs)
        {
            try
            {
                return DB.exec("delete from OperatingSystem where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteOS"));
                return false;
            }
        }

        internal static bool updateOS(model.OperatingSystem os)
        {
            try
            {
                object[,] parameters = {   {"@Id", os.id},
                                           {"@Name", os.name}, 
                                           {"@GroupId", os.groupId}
                                    };
                return DB.exec("update OperatingSystem set name = @name, groupId = @groupId where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateOS"));
                return false;
            }
        }

        internal static bool createOS(model.OperatingSystem os)
        {
            try
            {
                object[,] parameters = {   {"@Name", os.name}, 
                                           {"@GroupId", os.groupId}
                                    };
                return DB.exec("insert into OperatingSystem (Name, GroupId) values (@Name, @GroupId);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createSlider"));
                return false;
            }
        }
    }
}