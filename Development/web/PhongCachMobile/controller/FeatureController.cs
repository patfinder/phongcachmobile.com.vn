﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class FeatureController
    {
        public static DataTable getFeaturesByGroupId(string groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from FEATURE where groupId = @groupId;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFeatures"));
                return null;
            }
        }

        public static DataTable getProductByFeature(int groupID, int categoryID, int featureId, int pageIndex = 0, int pageSize = 0)
        {
            if (pageIndex < 0)
                pageIndex = 0;
            if (pageSize <= 0)
                pageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "16"));

            //get
            try
            {
                // Build Where expression
                string whereExp = " WHERE Value = 'True' AND featureId = " + featureId + (groupID >= 0 ? " AND groupID = " + groupID : "")
                    + (categoryID >= 0 ? " AND categoryID = " + categoryID : "");
                // Order Expression
                string orderExp = "ORDER BY isComingSoon DESC, isOutOfStock ASC, CurrentPrice ASC";
                string query = "* FROM (SELECT DISTINCT P.* FROM PRODUCT P INNER JOIN PRODUCTFEATURE PF ON P.Id = PF.ProductId " + whereExp + ") AS Table1";

                object[,] paramss;
                query = DB.wrapInPager(query, orderExp, pageIndex, pageSize, out paramss);
                return DB.getData(query, paramss);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByFeatureId"));
                return null;
            }
        }

        public static string getFeatureNameById(string id)
        {
            //get
            try
            {
                object[,] parameters =  {{ "@id", id }
                                        };
                return DB.getValue("select Name from FEATURE where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFeatureNameById"));
                return "";
            }
        }

         public static DataTable getFeatures(string productId, string groupId)
        {
            //get
            try
            {
                object[,] parameters =  {{ "@productId", productId },
                                         { "@groupId", groupId}};
                return DB.getData("select Feature.Name, PRODUCTFEATURE.Value " + 
                            "from PRODUCT join PRODUCTFEATURE on PRODUCT.Id = PRODUCTFEATURE.ProductId join FEATURE on FEATURE.Id = FeatureId " +
                            "where ProductId = @productId and Feature.GroupId = @groupId;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFeatures"));
                return null;
            }
        }
         public static DataTable getFeatures(int productID, int[] featureIDs)
         {
             //get
             try
             {
                 string ids =  string.Join(",", featureIDs);
                 return DB.getData("select Feature.Name, PRODUCTFEATURE.Value " +
                             "from PRODUCT join PRODUCTFEATURE on PRODUCT.Id = PRODUCTFEATURE.ProductId join FEATURE on FEATURE.Id = FeatureId AND FeatureId IN(" + ids + ") " +
                             "where ProductId = @productId;", new object[,] { { "@productId", productID } });
             }
             catch (Exception ex)
             {
                 // log
                 LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFeatures"));
                 return null;
             }
         }
    }
}