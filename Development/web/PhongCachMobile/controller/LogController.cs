﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using Newtonsoft.Json;

namespace PhongCachMobile.controller
{
    public class LogController
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(LogController).FullName);

        public static bool createLog(Log log)
        {
            try
            {
                // object[,] parameters = {{"@time", log.time },
                //                         {"@description", log.description },
                //                         {"@status", log.status },
                //                         {"@text", log.text }};
                // DB.exec("insert into LOG(time, description, status, text) values(@time, @description, @status, @text);", parameters, false);
                // return true;

                logger.Error(log.text + ": " + log.description);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}