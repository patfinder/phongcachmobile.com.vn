﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class PointController
    {

        internal static DataTable getPointsByCardId(string cardId)
        {
            //get
            try
            {
                object[,] parameters = { { "@cardId", cardId } };
                return DB.getData("select * from POINT where cardId = @cardId;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getPointsByCardId"));
                return null;
            }
        }

        internal static bool updatePoint(Point point)
        {
            try
            {
                object[,] parameters =  {{ "@Code", point.code },
                                         { "@Id", point.id },
                                        };
                return DB.exec("update POINT set Code = @Code where Id = @Id", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updatePoint"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('POINT')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentPointId"));
                return "";
            }
        }

        internal static bool createPoint(Point point)
        {

            try
            {
                object[,] parameters =  {   
                                    { "@code", point.code },
                                    { "@userId", point.userId },
                                    { "@posteddate", point.postedDate },
                                    { "@cardId", point.cardId }, 
                                    { "@IsApproved", point.isApproved}
                                };
                return DB.exec("insert into POINT (code, userId, posteddate, cardId, IsApproved) values " +
                    "(@code, @userId, @posteddate, @cardId, @IsApproved);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createPoint"));
                return false;
            }
        }

        internal static string getTotalPointByCardId(string cardId)
        {
            try
            {
                object[,] parameter = { { "@cardId", cardId } };
                return DB.getValue("select count(*) from POINT where cardId = @cardId and IsApproved = 'true';",parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTotalPointByCardId"));
                return "";
            }
        }

        internal static bool deletePoint(string gvIDs)
        {
            try
            {
                return DB.exec("delete from POINT where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deletePoint"));
                return false;
            }
        }

        internal static bool getUnapprovedPoint(string gvIDs)
        {
            try
            {
                return DB.exec("select COUNT(*) from POINT  where IsApproved = 'false' and cardid in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getUnapprovedPoint"));
                return false;
            }
        }

        internal static DataTable getUnApprovedPoint(string timeFrom, string timeTo)
        {
            //get
            try
            {
                object[,] parameters = { { "@StartTime", timeFrom }, 
                                       {"@EndTime", timeTo}};
                DataTable dt = DB.getData("select * from POINT where PostedDate > @StartTime and PostedDate < @EndTime and IsApproved = 'false';", parameters);
                return dt;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getUnApprovedPoint"));
                return null;
            }
        }

        internal static bool approvePoint(string gvIDs)
        {
            try
            {
                return DB.exec("update POINT set IsApproved = 'true' where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error approvePoint"));
                return false;
            }
        }

        internal static string getPointsByCardCode(string code)
        {
            try
            {
                object[,] parameter = { { "@code", code } };
                return DB.getValue("select COUNT(*) from POINT where IsApproved != 'false' and cardid = (select ID from CARD where Code = @code);", parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTotalPointByCardId"));
                return "";
            }
        }
    }
}