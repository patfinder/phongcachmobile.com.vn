﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;
using System.Globalization;

namespace PhongCachMobile.controller
{
    public class ArticleController
    {
        //----------
        public static string getNumberOfArticlesInCategory(string categoryId)
        {
            //get
            try
            {
                object[,] parameters = { { "@categoryId", categoryId } };
                return DB.getValue("select COUNT(*) from ARTICLE where categoryId=@categoryId;", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfArticlesInCategory"));
                return null;
            }
        }
        //----------
        public static string getTotalReadCountInArticle(string categoryId)
        {
            //get
            try
            {
                object[,] parameters = { { "@categoryId", categoryId } };
                string rs = DB.getValue("select SUM(ViewCount) from ARTICLE where categoryId=@categoryId;", parameters);
                if (rs == "")
                    return "0";
                return rs;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTotalReadCountInArticle"));
                return "0";
            }
        }

        //---------


        public static DataTable getArticlesOrderByPostedDate(int number, string groupId, string categoryId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId }};
                    dt = DB.getData("select TOP " + number + " *  from ARTICLE WHERE CategoryId = @categoryId and GroupId = @groupId order by PostedDate desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId }};
                        dt = DB.getData("select *  from ARTICLE WHERE CategoryId = @categoryId and GroupId = @groupId order by PostedDate desc;", parameters);
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOrderByPostedDate"));
                return null;
            }
        }
        public static DataTable getArticlesOrderByPostedDate(out int remainCount, int categoryId = -1, int pageIndex = 0, int pageSize = 0)
        {
            remainCount = 0;

            if (pageIndex <= 0)
                pageIndex = 0;
            if (pageSize <= 0)
                pageSize = int.Parse(SysIniController.getSetting("Tin_Khuyen_Mai_So_Tin", "10"));

            //get
            try
            {
                string orderExp = "ORDER BY PostedDate DESC";
                string query = "* from ARTICLE WHERE GroupId = 22 " +
                    (categoryId > 0 ? "AND CategoryId = " + categoryId : "");

                // Get count
                remainCount = int.Parse(DB.getValue("SELECT COUNT(ID) " + query.Substring(2))) - (pageIndex * pageSize + pageSize);
                if (remainCount < 0)
                    remainCount = 0;

                object[,] paramss;
                query = DB.wrapInPager(query, orderExp, pageIndex, pageSize, out paramss);
                return DB.getData(query, paramss);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOrderByPostedDate"));
                return null;
            }
        }

        public static DataTable getArticlesOrderByPostedDate(int number, string groupId, string categoryId, string productId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId },
                                            {"@productId", productId}};
                    dt = DB.getData("select TOP " + number + " *  from ARTICLE WHERE CategoryId = @categoryId and GroupId = @groupId and ProductId = @productId order by PostedDate desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = {{"@groupId", groupId },
                                                {"@categoryId", categoryId },
                                                {"@productId", productId}};
                        dt = DB.getData("select *  from ARTICLE WHERE CategoryId = @categoryId and GroupId = @groupId and ProductId = @productId order by PostedDate desc;", parameters);
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOrderByPostedDate"));
                return null;
            }
        }

        public static DataTable getArticlesOrderByPostedDate(int number, string groupId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = {{"@groupId", groupId }};
                    dt = DB.getData("select TOP " + number + " *  from ARTICLE WHERE GroupId = @groupId order by PostedDate desc;", parameters);
                    return dt;
                }
                else if (number <= 0)
                {
                    object[,] parameters = {{"@groupId", groupId }};
                    dt = DB.getData("select *  from ARTICLE WHERE GroupId = @groupId order by PostedDate desc;", parameters);
                    return dt;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOrderByPostedDate"));
                return null;
            }
        }

        public static DataTable getArticlesOnWeekOrderByViewCount(int number, string groupId, string categoryId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId }};
                    dt = DB.getData("SELECT top " + number + " * from ARTICLE " +
                        "where DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) < 1 " +
                        "and DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) >=0 " +
                        "and CategoryId = @categoryId and GroupId = @groupId order by ViewCount desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId }};
                        dt = DB.getData("SELECT * from ARTICLE " +
                         "where DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) < 1 " +
                         "and DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) >=0 " +
                         "and CategoryId = @categoryId and GroupId = @groupId order by ViewCount desc;", parameters);
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOnWeekOrderByViewCount"));
                return null;
            }
        }


        public static DataTable getArticlesOnMonthOrderByViewCount(int number, string groupId, string categoryId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId }};
                    dt = DB.getData("SELECT top " + number + " * from ARTICLE " +
                        "where DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) < 4 " +
                        "and DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) >=0 " +
                        "and CategoryId = @categoryId and GroupId = @groupId order by ViewCount desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId }};
                        dt = DB.getData("SELECT * from ARTICLE " +
                         "where DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) < 4 " +
                         "and DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) >=0 " +
                         "and CategoryId = @categoryId and GroupId = @groupId order by ViewCount desc;", parameters);
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOnMonthOrderByViewCount"));
                return null;
            }
        }

        public static string getNumberOfArticles()
        {
            //get
            try
            {
                string result = DB.getValue("select COUNT(*) from ARTICLE");
                if (result == "")
                    return "0";
                else
                    return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfArticles"));
                return null;
            }
        }


        public static DataTable getArticleById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                DataTable dt = DB.getData("select * from ARTICLE where id = @id", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticleById"));
                return null;
            }
        }

        public static DataTable getArticlesOnMonthOrderByViewCount(int number, string exceptId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = { { "@exceptId", exceptId } };
                    dt = DB.getData("SELECT top " + number + " * from ARTICLE " +
                        "where DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) < 4 " +
                        "and DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) >=0 " +
                        "and id != @exceptId order by ViewCount desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = { { "@exceptId", exceptId } };
                        dt = DB.getData("SELECT * from ARTICLE " +
                         "where DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) < 4 " +
                         "and DateDiff(wk,CONVERT(datetime,LEFT(PostedDate,4)+SUBSTRING(PostedDate,5,2)+SUBSTRING(PostedDate,7,2)),getdate()) >=0 " +
                         "and id != @exceptId order by ViewCount desc;");
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOnMonthOrderByViewCount"));
                return null;
            }
        }


        public static DataTable getArticlesOrderByPostedDateExcept(int number, string groupId, string categoryId, string exceptId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId },
                                            {"@exceptId", exceptId}};
                    dt = DB.getData("select TOP " + number + " *  from ARTICLE WHERE id != @exceptId and  CategoryId = @categoryId and GroupId = @groupId order by PostedDate desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = {{"@groupId", groupId },
                                            {"@categoryId", categoryId },
                                            {"@exceptId", exceptId}};
                        dt = DB.getData("select *  from ARTICLE WHERE id != @exceptId and  CategoryId = @categoryId and GroupId = @groupId order by PostedDate desc;", parameters);
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOrderByPostedDateExcept"));
                return null;
            }
        }

        public static bool deleteArticle(string gvIDs)
        {
            try
            {
                return DB.exec("delete from ARTICLE where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteArticle"));
                return false;
            }
        }

        public static bool updateArticle(Article article)
        {
            try
            {
                object[,] parameters =  {   { "@Title", article.title },
                                        { "@ShortDesc", article.shortDesc},
                                        { "@LongDesc", article.longDesc},
                                        { "@ViewCount", article.viewCount},
                                        { "@Flag", article.flag},
                                        { "@GroupId", article.groupId},
                                        { "@CategoryId", article.categoryId},
                                        { "@Id", article.id },
                                        { "@ProductId", article.productId }
                                    };
                return DB.exec("update ARTICLE set Title = @Title, ShortDesc = @ShortDesc, LongDesc = @LongDesc, ViewCount = @ViewCount, " +
                    "Flag = @Flag, GroupId = @GroupId, CategoryId = @CategoryId where Id = @Id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateArticle"));
                return false;
            }
        }

        public static bool createArticle(Article article)
        {
            try
            {
                object[,] parameters =  {   { "@Title", article.title },
                                        { "@ShortDesc", article.shortDesc},
                                        { "@LongDesc", article.longDesc},
                                        { "@ViewCount", article.viewCount},
                                        { "@PostedDate", article.postedDate},
                                        { "@Flag", article.flag},
                                        { "@GroupId", article.groupId},
                                        { "@CategoryId", article.categoryId},
                                        { "@ProductId", article.productId }
                                    };
                if (article.productId == -1)
                    return DB.exec("insert into ARTICLE (Title, ShortDesc, LongDesc, ViewCount, PostedDate, Flag, GroupId, CategoryId) " +
                    "values (@Title, @ShortDesc, @LongDesc, @ViewCount, @PostedDate, @Flag, @GroupId, @CategoryId);", parameters);
                else
                    return DB.exec("insert into ARTICLE (Title, ShortDesc, LongDesc, ViewCount, PostedDate, Flag, GroupId, CategoryId, ProductId) " +
                    "values (@Title, @ShortDesc, @LongDesc, @ViewCount, @PostedDate, @Flag, @GroupId, @CategoryId, @ProductId);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createArticle"));
                return false;
            }
        }

        public static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('ARTICLE')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentArticleId"));
                return "";
            }
        }

        public static bool addViewCount(string id)
        {
            try
            {
                object[,] parameters = {   {"@Id", id}
                                    };
                return DB.exec("update Article set ViewCount = ViewCount + 1 where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addArticleViewCount"));
                return false;
            }
        }

        internal static DataTable getArticlesOnMonthOrderByCommentCount(int number, string exceptId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = { { "@exceptId", exceptId } };
                    dt = DB.getData("SELECT top " + number + "ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," + 
                        "Article.PostedDate, COUNT(*) as CommentCount from ARTICLE left join COMMENT on ARTICLE.Id = COMMENT.ArticleId "+
                        "where DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) < 4 " + 
                        "and DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) >=0 " +
                        "and ARTICLE.Id != @exceptId group by ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," + 
                        "Article.PostedDate " + 
                        "order by CommentCount desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = { { "@exceptId", exceptId } };
                        dt = DB.getData("SELECT top ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," + 
                        "Article.PostedDate, COUNT(*) as CommentCount from ARTICLE left join COMMENT on ARTICLE.Id = COMMENT.ArticleId "+
                        "where DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) < 4 " + 
                        "and DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) >=0 " + 
                        "and ARTICLE.Id != -1 group by ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," + 
                        "Article.PostedDate" + 
                        "order by CommentCount desc;");
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOnMonthOrderByCommentCount"));
                return null;
            }
        }

        internal static DataTable getArticlesOnMonthOrderByCommentCount(int number, string exceptId, string groupId, string categoryId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = { { "@exceptId", exceptId },
                                             { "@groupId", groupId },
                                             { "@categoryId", categoryId },};
                    dt = DB.getData("SELECT top " + number + "ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," +
                        "Article.PostedDate, COUNT(*) as CommentCount from ARTICLE left join COMMENT on ARTICLE.Id = COMMENT.ArticleId " +
                        "where DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) < 4 " +
                        "and DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) >=0 " +
                        "and ARTICLE.Id != @exceptId and groupId = @groupId and categoryId = @categoryId group by ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," +
                        "Article.PostedDate " +
                        "order by CommentCount desc;", parameters);
                    return dt;
                }
                else
                    if (number <= 0)
                    {
                        object[,] parameters = { { "@exceptId", exceptId } };
                        dt = DB.getData("SELECT top ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," +
                        "Article.PostedDate, COUNT(*) as CommentCount from ARTICLE left join COMMENT on ARTICLE.Id = COMMENT.ArticleId " +
                        "where DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) < 4 " +
                        "and DateDiff(wk,CONVERT(datetime,LEFT(ARTICLE.PostedDate,4)+SUBSTRING(ARTICLE.PostedDate,5,2)+SUBSTRING(ARTICLE.PostedDate,7,2)),getdate()) >=0 " +
                        "and ARTICLE.Id != -1 and groupId = @groupId and categoryId = @categoryId group by ARTICLE.Id, ARTICLE.Title, ARTICLE.ShortDesc, ARTICLE.LongDesc, Article.DisplayImage, Article.ViewCount," +
                        "Article.PostedDate" +
                        "order by CommentCount desc;");
                        return dt;
                    }
                    else
                        return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticlesOnMonthOrderByCommentCount"));
                return null;
            }
        }

        internal static DataTable getArticleByProductId(string groupId, string productId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId }, { "@productId", productId } };
                return DB.getData("select *  from ARTICLE WHERE GroupId = @groupId and ProductId = @productId order by PostedDate desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getArticleByProductId"));
                return null;
            }
        }

        public static string getArticleLink(Article article)
        {
            return Common.m() + "tt-" + HTMLRemoval.StripTagsRegex(article.title)
                .NormalizeDiacriticalCharacters().ReplaceNonAlphaNumSpace().ToLower().Replace(' ', '-') + "-" + article.id;
        }
        //public static string getArticleLink(string title, int id)
        //{
        //    return Common.m() + "tt-" + HTMLRemoval.StripTagsRegex(title).ToLower().Replace(' ', '-') + id;
        //}

        public static Article rowToSimpleArticle(DataRow row)
        {
            Article article = new Article
            {
                id = (int)row["Id"],
                title = row["Title"] as string,
                shortDesc = row["shortDesc"] as string,
            };

            article.link = getArticleLink(article);

            return article;
        }
        public static Article rowToArticle(DataRow row)
        {
            Article article = new Article
            {
                id = (int)row["Id"],
                title = row["Title"] as string,
                displayImg = row["displayImage"] as string,
                postedDate = DateTime.ParseExact(row["postedDate"] as string, "yyyyMMddHHmmss", CultureInfo.InvariantCulture).ToString("dd/MM/yyyy"),
                shortDesc = row["shortDesc"] as string,
                longDesc = row["longDesc"] as string,
                viewCount = (int)row["viewCount"],
                groupId = (int)row["groupId"],
                categoryId = (int)row["categoryId"],
            };

            article.link = getArticleLink(article);

            return article;
        }
    }
}