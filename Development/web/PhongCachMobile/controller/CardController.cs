﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class CardController
    {

        internal static DataTable getCards(int status)
        {
            //get
            try
            {
                if (status == -1)
                    return DB.getData("select * from CARD;");
                else if (status == 0)
                    return DB.getData("select * from CARD where Status = 0;");
                else if (status == 1)
                    return DB.getData("select * from CARD where Status = 1;");
                else // if (status == 3)
                    return DB.getData("select * from CARD where Status = 2;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCards"));
                return null;
            }
        }

        internal static bool updateCard(Card card)
        {
            try
            {
                object[,] parameters =  {{ "@Code", card.code },
                                         { "@BrandNumber", card.brandNumber},
                                         { "@Id", card.id }
                                        };
                return DB.exec("update CARD set Code = @Code, BrandNumber = @BrandNumber where Id = @Id", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateCard"));
                return false;
            }
        }

        internal static bool createCard(Card card)
        {

            try
            {
                object[,] parameters =  {   
                                    { "@code", card.code },
                                    { "@brandNumber", card.brandNumber },
                                    { "@status", card.status },
                                    { "@postedDate", card.postedDate }
                                };
                return DB.exec("insert into CARD (code, brandNumber, status, postedDate) values " +
                    "(@code, @brandNumber, @status, @postedDate);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createCard"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('CARD')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentCardId"));
                return "";
            }
        }

        internal static DataTable getCardByCode(string code)
        {
            //get
            try
            {
                object[,] parameter = { { "@code", code } };
                return DB.getData("select * from CARD where code = @code;", parameter);
               
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCardByCode"));
                return null;
            }
        }

        internal static bool deleteCard(string gvIDs)
        {
            try
            {
                return DB.exec("delete from CARD where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteCard"));
                return false;
            }
        }

        internal static bool approveCard(string gvIDs)
        {
            try
            {
                return DB.exec("update CARD set Status = 2 where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error approveCard"));
                return false;
            }
        }

        internal static bool requestToApproveCard(string gvIDs)
        {
            try
            {
                return DB.exec("update CARD set Status = 1 where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error requestToApproveCard"));
                return false;
            }
        }
    }
}