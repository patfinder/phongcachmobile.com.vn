﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.controller
{
    public class LangController
    {
        // Get single Lang
        public static string getLng(string name, string defaultValue)
        {
            string result = DB.getValue("select value from LANG where name = '" + name + "';");
            if (result == "")
            {
                try
                {
                    //create
                    object[,] parameters = { {"@name", name },
                                       {"@value", defaultValue }};
                    DB.exec("insert into LANG(name, value) values(@name, @value);", parameters);
                    return defaultValue;
                }
                catch (Exception ex)
                {
                    // log
                    LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getLng", 1, ex.Message));
                    return result;
                }
            }
            else
            {
                return result;
            }
        }

        // Get all Langs
        public static DataTable getLngs()
        {
            //get
            try
            {
                DataTable dt = DB.getData("select * from LANG;");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getLngs", 1, ex.Message));
                return null;
            }

        }


        //Update single Language
        public static bool updateLng(string name, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@name", name },
                                       {"@value", value }};
                bool result = DB.exec("update LANG set value = @value where name = @name;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateLng", 1, ex.Message));
                return false;
            }
        }

        //Update single Language by id
        public static bool updateLngById(string id, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@id", id },
                                       {"@value", value }};
                bool result = DB.exec("update LANG set value = @value where id = @id;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateLngById", 1, ex.Message));
                return false;
            }
        }


        public static bool deleteLang(string gvIDs)
        {
            try
            {
                return DB.exec("delete from Lang where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteLang"));
                return false;
            }
        }
    }
}