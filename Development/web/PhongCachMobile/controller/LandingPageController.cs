﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.controller
{
    public class LandingPageController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProductController).FullName);

        internal static DataTable GetPage(int page)
        {
            //get
            try
            {
                object[,] parameters = { { "@Page", page } };

                return DB.getData("SELECT * FROM LANDINGPAGEDATA WHERE Page = @Page;", parameters);
            }
            catch (Exception ex)
            {
                // log
                log.Error("getPage", ex);
                return null;
            }
        }

        internal static bool CreatePage(int page, Dictionary<string, string> pageValues)
        {
            try
            {
                var result = true;
                foreach (KeyValuePair<string, string> kv in pageValues)
                {
                    var valueList = new Dictionary<string, object>() { 
                        {"@Page", 1},
                        {"@Name", kv.Key},
                        {"@Value", kv.Value},
                    };

                    var sql = "INSERT INTO LANDINGPAGEDATA(Page, Name, [Value]) VALUES(@Page, @Name, @Value)";

                    result = result && DB.exec(sql, valueList);
                }

                return result;
            }
            catch (Exception ex)
            {
                log.Error("CreatePage", ex);
                return false;
            }
        }

        internal static bool UpdatePage(int page, Dictionary<string, string> pageValues)
        {
            try
            {
                var result = true;
                foreach (KeyValuePair<string, string> kv in pageValues)
                {
                    var paramss = new object[,]
                    {
                        {"@Page", page},
                        {"@Name", kv.Key},
                    };

                    // Check if not exist, create new
                    if (int.Parse(DB.getValue("SELECT COUNT(ID) FROM LANDINGPAGEDATA WHERE Page = @Page AND Name = @Name", paramss)) <= 0)
                    {
                        result = result & CreatePage(page, new Dictionary<string, string> { { kv.Key, kv.Value } });
                        continue;
                    }

                    var valueList = new Dictionary<string, object>() { 
                        {"@Page", 1},
                        {"@Name", kv.Key},
                        {"@Value", kv.Value},
                    };

                    var sql = "UPDATE LANDINGPAGEDATA SET [Value] = @Value WHERE Page = @Page AND Name = @Name";

                    result = result && DB.exec(sql, valueList);
                }

                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateProduct"));
                return false;
            }
        }
    }
}