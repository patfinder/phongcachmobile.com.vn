﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile.controller
{
    public class TemplateController
    {
        // Get single Template
        public static string getTemplate(string name, string defaultValue)
        {
            //get
            string result = DB.getValue("select value from TEMPLATE where name = '" + name + "';");
            if (result == "")
            {
                try
                {
                    //create
                    object[,] parameters = { {"@name", name },
                                       {"@value", defaultValue }};
                    DB.exec("insert into TEMPLATE(name, value) values(@name, @value);", parameters);
                    return defaultValue;
                }
                catch (Exception ex)
                {
                    // log
                    LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTemplate"));
                    return result;
                }
            }
            else
            {
                return result;
            }
        }

        // Get all Langs
        public static DataTable getTemplates()
        {
            //get
            try
            {
                DataTable dt = DB.getData("select * from TEMPLATE;");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTemplates"));
                return null;
            }

        }


        //Update single Template
        public static bool updateTemplate(string name, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@name", name },
                                       {"@value", value }};
                bool result = DB.exec("update TEMPLATE set value = @value where name = @name;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateTemplate"));
                return false;
            }
        }

        public static bool updateTemplateById(string id, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@id", id },
                                       {"@value", value }};
                bool result = DB.exec("update TEMPLATE set value = @value where id = @id;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateTemplateById", 1, ex.Message));
                return false;
            }
        }
    }
}