﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;
using PhongCachMobile.admin;

namespace PhongCachMobile.controller
{
    public class AccessoryGroupController
    {
        public static List<SectionInfo> getGroups()
        {
            //get
            var dt = DB.getData("SELECT * FROM AccessorySection ORDER BY ID ASC");
            if (dt == null)
                return null;

            List<SectionInfo> groups = new List<SectionInfo>();
            foreach (DataRow dr in dt.Rows)
            {
                List<string> categories = (dr["Categories"] as string ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                groups.Add(new SectionInfo
                {
                    ID = (int)dr["ID"],
                    Title = (string)dr["Name"],
                    Image = (string)dr["Image"],
                    Link = dr["Link"] as string,
                    ShowItemCount = (int)dr["ShowItemCount"],
                    Categories = categories.Select(g => new CategoryInfo { ID = int.Parse(g) }).ToList()
                });
            }

            return groups;
        }

        public static SectionInfo getGroup(int sectionID)
        {
            //get
            var dt = DB.getData("SELECT * FROM AccessorySection WHERE ID = " + sectionID);
            if (dt == null)
                return null;

            var dr = dt.Rows[0];
            List<string> categories = (dr["Categories"] as string ?? "").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            SectionInfo group = new SectionInfo
            {
                ID = (int)dr["ID"],
                Title = (string)dr["Name"],
                Image = (string)dr["Image"],
                Link = dr["Link"] as string,
                ShowItemCount = (int)dr["ShowItemCount"],
                Categories = categories.Select(g => new CategoryInfo { ID = int.Parse(g) }).ToList()
            };

            return group;
        }

        public static bool updateGroup(SectionInfo section)
        {
            if (section.ID <= 0)
            {
                // Create
                object[,] parameters = {
                                   {"@Name", section.Title },
                                   {"@Image", section.Image },
                                   {"@Link", section.Link },
                                   {"@ShowItemCount", section.ShowItemCount },
                                   {"@Categories", string.Join(",", section.Categories.Select(c => c.ID)) },
                                       };

                return DB.exec("INSERT INTO AccessorySection (Name, Categories, Image, Link, ShowItemCount) VALUES(@Name, @Categories, @Image, @Link, @ShowItemCount)", parameters);
            }
            else
            {
                // Update
                object[,] parameters = {
                                   {"@ID", section.ID },
                                   {"@Name", section.Title },
                                   {"@Image", section.Image },
                                   {"@Link", section.Link },
                                   {"@ShowItemCount", section.ShowItemCount },
                                   {"@Categories", string.Join(",", section.Categories.Select(c => c.ID)) },
                                       };

                return DB.exec("UPDATE AccessorySection SET Name = @Name, Categories = @Categories, " +
                    "Image = @Image, Link = @Link, ShowItemCount = @ShowItemCount WHERE ID = @ID", parameters);
            }
        }

        public static bool deleteGroup(int sectionID)
        {
            object[,] parameters = {
                                   {"@ID", sectionID },
                                       };

            return DB.exec("DELETE FROM AccessorySection WHERE ID = @ID", parameters);
        }

    }
}