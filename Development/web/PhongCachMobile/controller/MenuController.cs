﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile.controller
{
    public class MenuController
    {
        public static DataTable getMenus(string groupId, string parentId)
        {
            //get
            try
            {

                DataTable dt = new DataTable();
                if (parentId == null || parentId == "")
                {
                    object[,] parameters =  {  { "@groupId", groupId }
                                        };
                    dt = DB.getData("select Id, CAST(ParentId AS varchar(10)) as ParentId, GroupId, Text, ActionUrl, Flag, InOrder from MENU where ParentId is null and GroupId = @groupId order by InOrder;", parameters);
                }
                else
                {
                    object[,] parameters =  {{ "@parentId", parentId },
                                         { "@groupId", groupId },
                                        };
                    dt = DB.getData("select Id, CAST(ParentId AS varchar(10)) as ParentId, GroupId, Text, ActionUrl, Flag, InOrder from MENU where ParentId = @parentId and GroupId = @groupId order by InOrder;", parameters);
                }
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getMenus"));
                return null;
            }
        }

        public static bool updateMenu(Menu menu)
        {
            try
            {
                if (menu.inOrder <= -1)
                {
                    if (menu.parentId == -1)
                    {
                        object[,] parameters =  {   { "@ParentId", DBNull.Value },
                                            { "@Text", menu.text },
                                            { "@ActionUrl", menu.actionUrl },
                                            { "@Flag", menu.flag },
                                            { "@Id", menu.id }
                                        };
                        return DB.exec("update MENU set ParentId = @ParentId, Text = @Text, ActionUrl = @ActionUrl, Flag = @Flag where Id = @Id", parameters);
                    }
                    else
                    {
                        object[,] parameters =  {   { "@ParentId", menu.parentId },
                                            { "@Text", menu.text },
                                            { "@ActionUrl", menu.actionUrl },
                                            { "@Flag", menu.flag },
                                            { "@Id", menu.id }
                                        };
                        return DB.exec("update MENU set ParentId = @ParentId, Text = @Text, ActionUrl = @ActionUrl, Flag = @Flag where Id = @Id", parameters);
                    }
                }
                else
                {
                    object[,] parameters =  {  { "@InOrder", menu.inOrder },
                                            { "@Id", menu.id }
                                        };
                    return DB.exec("update MENU set InOrder = @InOrder where Id = @Id", parameters);
                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateMenu"));
                return false;
            }
        }

        public static bool createMenu(Menu menu)
        {
            try
            {
                if (menu.parentId == -1)
                {
                    object[,] parameters =  {   { "@ParentId",DBNull.Value },
                                        { "@Text", menu.text },
                                        { "@GroupId", menu.groupId },
                                        { "@InOrder", menu.inOrder },
                                        { "@ActionUrl", menu.actionUrl },
                                        { "@Flag", menu.flag }
                                    };
                    return DB.exec("insert into MENU (ParentId, GroupId, Text, ActionUrl, Flag, InOrder) values (@ParentId, @GroupId, @Text, @ActionUrl, @Flag, @InOrder);", parameters);
                }
                else
                {
                    object[,] parameters =  {   { "@ParentId",menu.parentId },
                                        { "@Text", menu.text },
                                        { "@GroupId", menu.groupId },
                                        { "@InOrder", menu.inOrder },
                                        { "@ActionUrl", menu.actionUrl },
                                        { "@Flag", menu.flag }
                                    };
                    return DB.exec("insert into MENU (ParentId, GroupId, Text, ActionUrl, Flag, InOrder) values (@ParentId, @GroupId, @Text, @ActionUrl, @Flag, @InOrder);", parameters);
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createMenu"));
                return false;
            }
        }

        public static bool deleteMenu(string lstId)
        {
            try
            {
                return DB.exec("delete from Menu where id in " + lstId + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteMenu"));
                return false;
            }
        }

        public static bool updateOrderMenu(string menuId, int inOrder)
        {
            try
            {
                object[,] parameters =  {{ "@InOrder", inOrder },
                                         { "@Id", menuId }
                                        };
                return DB.exec("update MENU set InOrder = @InOrder where Id = @Id", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateOrderMenu"));
                return false;
            }
        }



        public static bool authorize(string groupId, string actionUrl)
        {
            try
            {
                DataTable dt = null;
                object[,] parameters =  {{ "@GroupId", groupId },
                                         { "@ActionUrl", actionUrl }
                                        };
                dt = DB.getData("select * from MENU where ActionUrl = @ActionUrl and GroupId = @GroupId;", parameters);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error authorize"));
                return false;
            }
        }
    }
}