﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.model;
using PhongCachMobile.util;

namespace PhongCachMobile.controller
{
    public class SysUserController
    {
        public static string getNumberOfUsers()
        {
            //get
            try
            {
                string result = DB.getValue("select COUNT(*) from SYSUSER");
                if (result == "")
                    return "0";
                else
                    return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfUsers"));
                return null;
            }
        }

        public static string getMostRecentUser()
        {
            //get
            try
            {
                return DB.getValue("select top 1 Email from SysUser where ActivatedDate is not null order by id desc;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getMostRecentUser"));
                return null;
            }
        }

        public static bool checkAvailableUser(string email)
        {
            //get
            try
            {
                object[,] parameters = { { "@email", email } };
                string result = DB.getValue("select email from SysUser where email = @email;", parameters);
                if (result == "")
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error checkAvailableUser"));
                return false;
            }
        }

        public static string createUserForActivating(string email, string password, string stateId)
        {
            //get
            try
            {
                string hashPassword = Common.generateHash(password);
                string activatedToken = Common.generateHash(email);
                object[,] parameters = {{"@email", email }, 
                                        {"@password", hashPassword },
                                        {"@registeredDate", DateTime.Now.ToString("yyyyMMddHHmmss")},
                                        {"@stateId", stateId},
                                        {"@activatedToken", activatedToken}};
                bool result = DB.exec("insert into SysUser(email, password, registeredDate, activatedToken, stateId) " +
                    "values (@email, @password, @registeredDate, @activatedToken, @stateId);", parameters);

                if (result)
                    return DB.getValue("select IDENT_CURRENT('SYSUSER');");
                else
                    return "";
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createUserForActivating"));
                return "";
            }
        }

        public static DataTable getUserById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                return DB.getData("select * from SYSUSER where id=@id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getUserById"));
                return null;
            }
        }

        public static DataTable getUserByEmail(string email)
        {
            //get
            try
            {
                object[,] parameters = { { "@email", email } };
                return DB.getData("select * from SYSUSER where email=@email", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getUserByEmail"));
                return null;
            }
        }

        public static ActivateUserResult activateUser(string id, string token)
        {
            try
            {
                DataTable dt = getUserById(id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DateTime datetimeNow = DateTime.Now;
                    DateTime datetimeRegister = Common.strToDateTime(dt.Rows[0]["RegisteredDate"].ToString());
                    if ((datetimeNow - datetimeRegister).TotalHours > int.Parse(SysIniController.getSetting("User_ActivateTimeSpan_InHours", "24")))
                        return new ActivateUserResult(ActivateUserResult.STATUS_EXPIRED_TIME_CODE, ActivateUserResult.STATUS_EXPIRED_TIME_TEXT);

                    if (token != dt.Rows[0]["ActivatedToken"].ToString())
                        return new ActivateUserResult(ActivateUserResult.STATUS_INVALID_TOKEN_CODE, ActivateUserResult.STATUS_INVALID_TOKEN_TEXT);

                    if (dt.Rows[0]["ActivatedDate"] != null && dt.Rows[0]["ActivatedDate"].ToString() != "")
                        return new ActivateUserResult(ActivateUserResult.STATUS_ALREADY_ACTIVATED_CODE, ActivateUserResult.STATUS_ALREADY_ACTIVATED_TEXT);

                    string avatar = ((int.Parse(id) % 11) + 1) + ".png";
                    object[,] parameters = { { "@Id", id },
                                             { "@ActivatedDate", datetimeNow.ToString("yyyyMMddHHmmss") },
                                             { "@GroupId", "6"},
                                             { "@Avatar", avatar}};
                    bool result = DB.exec("update SysUser set ActivatedDate = @ActivatedDate, GroupId = @GroupId, avatar = @avatar where Id = @Id;", parameters);
                    if (result)
                    {
                        return new ActivateUserResult(ActivateUserResult.STATUS_VALID_TOKEN_CODE, ActivateUserResult.STATUS_VALID_TOKEN_TEXT);
                    }
                    else
                        return new ActivateUserResult(ActivateUserResult.STATUS_GENERAL_ERROR_CODE, ActivateUserResult.STATUS_GENERAL_ERROR_TEXT);
                }
                else
                    return new ActivateUserResult(ActivateUserResult.STATUS_GENERAL_ERROR_CODE, ActivateUserResult.STATUS_GENERAL_ERROR_TEXT);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error activateUser"));
                return new ActivateUserResult(ActivateUserResult.STATUS_GENERAL_ERROR_CODE, ActivateUserResult.STATUS_GENERAL_ERROR_TEXT);
            }
        }

        public static bool resetPassword(string email, string randomPassword)
        {
            try
            {
                DataTable dt = getUserByEmail(email);
                if (dt != null && dt.Rows.Count > 0)
                {
                    object[,] parameters = { { "@Id", dt.Rows[0]["Id"].ToString() },
                                             { "@Password", Common.generateHash(randomPassword)}};
                    return DB.exec("update SysUser set Password = @Password where Id = @Id;", parameters);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error resetPassword"));
                return false;
            }
        }

        public static LogInUserResult logIn(string email, string password)
        {
            try
            {
                DataTable dt = getUserByEmail(email);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (email != dt.Rows[0]["Email"].ToString() || Common.generateHash(password) != dt.Rows[0]["Password"].ToString())
                        return new LogInUserResult(LogInUserResult.STATUS_INVALID_CREDENTIAL_CODE, LogInUserResult.STATUS_INVALID_CREDENTIAL_TEXT);
                    if (dt.Rows[0]["ActivatedDate"] == null || dt.Rows[0]["ActivatedDate"].ToString() == "")
                        return new LogInUserResult(LogInUserResult.STATUS_INACTIVATED_USER_CODE, LogInUserResult.STATUS_INACTIVATED_USER_TEXT);
                    if (dt.Rows[0]["Flag"].ToString() == "False")
                        return new LogInUserResult(LogInUserResult.STATUS_DISABLE_USER_ERROR_CODE, LogInUserResult.STATUS_DISABLE_USER_ERROR_TEXT);
                    return new LogInUserResult(LogInUserResult.STATUS_VALID_CREDENTIAL_CODE, LogInUserResult.STATUS_VALID_CREDENTIAL_TEXT);
                }
                else
                {
                    return new LogInUserResult(LogInUserResult.STATUS_GENERAL_ERROR_CODE, LogInUserResult.STATUS_GENERAL_ERROR_TEXT);
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error logIn"));
                return new LogInUserResult(LogInUserResult.STATUS_GENERAL_ERROR_CODE, LogInUserResult.STATUS_GENERAL_ERROR_TEXT);
            }
        }

        public static ResetActivateResult resetActivatedToken(string email, string randomString)
        {
            //get
            try
            {
                DataTable dt = getUserByEmail(email);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["ActivatedDate"] != null && dt.Rows[0]["ActivatedDate"].ToString() != "")
                        return new ResetActivateResult(ResetActivateResult.STATUS_ALREADY_ACTIVATED_USER_CODE,
                            ResetActivateResult.STATUS_ALREADY_ACTIVATED_USER_TEXT);

                    string resetActivatedToken = Common.generateHash(email + randomString);
                    object[,] parameters = {{"@email", email }, 
                                        {"@registeredDate", DateTime.Now.ToString("yyyyMMddHHmmss")},
                                        {"@activatedToken", resetActivatedToken}};
                    DB.exec("update SysUser set registeredDate = @registeredDate, activatedToken = @activatedToken " +
                        "where email = @email;", parameters);

                    return new ResetActivateResult(ResetActivateResult.STATUS_VALID_RESET_CODE, ResetActivateResult.STATUS_VALID_RESET_TEXT);
                }
                else
                {
                    return new ResetActivateResult(ResetActivateResult.STATUS_GENERAL_ERROR_CODE, ResetActivateResult.STATUS_GENERAL_ERROR_TEXT);
                }


            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error resetActivatedToken"));
                return new ResetActivateResult(ResetActivateResult.STATUS_GENERAL_ERROR_CODE, ResetActivateResult.STATUS_GENERAL_ERROR_TEXT);
            }
        }

        public static bool updateUser(SysUser user)
        {
            //get
            try
            {
                if (user.groupId == -1)
                {
                    object[,] parameters = {   {"@alias", user.alias }, 
                                           {"@firstname", user.firstName }, 
                                           {"@lastname", user.lastName }, 
                                           {"@dob", user.DOB }, 
                                           {"@gender", user.gender }, 
                                           {"@address", user.address }, 
                                           {"@mobile", user.mobile }, 
                                           {"@tel", user.tel }, 
                                           {"@stateId", user.stateId },
                                           {"@flag", user.flag },
                                           {"@id", user.id }};
                    DB.exec("update SysUser set alias = @alias, firstname = @firstname, lastname = @lastname, dob = @dob, " +
                        "gender = @gender, address = @address, mobile = @mobile, tel = @tel, stateId = @stateId, flag = @flag " +
                        "where id = @id;", parameters);
                }
                else
                {
                    object[,] parameters = {   {"@alias", user.alias }, 
                                           {"@firstname", user.firstName }, 
                                           {"@lastname", user.lastName }, 
                                           {"@dob", user.DOB }, 
                                           {"@gender", user.gender }, 
                                           {"@address", user.address }, 
                                           {"@mobile", user.mobile }, 
                                           {"@tel", user.tel }, 
                                           {"@stateId", user.stateId },
                                           {"@flag", user.flag },
                                           {"@groupId", user.groupId },
                                           {"@id", user.id }};
                    DB.exec("update SysUser set alias = @alias, firstname = @firstname, lastname = @lastname, dob = @dob, " +
                        "gender = @gender, address = @address, mobile = @mobile, tel = @tel, stateId = @stateId, flag = @flag, " +
                        "groupId = @groupId " +
                        "where id = @id;", parameters);
                }
                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateUser"));
                return false;
            }
        }


        public static bool updatePasswordById(string id, string newHashedPassword)
        {
            //get
            try
            {
                object[,] parameters = {{"@Password", newHashedPassword }, 
                                        {"@id", id }};
                DB.exec("update SysUser set Password = @Password " +
                    "where id = @id;", parameters);
                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updatePasswordById"));
                return false;
            }
        }


        internal static DataTable getTopPointCollector(int number)
        {
            //get
            try
            {
                return DB.getData("select top " + number + " alias, email, point from SYSUSER order by point desc");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTopPointCollector"));
                return null;
            }
        }

        public static bool updateBudgetPointByUserId(string id, int budget, int point)
        {
            //get
            try
            {
                object[,] parameters = {{"@budget", budget }, 
                                        {"@point", point }, 
                                        {"@id", id }};
                DB.exec("update SysUser set budget = @budget, point = @point " +
                    "where id = @id;", parameters);
                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateBudgetByUserId"));
                return false;
            }
        }

        public static bool addBudget(string id, int amount)
        {
            try
            {
                object[,] parameters = { { "@Id", id },
                                             { "@Amount", amount}};
                return DB.exec("update SysUser set Budget = Budget + @Amount where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addBudget"));
                return false;
            }
        }

        public static bool disableUser(string gvIDs)
        {
            try
            {
                return DB.exec("update SysUser set Flag = 'false' where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error disableUser"));
                return false;
            }
        }

        public static DataTable getUserByAlias(string alias)
        {
            //get
            try
            {
                object[,] parameters = { { "@alias", alias } };
                return DB.getData("select * from SYSUSER where alias=@alias", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getUserByAlias"));
                return null;
            }
        }

        public static DataTable getUserByPrefixEmail(string prefixEmail)
        {
            //get
            try
            {
                object[,] parameters = { { "@email", prefixEmail + "%" } };
                return DB.getData("select * from SYSUSER where email like @email", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getUserByPrefixEmail"));
                return null;
            }
        }

        public static string getFullnameById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                return DB.getValue("select firstname + ' ' + lastname as Fullname from SYSUSER where id = @id; ", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFullnameById"));
                return null;
            }
        }

        internal static DataTable getActivatedUser()
        {
            //get
            try
            {
                return DB.getData("select email from SYSUSER where ActivatedDate is not null");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getActivatedUser"));
                return null;
            }
        }

        internal static string getEmailById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                return DB.getValue("select email from SYSUSER where id=@id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getEmailById"));
                return null;
            }
        }

        internal static bool updateLastRead(string id)
        {
            try
            {
                object[,] parameters = { { "@id", id },
                                        {"@lastread", DateTime.Now.ToString("yyyyMMddHHmmss")}};
                return DB.exec("update sysuser set lastread = @lastread where id = @id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateLastRead"));
                return false;
            }
        }
    }
}