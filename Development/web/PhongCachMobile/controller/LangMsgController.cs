﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile.controller
{
    public class LangMsgController
    {
        // Get single LangMsg
        public static string getLngMsg(string name, string defaultValue)
        {
            //get
            string result = DB.getValue("select value from LANGMSG where name = '" + name + "';");
            if (result == "")
            {
                try
                {
                    //create
                    object[,] parameters = { {"@name", name },
                                       {"@value", defaultValue }};
                    DB.exec("insert into LANGMSG(name, value) values(@name, @value);", parameters);
                    return defaultValue;
                }
                catch (Exception ex)
                {
                    // log
                    LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getLngMsg", 1, ex.Message));
                    return result;
                }
            }
            else
            {
                return result;
            }
        }

        // Get all LangMsgs
        public static DataTable getLngMsgs()
        {
            //get
            try
            {
                DataTable dt = DB.getData("select * from LANGMSG;");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getLngMsgs", 1, ex.Message));
                return null;
            }

        }


        //Update single LangMsg
        public static bool updateLngMsg(string name, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@name", name },
                                       {"@value", value }};
                bool result = DB.exec("update LANGMSG set value = @value where name = @name;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateLngMsg", 1, ex.Message));
                return false;
            }
        }

        public static bool deleteLangMsg(string gvIDs)
        {
            try
            {
                return DB.exec("delete from LangMsg where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteLangMsg"));
                return false;
            }
        }

        public static bool updateLngMsgById(string id, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@id", id },
                                       {"@value", value }};
                bool result = DB.exec("update LANGMSG set value = @value where id = @id;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateLngMsgById", 1, ex.Message));
                return false;
            }
        }
    }
}