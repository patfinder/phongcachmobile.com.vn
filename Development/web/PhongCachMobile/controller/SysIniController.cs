﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.controller
{
    public class SysIniController
    {
        // Get single Setting
        public static string getSetting(string name, string defaultValue, string description)
        {
            //get
            string result = DB.getValue("select value from SYSINI where name = '" + name + "';");
            if (result == "")
            {
                try
                {
                    //create
                    object[,] parameters = {{"@name", name },
                                            {"@value", defaultValue },
                                            {"@description", description }};
                    DB.exec("insert into SYSINI(name, value, description) values(@name, @value, @description);", parameters);
                    return defaultValue;
                }
                catch (Exception ex)
                {
                    // log
                    LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getSetting", 1, ex.Message));
                    return result;
                }
            }
            else
            {
                return result;
            }
        }

        // Overload get single Setting
        public static string getSetting(string name, string defaultValue)
        {
            //get
            string result = "";
            result = DB.getValue("select value from SYSINI where name = '" + name + "';");
            if (result == "")
            {
                try
                {
                    //create
                    object[,] parameters = {{"@name", name },
                                            {"@value", defaultValue }};
                    // DB.exec("insert into SYSINI(name, value) values(@name, @value);", parameters);
                    return defaultValue;
                }
                catch (Exception ex)
                {
                    // log
                    LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getSetting", 1, ex.Message));
                    return result;
                }
            }
            else
            {
                return result;
            }
        }

        public static string getSettingWithPrefix(string prefix)
        {
            return DB.getValue("SELECT value FROM SYSINI WHERE name LIKE '" + prefix + "%';");
        }

        // Get all Settings
        public static DataTable getSettings()
        {
            //get
            try
            {
                DataTable dt = DB.getData("select * from SYSINI;");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error getSettings", 1, ex.Message));
                return null;
            }

        }


        //Update single Setting
        public static bool updateSetting(string name, string value, string description)
        {
            try
            {
                //update
                object[,] parameters = { {"@name", name },
                                       {"@value", value },
                                       {"@description", description }};
                bool result = DB.exec("update SYSINI set value = @value, description = @description where name = @name;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateSetting", 1, ex.Message));
                return false;
            }
        }

        //Overload update single Setting
        public static bool updateSetting(string name, string value)
        {
            try
            {
                //update
                object[,] parameters = { {"@name", name },
                                       {"@value", value }};
                bool result = DB.exec("update SYSINI set value = @value where name = @name;", parameters);
                return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), "Error updateSetting", 1, ex.Message));
                return false;
            }
        }
    }
}