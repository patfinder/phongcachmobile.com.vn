﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class TestimonialController
    {
        internal static DataTable getTestimonial()
        {
            try
            {
                return DB.getData("select * from Testimonial order by postedDate;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTestimonial"));
                return null;
            }
        }

        internal static DataTable getTestimonial(int number, string exceptId)
        {
            try
            {
                object[,] parameters = { {"@exceptId", exceptId } };
                return DB.getData("select top " +  number + " * from Testimonial where id != @exceptId order by NEWID();", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTestimonial"));
                return null;
            }
        }

        internal static DataTable getTestimonialById(string id)
        {
            try
            {
                object[,] parameters = { { "@id", id } };
                return DB.getData("select * from Testimonial where id = @id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getTestimonialById"));
                return null;
            }
        }

        internal static bool deleteTestimonial(string gvIDs)
        {
            try
            {
                return DB.exec("delete from TESTIMONIAL where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteTestimonial"));
                return false;
            }
        }

        internal static bool updateTestimonial(Testimonial testimonial)
        {
            try
            {
                object[,] parameters =  {   { "@name", testimonial.name },
                                            { "@shortDesc", testimonial.shortDesc},
                                            { "@text", testimonial.text},
                                            { "@id", testimonial.id}
                                         };

                return DB.exec("update Testimonial set name = @name, shortDesc = @shortDesc, text = @text where Id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateTestimonial"));
                return false;
            }
        }

        internal static bool createTestimonial(Testimonial testimonial)
        {
            try
            {
                object[,] parameters =  {   { "@name", testimonial.name },
                                            { "@shortDesc", testimonial.shortDesc},
                                            { "@text", testimonial.text},
                                            { "@postedDate", testimonial.postedDate}
                                         };

                return DB.exec("insert into Testimonial (name, shortDesc, text, postedDate) " +
                    "values (@name, @shortDesc, @text, @postedDate) ;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createTestimonial"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('TESTIMONIAL')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentTestimonialId"));
                return "";
            }
        }
    }
}