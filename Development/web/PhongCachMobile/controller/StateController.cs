﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.model;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile.controller
{
    public class StateController
    {
        public static DataTable getStates()
        {
            //get
            try
            {
                return DB.getData("select * from STATE ");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getStates"));
                return null;
            }
        }
    }
}