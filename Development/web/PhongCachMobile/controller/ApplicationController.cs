﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public enum ApplicationType
    {
        Unknown,
        Application,
        Game,
    }

    public enum OSType
    {
        Unknown,
        iOS,
        Android,
        WindowsPhone
    }

    public class ApplicationController
    {
        static ApplicationController()
        {
        }

        public static DataTable getRandomApplications(int categoryId, int number)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select top " + number + " * from Application where CategoryId = @categoryId order by NEWID()", parameters);
                }
                else
                {
                    return DB.getData("select top " + number + " * from Application order by NEWID()");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getGamesByCategory"));
                return null;
            }
        }

        public static DataTable getAppsByCategory(int categoryId, int number)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select top " + number + " * from APPLICATION where CategoryId = @categoryId;", parameters);
                }
                else
                {
                    return DB.getData("select top " + number + " * from APPLICATION order by posteddate desc");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAppsByCategory"));
                return null;
            }
        }

        public static DataTable getAppsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from APPLICATION where CategoryId = @categoryId;", parameters);
                }
                else
                {
                    return DB.getData("select * from APPLICATION order by posteddate desc");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAppsByCategory"));
                return null;
            }
        }

        public static DataTable getViewCountAppsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from APPLICATION where CategoryId = @categoryId order by viewcount desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from APPLICATION order by viewcount desc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getViewCountAppsByCategory"));
                return null;
            }
        }

        public static DataTable getDownloadCountAppsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from APPLICATION where CategoryId = @categoryId order by downloadcount desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from APPLICATION order by downloadcount desc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getDownloadCountAppsByCategory"));
                return null;
            }
        }

        public static Dictionary<OSType, int[]> osTypeAppOSIDsMap = new Dictionary<OSType, int[]>()
        {
                {OSType.iOS, new int[]{11, 14}},
                {OSType.Android, new int[]{12, 15}},
                {OSType.WindowsPhone, new int[]{10, 13}},
        };

        public static Dictionary<int, ApplicationType> appCategoryTypeMap = new Dictionary<int, ApplicationType>
        {
            {-1, ApplicationType.Unknown},
            {22, ApplicationType.Game},
            {24, ApplicationType.Game},
            {27, ApplicationType.Game},
            {32, ApplicationType.Application},
            {34, ApplicationType.Application},
            {39, ApplicationType.Application},
        };

        public static int getAppAndGameCount(OSType osType)
        {
            string osIDList = string.Join(",", osTypeAppOSIDsMap[osType]);
            return int.Parse(DB.getValue("SELECT COUNT(ID) FROM Application WHERE OSID IN (" + osIDList + ")")) +
                int.Parse(DB.getValue("SELECT COUNT(ID) FROM Game WHERE OSID IN (" + osIDList + ")"));
        }

        internal static DataTable getApps(string categoryId, string osId)
        {
            //get
            try
            {
                object[,] parameters = { {"@categoryId", categoryId },
                                         {"@osId", osId }};
                return DB.getData("select * from APPLICATION where CategoryId = @categoryId and OsId = @osId", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getApps"));
                return null;
            }
        }

        internal static bool createApp(Application application)
        {
            try
            {
                object[,] parameters =  {   { "@name", application.name },
                                        { "@isFree", application.isFree},
                                        { "@description", application.description},
                                        { "@likeCount", application.likeCount},
                                        { "@viewCount", application.viewCount},
                                        { "@Filename", application.filename},
                                        { "@currentVersion", application.currentVersion},
                                        { "@developer", application.developer},
                                        { "@postedDate", application.postedDate},
                                        { "@requiredOS", application.requiredOS},
                                        { "@filesize", application.filesize},
                                        { "@categoryId", application.categoryId},
                                        { "@osId", application.osId}
                                    };

                string appTable = application is Game ? "Game" : "Application";
                return DB.exec("insert into " + appTable + " (name, isFree, [Description], likeCount, viewCount, Filename, currentVersion, developer, postedDate, requiredOS, filesize, categoryId, osId) " +
                    "values (@name, @isFree, @description, @likeCount, @viewCount, @Filename, @currentVersion, @developer, @postedDate, @requiredOS, @filesize, @categoryId, @osId);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createApp"));
                return false;
            }
        }

        internal static bool updateApp(Application application)
        {
            try
            {
                object[,] parameters =  {   { "@name", application.name },
                                        { "@isFree", application.isFree},
                                        { "@description", application.description},
                                        { "@likeCount", application.likeCount},
                                        { "@viewCount", application.viewCount},
                                        { "@Filename", application.filename},
                                        { "@currentVersion", application.currentVersion},
                                        { "@developer", application.developer},
                                        { "@postedDate", application.postedDate},
                                        { "@requiredOS", application.requiredOS},
                                        { "@filesize", application.filesize},
                                        { "@id", application.id }
                                    };

                string appTable = application is Game ? "Game" : "Application";
                return DB.exec("update " + appTable + " set name = @name, isFree = @isFree, [Description] = @description, likeCount = @likeCount, viewCount = @viewCount, Filename = @Filename, currentVersion = @currentVersion, " +
                    "developer = @developer, postedDate = @postedDate, requiredOS = @requiredOS, filesize = @filesize where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateApp"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('APPLICATION')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentAppId"));
                return "";
            }
        }

        internal static bool deleteApplication(string gvIDs)
        {
            try
            {
                return DB.exec("delete from APPLICATION where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteApplication"));
                return false;
            }
        }

        internal static bool addViewCount(string id)
        {
            try
            {
                return DB.exec("update Application set ViewCount = ViewCount+1 where id = " + id);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addViewCountApp"));
                return false;
            }
        }

        public static string getAppLink(Application app)
        {
            return Common.m() + "ud-" + HTMLRemoval.StripTagsRegex(app.name).ToLower().Replace(' ', '-') + "-" + app.id;
        }
        public static Application rowToApplication(DataRow row)
        {
            int[] gameCatIDs = new int[] { 22, 24, 27 };
            // Apps: 32, 34, 39

            Application app = gameCatIDs.Contains((int)row["CategoryId"]) ? new Game() : new Application();

            app.id = (int)row["Id"];
            app.name = row["Name"] as string;
            app.description = row["Description"].ToString();
            app.categoryId = (int)row["CategoryId"];
            app.likeCount = (int)row["LikeCount"];
            app.viewCount = (int)row["ViewCount"];
            app.displayImage = row["DisplayImage"] as string;
            app.osId = (int)row["OSId"];
            app.filename = row["Filename"] as string;
            app.filesize = row["Filesize"] as string;
            app.postedDate = row["PostedDate"] as string;
            app.requiredOS = row["RequiredOS"] as string;
            app.currentVersion = row["CurrentVersion"] as string;
            app.developer = row["Developer"] as string;

            app.link = getAppLink(app);
            //app.download = (app is Game ? "game" : "app") + "/" + app.filename;

            return app;
        }

        public static DataTable getAppWithFilter(ApplicationType appType, int categoryID, OSType osType, bool sortView, out int remainCount, int pageIndex = 0, int pageSize = 0)
        {
            remainCount = 0;

            if (pageIndex < 0)
                pageIndex = 0;
            if (pageSize <= 0)
                pageSize = int.Parse(SysIniController.getSetting("Ung_Dung_So_SP_Filter_Page", "8"));

            //get
            try
            {
                // Build WHERE expression
                string whereExp =
                    // cat: all / catID
                    (categoryID <= 0 ? "" : " AND categoryID = " + categoryID) +
                    // os
                    (osType == OSType.Unknown ? "" : " AND OSID IN (" + string.Join(",", osTypeAppOSIDsMap[osType]) + ")");
                if (whereExp.StartsWith(" AND"))
                    whereExp = "WHERE " + whereExp.Substring(4);

                // Build ORDER expression
                string orderExp = sortView ? " ORDER BY viewCount DESC" : " ORDER BY PostedDate DESC";

                string query = "";
                if (appType == ApplicationType.Unknown)
                {
                    query = "* FROM ( " +
                        "SELECT Id, Name, IsFree, Description, CategoryId, LikeCount, ViewCount, DisplayImage, OSId, Filename, Filesize, PostedDate, RequiredOS, CurrentVersion, Developer " +
                        "	FROM APPLICATION " +
                        "UNION " +
                        "SELECT Id, Name, IsFree, Description, CategoryId, LikeCount, ViewCount, DisplayImage, OSId, Filename, Filesize, PostedDate, RequiredOS, CurrentVersion, Developer " +
                        "	FROM GAME) AS U " + whereExp;
                }
                else
                {
                    query = "* FROM " + (appType == ApplicationType.Application ? "Application " : "Game ") + whereExp;
                }

                // Get count
                remainCount = int.Parse(DB.getValue("SELECT COUNT(ID) " + query.Substring(2))) - (pageIndex * pageSize + pageSize);
                if (remainCount < 0)
                    remainCount = 0;

                object[,] paramss;
                query = DB.wrapInPager(query, orderExp, pageIndex, pageSize, out paramss);
                return DB.getData(query, paramss);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }
    }
}