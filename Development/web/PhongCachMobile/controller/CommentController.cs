﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class CommentController
    {
        public static string getNumberOfCommentsByArticleId(string articleId)
        {
            //get
            try
            {
                object[,] parameters = { { "@articleId", articleId } };
                return DB.getValue("select COUNT(*) from COMMENT where articleId=@articleId and IsApproved != 'false';", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfCommentsByArticleId"));
                return "";
            }
        }

        public static DataTable getNumberOfCommentsByArticleIDs(List<int> articleIDs)
        {
            //get
            try
            {
                string ids = string.Join(",", articleIDs);
                return DB.getData("SELECT articleID, COUNT(*) CommentCount FROM COMMENT WHERE articleID IN (" + ids + ") AND IsApproved != 'false' GROUP BY articleID;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfCommentsByArticleId"));
                return null;
            }
        }

        public static string getNumberOfComments()
        {
            //get
            try
            {
                string result = DB.getValue("select COUNT(*) from COMMENT where IsApproved != 'false'");
                if (result == "")
                    return "0";
                else
                    return result;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfComments"));
                return "";
            }
        }

        public static DataTable getCommentsByArticleId(string articleId, string parentId)
        {
            //get
            try
            {
                if (parentId == "")
                {
                    object[,] parameters = { { "@articleId", articleId }, 
                                       { "@parentId", DBNull.Value }, };
                    return DB.getData("select * from COMMENT where articleId=@articleId and parentId is null and IsApproved != 'false' order by posteddate desc;", parameters);
                }
                else
                {
                    object[,] parameters = { { "@articleId", articleId }, 
                                    { "@parentId", parentId }, };
                    return DB.getData("select * from COMMENT where articleId=@articleId and parentId = @parentId and IsApproved != 'false' order by posteddate desc;", parameters);

                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCommentsByArticleId"));
                return null;
            }
        }

        public static bool createArticleComment(Comment comment)
        {
            //get
            try
            {
                if (comment.parentId != -1)
                {
                    object[,] parameters = { { "@parentId", comment.parentId },
                                       { "@description", comment.description },
                                       { "@postedDate", comment.postedDate }, 
                                       { "@likeCount", comment.likeCount }, 
                                       { "@userId", comment.userId }, 
                                       { "@articleId", comment.articleId }};
                    return DB.exec("insert into Comment(parentId, [Description], postedDate, likeCount, userId, articleId) " +
                        "values (@parentId, @description, @postedDate, @likeCount, @userId, @articleId);", parameters);
                }
                else
                {
                    object[,] parameters = { { "@parentId", DBNull.Value},
                                       { "@description", comment.description },
                                       { "@postedDate", comment.postedDate }, 
                                       { "@likeCount", comment.likeCount }, 
                                       { "@userId", comment.userId }, 
                                       { "@articleId", comment.articleId }};
                    return DB.exec("insert into Comment(parentId, [Description], postedDate, likeCount, userId, articleId) " +
                        "values (@parentId, @description, @postedDate, @likeCount, @userId, @articleId);", parameters);
                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createArticleComment"));
                return false;
            }
        }

        public static string getNumberOfCommentsByProductId(string productId)
        {
            //get
            try
            {
                object[,] parameters = { { "@productId", productId } };
                return DB.getValue("select COUNT(*) from COMMENT where productId=@productId and IsApproved != 'false';", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfCommentsByProductId"));
                return "";
            }
        }

        public static DataTable getCommentsByProductId(string productId, string parentId)
        {
            //get
            try
            {
                if (parentId == "")
                {
                    object[,] parameters = { { "@productId", productId }, 
                                       { "@parentId", DBNull.Value }, };
                    return DB.getData("select * from COMMENT where productId=@productId and parentId is null and IsApproved != 'false' order by posteddate desc;", parameters);
                }
                else
                {
                    object[,] parameters = { { "@productId", productId }, 
                                    { "@parentId", parentId }, };
                    return DB.getData("select * from COMMENT where productId=@productId and parentId = @parentId and IsApproved != 'false' order by posteddate desc;", parameters);

                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCommentsByProductId"));
                return null;
            }
        }

        public static bool createProductComment(Comment comment)
        {
            //get
            try
            {
                if (comment.parentId != -1)
                {
                    object[,] parameters = { { "@parentId", comment.parentId },
                                       { "@description", comment.description },
                                       { "@postedDate", comment.postedDate }, 
                                       { "@likeCount", comment.likeCount }, 
                                       { "@userId", comment.userId }, 
                                       { "@productId", comment.productId }, };
                    return DB.exec("insert into Comment(parentId, [Description], postedDate, likeCount, userId, productId) " +
                        "values (@parentId, @description, @postedDate, @likeCount, @userId, @productId);", parameters);
                }
                else
                {
                    object[,] parameters = { { "@parentId", DBNull.Value},
                                       { "@description", comment.description },
                                       { "@postedDate", comment.postedDate }, 
                                       { "@likeCount", comment.likeCount }, 
                                       { "@userId", comment.userId }, 
                                       { "@productId", comment.productId }, };
                    return DB.exec("insert into Comment(parentId, [Description], postedDate, likeCount, userId, productId) " +
                        "values (@parentId, @description, @postedDate, @likeCount, @userId, @productId);", parameters);
                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createProductComment"));
                return false;
            }
        }

        public static DataTable getComments(string from, string to)
        {
            //get
            try
            {
                object[,] parameters = { { "@StartTime", from }, 
                                       {"@EndTime", to}};
                DataTable dt = DB.getData("select COMMENT.*, Email from COMMENT join SYSUSER on COMMENT.UserId = SysUser.Id " +
                    "where PostedDate > @StartTime and PostedDate < @EndTime;", parameters);
                return dt;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getComments"));
                return null;
            }
        }

        public static bool deleteComment(string gvIDs)
        {
            try
            {
                return DB.exec("delete from Comment where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteComment"));
                return false;
            }
        }

        public static bool approveComment(string gvIDs)
        {
            try
            {
                return DB.exec("update Comment set IsApproved = 'true' where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error approveComment"));
                return false;
            }
        }

        public static bool updateComment(Comment comment)
        {
            try
            {
                object[,] parameters = {   {"@Id", comment.id},
                                           {"@Description", comment.description}
                                    };
                return DB.exec("update Comment set [Description] = @Description where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateComment"));
                return false;
            }
        }

        public static DataTable getCommentsByArticleIdAsc(string articleId, string parentId)
        {
            //get
            try
            {
                if (parentId == "")
                {
                    object[,] parameters = { { "@articleId", articleId }, 
                                       { "@parentId", DBNull.Value }, };
                    return DB.getData("select * from COMMENT where articleId=@articleId and parentId is null and IsApproved != 'false'  order by posteddate asc;", parameters);
                }
                else
                {
                    object[,] parameters = { { "@articleId", articleId }, 
                                    { "@parentId", parentId }, };
                    return DB.getData("select * from COMMENT where articleId=@articleId and parentId = @parentId and IsApproved != 'false'  order by posteddate asc;", parameters);

                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCommentsByArticleIdAsc"));
                return null;
            }
        }

        public static DataTable getCommentsByProductIdAsc(string productId, string parentId)
        {
            //get
            try
            {
                if (parentId == "")
                {
                    object[,] parameters = { { "@productId", productId }, 
                                       { "@parentId", DBNull.Value }, };
                    return DB.getData("select * from COMMENT where productId=@productId and parentId is null and IsApproved != 'false'  order by posteddate asc;", parameters);
                }
                else
                {
                    object[,] parameters = { { "@productId", productId }, 
                                    { "@parentId", parentId }, };
                    return DB.getData("select * from COMMENT where productId=@productId and parentId = @parentId and IsApproved != 'false'  order by posteddate asc;", parameters);

                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCommentsByProductIdAsc"));
                return null;
            }
        }
    }
}