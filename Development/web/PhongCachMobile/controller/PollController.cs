﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class PollController
    {
        public static DataTable getLastest(string currentDateTime)
        {
            //get
            try
            {
                object[,] parameter = { { "@currentDateTime", currentDateTime } };
                return DB.getData("select top 1 * from Poll where startDate < @currentDateTime and endDate > @currentDateTime;", parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getLastest"));
                return null;
            }
        }

        public static DataTable getPolls()
        {
            //get
            try
            {
                return DB.getData("select * from Poll;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getPolls"));
                return null;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('POLL')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentPollId"));
                return "";
            }
        }

        internal static bool createPoll(Poll poll)
        {
            try
            {
                object[,] parameters =  {   { "@name", poll.name },
                                        { "@startDate", poll.startDate},
                                        { "@endDate", poll.endDate}
                                       };
                return DB.exec("insert into Poll (name, startDate, endDate) " +
                    "values (@name, @startDate, @endDate);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createPoll"));
                return false;
            }
        }

        internal static bool updatePoll(Poll poll)
        {
            try
            {
                object[,] parameters =  {   { "@name", poll.name },
                                        { "@startDate", poll.startDate},
                                        { "@endDate", poll.endDate},
                                        { "@id", poll.id}
                                       };
                return DB.exec("update POLL set name = @name, startDate = @startDate, endDate = @endDate where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updatePoll"));
                return false;
            }
        }
    }
}