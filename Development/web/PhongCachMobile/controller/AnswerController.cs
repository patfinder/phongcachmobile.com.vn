﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.controller
{
    public class AnswerController
    {
        internal static string getNumberOfAnswerByAskId(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@askId", id } };
                return DB.getValue("select COUNT(*) from ANSWER where askId=@askId;", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberOfAnswerByAskId"));
                return "";
            }
        }

        internal static string getNumberAnswer()
        {
            //get
            try
            {
                return DB.getValue("select count(*) from ANSWER");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberAnswer"));
                return null;
            }
        }

        internal static bool createAnswer(Answer answer)
        {
            //get
            try
            {
                if (answer.parentId != -1)
                {
                    object[,] parameters = { { "@parentId", answer.parentId },
                                       { "@description", answer.description },
                                       { "@postedDate", answer.postedDate }, 
                                       { "@likeCount", answer.likeCount }, 
                                       { "@userId", answer.userId }, 
                                       { "@askId", answer.askId }};
                    return DB.exec("insert into Answer(parentId, [Description], postedDate, likeCount, userId, askId) " +
                        "values (@parentId, @description, @postedDate, @likeCount, @userId, @askId);", parameters);
                }
                else
                {
                    object[,] parameters = { { "@parentId", DBNull.Value},
                                       { "@description", answer.description },
                                       { "@postedDate", answer.postedDate }, 
                                       { "@likeCount", answer.likeCount }, 
                                       { "@userId", answer.userId },
                                        { "@askId", answer.askId }};
                    return DB.exec("insert into Answer(parentId, [Description], postedDate, likeCount, userId, askId) " +
                        "values (@parentId, @description, @postedDate, @likeCount, @userId, @askId);", parameters);
                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createAnswer"));
                return false;
            }
        }

        internal static DataTable getAnswersByAskIdAsc(string askId, string parentId)
        {
            //get
            try
            {
                if (parentId == "")
                {
                    object[,] parameters = { { "@askId", askId }, 
                                       { "@parentId", DBNull.Value }, };
                    return DB.getData("select * from ANSWER where askId=@askId and parentId is null order by posteddate asc;", parameters);
                    //return DB.getData("select * from ANSWER where askId=@askId and parentId is null and IsApproved != 'false'  order by posteddate asc;", parameters);
                }
                else
                {
                    object[,] parameters = { { "@askId", askId }, 
                                    { "@parentId", parentId }, };
                    return DB.getData("select * from ANSWER where askId=@askId and parentId = @parentId order by posteddate asc;", parameters);
                    //return DB.getData("select * from ANSWER where askId=@askId and parentId = @parentId and IsApproved != 'false'  order by posteddate asc;", parameters);

                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAnswersByAskIdAsc"));
                return null;
            }
        }

        internal static DataTable getAnswersByAskId(string askId, string parentId)
        {
            //get
            try
            {
                if (parentId == "")
                {
                    object[,] parameters = { { "@askId", askId }, 
                                       { "@parentId", DBNull.Value }, };
                    return DB.getData("select * from ANSWER where askId=@askId and parentId is null order by posteddate desc;", parameters);
                    //return DB.getData("select * from ANSWER where askId=@askId and parentId is null and IsApproved != 'false' order by posteddate desc;", parameters);
                }
                else
                {
                    object[,] parameters = { { "@askId", askId }, 
                                    { "@parentId", parentId }, };
                    return DB.getData("select * from ANSWER where askId=@askId and parentId = @parentId  order by posteddate desc;", parameters);
                    //return DB.getData("select * from ANSWER where articleId=@articleId and parentId = @parentId and IsApproved != 'false' order by posteddate desc;", parameters);

                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAnswersByAskId"));
                return null;
            }
        }
    }
}