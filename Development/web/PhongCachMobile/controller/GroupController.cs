﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.controller
{
    public class GroupController
    {
        public static DataTable getGroupsByFilter(string filter)
        {
            //get
            try
            {
                object[,] parameters = { { "@filter", filter } };
                return DB.getData("select * from [Group] where filter = @filter;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getGroupsByFilter"));
                return null;
            }
        }

        public static string getGroupNameById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                return DB.getValue("select Name from [Group] where id = @id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getGroupNameById"));
                return null;
            }
        }
    }
}