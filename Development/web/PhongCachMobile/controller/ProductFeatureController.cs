﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System.Data;

namespace PhongCachMobile.controller
{
    public class ProductFeatureController
    {

        public static bool updateProductFeature(ProductFeature feature)
        {
            try
            {
                object[,] parameters =  {   { "@FeatureId", feature.featureId },
                                        { "@ProductId", feature.productId},
                                        { "@Value", feature.value }
                                    };
                return DB.exec("update PRODUCTFEATURE set Value = @Value where ProductId = @ProductId and FeatureId = @FeatureId;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateProductFeature"));
                return false;
            }
        }

        public static bool createProductFeature(ProductFeature feature)
        {
            try
            {

                object[,] parameters =  {   { "@FeatureId", feature.featureId },
                                        { "@ProductId", feature.productId},
                                        { "@Value", feature.value }
                                    };
                return DB.exec("insert into PRODUCTFEATURE (productId, featureId, value) " +
                    "values (@ProductId, @FeatureId, @Value);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createProductFeature"));
                return false;
            }
        }


        internal static DataTable getFeatureByProductId(string id)
        {
            try
            {

                object[,] parameters =  {   { "@Id", id } };
                return DB.getData("select featureId, value from PRODUCTFEATURE where productId = @id and featureId not in (1,2,3)", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFeatureByProductId"));
                return null;
            }
        }

        internal static DataTable getFeatureByProductId(string productId, string featureId)
        {
            try
            {

                object[,] parameters = { { "@productId", productId },
                                         { "@featureId", featureId }};
                return DB.getData("select * from PRODUCTFEATURE where productId = @productId and featureId = @featureId", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFeatureByProductId"));
                return null;
            }
        }
    }
}