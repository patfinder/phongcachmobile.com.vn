﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class VideoController
    {
        internal static DataTable getVideoByProductId(string productId)
        {
            try
            {
                object[,] parameter = { { "@productId", productId } };
                return DB.getData("select * from VIDEO where productId = @productId;", parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getVideoByProductId"));
                return null;
            }
        }

        internal static bool deleteVideo(string gvIDs)
        {
            try
            {
                return DB.exec("delete from VIDEO where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteVideo"));
                return false;
            }
        }

        internal static bool updateVideo(Video video)
        {
            try
            {
                object[,] parameters =  {   
                                    { "@id", video.id },
                                    { "@title", video.title },
                                    { "@video", video.video }
                                };
                return DB.exec("update Video set video = @video, title = @title where Id = @Id", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateVideo"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('VIDEO')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentVideoId"));
                return "";
            }
        }

        internal static bool createVideo(Video video)
        {

            try
            {
                object[,] parameters =  {   
                                    { "@video", video.video },
                                    { "@title", video.title },
                                    { "@productId", video.productId}
                                };
                return DB.exec("insert into Video (video, title, productId) values " +
                    "(@video, @title, @productId);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createVideo"));
                return false;
            }
        }
    }
}