﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Globalization;
using System.Threading;

namespace PhongCachMobile.controller
{
    public class ProductController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ProductController).FullName);

        public const string ProductGroupPhone = "3";
        public const int ProductGroupPhoneInt = 3;
        public const string ProductGroupTablet = "4";
        public const int ProductGroupTabletInt = 4;
        public const string ProductGroupGame = "5";
        public const int ProductGroupGameInt = 5;
        public const string ProductGroupApplication = "6";
        public const int ProductGroupApplicationInt = 6;
        public const string ProductGroupAccessory = "7";
        public const int ProductGroupAccessoryInt = 7;

        public const string ProductGroupMacbook = "1076";
        public const int ProductGroupMacbookInt = 1076;

        public const int TabletCategoryApple = 15;
        public const int TabletCategorySamsung = 16;
        public const int TabletCategoryOther = 9999;

        public static Dictionary<int, string> ProductGroupIDNameMap = new Dictionary<int, string>
        {
            {ProductGroupPhoneInt, "Điện thoại"},
            {ProductGroupMacbookInt, "Macbook"},
            {ProductGroupTabletInt, "Máy tính bảng"},
            {ProductGroupGameInt, "Game"},
            {ProductGroupApplicationInt, "Ứng dụng"},
            {ProductGroupAccessoryInt, "Phụ kiện"},
        };

        public static Dictionary<int, string> ProductGroupIDLinkMap = new Dictionary<int, string>
        {
            {ProductGroupPhoneInt, "dtdd"},
            {ProductGroupMacbookInt, "macbook"},
            {ProductGroupTabletInt, "may-tinh-bang"},
            {ProductGroupGameInt, "ung-dung"},
            {ProductGroupApplicationInt, "ung-dung"},
            {ProductGroupAccessoryInt, "pkdtdd"},
        };

        public static readonly Dictionary<string, int> PriceList = new Dictionary<string, int>()
            {
                {"Tất cả", -1},
                {"Dưới 1 triệu", 1000000},
                {"Dưới 2 triệu", 2000000},
                {"Dưới 3 triệu", 3000000},
                {"Dưới 5 triệu", 5000000},
                {"Dưới 8 triệu", 8000000},
                {"Dưới 10 triệu", 10000000},
                {"Trên 10 triệu", 2000000000},
            };

        public static DataTable getRandomSpecial(int number, string groupId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (groupId == "")
                {
                    dt = DB.getData("select top " + number + " * from Product where IsSpeedySold = 'true' and flag = 'true' and isComingSoon = 0 and isOutOfStock = 0 order by NEWID();");
                }
                else
                {
                    object[,] parameters =  {{ "@groupId", groupId }};
                    dt = DB.getData("select top " + number + " * from Product where IsSpeedySold = 'true' and flag = 'true' and isComingSoon = 0 and isOutOfStock = 0 and groupId = @groupId order by NEWID();", parameters);
                }
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getRandomSpecial"));
                return null;
            }
        }
             
        public static DataTable getRandomFeatured(int number, string groupId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (groupId == "")
                {
                    dt = DB.getData("select top " + number + " * from Product where IsFeatured = 'true' and flag = 'true' order by NEWID();");
                }
                else
                {
                    object[,] parameters = { { "@groupId", groupId } };
                    dt = DB.getData("select top " + number + " * from Product where IsFeatured = 'true' and flag = 'true' and groupId = @groupId order by NEWID();", parameters);
                }
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getRandomFeatured"));
                return null;
            }
        }

        public static DataTable getNew(int number, string groupId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (groupId == "")
                {
                    // dt = DB.getData("select top " + number + " * from Product where flag = 'true' order by Id desc;");
                    dt = DB.getData("select top " + number + " * from Product where flag = 'true' AND isFeatured = 'true' order by Id desc;");
                }
                else
                {
                    object[,] parameters = { { "@groupId", groupId } };
                    //dt = DB.getData("select top " + number + " * from Product where flag = 'true' and groupId = @groupId order by Id desc;", parameters);
                    dt = DB.getData("select top " + number + " * from Product where flag = 'true' AND isFeatured = 'true' AND groupId = @groupId order by Id desc;", parameters);
                }
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNew"));
                return null;
            }
        }
        
        public static DataTable getProductsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from PRODUCT where CategoryId = @categoryId and flag = 'true' order by isComingSoon desc, id desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from PRODUCT where flag = 'true' order by isComingSoon desc, id desc");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByCategory"));
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="categoryId">-1: all, </param>
        /// <returns></returns>
        public static int getProductCountByCategory(int groupID, int categoryID = TabletCategoryOther)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupID", groupID } };

                // -1: all, 15: Apple, 16: Samsung, TabletCategoryOther: others
                if (categoryID == -1)
                    return int.Parse(DB.getValue("SELECT count(id) FROM PRODUCT WHERE groupID = @groupID AND flag = 'true';", parameters));
                else if (categoryID == TabletCategoryOther)
                    return int.Parse(DB.getValue("SELECT count(id) FROM PRODUCT WHERE groupID = @groupID AND flag = 'true' AND categoryID != 15 AND categoryID != 16;", parameters));
                else {
                    parameters = new object[,] { { "@groupID", groupID }, { "@categoryID", categoryID } };
                    return int.Parse(DB.getValue("SELECT count(id) FROM PRODUCT WHERE groupID = @groupID AND flag = 'true' AND categoryID = @categoryID;", parameters));
                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByCategory"));
                return 0;
            }
        }

        public static DataTable getBackEndProductsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from PRODUCT where CategoryId = @categoryId  order by id desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from PRODUCT  order by id desc");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByCategory"));
                return null;
            }
        }

        public static DataTable getAccessoriesByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from PRODUCT where CategoryId = @categoryId and flag = 'true' order by isComingSoon desc, CurrentPrice desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from PRODUCT where flag = 'true' order by CurrentPrice desc");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByCategory"));
                return null;
            }
        }

        public static DataTable getSimpleProductsByGroupId(int groupId, string filter)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                // TOP 20 
                return DB.getData("select ID, Name from PRODUCT where flag = 'true' and groupId = @groupId AND Name LIKE '%" + filter + "%' ORDER BY Name;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }

        public static DataTable getProductsByGroupId(int groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from PRODUCT where flag = 'true' and CategoryId in (select Id from Category where groupId = @groupId)  order by isComingSoon desc, id desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }

        public static DataTable getRandomProductsByGroupId(int groupId, int number)
        {
            //get
            try
            {
                if(groupId > 0)
                {
                    object[,] parameters = { { "@groupId", groupId } };
                    return DB.getData("SELECT TOP " + number + " * FROM PRODUCT WHERE flag = 'true' AND CategoryId IN (SELECT Id FROM Category WHERE groupId = @groupId) ORDER BY NEWID();", parameters);
                }
                else
                {
                    return DB.getData("SELECT TOP " + number + " * FROM PRODUCT WHERE flag = 'true' AND ORDER BY NEWID();");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }

        public static DataTable getProductsByGroupId(string groupId, int number)
        {
            return getProductsByGroupId(int.Parse(groupId), number);
        }
        public static DataTable getProductsByGroupId(int groupId, int number)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select top " + number + " * from PRODUCT where flag = 'true' and CategoryId in (select Id from Category where groupId = @groupId)  order by isComingSoon desc, id desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }
        public static DataTable getProductsByCategoryIDs(int[] catIDs, int number)
        {
            //get
            try
            {
                string catIDsStr = string.Join(",", catIDs);
                return DB.getData("select top " + number + " * from PRODUCT where flag = 'true' and CategoryId in (" + catIDsStr + ") order by NEWID();");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }

        public static DataTable getProductsByGroupId(int groupID, int categoryID, int priceMax, int os, bool promotion, bool sortPrice, out int remainCount, int pageIndex = 0, int pageSize = 0)
        {
            remainCount = 0;

            if (pageIndex < 0)
                pageIndex = 0;
            if (pageSize <= 0)
                pageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "20"));

            //get
            try
            {
                // Order Expression
                string orderExp = "ORDER BY isComingSoon DESC, isOutOfStock ASC, currentPrice " + (sortPrice ? "DESC" : "ASC");
                string query = "* FROM PRODUCT WHERE flag = 'true' AND groupID = " + groupID.ToString() +
                    // cat: all
                    (categoryID <= 0 ? "" :
                    // cat: other
                    (categoryID == TabletCategoryOther ? " AND categoryID != 15 AND categoryID != 16" :
                        // cat: specific
                        " AND categoryID = " + categoryID)) +
                    (priceMax > 0 && priceMax <= 10000000 ? " AND currentPrice <= " + priceMax : 
                    priceMax > 0 && priceMax > 10000000 ? " AND currentPrice > 10000000" : "") +
                    (os >= 0 ? " AND OSID = " + os : "") +
                    (promotion ? " AND DiscountPrice IS NOT NULL" : "");

                // Get count
                remainCount = int.Parse(DB.getValue("SELECT COUNT(ID) " + query.Substring(2))) - (pageIndex * pageSize + pageSize);
                if (remainCount < 0)
                    remainCount = 0;
            
                object[,] paramss;
                query = DB.wrapInPager(query, orderExp, pageIndex, pageSize, out paramss);
                return DB.getData(query, paramss);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }

        public static DataTable getAccessoriesByGroupId(int groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from PRODUCT where flag = 'true' and CategoryId in (select Id from Category where groupId = @groupId)  order by isComingSoon desc, CurrentPrice desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByGroupId"));
                return null;
            }
        }

        public static DataTable getLowToHighPriceProductsByGroupId(int groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from PRODUCT where flag = 'true' and CategoryId in (select Id from Category where groupId = @groupId) order by isComingSoon desc, currentPrice asc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getLowToHighPriceProductsByCategory"));
                return null;
            }
        }

        public static DataTable getLowToHighPriceProductsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from PRODUCT where flag = 'true' and CategoryId = @categoryId order by isComingSoon desc, currentPrice asc;", parameters);
                }
                else
                {
                    return DB.getData("select * from PRODUCT where flag = 'true' order by currentPrice asc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getLowToHighPriceProductsByCategory"));
                return null;
            }
        }

        public static DataTable getHighToLowPriceProductsByGroupId(int groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from PRODUCT where  flag = 'true' and CategoryId in (select Id from Category where groupId = @groupId)  order by isComingSoon desc, currentPrice desc;", parameters);
              
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getHighToLowPriceProductsByCategory"));
                return null;
            }
        }

        public static DataTable getHighToLowPriceProductsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from PRODUCT where  flag = 'true' and CategoryId = @categoryId order by isComingSoon desc, currentPrice desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from PRODUCT order by currentPrice desc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getHighToLowPriceProductsByCategory"));
                return null;
            }
        }

        public static DataTable getHighViewProductsByGroupId(int groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from PRODUCT where  flag = 'true' and CategoryId in (select Id from Category where groupId = @groupId) order by isComingSoon desc, viewcount desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getHighViewProductsByCategory"));
                return null;
            }
        }

        public static DataTable getHighViewProductsByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from PRODUCT where  flag = 'true' and CategoryId = @categoryId order by isComingSoon desc, viewcount desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from PRODUCT order by viewcount desc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getHighViewProductsByCategory"));
                return null;
            }
        }

        public static DataTable getNewPhoneTablet(int number)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                dt = DB.getData("select top " + number + " * from Product where  flag = 'true' and (groupid = 3 or groupid = 4) order by Id desc;");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNewPhoneTablet"));
                return null;
            }
        }

        public static DataTable getNewAccessories(int number)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                dt = DB.getData("select top " + number + " * from Product where  flag = 'true' and (groupid = 7 or groupid = 8) order by Id desc;");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNewAccessories"));
                return null;
            }
        }

        public static bool addViewCount(string id)
        {
            try
            {
                object[,] parameters = {   {"@Id", id}
                                    };
                return DB.exec("update Product set ViewCount = ViewCount + 1 where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addViewCount"));
                return false;
            }
        }

        public static DataTable getProductById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                DataTable dt = DB.getData("select * from PRODUCT where id = @id", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductById"));
                return null;
            }
        }

        public static DataTable getProductByIDs(List<int> IDs)
        {
            //get
            try
            {
                if (IDs == null || IDs.Count <= 0)
                    return null;

                DataTable dt = DB.getData("select * from PRODUCT where id in (" + string.Join(",", IDs) + ")");
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductById"));
                return null;
            }
        }

        public static DataTable getOtherProducts(int number, string exceptId)
        {
            //get
            try
            {
                object[,] parameters = { { "@exceptId", exceptId } };
                DataTable dt = DB.getData("select top " + number + " * from PRODUCT where  flag = 'true' and id != @exceptId and CategoryId = (select CategoryId from PRODUCT where id = @exceptId) order by NEWID();", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getOtherProducts"));
                return null;
            }
        }

        public static DataTable getDiscountProducts(int number = -1)
        {
            //get
            try
            {
                return DB.getData("select" + (number <= 0 ? "" : " top " + number) + " * from Product where  flag = 'true' and DiscountPrice is not null order by DiscountPrice desc;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getDiscountProducts"));
                return null;
            }
        }
        public static DataTable getDiscountProducts2(out int remainCount, int pageIndex = 0, int pageSize = 0)
        {
            remainCount = 0;

            if (pageIndex < 0)
                pageIndex = 0;
            if (pageSize <= 0)
                pageSize = int.Parse(SysIniController.getSetting("Gia_Hot_So_SP_Filter_Page", "16"));

            //get
            try
            {
                // Order Expression
                string orderExp = "ORDER BY isOutOfStock ASC, currentPrice DESC, lastPriceUpdate desc";
                // string query = "* FROM Product WHERE  flag = 'true' AND DiscountPrice IS NOT NULL";
                string query = "* FROM Product WHERE  flag = 'true' AND DATEDIFF(d, lastPriceUpdate, GETDATE()) <= 14";

                // Get count
                remainCount = int.Parse(DB.getValue("SELECT COUNT(ID) " + query.Substring(2))) - (pageIndex * pageSize + pageSize);
                if (remainCount < 0)
                    remainCount = 0;

                object[,] paramss;
                query = DB.wrapInPager(query, orderExp, pageIndex, pageSize, out paramss);
                return DB.getData(query, paramss);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getDiscountProducts"));
                return null;
            }
        }
        public static DataTable getRandomDiscountProducts(int number = -1)
        {
            //get
            try
            {
                return DB.getData("select" + (number <= 0 ? "" : " top " + number) + " * from Product where  flag = 'true' and DiscountPrice is not null order by NEWID();");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getDiscountProducts"));
                return null;
            }
        }

        public static int getDiscountProductCount(int groupID = -1)
        {
            //get
            try
            {
                return int.Parse(DB.getValue("SELECT COUNT(id) FROM Product WHERE flag = 'true'"
                    + (groupID <= 0 ? "" : " AND groupID = " + groupID)
                    + " AND DiscountPrice IS NOT null;"));
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getDiscountProducts"));
                return 0;
            }
        }

        internal static DataTable getProducts(string groupId, string categoryId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId }, { "@categoryId", categoryId } };
                return DB.getData("select * from Product where categoryId = @categoryId and groupId = @groupId;", parameters);
                
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProducts"));
                return null;
            }
        }

        internal static bool updateProduct(Product product)
        {
            try
            {
                List<object[]> parameters = new List<object[]>(){
                                            new object[]{ "@Id", product.id },
                                            new object[]{ "@name", product.name },
                                            new object[]{ "@shortDesc", product.shortDesc},
                                            new object[]{ "@longDesc", product.longDesc},
                                            new object[]{ "@include", product.include},
                                            new object[]{ "@remark", product.remark},
                                            new object[]{ "@outstanding", product.outstanding},
                                            new object[]{ "@viewcount", product.viewCount},
                                            new object[]{ "@rate", product.rate},
                                            new object[]{ "@categoryId", product.categoryId},
                                            new object[]{ "@isFeatured", product.isFeatured},
                                            new object[]{ "@isSpeedySold", product.isSpeedySold},
                                            new object[]{ "@isCompanyProduct", product.isCompanyProduct},
                                            new object[]{ "@isGifted", product.isGifted},
                                            new object[]{ "@isOutOfStock", product.isOutOfStock},
                                            new object[]{ "@isComingSoon", product.isComingSoon},
                                            new object[]{ "@currentPrice", product.currentPrice},
                                            new object[]{ "@osId", product.osId},
                                            new object[]{ "@groupId", product.groupId},
                                            new object[]{ "@video", product.video},
                                            new object[]{ "@discountDesc", product.discountDesc},
                                            new object[]{ "@hotDesc", product.hotDesc},
                                            new object[]{ "@flag", product.flag},
                                            new object[]{ "@firstImageDesc", product.firstImageDesc},
                                            new object[]{ "@secondImageDesc", product.secondImageDesc},
                                            new object[]{ "@thirdImageDesc", product.thirdImageDesc},
                                            new object[]{ "@fourthImageDesc", product.fourthImageDesc},
                                            new object[]{ "@fifthImageDesc", product.fifthImageDesc},
                                            new object[]{ "@useDarkBackground", product.useDarkBackground},
                                        };

                bool noDiscountPrice = product.discountPrice <= 0;

                if (!noDiscountPrice)
                    parameters.Add(new object[] { "@discountPrice", product.discountPrice });

                // update lastPriceUpdate
                string priceUpdate = "";
                if (product.lastPriceUpdate != null)
                {
                    parameters.Add(new object[] { "@lastPriceUpdate", product.lastPriceUpdate });
                    priceUpdate = ", lastPriceUpdate = @lastPriceUpdate";
                }

                return DB.exec("update PRODUCT set name = @name, shortDesc = @shortDesc, longDesc = @longDesc, include = @include, remark = @remark, outstanding = @outstanding, viewcount = @viewcount, " +
                    "rate = @rate, categoryId = @categoryId, isFeatured= @isFeatured, isSpeedySold= @isSpeedySold, isGifted = @isGifted, isComingSoon = @isComingSoon, isOutOfStock = @isOutOfStock, " +
                    "isCompanyProduct = @isCompanyProduct, currentPrice = @currentPrice, " + (noDiscountPrice ? "discountPrice = NULL, " : "discountPrice = @discountPrice, ") + "osId = @osId,  " +
                    "video = @video, discountDesc = @discountDesc, hotDesc = @hotDesc, flag = @flag, " + 
                    "firstImageDesc = @firstImageDesc, secondImageDesc = @secondImageDesc, thirdImageDesc = @thirdImageDesc, " +
                    // suggestedAccessories = @suggestedAccessories, , backgroundImage = @backgroundImage
                    "fourthImageDesc = @fourthImageDesc, fifthImageDesc = @fifthImageDesc, useDarkBackground = @useDarkBackground " + priceUpdate +
                    " where Id = @Id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateProduct"));
                return false;
            }
        }

        internal static bool updateProductAccessories(int productID, string accessories)
        {
            try
            {
                object[,] parameters = { 
                                       {"@ID", productID }, 
                                       {"@suggestedAccessories", accessories }, 
                                       };
                return DB.exec("UPDATE PRODUCT SET suggestedAccessories = @suggestedAccessories WHERE ID = @Id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateProduct"));
                return false;
            }
        }

        internal static bool createProduct(Product product)
        {
            try
            {
                List<object[]> parameters = new List<object[]>(){
                                            new object[]{ "@name", product.name },
                                            new object[]{ "@shortDesc", product.shortDesc},
                                            new object[]{ "@longDesc", product.longDesc},
                                            new object[]{ "@include", product.include},
                                            new object[]{ "@remark", product.remark},
                                            new object[]{ "@outstanding", product.outstanding},
                                            new object[]{ "@viewcount", product.viewCount},
                                            new object[]{ "@rate", product.rate},
                                            new object[]{ "@categoryId", product.categoryId},
                                            new object[]{ "@isFeatured", product.isFeatured},
                                            new object[]{ "@isSpeedySold", product.isSpeedySold},
                                            new object[]{ "@isCompanyProduct", product.isCompanyProduct},
                                            new object[]{ "@isGifted", product.isGifted},
                                            new object[]{ "@isOutOfStock", product.isOutOfStock},
                                            new object[]{ "@isComingSoon", product.isComingSoon},
                                            new object[]{ "@currentPrice", product.currentPrice},
                                            new object[]{ "@osId", product.osId},
                                            new object[]{ "@groupId", product.groupId},
                                            new object[]{ "@video", product.video},
                                            new object[]{ "@discountDesc", product.discountDesc},
                                            new object[]{ "@hotDesc", product.hotDesc},
                                            new object[]{ "@flag", product.flag},
                                            new object[]{ "@firstImageDesc", product.firstImageDesc},
                                            new object[]{ "@secondImageDesc", product.secondImageDesc},
                                            new object[]{ "@thirdImageDesc", product.thirdImageDesc},
                                            new object[]{ "@fourthImageDesc", product.fourthImageDesc},
                                            new object[]{ "@fifthImageDesc", product.fifthImageDesc},
                                        };
                
                bool noDiscountPrice = product.discountPrice <= 0;

                if (!noDiscountPrice)
                    parameters.Add(new object[] { "@discountPrice", product.discountPrice });

                return DB.exec("insert into PRODUCT (flag, name, shortDesc, longDesc, include, remark, outstanding, viewcount, rate, categoryId, isFeatured, isSpeedySold, isCompanyProduct, isGifted, isOutOfStock, isComingSoon, currentPrice, " + (noDiscountPrice ? "discountPrice, " : "discountPrice, ") + "osId, groupId, video, discountDesc, hotDesc, firstImageDesc, secondImageDesc, thirdImageDesc, fourthImageDesc, fifthImageDesc) " +
                    "values (@flag, @name, @shortDesc, @longDesc, @include, @remark, @outstanding, @viewcount, @rate, @categoryId, @isFeatured, @isSpeedySold, @isCompanyProduct, @isGifted, @isOutOfStock, @isComingSoon, @currentPrice, " + (noDiscountPrice ? "NULL, " : "@discountPrice, ") + "@osId, @groupId, @video, @discountDesc, @hotDesc, @firstImageDesc, @secondImageDesc, @thirdImageDesc, @fourthImageDesc, @fifthImageDesc);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createProduct"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('PRODUCT')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentProductId"));
                return "";
            }
        }

        internal static bool deleteProduct(string gvIDs)
        {
            try
            {
                return DB.exec("delete from PRODUCT where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteProduct"));
                return false;
            }
        }

        internal static DataTable getProductsByName(string productName)
        {
            try
            {
                object[,] parameter = { { "@name", "%" + productName + "%" } };
                return DB.getData("select top 10 * from PRODUCT where  flag = 'true' and name like @name;", parameter);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getProductsByName"));
                return null;
            }
        }

        internal static DataTable searchProductsByName(string name)
        {
            //get
            try
            {
                object[,] parameters = { { "@name", "%" + name + "%" } };
                return DB.getData("select top 100 * from PRODUCT where  flag = 'true' and name like @name order by id desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error searchProductsByName"));
                return null;
            }
        }

        internal static DataTable searchHighToLowPriceProductsByName(string name)
        {
            //get
            try
            {
                object[,] parameters = { { "@name", "%" + name + "%" } };
                return DB.getData("select top 100 *, ISNULL(DiscountPrice,CurrentPrice) as Price from PRODUCT where  flag = 'true' and name like @name order by Price desc;", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error searchHighToLowPriceProductsByName"));
                return null;
            }
        }

        internal static DataTable searchLowToHighPriceProductsByName(string name)
        {
            //get
            try
            {
                object[,] parameters = { { "@name", "%" + name + "%" } };
                return DB.getData("select top 100 *, ISNULL(DiscountPrice,CurrentPrice) as Price from PRODUCT where  flag = 'true' and name like @name order by Price asc;", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error searchLowToHighPriceProductsByName"));
                return null;
            }
        }

        internal static DataTable searchHighViewProductsByName(string name)
        {
            //get
            try
            {
                object[,] parameters = { { "@name", "%" + name + "%" } };
                return DB.getData("select top 100 * from PRODUCT where  flag = 'true' and name like @name order by viewcount desc;", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error searchHighViewProductsByName"));
                return null;
            }
        }

        internal static DataTable getSamePriceProducts(int number, string id, int interval)
        {
            //get
            try
            {
                string categoryId = DB.getValue("select categoryId from Product where  flag = 'true' and Id = " + id);
                if (categoryId != "")
                {
                    string groupId = DB.getValue("select groupId from category where id = " + categoryId);

                    string sPrice = "";
                    sPrice = DB.getValue("select ISNULL(DiscountPrice,CurrentPrice) as Price from PRODUCT where  flag = 'true' and id = " + id);
                    if (groupId != "" && sPrice != "")
                    {
                        object[,] parameters = { { "@exceptId", id },
                                               { "@groupId", groupId},
                                               { "@maxPrice", long.Parse(sPrice) + interval},
                                               { "@minPrice", long.Parse(sPrice) - interval < 0 ? 0 :  long.Parse(sPrice) - interval} };

                        DataTable dt = DB.getData("select top " + number + " * from PRODUCT where  flag = 'true' and id != @exceptId and " +
                            "CurrentPrice >= @minPrice and CurrentPrice <= @maxPrice and " + 
                            "CategoryId in (select Id from CATEGORY where GroupId = @groupId) order by NEWID();", parameters);
                        return dt;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getOtherProducts"));
                return null;
            }
        }

        internal static bool resetWeeklyCount(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                return DB.exec("update product set weeklycount = 1, isWeeklyCountUpdated = 'True' where id = id;", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error resetWeeklyCount"));
                return false;
            }
        }

        internal static bool addWeeklyViewCount(string id)
        {
            try
            {
                object[,] parameters = {   {"@Id", id}
                                    };
                return DB.exec("update Product set WeeklyCount = WeeklyCount + 1 where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addWeeklyViewCount"));
                return false;
            }
        }

        internal static bool add_resetWeeklyViewCount(string id)
        {
            try
            {
                object[,] parameters = {   {"@Id", id}
                                    };
                return DB.exec("update Product set WeeklyCount = WeeklyCount + 1, isWeeklyCountUpdated = 'False'  where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error add_resetWeeklyViewCount"));
                return false;
            }
        }

        internal static DataTable getPrdByWeeklyCountDesc(int number, string groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select top " + number + " * from Product where  flag = 'true' and groupId = @groupId order by WeeklyCount desc;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getPrdByWeeklyCountDesc"));
                return null;
            }
        }

        public static string getProductLink(Product product)
        {
            return Common.m() + (product.oldProduct ? "spc-" : "sp-") + 
                HttpUtility.UrlEncode(product.name.NormalizeDiacriticalCharacters().ReplaceNonAlphaNumSpace().ToLower())
                + "-" + product.id;
        }

        public static Product rowToSimpleProduct(DataRow row, bool oldProduct = false)
        {
            Product product = new Product
            {
                id = (int)row["Id"],
                name = row["Name"] as string,
                currentPrice = row["CurrentPrice"] as int? ?? 0,
                oldProduct = oldProduct,
            };

            CultureInfo cultureInfo = new CultureInfo("vi-VN");
            //cultureInfo.NumberFormat.CurrencyDecimalDigits = 0;
            product.currentPriceStr = product.currentPrice.ToString("C0", cultureInfo);

            // No folder part
            if (oldProduct)
            {
                product.firstImage = row["DisplayImage"] as string;
            }
            else
            {
                // TODO_: product image
                product.firstImage = row["FirstImage"] as string;
                product.firstImageDesc = row["firstImageDesc"] as string;
                //product.firstImage = "668_LUMIA 630 3.jpeg";

                // Name only, no path
                product.link = getProductLink(product);
            }

            return product;
        }

        public static Product rowToProduct(DataRow row, bool oldProduct = false)
        {
            Product product = rowToSimpleProduct(row, oldProduct);

            if (oldProduct)
            {
                product.shortDesc = row["Description"].ToString();
                product.discountPrice = row["OriginalPrice"] is DBNull ? 0 : (int)row["OriginalPrice"];
            }
            else
            {
                product.secondImage = row["secondImage"] as string;
                product.thirdImage = row["thirdImage"] as string;
                product.fourthImage = row["fourthImage"] as string;
                product.fifthImage = row["fifthImage"] as string;
                product.secondImageDesc = row["secondImageDesc"] as string;
                product.thirdImageDesc = row["thirdImageDesc"] as string;
                product.fourthImageDesc = row["fourthImageDesc"] as string;
                product.fifthImageDesc = row["fifthImageDesc"] as string;
                product.shortDesc = row["ShortDesc"] as string;
                product.longDesc = row["longDesc"] as string;
                product.include = row["include"] as string;
                product.remark = row["remark"] as string;
                product.outstanding = row["outstanding"] as string;
                product.discountDesc = row["discountDesc"] as string;
                product.hotDesc = row["hotDesc"] as string;
                product.isComingSoon = row["isComingSoon"] as bool? ?? false;
                product.isOutOfStock = row["isOutOfStock"] as bool? ?? false;
                product.isGifted = row["isGifted"] as bool? ?? false;
                product.isSpeedySold = row["IsSpeedySold"] as bool? ?? false;
                product.discountPrice = row["DiscountPrice"] as int? ?? 0;
                product.groupId = row["GroupId"] as int? ?? 0;
                product.video = row["Video"] as string;
                product.categoryId = row["categoryId"] as int? ?? 0;
                product.suggestedAccessories = row["suggestedAccessories"] as string ?? "";
                product.useDarkBackground = row["useDarkBackground"] as bool? ?? false;
                product.backgroundImage = row["backgroundImage"] as string ?? "";
                product.lastPriceUpdate = row["lastPriceUpdate"] as DateTime?;
            }

            CultureInfo cultureInfo = new CultureInfo("vi-VN");
            //cultureInfo.NumberFormat.CurrencyDecimalDigits = 0;
            product.discountPriceStr = product.discountPrice.ToString("C0", cultureInfo);

            // For new product and if there is a discount price, swap discount price and current price
            if (!oldProduct)
            {
                if(product.discountPrice != 0)
                {
                    var tmp = product.discountPriceStr;
                    product.discountPriceStr = product.currentPriceStr;
                    product.currentPriceStr = tmp;
                }
            }

            return product;
        }
    }
}