﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class FaqController
    {
        public static DataTable getFaqs()
        {
            //get
            try
            {
                return DB.getData("select * from FAQ");

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getFaqs"));
                return null;
            }
        }

        internal static bool deleteFaq(string gvIDs)
        {
            try
            {
                return DB.exec("delete from FAQ where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteFaq"));
                return false;
            }
        }

        internal static bool updateFaq(FAQ faq)
        {
            try
            {
                object[,] parameters = {   {"@Id", faq.id},
                                           {"@Ask", faq.ask}, 
                                           {"@Answer", faq.answer}
                                    };
                return DB.exec("update FAQ set Ask = @Ask, Answer = @Answer where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateFaq"));
                return false;
            }
        }

        internal static bool createFaq(FAQ faq)
        {
            try
            {
                object[,] parameters = {  
                                           {"@Ask", faq.ask}, 
                                           {"@Answer", faq.answer}
                                    };
                return DB.exec("insert into FAQ (Ask, Answer) values (@Ask, @Answer);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createFaq"));
                return false;
            }
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('FAQ')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentFAQId"));
                return "";
            }
        }
    }
}