﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class SupportController
    {
        internal static DataTable getSupportByGroupId(string groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select * from SUPPORT where groupId=@groupId;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getSupportByGroupId"));
                return null;
            }
        }

        internal static bool deleteSupport(string gvIDs)
        {
            try
            {
                return DB.exec("delete from SUPPORT where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteSupport"));
                return false;
            }
        }

        internal static bool updateSupport(Support support)
        {
            try
            {
                object[,] parameters =  {   { "@name",support.name },
                                    { "@yahooMessenger", support.yahooMessenger },
                                    { "@skype", support.skype },
                                    { "@phone", support.phone },
                                    { "@groupId", support.groupId },
                                    { "@id", support.id }
                                };
                return DB.exec("update Support set name = @name, yahooMessenger = @yahooMessenger, skype = @skype, phone = @phone, groupId = @groupId where Id = @Id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateSupport"));
                return false;
            }
        }

        internal static bool createSupport(Support support)
        {
            try
            {
                object[,] parameters =  {   { "@name",support.name },
                                    { "@yahooMessenger", support.yahooMessenger },
                                    { "@skype", support.skype },
                                    { "@phone", support.phone },
                                    { "@groupId", support.groupId }
                                };
                return DB.exec("insert into Support (name, yahooMessenger, skype, phone, groupId) values (@name, @yahooMessenger, @skype, @phone, @groupId);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createSupport"));
                return false;
            }
        }
    }
}