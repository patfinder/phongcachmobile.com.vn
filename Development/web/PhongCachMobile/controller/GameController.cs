﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class GameController
    {
        public static DataTable getRandomGames(int categoryId, int number)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select top " + number + " * from GAME where CategoryId = @categoryId order by NEWID();", parameters);
                }
                else
                {
                    return DB.getData("select top " + number + " * from GAME order by NEWID()");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getGamesByCategory"));
                return null;
            }
        }

        public static DataTable getGamesByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from GAME where CategoryId = @categoryId order by posteddate desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from GAME order by posteddate desc");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getGamesByCategory"));
                return null;
            }
        }

        public static DataTable getViewCountGamesByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from GAME where CategoryId = @categoryId order by viewcount desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from GAME order by viewcount desc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getViewCountGamesByCategory"));
                return null;
            }
        }

        public static DataTable getDownloadCountGamesByCategory(int categoryId)
        {
            //get
            try
            {
                if (categoryId != -1)
                {
                    object[,] parameters = { { "@categoryId", categoryId } };
                    return DB.getData("select * from GAME where CategoryId = @categoryId order by downloadcount desc;", parameters);
                }
                else
                {
                    return DB.getData("select * from GAME order by downloadcount desc;");
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getDownloadCountGamesByCategory"));
                return null;
            }
        }

        internal static DataTable getGames(string categoryId, string osId)
        {
            //get
            try
            {
                object[,] parameters = { {"@categoryId", categoryId },
                                         {"@osId", osId }};
                return DB.getData("select * from GAME where CategoryId = @categoryId and OsId = @osId", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getGames"));
                return null;
            }
        }

        internal static bool updateGame(Game game)
        {
            return ApplicationController.updateApp(game);
        }

        internal static bool createGame(Game game)
        {
            return ApplicationController.createApp(game);
        }

        internal static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('GAME')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentGameId"));
                return "";
            }
        }

        internal static bool deleteGame(string gvIDs)
        {
            try
            {
                return DB.exec("delete from GAME where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteGame"));
                return false;
            }
        }

        internal static bool addViewCount(string id)
        {
            try
            {
                return DB.exec("update game set ViewCount = ViewCount+1 where id = " + id);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addViewCountGame"));
                return false;
            }
        }
    }
}