﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class AskController
    {
        internal static DataTable getAskOrderByPostedDate()
        {
            //get
            try
            {
                return DB.getData("select ask.Id, ask.ask, ask.Title, ask.UserId, ASK.PostedDate, ask.ViewCount, ask.isSolved, max(answer.PostedDate) " + 
                    "From ask left join answer on ASK.Id = answer.AskId " +
                    "group by ask.Id, ask.ask, ask.Title, ask.UserId, ASK.PostedDate, ask.ViewCount, ask.isSolved " + 
                    "order by coalesce(max(answer.PostedDate), ask.postedDate) desc; ");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAskOrderByPostedDate"));
                return null;
            }
        }

        internal static string getNumberAsk()
        {
            //get
            try
            {
                return DB.getValue("select count(*) from ASK");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberAsk"));
                return null;
            }
        }

        internal static string getNumberUser()
        {
            //get
            try
            {
                return DB.getValue("select count(*) from (SELECT userid FROM ASK UNION SELECT userid FROM ANSWER) sub;");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getNumberUser"));
                return null;
            }
        }

        internal static DataTable getAskOrderByViewCount()
        {
            //get
            try
            {
                return DB.getData("select ask.Id, ask.ask, ask.Title, ask.UserId, ASK.PostedDate, ask.ViewCount, ask.isSolved " +
                    "From ask left join answer on ASK.Id = answer.AskId " +
                    "group by ask.Id, ask.ask, ask.Title, ask.UserId, ASK.PostedDate, ask.ViewCount, ask.isSolved " +
                    "order by viewcount desc; ");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAskOrderByViewCount"));
                return null;
            }
        }

        internal static DataTable getAskOrderByAnswerCount()
        {
            //get
            try
            {
                return DB.getData("select ask.Id, ask.ask, ask.Title, ask.UserId, ASK.PostedDate, ask.ViewCount, ask.isSolved, COUNT(ANSWER.AskId) " +
                    "From ask left join answer on ASK.Id = answer.AskId " +
                    "group by ask.Id, ask.ask, ask.Title, ask.UserId, ASK.PostedDate, ask.ViewCount, ask.isSolved " + 
                    "order by COUNT(ANSWER.AskId) desc;  ");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAskOrderByAnswerCount"));
                return null;
            }
        }

        internal static bool addViewCount(string id)
        {
            try
            {
                object[,] parameters = {   {"@Id", id} };
                return DB.exec("update Ask set ViewCount = ViewCount + 1 where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error addViewCount"));
                return false;
            }
        }

        internal static DataTable getAskById(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                DataTable dt = DB.getData("select * from ASK where id = @id", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getAskById"));
                return null;
            }
        }

        internal static bool solve(string id)
        {
            //get
            try
            {
                object[,] parameters = { { "@id", id } };
                DB.getData("update ASK set isSolved = 'True' where id = @id", parameters);
                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error solve"));
                return false;
            }
        }

        internal static bool createAsk(Ask ask)
        {
            //get
            try
            {
                object[,] parameters = { { "@ask", ask.ask},
                                    { "@userId", ask.userId  },
                                    { "@postedDate", ask.postedDate }, 
                                    { "@viewCount", ask.viewCount }, 
                                    { "@title", ask.title }, 
                                    { "@isSolved", ask.isSolved }};
                return DB.exec("insert into Ask(ask, userId, postedDate, viewCount, title, isSolved) " +
                    "values (@ask, @userId, @postedDate, @viewCount, @title, @isSolved);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createAsk"));
                return false;
            }
        }
    }
}