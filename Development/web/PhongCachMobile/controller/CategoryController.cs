﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile.controller
{
    public class CategoryController
    {
        public static DataTable getCategoryById(string id)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                object[,] parameters = { { "@id", id } };
                dt = DB.getData("select * from CATEGORY where id = @id;", parameters);
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCategoryById"));
                return null;
            }
        }

        public static DataTable getCategories(string groupId, string parentId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (parentId == null || parentId == "")
                {
                    object[,] parameters =  {  { "@groupId", groupId } };
                    dt = groupId != "-1" ? 
                        DB.getData("select Id, CAST(ParentId AS varchar(10)) as ParentId, GroupId, Name, Description, DisplayImage, Display, InOrder from CATEGORY where ParentId is null and GroupId = @groupId order by InOrder;", parameters) :
                        DB.getData("select Id, CAST(ParentId AS varchar(10)) as ParentId, GroupId, Name, Description, DisplayImage, Display, InOrder from CATEGORY where ParentId is null order by InOrder;");
                }
                else
                {
                    object[,] parameters =  {{ "@parentId", parentId }, { "@groupId", groupId }, };
                    dt = DB.getData("select Id, CAST(ParentId AS varchar(10)) as ParentId, GroupId, Name, Description, DisplayImage, Display, InOrder from CATEGORY where ParentId = @parentId and GroupId = @groupId order by InOrder;", parameters);
                }
                return dt;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCategories"));
                return null;
            }
        }

        public static string getCategoryNameById(string id)
        {
            //get
            try
            {
                object[,] parameters =  {{ "@id", id }
                                        };
                return DB.getValue("select Name from CATEGORY where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCategoryNameById"));
                return "";
            }
        }

        public static DataTable getSameBrandCategories(int categoryID)
        {
            //get
            try
            {
                return DB.getData("select C1.Id from [CATEGORY] C1 INNER JOIN [CATEGORY] C2 " +
                    "ON C1.Name = C2.Name AND C2.Id = @categoryID", new object[,] { { "@categoryID", categoryID } });
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCategoryNameById"));
                return null;
            }
        }

        public static string getCategoryParentIdById(string id)
        {
            //get
            try
            {
                object[,] parameters =  {{ "@id", id }
                                        };
                return DB.getValue("select ParentId from CATEGORY where id = @id", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCategoryParentIdById"));
                return "";
            }
        }

        public static bool deleteCategory(string gvIDs)
        {
            try
            {
                return DB.exec("delete from CATEGORY where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteCategory"));
                return false;
            }
        }

        public static bool updateCategory(Category category)
        {
            try
            {
                if (category.inOrder <= -1)
                {
                    if (category.parentId == -1)
                    {
                        object[,] parameters =  {   { "@ParentId", DBNull.Value},
                                            { "@Name", category.name },
                                            { "@Description", category.description },
                                            { "@Display", category.display },
                                            { "@GroupId", category.groupId },
                                            { "@DisplayImage", category.displayImg },
                                            { "@Id", category.id }
                                        };
                        return DB.exec("update CATEGORY set ParentId = @ParentId, Name = @Name, Description = @Description, Display = @Display, " +
                            "GroupId = @GroupId, DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }
                    else
                    {
                        object[,] parameters =  {   { "@ParentId", category.parentId},
                                            { "@Name", category.name },
                                            { "@Description", category.description },
                                            { "@Display", category.display },
                                            { "@GroupId", category.groupId },
                                            { "@DisplayImage", category.displayImg },
                                            { "@Id", category.id }
                                        };
                        return DB.exec("update CATEGORY set ParentId = @ParentId, Name = @Name, Description = @Description, Display = @Display, " +
                            "GroupId = @GroupId, DisplayImage = @DisplayImage where Id = @Id", parameters);
                    }

                }
                else
                {
                    object[,] parameters =  {  { "@InOrder", category.inOrder },
                                            { "@Id", category.id }
                                        };
                    return DB.exec("update CATEGORY set InOrder = @InOrder where Id = @Id", parameters);
                }

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateCategory"));
                return false;
            }
        }

        public static bool createCategory(Category category)
        {
            try
            {
                if (category.parentId == -1)
                {
                    object[,] parameters =  {   { "@ParentId",DBNull.Value },
                                        { "@Name", category.name },
                                        { "@Description", category.description },
                                        { "@DisplayImage", category.displayImg },
                                        { "@Display", category.display },
                                        { "@GroupId", category.groupId },
                                        { "@InOrder", category.inOrder }
                                    };
                    return DB.exec("insert into CATEGORY (ParentId, Name, Description, DisplayImage, Display, GroupId, InOrder) values " +
                        "(@ParentId, @Name, @Description, @DisplayImage, @Display, @GroupId, @InOrder);", parameters);
                }
                else
                {
                    object[,] parameters =  {   { "@ParentId", category.parentId},
                                        { "@Name", category.name },
                                        { "@Description", category.description },
                                        { "@DisplayImage", category.displayImg },
                                        { "@Display", category.display },
                                        { "@GroupId", category.groupId },
                                        { "@InOrder", category.inOrder }
                                    };
                    return DB.exec("insert into CATEGORY (ParentId, Name, Description, DisplayImage, Display, GroupId, InOrder) values " +
                       "(@ParentId, @Name, @Description, @DisplayImage, @Display, @GroupId, @InOrder);", parameters);
                }
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createCategory"));
                return false;
            }
        }

        internal static bool updateOrderCategory(string categoryId, int inOrder)
        {
            try
            {
                object[,] parameters =  {{ "@InOrder", inOrder },
                                         { "@Id", categoryId }
                                        };
                return DB.exec("update CATEGORY set InOrder = @InOrder where Id = @Id", parameters);

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateOrderCategory"));
                return false;
            }
        }
    }
}