﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class SliderController
    {
        public static DataTable getSliders(int number, string groupId)
        {
            //get
            try
            {
                DataTable dt = new DataTable();
                if (number > 0)
                {
                    object[,] parameters = { { "@groupId", groupId } };
                    dt = DB.getData("select TOP " + number + " * from SLIDER where groupId = @groupId order by NEWID(), PostedDate desc;", parameters);
                    return dt;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getSliders"));
                return null;
            }
        }

        public static DataTable getSliders(string groupId)
        {
            //get
            try
            {
                object[,] parameters = { { "@groupId", groupId } };
                return DB.getData("select Slider.*, [GROUP].Name " +
                    "from slider join [GROUP] on SLIDER.GroupId = [GROUP].Id " +
                    "where Slider.groupId = @groupId;", parameters);


            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getSliders"));
                return null;
            }
        }

        public static bool deleteSlider(string gvIDs)
        {
            try
            {
                return DB.exec("delete from Slider where id in " + gvIDs + ";");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error deleteSlider"));
                return false;
            }
        }

        public static bool updateSlider(Slider slider)
        {
            try
            {
                object[,] parameters = {   {"@Id", slider.id},
                                           {"@Text", slider.text}, 
                                           {"@ActionUrl", slider.actionUrl},
                                           {"@PostedDate", slider.postedDate},
                                           {"@Flag", slider.flag},
                                           {"@GroupId", slider.groupId},
                                           {"@Title", slider.title}
                                    };
                return DB.exec("update SLIDER set Text = @Text, ActionUrl = @ActionUrl, PostedDate = @PostedDate, Title = @Title where Id = @Id;", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error updateSlider"));
                return false;
            }
        }

        public static bool createSlider(Slider slider)
        {
            try
            {
                object[,] parameters = {   {"@Text", slider.text}, 
                                           {"@ActionUrl", slider.actionUrl},
                                           {"@PostedDate", slider.postedDate},
                                           {"@Flag", slider.flag},
                                           {"@GroupId", slider.groupId},
                                           {"@Title", slider.title}
                                    };
                return DB.exec("insert into Slider (Text, ActionUrl, PostedDate, Flag, GroupId, Title) values (@Text, @ActionUrl, @PostedDate, @Flag, @GroupId, @Title);", parameters);
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error createSlider"));
                return false;
            }
        }

        public static string getCurrentId()
        {
            try
            {
                return DB.getValue("select IDENT_CURRENT('SLIDER')");
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getCurrentSliderId"));
                return "";
            }
        }

        public static Slider rowToSlider(DataRow dr)
        {
            return new Slider
            {
                id = (int)dr["id"],
                displayImg = dr["DisplayImage"] as string,
                thumbImg = dr["ThumbImage"] as string,
                text = dr["text"] as string,
                actionUrl = dr["actionUrl"].ToString(),
                postedDate = dr["postedDate"] as string,
                flag = (bool)dr["flag"],
                groupId = (int)dr["groupId"],
                title = dr["title"] as string,
            };
        }

    }
}