﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;

namespace PhongCachMobile.controller
{
    public class EmotionController
    {
        public static DataTable getEmotions(int number)
        {
            //get
            try
            {
                DataTable dt = null;
                if (number > 0)
                    dt = DB.getData("select top " + number + " * from EMOTION;");
                else
                    dt = DB.getData("select * from EMOTION;");
                return dt;

            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error getEmotions"));
                return null;
            }
        }
    }
}