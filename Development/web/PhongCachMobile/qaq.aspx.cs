﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.Text.RegularExpressions;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class qaq : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadAsk();
                loadEmotion();
            }
            checkVisibleSolvedPanel();
        }

        private void checkVisibleSolvedPanel()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);

                DataTable dt = AskController.getAskById(id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (Session["Email"] != null && Session["Email"].ToString() != "")
                    {
                        DataTable dtUser = SysUserController.getUserById(Session["UserId"].ToString());
                        if (Session["UserId"].ToString() == dt.Rows[0]["UserId"].ToString() || dtUser.Rows[0]["GroupId"].ToString() == "32")
                        {
                            if (!bool.Parse(dt.Rows[0]["isSolved"].ToString()))
                            {
                                divSolved.Visible = true;
                               
                            }

                        }
                    }

                    if (!bool.Parse(dt.Rows[0]["isSolved"].ToString()))
                    {
                        spanSolved.Visible = false;
                    }
                }
            }
        }

        private void loadLang()
        {
            this.litAnswer.Text = LangController.getLng("litAnswer.Text", "Trả lời câu hỏi");
            string imagePath = SysIniController.getSetting("Avatar_ImagePath", "images/avatar/");
            this.imgAvatar.ImageUrl = imagePath + new Random().Next(1, 11) + ".png";
            this.lbtSend.Text = "<span><span>" + LangController.getLng("litSend.Text", "Gửi") + "</span></span>";
            if (Session["Answer"] != null && Session["Answer"].ToString() != "")
                txtAnswer.Text = Session["Answer"].ToString();
            this.rgeAnswerLength.ErrorMessage = LangController.getLng("litCommentLength", "Độ dài từ 20 - 1000 kí tự.");
            this.reqValAnswer.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
        }

        private void loadAsk()
        {
            string title = "";
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);
                // update counter
                if (Session["CurrentAskId"] == null || Session["CurrentAskId"].ToString() != id)
                {
                    AskController.addViewCount(id);
                    Session["CurrentAskId"] = id;
                }

                DataTable dt = AskController.getAskById(id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    litTitle.Text = title = dt.Rows[0]["Title"].ToString();
                    this.litDate.Text = Common.strToDateTime(dt.Rows[0]["PostedDate"].ToString()).ToLongDateString();
                    this.litViewCount.Text = int.Parse(dt.Rows[0]["ViewCount"].ToString()).ToString("#,##0");
                    this.litAnswerCount.Text = AnswerController.getNumberOfAnswerByAskId(dt.Rows[0]["Id"].ToString());
                    this.litContent.Text = dt.Rows[0]["Ask"].ToString();
                    string email =  SysUserController.getEmailById(dt.Rows[0]["UserId"].ToString());
                    litEmailName.Text = email.Substring(0, email.IndexOf('@'));
                }
            }

            this.Title = title;
        }

        protected void lbtSolved_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);
                // update counter

                bool result = AskController.solve(id);
                if (result)
                {
                    divSolved.Visible = false;
                    spanSolved.Visible = true;
                }

            }
        }


        private void loadAnswer(string id)
        {
            string answer = LangController.getLng("litAnswer.Text", "Trả lời câu hỏi");
            this.litTotalAnswer.Text = AnswerController.getNumberOfAnswerByAskId(id) + " " + answer.ToLower();

            Dictionary<string, string> dictEmotion = getEmotionDictionary();

            DataTable dt = AnswerController.getAnswersByAskId(id, "");
            if (dt != null && dt.Rows.Count > 0)
            {
                string avatarImagePath = SysIniController.getSetting("Avatar_ImagePath", "images/avatar/");
                dt.Columns.Add("Alias", typeof(string));
                dt.Columns.Add("Avatar", typeof(string));
                dt.Columns.Add("Index", typeof(int));
                dt.Columns.Add("ChildAnswer", typeof(string));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["Description"] = replaceEmotion(dt.Rows[i]["Description"].ToString(), dictEmotion);
                    DataTable dtUser = SysUserController.getUserById(dt.Rows[i]["UserId"].ToString());
                    if (dtUser.Rows[0]["Alias"] != null && dtUser.Rows[0]["Alias"].ToString() != "")
                        dt.Rows[i]["Alias"] = dtUser.Rows[0]["Alias"].ToString();
                    else
                        dt.Rows[i]["Alias"] = dtUser.Rows[0]["Email"].ToString().Substring(0, dtUser.Rows[0]["Email"].ToString().IndexOf('@'));

                    dt.Rows[i]["PostedDate"] = Common.strToDateTime(dt.Rows[i]["PostedDate"].ToString()).ToShortDateString();
                    dt.Rows[i]["Avatar"] = avatarImagePath + dtUser.Rows[0]["Avatar"].ToString();
                    dt.Rows[i]["Index"] = dt.Rows.Count - i;

                    DataTable dtChild = AnswerController.getAnswersByAskIdAsc(id, dt.Rows[i]["Id"].ToString());
                    if (dtChild != null && dtChild.Rows.Count > 0)
                    {
                        dt.Rows[i]["ChildAnswer"] = "";
                        for (int j = 0; j < dtChild.Rows.Count; j++)
                        {
                            DataTable dtChildUser = SysUserController.getUserById(dtChild.Rows[j]["UserId"].ToString());
                            string alias = "";
                            if (dtChildUser.Rows[0]["Alias"] != null && dtChildUser.Rows[0]["Alias"].ToString() != "")
                                alias = dtChildUser.Rows[0]["Alias"].ToString();
                            else
                                alias = dtChildUser.Rows[0]["Email"].ToString().Substring(0, dtChildUser.Rows[0]["Email"].ToString().IndexOf('@'));

                            dt.Rows[i]["ChildAnswer"] += "<div class='sub comment'>" +
                                "<figure class='page1-img2'>" +
                                    "<img width='25px' src='" + avatarImagePath + dtChildUser.Rows[0]["Avatar"].ToString() + "'/>" +
                                "</figure>" +
                                "<div class='smheight extra-wrap'>" +
                                    "<span>" +
                                        "<a class='link6'>" + alias + "</a> -" + Common.strToDateTime(dtChild.Rows[j]["PostedDate"].ToString()).ToShortDateString() +
                                    "</span>" +
                                    "<span class='fright'>#" + (dtChild.Rows.Count - j) + "</span>  <a onclick=\"return replyAnswer(" + dt.Rows[i]["Id"].ToString() + ",'" + alias +
                                        "');\" href='#_answer' class='fright link8'>Trả lời</a>" +
                                    "<br><span>" + replaceEmotion(dtChild.Rows[j]["Description"].ToString(), dictEmotion) + "</span>" +
                                    "</div>" +
                                "</div>" +
                                "<div class='sub2 btmline'></div>";
                        }
                    }
                }
                lstAnswer.DataSource = dt;
                lstAnswer.DataBind();
            }
        }

        private string replaceEmotion(string input, Dictionary<string, string> dictEmotion)
        {
            foreach (KeyValuePair<string, string> entry in dictEmotion)
            {
                input = input.Replace(entry.Key, entry.Value);
            }
            return input;
        }

        private Dictionary<string, string> getEmotionDictionary()
        {
            Dictionary<string, string> dictEmotion = new Dictionary<string, string>();
            int number = int.Parse(SysIniController.getSetting("Emotion_Comment_Number", "21"));
            DataTable dtEmotion = EmotionController.getEmotions(number);
            if (dtEmotion != null && dtEmotion.Rows.Count > 0)
            {
                string emotionImagePath = SysIniController.getSetting("Emotion_ImagePath", "images/emotion/");
                foreach (DataRow dr in dtEmotion.Rows)
                {
                    dictEmotion.Add(dr["Value"].ToString(), "<img border='0' src='" + emotionImagePath + dr["DisplayImage"].ToString() + "' />");
                }
            }
            return dictEmotion;
        }

        protected void dpLstAnswer_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);
                loadAnswer(id);
            }
        }

        private void loadEmotion()
        {
            int number = int.Parse(SysIniController.getSetting("Emotion_Comment_Number", "21"));
            DataTable dt = EmotionController.getEmotions(number);
            if (dt != null && dt.Rows.Count > 0)
            {
                litEmotion.Text = "";
                string emotionImagePath = SysIniController.getSetting("Emotion_ImagePath", "images/emotion/");
                foreach (DataRow dr in dt.Rows)
                {
                    litEmotion.Text += "<img alt='" + dr["Value"].ToString() + "' border='0' src='" + emotionImagePath + dr["DisplayImage"].ToString() + "' />&nbsp;";
                }
            }
        }

        protected void lbtSend_Click(object sender, EventArgs e)
        {
            if (Session["Email"] == null || Session["Email"].ToString() == "")
            {
                Session["Answer"] = txtAnswer.Text;
                Session["ParentAnswer"] = hdfAnswerId.Value;
                Session["RefUrl"] = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/')) +
                    Request.RawUrl;
                Response.Redirect("dang-nhap", true);
            }
            else
            {
                string pattern = "<(?!\r|\n).+?>";
                Regex regEx = new Regex(pattern);
                string description = HTMLRemoval.StripTagsRegexCmt(txtAnswer.Text).Replace("\r\n", "<br/>");

                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);

                int parentId = hdfAnswerId.Value == "" ? -1 : int.Parse(hdfAnswerId.Value);
                Answer answer = new Answer(parentId, description, DateTime.Now.ToString("yyyyMMddHHmmss"), 0, int.Parse(Session["UserId"].ToString()), int.Parse(id));

                bool result = AnswerController.createAnswer(answer);

                if (result)
                {
                    Session["Answer"] = null;
                    Session["ParentAnswer"] = null;
                    hdfAnswerId.Value = "";
                    txtAnswer.Text = "";
                    loadAnswer(id);
                }

                DataTable dt = AskController.getAskById(id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    this.Title = dt.Rows[0]["Ask"].ToString();
                }

                litAnswerCount.Text = AnswerController.getNumberOfAnswerByAskId(id);
            }
        }
    }
}