﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.util;
using PhongCachMobile.controller;
using PhongCachMobile.model;
using System.Data;

namespace PhongCachMobile
{
    public partial class m_dang_ky : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadState();
            }
        }

        private void loadState()
        {
            DataTable dt = StateController.getStates();
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlState.Items.Clear();
                ddlState.DataSource = dt;
                ddlState.DataTextField = "Name";
                ddlState.DataValueField = "Id";
                ddlState.SelectedIndex = 29;
                ddlState.DataBind();
            }
        }

        private void loadLang()
        {
            litRegister.Text = this.Title = "Đăng ký tài khoản";
            this.reqValEmail.ErrorMessage = this.reqValPassword.ErrorMessage = this.reqValRePassword.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
            this.cmpValRePassword.ErrorMessage = LangController.getLng("litPasswordMismatched.Text", " Mật khẩu xác nhận không đúng.");
            this.rgePasswordLength.ErrorMessage = this.rgeRePasswordLength.ErrorMessage = LangController.getLng("litPasswordLength", "Độ dài từ 6 - 30 kí tự.");
        }

        protected void lbtRegister_Click(object sender, EventArgs e)
        {
            string id = SysUserController.createUserForActivating(txtEmail.Text, txtPassword.Text, ddlState.SelectedValue);
            registerResponse.Visible = true;
            frmInput.Visible = false;
            if (id != "")
            {
                if (sendActivatingEmail(txtEmail.Text, txtPassword.Text, id))
                {
                    this.litRegisterResponse.Text = String.Format(LangMsgController.getLngMsg("litRegisterResponse.OK", "Cảm ơn bạn đã đăng ký tài khoản tại <font style='font-weight:bold;color:red'>PhongCachMobile</font>.<br/>" +
                        "Một email kích hoạt đã được gửi đến địa chỉ <font style='font-weight:bold;color:red'><u>{0}</u></font>. Hãy kiểm tra email của bạn và kích hoạt tài khoản."), txtEmail.Text);
                    this.Title = LangController.getLng("litRegisterResponse.OK", "Đăng ký tài khoản thành công");
                }
                else
                {
                    this.litRegisterResponse.Text = LangMsgController.getLngMsg("litRegisterResponse.Error", "Đã có lỗi xảy ra trong quá trình tạo tài khoản.<br/>Bạn vui lòng thử lại sau.");
                    this.Title = LangController.getLng("litRegisterResponse.Error", "Lỗi xảy ra trong quá trình đăng ký");
                }
            }
            else
            {
                this.litRegisterResponse.Text = LangMsgController.getLngMsg("litRegisterResponse.Error", "Đã có lỗi xảy ra trong quá trình tạo tài khoản.<br/>Bạn vui lòng thử lại sau.");
                this.Title = LangController.getLng("litRegisterResponse.Error", "Lỗi xảy ra trong quá trình đăng ký");
            }
        }

        private bool sendActivatingEmail(string email, string password, string userId)
        {
            try
            {
                string activatedToken = Common.generateHash(txtEmail.Text);
                string activatedEmailTemplate = TemplateController.getTemplate("EMAIL_REGISTER_TEMP", "<p><font style='font-weight:bold;color:red'>PhongCachMobile</font> - Kích hoạt tài khoản của bạn</p>" +
                        "<p><strong>Email:</strong> {0}</p>" +
                        "<p><strong>Mật khẩu:</strong> {1}</p>" +
                        "<p>Để kích hoạt tài khoản của bạn tại <font style='font-weight:bold;color:red'>PhongCachMobile</font>, hãy click vào link bên dưới:<br/>{2}</p><br/>" +
                        "<p>Nội dung trên được chuyển đến từ website <font style='font-weight:bold;color:red'>PhongCachMobile</font></p>" +
                        "<p><font style='font-weight:bold'>PhongCachMobile Team</font><br/>http://phongcachmobile.com.vn/</p>");
                string activatedUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/')) +
                    "/kich-hoat-" + userId + "-" + activatedToken;
                Common.sendEmail(txtEmail.Text, "", "", "", LangController.getLng("litActivatingEmail.Subject", "[PhongCachMobile] Kích hoạt tài khoản tại PhongCachMobile"),
                    String.Format(activatedEmailTemplate, email, "*****" + password.Substring(4), activatedUrl), "");
                return true;
            }
            catch (Exception ex)
            {
                // log
                LogController.createLog(new Log(DateTime.Now.ToString("yyyyMMddHHmmss"), ex.Message, 1, "Error sendActivatingEmail"));
                return false;
            }
        }

        protected void txtEmail_TextChanged(object sender, EventArgs e)
        {
            string emailPattern = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
            if (System.Text.RegularExpressions.Regex.IsMatch(txtEmail.Text, emailPattern))
            {
                if (!SysUserController.checkAvailableUser(txtEmail.Text))
                {
                    this.lblIsAvailabelAccount.ForeColor = System.Drawing.Color.Green;
                    this.lblIsAvailabelAccount.Text = LangController.getLng("litAvailableAccount.Text", "Bạn có thể sử dụng email này.");
                }
                else
                {
                    this.lblIsAvailabelAccount.ForeColor = System.Drawing.Color.Red;
                    this.lblIsAvailabelAccount.Text = LangController.getLng("litInAvailableAccount.Text", "Email đã tồn tại.");
                }
            }
            else
            {
                this.lblIsAvailabelAccount.ForeColor = System.Drawing.Color.Red;
                this.lblIsAvailabelAccount.Text = LangController.getLng("litInvalidFormat.Text", "Sai định dạng");
            }

            this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + this.lblIsAvailabelAccount.Text;
        }


    }
}