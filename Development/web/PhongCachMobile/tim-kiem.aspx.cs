﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class tim_kiem : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadNews();
                ddlBotSort.SelectedIndex = ddlTopSort.SelectedIndex = 2;
                //loadSidebarBanner();
            }
        }

        private void loadNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "56");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.removeSpecialChar(HTMLRemoval.StripTagsRegex(dr["Title"].ToString())).ToLower().Replace(' ', '-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = ts.Hours + " hour(s) ago";
                    //else
                    //    dr["TimeSpan"] = ts.Days + " day(s) ago";
                }
                lstNews.DataSource = dt;
                lstNews.DataBind();
            }
        }

        private void loadProductByProductName(string name)
        {
            DataTable dt = null;
            if (ddlTopSort.SelectedValue.Equals("posteddate", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.searchProductsByName(name);
            else if (ddlTopSort.SelectedValue.Equals("hightolowprice", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.searchHighToLowPriceProductsByName(name);
            else if (ddlTopSort.SelectedValue.Equals("lowtohighprice", StringComparison.CurrentCultureIgnoreCase))
                dt = ProductController.searchLowToHighPriceProductsByName(name);
            else //viewcount
                dt = ProductController.searchHighViewProductsByName(name);

            this.Title = this.litSearch.Text = dt.Rows.Count + " " + "sản phẩm được tìm thấy với từ khóa '" + name + "'.";

            if (dt != null && dt.Rows.Count > 0)
            {

                dt.Columns.Add("UrlName", typeof(string));
                dt.Columns.Add("sPrice", typeof(string));

                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlName"] = HTMLRemoval.removeSpecialChar(HTMLRemoval.StripTagsRegex(dr["Name"].ToString())).ToLower().Replace(' ', '-');
                    dr["sPrice"] = dr["DiscountPrice"].ToString() == "" ? long.Parse(dr["CurrentPrice"].ToString()).ToString("#,##0") :
                    long.Parse(dr["DiscountPrice"].ToString()).ToString("#,##0");
                }

                lstItem.DataSource = dt;
                lstItem.DataBind();
            }
        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string name = Request.QueryString["id"].ToString();
                loadProductByProductName(name);
            }
            else
                loadProductByProductName("");

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm #" + (dpgLstItemTop.StartRowIndex + 1) +
                " đến #" + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";

        }

        protected void ddlTopDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {

            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);
            //dpgLstItemTop.StartRowIndex =dpgLstItemBot. = 0;
            ddlBotDisplay.SelectedIndex = ddlTopDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string name = Request.QueryString["id"].ToString();

                loadProductByProductName(name);
            }
            else
            {
                loadProductByProductName("");
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm #" + (dpgLstItemTop.StartRowIndex + 1) +
               " đến #" + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlBotDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);

            ddlTopDisplay.SelectedIndex = ddlBotDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string name = Request.QueryString["id"].ToString();

                loadProductByProductName(name);
            }
            else
            {
                loadProductByProductName("");
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm #" + (dpgLstItemTop.StartRowIndex + 1) +
            " đến #" + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
            dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
            " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlTopSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBotSort.SelectedIndex = ddlTopSort.SelectedIndex;

            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string name = Request.QueryString["id"].ToString();

                loadProductByProductName(name);
            }
            else
            {
                loadProductByProductName("");
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm #" + (dpgLstItemTop.StartRowIndex + 1) +
             " đến #" + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
             dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
             " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }

        protected void ddlBotSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlTopSort.SelectedIndex = ddlBotSort.SelectedIndex;

            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string name = Request.QueryString["id"].ToString();

                loadProductByProductName(name);
            }
            else
            {
                loadProductByProductName("");
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = "Sản phẩm #" + (dpgLstItemTop.StartRowIndex + 1) +
            " đến #" + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
            dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
            " trong " + dpgLstItemTop.TotalRowCount + " sản phẩm.";
        }
    }
}