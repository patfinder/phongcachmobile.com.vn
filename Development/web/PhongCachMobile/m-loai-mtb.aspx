﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-loai-mtb.aspx.cs" Inherits="PhongCachMobile.m_loai_mtb" %>

<%@ Register Src="~/master/uc/ucProductListBare.ascx" TagName="ucProductListBare" TagPrefix="ucProductListBare" %>
<%@ Register Src="~/master/uc/ucTripleBanner.ascx" TagName="ucTripleBanner" TagPrefix="ucTripleBanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="css/category.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="content">

        <% if(!hideBanners) { %>
        <div class="specify">
            <div class="slide-home">
                <% foreach (var article in topBannerItems) { %>
                <div class="items">
                    <a href="<%= article.link %>">
                        <img src="/images/slide/<%= article.displayImg %>" alt="" />
                    </a>
                </div>
                <% } %>
            </div>
        </div>
        <% } %>

        <div class="product-list">
            <div class="product-other content-center">

                <div class="filter">
                    <div  class="label"><span>Tìm theo: </span></div>
                    <div class="dropdown" id="ddPrice">
                        <a data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Mức giá</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <% foreach(KeyValuePair<string, int> kv in prices) { %>
                            <li><a href="javascript:;" value="<%= kv.Value %>"><%= kv.Key %></a></li>
                            <% } %>
                        </ul>
                    </div>
                    <div class="promotion raBrand <%= activeBrandID == 15 ? "active" : "" %>" id="raApple" value="15">
                        <i class="fa fa-circle"></i> <span>Apple (<%= appleCount %>)</span>
                    </div>
                    <div class="promotion raBrand <%= activeBrandID == 16 ? "active" : "" %>" id="raSamsung" value="16">
                        <i class="fa fa-circle"></i> <span>Samsung (<%= samsungCount %>)</span>
                    </div>
                    <div class="promotion raBrand" id="raOther" value="9999">
                        <i class="fa fa-circle"></i> <span>Khác (<%= otherCount %>)</span>
                    </div>
                    <div class="promotion" id="ckPromotion">
                        <i class="fa fa-circle"></i> <span>KHUYẾN MÃI (<%= promotionCount %>)</span>
                    </div>
                    <div class="dropdown sort" id="ddSortPrice">
                        <a data-target="#" value="1" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Giá từ Cao đến Thấp</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="javascript:;" value="0">Giá từ Thấp đến Cao</a></li>
                        </ul>
                    </div>
                </div>
                <ul>
                    <ucProductListBare:ucProductListBare ID="ucFilterPhoneList" ProductList="<%# filterPhoneList %>" runat="server" />
                </ul>
                <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= pageSize ? pageSize : remainCount %></span> sản phẩm</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">
        
    <% // ---------------------------- Footer ---------------------------- %>

    <script type="text/javascript">

        var pageSize = <%= pageSize %>;

        $(document).ready(function () {

            window.filterIDs = { ddBrand: "brand", ddPrice: "price", ddSortPrice: "sortPrice" };

            // Apply filters handling
            applyFilterHandling("ddPrice", false, function() {
                loadMoreProducts("GetProducts", true, pageSize);
            });
            applyFilterHandling("ddSortPrice", true, function() {
                loadMoreProducts("GetProducts", true, pageSize);
            });

            var brandIDs = ["raApple", "raSamsung", "raOther"];

            // Brand filters
            $("div.raBrand").on("click", function () {

                // Clear feature filter
                paramValues.featureID = -1;

                var curID = $(this).attr("id");
                paramValues.brand = $(this).attr("value");
                $(this).addClass("active");

                // clear "active" of all other than current
                $.each(brandIDs, function (index, id) {
                    if (id != curID) {
                        $("#" + id).removeClass("active");
                    }
                });

                loadMoreProducts("GetProducts", true, pageSize);
            });

            // Promotion
            $("div#ckPromotion").on("click", function () {

                // Clear feature filter
                // paramValues.featureID = -1;

                paramValues.promotion = 1 - $(this).hasClass("active"); // toggle 0-1
                $(this).toggleClass("active");
                console.log("paramValues.promotion: " + paramValues.promotion);

                loadMoreProducts("GetProducts", true, pageSize);
            });

            $(".read-more").on("click", function () {
                loadMoreProducts("GetProducts", false, pageSize);
            });
        });

        var paramValues = { featureID: <%= activeFeatureID %>, group: 4, brand: <%= activeBrandID %>, price: -1, os: <%= activeOSID %>, promotion: 0, sortPrice: 1, page: 0 };

    </script>

</asp:Content>
