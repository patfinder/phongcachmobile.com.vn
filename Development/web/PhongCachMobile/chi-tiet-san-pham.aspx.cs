﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using System.Text.RegularExpressions;
using PhongCachMobile.util;
using PhongCachMobile.model;
using System.IO;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class chi_tiet_san_pham : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((Home)this.Master).HeaderType = master.uc.HeaderType.DetailPage;

            PageUrl = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
                (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + Request.RawUrl;
            //PageUrl = "http://phongcachmobile1.com/product";

            id = int.Parse(Request.QueryString["id"].Split(new char[]{'-'}).Last());

            if (!this.IsPostBack)
            {
                loadProduct();
                loadRelatedProducts();
            }
        }

        // Current product price
        protected bool IsAccessory;
        protected string PageUrl = "";
        protected int id = 0;

        /// <summary>
        /// Dark background
        /// </summary>
        protected bool DarkBG = false;
        protected string backgroundImage = "";

        protected Article breadcrumCategory = new Article();
        protected Article category = new Article();
        protected Product product = new Product();
        protected List<Article> productFeatures1 = new List<Article>();
        protected List<Article> productFeatures2 = new List<Article>();

        protected string RemoveLeadingSlash(string path)
        {
            return path.StartsWith("/") ? path.Substring(1) : path;
        }

        protected void loadProduct()
        {
            // Product
            DataRow drProduct = ProductController.getProductById(id.ToString()).Rows[0];
            product = ProductController.rowToProduct(drProduct);

            //this.Title = LangController.getLng("litWebsiteTitle.Text", "PhongCachMobile") + " - " + product.name;

            // Load background
            DarkBG = product.useDarkBackground;

            if (string.IsNullOrWhiteSpace(product.backgroundImage))
                backgroundImage = "/images/banner/" + 
                    (product.useDarkBackground ? SliderController.rowToSlider(SliderController.getSliders("2072").Rows[0]).displayImg
                    : SliderController.rowToSlider(SliderController.getSliders("72").Rows[0]).displayImg);
            else
                backgroundImage = "/images/product/" + product.backgroundImage;

            IsAccessory = product.groupId == ProductController.ProductGroupAccessoryInt;

            product.allImages = new List<string>();
            product.allImageDescs = new List<string>();

            string[] images = new string[] { product.firstImage, product.secondImage, product.thirdImage, product.fourthImage, product.fifthImage };
            string[] descs = new string[] { product.firstImageDesc, product.secondImageDesc, product.thirdImageDesc, product.fourthImageDesc, product.fifthImageDesc };

            // Update view counter
            if (Session["CurrentProductId-" + id] == null)
            {
                ProductController.addViewCount(id.ToString());

                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday && drProduct["isWeeklyCountUpdated"].ToString() == "False")
                {
                    ProductController.resetWeeklyCount(id.ToString());
                }
                else if (DateTime.Now.DayOfWeek == DayOfWeek.Monday && drProduct["isWeeklyCountUpdated"].ToString() == "True")
                {
                    ProductController.addWeeklyViewCount(id.ToString());
                }
                else if (DateTime.Now.DayOfWeek != DayOfWeek.Monday)
                {
                    ProductController.add_resetWeeklyViewCount(id.ToString());
                }

                // Mark current product as viewed
                Session["CurrentProductId-" + id] = true;
            }

            // Skip first image from slide
            for (int i = 1; i < 5; i++)
            {
                if (string.IsNullOrWhiteSpace(images[i]))
                    continue;

                product.allImages.Add(images[i]);
                // product.allImageDescs.Add(HTMLRemoval.StripTagsRegex(HTMLRemoval.StripTagsRegexCmt(descs[i])));
                product.allImageDescs.Add(descs[i]);
            }

            breadcrumCategory = new Article
            {
                title = ProductController.ProductGroupIDNameMap[product.groupId],
                link = Common.m() + ProductController.ProductGroupIDLinkMap[product.groupId],
            };

            // Category
            DataRow dr = CategoryController.getCategoryById(product.categoryId.ToString()).Rows[0];
            category = new Article { title = dr["Name"].ToString(), link = Common.m() + 
                (product.groupId == ProductController.ProductGroupAccessoryInt ? "sub-phu-kien" : ProductController.ProductGroupIDLinkMap[product.groupId]) + 
                "-" + dr["name"] + "-" + dr["id"] };

            List<Article> tmpList = new List<Article>();

            // Get features
            DataTable dtGroup = GroupController.getGroupsByFilter("FEATURE_TYPE");
            if (dtGroup != null)
            {
                foreach (DataRow drGroup in dtGroup.Rows)
                {
                    DataTable dt = FeatureController.getFeatures(product.id.ToString(), drGroup["Id"].ToString());

                    string tmp = "";
                    foreach (DataRow dr2 in dt.Rows)
                    {
                        if (string.IsNullOrWhiteSpace(dr2["Value"] as string))
                            continue;

                        if (tmp == "")
                            tmp = dr2["Name"] + ": " + dr2["Value"];
                        else
                            tmp += "<br />" + dr2["Name"] + ": " + dr2["Value"];
                    }

                    tmpList.Add(new Article
                    {
                        id = drGroup["Id"] is DBNull ? 0 : (int)drGroup["Id"],
                        title = (string)drGroup["Name"],
                        longDesc = tmp,
                    });
                }
            }

            // Đặc điểm, Màn hình, Bộ nhớ, Camera, Pin
            Dictionary<int, int> priorityMap = new Dictionary<int, int>
            {
                {17, 1}, // Đặc điểm
                {12, 2}, // Màn hình
                {14, 3}, // Bộ nhớ
                {16, 4}, // Camera
                {18, 5}, // Pin
            };

            tmpList.Sort(new FunctionalComparer<Article>((a1, a2) =>
            {
                int k1 = priorityMap.Keys.Contains(a1.id) ? priorityMap[a1.id] : 99;
                int k2 = priorityMap.Keys.Contains(a2.id) ? priorityMap[a2.id] : 99;

                return k1.CompareTo(k2);
            }));

            for (int i = 0; i < tmpList.Count; i++)
            {
                if (priorityMap.Keys.Contains(tmpList[i].id))
                    productFeatures1.Add(tmpList[i]);
                else
                    productFeatures2.Add(tmpList[i]);
            }
        }

        protected List<Product> accessories = new List<Product>();
        protected List<Product> similarProducts = new List<Product>();
        protected void loadRelatedProducts()
        {
            string imagePath = SysIniController.getSetting("Avatar_ImagePath", "images/avatar/");
            //this.imgAvatar.ImageUrl = imagePath + new Random().Next(1, 11) + ".png";

            // Suggested Accessories
            var productList = product.suggestedAccessories
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => int.Parse(s.Trim())).Take(4).ToList();

            DataTable dt = productList.Count <= 0 ? null : ProductController.getProductByIDs(productList);
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    accessories.Add(ProductController.rowToProduct(dr));
                }
            }

            // Similar Products
            int number = int.Parse(SysIniController.getSetting("Number_Other_Product", "4"));
            if (IsAccessory)
            {
                // Same category accessories
                dt = ProductController.getProductsByCategoryIDs(new int[] { product.categoryId }, number);
            }
            else
            {
                // Same price products
                int difference = int.Parse(SysIniController.getSetting("Number_Interval_Price", "1000000"));
                dt = ProductController.getSamePriceProducts(number, id.ToString(), difference); //GroupId Top Slider
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    similarProducts.Add(ProductController.rowToProduct(dr));
                }
            }

            ucAccessories.DataBind();
            ucSimilarProducts.DataBind();
        }
    }
}