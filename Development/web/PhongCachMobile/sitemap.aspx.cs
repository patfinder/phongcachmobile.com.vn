﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;

namespace PhongCachMobile
{
    public partial class sitemap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadMobile();
                loadTablet();
                loadApplication();
                loadAccessories();
            }
        }

        private void loadAccessories()
        {
            DataTable dt = MenuController.getMenus("2", "");
            if (dt != null && dt.Rows.Count > 0)
            {
                litAccessories.Text = "<ul class='tree'>";
                foreach (DataRow dr in dt.Rows)
                {
                    string text = dr["Text"].ToString();
                    string actionUrl = dr["ActionUrl"].ToString();
                    string inOrd = dr["InOrder"].ToString();

                    if (dr["Id"].ToString() != dt.Rows[dt.Rows.Count - 1]["Id"].ToString())
                        litAccessories.Text += "<li><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    else
                        litAccessories.Text += "<li class='last'><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    if (inOrd == "3") // Phu kien
                        loadMenuAccessoriesCategory();
                    litAccessories.Text += "</li>";
                }
            }
            litAccessories.Text += "</ul>";
        }

        private void loadMenuAccessoriesCategory()
        {
            DataTable dtSub = CategoryController.getCategories("7", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litAccessories.Text += "<ul>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {

                    DataRow drSub = dtSub.Rows[i - 1];
                    litAccessories.Text += "<li><a href='pkdtdd-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + drSub["Name"].ToString() + "</a></li>";
                }

                DataTable dtApp = CategoryController.getCategories("8", "");
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtApp.Rows.Count; i++)
                    {
                        DataRow drApp = dtApp.Rows[i - 1];
                        if (drApp["Id"].ToString() != dtApp.Rows[dtApp.Rows.Count - 1]["Id"].ToString())
                            litAccessories.Text += "<li><a href='pkmtb-" + drApp["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drApp["Id"].ToString() + "'>" + drApp["Name"].ToString() + "</a></li>";
                        else
                            litAccessories.Text += "<li class='last'><a href='pkmtb-" + drApp["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drApp["Id"].ToString() + "'>" + drApp["Name"].ToString() + "</a></li>";
                    }
                }
                litAccessories.Text += "</ul>";
            }
        }

        private void loadApplication()
        {
            DataTable dt = MenuController.getMenus("2", "");
            if (dt != null && dt.Rows.Count > 0)
            {
                litApplication.Text = "<ul class='tree'>";
                foreach (DataRow dr in dt.Rows)
                {
                    string text = dr["Text"].ToString();
                    string actionUrl = dr["ActionUrl"].ToString();
                    string inOrd = dr["InOrder"].ToString();

                    if (dr["Id"].ToString() != dt.Rows[dt.Rows.Count - 1]["Id"].ToString())
                        litApplication.Text += "<li><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    else
                        litApplication.Text += "<li class='last'><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    if (inOrd == "2") //Ung dung
                        loadMenuGameAppCategory();
                    litApplication.Text += "</li>";
                }
            }
            litApplication.Text += "</ul>";
        }

        private void loadMenuGameAppCategory()
        {
            DataTable dtSub = CategoryController.getCategories("5", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litApplication.Text += "<ul>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {
                    DataRow drSub = dtSub.Rows[i - 1];
                    litApplication.Text += "<li><a href='game-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + "Game- " + drSub["Name"].ToString() + "</a></li>";
                }
               
                DataTable dtApp = CategoryController.getCategories("6", "");
                if (dtApp != null && dtApp.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtApp.Rows.Count; i++)
                    {
                        DataRow drApp = dtApp.Rows[i - 1];
                        if (drApp["Id"].ToString() != dtApp.Rows[dtApp.Rows.Count - 1]["Id"].ToString())
                            litApplication.Text += "<li><a href='ud-" + drApp["Name"].ToString().ToLower().Replace(' ', '-') + "-" + 
                                drApp["Id"].ToString() + "'>" + "Ứng dụng- " + drApp["Name"].ToString() + "</a></li>";
                        else
                            litApplication.Text += "<li class='last'><a href='ud-" + drApp["Name"].ToString().ToLower().Replace(' ', '-') + "-" +
                                drApp["Id"].ToString() + "'>" + "Ứng dụng- " + drApp["Name"].ToString() + "</a></li>";
                    }
                }

                litApplication.Text += "</ul>";
            }
        }

        private void loadTablet()
        {
            DataTable dt = MenuController.getMenus("2", "");
            if (dt != null && dt.Rows.Count > 0)
            {
                litTablet.Text = "<ul class='tree'>";
                foreach (DataRow dr in dt.Rows)
                {
                    string text = dr["Text"].ToString();
                    string actionUrl = dr["ActionUrl"].ToString();
                    string inOrd = dr["InOrder"].ToString();

                    if (dr["Id"].ToString() != dt.Rows[dt.Rows.Count - 1]["Id"].ToString())
                        litTablet.Text += "<li><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    else
                        litTablet.Text += "<li class='last'><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    if (inOrd == "1") //May tinh bang
                        loadMenuTabletCategory();
                    litTablet.Text += "</li>";
                }
            }
            litTablet.Text += "</ul>";
        }

        private void loadMenuTabletCategory()
        {
            DataTable dtSub = CategoryController.getCategories("4", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litTablet.Text += "<ul>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {
                    DataRow drSub = dtSub.Rows[i - 1];
                    litTablet.Text += "<li><a href='mtb-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + drSub["Name"].ToString() + "</a></li>";
                }

                DataTable dtOS = OperatingSystemController.getOperatingSystemsByGroupId("4");
                if (dtOS != null && dtOS.Rows.Count > 0)
                {
                    foreach (DataRow drOS in dtOS.Rows)
                    {
                        if (drOS["Id"].ToString() != dtOS.Rows[dtOS.Rows.Count - 1]["Id"].ToString())
                            litTablet.Text += "<li><a href='dtdd-hdh-" + drOS["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drOS["Id"].ToString() + "'>" + "Hệ điều hành " + drOS["Name"].ToString() + "</a></li>";
                        else
                            litTablet.Text += "<li class='last'><a href='dtdd-hdh-" + drOS["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drOS["Id"].ToString() + "'>" + "Hệ điều hành " + drOS["Name"].ToString() + "</a></li>";
                    }
                }

                litTablet.Text += "</ul>";
            }
        }

        private void loadMobile()
        {
            DataTable dt = MenuController.getMenus("2", "");
            if(dt != null && dt.Rows.Count > 0 )
            {
                litMobile.Text = "<ul class='tree'>";
                foreach(DataRow dr in dt.Rows)
                {
                    string text = dr["Text"].ToString();
                    string actionUrl = dr["ActionUrl"].ToString();
                    string inOrd = dr["InOrder"].ToString();

                    if (dr["Id"].ToString() != dt.Rows[dt.Rows.Count -1]["Id"].ToString())
                        litMobile.Text += "<li><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    else
                        litMobile.Text += "<li class='last'><a href='" + actionUrl.Split('.')[0] + "'>" + text + "</a>";
                    if (inOrd == "0") //Dien thoai di dong
                        loadMenuPhoneCategory();
                    litMobile.Text += "</li>";
                }
            }
            litMobile.Text += "</ul>";
                                      
        }

        private void loadMenuPhoneCategory()
        {
            DataTable dtSub = CategoryController.getCategories("3", "");
            if (dtSub != null && dtSub.Rows.Count > 0)
            {
                litMobile.Text += "<ul>";
                for (int i = 1; i <= dtSub.Rows.Count; i++)
                {
                    DataRow drSub = dtSub.Rows[i - 1];
                    litMobile.Text += "<li><a href='dtdd-" + drSub["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drSub["Id"].ToString() + "'>" + drSub["Name"].ToString() + "</a></li>";
                }

                DataTable dtOS = OperatingSystemController.getOperatingSystemsByGroupId("3");
                if (dtOS != null && dtOS.Rows.Count > 0)
                {
                    foreach (DataRow drOS in dtOS.Rows)
                        litMobile.Text += "<li><a href='dtdd-hdh-" + drOS["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drOS["Id"].ToString() + "'>" + "Hệ điều hành " + drOS["Name"].ToString() + "</a></li>";
                }

                DataTable dtFeature = FeatureController.getFeaturesByGroupId("3");
                if (dtFeature != null && dtFeature.Rows.Count > 0)
                {
                    foreach (DataRow drFeature in dtFeature.Rows)
                    {
                        if (drFeature["Id"].ToString() != dtFeature.Rows[dtFeature.Rows.Count - 1]["Id"].ToString())
                            litMobile.Text += "<li><a href='dtdd-tn-" + drFeature["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drFeature["Id"].ToString() + "'>" + drFeature["Name"].ToString() + "</a></li>";
                        else
                            litMobile.Text += "<li class='last'><a href='dtdd-tn-" + drFeature["Name"].ToString().ToLower().Replace(' ', '-') + "-" + drFeature["Id"].ToString() + "'>" + drFeature["Name"].ToString() + "</a></li>";
                    }

                }
                litMobile.Text += "</ul>";
            }
        }

        private void loadLang()
        {
            litSitemap.Text = this.Title =  LangController.getLng("litSitemap.Text", "Sitemap");
            litProduct.Text = LangController.getLng("litProduct.Text", "Sản phẩm");

            litNews.Text = LangController.getLng("litNews.Text", "Tin tức");
            litLastestNews.Text = LangController.getLng("litLastestNews.Text", "Tin mới nhất");
            litPromotionNews.Text = LangController.getLng("litPromotionNews.Text", "Tin khuyến mãi");

            litInfo.Text = LangController.getLng("litInfo.Text", "Thông tin");
            litIntroduce.Text = LangController.getLng("litIntroduce.Text", "Giới thiệu");
            litProductQuality.Text = LangController.getLng("litProductQuality.Text", "Chất lượng hàng hóa");
            litJoinBusiness.Text = LangController.getLng("litJoinBusiness.Text", "Hợp tác kinh doanh");
            litContact.Text = LangController.getLng("litContact.Text", "Liên hệ");

            litSupport.Text = LangController.getLng("litSupport.Text", "Hỗ trợ");
            litFAQ.Text = LangController.getLng("litFAQ.Text", "FAQ");
            litDistanceBuying.Text = LangController.getLng("litDistanceBuying.Text", "Mua hàng từ xa");
            litGuaranteeRule.Text = LangController.getLng("litGuaranteeRule.Text", "Quy định bảo hành");
            litFixSetup.Text = LangController.getLng("litFixSetup.Text", "Sửa chữa & cài đặt");

        }
    }
}