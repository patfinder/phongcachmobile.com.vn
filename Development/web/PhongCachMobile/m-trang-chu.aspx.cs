﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.master;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class m_trang_chu : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((MHome)this.Master).HeaderType = master.uc.HeaderType.Home;
            ((MHome)this.Master).TemplateType = TemplateType.NewType;

            if (!this.IsPostBack)
            {
                loadBanner();
                loadHotProducts();
                loadRandomProducts();
                loadBottomProducts();
            }
        }

        protected List<Article> bannerItems = new List<Article>();
        protected void loadBanner()
        {
            // Trang_Chu_Banner_Images // V2-Home Page - Sliders
            foreach (DataRow dr in SliderController.getSliders("69").Rows)
            {
                bannerItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
        }

        protected List<Product> hot2Products = new List<Product>();
        protected void loadHotProducts()
        {
            var dt = ProductController.getRandomDiscountProducts(2);
            foreach (DataRow dr in dt.Rows)
            {
                hot2Products.Add(ProductController.rowToProduct(dr));
            }
        }

        protected List<Product> rand2Products = new List<Product>();
        protected void loadRandomProducts()
        {
            var dt = ProductController.getRandomSpecial(1, ProductController.ProductGroupPhone);
            
            if(dt != null && dt.Rows.Count > 0)
                rand2Products.Add(ProductController.rowToProduct(dt.Rows[0]));

            dt = ProductController.getRandomProductsByGroupId(ProductController.ProductGroupTabletInt, 1);
            if(dt != null && dt.Rows.Count > 0)
                rand2Products.Add(ProductController.rowToProduct(dt.Rows[0]));
        }

        protected Product randAccessory = null;
        protected Product randBottomProduct = null;
        protected void loadBottomProducts()
        {
            randAccessory = ProductController.rowToProduct(ProductController.getRandomFeatured(1, ProductController.ProductGroupAccessory).Rows[0]);
            randBottomProduct = ProductController.rowToProduct(OldProductController.getRandomOldProducts(1).Rows[0], true);
        }
    }
}