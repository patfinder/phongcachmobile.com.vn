﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="tin-thi-truong.aspx.cs" Inherits="PhongCachMobile.tin_thi_truong" %>

<%@ Register Src="~/master/uc/ucNewsListBare.ascx" TagName="ucNewsListBare" TagPrefix="ucNewsListBare" %>
<%@ Register Src="~/master/uc/ucDigitalToys.ascx" TagName="ucDigitalToys" TagPrefix="ucDigitalToys" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">

    <div class="content">
        <div class="breadcrumb">
            <div class="content-center">
                <ol>
                    <li><a href="/">Trang chủ</a><i class="fa fa-angle-right"></i></li>
                    <li>Tin Thị trường</li>
                </ol>
            </div>
        </div>
        <section class="page-new">
            <div class="group-infor content-center clearfix">
                <div class="product-other content-main">
                    <ul class="clearfix" id="newsList">
                        
                        <ucNewsListBare:ucNewsListBare ID="ucNewsListBare" NewsList="<%# newsList %>" runat="server" />

                    </ul>
                    <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= 10 ? 10 : remainCount %></span> tin</a>
                </div>

                <ucDigitalToys:ucDigitalToys ID="ucDigitalToys" runat="server" />

            </div>
        </section>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">

    <script type="text/javascript">
        var top_footer = $('.footer').offset().top;
        var top_right = $('.aside-right').offset().top;
        $(document).ready(function () {

            $(window).scroll( function(){
                var top_scroll  = $(this).scrollTop();
                if(top_scroll > top_right){
                    $('.aside-right .content-right').addClass('fixTop');
                }


                if( top_scroll > (top_footer - 480)){
                    $('.aside-right .content-right').addClass('fixBottom');
                    $('.aside-right .content-right').removeClass('fixTop');
                }
                else {
                    if( top_right < top_scroll < (top_footer - 480)){
                        $('.aside-right .content-right').addClass('fixTop');
                        $('.aside-right .content-right').removeClass('fixBottom');
                    }
                    else if(  top_scroll < top_right){
                        $('.aside-right .content-right').removeClass('fixTop');
                    }
                }
                if(  top_scroll < top_right){
                    $('.aside-right .content-right').removeClass('fixTop');
                }
            });

            $(".read-more").on("click", function () {
                loadMoreArticles("GetMarketNews", false, 10);
            });
        });

        var paramValues = { page: 0, pageSize: 10 };

    </script>

</asp:Content>
