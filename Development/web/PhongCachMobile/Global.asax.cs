﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using PhongCachMobile.controller;
using System.Threading;
using log4net.Config;

namespace PhongCachMobile
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            XmlConfigurator.Configure();

            // Code that runs on application startup
            Application["OnlineUsers"] = 0;
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }

        void Session_Start(object sender, EventArgs e)
        {
            // Just to save something to session so later Session_End will be triggered.
            // http://stackoverflow.com/questions/4813462/session-end-does-not-fire

            Session["Start_Time"] = DateTime.Now;

            Application.Lock();
            Application["OnlineUsers"] = int.Parse(Application["OnlineUsers"].ToString()) + 1;
            Application.UnLock();
            SysIniController.updateSetting("TotalVisit", (int.Parse(SysIniController.getSetting("TotalVisit", "1")) + 1) + "");
            string todayDate = SysIniController.getSetting("Counter_TodayDate", "20120101");
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) > int.Parse(todayDate))
            {
                SysIniController.updateSetting("TodayVisit", "1");
                SysIniController.updateSetting("Counter_TodayDate", DateTime.Now.ToString("yyyyMMdd"));

            }
            else // Current Date
            {
                SysIniController.updateSetting("TodayVisit", (int.Parse(SysIniController.getSetting("TodayVisit", "1")) + 1) + "");
            }
        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
            Application.Lock();
            Application["OnlineUsers"] = int.Parse(Application["OnlineUsers"].ToString()) - 1;
            Application.UnLock();

        }

    }
}
