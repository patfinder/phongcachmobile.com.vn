﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using PhongCachMobile.admin.master;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class loai_macbook : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.Home)this.Master).TemplateType = TemplateType.NewType;

            // Brand
            string idStr = Request.QueryString["id"];
            if (!string.IsNullOrWhiteSpace(idStr))
            {
                // Category
                activeCategoryID = int.Parse(idStr.Split('-').Last());
                activeCategoryName = PhongCachMobile.controller.CategoryController.getCategoryNameById(activeCategoryID.ToString());

                hideBanners = true;
            }

            if (!this.IsPostBack)
            {
                loadBannerItems();
                loadFilterValues();
                loadFilterMacbookList();
            }
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("Macbook_So_SP_Filter_Page", "20"));

        // Hide Banner
        protected bool hideBanners = false;

        // Active Category
        protected int activeCategoryID = -1;
        protected string activeCategoryName = "Tất cả";

        protected List<Article> topBannerLeftItems = new List<Article>();
        protected List<Article> topBannerRightItem1s = new List<Article>();
        protected List<Article> topBannerRightItem2s = new List<Article>();
        private void loadBannerItems()
        {
            // MTB_Top_Banner_Left_Items // V2-Macbook - Sliders Left
            foreach (DataRow dr in SliderController.getSliders("1077").Rows)
            {
                topBannerLeftItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // MTB_Top_Banner_Right_Items1 // V2-Macbook - Sliders Right 1
            foreach (DataRow dr in SliderController.getSliders("1078").Rows)
            {
                topBannerRightItem1s.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // MTB_Top_Banner_Right_Items2 // V2-Macbook - Sliders Right 2
            foreach (DataRow dr in SliderController.getSliders("1079").Rows)
            {
                topBannerRightItem2s.Add(SliderController.rowToSlider(dr).toArticle());
            }

            ucTripleBanner.DataBind();
        }

        protected Dictionary<string, int> prices = new Dictionary<string, int>();
        protected Dictionary<string, int> oses = new Dictionary<string, int>();
        protected int promotionCount = 0;

        private void loadFilterValues()
        {
            // Prices
            prices = ProductController.PriceList;

            // OSes
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId(ProductController.ProductGroupMacbook);
            if (dt != null && dt.Rows.Count > 0)
            {
                oses.Add("Tất cả", -1);
                foreach (DataRow dr in dt.Rows)
                {
                    oses.Add(dr["Name"].ToString(), int.Parse(dr["Id"].ToString()));
                }
            }

            promotionCount = ProductController.getDiscountProductCount(ProductController.ProductGroupMacbookInt);
        }

        protected int remainCount = 0;
        protected List<Product> filterPhoneList = new List<Product>();
        private void loadFilterMacbookList()
        {
            DataTable dt = ProductController.getProductsByGroupId(
                ProductController.ProductGroupMacbookInt, activeCategoryID, -1, -1, false, true, out remainCount);

            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);
                filterPhoneList.Add(product);
            }

            ucFilterPhoneList.DataBind();
        }
    }
}