﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.Text.RegularExpressions;
using PhongCachMobile.model;

namespace PhongCachMobile
{
    public partial class hoi_dap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "Hỏi - đáp";

            if (!this.IsPostBack)
            {
                loadLang();
                loadAsk();
                loadStat();
                
            }
        }

       

        private void loadStat()
        {
            litNumberAsk.Text = int.Parse(AskController.getNumberAsk()).ToString("#,##0");
            litNumberAnswer.Text = int.Parse(AnswerController.getNumberAnswer()).ToString("#,##0");
            litNumberUser.Text = int.Parse(AskController.getNumberUser()).ToString("#,##0");
        }

        private void loadAsk()
        {
            DataTable dt = null;
            if (ddlSort.SelectedValue == "moi-nhat")
                dt = AskController.getAskOrderByPostedDate();
            else if (ddlSort.SelectedValue == "luot-xem")
                dt = AskController.getAskOrderByViewCount();
            else if (ddlSort.SelectedValue == "luot-tra-loi")
                dt = AskController.getAskOrderByAnswerCount();

            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("sPostedDate", typeof(string));
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("Emailname", typeof(string));
                dt.Columns.Add("CommentCount", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["sPostedDate"] = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss").ToLongDateString();
                    dr["UrlTitle"] = dr["Title"].ToString().ToLower().Replace(' ', '-');
                    string email = SysUserController.getEmailById(dr["UserId"].ToString());
                    dr["Emailname"] = email.Substring(0, email.IndexOf('@'));
                    dr["CommentCount"] = AnswerController.getNumberOfAnswerByAskId(dr["Id"].ToString());
                }
                lstQAQ.DataSource = dt;
                lstQAQ.DataBind();
            }
        }

        private void loadLang()
        {
            lbtAsk.Text = "Đặt câu hỏi";
            this.rgeAskLength.ErrorMessage = LangController.getLng("litCommentLength", "Độ dài từ 20 - 1000 kí tự.");
            this.reqValAsk.ErrorMessage = this.reqValTitle.ErrorMessage = LangController.getLng("litRequired.Text", "(*)");
            if (Session["Ask"] != null && Session["Ask"].ToString() != "")
                txtAsk.Text = Session["Ask"].ToString();

            if (Session["Title"] != null && Session["Title"].ToString() != "")
                txtTitle.Text = Session["Title"].ToString();
        }

        protected void ddlBotDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = int.Parse(ddlBotDisplay.SelectedValue);

            loadAsk();

            dpgLstItemBot.SetPageProperties(0, dpgLstItemBot.PageSize, true);

            litBotItemsIndex.Text = "Câu hỏi #" + (dpgLstItemBot.StartRowIndex + 1) +
                " đến #" + (dpgLstItemBot.StartRowIndex + dpgLstItemBot.PageSize < dpgLstItemBot.TotalRowCount ?
                dpgLstItemBot.StartRowIndex + dpgLstItemBot.PageSize : dpgLstItemBot.TotalRowCount) +
                " trong " + dpgLstItemBot.TotalRowCount + " câu.";
        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            loadAsk();

            litBotItemsIndex.Text = "Câu hỏi # " + (dpgLstItemBot.StartRowIndex + 1) +
                 " đến #" + (dpgLstItemBot.StartRowIndex + dpgLstItemBot.PageSize < dpgLstItemBot.TotalRowCount ?
                 dpgLstItemBot.StartRowIndex + dpgLstItemBot.PageSize : dpgLstItemBot.TotalRowCount) +
                 " trong " + dpgLstItemBot.TotalRowCount + " câu.";
        }

        protected void ddlSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadAsk();
        }

        protected void lbtAsk_Click(object sender, EventArgs e)
        {
            if (Session["Email"] == null || Session["Email"].ToString() == "")
            {
                Session["Ask"] = txtAsk.Text;
                Session["Title"] = txtTitle.Text;
                Session["RefUrl"] = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/')) +
                    Request.RawUrl;
                Response.Redirect("dang-nhap", true);
            }
            else
            {
                string pattern = "<(?!\r|\n).+?>";
                Regex regEx = new Regex(pattern);
                string description = HTMLRemoval.StripTagsRegexCmt(txtAsk.Text).Replace("\r\n", "<br/>");
                string _title = HTMLRemoval.StripTagsRegexCmt(txtTitle.Text).Replace("\r\n", "<br/>");

                Ask ask = new Ask{
                    ask = description,
                    userId = int.Parse(Session["UserId"].ToString()),
                    postedDate = DateTime.Now.ToString("yyyyMMddHHmmss"),
                    viewCount = 0,
                    title = _title,
                    isSolved = false
                };

                bool result = AskController.createAsk(ask);

                if (result)
                {
                    Session["Ask"] = null;
                    txtAsk.Text = "";
                    txtTitle.Text = "";
                    loadAsk();
                    loadStat();
                }
            }
        }
    }
}