﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using PhongCachMobile.admin.master;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class dtdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.Home)this.Master).TemplateType = TemplateType.NewType;

            // Brand
            string idStr = Request.QueryString["id"];
            if(!string.IsNullOrWhiteSpace(idStr))
            {
                // OS
                if (idStr.StartsWith("hdh-"))
                {
                    activeOSID = int.Parse(idStr.Split('-').Last());
                    activeOSName = PhongCachMobile.controller.OperatingSystemController.getOSNameById(activeOSID.ToString());
                }
                // Brand
                else
                {
                    activeBrandID = int.Parse(idStr.Split('-').Last());
                    activeBrandName = PhongCachMobile.controller.CategoryController.getCategoryNameById(activeBrandID.ToString());
                }

                hideBanners = true;
            }

            if (!this.IsPostBack)
            {
                loadBannerItems();
                loadWeekHotProducts();
                loadAds();
                loadNewPhoneList();
                loadFilterValues();
                loadFilterPhoneList();

                loadPromotion();
                loadMarketNews();
            }
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "20"));

        // Hide Banner
        protected bool hideBanners = false;

        // Active Brand
        protected int activeBrandID = -1;
        protected string activeBrandName = "Tất cả";
        // Active OS
        protected int activeOSID = -1;
        protected string activeOSName = "Tất cả";

        protected List<Article> topBannerLeftItems = new List<Article>();
        protected List<Article> topBannerRightItem1s = new List<Article>();
        protected List<Article> topBannerRightItem2s = new List<Article>();
        private void loadBannerItems()
        {
            // DTDD_Top_Banner_Left_Items // V2-Phone - Sliders Left
            foreach (DataRow dr in SliderController.getSliders("56").Rows)
            {
                topBannerLeftItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // DTDD_Top_Banner_Right_Items1 // V2-Phone - Sliders Right 1
            foreach (DataRow dr in SliderController.getSliders("57").Rows)
            {
                topBannerRightItem1s.Add(SliderController.rowToSlider(dr).toArticle());
            }
            // DTDD_Top_Banner_Right_Items2 // V2-Phone - Sliders Right 2
            foreach (DataRow dr in SliderController.getSliders("58").Rows)
            {
                topBannerRightItem2s.Add(SliderController.rowToSlider(dr).toArticle());
            }

            ucTripleBanner.DataBind();
        }

        protected List<Product> weekHotProducts = new List<Product>();
        private void loadWeekHotProducts()
        {
            int soTin = int.Parse(SysIniController.getSetting("DTDD_So_SP_Quan_Tam", "8"));
            DataTable dt = ProductController.getPrdByWeeklyCountDesc(soTin, ProductController.ProductGroupPhone);
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);
                weekHotProducts.Add(product);
            }

            ucWeekHotProducts.DataBind();
        }

        protected List<Article> newPhoneAds = new List<Article>();
        private void loadAds()
        {
            // DTDD_Ads_Banner_Items // V2-Phone - Banners
            foreach (DataRow dr in SliderController.getSliders("62").Rows)
            {
                newPhoneAds.Add(SliderController.rowToSlider(dr).toArticle());
            }
        }

        protected List<Product> newPhoneList = new List<Product>();
        private void loadNewPhoneList()
        {
            int soTin = int.Parse(SysIniController.getSetting("DTDD_So_SP_Moi", "4"));
            DataTable dt = ProductController.getNew(soTin, ProductController.ProductGroupPhone);
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);
                newPhoneList.Add(product);
            }

            ucNewPhoneList.DataBind();
        }

        protected List<Article> promotionArticles = new List<Article>();
        private void loadPromotion()
        {
            int soTin = int.Parse(SysIniController.getSetting("Gia_Hot_So_Tin_Hot", "10"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(soTin, "22", "56");
            foreach (DataRow row in dt.Rows)
            {
                var article = ArticleController.rowToSimpleArticle(row);
                article.title = article.title.Replace("\"", "\\\"");
                promotionArticles.Add(article);
            }
        }

        protected List<Article> marketNews = new List<Article>();
        private void loadMarketNews()
        {
            int soTin = int.Parse(SysIniController.getSetting("Gia_Hot_So_Tin_Thi_Truong", "10"));
            //DataTable dt = ArticleController.getArticlesOrderByPostedDate(soTin, "22", "55");
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(soTin, "22", "56");
            foreach (DataRow row in dt.Rows)
            {
                var article = ArticleController.rowToSimpleArticle(row);
                article.title = article.title.Replace("\"", "\\\"");
                marketNews.Add(article);
            }
        }

        protected Dictionary<string, int> brands = new Dictionary<string, int>();
        protected Dictionary<string, int> prices = new Dictionary<string, int>();
        protected Dictionary<string, int> oses = new Dictionary<string, int>();
        protected int promotionCount = 0;

        private void loadFilterValues()
        {
            // Brands
            DataTable dt = CategoryController.getCategories(ProductController.ProductGroupPhone, "");
            if (dt != null && dt.Rows.Count > 0)
            {
                brands.Add("Tất cả", -1);
                foreach (DataRow dr in dt.Rows)
                {
                    brands.Add(dr["Name"].ToString(), int.Parse(dr["Id"].ToString()));
                }
            }

            // Prices
            prices = ProductController.PriceList;

            // OSes
            dt = OperatingSystemController.getOperatingSystemsByGroupId("3");
            if (dt != null && dt.Rows.Count > 0)
            {
                oses.Add("Tất cả", -1);
                foreach (DataRow dr in dt.Rows)
                {
                    oses.Add(dr["Name"].ToString(), int.Parse(dr["Id"].ToString()));
                }
            }

            promotionCount = ProductController.getDiscountProductCount(ProductController.ProductGroupPhoneInt);
        }

        protected int remainCount = 0;
        protected List<Product> filterPhoneList = new List<Product>();
        private void loadFilterPhoneList()
        {
            DataTable dt = ProductController.getProductsByGroupId(
                ProductController.ProductGroupPhoneInt, activeBrandID, -1, activeOSID, false, true, out remainCount);
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);
                filterPhoneList.Add(product);
            }

            ucFilterPhoneList.DataBind();
        }
    }
}