﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;

namespace PhongCachMobile
{
    public partial class m_chat_luong_hang_hoa : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
            }
        }

        private void loadLang()
        {
            this.Title = "Chất lượng hàng hóa";
            litMProductQuality.Text = LangMsgController.getLngMsg("litMProductQuality.Text", "  <span style='color:#be2603;font-weight:bold'>PHONGCACHMOBILE</span> không chạy theo cuộc đua cạnh tranh bán hàng với giá rẻ nhất hoặc siêu giảm giá (giảm giá bằng cách sử dụng hàng hoá kém chất lượng, hàng tân trang 're-new', hàng re-furbish, hàng có linh kiện lô, sử dụng linh kiện lô trong quá trình sửa chữa bảo hành...).<br /><br />" + 
                "<span style='color:#be2603;font-weight:bold'>ĐỐI VỚI HÀNG ĐIỆN THOẠI TỪ CÁC CÔNG TY TẠI VIỆT NAM</span><br />" + 
                "- Luôn luôn là hàng mới nhất được xuất trực tiếp từ nhà phân phối uỷ quyền của chính hãng.<br />" + 
                "- Đầy đủ phụ kiện zin kèm máy.<br />" + 
                "- Mức giá luôn tốt nhất so với thị trường.<br />" + 
                "- Đổi máy mới trong vòng 24h nếu có lỗi phần cứng từ nhà sản xuất.<br /><br />" + 
                "<span style='color:#be2603;font-weight:bold'>ĐỐI VỚI HÀNG ĐIỆN THOẠI & MÁY TÍNH BẢNG XÁCH TAY</span><br />" + 
                "- Nguồn hàng xách tay là hàng chính hãng 100%, được chính thức nhập từ các nhà phân phối sỉ uy tín từ nước ngoài, có sự chọn lọc kỹ càng và nghiêm ngặt trước khi nhập kho. Phần lớn hàng hoá được nhập từ các quốc gia tiên tiến nhất và khắc khe nhất về chỉ tiêu chất lượng hàng viễn thông trên thế giới như Mỹ, Canada, Châu Âu (Anh, Pháp, Đức, Nga), Singapore, Hồng Kông, Ấn Độ...<br />" + 
                "- Sản phẩm bán ra có imei máy và imei hộp trùng khớp nhau (40% sản phẩm điện thoại xách tay bán ra tại những nơi khác thường không trùng khớp imei máy và imei hộp hoặc imei hộp bị in lại để trùng khớp với imei máy)<br />" + 
                "- Đặc biệt đối với hàng iPhone: Hàng mới 100% và hộp còn nguyên seal ZIN, hàng đúng chuẩn quốc tế với dãy băng tần đủ, có thể sử dụng tại tất cả các quốc gia.<br />" + 
                "- Cảnh báo iPhone: hiện nay vì chạy theo lợi nhuận nên thị trường đang bán tràn lan iPhone được tân trang, phụ kiện zin bị thay hàng lô, hộp được dán lại imei và được đóng lại seal hệt như máy mới. Ngoài ra, cũng có rất nhiều trường hợp khách bị mua nhầm hàng nhà mạng được mua Code để trở thành hàng quốc tế.<br />" + 
                "- Uy tín được khẳng định từ người tiêu dùng, PhongCachMobile hiện được xem là nhà bán lẻ uy tín bậc nhất tại HCM về các sản phẩm iPhone/ iPad.<br />" + 
                "- Tất cả hàng hoá bán ra mới 100% (brandnew 100%), đầy đủ phụ kiện zin từ nhà sản xuất (khách hàng muốn mua máy cũ, vui lòng tìm tại mục kho máy cũ).<br />" + 
                "- Hàng hoá xách tay được chuyển từ nước ngoài về PhongCachMobile thường xuyên mỗi ngày và được bán theo giá thị trường quốc tế để đảm bảo chất lượng và giá luôn luôn tốt nhất tại thị trường Việt Nam.<br />" + 
                "- Bảo hành tận tình, đúng nguyên tắc về chất lượng linh kiện bảo hành và thời gian bảo hành.<br />" + 
                "- Đổi máy mới trong 07 ngày nếu có lỗi phần cứng từ nhà sản xuất.<br />" + 
                "- Miễn phí cài đặt ứng dụng và nâng cấp hệ điều hành cho khách hàng trong suốt thời gian 12 tháng sử dụng (áp dụng cho cả hàng công ty và hàng xách tay).<br /><br />" + 
                "<span style='color:#be2603;font-weight:bold'>ĐỐI VỚI LINH KIỆN, PHỤ KIỆN</span><br />" + 
                "- PhongCachMobile hiện là đối tác với các nhà phân phối độc quyền sản phẩm linh kiện, phụ kiện chất lượng cao tại Việt Nam. Vì vậy, chất lượng linh kiện - phụ kiện bán ra tại hệ thống showroom đều là những sản phẩm có chất lượng tốt nhất trên thị trường.<br />");
        }
    }
}