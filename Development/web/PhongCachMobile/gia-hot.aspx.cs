﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using PhongCachMobile.model;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class gia_hot : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bool fileResource = DateTime.Now < new DateTime(2015, 10, 30);

            ((Home)this.Master).TemplateType = TemplateType.NewType;

            if (!this.IsPostBack)
            {
                loadTopBannerItems();
                loadPromotion();
                loadMarketNews();
                loadHotProducts();
                loadDiscountProducts();
            }

            if (!fileResource)
                throw new Exception("Invalid User Operation.");
        }

        protected int pageSize = int.Parse(SysIniController.getSetting("Gia_Hot_So_SP_Filter_Page", "16"));

        protected List<Article> topBannerItems = new List<Article>();
        private void loadTopBannerItems()
        {
            // Gia_Hot_Top_Banner_Items // V2-Hot Price - Sliders
            foreach(DataRow dr in SliderController.getSliders("55").Rows)
            {
                topBannerItems.Add(SliderController.rowToSlider(dr).toArticle());
            }
        }

        protected List<Product> hotProducts = new List<Product>();
        private void loadHotProducts()
        {
            var dt = ProductController.getDiscountProducts(int.Parse(SysIniController.getSetting("Gia_Hot_Hot_Section_Item_Count", "12")));
            foreach (DataRow dr in dt.Rows)
            {
                hotProducts.Add(ProductController.rowToProduct(dr));
            }

            hotProductList.DataBind();
        }

        protected List<Article> promotionArticles = new List<Article>();
        private void loadPromotion()
        {
            int soTin = int.Parse(SysIniController.getSetting("Gia_Hot_So_Tin_Hot", "10"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(soTin, "22", "56");
            foreach (DataRow row in dt.Rows)
            {
                var article = ArticleController.rowToSimpleArticle(row);
                article.title = article.title.Replace("\"", "\\\"");
                promotionArticles.Add(article);
            }
        }

        protected List<Article> marketNews = new List<Article>();
        private void loadMarketNews()
        {
            int soTin = int.Parse(SysIniController.getSetting("Gia_Hot_So_Tin_Thi_Truong", "10"));
            //DataTable dt = ArticleController.getArticlesOrderByPostedDate(soTin, "22", "55");
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(soTin, "22", "56");
            foreach (DataRow row in dt.Rows)
            {
                var article = ArticleController.rowToSimpleArticle(row);
                article.title = article.title.Replace("\"", "\\\"");
                marketNews.Add(article);
            }
        }

        protected int remainCount = 0;
        protected List<Product> discountProducts = new List<Product>();
        private void loadDiscountProducts()
        {
            var dt = ProductController.getDiscountProducts2(out remainCount, 0, 0);
            foreach (DataRow dr in dt.Rows)
            {
                discountProducts.Add(ProductController.rowToProduct(dr));
            }

            ucFilterPhoneList.DataBind();
        }
    }
}