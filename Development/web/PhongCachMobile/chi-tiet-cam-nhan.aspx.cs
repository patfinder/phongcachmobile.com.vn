﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PhongCachMobile.controller;
using PhongCachMobile.util;

namespace PhongCachMobile
{
    public partial class chi_tiet_cam_nhan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                loadLang();
                loadTestimonialInfo();
                loadOtherTestimonial();
            }
        }

        private void loadTestimonialInfo()
        {
            string title = "";
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);

                DataTable dt = TestimonialController.getTestimonialById(id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    litName.Text = title =  dr["Name"].ToString();
                    litShortDesc.Text = dr["ShortDesc"].ToString();
                    litValue.Text = dr["Text"].ToString();
                }

            }

            this.Title += " - " + title;
        }

        private void loadOtherTestimonial()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string id = Request.QueryString["id"].ToString();
                id = id.Substring(id.LastIndexOf('-') + 1, id.Length - id.LastIndexOf('-') - 1);

                DataTable dt = TestimonialController.getTestimonial(3, id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.Columns.Add("UrlName", typeof(string));
                    foreach (DataRow dr in dt.Rows)
                    {
                        dr["UrlName"] = "khach-hang-" + HTMLRemoval.StripTagsRegex(dr["Name"].ToString()).ToLower().Replace(' ', '-') + "-" + dr["Id"].ToString();
                        string text = HTMLRemoval.StripTagsRegex(dr["Text"].ToString());
                        dr["Text"] = (text.Length > 200 ? text.Substring(0, 200) + "..." : text);
                    }
                    lstTestimonial.DataSource = dt;
                    lstTestimonial.DataBind();
                }

            }
        }

        private void loadLang()
        {
            litTestimonial.Text = this.Title = LangController.getLng("litTestimonial.Text", "Cảm nhận từ khách khi mua hàng tại PhongCachMobile");

        }
    }
}