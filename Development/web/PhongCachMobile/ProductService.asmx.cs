﻿using Newtonsoft.Json;
using PhongCachMobile.controller;
using PhongCachMobile.model;
using PhongCachMobile.util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace PhongCachMobile
{
    /// <summary>
    /// Summary description for ProductService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ProductService : System.Web.Services.WebService
    {
        protected string ProductToPattern(Product product, bool hideGiaHot = false)
        {
            return

                "    <li class='" + (product.isOutOfStock ? "over-product" : "") + " " + (product.isComingSoon ? "comming-product" : "") + "'> " +
                "        " + (product.isOutOfStock ? "<img src='images/icon-het.png' class='icon-hethang'> " : "") +
                "        " + (product.isComingSoon ? "<img src='images/icon-sapco.png' class='icon-sapco'> " : "")  +
                "        <figure> " +
                "            <a href='" + product.link + "' class='img-box'><img src='images/" + (product.oldProduct ? "oldproduct" : "product") + "/" + product.firstImage + "' alt=''> " +
                "                <div class='group'> " +
                "                    " + (product.isGifted ? "<span class='gift'></span>" : "") +
                "                    " + (product.discountPrice > 0 && !hideGiaHot ? "<span class='hot'></span>" : "") +
                "                </div> " +
                "            </a> " +
                "            <figcaption> " +
                "                <h3><a href='" + product.link + "'>" + product.name + "</a></h3> " +
                "                <span class='price'>" + product.currentPriceStr + "</span> " +
                "                " + (product.discountPrice != 0 ? "<span class='price-regular'><strike>(" + product.discountPriceStr + ")</strike></span> " : "") +
                "                <p class='intro'>" + product.shortDesc + "</p> " +
                "            </figcaption> " +
                "        </figure> " +
                "        <a href='" + product.link + "' class='read-more'>Xem chi tiết</a> " +
                "    </li> ";
        }

        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetDiscountProducts(string page)
        {
            return DoGetDiscountProducts(page);
        }

        protected string DoGetDiscountProducts(string page)
        {
            int iPage = string.IsNullOrEmpty(page) ? 0 : int.Parse(page);
            if (iPage < 0)
                iPage = 0;

            int remainCount;
            DataTable dt = ProductController.getDiscountProducts2(out remainCount, iPage);

            string result = "";
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);

                result += ProductToPattern(product, true);
            }

            return JsonConvert.SerializeObject(new { remainCount = remainCount, items = result });
        }

        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetProducts(string group, string brand, string price, string os, string promotion, string sortPrice, string page)
        {
            return DoGetProducts(group, brand, price, os, promotion, sortPrice, page);
        }

        //public string GetPhones(string brands)
        protected string DoGetProducts(string group, string brand, string price, string os, string promotion, string sortPrice, string page)
        {
            int iGroup = string.IsNullOrEmpty(group) ? -1 : int.Parse(group);
            int iBrand = string.IsNullOrEmpty(brand) ? -1 : int.Parse(brand);
            int iPrice = string.IsNullOrEmpty(price) ? -1 : int.Parse(price);
            int iOS = string.IsNullOrEmpty(os) ? -1 : int.Parse(os);
            bool bPromotion = string.IsNullOrEmpty(promotion) ? false : int.Parse(promotion) == 1;
            bool bSortPrice = string.IsNullOrEmpty(sortPrice) ? false : int.Parse(sortPrice) == 1;
            int iPage = string.IsNullOrEmpty(page) ? 0 : int.Parse(page);

            if (iPage < 0)
                iPage = 0;

            int iPageSize = int.Parse(SysIniController.getSetting("DTDD_So_SP_Filter_Page", "16"));
            int remainCount;
            DataTable dt = ProductController.getProductsByGroupId(
                iGroup, iBrand, iPrice, iOS, bPromotion, bSortPrice, out remainCount, iPage, iPageSize);

            string result = "";
            foreach(DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);

                result += ProductToPattern(product);
            }

            return JsonConvert.SerializeObject(new { remainCount = remainCount, items = result });
        }

        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetFeatureTablets(string featureID, string page)
        {
            int iPage = string.IsNullOrEmpty(page) ? 0 : int.Parse(page);

            if (iPage < 0)
                iPage = 0;

            DataTable dt = FeatureController.getProductByFeature(ProductController.ProductGroupTabletInt, -1, int.Parse(featureID), iPage);

            string result = "";
            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row);

                result += ProductToPattern(product);
            }

            return result;
        }

        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetOldProducts(string brand, string price, string sortPrice, string page)
        {
            return DoGetOldProducts(brand, price, sortPrice, page);
        }

        protected string DoGetOldProducts(string brand, string price, string sortPrice, string page)
        {
            int iBrand = string.IsNullOrEmpty(brand) ? -1 : int.Parse(brand);
            int iPrice = string.IsNullOrEmpty(price) ? -1 : int.Parse(price);
            int iPage = string.IsNullOrEmpty(page) ? 0 : int.Parse(page);
            bool bSortPrice = false;
            if(string.IsNullOrEmpty(sortPrice))
                bSortPrice = false;
            else if (sortPrice == "0" || sortPrice == "1")
                bSortPrice = int.Parse(sortPrice) == 1;
            else
                bSortPrice = bool.Parse(sortPrice);

            if (iPage < 0)
                iPage = 0;
            int iPageSize = int.Parse(SysIniController.getSetting("Kho_May_Cu_So_SP_1_Trang", "16"));

            string result = "";

            // Get tab category ID for current brand
            List<int> iBrands = new List<int>();
            foreach(DataRow dr in CategoryController.getSameBrandCategories(iBrand).Rows)
            {
                iBrands.Add((int)dr["id"]);
            }

            int remainCount;
            DataTable dt = OldProductController.getOldProducts(iBrands, iPrice, bSortPrice, out remainCount, iPage, iPageSize);

            foreach (DataRow row in dt.Rows)
            {
                var product = ProductController.rowToProduct(row, true);
                product.shortDesc = HTMLRemoval.StripTagsRegexCompiled(product.shortDesc);

                result += ProductToPattern(product);
            }

            return JsonConvert.SerializeObject(new { remainCount = remainCount, items = result });
        }
    }
}
