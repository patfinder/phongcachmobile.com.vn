﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PhongCachMobile.controller;
using System.Data;
using PhongCachMobile.util;
using System.Web.UI.HtmlControls;
using PhongCachMobile.master;

namespace PhongCachMobile
{
    public partial class loai_game : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ((PhongCachMobile.master.Home)this.Master).TemplateType = TemplateType.NewType;

            if (!this.IsPostBack)
            {
                loadOS();
                loadNews();
                loadSidebarBanner();
                loadLang();
            }

            loadTitle();
        }

        private void loadLang()
        {
            litAdvantage.Text = LangMsgController.getLngMsg("litAdvantage.Text", "<p>MUA MÁY TẠI PHONGCACHMOBILE</p>" + 
                                                   "Khách hàng được tặng phiếu cài đặt miễn phí Games - Ứng dụng bản quyền <br /><br />Nâng cấp firmware /hệ điều hành trị giá " + 
                                                   "<font style='color:red'>1.250.000đ</font>" + 
                                                   "<br /><br />" + 
                                                   "<p>MUA MÁY BÊN NGOÀI</p>" + 
                                                   "Đơn giá: 150.000Đ / 1 lần cài đặt và khách hàng tự do tùy chọn Games và Ứng dụng yêu thích");
        }

        private void loadSidebarBanner()
        {
            //Phone
            DataTable dt = SliderController.getSliders("36");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litPhone.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Tablet
            dt = SliderController.getSliders("37");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litTablet.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Accessory
            dt = SliderController.getSliders("38");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litAccessory.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Application
            dt = SliderController.getSliders("39");
            if (dt != null && dt.Rows.Count > 0)
            {
                int index = DateTime.Now.Second % dt.Rows.Count;
                litApplication.Text = "<a href='" + dt.Rows[index]["ActionURL"].ToString() + "'><img src='images/banner/" + dt.Rows[index]["DisplayImage"].ToString() + "' /></a>"; 
            }

            //Game
            //dt = SliderController.getSliders("40");
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    int index = DateTime.Now.Second % dt.Rows.Count;
            //    imgGame.ImageUrl = "images/slide/" + dt.Rows[index]["DisplayImage"].ToString();
            //}
        }

        private void loadNews()
        {
            int number = int.Parse(SysIniController.getSetting("Number_Article_Home", "5"));
            DataTable dt = ArticleController.getArticlesOrderByPostedDate(number, "22", "56");
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("UrlTitle", typeof(string));
                dt.Columns.Add("TimeSpan", typeof(string));
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UrlTitle"] = HTMLRemoval.StripTagsRegex(dr["Title"].ToString()).ToLower().Replace(' ', '-');

                    //DateTime endtime = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss");
                    //TimeSpan ts = DateTime.Now - endtime;
                    //if (ts.Days == 0)
                    //    dr["TimeSpan"] = " cách đây " + ts.Hours + " giờ.";
                    //else
                    //    dr["TimeSpan"] = " cách đây " + ts.Days + " ngày";
                }
                lstNews.DataSource = dt;
                lstNews.DataBind();
            }
        }

        private void loadOS()
        {
            DataTable dt = OperatingSystemController.getOperatingSystemsByGroupId("5");
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlOS.Items.Clear();
                ddlOS.Items.Add(new ListItem("Tất cả", "-1"));
                foreach (DataRow dr in dt.Rows)
                {
                    ddlOS.Items.Add(new ListItem(dr["Name"].ToString(), dr["Id"].ToString()));
                }
                ddlOS.DataBind();
            }
        }

        private void loadTitle()
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];
              
                litMobile.Text = this.Title = "Game " + CategoryController.getCategoryNameById(id);
            }
            else
                litMobile.Text = this.Title = "Game điện thoại di động";
        }

        private void loadGamesByCategory(int categoryId)
        {
            DataTable dt = null;
            if (ddlTopSort.SelectedValue.Equals("posteddate", StringComparison.CurrentCultureIgnoreCase))
                dt = GameController.getGamesByCategory(categoryId);
            else if (ddlTopSort.SelectedValue.Equals("viewcount", StringComparison.CurrentCultureIgnoreCase))
                dt = GameController.getViewCountGamesByCategory(categoryId);
            //else if (ddlTopSort.SelectedValue.Equals("downloadcount", StringComparison.CurrentCultureIgnoreCase))
            //    dt = GameController.getDownloadCountGamesByCategory(categoryId);

            EnumerableRowCollection<DataRow> temp = null;
            DataTable dtNew = dt;

            //OS filter
            if (!ddlOS.SelectedValue.Equals("-1"))
            {
                temp = dtNew.AsEnumerable().Where(row => row.Field<int>("OSId") == int.Parse(ddlOS.SelectedValue));
                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }

            

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("Free", typeof(string));
                dtNew.Columns.Add("Token", typeof(string));
                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["Free"] = "<span class='free'>Miễn phí &nbsp;</span>";
                    dr["Token"] = dr["Filename"].ToString();
                    dr["PostedDate"] = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss").ToShortDateString();
                }
            }
            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }

        protected void dpgLstItem_PreRender(object sender, EventArgs e)
        {
            if (lstItem.DataSource == null)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
                {
                    string sId = Request.QueryString["id"].ToString();
                    string[] ids = sId.Split('-');
                    string id = ids[ids.Length - 1];

                    loadGamesByCategory(int.Parse(id));
                }
                else
                {
                    loadGamesByCategory();
                }

                int currentPage = ((dpgLstItemTop.StartRowIndex) / dpgLstItemTop.MaximumRows); // -1
                DataTable dt = (DataTable)lstItem.DataSource;
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = currentPage * dpgLstItemTop.PageSize; i < (currentPage +1)*dpgLstItemTop.PageSize && i <dt.Rows.Count; i++)
                    {
                        DataRow dr = dt.Rows[i];
                        if (dr != null)
                        {
                            GameController.addViewCount(dr["Id"].ToString());
                            dr["ViewCount"] = int.Parse(dr["ViewCount"].ToString()) + 1;
                        }
                    }
                }

            }

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
                " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount;

        }


        private void loadGamesByCategory()
        {
            DataTable dt = null;
            if (ddlTopSort.SelectedValue.Equals("posteddate", StringComparison.CurrentCultureIgnoreCase))
                dt = GameController.getGamesByCategory(-1);
            else if (ddlTopSort.SelectedValue.Equals("viewcount", StringComparison.CurrentCultureIgnoreCase))
                dt = GameController.getViewCountGamesByCategory(-1);
            //else if (ddlTopSort.SelectedValue.Equals("downloadcount", StringComparison.CurrentCultureIgnoreCase))
            //    dt = GameController.getDownloadCountGamesByCategory(-1);

            EnumerableRowCollection<DataRow> temp = null;
            DataTable dtNew = dt;

            //OS filter
            if (!ddlOS.SelectedValue.Equals("-1"))
            {
                temp = dtNew.AsEnumerable().Where(row => row.Field<int>("OSId") == int.Parse(ddlOS.SelectedValue));
                if (temp != null && temp.Count() > 0)
                    dtNew = temp.CopyToDataTable();
                else
                    dtNew = null;
            }

            if (dtNew != null && dtNew.Rows.Count > 0)
            {
                dtNew.Columns.Add("Free", typeof(string));
                dtNew.Columns.Add("Token", typeof(string));
                foreach (DataRow dr in dtNew.Rows)
                {
                    dr["Free"] = "<span class='free'>Miễn phí</span>";
                    dr["Token"] = dr["Filename"].ToString();
                    dr["PostedDate"] = Common.parseDate(dr["PostedDate"].ToString(), "yyyyMMddHHmmss").ToShortDateString();
                }
            }
            lstItem.DataSource = dtNew;
            lstItem.DataBind();
        }

        protected void ddlTopDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);
            //dpgLstItemTop.StartRowIndex =dpgLstItemBot. = 0;
            ddlBotDisplay.SelectedIndex = ddlTopDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));

            }
            else
            {
                loadGamesByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
               " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount;
        }

        protected void ddlBotDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            dpgLstItemBot.PageSize = dpgLstItemTop.PageSize = int.Parse(ddlTopDisplay.SelectedValue);

            ddlTopDisplay.SelectedIndex = ddlBotDisplay.SelectedIndex;
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));
            }
            else
            {
                loadGamesByCategory();

            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
                " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
                dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
                " trong " + dpgLstItemTop.TotalRowCount;
        }

        protected void ddlTopSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBotSort.SelectedIndex = ddlTopSort.SelectedIndex;

            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));
            }
            else
            {
                loadGamesByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
               " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount;
        }

        protected void ddlBotSort_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlTopSort.SelectedIndex = ddlBotSort.SelectedIndex;

            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));
            }
            else
            {
                loadGamesByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
               " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount;
        }

        protected void ddlOS_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));

            }
            else
            {
                loadGamesByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
               " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount;
        }

        protected void ddlFeatures_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToString() != "")
            {
                string sId = Request.QueryString["id"].ToString();
                string[] ids = sId.Split('-');
                string id = ids[ids.Length - 1];

                loadGamesByCategory(int.Parse(id));
            }
            else
            {
                loadGamesByCategory();
            }

            dpgLstItemTop.SetPageProperties(0, dpgLstItemTop.PageSize, true);
            dpgLstItemBot.SetPageProperties(0, dpgLstItemTop.PageSize, true);

            litTopItemsIndex.Text = litBotItemsIndex.Text = (dpgLstItemTop.StartRowIndex + 1) +
               " - " + (dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize < dpgLstItemTop.TotalRowCount ?
               dpgLstItemTop.StartRowIndex + dpgLstItemTop.PageSize : dpgLstItemTop.TotalRowCount) +
               " trong " + dpgLstItemTop.TotalRowCount;
        }

        protected void lstItem_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            HyperLink hplDownload = (HyperLink)e.Item.FindControl("hplDownload");
            HiddenField hdfName = (HiddenField)e.Item.FindControl("hdfName");
            HiddenField hdfId = (HiddenField)e.Item.FindControl("hdfId");

            if (SysIniController.getSetting("IsGameAppDownloadable", "true") == "true")
            {
                if (Session["Email"] != null && Session["Email"].ToString() != "")
                {
                    if (hdfName.Value != "#" && hdfName.Value != "")
                        hplDownload.NavigateUrl = "game/" + hdfName.Value;
                    else
                    {
                        hplDownload.NavigateUrl = "#";
                        hplDownload.Text = "Cài đặt tại cửa hàng &nbsp; ";
                    }
                }
                else
                {
                    hplDownload.NavigateUrl = "dang-nhap";
                }
            }
            else
            {
                hplDownload.Visible = false;
            }
        }

      
    }
}