﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="phu-kien-sub.aspx.cs" Inherits="PhongCachMobile.phu_kien_sub" %>

<%@ Register Src="~/master/uc/ucTripleBanner.ascx" TagName="ucTripleBanner" TagPrefix="ucTripleBanner" %>
<%@ Register Src="~/master/uc/ucBrandsLogoList.ascx" TagName="ucBrandsLogoList" TagPrefix="ucBrandsLogoList" %>
<%@ Register Src="~/master/uc/ucProductListBare.ascx" TagName="ucProductListBare" TagPrefix="ucProductListBare" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/tm3DCircleCarousel.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="css/category.css" rel="stylesheet" type="text/css" />
    <script src="js/tms-0.3.js" type="text/javascript"></script>
    <script src="js/tms_presets.js" type="text/javascript"></script>
    <script src="js/jquery.cycle.all.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
  <script src="js/tm3DCircleCarousel.js" type="text/javascript"></script>
     <script type="text/javascript">
         $(window).load(function () {
             $('.slider')._TMS({
                 prevBu: '.prev',
                 nextBu: '.next',
                 playBu: '.play',
                 duration: 700,
                 easing: 'easeOutQuad',
                 preset: 'simpleFade',
                 pagination: true,
                 //pagNums:false,
                 slideshow: 8000,
                 numStatus: false,
                 banners: 'fade', // fromLeft, fromRight, fromTop, fromBottom
                 waitBannerAnimation: false
             })
         })
    </script>
    <script type="text/javascript">
        jQuery('#slideshow').cycle({
            fx: 'scrollHorz',
            speed: 'fast',
            pager: '#pagination1',
            timeout: 8000,
            speed: 700,
            cleartype: true,
            cleartypeNoBg: true
        });
    </script>
      <script type="text/javascript">
          function tabPrd() {
              jQuery('#tabAcs').hide("slow");
              jQuery('#tabPrd').show("slow");
          }

          function tabAcs() {
              jQuery('#tabAcs').show("slow");
              jQuery('#tabPrd').hide("slow");
          }

  </script>

    <script type="text/javascript">

        var win = $(window),
    doc = $(document),
    $splashGallery;

        function initCarousel() {
            // init carousel
            $splashGallery = $('.splash');
            $splashGallery
        .tooltip({
            track: true
        })
        .tm3DCircleCarousel({
            container: '.splashHolder',
            transformClasses: '.scale100, .scale90, .scale80, .scale70, .scale60, .scale50',
            clickableClasses: '.scale100, .scale90',
            itemOffset: 108,//328
            itemOffsetCenter: 78, //78
            useCSS3Animation: true,
            autoplay: {
                enable: true,
                timeout: 3000
            },
            onChange: function (element, currInd, length) {
            },
            onShowActions: function (e) {
                $splashGallery.tooltip('enable');
            },
            onHideActions: function (e) {
                $splashGallery.tooltip('enable');
            },
            onUserActivate: function (e) {
                $splashGallery.tooltip('disable');
            }
        });
        }


        /*---------------------- end ready -------------------------------*/

        win
.load(function () {
    initCarousel();
});

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="content">

        <% if(!hideBanners) { %>
        <ucTripleBanner:ucTripleBanner ID="ucTripleBanner" bannerLeftItems="<%# topBannerLeftItems %>" 
            bannerRightItem1s="<%# topBannerRightItem1s %>" bannerRightItem2s="<%# topBannerRightItem2s %>" runat="server" />

        <ucBrandsLogoList:ucBrandsLogoList ID="idUCBrandsLogoList" runat="server" />
        <% } %>

        <div class="product-list">
            <div class="product-other list-5-items content-center">
                <div class="filter">
                    <div  class="label"><span>Tìm theo: </span></div>
                    <div class="dropdown" id="ddPrice">
                        <a data-target="#" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Mức giá</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <% foreach(KeyValuePair<string, int> kv in prices) { %>
                            <li><a href="javascript:;" value="<%= kv.Value %>"><%= kv.Key %></a></li>
                            <% } %>
                        </ul>
                    </div>
                    <div class="promotion" id="ckPromotion">
                        <i class="fa fa-circle"></i> <span>KHUYẾN MÃI (<%= promotionCount %>)</span>
                    </div>
                    <div class="dropdown sort" id="ddSortPrice">
                        <a data-target="#" value="1" href="javascript:;" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span>Giá từ Cao đến Thấp</span>
                            <i class="fa fa-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li><a href="javascript:;" value="0">Giá từ Thấp đến Cao</a></li>
                        </ul>
                    </div>
                </div>
                <ul>

                    <ucProductListBare:ucProductListBare ID="ucFilterPhoneList" ProductList="<%# filterPhoneList %>" runat="server" />

                </ul>
                <a href="javascript:;" class="read-more">Xem thêm <span class="number"><%= remainCount >= pageSize ? pageSize : remainCount %></span> sản phẩm</a>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="foot" runat="server">
        
    <% // ---------------------------- Footer ---------------------------- %>

    <script type="text/javascript">

        var pageSize = <%= pageSize %>;

        $(document).ready(function () {

            window.filterIDs = {ddPrice: "price", ddSortPrice: "sortPrice"};

            // Apply filters handling
            applyFilterHandling("ddPrice", false, function() {
                loadMoreProducts("GetProducts", true, pageSize);
            });
            applyFilterHandling("ddSortPrice", true, function() {
                loadMoreProducts("GetProducts", true, pageSize);
            });

            // Promotion
            $("div#ckPromotion").on("click", function () {
                paramValues.promotion = 1 - $(this).hasClass("active"); // toggle 0-1
                $(this).toggleClass("active");
                console.log("paramValues.promotion: " + paramValues.promotion);

                loadMoreProducts("GetProducts", true, pageSize);
            });

            $(".read-more").on("click", function () {
                loadMoreProducts("GetProducts", false, pageSize);
            });
        });

        var paramValues = { group: 7, brand: <%= activeCategoryID %>, price: -1, os: -1, promotion: 0, sortPrice: 1, page: 0 };

    </script>

    <script type="text/javascript">
    $(document).ready(function () {
        $('.slide-banner').slick({
            slidesToShow: 1,
            variableWidth: false,
            autoplay: true,
            autoplaySpeed: 7000
        });
    });

    </script>

</asp:Content>
