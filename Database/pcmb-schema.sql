/****** Object:  Table [dbo].[ACCESSORYSECTION]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACCESSORYSECTION](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Categories] [nvarchar](200) NOT NULL,
	[Image] [nvarchar](200) NOT NULL,
	[Link] [nvarchar](500) NULL,
	[ShowItemCount] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ADS]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ADS](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[StartTime] [char](14) NULL,
	[EndTime] [char](14) NULL,
	[GroupId] [int] NOT NULL,
	[Type] [varchar](10) NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
	[ActionURL] [varchar](50) NULL,
	[Filename] [varchar](100) NULL,
	[Default] [varchar](100) NULL,
	[Display] [bit] NULL,
 CONSTRAINT [PK_ADS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ANSWER]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ANSWER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Description] [nvarchar](1000) NULL,
	[LikeCount] [int] NULL,
	[UserId] [int] NULL,
	[AskId] [int] NULL,
	[PostedDate] [char](14) NULL,
 CONSTRAINT [PK_ANSWER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[APPLICATION]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[APPLICATION](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[IsFree] [bit] NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [int] NULL,
	[LikeCount] [int] NULL,
	[ViewCount] [int] NULL,
	[DisplayImage] [nvarchar](150) NULL,
	[OSId] [int] NULL,
	[Filename] [nvarchar](150) NULL,
	[Filesize] [nvarchar](20) NULL,
	[PostedDate] [char](14) NULL,
	[RequiredOS] [nvarchar](50) NULL,
	[CurrentVersion] [nvarchar](50) NULL,
	[Developer] [nvarchar](50) NULL,
 CONSTRAINT [PK_APPLICATION] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ARTICLE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ARTICLE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[ShortDesc] [nvarchar](max) NULL,
	[LongDesc] [nvarchar](max) NULL,
	[DisplayImage] [varchar](150) NULL,
	[ViewCount] [int] NULL,
	[Flag] [bit] NULL,
	[PostedDate] [char](14) NULL,
	[GroupId] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[UserId] [int] NULL,
	[ProductId] [int] NULL,
 CONSTRAINT [PK_ARTICLE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ASK]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ASK](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ask] [nvarchar](1000) NULL,
	[UserId] [int] NULL,
	[PostedDate] [varchar](14) NULL,
	[ViewCount] [int] NULL,
	[Title] [nvarchar](200) NULL,
	[isSolved] [bit] NULL,
 CONSTRAINT [PK_ASK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CARD]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CARD](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NULL,
	[BrandNumber] [varchar](10) NULL,
	[PostedDate] [char](14) NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_CARD] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATEGORY]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATEGORY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [ntext] NULL,
	[DisplayImage] [varchar](50) NULL,
	[ParentId] [int] NULL,
	[GroupId] [int] NOT NULL,
	[InOrder] [smallint] NULL,
	[Display] [bit] NULL,
 CONSTRAINT [PK_CATEGORY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CHALLENGE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CHALLENGE](
	[Id] [int] NOT NULL,
	[GameId] [int] NOT NULL,
	[StartTime] [char](14) NOT NULL,
	[EndTime] [char](14) NOT NULL,
	[ExtraChargeTime] [char](14) NULL,
	[Charge] [real] NOT NULL,
	[ExtraCharge] [real] NULL,
	[Point] [int] NOT NULL,
	[Description] [ntext] NULL,
	[isFinished] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COLOR]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COLOR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [PK_COLOR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[COMMENT]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[COMMENT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[Description] [ntext] NULL,
	[PostedDate] [char](14) NULL,
	[LikeCount] [smallint] NULL,
	[UserId] [int] NOT NULL,
	[ProductId] [int] NULL,
	[ArticleId] [int] NULL,
	[IsApproved] [bit] NULL,
 CONSTRAINT [PK_COMMENT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CONTACTDATA]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CONTACTDATA](
	[Id] [int] NOT NULL,
	[SenderName] [nvarchar](50) NULL,
	[SenderEmail] [varchar](50) NULL,
	[Description] [ntext] NULL,
	[Subject] [nvarchar](150) NULL,
	[PostedDate] [char](14) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[COUNTRY]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COUNTRY](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CUSTOMER]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CUSTOMER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[DateBirth] [int] NULL,
	[MonthBirth] [int] NULL,
	[Phone] [varchar](20) NULL,
 CONSTRAINT [PK_CUSTOMER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EMOTION]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EMOTION](
	[Id] [int] NOT NULL,
	[Title] [varchar](50) NULL,
	[Value] [varchar](10) NULL,
	[DisplayImage] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FAQ]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FAQ](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ask] [ntext] NULL,
	[Answer] [ntext] NULL,
 CONSTRAINT [PK_FAQ] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FEATURE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FEATURE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_FEATURE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GAME]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GAME](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
	[IsFree] [bit] NULL,
	[Description] [nvarchar](max) NULL,
	[CategoryId] [int] NULL,
	[LikeCount] [int] NULL,
	[ViewCount] [int] NULL,
	[DisplayImage] [nvarchar](150) NULL,
	[CurrentVersion] [nvarchar](50) NULL,
	[Developer] [nvarchar](50) NULL,
	[PostedDate] [char](14) NULL,
	[RequiredOS] [nvarchar](50) NULL,
	[OSId] [int] NULL,
	[Filename] [nvarchar](150) NULL,
	[Filesize] [nvarchar](50) NULL,
 CONSTRAINT [PK_GAME] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GENRE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GENRE](
	[Id] [int] NOT NULL,
	[CategoryId] [int] NOT NULL,
	[VideoId] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GROUP]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GROUP](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Value] [varchar](50) NULL,
	[Description] [ntext] NULL,
	[Filter] [varchar](50) NULL,
	[ImagePath] [varchar](100) NULL,
 CONSTRAINT [PK_GROUP] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INVOICE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INVOICE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PostedDate] [varchar](8) NULL,
	[UserId] [int] NULL,
	[CustomerId] [int] NULL,
	[CardId] [int] NULL,
	[Point] [int] NULL,
	[StoreId] [int] NULL,
 CONSTRAINT [PK_INVOICE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[INVOICEDETAIL]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[INVOICEDETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InvContent] [nvarchar](250) NULL,
	[Price] [int] NULL,
	[PriceType] [nvarchar](100) NULL,
	[InvDetail] [nvarchar](250) NULL,
	[Receiver] [nvarchar](50) NULL,
	[WareId] [int] NULL,
	[Imei] [varchar](20) NULL,
	[OriginalPrice] [int] NULL,
	[Origin] [nvarchar](50) NULL,
	[InvoiceId] [int] NULL,
	[ColorId] [int] NULL,
	[OffTaxQuantity] [int] NULL,
	[UsedQuantity] [int] NULL,
 CONSTRAINT [PK_INVOICEDETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LANDINGPAGEDATA]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LANDINGPAGEDATA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Page] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_LANDINGPAGEDATA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LANG]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LANG](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Value] [nvarchar](255) NULL,
 CONSTRAINT [PK_LANG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LANGMSG]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LANGMSG](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_LANGMSG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LOG]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LOG](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Time] [char](14) NULL,
	[Description] [nvarchar](max) NULL,
	[Status] [smallint] NOT NULL,
	[Text] [nvarchar](max) NULL,
 CONSTRAINT [PK_LOG] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MENU]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MENU](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[GroupId] [int] NOT NULL,
	[Text] [nvarchar](50) NULL,
	[ActionUrl] [nvarchar](50) NULL,
	[DisplayImage] [nvarchar](50) NULL,
	[Flag] [bit] NULL,
	[InOrder] [smallint] NULL,
 CONSTRAINT [PK_MENU] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OLDPRODUCT]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OLDPRODUCT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[DisplayImage] [varchar](200) NULL,
	[Description] [nvarchar](max) NULL,
	[CurrentPrice] [int] NULL,
	[OriginalPrice] [int] NULL,
	[CategoryID] [int] NULL,
 CONSTRAINT [PK_OLDPRODUCT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OPERATINGSYSTEM]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OPERATINGSYSTEM](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_OPERATINGSYSTEM] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[POINT]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POINT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[Code] [varchar](10) NULL,
	[PostedDate] [char](14) NULL,
	[IsApproved] [bit] NULL,
	[CardId] [int] NULL,
	[IsUsed] [bit] NULL,
	[UsedDate] [varchar](8) NULL,
 CONSTRAINT [PK_POINT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[POLL]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[POLL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[StartDate] [char](14) NULL,
	[EndDate] [char](14) NULL,
 CONSTRAINT [PK_POLL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[POLLDETAIL]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POLLDETAIL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PollId] [int] NULL,
	[Name] [nvarchar](255) NULL,
	[Value] [int] NULL,
 CONSTRAINT [PK_POLLDETAIL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODUCT]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[FirstImage] [nvarchar](max) NULL,
	[SecondImage] [nvarchar](max) NULL,
	[ThirdImage] [nvarchar](max) NULL,
	[FourthImage] [nvarchar](max) NULL,
	[FifthImage] [nvarchar](max) NULL,
	[FirstImageDesc] [nvarchar](max) NULL,
	[SecondImageDesc] [nvarchar](max) NULL,
	[ThirdImageDesc] [nvarchar](max) NULL,
	[FourthImageDesc] [nvarchar](max) NULL,
	[FifthImageDesc] [nvarchar](max) NULL,
	[ShortDesc] [nvarchar](max) NULL,
	[LongDesc] [nvarchar](max) NULL,
	[Include] [nvarchar](max) NULL,
	[Remark] [nvarchar](max) NULL,
	[Outstanding] [nvarchar](max) NULL,
	[Flag] [bit] NULL,
	[Display] [bit] NULL,
	[ViewCount] [int] NULL,
	[Rate] [real] NULL,
	[CategoryId] [int] NOT NULL,
	[IsFeatured] [bit] NULL,
	[IsSpeedySold] [bit] NULL,
	[CurrentPrice] [int] NULL,
	[DiscountPrice] [int] NULL,
	[OSId] [int] NULL,
	[GroupId] [int] NULL,
	[Video] [nvarchar](max) NULL,
	[DiscountDesc] [nvarchar](max) NULL,
	[isCompanyProduct] [bit] NULL,
	[HotDesc] [nvarchar](max) NULL,
	[isGifted] [bit] NULL,
	[isOutOfStock] [bit] NULL,
	[isComingSoon] [bit] NULL,
	[WeeklyCount] [int] NULL,
	[isWeeklyCountUpdated] [bit] NULL,
	[suggestedAccessories] [nvarchar](max) NULL,
	[useDarkBackground] [bit] NULL,
	[backgroundImage] [nvarchar](max) NULL,
 CONSTRAINT [PK_PRODUCT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODUCTFEATURE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTFEATURE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[FeatureId] [int] NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_PRODUCTFEATUREINDEX] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODUCTGROUP]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTGROUP](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NULL,
 CONSTRAINT [PK_PRODUCTGROUP] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODUCTLINK]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRODUCTLINK](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[ProductGroupId] [int] NULL,
 CONSTRAINT [PK_PRODUCTLINK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SERVER]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SERVER](
	[Id] [int] NOT NULL,
	[ServerName] [nvarchar](50) NULL,
	[Code] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SLIDER]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SLIDER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DisplayImage] [varchar](150) NULL,
	[ThumbImage] [varchar](150) NULL,
	[Text] [nvarchar](max) NULL,
	[ActionUrl] [nvarchar](250) NULL,
	[PostedDate] [char](14) NULL,
	[Flag] [bit] NULL,
	[GroupId] [int] NULL,
	[Title] [nvarchar](250) NULL,
 CONSTRAINT [PK_SLIDER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STAFF]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[STAFF](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mobile] [varchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Flag] [bit] NULL,
	[Remark] [ntext] NULL,
	[Password] [varchar](50) NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_STAFF] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[STATE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STATE](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[STATUS]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STATUS](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_STATUS] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[STORE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STORE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_STORE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[STORE_WARE_COLOR]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[STORE_WARE_COLOR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StoreId] [int] NULL,
	[WareId] [int] NULL,
	[ColorId] [int] NULL,
	[OffTaxQuantity] [int] NULL,
	[UsedQuantity] [int] NULL,
 CONSTRAINT [PK_STORE_WARE_COLOR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SUPPORT]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUPPORT](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[YahooMessenger] [varchar](50) NULL,
	[Skype] [varchar](50) NULL,
	[GroupId] [int] NULL,
	[Phone] [varchar](50) NULL,
 CONSTRAINT [PK_SUPPORT] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYSINI]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYSINI](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Value] [varchar](150) NULL,
	[Description] [ntext] NULL,
 CONSTRAINT [PK_SYSINI] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SYSUSER]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SYSUSER](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Alias] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[LastName] [nvarchar](50) NULL,
	[Gender] [bit] NULL,
	[Address] [nvarchar](255) NULL,
	[DOB] [char](14) NULL,
	[Email] [varchar](50) NULL,
	[Mobile] [varchar](30) NULL,
	[Tel] [varchar](30) NULL,
	[Avatar] [varchar](250) NULL,
	[Flag] [bit] NULL,
	[Remark] [ntext] NULL,
	[Password] [varchar](50) NULL,
	[RegisteredDate] [char](14) NULL,
	[ActivatedToken] [varchar](50) NULL,
	[ActivatedDate] [char](14) NULL,
	[StateId] [int] NULL,
	[GroupId] [int] NULL,
	[LastRead] [char](14) NULL,
 CONSTRAINT [PK_SYSUSER] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TAG]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TAG](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TECHNICALINDEX]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TECHNICALINDEX](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[Name] [nvarchar](150) NULL,
	[Value] [nvarchar](200) NULL,
 CONSTRAINT [PK_TECHNICALINDEX] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TEMPLATE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TEMPLATE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Value] [ntext] NULL,
 CONSTRAINT [PK_TEMPLATE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TESTIMONIAL]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TESTIMONIAL](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Text] [nvarchar](max) NULL,
	[Name] [nvarchar](100) NULL,
	[PostedDate] [char](14) NULL,
	[DisplayImage] [varchar](150) NULL,
	[ShortDesc] [nvarchar](250) NULL,
 CONSTRAINT [PK_TESTIMONIAL] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VIDEO]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VIDEO](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Video] [nvarchar](50) NULL,
	[ProductId] [int] NULL,
	[Title] [nvarchar](250) NULL,
 CONSTRAINT [PK_VIDEO] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WARE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WARE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[OrderNote] [nvarchar](500) NULL,
	[Remark] [nvarchar](500) NULL,
	[StatusId] [int] NULL,
	[CategoryId] [int] NULL,
	[GroupId] [int] NULL,
 CONSTRAINT [PK_WARE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WAREHOUSE]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WAREHOUSE](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NULL,
 CONSTRAINT [PK_WAREHOUSE] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WAREHOUSE_HISTORY]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WAREHOUSE_HISTORY](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[WareId] [int] NULL,
	[ColorId] [int] NULL,
	[OffTaxQuantity] [int] NULL,
	[UsedQuantity] [int] NULL,
	[WareHouseId] [int] NULL,
	[StoreId] [int] NULL,
	[PostedDate] [char](14) NULL,
 CONSTRAINT [PK_IMPORTHISTORY] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WAREHOUSE_WARE_COLOR]    Script Date: 9/2/2015 6:06:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WAREHOUSE_WARE_COLOR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WareHouseId] [int] NULL,
	[WareId] [int] NULL,
	[ColorId] [int] NULL,
	[OffTaxQuantity] [int] NULL,
	[UsedQuantity] [int] NULL,
 CONSTRAINT [PK_WAREHOUSE_WARE_COLOR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[APPLICATION] ADD  CONSTRAINT [DF_APPLICATION_LikeCount]  DEFAULT ((0)) FOR [LikeCount]
GO
ALTER TABLE [dbo].[APPLICATION] ADD  CONSTRAINT [DF_APPLICATION_ViewCount]  DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[ASK] ADD  CONSTRAINT [DF_ASK_isSolved]  DEFAULT ((0)) FOR [isSolved]
GO
ALTER TABLE [dbo].[COMMENT] ADD  CONSTRAINT [DF_COMMENT_IsApproved]  DEFAULT ((0)) FOR [IsApproved]
GO
ALTER TABLE [dbo].[GAME] ADD  CONSTRAINT [DF_GAME_LikeCount]  DEFAULT ((0)) FOR [LikeCount]
GO
ALTER TABLE [dbo].[GAME] ADD  CONSTRAINT [DF_GAME_ViewCount]  DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[INVOICEDETAIL] ADD  CONSTRAINT [DF_INVOICEDETAIL_Price]  DEFAULT ((0)) FOR [Price]
GO
ALTER TABLE [dbo].[INVOICEDETAIL] ADD  CONSTRAINT [DF_INVOICEDETAIL_OffTaxQuantity]  DEFAULT ((0)) FOR [OffTaxQuantity]
GO
ALTER TABLE [dbo].[INVOICEDETAIL] ADD  CONSTRAINT [DF_INVOICEDETAIL_UsedQuantity]  DEFAULT ((0)) FOR [UsedQuantity]
GO
ALTER TABLE [dbo].[POINT] ADD  CONSTRAINT [DF_POINT_IsUsed]  DEFAULT ((0)) FOR [IsUsed]
GO
ALTER TABLE [dbo].[POLLDETAIL] ADD  CONSTRAINT [DF_POLLDETAIL_Value]  DEFAULT ((0)) FOR [Value]
GO
ALTER TABLE [dbo].[PRODUCT] ADD  CONSTRAINT [DF_PRODUCT_isCompanyProduct]  DEFAULT ((0)) FOR [isCompanyProduct]
GO
ALTER TABLE [dbo].[PRODUCT] ADD  CONSTRAINT [DF_PRODUCT_isGifted]  DEFAULT ((0)) FOR [isGifted]
GO
ALTER TABLE [dbo].[PRODUCT] ADD  CONSTRAINT [DF_PRODUCT_isOutOfStock]  DEFAULT ((0)) FOR [isOutOfStock]
GO
ALTER TABLE [dbo].[PRODUCT] ADD  CONSTRAINT [DF_PRODUCT_isComingSoon]  DEFAULT ((0)) FOR [isComingSoon]
GO
ALTER TABLE [dbo].[PRODUCT] ADD  CONSTRAINT [DF_PRODUCT_WeeklyCount]  DEFAULT ((0)) FOR [WeeklyCount]
GO
ALTER TABLE [dbo].[PRODUCT] ADD  CONSTRAINT [DF_PRODUCT_isWeeklyCountUpdated]  DEFAULT ((0)) FOR [isWeeklyCountUpdated]
GO
ALTER TABLE [dbo].[STORE_WARE_COLOR] ADD  CONSTRAINT [DF_STORE_WARE_COLOR_OffTaxQuantity]  DEFAULT ((0)) FOR [OffTaxQuantity]
GO
ALTER TABLE [dbo].[STORE_WARE_COLOR] ADD  CONSTRAINT [DF_STORE_WARE_COLOR_UsedQuantity]  DEFAULT ((0)) FOR [UsedQuantity]
GO
ALTER TABLE [dbo].[WARE] ADD  CONSTRAINT [DF_WARE_StatusId]  DEFAULT ((4)) FOR [StatusId]
GO
ALTER TABLE [dbo].[WAREHOUSE_WARE_COLOR] ADD  CONSTRAINT [DF_WAREHOUSE_WARE_COLOR_OffTaxQuantity]  DEFAULT ((0)) FOR [OffTaxQuantity]
GO
ALTER TABLE [dbo].[WAREHOUSE_WARE_COLOR] ADD  CONSTRAINT [DF_WAREHOUSE_WARE_COLOR_UsedQuantity]  DEFAULT ((0)) FOR [UsedQuantity]
GO
