﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="binh-luan.aspx.cs" Inherits="PhongCachMobile.admin.binh_luan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <link href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#content_txtFrom").datetimepicker({
            dateFormat: 'dd/mm/yy'
        });
        jQuery("#content_txtTo").datetimepicker({
            dateFormat: 'dd/mm/yy'
        });
    });

    function cancelAddSaveComment() {
        jQuery('#content_divAddComment').hide();
        jQuery('#content_divAddDeleteComment').show();
    }

    function check_uncheck(Val) {
        var ValChecked = Val.checked;
        var ValId = Val.id;
        var frm = document.forms[0];
        // Loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for Header Template's Checkbox
            //As we have not other control other than checkbox we just check following statement
            if (this != null) {
                if (ValId.indexOf('CheckAll') != -1) {
                    // Check if main checkbox is checked,
                    // then select or deselect datagrid checkboxes
                    //alert(frm.elements[i].id);
                    // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                    if (frm.elements[i].id.indexOf('deleteRec') != -1)
                        if (ValChecked)
                            frm.elements[i].checked = true;
                        else
                            frm.elements[i].checked = false;
                }
                else if (ValId.indexOf('deleteRec') != -1) {
                    //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                    // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                    document.getElementById('content_dgrComment_CheckAll').checked = false;
                    if (frm.elements[i].checked == false)
                        frm.elements[1].checked = false;
                }
            } // if
        } // for
    } // function</PRE>

    function confirmMsg(frm) {
        // loop through all elements
        for (i = 0; i < frm.length; i++) {
            // Look for our checkboxes only
            if (frm.elements[i].name.indexOf('deleteRec') != -1) {
                // If any are checked then confirm alert, otherwise nothing happens
                if (frm.elements[i].checked)
                    return confirm('Bạn có chắc muốn xóa?')
            }
        }
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litComment" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseComment" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseComment" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmServer">
                                 <p class="field">
                                        <asp:Literal ID="litFrom" runat="server"> </asp:Literal> <small> * </small>: &nbsp; 
                                        <asp:TextBox ID="txtFrom" runat="server" CssClass="wpcf7-text" Width="150px" MaxLength="30"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqFrom" runat="server" ControlToValidate="txtFrom"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator> &nbsp; 
                                        <asp:Literal ID="litTo" runat="server"> </asp:Literal>: &nbsp; 
                                        <asp:TextBox ID="txtTo" runat="server" CssClass="wpcf7-text" Width="150px" MaxLength="30"></asp:TextBox>
                                        &nbsp; &nbsp; 
                                        <asp:LinkButton ID="lbtFind" Font-Bold="true" runat="server" onclick="lbtFind_Click"></asp:LinkButton>
                                        </p>
                                    <asp:DataGrid ID="dgrComment" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" 
                                        oneditcommand="dgrComment_EditCommand" 
                                        onpageindexchanged="dgrComment_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="DESCRIPTION" HeaderText="Nội dung">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" 
                                                    Width="80%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                              <asp:BoundColumn DataField="Email" HeaderText="Người dùng">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                           <asp:TemplateColumn HeaderText="Liên kết">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("Target") %>' target="_blank">
                                                        ...
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật">
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAll" onclick="return check_uncheck (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRec" onclick="return check_uncheck (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" 
                                            HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                            Font-Strikeout="False" Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnDeleteComment" runat="server" CssClass="wpcf7-submit" CausesValidation="False" OnClientClick="return confirmMsg(this.form)"
                                            ClientIDMode="Static" OnClick="btnDelete_Click" />
                                        <asp:Button ID="btnApprovedComment" style="padding-left:15px;" Text="Cho phép" CssClass="wpcf7-submit" 
                                            CausesValidation="False"  runat="server" onclick="btnApprovedComment_Click"  />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <br />
            <div class="box" id="divAddComment" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                <asp:Literal ID="litEditComment" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            <asp:Literal ID="litDescription" runat="server"></asp:Literal>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDescription" runat="server" CssClass="wpcf7-text" Rows="3" Columns="5" TextMode="MultiLine"  ></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveComment" runat="server" class="wpcf7-submit" OnClick="btnAddSaveComment_Click" />
                                            <asp:Button ID="btnCancelComment" runat="server" CssClass="wpcf7-submit"
                                                Style="margin-left: 15px;" CausesValidation="False" OnClientClick="return cancelAddSaveComment();"
                                                UseSubmitBehavior="false" ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div2" class="widget">
                            <h2>
                                <asp:Literal ID="litAddCommentHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div3" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddCommentHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
