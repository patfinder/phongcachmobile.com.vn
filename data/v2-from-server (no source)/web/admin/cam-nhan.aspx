﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="cam-nhan.aspx.cs" Inherits="PhongCachMobile.admin.cam_nhan" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function addTest() {
            jQuery('#content_divAddTest').show();
            jQuery('#content_divAddDeleteTest').hide();
        }

        function cancelAddSaveTest() {
            jQuery('#content_divAdd').hide();
            jQuery('#content_divAddDeleteTest').show();
        }

        function check_uncheckprd(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllTest') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecTest') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecPrd') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrVideo_CheckAllTest').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgTest(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecTest') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litTest" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseTest" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseTest" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmVideo">
                                    <asp:DataGrid ID="dgrTest" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrTest_EditCommand"
                                        OnPageIndexChanged="dgrTest_PageIndexChanged" PageSize="20" >
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="Tên">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("DisplayImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("NAME")%>' AlternateText='<%#Eval("NAME")%>'
                                                            ImageUrl='<%# Eval("DisplayImage") %>' Width="50px" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="SHORTDESC" HeaderText="Giới thiệu ngắn" Visible="true" DataFormatString="{0:N0}">
                                                <HeaderStyle Width="20%" />
                                                <ItemStyle Font-Bold="False" Width="10%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllTest" onclick="return check_unchecktest(this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecTest" onclick="return check_unchecktest(this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeleteTest" runat="server">
                                        <asp:Button ID="btnAddTest" runat="server" CssClass="wpcf7-submit" OnClientClick="return addTest();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteTest" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgTest(this.form)" ClientIDMode="Static"
                                            OnClick="btnDeleteTest_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddTest" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth" style="width: 100%">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa cảm nhận</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            Tên<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValName" runat="server" ControlToValidate="txtName"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                         <p class="field">
                                            Giới thiệu ngắn
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtShortDesc" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox>
                                            </span>
                                        </p>

                                        <p class="field">
                                            Cảm nhận<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtText" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị
                                            <br />
                                            <asp:HyperLink ID="hplDisplayImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgDisplay" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulDisplayImage" runat="server" />
                                        </p>
                                      
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveTest" CausesValidation="false" runat="server" class="wpcf7-submit"
                                                OnClick="btnAddSaveTest_Click" />
                                            <asp:Button ID="btnCancelTest" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSaveTest();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
