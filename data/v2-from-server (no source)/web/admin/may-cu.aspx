﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="may-cu.aspx.cs" Inherits="PhongCachMobile.admin.may_cu" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function addPrd() {
            jQuery('#content_divAddPrd').show();
            jQuery('#content_divAddDeletePrd').hide();
        }

        function cancelAddSavePrd() {
            jQuery('#content_divAddPrd').hide();
            jQuery('#content_divAddDeletePrd').show();
        }

        function check_uncheckprd(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllPrd') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecPrd') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecPrd') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrVideo_CheckAllPrd').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgPrd(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecPrd') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litPrd" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponsePrd" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponsePrd" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmVideo">
                                    <asp:DataGrid ID="dgrPrd" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrPrd_EditCommand"
                                        OnPageIndexChanged="dgrPrd_PageIndexChanged" PageSize="20" >
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="Tên">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("DisplayImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("NAME")%>' AlternateText='<%#Eval("NAME")%>'
                                                            ImageUrl='<%# Eval("DisplayImage") %>' Width="50px" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="CURRENTPRICE" HeaderText="Giá bán" Visible="true" DataFormatString="{0:N0}">
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle Font-Bold="False" Width="10%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="ORIGINALPRICE" HeaderText="Giá máy mới" Visible="true"
                                                DataFormatString="{0:N0}">
                                                <ItemStyle Font-Bold="False" Width="10%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                          
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllPrd" onclick="return check_uncheckprd (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecPrd" onclick="return check_uncheckprd (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeletePrd" runat="server">
                                        <asp:Button ID="btnAddPrd" runat="server" CssClass="wpcf7-submit" OnClientClick="return addPrd();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeletePrd" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgPrd(this.form)" ClientIDMode="Static"
                                            OnClick="btnDeletePrd_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddPrd" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth" style="width: 100%">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa máy cũ</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            Tên sản phẩm<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValName" runat="server" ControlToValidate="txtName"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Giới thiệu<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtLongDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Giá bán
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCurrentPrice" runat="server" CssClass="wpcf7-text" Width="200px"
                                                    MaxLength="100"></asp:TextBox>
                                            </span>
                                        </p>
                                        <p class="field">
                                            Giá máy mới
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtOriginalPrice" runat="server" CssClass="wpcf7-text" Width="200px"
                                                    MaxLength="100"></asp:TextBox>
                                            </span>
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị
                                            <br />
                                            <asp:HyperLink ID="hplDisplayImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgDisplay" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulDisplayImage" runat="server" />
                                        </p>
                                      
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSavePrd" CausesValidation="false" runat="server" class="wpcf7-submit"
                                                OnClick="btnAddSavePrd_Click" />
                                            <asp:Button ID="btnCancelPrd" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSavePrd();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
