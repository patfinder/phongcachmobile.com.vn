﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="newsletter.aspx.cs" Inherits="PhongCachMobile.admin.newsletter" ValidateRequest="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>e-Newsletter</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseNewsletter" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseNewsletter" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmNewsletter" runat="server">
                                 <p class="field">Gửi đến (Bcc)<small> * </small>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtBcc" runat="server" CssClass="wpcf7-text" Width="600px" Height="300px" TextMode="MultiLine" MaxLength="10000"></asp:TextBox></span>
                                        </p>
                                   <p class="field">Tiêu đề<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValTittle" runat="server" ControlToValidate="txtTitle"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtTitle" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="100"></asp:TextBox></span>
                                        </p>
                                           <p class="field">Nội dung<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtContent" TextMode="MultiLine" runat="server" CssClass="wpcf7-text" Width="400px" Height="200px"></asp:TextBox></span>
                                        </p>
                                    <p class="submit-wrap" id="divAddDelete" runat="server">
                                        <asp:Button ID="btnSend" runat="server" CssClass="wpcf7-submit" Text="Gửi"
                                            ClientIDMode="Static" onclick="btnSend_Click" />
                                     
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 
            
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
