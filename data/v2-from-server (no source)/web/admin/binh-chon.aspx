﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="binh-chon.aspx.cs" Inherits="PhongCachMobile.admin.binh_chon" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <link href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#content_txtStartDate").datepicker({
                dateFormat: 'dd/mm/yy'
            });
            jQuery("#content_txtEndDate").datepicker({
                dateFormat: 'dd/mm/yy'
            });
        });

        function addPoll() {
            jQuery('#content_divAddPoll').show();
            jQuery('#content_divAddDeletePoll').hide();
        }

        function cancelAddSavePoll() {
            jQuery('#content_divAddPoll').hide();
            jQuery('#content_divAddDeletePoll').show();
        }

        function check_uncheckpoll(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllPoll') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecPoll') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecPoll') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrPoll_CheckAllPoll').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgPoll(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecPoll') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }


        function addDetail() {
            jQuery('#content_divAddDetail').show();
            jQuery('#content_divAddDeleteDetail').hide();
        }

        function cancelAddSaveDetail() {
            jQuery('#content_divAddDetail').hide();
            jQuery('#content_divAddDeleteDetail').show();
        }

        function check_uncheckdetail(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllDetail') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecDetail') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecDetail') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrDetail_CheckAllDetail').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgDetail(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecDetail') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }


        function closeDetail() {
            jQuery('#content_divDetail').hide();
            jQuery('#content_divAddDeletePoll').show();
        }


   

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>Bình chọn</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponsePoll" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponsePoll" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmPoll">
                                    <asp:DataGrid ID="dgrPoll" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrPoll_EditCommand"
                                        OnPageIndexChanged="dgrPoll_PageIndexChanged" PageSize="20" OnItemCommand="dgrPoll_ItemCommand">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="Tên">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                            </asp:BoundColumn>
                                             <asp:BoundColumn DataField="STARTDATE" HeaderText="Ngày bắt đầu">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="15%" />
                                            </asp:BoundColumn>
                                             <asp:BoundColumn DataField="ENDDATE" HeaderText="Ngày kết thúc">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="15%" />
                                            </asp:BoundColumn>
                                              <asp:TemplateColumn HeaderText="Chi tiết">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton CommandName="Detail" ID="hplDetail" Text="..." runat="server" CausesValidation="False" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllPoll" onclick="return check_uncheckpoll(this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecPoll" onclick="return check_uncheckpoll(this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeletePoll" runat="server">
                                        <asp:Button ID="btnAddPoll" runat="server" CssClass="wpcf7-submit" OnClientClick="return addPoll();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeletePoll" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgPoll(this.form)" ClientIDMode="Static"
                                            OnClick="btnDeletePoll_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddPoll" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth" style="width: 100%">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa bình chọn</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            Câu hỏi<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValName" runat="server" ControlToValidate="txtName"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Ngày bắt đầu<small> * </small>   <asp:RequiredFieldValidator ID="reqValStartDate" runat="server" ControlToValidate="txtStartDate"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="wpcf7-text" Width="200px" MaxLength="14"></asp:TextBox> </span>
                                        </p>
                                        <p class="field">
                                            Ngày kết thúc<small> * </small> <asp:RequiredFieldValidator ID="reqValEndDate" runat="server" ControlToValidate="txtEndDate"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="wpcf7-text" Width="200px" MaxLength="14"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSavePoll" CausesValidation="false" runat="server" class="wpcf7-submit"
                                                OnClick="btnAddSavePoll_Click" />
                                            <asp:Button ID="btnCancelPoll" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSavePoll();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" style="display: none;" id="divDetail" runat="server">
                <div class="wrapper">
                    <div>
                        <h2>Chi tiết bình chọn</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseDetail" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseDetail" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="Div11">
                                    <asp:DataGrid ID="dgrDetail" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrDetail_EditCommand"
                                        OnPageIndexChanged="dgrDetail_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="CÂU TRẢ LỜI">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="60%" />
                                            </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllDetail" onclick="return check_uncheckdetail(this);"
                                                        runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecDetail" onclick="return check_uncheckdetail(this);"
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeleteDetail" runat="server">
                                        <asp:Button ID="btnAddDetail" runat="server" CssClass="wpcf7-submit" OnClientClick="return addDetail();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteDetail" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgDetail(this.form)"
                                            ClientIDMode="Static" OnClick="btnDeleteDetail_Click" />
                                        <asp:Button ID="btnCloseDetail" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            OnClientClick="return closeDetail();" UseSubmitBehavior="false" ClientIDMode="Static" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box boxmg" id="divAddDetail" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>Thêm / chỉnh sửa chi tiết bình chọn</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div6">
                                        <asp:HiddenField ID="hdfPollId" runat="server" />
                                        <asp:HiddenField ID="hdfDetailId" runat="server" />
                                        <p class="field">
                                            Câu trả lời<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDetailName" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveDetail" runat="server" Text="Lưu" CausesValidation="false" class="wpcf7-submit"
                                                OnClick="btnAddSaveDetail_Click" />
                                            <asp:Button ID="btnCancelSaveDetail" runat="server" Text="Hủy" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelSaveDetail();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
