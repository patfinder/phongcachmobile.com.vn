﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="sitemap.aspx.cs" Inherits="PhongCachMobile.admin.sitemap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
  <div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="Div1" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            Tạo SiteMap </h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseText" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseText" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div>
                                    Sitemap được tạo ra mỗi 2 tuần 1 lần. Chỉ cập nhật Sitemap lên Webmaster Tool 1 lần duy nhất, sau đó khi sitemap thay đổi thì Google Webmaster tool tự động cập nhật.<br />
                                    - Sản phẩm, loại sản phẩm <br />
                                    - Ứng dụng, game <br />
                                    - Tin khuyến mãi, tin thị trường, hướng dẫn sử dụng<br />
                                <br />
                                    <asp:Button ID="btSubmit" runat="server" Text="Tạo Sitemap" OnClick="Button1_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
