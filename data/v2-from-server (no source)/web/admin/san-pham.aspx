﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" ValidateRequest="false"
    AutoEventWireup="true" CodeBehind="san-pham.aspx.cs" Inherits="PhongCachMobile.admin.san_pham" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function addPrd() {
            jQuery('#content_divAddPrd').show();
            jQuery('#content_divAddDeletePrd').hide();
        }

        function cancelAddSavePrd() {
            jQuery('#content_divAddPrd').hide();
            jQuery('#content_divAddDeletePrd').show();
        }

        function check_uncheckprd(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllPrd') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecPrd') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecPrd') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrPrd_CheckAllPrd').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgPrd(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecPrd') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }


        function addVideo() {
            jQuery('#content_divAddVideo').show();
            jQuery('#content_divAddDeleteVideo').hide();
        }

        function cancelAddSaveVideo() {
            jQuery('#content_divAddVideo').hide();
            jQuery('#content_divAddDeleteVideo').show();
        }

        function check_uncheckvideo(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllVideo') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecVideo') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecVideo') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrVideo_CheckAllVideo').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgVideo(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecVideo') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }


        function closeVideo() {
            jQuery('#content_divVideo').hide();
            jQuery('#content_divAddDeleteVideo').show();
        }


        function addArticle() {
            jQuery('#content_divAddArticle').show();
            jQuery('#content_divAddDeleteArticle').hide();
        }

        function cancelAddSaveArticle() {
            jQuery('#content_divArticle').hide();
            jQuery('#content_divAddDeleteArticle').show();
        }

        function closeArticle() {
            jQuery('#content_divArticle').hide();
            jQuery('#content_divAddDeletePrd').show();
        }



        function check_uncheckarticle(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllArticle') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecArticle') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecArticle') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrPart_CheckAllArticle').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgArticle(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecArticle') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }

        function cancelSaveFeature() {
            jQuery('#content_divFeature').hide();
            jQuery('#content_divAddDeletePrd').show();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litPrd" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponsePrd" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponsePrd" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmVideo">
                                    <p class="field">
                                        Loại &nbsp;
                                        <asp:DropDownList ID="ddlType" runat="server" OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                        &nbsp; | &nbsp; Category &nbsp;
                                        <asp:DropDownList ID="ddlCateogry" runat="server" OnSelectedIndexChanged="ddlCateogry_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrPrd" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrPrd_EditCommand"
                                        OnPageIndexChanged="dgrPrd_PageIndexChanged" PageSize="20" OnItemCommand="dgrPrd_ItemCommand">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="NAME" HeaderText="Tên">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("FirstImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("NAME")%>' AlternateText='<%#Eval("NAME")%>'
                                                            ImageUrl='<%# Eval("FirstImage") %>' Width="50%" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="CURRENTPRICE" HeaderText="Giá bán" Visible="true" DataFormatString="{0:N0}">
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle Font-Bold="False" Width="10%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="DISCOUNTPRICE" HeaderText="Giá khuyến mãi" Visible="true"
                                                DataFormatString="{0:N0}">
                                                <ItemStyle Font-Bold="False" Width="10%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                             <asp:TemplateColumn HeaderText="Thông số KT">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton CommandName="Feature" ID="hplFeature" Text="..." runat="server" CausesValidation="False" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:TemplateColumn HeaderText="Tư vấn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton CommandName="Article" ID="hplArticle" Text="..." runat="server" CausesValidation="False" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                              <asp:TemplateColumn HeaderText="Video">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton CommandName="Video" ID="hplVideo" Text="..." runat="server" CausesValidation="False" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>

                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllPrd" onclick="return check_uncheckprd (this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecPrd" onclick="return check_uncheckprd (this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeletePrd" runat="server">
                                        <asp:Button ID="btnAddPrd" runat="server" CssClass="wpcf7-submit" OnClientClick="return addPrd();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeletePrd" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgPrd(this.form)" ClientIDMode="Static"
                                            OnClick="btnDeletePrd_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddPrd" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth" style="width: 100%">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa sản phẩm</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />

                                          <p class="field">
                                           Kích hoạt<br />
                                          <table style="width:150px;">
                                            <tr>
                                                <td>    <asp:RadioButton Text="Có" ID="rbtEnableYes" runat="server" Checked="true" GroupName="enable" /></td>
                                                <td><asp:RadioButton Text="Không" ID="rbtEnableNo" GroupName="enable"
                                                 runat="server" /></td>
                                            </tr>
                                            </table>
                                        </p>



                                        <p class="field">
                                            Tên sản phẩm<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValName" runat="server" ControlToValidate="txtName"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Giới thiệu ngắn<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtShortDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Giới thiệu chi tiết<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtLongDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Sản phẩm bao gồm
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtInclude" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Nổi bật<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtRemark" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Ưu điểm<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtOutstanding" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Khuyến mãi
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtDiscountDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                           Ưu đãi (Giá hot)
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtHotDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Video
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtVideo" runat="server" CssClass="wpcf7-text" Width="200px" MaxLength="20"></asp:TextBox>
                                            </span>
                                        </p>
                                        <p class="field">
                                            Giá hiện tại
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCurrentPrice" runat="server" CssClass="wpcf7-text" Width="200px"
                                                    MaxLength="100"></asp:TextBox>
                                            </span>
                                        </p>
                                        <p class="field">
                                            Giá khuyến mãi
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDiscountPrice" runat="server" CssClass="wpcf7-text" Width="200px"
                                                    MaxLength="100"></asp:TextBox>
                                            </span>
                                        </p>
                                        <p>
                                            <br />
                                            <asp:CheckBox ID="chbIsFeatured" runat="server" />
                                            là sản phẩm nổi bật
                                        </p>
                                         <p>
                                            <br />
                                            <asp:CheckBox ID="chbIsSpeedySold" runat="server" />
                                            là sản phẩm bán chạy
                                        </p>
                                         <p>
                                            <br />
                                            <asp:CheckBox ID="chbIsCompanyProduct" runat="server" />
                                            là sản phẩm từ công ty
                                        </p>
                                        <p>
                                            <br />
                                            <asp:CheckBox ID="chbIsGifted" runat="server" />
                                            có quà tặng kèm theo
                                        </p>
                                           <p>
                                            <br />
                                            <asp:CheckBox ID="chbIsOutOfStock" runat="server" />
                                            tạm hết
                                        </p>
                                           <p>
                                            <br />
                                            <asp:CheckBox ID="chbIsComingSoon" runat="server" />
                                            sắp có hàng
                                        </p>
                                        <p class="field">
                                            Hệ điều hành
                                            <br />
                                            <asp:DropDownList ID="ddlOS" runat="server">
                                            </asp:DropDownList>
                                        </p>
                                           <p class="field">
                                           Tính năng chính
                                            <br />
                                               <asp:CheckBoxList ID="cblFeature" runat="server">
                                               </asp:CheckBoxList>
                                        </p>
                                        <p class="field">
                                            Category
                                            <asp:DropDownList ID="ddlAnotherCategory" runat="server">
                                            </asp:DropDownList>
                                        </p>
                                        <p class="field">
                                            Lượt xem<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqViewCount" runat="server" ControlToValidate="txtViewCount"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtViewCount" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="10"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Đánh giá<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqRate" runat="server" ControlToValidate="txtViewCount"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtRate" runat="server" CssClass="wpcf7-text" Width="50px" MaxLength="10"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị #1
                                            <br />
                                            <asp:HyperLink ID="hplFirstImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgFirst" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulFirstImage" runat="server" />
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị #2
                                            <br />
                                            <asp:HyperLink ID="hplSecondImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgSecond" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulSecondImage" runat="server" />
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị #3
                                            <br />
                                            <asp:HyperLink ID="hplThirdImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgThird" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulThirdImage" runat="server" />
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị #4
                                            <br />
                                            <asp:HyperLink ID="hplFourthImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgFourth" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulFourthImage" runat="server" />
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị #5
                                            <br />
                                            <asp:HyperLink ID="hplFifthImage" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgFifth" Width="150" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulFifthImage" runat="server" />
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSavePrd" CausesValidation="false" runat="server" class="wpcf7-submit"
                                                OnClick="btnAddSavePrd_Click" />
                                            <asp:Button ID="btnCancelPrd" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSavePrd();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" style="display: none;" id="divArticle" runat="server">
                <div class="wrapper">
                    <div>
                        <h2>
                            Tư vấn sử dụng</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseArticle" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseArticle" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="Div5">
                                    <asp:DataGrid ID="dgrArticle" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrArticle_EditCommand"
                                        OnPageIndexChanged="dgrArticle_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="TITLE" HeaderText="Tiêu đề">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="40%" />
                                            </asp:BoundColumn>
                                            <%--<asp:BoundColumn DataField="SHORTDESC" HeaderText="Nội dung ngắn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>--%>
                                            <asp:BoundColumn DataField="GROUPID" HeaderText="GROUPID" Visible="False">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="Ảnh hiển thị">
                                                <ItemTemplate>
                                                    <a href='<%# Eval("DisplayImage") %>' rel="prettyPhoto">
                                                        <asp:Image ID="Image1" ToolTip='<%#Eval("Title")%>' AlternateText='<%#Eval("Title")%>'
                                                            ImageUrl='<%# Eval("DisplayImage") %>' Width="30%" runat="server" />
                                                    </a>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="LONGDESC" HeaderText="Nội dung" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="POSTEDDATE" HeaderText="Ngày đăng">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CATEGORYID" HeaderText="CATID" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="VIEWCOUNT" HeaderText="Lượt xem" DataFormatString="{0:#,###}">
                                            </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllArticle" onclick="return check_uncheckarticle (this);"
                                                        runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecArticle" onclick="return check_uncheckarticle (this);"
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeleteArticle" runat="server">
                                        <asp:Button ID="btnAddArticle" runat="server" CssClass="wpcf7-submit" OnClientClick="return addArticle();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteArticle" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgArticle(this.form)"
                                            ClientIDMode="Static" OnClick="btnDeleteArticle_Click" />
                                        <asp:Button ID="btnCloseArticle" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            OnClientClick="return closeArticle();" UseSubmitBehavior="false" ClientIDMode="Static" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddArticle" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa tư vấn sử dụng</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div7">
                                        <asp:HiddenField ID="hdfArticleId" runat="server" />
                                        <asp:HiddenField ID="hdfPrdId" runat="server" />
                                        <p class="field">
                                            Tiêu đề<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtArticleTitle" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Giới thiệu ngắn<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtArticleShortDesc" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Giới thiệu chi tiết<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <CKEditor:CKEditorControl ID="txtArticleLongDesc" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Lượt xem<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtArticleViewCount" runat="server" CssClass="wpcf7-text" Width="50px"
                                                    MaxLength="5"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Ảnh hiển thị
                                            <br />
                                            <asp:HyperLink ID="hplDisplayImg" rel="prettyPhoto" runat="server">
                                                <asp:Image ID="imgDisplay" runat="server" />
                                            </asp:HyperLink><br />
                                            <asp:FileUpload ID="fulDisplayImg" runat="server" />
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveArticle" runat="server" CausesValidation="false" class="wpcf7-submit"
                                                OnClick="btnAddSaveArticle_Click" />
                                            <asp:Button ID="btnCancelArticle" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSaveArticle();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div8" class="widget">
                            <h2>
                                <asp:Literal ID="litAddArticleHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div9" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddArticleHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divFeature" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                Tính năng sản phẩm</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div3">
                                        <asp:HiddenField ID="hdfFeatureId" runat="server" />
                                        <asp:HiddenField ID="hdfAnotherPrdId" runat="server" />
                                        <h4 style="color:#be2603;">
                                            TỔNG QUAN</h4>
                                        <p class="field">
                                            Mạng 2G<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txt2G" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Mạng 3G<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txt3G" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Mạng 4G<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txt4G" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Ra mắt<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCommingOut" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            KÍCH THƯỚC</h4>
                                        <p class="field">
                                            Kích thước<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtSize" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Trọng lượng<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtWeight" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            MÀN HÌNH</h4>
                                        <p class="field">
                                            Loại<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDisplayType" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Kích cỡ<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDisplaySize" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Khác<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                             <CKEditor:CKEditorControl ID="txtDisplayOther" Toolbar="Basic" runat="server"></CKEditor:CKEditorControl></span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            ÂM THANH</h4>
                                        <p class="field">
                                            Kiểu chuông<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtRingType" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Loa ngoài<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtOutSound" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Khác<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                            <CKEditor:CKEditorControl ID="txtOtherSound" Toolbar="Basic" runat="server"></CKEditor:CKEditorControl>
                                               </span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            BỘ NHỚ</h4>
                                        <p class="field">
                                            Danh bạ<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtNumberBook" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Các số đã gọi<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCalledNumber" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Bộ nhớ trong<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtInsideMemory" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Khe cắm thẻ nhớ<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCardSlot" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            TRUYỀN DỮ LIỆU</h4>
                                        <p class="field">
                                            GPRS<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtGPRS" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            EDGE<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtEDGE" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            3G<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtDataLoad3G" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            WLAN<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtWLAN" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Bluetooth<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtBluetooth" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            NFC<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtNFC" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            USB<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtUSB" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            CAMERA</h4>
                                        <p class="field">
                                            Camera chính<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtMainCamera" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Đặc điểm<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCameraFeature" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Quay phim<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtVideoRecorder" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Camera phụ<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtSubCamera" runat="server" CssClass="wpcf7-text" Width="400px"
                                                    MaxLength="250"></asp:TextBox></span>
                                        </p><br />
                                        <h4 style="color:#be2603;">
                                            ĐẶC ĐIỂM</h4>
                                        <p class="field">
                                            Hệ điều hành<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtOS" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Bộ xử lý<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                 <CKEditor:CKEditorControl ID="txtChip" Toolbar="Basic" runat="server"></CKEditor:CKEditorControl></span>
                                        </p>
                                        <p class="field">
                                            Tin nhắn<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtSMS" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Trình duyệt<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtBrowser" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Radio<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtRadio" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Trò chơi<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtGame" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Màu sắc<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtColor" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Ngôn ngữ<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtLanguage" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                              <p class="field">
                                            Định vị toàn cầu<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtGPS" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                              <p class="field">
                                            Java<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtJava" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                              <p class="field">
                                            Khác<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                               <CKEditor:CKEditorControl ID="txtOtherFeature" Toolbar="Basic" runat="server"></CKEditor:CKEditorControl>
                                                </span>
                                        </p><br />
                                           <h4 style="color:#be2603;">
                                            PIN</h4>
                                              <p class="field">
                                            Pin chuẩn<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtPin" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="200"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnSaveFeature" runat="server" Text="Lưu" CausesValidation="false" class="wpcf7-submit"
                                                OnClick="btnSaveFeature_Click" />
                                            <asp:Button ID="btnCancelFeature" runat="server" Text="Hủy" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelSaveFeature();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" style="display: none;" id="divVideo" runat="server">
                <div class="wrapper">
                    <div>
                        <h2>Video</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseVideo" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseVideo" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="Div11">
                                    <asp:DataGrid ID="dgrVideo" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrVideo_EditCommand"
                                        OnPageIndexChanged="dgrVideo_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="TITLE" HeaderText="Tên video">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="40%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="VIDEO" HeaderText="Video" Visible="False"></asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllVideo" onclick="return check_uncheckvideo (this);"
                                                        runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecVideo" onclick="return check_uncheckvideo (this);"
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeleteVideo" runat="server">
                                        <asp:Button ID="btnAddVideo" runat="server" CssClass="wpcf7-submit" OnClientClick="return addVideo();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteVideo" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgVideo(this.form)"
                                            ClientIDMode="Static" OnClick="btnDeleteVideo_Click" />
                                        <asp:Button ID="btnCloseVideo" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            OnClientClick="return closeVideo();" UseSubmitBehavior="false" ClientIDMode="Static" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box boxmg" id="divAddVideo" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>Thêm / chỉnh sửa Video</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div6">
                                        <asp:HiddenField ID="hdfProductId" runat="server" />
                                        <asp:HiddenField ID="hdfVideoId" runat="server" />
                                    
                                        <p class="field">
                                            Video<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtVideoCode" runat="server" CssClass="wpcf7-text" Width="100px" MaxLength="20"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Title<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtVideoTitle" runat="server" CssClass="wpcf7-text" Width="400px" MaxLength="250"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveVideo" runat="server" Text="Lưu" CausesValidation="false" class="wpcf7-submit"
                                                OnClick="btnAddSaveVideo_Click" />
                                            <asp:Button ID="btnCancelSaveVideo" runat="server" Text="Hủy" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelSaveVideo();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
