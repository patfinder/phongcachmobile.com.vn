﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/master/Home.Master" AutoEventWireup="true" CodeBehind="diem-tich-luy.aspx.cs" Inherits="PhongCachMobile.admin.diem_tich_luy" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <script src="js/jquery-ui-1.8.14.custom.min.js" type="text/javascript"></script>
    <link href="css/ui-lightness/jquery-ui-1.8.14.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    <link href="css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#content_txtFrom").datetimepicker({
                dateFormat: 'dd/mm/yy'
            });
            jQuery("#content_txtTo").datetimepicker({
                dateFormat: 'dd/mm/yy'
            });
        });

        function addCard() {
            jQuery('#content_divAddCard').show();
            jQuery('#content_divAddDeleteCard').hide();
        }

        function cancelAddSaveCard() {
            jQuery('#content_divAddCard').hide();
            jQuery('#content_divAddDeleteCard').show();
        }

        function check_uncheckcard(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllCard') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecCard') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                    else if (ValId.indexOf('deleteRecCard') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrCard_CheckAllCard').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgCard(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecCard') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }

        function addPoint() {
            jQuery('#content_divAddPoint').show();
            jQuery('#content_divAddDeletePoint').hide();
        }

        function cancelAddSavePoint() {
            jQuery('#content_divAddPoint').hide();
            jQuery('#content_divAddDeletePoint').show();
        }

        function closePoint() {
            jQuery('#content_divPoint').hide();
            jQuery('#content_divAddDeleteCard').show();
        }

        function check_uncheckpoint(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllPoint') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecPoint') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                        else if (ValId.indexOf('deleteRecPoint') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                        document.getElementById('content_dgrPart_CheckAllPoint').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

        function confirmMsgPoint(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecPoint') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xóa?')
                }
            }
        }

        function confirmMsgUnApproved(frm) {
            // loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for our checkboxes only
                if (frm.elements[i].name.indexOf('deleteRecUnApprovedPoint') != -1) {
                    // If any are checked then confirm alert, otherwise nothing happens
                    if (frm.elements[i].checked)
                        return confirm('Bạn có chắc muốn xác nhận?')
                }
            }
        }

        function check_uncheckunapprovedpoint(Val) {
            var ValChecked = Val.checked;
            var ValId = Val.id;
            var frm = document.forms[0];
            // Loop through all elements
            for (i = 0; i < frm.length; i++) {
                // Look for Header Template's Checkbox
                //As we have not other control other than checkbox we just check following statement
                if (this != null) {
                    if (ValId.indexOf('CheckAllUnApprovedPoint') != -1) {
                        // Check if main checkbox is checked,
                        // then select or deselect datagrid checkboxes
                        //alert(frm.elements[i].id);
                        // alert(frm.elements[i].id.substr(frm.elements[i].id.length - 9, frm.elements[i].id.length - 1));
                        if (frm.elements[i].id.indexOf('deleteRecUnApprovedPoint') != -1)
                            if (ValChecked)
                                frm.elements[i].checked = true;
                            else
                                frm.elements[i].checked = false;
                    }
                        else if (ValId.indexOf('deleteRecUnApprovedPoint') != -1) {
                        //alert(document.getElementById('grdProduct_ct102_CheckAll'));
                        // Check if any of the checkboxes are not checked, and then uncheck top select all checkbox
                            document.getElementById('content_dgrUnApprovedPoint_CheckAllUnApprovedPoint').checked = false;

                        if (frm.elements[i].checked == false)
                            frm.elements[1].checked = false;
                    }
                } // if
            } // for
        } // function</PRE>

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div class="container_12 primary_content_wrap clearfix">
        <form id="Form1" runat="server" class="wpcf7-form">
        <div id="content" class="grid_12">
        <div class="box" id="divUnApprovedPoint" runat="server" style="margin-bottom: 15px;">
                <div class="wrapper">
                    <div>
                        <h2>Quản lý điểm tích lũy chưa chứng thực</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseUnapprovedPoint" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblResponseUnapprovedPoint" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="Div3">
                                    <p class="field">
                                        <asp:Literal ID="litFrom" runat="server"> </asp:Literal> <small> * </small>: &nbsp; 
                                        <asp:TextBox ID="txtFrom" runat="server" CssClass="wpcf7-text" Width="150px" MaxLength="30"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqFrom" runat="server" ControlToValidate="txtFrom"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator> &nbsp; 
                                        <asp:Literal ID="litTo" runat="server"> </asp:Literal>: &nbsp; 
                                        <asp:TextBox ID="txtTo" runat="server" CssClass="wpcf7-text" Width="150px" MaxLength="30"></asp:TextBox>
                                        &nbsp; &nbsp; 
                                        <asp:LinkButton ID="lbtFind" Font-Bold="true" runat="server" CausesValidation="false" Text="Tìm kiếm" onclick="lbtFind_Click"></asp:LinkButton>
                                        </p>
                                    <asp:DataGrid ID="dgrUnApprovedPoint" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrUnApprovedPoint_EditCommand"
                                        OnPageIndexChanged="dgrUnApprovedPoint_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Code" HeaderText="Mã hóa đơn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <%--<asp:BoundColumn DataField="SHORTDESC" HeaderText="Nội dung ngắn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>--%>
                                              <asp:BoundColumn DataField="User" HeaderText="Tên người tạo">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="sDate" HeaderText="Ngày tạo">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="STATUS" HeaderText="Trạng thái">
                                              <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllUnApprovedPoint" onclick="return check_uncheckunapprovedpoint(this);"
                                                        runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecUnApprovedPoint" onclick="return check_uncheckunapprovedpoint(this);"
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="P1" runat="server">
                                        <asp:Button ID="btnApprovePoint" runat="server" CssClass="wpcf7-submit"
                                            CausesValidation="False" ClientIDMode="Static" Text="Xác nhận"  OnClientClick="return confirmMsgUnApproved(this.form)" 
                                            OnClick="btnApprovePoint_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="box">
                <div class="wrapper">
                    <div>
                        <h2>
                            <asp:Literal ID="litCard" runat="server"></asp:Literal></h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponseCard" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponseCard" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="frmVideo">
                                    <p class="field">
                                        Mã số thẻ &nbsp;
                                        <asp:TextBox ID="txtCode" Width="100px" runat="server"></asp:TextBox> &nbsp; 
                                        <asp:LinkButton ID="lbtSearch" CausesValidation="false" runat="server" onclick="lbtSearch_Click">Tìm</asp:LinkButton> &nbsp; | &nbsp; Trạng thái &nbsp;
                                        <asp:DropDownList ID="ddlStatus" runat="server" 
                                            onselectedindexchanged="ddlStatus_SelectedIndexChanged" 
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </p>
                                    <asp:DataGrid ID="dgrCard" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrCard_EditCommand"
                                        OnPageIndexChanged="dgrCard_PageIndexChanged" PageSize="20" OnItemCommand="dgrCard_ItemCommand">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Code" HeaderText="Mã số thẻ">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="BrandNumber" HeaderText="Chi nhánh" Visible="true">
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle Font-Bold="False" Width="10%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="sSTATUS" HeaderText="Trạng thái" Visible="true"                    >
                                                <ItemStyle Font-Bold="False" Width="25%" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Point" HeaderText="Điểm tích lũy" Visible="true"                    >
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                             <asp:TemplateColumn HeaderText="Chi tiết điểm">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton CommandName="Point" ID="hplPoint" Text="..." runat="server" CausesValidation="False" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                               <asp:BoundColumn DataField="sDate" HeaderText="Ngày tạo">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                                <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllCard" onclick="return check_uncheckcard(this);" runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecCard" onclick="return check_uncheckcard(this);" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeleteCard" runat="server">
                                        <asp:Button ID="btnAddCard" runat="server" CssClass="wpcf7-submit" OnClientClick="return addCard();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeleteCard" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgCard(this.form)" ClientIDMode="Static"
                                            OnClick="btnDeleteCard_Click" />
                                        <asp:Button ID="btnRequestClose" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" ClientIDMode="Static" Text="Yêu cầu đóng" 
                                            OnClick="btnRequestClose_Click" />
                                        <asp:Button ID="btnApproveClose" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" ClientIDMode="Static" Text="Xác nhận đóng"
                                            OnClick="btnApproveClose_Click" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddCard" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth" style="width: 100%">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa card</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div1" runat="server">
                                        <asp:HiddenField ID="hdfId" runat="server" />
                                        <p class="field">
                                            Mã số thẻ<small> * </small>
                                            <asp:RequiredFieldValidator ID="reqValCode" runat="server" ControlToValidate="txtCardCode"
                                                Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtCardCode" runat="server" CssClass="wpcf7-text" Width="100px" MaxLength="10"></asp:TextBox></span>
                                        </p>
                                        <p class="field">
                                            Chi nhánh<small> * </small>
                                            <br />
                                             <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtBrandNumber" runat="server" CssClass="wpcf7-text" Width="100px" MaxLength="10"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSaveCard" CausesValidation="false" runat="server" class="wpcf7-submit"
                                                OnClick="btnAddSaveCard_Click" />
                                            <asp:Button ID="btnCancelCard" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSaveCard();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                         
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" style="display: none;" id="divPoint" runat="server">
                <div class="wrapper">
                    <div>
                        <h2>
                            Điểm tích lũy</h2>
                        <div class="textwidget">
                            <div class="wpcf7">
                                <div id="frmResponsePoint" runat="server" visible="false">
                                    <p class="field">
                                        <asp:Label ID="lblFrmResponsePoint" runat="server"></asp:Label>
                                    </p>
                                </div>
                                <div id="Div5">
                                    <asp:DataGrid ID="dgrPoint" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                        CssClass="datagrid" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                        Font-Strikeout="False" Font-Underline="False" OnEditCommand="dgrPoint_EditCommand"
                                        OnPageIndexChanged="dgrPoint_PageIndexChanged" PageSize="20">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="NO.">
                                                <ItemTemplate>
                                                    <%# Container.DataSetIndex + 1%>
                                                </ItemTemplate>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ID" HeaderText="Id" Visible="False"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Code" HeaderText="Mã hóa đơn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <%--<asp:BoundColumn DataField="SHORTDESC" HeaderText="Nội dung ngắn">
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>--%>
                                              <asp:BoundColumn DataField="User" HeaderText="Tên người tạo">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="sDate" HeaderText="Ngày tạo">
                                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="STATUS" HeaderText="Trạng thái">
                                              <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                        <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                            </asp:BoundColumn>
                                            <asp:EditCommandColumn CancelText="Hủy" EditText="Sửa" UpdateText="Cập nhật"></asp:EditCommandColumn>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                    Font-Underline="False" HorizontalAlign="Center" />
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="CheckAllPoint" onclick="return check_uncheckpoint(this);"
                                                        runat="server" />
                                                </HeaderTemplate>
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="deleteRecPoint" onclick="return check_uncheckpoint(this);"
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle Font-Bold="True" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                            Font-Underline="False" ForeColor="White" BackColor="#313737" HorizontalAlign="Center" />
                                        <PagerStyle Mode="NumericPages" />
                                    </asp:DataGrid>
                                    <p class="submit-wrap" id="divAddDeletePoint" runat="server">
                                        <asp:Button ID="btnAddPoint" runat="server" CssClass="wpcf7-submit" OnClientClick="return addPoint();"
                                            UseSubmitBehavior="false" ClientIDMode="Static" />
                                        <asp:Button ID="btnDeletePoint" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            CausesValidation="False" OnClientClick="return confirmMsgPoint(this.form)"
                                            ClientIDMode="Static" OnClick="btnDeletePoint_Click" />
                                        <asp:Button ID="btnClosePoint" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                            OnClientClick="return closePoint();" UseSubmitBehavior="false" ClientIDMode="Static" />

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box boxmg" id="divAddPoint" style="display: none;" runat="server">
                <div class="wrapper">
                    <div class="three_fifth">
                        <div>
                            <h2>
                                Thêm / chỉnh sửa điểm tích lũy</h2>
                            <div class="textwidget">
                                <div class="wpcf7">
                                    <div id="Div7">
                                        <asp:HiddenField ID="hdfPointId" runat="server" />
                                        <asp:HiddenField ID="hdfCardId" runat="server" />
                                        <asp:HiddenField ID="hdfUpdateFromUnapproved" runat="server" />
                                        <p class="field">
                                            Mã hóa đơn<small> * </small>
                                            <br />
                                            <span class="wpcf7-form-control-wrap your-email">
                                                <asp:TextBox ID="txtAnotherCardCode" runat="server" CssClass="wpcf7-text" Width="100px"
                                                    MaxLength="10"></asp:TextBox></span>
                                        </p>
                                        <p class="submit-wrap">
                                            <asp:Button ID="btnAddSavePoint" runat="server" CausesValidation="false" class="wpcf7-submit"
                                                OnClick="btnAddSavePoint_Click" />
                                            <asp:Button ID="btnCancelPoint" runat="server" CssClass="wpcf7-submit" Style="margin-left: 15px;"
                                                CausesValidation="False" OnClientClick="return cancelAddSavePoint();" UseSubmitBehavior="false"
                                                ClientIDMode="Static" />
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two_fifth last">
                        <div id="Div8" class="widget">
                            <h2>
                                <asp:Literal ID="litAddPointHelp" runat="server"></asp:Literal></h2>
                            <div class="textwidget">
                            </div>
                        </div>
                        <div id="Div9" class="widget">
                            <div class="textwidget">
                                <dl class="address">
                                    <dt>
                                        <asp:Literal ID="litAddPointHelpDesc" runat="server"></asp:Literal></dt>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--#content-->
        </form>
    </div>
</asp:Content>
