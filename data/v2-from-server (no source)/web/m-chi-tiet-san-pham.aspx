﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true"
    CodeBehind="m-chi-tiet-san-pham.aspx.cs" Inherits="PhongCachMobile.m_chi_tiet_san_pham" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">        stLight.options({ publisher: "da6af5b0-d769-45cc-8003-392451774e04", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                 <div style="line-height:18px;">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2 class="subtitle">
                                        <asp:HyperLink ID="hplCategory" runat="server"></asp:HyperLink>
                                        <span class="navigation-pipe">&gt;</span>
                                        <asp:HyperLink ID="hplSubCategory" runat="server"></asp:HyperLink>
                                        <span class="navigation-pipe">&gt;</span> <span>
                                            <asp:Literal ID="litPrdName" runat="server"></asp:Literal></span></h2>
                                </div>
                                <div class="category-products" style="display: inline-block;">
                                    <div class="prd">
                                        <div id="center_column" class="center_column" style="width: 100%;">
                                            <div id="primary_block" class="clearfix">
                                                <div id="pb-left-column">
                                                  <asp:Image ID="imgDisplay"  style="width:320px" runat="server" /><br /><br />
                                                    <h1>
                                                        <asp:Literal ID="litAnotherPrdName" runat="server"></asp:Literal></h1>
                                                          <ul id="usefull_link_block" class="bordercolor">
                                                        <li style="background: none;">Lượt xem: <font color="red">
                                                            <asp:Literal ID="litViewCount" runat="server"></asp:Literal></font></li>
                                                    </ul>
                                                    <div id="buy_block" class="bordercolor">
                                                        <div class="price bordercolor">
                                                            <span class="our_price_display">
                                                                <asp:Literal ID="litPriceType" runat="server"></asp:Literal>:
                                                                <br />
                                                                <span id="our_price_display" class="price">
                                                                    <asp:Literal ID="litPrdPrice" runat="server"></asp:Literal></span> </span>
                                                        </div>
                                                        <div class="other_options bordercolor" id="discount" runat="server">
                                                            <div id="discountdesc">
                                                                <asp:Literal ID="litDiscountDesc" runat="server"></asp:Literal>
                                                            </div>
                                                            <%--<div id="other_prices">Giá gốc: 
                                                                <p id="old_price" style="padding:5px 0 0 0 ;">
                                                                    <span id="old_price_display" style="font-size: 16px;">
                                                                        <asp:Literal ID="litOriginalPrice" runat="server"></asp:Literal></span> 
                                                                </p>
                                                            </div>
                                                            <div id="attributes">
                                                                <span class="discount">Đang giảm giá</span>
                                                                <div class="clearblock">
                                                                </div>
                                                            </div>--%>
                                                            <div class="clearblock">
                                                            </div>
                                                        </div>
                                                        <div id="short_description_block" class="bordercolor">
                                                            <div id="short_description_content" class="rte align_justify">
                                                                <asp:Literal ID="litRemark" runat="server"></asp:Literal></div>
                                                            <%--      <p class="buttons_bottom_block">
                                                                <a href="javascript:{}" class="button">chi tiết </a></p>--%>
                                                        </div>
                                                        <p id="oosHook" style="display: none;">
                                                        </p>
                                                        <br />
                                                        <div class="share bordercolor">
                                                            <!-- AddThis Button BEGIN -->
                                                            <div class="addthis_toolbox addthis_default_style ">
                                                                <span class='st_facebook_hcount' displaytext='Facebook'></span><span class='st_twitter_hcount'
                                                                    displaytext='Tweet'></span><span class='st_googleplus_hcount' displaytext='Google +'>
                                                                    </span><span class='st_email_hcount' displaytext='Email'></span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="clearblock">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-role="collapsible-set" id="more_info_block" runat="server" data-theme="c" data-content-theme="d">
                                                <div data-role="collapsible">
                                                    <h3>
                                                        Điểm nổi bật</h3>
                                                    <asp:Literal ID="litOutstanding" runat="server"></asp:Literal>
                                                </div>
                                                <div data-role="collapsible">
                                                    <h3>
                                                        Thông số kĩ thuật</h3>
                                                     <asp:Literal ID="litFeatures" runat="server"></asp:Literal>
                                                </div>
                                                <div data-role="collapsible">
                                                    <h3>
                                                        Phụ kiện</h3>
                                                 <asp:Literal ID="litAccessories" runat="server"></asp:Literal>
                                                </div>
                                                <div data-role="collapsible">
                                                    <h3>
                                                        Quảng cáo</h3>
                                                    <asp:Literal ID="litVideo" runat="server"></asp:Literal>
                                                </div>
                                                <div data-role="collapsible">
                                                    <h3>
                                                        Điểm tích lũy</h3>
                                                        <div  style="width:200px;">
                                                     <span style="padding-top: 4px; display: inline-flex; font-weight: bold;">Mã số thẻ:
                                                        </span>
                                                        <asp:TextBox ID="txtCardCode" Style="padding: 4px 8px;" runat="server"></asp:TextBox>
                                                        &nbsp; &nbsp; &nbsp;
                                                        <asp:Button ID="btnCheck" Text="Kiểm tra" Style="cursor: pointer; padding-top: 4px;"
                                                            Font-Bold="true" runat="server" OnClick="btnCheck_Click" CausesValidation="false" /><br />
                                                        <br />
                                                        <asp:Literal ID="litCardInfo" runat="server"></asp:Literal>
                                                        </div>
                                                </div>
                                                <div data-role="collapsible">
                                                    <h3>
                                                        Tư vấn sử dụng</h3>
                                                    <asp:ListView ID="lstPrdTrick" runat="server">
                                                        <LayoutTemplate>
                                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                        </LayoutTemplate>
                                                        <ItemTemplate>
                                                            <li>
                                                                <div class="product-name">
                                                                    <a target="_blank" href="m-tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                        <%# Eval("Title")%></a>
                                                                    <%--lượt xem:
                                                            <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%>--%></div>
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:ListView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
