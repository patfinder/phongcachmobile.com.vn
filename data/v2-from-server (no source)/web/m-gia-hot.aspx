﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true"
    CodeBehind="m-gia-hot.aspx.cs" Inherits="PhongCachMobile.m_gia_hot" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Slider Starts -->
        <div class="banner">
            <div style="margin-bottom: 0px!important">
                <div style="padding-right: 0px;">
                    <div>
                        <asp:HyperLink ID="hpl1stTopBanner" runat="server"></asp:HyperLink>
                        <div class="banner_title_m" style="top: 150px;" id="div1stTopBanner" runat="server">
                            <asp:Literal ID="lit1stTopBanner" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div style="padding-top: 10px;">
                        <asp:HyperLink ID="hpl2ndTopBanner" runat="server"></asp:HyperLink>
                        <div class="banner_title_m" style="top: 265px;" id="div2ndTopBanner" runat="server">
                            <asp:Literal ID="lit2ndTopBanner" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div style="padding-top: 10px;">
                        <asp:HyperLink ID="hpl3rdTopBanner" runat="server"></asp:HyperLink>
                        <div class="banner_title_m" style="top: 372px;" id="div3rdTopBanner" runat="server">
                            <asp:Literal ID="lit3rdTopBanner" runat="server"></asp:Literal>
                        </div>
                    </div>
                    <div>
                        <asp:HyperLink ID="hpl4thTopBanner" runat="server"></asp:HyperLink></div>
                </div>
            </div>
        </div>
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                  <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                            <asp:Literal ID="litSubtitle" runat="server"></asp:Literal></h2>
                    </div>
                    <ul class="products-grid" style=" text-align: center; ">
                        <asp:Literal ID="litHotPrd" runat="server"></asp:Literal>
                    </ul>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
