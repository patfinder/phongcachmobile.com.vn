﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-loai-dtdd.aspx.cs" Inherits="PhongCachMobile.m_loai_dtdd" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <div class="index-pic-text">
            <div class="ui-responsive">
                  <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                             <asp:Literal ID="litMobile" runat="server"></asp:Literal></h2>
                    </div>
                    <div class="toolbar">
                                        <div class="pager">
                                            <div class="limiter">
                                                <label>
                                                    &nbsp; &nbsp; Mức giá</label>
                                                <asp:DropDownList ID="ddlPrice" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPrice_SelectedIndexChanged">
                                                        <asp:ListItem Text="Tất cả" Value="tat-ca" />
                                                        <asp:ListItem Text="Dưới 1 triệu" Value="duoi-1-trieu" />
                                                        <asp:ListItem Text="Dưới 2 triệu" Value="duoi-2-trieu" />
                                                        <asp:ListItem Text="Dưới 3 triệu" Value="duoi-3-trieu" />
                                                        <asp:ListItem Text="Dưới 5 triệu" Value="duoi-5-trieu" />
                                                        <asp:ListItem Text="Dưới 8 triệu" Value="duoi-8-trieu" />
                                                        <asp:ListItem Text="Dưới 10 triệu" Value="duoi-10-trieu" />
                                                        <asp:ListItem Text="Trên 10 triệu" Value="tren-10-trieu" />
                                                    </asp:DropDownList>
                                                <label>
                                                    &nbsp; &nbsp; Sắp xếp</label>
                                                <asp:DropDownList ID="ddlTopSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Mức giá giảm" Value="hightolowprice" />
                                                    <asp:ListItem Text="Mức giá tăng" Value="lowtohighprice" />
                                                    <asp:ListItem Text="Ngày đăng" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>


                 <asp:ListView ID="lstItem" runat="server">
                                        <LayoutTemplate>
                                            <ul class="products-grid" style=" text-align: center; ">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="item">
                                               <%# Eval("isOutOfStock").ToString() == "True" ? "<div class='badge-custom badge-tam-het'></div>" : "" %>
                                            <%# Eval("isComingSoon").ToString() == "True" ? "<div class='badge-custom badge-sap-co-hang'></div>" : "" %>
                                              <div class="desc">
                                                <h2 class="product-name product-name-height">
                                                    <a href="m-sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                        <%#Eval("Name")%></a></h2>
                                                <div class=" std">
                                                    <%#Eval("ShortDesc")%>
                                                </div>
                                                </div>
                                                <div class="grid-inner">
                                                    <a href="m-sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" class="product-image">
                                                        <img class="pr-img" src="images/product/<%#Eval("FirstImage")%>" width="150" style="max-height:150px;"
                                                            alt="<%#Eval("Name")%>"></a>
                                                </div>
                                                <div class="product-box-2">
                                                    <div class="product-atr-height">
                                                        <div class="price-box">
                                                            <span class="regular-price"><span class="price">
                                                                <%# Eval("sPrice") %>
                                                                đ</span> </span>
                                                        </div>
                                                    </div>
                                                    <div class="actions">
                                                        <a class="btnAddCart" href="m-sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                            <span>Xem chi tiết</span></a> <%# (Eval("DiscountPrice").ToString() != "") ? "<img src='images/hotprice.png' style='margin-top:-10px;' height='40px' alt='' />" : ((Eval("IsGifted").ToString() == "" || Eval("IsGifted").ToString() == "False") ? "" : "<img src='images/giftpack.png' style='margin-top:-10px;' height='40px' alt='' />")%> 
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>

                    <div class="toolbar">
                                        <div class="pager">
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                   <asp:DataPager ID="dpgLstItemBot" PageSize="6" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>

                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
