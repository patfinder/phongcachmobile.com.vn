﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="kho-may-cu.aspx.cs" Inherits="PhongCachMobile.kho_may_cu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <!-- Center -->
                <div id="center_column" class="center_column">
                </div>
                <div class="clearblock">
                </div>
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2 class="subtitle">
                                        <asp:Literal ID="litSubtitle" runat="server"></asp:Literal></h2>
                                </div>
                                <div id="tabPrd">
                                    <asp:Literal ID="litOldPrd" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstDiscountNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll" id="divPrdNews" runat="server">
                                    <div class="block-title">
                                        <strong><span>Tin thị trường</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstPrdNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll" id="divPoll" runat="server">
                                    <div class="block-title">
                                        <strong><span>
                                            <asp:Literal ID="litYourVote" runat="server"></asp:Literal></span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <p class="block-subtitle">
                                            <asp:Literal ID="litPollName" runat="server"></asp:Literal></p>
                                        <asp:RadioButtonList ID="rblPoll" runat="server">
                                        </asp:RadioButtonList>
                                        <asp:ListView ID="lstPollAnswer" runat="server">
                                            <LayoutTemplate>
                                                <ul style="display: inline-block;">
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </ul>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <li class="answer">
                                                    <%# Eval("Name") %>
                                                    <span>-
                                                        <%# Eval("Value") %>
                                                        (<%# Eval("Percent") %>%)</span> </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <div class="actions">
                                            <asp:Button ID="btnVote" CssClass="button" runat="server" Text="Bình chọn" OnClick="btnVote_Click" />
                                        </div>
                                    </div>
                                </div>
                                 <div class="block block-poll last_block" id="divBannerBot" runat="server"  style="border:none;">
                                    <asp:HyperLink ID="hplBannerBot" runat="server"></asp:HyperLink>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
