﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="qaq.aspx.cs" Inherits="PhongCachMobile.qaq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function replyAnswer(id, alias) {
            $('#content_hdfAnswerId').val(id);
            $('#content_txtAnswer').focus().val('@' + alias + ': ');
            $('#_answer').focus();
        }

        $(document).ready(function () {
            $('#emoticons img').click(function () {
                var smiley = $(this).attr('alt');
                ins2pos(smiley, 'content_txtAnswer');
            });

            function ins2pos(str, id) {
                var TextArea = document.getElementById(id);
                var val = TextArea.value;
                var before = val.substring(0, TextArea.selectionStart);
                var after = val.substring(TextArea.selectionEnd, val.length);

                TextArea.value = before + str + after;
                setCursor(TextArea, before.length + str.length);

            }

            function setCursor(elem, pos) {
                if (elem.setSelectionRange) {
                    elem.focus();
                    elem.setSelectionRange(pos, pos);
                } else if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                }
            }
        });
    </script>
    <script type="text/javascript">        var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">        stLight.options({ publisher: "da6af5b0-d769-45cc-8003-392451774e04", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="fb-root"></div>
<script>    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));</script>

    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2>Hỏi đáp</h2>
                                </div>
                                <div class="category-products" style="display: inline-block;">

                                            <ul class="news-grid">
                                               
                                            <li class="item ">
                                                <h3 class="product-name">
                                                    <a href="#">
                                                        <asp:Literal ID="litTitle" runat="server"></asp:Literal></a> 
                                                        <span style="padding-left:20px;color:#75c311;" id="spanSolved" runat="server">
                                                        <img width="25px;" src="../images/checkdone.png" alt="" /> Đã giải quyết</span></h3>
                                                <div class="info">
                                                    đăng bởi <span class="nameuser"><a href="#">
                                                        <asp:Literal ID="litEmailName" runat="server"></asp:Literal></a></span> vào <span class="timepost">
                                                            <asp:Literal ID="litDate" runat="server"></asp:Literal></span> <span class="viewcount"><i class="icon-eye-open"></i>
                                                                <span class="count">
                                                                    <asp:Literal ID="litViewCount" runat="server"></asp:Literal></span> lượt xem</span>
                                                    <span class="commentcount"><i class="icon-comment-alt"></i><span class="count">
                                                        <asp:Literal ID="litAnswerCount" runat="server"></asp:Literal></span> trả lời
                                                        &nbsp; &nbsp; &nbsp; </span>
                                                </div>
                                                <div class="desc">
                                                
                                                    <asp:Literal ID="litContent" runat="server"></asp:Literal><br /><br /></div>
                                                       <span class='st_facebook_hcount' displaytext='Facebook'></span><span class='st_twitter_hcount'
                                                displaytext='Tweet'></span><span class='st_googleplus_hcount' displaytext='Google +'>
                                                </span><span class='st_email_hcount' displaytext='Email'></span>
                                                <br /><br />
                                            </li>

                                            <li class="item ">
                                            <article class="grid_8 fleft" id="_answer">
                   		 <h2 class="p10"><asp:Literal ID="litAnswer" runat="server"></asp:Literal></h2> 
                         <div runat="server" id="form4" clientidmode="Static">
                            <figure class="avatar">
                                <asp:Image Width="50px" ID="imgAvatar" runat="server"></asp:Image>
                            </figure>
                            <div style="overflow:hidden">
                            <asp:TextBox ID="txtAnswer" Height="80" Width="100%" TextMode="MultiLine" runat="server" style="margin-bottom:7px;"></asp:TextBox>

                            <div id="emoticons">
                            <asp:Literal ID="litEmotion" runat="server"></asp:Literal>
                               
                            </div>
                             <asp:RequiredFieldValidator style="margin-top:8px;" ID="reqValAnswer" CssClass="fleft" Display="Dynamic" runat="server"
                                ControlToValidate="txtAnswer" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator style="margin-top:8px;" ForeColor="Red" CssClass="fleft" ID="rgeAnswerLength" Display="Dynamic" runat="server" 
                                ControlToValidate="txtAnswer" ValidationExpression="[\s\S]{20,1000}"></asp:RegularExpressionValidator>
                            <asp:LinkButton style="margin-top:13px" CssClass="button_large fright reg2" 
                                ID="lbtSend" runat="server" onclick="lbtSend_Click"></asp:LinkButton>
                            </div>
                            <br />
                            <div class="bar">
						<span class="total_comment"><asp:Literal ID="litTotalAnswer" runat="server"></asp:Literal></span>
                    </div>
                    <asp:HiddenField ID="hdfAnswerId" runat="server"></asp:HiddenField>
                    <asp:ListView ID="lstAnswer" runat="server">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                            <div class="comment <%# (((ListViewDataItem)Container).DataItemIndex)  % 10 == 9 ? "nobg" : "" %>">
                                <figure class="page1-img2">
                                    <img width="50px" src="<%#Eval("Avatar")%>">
                                </figure>
                                <div class="mheight extra-wrap">
                                    <span>
                                        <a class="link6"><%#Eval("Alias")%></a> - <%#Eval("PostedDate")%> 
                                    </span>

                                    <span class='fright'>#<%# Eval("Index") %></span><a onclick="return replyAnswer(<%# Eval("Id") %>, '<%# Eval("Alias") %>');" href="#_answer" class="fright link8">Trả lời</a>
                                    <br /><span><%#Eval("Description")%></span>
                                  
                                    </div>
                                </div>
                                    <div class="btmline"></div>
                                    <%# Eval("ChildAnswer") %>
                            </ItemTemplate>
                        </asp:ListView>

                        <br />
                                   <div class="pagenavi">
                    <asp:DataPager ID="dpLstAnswer" runat="server" PagedControlID="lstAnswer" PageSize="10"
                        OnPreRender="dpLstAnswer_PreRender">
                        <Fields>
                            <asp:NumericPagerField NumericButtonCssClass="inactive" CurrentPageLabelCssClass="current" />
                        </Fields>
                    </asp:DataPager>
                </div>

                            <%--<div class="comment"><figure class="page1-img2"><img width="50px" src="images/avatar/7.png" title="Tiêu đề tin tức 5" alt="Tiêu đề tin tức 5"></figure><div class="extra-wrap"><span><a class="link6" href="thong-tin-tiêu-đề-tin-tức-5-7" title="Tiêu đề tin tức 5">Tiêu đề tin tức 5</a></span><br><span>Lượt xem: <font color="red">5.213</font></span><br><span>Giới thiệu ngắn về tin tức 5</span></div></div>--%>
                            </div>
                    </article>
                                        </li>

                                        </ul>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                              <div class="block block-poll " id="divSolved" visible="false" runat="server">
                                    <div class="block-title">
                                        <strong><span>Xác nhận đã giải quyết</span></strong>
                                    </div>

                                    <div class="block-content" style="background:white;">
                                        <ul class="ulRecentNews">
                                         <li>
                                                    <p style="line-height:18px;">Nếu vấn đề đã được trả lời, hãy click vào nút bên dưới để đánh dấu vấn đề đã được giải quyết.</p>
                                                     <p style="text-align:center;padding-top:10px;"><asp:LinkButton CssClass="button_large" CausesValidation="false" ID="lbtSolved" runat="server" onclick="lbtSolved_Click">Đã giải quyết</asp:LinkButton></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Chúng tôi trên Facebook</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                      <div class="fb-like-box" data-href="https://www.facebook.com/phongcachmobile.com.vn"  data-width="219" data-height="330" data-colorscheme="light" data-show-faces="true" data-header="true" data-show-border="false"></div>
                                    </div>
                                </div>
                                <div class="block block-poll last_block">
                                    <div class="block-title">
                                        <strong><span>Quy định tham gia</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <p style="line-height:18px;">- Sử dụng <font style="font-weight:bold;color:#ff3708">tiếng Việt có dấu</font> khi tham gia.</p>
                                                <p style="line-height:18px;">- <font style="font-weight:bold;color:#ff3708">Chủ đề</font> cần ngắn gọn, đầy đủ.</p>
                                                <p style="line-height:18px;">- <font style="font-weight:bold;color:#ff3708">Nội dung câu hỏi</font> phải phù hợp với chủ đề.</p>
                                                <p style="line-height:18px;">- Khi vấn đề đã được giải quyết, vui lòng check vào nút <font style="font-weight:bold;color:#ff3708">đã giải quyết</font>.</p>
                                                <p style="line-height:18px;">- Mọi câu hỏi không phù hợp với quy định sẽ được <font style="font-weight:bold;color:#ff3708">xóa mà không cần báo trước</font>.</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
