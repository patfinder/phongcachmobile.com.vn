﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="chi-tiet-cam-nhan.aspx.cs" Inherits="PhongCachMobile.chi_tiet_cam_nhan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class='block'>
                                <div class='block-title'>
                                    <strong><span>
                                        <asp:Literal ID="litTestimonial" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content" style="background: white; padding: 15px 30px 16px 30px;
                                    line-height: 18px; color: #515151;">
                                    <h3 style="font-size: 12px;text-transform: uppercase;color: #be2603;margin-bottom: 8px;">
                                        <asp:Literal ID="litName" runat="server"></asp:Literal></h3>

                                    <p style="font-style:italic">
                                        <asp:Literal ID="litShortDesc" runat="server"></asp:Literal></p>
                                    <asp:Literal ID="litValue" runat="server"></asp:Literal>
                                    <br /><br />
                                    <div class="about-padd">
                                        <div class="wrapper">
                                        <asp:ListView ID="lstTestimonial" runat="server">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>

                            <div class="testimonial" <%# (((ListViewDataItem)Container).DataItemIndex)  % 3 == 2 ? "style='margin-right:0px;'" : "style='margin-right:16px;'" %>>
                                            <a href='<%#Eval("UrlName")%>'  title="<%#Eval("Name")%>" ><img src="../images/testimonial/<%#Eval("DisplayImage")%>" alt="" width="320px;" /></a>
                                            <div style="padding: 13px">
                                                <h3 style="font-size: 12px;text-transform: uppercase;color: #be2603;margin-bottom: 8px;">
                                                    <%#Eval("Name")%></h3>
                                                <p>
                                                    <%#Eval("Text")%></p>
                                                    </div>
                                            </div>
                            </ItemTemplate>
                        </asp:ListView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
