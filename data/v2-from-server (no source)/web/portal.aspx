﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="portal.aspx.cs" Inherits="PhongCachMobile.portal" %>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head runat="server">
 <link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="keywords" content="phongcachmobile, phong cach mobile, phong cách mobile, mua điện thoại ở đâu uy tín, cửa hàng điện thoại xách tay uy tín nhất, cửa hàng điện thoại giá rẻ nhất, nên mua điện thoại xách tay ở đâu" />
    <meta name="description" content="phongcachmobile, phong cach mobile, phong cách mobile, mua điện thoại ở đâu uy tín, cửa hàng điện thoại xách tay uy tín nhất, cửa hàng điện thoại giá rẻ nhất, nên mua điện thoại xách tay ở đâu" />
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="True" />
    <meta name="MobileOptimized" content="320" />
    <meta http-equiv="cleartype" content="on" />
    <script>        (function (a, b, c) { if (c in b && b[c]) { var d, e = a.location, f = /^(a|html)$/i; a.addEventListener("click", function (a) { d = a.target; while (!f.test(d.nodeName)) d = d.parentNode; "href" in d && (d.href.indexOf("http") || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href) }, !1) } })(document, window.navigator, "standalone")</script>
    <!-- Stylesheets -->
    <link rel='stylesheet' href='css/pcmb.css' />
    <link rel='stylesheet' href='css/index.css' />
</head>
<body class="dark light-foot">
    <!--[if lt IE 7]>
      <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <div class="preloader js-preloader">
    </div>
    <!-- This allows for opening and closing of the main nav -->
    <div class="web global-container js-global-container">
        <nav class="global-nav js-global-nav">

    <div class="actions yui3-g">
      <a href="#" class="close js-menu yui3-u-1-2 res-s-hidden">
        <span class="icon-close"></span>
      </a>
      
        <a href="dang-nhap" class="signin yui3-u-1-2 res-s-hidden">
          Đăng nhập
        </a>
        <a href="dang-ky" class="signup yui3-u-1">
          Đăng ký
        </a>
      
    </div>

    <p class="primary">
      <a href="gia-hot">
        Giá hot
      </a>
      <a href="dtdd">
        Điện thoại
      </a>
       <a href="may-tinh-bang">
        Máy tính bảng
      </a>
        <a href="kho-may-cu">
        Kho máy cũ
      </a>
    </p>

    <div class="secondary">
      <p><a href="game">Kho game</a></p>
      <p><a href="ung-dung">Ứng dụng</a></p>
      <p><a href="phu-kien">Phụ kiện</a></p>
      <p><a href="khuyen-mai">Khuyến mãi</a></p>
      <p><a href="tin-thi-truong">Tin thi trường</a></p>
      <p><a href="gioi-thieu">Về PhongCachMobile</a></p>
      <p><a href="sua-chua-cai-dat">Hỗ trợ</a></p>
      <p>
        
          <a href="dang-nhap" class="res-m-hidden res-l-hidden">
            Đăng nhập
          </a>
        
      </p>
    </div>

      <form class="language-picker js-language-picker" action="#">
        <label for="language">Ngôn ngữ</label>
        <select id="language" class="lang-select js-lang-select">
          
            <option value="vi-VN">
              Tiếng Việt
            </option>
        </select>
        <span class="dd-arrow icon-signup-downarrow"></span>
      </form>

    

   <%-- <div class="downloads res-s-hidden">
      <a href="https://itunes.apple.com/us/app/uber/id368677368?mt=8"
         target="_blank" class="apple-phone ir">Apple iPhone</a>
      <a href="https://play.google.com/store/apps/details?id=com.ubercab"
         target="_blank" class="google-phone ir">Android</a>
      <a href="https://itunes.apple.com/us/app/uber/id368677368?mt=8"
         target="_blank" class="apple-store ir">Apple Store</a>
      <a href="https://play.google.com/store/apps/details?id=com.ubercab"
         target="_blank" class="google-play ir">Google Play Store</a>
    </div>--%>

</nav>
        <section class="site-wrapper js-site-wrapper">
        <div class="global-nav-overlay js-global-nav-overlay"></div>
        <div class="head-fade"></div>

        <header class="global-header js-global-header">
          <div class="pull-left">
            <a href="#" class="menu js-menu">
              <span class="icon-menu"></span>
              <span class="menu-text res-s-hidden">Chuyến đến</span>
            </a>
          </div>

          <p class="pull-right actions res-s-hidden">
            
              <a href="dang-nhap" class="ga-click-track"
                 data-ga="global-header.login-button-click">
               Đăng nhập
              </a>
              <a href="dang-ky" class="btn ga-click-track"
                 data-ga="global-header.sign-up-button-click">
                Trở thành thành viên
              </a>
            
          </p>
          <div class="yui3-u-1-3 center block-center" style="width: initial;">
            <a href="gia-hot" class="pcmb-logo ir ga-click-track"
               data-ga="global-header.logo-click">&nbsp;</a>
          </div>

        </header>



  <section class="hero js-home-hero">
  <asp:Literal ID="litNavigation" runat="server"></asp:Literal>
 <%-- <nav class="controller js-home-controller">
    <a class="slide-trigger active ga-click-track" href="#"
       data-hero-slide="moving-people">
      <span class="blue-dot"></span>
      <span class="slide-trigger">1</span>
    </a>
    <a class="slide-trigger ga-click-track" href="#"
       data-hero-slide="mark-it">
      <span class="blue-dot"></span>
      <span class="slide-trigger">2</span>
    </a>
    <a class="slide-trigger ga-click-track" href="#"
       data-hero-slide="night-out">
      <span class="blue-dot"></span>
      <span class="slide-trigger">3</span>
    </a>
    <a class="slide-trigger ga-click-track" href="#"
       data-hero-slide="be-the-boss">
      <span class="blue-dot"></span>
      <span class="slide-trigger">4</span>
    </a>
  </nav>--%>
   <asp:Literal ID="litSlider" runat="server"></asp:Literal>
 <%-- <article class="active hero-slide js-home-slide" data-slide="moving-people">
    <div class="headline">
       <h1>SẢN PHẨM CHẤT LƯỢNG</h1>
      <p>PHONGCACHMOBILE tự hào mang đến cho quý khách hàng những sản phẩm chất lượng chính hãng với mức giá tốt nhất</p>
    </div>
     <img src="images/home-hero-2-1440-900.jpg"
         class="js-hisrc-preloader" />
    <img class='hisrc'
         src="images/home-hero-2-1440-900.jpg"
         data-1x="images/home-hero-2-1440-900.jpg"
         style="height: auto; width: 100%; " />
  </article>

  <article class="hero-slide js-home-slide" data-slide="mark-it">
    <div class="headline">
     <h1>BẢO HÀNH YÊN TÂM</h1>
      <p>Luôn luôn chú trọng vào công tác bảo hành để mang đến sự yên tâm trong suốt quá trình sử dụng của khách hàng</p>
    </div>
    <img class='hisrc'
         src="images/home-hero-3-1440-900.jpg"
         data-1x="images/home-hero-3-1440-900.jpg"
         style="height: auto; width: 100%; " />
  </article>

  <article class="hero-slide js-home-slide" data-slide="night-out">
    <div class="headline">
      <h1>HẬU MÃI ĐA DẠNG</h1>
      <p>Các chính sách ưu đãi tốt nhất - mang đến những giá trị, những tiện nghi mua sắm hài lòng nhất cho khách hàng</p>
    </div>
    <img class='hisrc'
         src="images/home-hero-4-1440-900.jpg"
         data-1x="images/home-hero-4-1440-900.jpg"
         style="height: auto; width: 100%; " />
  </article>

  <article class="hero-slide js-home-slide" data-slide="be-the-boss">
    <div class="headline">
      <h1>HỖ TRỢ NHIỆT TÌNH</h1>
      <p>Đội ngũ nhân viên bán hàng và kỹ thuật luôn hết lòng phục vụ và hỗ trợ khách hàng</p>
    </div>
    <img class='hisrc'
         src="images/home-hero-5-1440-900.jpg"
         data-1x="images/home-hero-5-1440-900.jpg"
         style="height: auto; width: 100%; " />
  </article>--%>

  <%--<article class="hero-slide js-home-slide" data-slide="arrive-style">
    <div class="headline">
      <h1>Hỗ trợ nhiệt tình</h1>
      <p>Nhân viên kĩ thuật luôn xem chất lượng phục vụ khách hàng là quan trọng nhất</p>
    </div>
    <img class='hisrc'
         src="images/home-hero-5-1440-900.jpg"
         data-1x="images/home-hero-5-1440-900.jpg"
         data-2x="images/home-hero-5-2880-1800.jpg"
         style="height: auto; width: 100%; " />
  </article>--%>

  <%--<article class="hero-slide js-home-slide" data-slide="go-international">
    <div class="headline">
      <h1>Tư vấn chuyên nghiệp</h1>
      <p>PhongCachMobile nắm bắt thị hiếu của khách hàng để đem đến những sản phẩm tốt nhất</p>
    </div>
    <img class='hisrc'
         src="images/home-hero-6-1440-900.jpg"
         data-1x="images/home-hero-6-1440-900.jpg"
         data-2x="images/home-hero-6-2880-1800.jpg"
         style="height: auto; width: 100%; " />
  </article>--%>

</section>
<%--<section class="request-and-ride js-request-and-ride">
  <div class="grid-locked grid-squeezed">
<article >
  <p class="label">Lĩnh vực kinh doanh</p>
  <a href="dtdd"><h3 style='color:black'>Phong Cách Mobile</h3></a>
  <p>Có "phong cách" để khẳng định "cái tôi", có "phong cách" để khẳng định "điều khác biệt"</p>

  <ul class="story js-story yui3-g">
    
    <li class="yui3-u-1-3 yui3-u-m-1-2 yui3-u-s-1-2 tap-to-ride ga-click-track"
        data-ga="home-pcmb-app.tap-to-ride-box-click"
        data-modal-name="dtdd-mtb">
      <div>
        <div class="title-box">
          <h4 class="headline">Mua bán<br />Điện thoại di động &<br />Máy tính bảng</h4>
          <span class="plus icon-thin_plus"></span>
        </div>
        <div class="overlay"></div>
      </div>
    </li>
    
    <li class="yui3-u-1-3 yui3-u-m-1-2 yui3-u-s-1-2 reliable-pickup ga-click-track"
        data-ga="home-pcmb-app.reliable-pickup-box-click"
        data-modal-name="reliable-pickup">
      <div>
        <div class="title-box">
          <h4 class="headline">Sửa chữa</h4>
          <span class="plus icon-thin_plus"></span>
        </div>
        <div class="overlay"></div>
      </div>
    </li>
    
    <li class="yui3-u-1-3 yui3-u-m-1-2 yui3-u-s-1-2 clear-pricing ga-click-track"
        data-ga="home-pcmb-app.clear-pricing-box-click"
        data-modal-name="clear-pricing">
      <div>
        <div class="title-box">
          <h4 class="headline">Cài đặt</h4>
          <span class="plus icon-thin_plus"></span>
        </div>
        <div class="overlay"></div>
      </div>
    </li>
    
    <li class="yui3-u-1-3 yui3-u-m-1-2 yui3-u-s-1-2 cashless-convenient ga-click-track"
        data-ga="home-pcmb-app.cashless-convenient-box-click"
        data-modal-name="cashless-convenient">
      <div>
        <div class="title-box">
          <h4 class="headline">Game & ứng dụng<br />bản quyền</h4>
          <span class="plus icon-thin_plus"></span>
        </div>
        <div class="overlay"></div>
      </div>
    </li>
    
    <li class="yui3-u-1-3 yui3-u-m-1-2 yui3-u-s-1-2 feedback-matters ga-click-track"
        data-ga="home-pcmb-app.feedback-matters-box-click"
        data-modal-name="feedback-matters">
      <div>
        <div class="title-box">
          <h4 class="headline">Bảo dưỡng đinh kỳ<br />3 tháng miễn phí</h4>
          <span class="plus icon-thin_plus"></span>
        </div>
        <div class="overlay"></div>
      </div>
    </li>
    
    <li class="yui3-u-1-3 yui3-u-m-1-2 yui3-u-s-1-2 split-fare ga-click-track"
        data-ga="home-pcmb-app.split-fare-box-click"
        data-modal-name="split-fare">
      <div>
        <div class="title-box">
          <h4 class="headline">Chép phim HD<br />miễn phí</h4>
          <span class="plus icon-thin_plus"></span>
        </div>
        <div class="overlay"></div>
      </div>
    </li>
    
  </ul>
</article>

<article class="modal-gallery js-modal-gallery">
  <a href="#" class="close-modal js-close-modal icon-modal-close"></a>

  <div class="modal-container">
    <a href="#" class="last btn icon-left_thin"></a>

    <ul >
      
        <li class="modal-slide tap-to-ride"
            data-modal-slide="dtdd-mtb">
          <p class="feature-image js-preload"></p>
          <p class="stick"></p>
          <div class="copy">
            <a href="dtdd"><h3 style='color:White'>Mua bán điện thoại di động và máy tính bảng</h3></a>
            <p>Sản phẩm kinh doanh được xách tay trực tiếp từ các quốc gia khu vực Châu Âu, Mỹ, Singapore, Ấn Độ, Hồng Kông, Úc... , được tuyển chọn kỹ càng  và chỉ tập trung vào những sản phẩm "thời trang" nhất, "công nghệ" nhất và "phong cách" nhất để đáp ứng được gu phong cách khó tính nhất của giới trung và thượng lưu.</p>
          </div>
        </li>
      
        <li class="modal-slide reliable-pickup"
            data-modal-slide="reliable-pickup">
          <p class="feature-image js-preload"></p>
          <p class="stick"></p>
          <div class="copy">
            <a href="dtdd"><h3 style='color:White'>Sửa chữa</h3></a>
            <p>Với bề dày kinh nghiệm kết hợp với tinh thần học hỏi và nghiên cứu, Kỹ thuật viên của PHONGCACHMOBILE tự hào là một trong số những KTV có tay nghề cao nhất tại Tp.HCM, có thể sửa chữa được những "bệnh" khó nhất của di động đặc biệt là những dòng cao cấp hiện nay của các hãng danh tiếng như iPhone, HTC, Nokia, Sony Ericsson, Samsung, LG, Motorola, Blackberry ... 
Chúng tôi hân hạnh được phục vụ tất cả quý khách với tinh thần và trách nhiệm cao nhất !</p>
          </div>
        </li>
      
        <li class="modal-slide clear-pricing"
            data-modal-slide="clear-pricing">
          <p class="feature-image js-preload"></p>
          <p class="stick"></p>
          <div class="copy">
             <a href="dtdd"><h3 style='color:White'>Cài đặt</h3></a>
            <p>Chúng tôi kiểm tra và cài đặt máy theo đúng yêu cầu của khách hàng một cách chuyên nghiệp trong thời gian nhanh nhất.</p>
          </div>
        </li>
      
        <li class="modal-slide cashless-convenient"
            data-modal-slide="cashless-convenient">
          <p class="feature-image js-preload"></p>
          <p class="stick"></p>
          <div class="copy">
            <a href="dtdd"><h3 style='color:White'>Game &amp; ứng dụng bản quyền</h3></a>
            <p>Đến với PhongCachMobile, quý khách hàng sẽ được hưởng những ưu đãi từ dịch vụ cài đặt game và ứng dụng có bản quyền miễn phí hot nhất hiện nay.</p>
          </div>
        </li>
      
        <li class="modal-slide feedback-matters"
            data-modal-slide="feedback-matters">
          <p class="feature-image js-preload"></p>
          <p class="stick"></p>
          <div class="copy">
            <a href="dtdd"><h3 style='color:White'>Bảo dưỡng định kỳ 3 tháng miễn phí</h3></a>
            <p>Trung tâm sửa chữa - bảo hành kỹ thuật viên cam kết chất lượng linh kiện và tính trung thực trong quá trình sửa chữa. Mọi trường hợp trái với cam kết, chúng tôi hoàn tiền gấp đôi cho quý khách hàng.</p>
          </div>
        </li>
      
        <li class="modal-slide split-fare"
            data-modal-slide="split-fare">
          <p class="feature-image js-preload"></p>
          <p class="stick"></p>
          <div class="copy">
              <a href="dtdd"><h3 style='color:White'>Chép phim HD miễn phí</h3></a>
            <p>Chúng tôi cung cấp dịch vụ chép phim HD cho điện thoại của khách hàng. Những phim nổi bật trong thời điểm hiện tại với chất lượng cao sẽ mang đến cho bạn những giờ phút thực sự thư giãn.</p>
          </div>
        </li>
      
    </ul>

    <a href="#" class="next btn icon-right_thin"></a>
  </div>
</article>


  </div>
</section>--%>

<%--<section class="vehicle-fleet yui3-g">
  <div class="bg-top"></div>
  <div class="grid-locked grid-squeezed">

    <article class="content js-content yui3-u-1">

      <p class="label inverted">Thương hiệu</p>
      <h3>Chọn điện thoại "phong cách"</h3>
      <p>Để thể hiện "gu" và "cá tính" của mỗi chúng ta</p>

      <section class="pcmbx active vehicle-slide js-vehicle-slide"
               data-vehicle-slide="pcmbx">
        <div class="sunrise-box">
          <h4>Thương hiệu được ưa chuộng hàng đầu</h4>
          <p class="sunrise">iPhone</p>
        </div>
        <div class="vehicle-info">
          <p class="vehicle-img pcmbx">iPhone</p>
          <div class="copy">
            <p>Hiện đại, tinh tế trong thiết kế vỏ máy.<br />Hoàn hảo với từng tính năng.</p>
          </div>
        </div>
      </section>
      <section class="taxi vehicle-slide js-vehicle-slide"
               data-vehicle-slide="taxi">
        <div class="sunrise-box">
          <h4>Thiết kế tinh xảo, hiện đại &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </h4>
          <p class="sunrise">Samsung</strong></p>
        </div>
        <div class="vehicle-info">
          <p class="vehicle-img js-preload">Taxi</p>
          <div class="copy">
            <p>Khả năng quay video với độ phân giải 4K<br /> Bộ vi xử lý lõi tứ của Qualcomm đạt tốc độ 2,3 GHz</p>
          </div>
        </div>
      </section>
      <section class="black vehicle-slide js-vehicle-slide"
               data-vehicle-slide="black">
        <div class="sunrise-box">
          <h4>Hiệu ứng âm thanh, quay phim, màn hình tốt nhất</h4>
          <p class="sunrise">HTC</strong></p>
        </div>
        <div class="vehicle-info">
          <p class="vehicle-img js-preload">Black</p>
          <div class="copy">
            <p>Công nghệ Ultrapixel tích hợp chụp hình cực tốt<br />Cảm biến camera mới cho phép thu sáng nhiều gấp 3 lần camera 8 megapixel truyền thống.</p>
          </div>
        </div>
      </section>
      <section class="suv vehicle-slide js-vehicle-slide"
               data-vehicle-slide="suv">
        <div class="sunrise-box">
          <h4>Điện thoại Full HD mỏng nhất thế giới</h4>
          <p class="sunrise">Sony</strong></p>
        </div>
        <div class="vehicle-info">
          <p class="vehicle-img js-preload">Sony</p>
          <div class="copy">
            <p>Máy sở hữu màn hình cực lớn 6.4 inch<br />Sử dụng bút cảm ứng, bút chì, bút thường.</p>
          </div>
        </div>
      </section>
      <section class="lux vehicle-slide js-vehicle-slide"
               data-vehicle-slide="lux">
        <div class="sunrise-box">
          <h4>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Thương hiệu lâu đời</h4>
          <p class="sunrise">Nokia</strong></p>
        </div>
        <div class="vehicle-info">
          <p class="vehicle-img js-preload">Nokia</p>
          <div class="copy">
            <p>Màu sắc trẻ trung, năng động.<br />Giá thành hợp lý dành cho người sành điệu.</p>
          </div>
        </div>
      </section>

    </article>

    <article class="vehicle-chooser js-vehicle-chooser res-s-hidden yui3-u-1">

      <div class="vehicles js-vehicles yui3-g">
        <a href="#" data-icon-name="icon-pcmbx" data-slide-type="pcmbx"
           class="active yui3-u-1-5 pcmbx">IPHONE</a>
        <a href="#" data-icon-name="icon-taxi" data-slide-type="taxi"
           class="yui3-u-1-5">Samsung</a>
        <a href="#" data-icon-name="icon-black" data-slide-type="black"
           class="yui3-u-1-5">HTC</a>
        <a href="#" data-icon-name="icon-suv" data-slide-type="suv"
           class="yui3-u-1-5">Sony</a>
        <a href="#" data-icon-name="icon-lux" data-slide-type="lux"
           class="yui3-u-1-5">Nokia</a>
      </div>

      <div class="slider-bar js-slider-bar">
        <p href="#" class="slider-circle js-slider">
          <span class="car icon-pcmbx"></span>
        </p>
      </div>

    </article>

    <!-- TODO Make partials and only load what is needed here -->
    <!-- TODO Make this dynamic data -->
    <nav class="mobile-nav js-mobile-nav res-l-hidden res-m-hidden yui3-u-1">

      <div class="pcmbx">
        <a class="vehicle-toggle js-vehicle-toggle" href="#">
          uber<strong>X</strong>
          <span class="closed mobile-arrow icon-signup-downarrow"></span>
          <span class="open mobile-arrow icon-signup-uparrow"></span>
        </a>
        <div class="data">
          <div class="sunrise-box"><p class="sunrise">pcmbx</p></div>
          <p class="vehicle-img js-preload">pcmbx</p>
          <div class="info">
            <h5>The low-cost Uber</h5>
            <p>Everyday cars for everyday use.<br />Better, faster, and cheaper than a taxi.</p>
          </div>
        </div>
      </div>

      <div class="taxi">
        <a class="vehicle-toggle js-vehicle-toggle" href="#">
          uber<strong>TAXI</strong>
          <span class="closed mobile-arrow icon-signup-downarrow"></span>
          <span class="open mobile-arrow icon-signup-uparrow"></span>
        </a>
        <div class="data">
          <div class="sunrise-box"><p class="sunrise">uberTAXI</p></div>
          <p class="vehicle-img js-preload taxi">uberTAXI</p>
          <div class="info">
            <h5>Taxi without the hassle</h5>
            <p>No whistling, no waving, no cash needed.</p>
          </div>
        </div>
      </div>

      <div class="black">
        <a class="vehicle-toggle js-vehicle-toggle" href="#">
          Uber<strong>BLACK</strong>
          <span class="closed mobile-arrow icon-signup-downarrow"></span>
          <span class="open mobile-arrow icon-signup-uparrow"></span>
        </a>
        <div class="data">
          <div class="sunrise-box"><p class="sunrise">UberBLACK</p></div>
          <p class="vehicle-img js-preload">UberBLACK</p>
          <div class="info">
            <h5>The original</h5>
            <p>Your own private driver, on demand.<br />Expect pickup in a high-end sedan within minutes.</p>
          </div>
        </div>
      </div>

      <div class="suv">
        <a class="vehicle-toggle js-vehicle-toggle" href="#">
          Uber<strong>SUV</strong>
          <span class="closed mobile-arrow icon-signup-downarrow"></span>
          <span class="open mobile-arrow icon-signup-uparrow"></span>
        </a>
        <div class="data">
          <div class="sunrise-box"><p class="sunrise">UberSUV</p></div>
          <p class="vehicle-img js-preload">UberSUV</p>
          <div class="info">
            <h5>Room for everyone</h5>
            <p>For those times when you need a bit more space.<br />Seats up to six people in style.</p>
          </div>
        </div>
      </div>

      <div class="lux">
        <a class="vehicle-toggle js-vehicle-toggle" href="#">
          Uber<strong>LUX</strong>
          <span class="closed mobile-arrow icon-signup-downarrow"></span>
          <span class="open mobile-arrow icon-signup-uparrow"></span>
        </a>
        <div class="data">
          <div class="sunrise-box"><p class="sunrise">UberLUX</p></div>
          <p class="vehicle-img js-preload">UberLUX</p>
          <div class="info">
            <h5>Ultimate luxury</h5>
            <p>The finest cars with prices to match.</p>
          </div>
        </div>
      </div>

    </nav>

  </div>
</section>--%>

<%--<section class="around-the-world">
  <div class="grid-locked grid-squeezed">

    <article class="map-title">

      <p class="label inverted">Sản phẩm nhập khẩu</p>
      <h3>Chủ yếu từ Châu Âu</h3>
      <p>và các quốc gia khác trên thế giới</p>
      <a class="cities-link" href="#">Chúng tôi tự tin mang đến cho khách hàng sản phẩm tốt nhất với giá thành cạnh tranh</a>

    </article>

  </div>
</section>--%>
<br /><br /><br /><br />
      </section>
        <%--<footer class="global-footer js-global-footer yui3-g">
  <div class="grid-locked">

    <a href="/app" class="sign-up yui3-u-1 res-l-hidden res-m-hidden ga-click-track"
       data-ga="global-footer.download-mobile">
      Download Uber
    </a>

    <article class="social yui3-u-1-4 yui3-u-s-1">
      <a href="https://www.facebook.com/phongcachmobile.com.vn" target="_blank"
         class="icon icon-facebook ga-click-track"
         data-ga="global-footer.facebook-button-click"></a>
    </article>

    <article class="navs yui3-u-1-2 res-s-hidden">
      <a href="dang-ky" class="sign-up ga-click-track"
         data-ga="global-footer.sign-up-button-web-click">
        Đăng ký làm thành viên
      </a>
      <nav class="primary">
        <a href="gia-hot" class="ga-click-track"
           data-ga="global-footer.home-button-click">
          Giá hot
        </a>
        <span>&bull;</span>
        <a href="dtdd" class="ga-click-track"
           data-ga="global-footer.cities-button-click">
          Điện thoại di động
        </a>
        <span>&bull;</span>
        <a href="may-tinh-bang" class="ga-click-track"
           data-ga="global-footer.drivers-button-click">
          Máy tính bảng
        </a>
      </nav>
      <nav class="secondary">
        <a href="gioi-thieu" class="ga-click-track"
           data-ga="global-footer.about-button-click">
          Giới thiệu
        </a>
        <a href="quy-dinh-bao-hanh" target="_blank">
          Quy định bảo hành
        </a>
        <a href="sua-chua-cai-dat" class="ga-click-track"
           data-ga="global-footer.jobs-button-click">
          Sửa chữa và cài đặt
        </a>
        
      </nav>
        <a href="#" class="current-lng">
          Tiếng Việt
        </a>

    </article>


  </div>
</footer>--%>
    </div>
    <!-- End of global-container -->
    <script src="/javascripts/vendor/require-2.1.5.min.js" data-main="/javascripts/path/index-path.js"></script>
</body>
</html>
