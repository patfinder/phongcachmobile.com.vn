﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="trang-chu.aspx.cs" Inherits="PhongCachMobile.trang_chu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="js/liteaccordion.jquery.js"></script>
    <script src="js/tms-0.3.js" type="text/javascript"></script>
    <script src="js/tms_presets.js" type="text/javascript"></script>
    <script src="js/jquery.cycle.all.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
        $(window).load(function () {
            $('.slider')._TMS({
                prevBu: '.prev',
                nextBu: '.next',
                playBu: '.play',
                duration: 700,
                easing: 'easeOutQuad',
                preset: 'simpleFade',
                pagination: true,
                //pagNums:false,
                slideshow: 8000,
                numStatus: false,
                banners: 'fade', // fromLeft, fromRight, fromTop, fromBottom
                waitBannerAnimation: false
            })
        })
    </script>
    <script type="text/javascript">
        jQuery('#slideshow').cycle({
            fx: 'scrollHorz',
            speed: 'fast',
            pager: '#pagination1',
            timeout: 8000,
            speed: 700,
            cleartype: true,
            cleartypeNoBg: true
        });
    </script>
      <script type="text/javascript">
          function tabPrd() {
              jQuery('#tabAcs').hide("slow");
              jQuery('#tabPrd').show("slow");
          }

          function tabAcs() {
              jQuery('#tabAcs').show("slow");
              jQuery('#tabPrd').hide("slow");
          }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <!-- Center -->
                <div id="center_column" class="center_column">
                    <div id="tmslider1">
                        <div id="one" style="width: 1062px; height: 427px;" class="accordion basic">
                            <ol>
                                <asp:ListView ID="lstTopSlider" runat="server">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li class="slide">
                                            <h2>
                                                <span class="slide_name">
                                                    <%#Eval("Title")%></span><span class="slide_number">0<%# (((ListViewDataItem)Container).DataItemIndex) + 1 %></span></h2>
                                            <div>
                                                <div>
                                                    <a href="<%#Eval("ActionUrl")%>">
                                                        <img src="images/slide/<%#Eval("DisplayImage")%>" alt=""></a>
                                                    <div>
                                                        <h2>
                                                            <a href="<%#Eval("ActionUrl")%>">
                                                                <%#Eval("Title")%></a></h2>
                                                        <%#Eval("Text")%>
                                                        <a class="tmslider1_btn" href="<%#Eval("ActionUrl")%>">Xem ngay</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ol>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('#one').liteAccordion({
                            onTriggerSlide: function () {
                                this.find('figcaption').fadeOut();
                            },
                            onSlideAnimComplete: function () {
                                this.find('figcaption').fadeIn();
                            },
                            autoPlay: true,
                            pauseOnHover: true,
                            theme: 'basic',
                            rounded: false,
                            enumerateSlides: false
                        }).find('figcaption:first').show();
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#carousel').bxSlider({
                                /*	auto: true,*/
                                autoStart: true,
                                auto: true,
                                speed: 700,
                                pause: 2000,
                                displaySlideQty: 4,
                                moveSlideQty: 1
                            });
                        });
                    </script>
                    <!-- tmspecials -->
                    <div id="tmspecials">
                        <h4>
                            <asp:Literal ID="litSpecialProduct" runat="server"></asp:Literal></h4>
                        <div class="block_content">
                            <asp:ListView ID="lstSpecial" runat="server">
                                <LayoutTemplate>
                                    <ul id="carousel" style="width: 999999px; position: relative; left: -256px;">
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li class="pager" style="width: 256px; float: left; list-style: none;">
                                        <div>
                                            <a class="product_image" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>">
                                                <img src="images/product/<%# Eval("FirstImage")%>" width="180px" alt="<%# Eval("Name")%>"></a>
                                            <h5>
                                                <a href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>">
                                                    <%# Eval("Name")%></a></h5>
                                            <span class="reduction">
                                                <%# int.Parse(Eval("DiscountPrice").ToString()).ToString("#,##0")%>
                                                đ</span> <span class="price">
                                                    <%# int.Parse(Eval("CurrentPrice").ToString()).ToString("#,##0")%>
                                                    đ</span> <a class="button" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>">Xem</a>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                    <!-- /tmspecials -->
                    <!-- MODULE Home Featured Products -->
                    <div id="featured_products">
                        <h4>
                            <asp:Literal ID="litFeaturedProduct" runat="server"></asp:Literal></h4>
                        <div class="block_content">
                            <ul>
                                <asp:ListView ID="lstFeatured" runat="server">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li class="ajax_block_product"><a class="product_image" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>"
                                            title="<%# Eval("Name")%>">
                                            <img src="images/product/<%# Eval("FirstImage")%>" width="180px" alt="<%# Eval("Name")%>"></a>
                                            <div>
                                                <h5>
                                                    <a href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>" title="<%# Eval("Name")%>">
                                                        <%# Eval("Name")%></a></h5>
                                                <span class="price">
                                                    <%# int.Parse(Eval("DiscountPrice").ToString() == "" ? Eval("CurrentPrice").ToString() : Eval("DiscountPrice").ToString()).ToString("#,##0")%>
                                                    đ</span> <a class="button" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>" title="View">
                                                        Xem</a>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ul>
                            <div class="clearblock">
                            </div>
                        </div>
                    </div>
                    <!-- /MODULE Home Featured Products -->
                </div>
                <div class="clearblock">
                </div>
                <div class="slider-block">
                    <div class="slider">
                        <ul class="items">
                            <asp:ListView ID="lstBotSlider" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li>
                                        <img alt="" src="images/slide/<%# Eval("DisplayImage") %>" />
                                        <div class="banner">
                                            <h4>
                                                <%# Eval("Title") %></h4>
                                            <%# Eval("Text") %>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </div>
                </div>
                <div class="banners">
                    <asp:ListView ID="lstBanner" runat="server">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div>
                                <a href="<%# Eval("ActionURL") %>">
                                    <img alt="" src="images/banner/<%# Eval("DisplayImage") %>" /></a>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
                <div class="clear">
                </div>
                <div class="clearblock">
                </div>
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2 class="subtitle" >
                                       <a href="javascript:tabPrd();">Sản phẩm mới</a> &nbsp; | &nbsp; <a  href="javascript:tabAcs();">Phụ kiện mới</a></h2>
                                </div>
                                <div id="tabPrd">
                                    <asp:Literal ID="litNewPrd" runat="server"></asp:Literal>
                                </div>
                                 <div id="tabAcs" style="display:none;">
                                    <asp:Literal ID="litNewAcs" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                      <ul class="ulRecentNews">
                                      <asp:ListView ID="lstDiscountNews" runat="server">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                             <li>
                                                <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>" width="60px" class="newsImg" />
                                                </a>
                                                <div class="product-name"><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>"><%# Eval("Title")%></a> <%# Eval("TimeSpan")%></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    </ul>
                                    </div>
                                </div>
                                <div class="block block-poll last_block" id="divPrdNews" runat="server">
                                    <div class="block-title">
                                        <strong><span>Tin thị trường</span></strong>
                                    </div>
                                    <div class="block-content" style="background:white;">
                                        <ul class="ulRecentNews">
                                      <asp:ListView ID="lstPrdNews" runat="server">
                                        <LayoutTemplate>
                                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                             <li>
                                                <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>" width="60px" class="newsImg" />
                                                </a>
                                                <div class="product-name"><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>"><%# Eval("Title")%></a> <%# Eval("TimeSpan")%></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    </ul>
                                    </div>
                                </div>

                                <div class="block block-poll last_block" id="divPoll" runat="server">
                                    <div class="block-title">
                                        <strong><span>
                                            <asp:Literal ID="litYourVote" runat="server"></asp:Literal></span></strong>
                                    </div>
                                    <div class="block-content" style="background:white;">
                                        <p class="block-subtitle">
                                            <asp:Literal ID="litPollName" runat="server"></asp:Literal></p>
                                            <asp:RadioButtonList ID="rblPoll" runat="server">
                                            </asp:RadioButtonList>
                                        <asp:ListView ID="lstPollAnswer" runat="server">
                                            <LayoutTemplate>
                                                <ul style="display:inline-block;">
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </ul>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <li class="answer">
                                                   <%# Eval("Name") %> <span> - <%# Eval("Value") %> (<%# Eval("Percent") %>%)</span>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <div class="actions">
                                            <asp:Button ID="btnVote" CssClass="button" runat="server" Text="Bình chọn" 
                                                onclick="btnVote_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
