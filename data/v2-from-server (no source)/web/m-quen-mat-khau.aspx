﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-quen-mat-khau.aspx.cs" Inherits="PhongCachMobile.m_quen_mat_khau" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                            <asp:Literal ID="litRegister" runat="server"></asp:Literal></h2>
                    </div>
                   <div class="register">
                                            <div class="content">
                                                <h2>Thông tin tài khoản</h2>
                                                    <div id="frmInput" runat="server">
                                                <ul class="form-list">
                                                    <li>
                                                        <p>Để thiết lập lại mật khẩu cho tài khoản. Bạn hãy cung cấp thông tin tài khoản - địa chỉ email vào ô bên dưới.</p>
                                                        <label for="email" class="required">
                                                            Tài khoản</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <asp:Label ID="lblIsAvailabelAccount" runat="server"></asp:Label>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtEmail"  CssClass="input-text required-entry validate-email" runat="server"></asp:TextBox>
                                                            &nbsp;
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="buttons-set">
                                                 <asp:Label ID="lblEmailResponse" runat="server"></asp:Label><br />
                                                    <asp:LinkButton ID="lbtSend" Text="Gửi" class="button_large" runat="server" onclick="lbtSend_Click"></asp:LinkButton>
                                                </div>
                                                </div>
                                                 <div id="forgetPasswordResponse" class="p8" visible="false" runat="server">
                                                 <p style="line-height:18px;">
                                                    <asp:Literal ID="litForgetPasswordResponse" runat="server"></asp:Literal>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
