﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="tin-khuyen-mai.aspx.cs" Inherits="PhongCachMobile.tin_khuyen_mai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="fb-root"></div>
<script>    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
        fjs.parentNode.insertBefore(js, fjs);
    } (document, 'script', 'facebook-jssdk'));</script>

    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2>Tin tức khuyến mãi</h2>
                                </div>
                                <div class="category-products" style="display: inline-block;">
                                    <asp:ListView ID="lstNews" runat="server">
                                        <LayoutTemplate>
                                            <ul class="news-grid">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="item ">
                                                <h3 class="product-name">
                                                    <a href="tt-<%#Eval("UrlTitle")%>-<%#Eval("Id")%>" title="<%#Eval("Title")%>">
                                                        <%#Eval("Title")%></a></h3>
                                                <div class="info">
                                                    đăng bởi <span class="nameuser"><a href="#">
                                                        <%#Eval("Fullname")%></a></span> vào <span class="timepost">
                                                            <%#Eval("sPostedDate")%></span> <span class="viewcount"><i class="icon-eye-open"></i>
                                                                <span class="count">
                                                                    <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%></span> lượt xem</span>
                                                    <span class="commentcount"><i class="icon-comment-alt"></i><span class="count">
                                                        <%#int.Parse(Eval("CommentCount").ToString()).ToString("#,##0")%></span> bình luận
                                                        &nbsp; &nbsp; &nbsp; </span>
                                                </div>
                                                <a href="tt-<%#Eval("UrlTitle")%>-<%#Eval("Id")%>" title="<%#Eval("Title")%>" class="product-image">
                                                    <img src="images/news/<%#Eval("DisplayImage")%>" width="100%" alt="<%#Eval("Title")%>"></a>
                                                <div class="desc">
                                                    <%#Eval("ShortDesc")%></div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </div>
                                <div class="toolbar">
                                    <div class="pager">
                                        <p class="amount">
                                            <asp:Literal ID="litBotItemsIndex" runat="server"></asp:Literal>
                                        </p>
                                        <div class="limiter">
                                            <label>
                                                Hiển thị</label>
                                            <asp:DropDownList ID="ddlBotDisplay" runat="server" OnSelectedIndexChanged="ddlBotDisplay_SelectedIndexChanged"
                                                AutoPostBack="True">
                                                <asp:ListItem Text="5" Value="5" />
                                                <asp:ListItem Text="10" Value="10" />
                                                <asp:ListItem Text="15" Value="15" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="pages">
                                            <strong>Trang:</strong>
                                            <ol>
                                                <asp:DataPager ID="dpgLstItemBot" PageSize="5" PagedControlID="lstNews" runat="server"
                                                    OnPreRender="dpgLstItem_PreRender">
                                                    <Fields>
                                                        <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                    </Fields>
                                                </asp:DataPager>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Chúng tôi trên Facebook</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                      <div class="fb-like-box" data-href="https://www.facebook.com/phongcachmobile.com.vn"  data-width="219" data-height="330" data-colorscheme="light" data-show-faces="true" data-header="true" data-show-border="false"></div>
                                    </div>
                                </div>
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin xem nhiều nhất</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                         <asp:ListView ID="lstHotNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            lượt xem:  <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>

                              <div class="block block-poll last_block" id="divPrdNews" runat="server">
                                    <div class="block-title">
                                        <strong><span>Chủ đề bình luận sôi nổi</span></strong>
                                    </div>

                                    <div class="block-content" style="background:white;">
                                        <ul class="ulRecentNews">
                                         <asp:ListView ID="lstCommentNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            lượt bình luận:  <%#int.Parse(Eval("CommentCount").ToString()).ToString("#,##0")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
