﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-chat-luong-hang-hoa.aspx.cs" Inherits="PhongCachMobile.m_chat_luong_hang_hoa" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">CHẤT LƯỢNG HÀNG HÓA</h2>
                    </div>
                    <asp:Literal ID="litMProductQuality" runat="server"></asp:Literal>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
