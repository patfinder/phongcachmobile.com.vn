﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-kho-may-cu.aspx.cs" Inherits="PhongCachMobile.m_kho_may_cu" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                 <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                            <asp:Literal ID="litSubtitle" runat="server"></asp:Literal></h2>
                    </div>
                   <ul class="products-grid" style=" text-align: center; ">
                        <asp:Literal ID="litOldPrd" runat="server"></asp:Literal>
                    </ul>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
