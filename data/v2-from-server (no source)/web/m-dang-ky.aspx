﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true"
    CodeBehind="m-dang-ky.aspx.cs" Inherits="PhongCachMobile.m_dang_ky" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                 <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                            <asp:Literal ID="litRegister" runat="server"></asp:Literal></h2>
                    </div>
                    <div class="register">
                        <div class="content">
                            <h2>
                                Thông tin đăng ký tài khoản</h2>
                            <div id="frmInput" runat="server">
                                <ul class="form-list">
                                    <li>
                                        <p>
                                            Thông tin về tỉnh / thành phố nơi bạn đang sống và làm việc sẽ giúp chúng tôi phục
                                            vụ tốt hơn.</p>
                                        <label>
                                            Tỉnh / Thành phố</label>
                                        <asp:DropDownList ID="ddlState" runat="server">
                                        </asp:DropDownList>
                                    </li>
                                    <li>
                                        <p>
                                            Để đảm bảo cho việc kích hoạt tài khoản, và tham gia các hoạt động của <font style='color: red;
                                                font-weight: bold;'>PhongCachMobile</font>. Chúng tôi đề nghị bạn sử dụng email
                                            thật để đăng ký</p>
                                        <label for="email" class="required">
                                            Tài khoản</label>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                            Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                            Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        <asp:Label ID="lblIsAvailabelAccount" runat="server"></asp:Label>
                                        <div class="input-box">
                                            <asp:TextBox ID="txtEmail" OnTextChanged="txtEmail_TextChanged" AutoPostBack="True"
                                                CssClass="input-text required-entry validate-email" runat="server"></asp:TextBox>
                                            &nbsp;
                                        </div>
                                    </li>
                                    <li>
                                        <label for="pass" class="required">
                                            Mật khẩu</label>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="reqValPassword" Display="Dynamic" runat="server"
                                            ControlToValidate="txtPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ForeColor="Red" ID="rgePasswordLength" Display="Dynamic"
                                            runat="server" ControlToValidate="txtPassword" ValidationExpression="[\s\S]{6,30}"></asp:RegularExpressionValidator>
                                        <div class="input-box">
                                            <asp:TextBox ID="txtPassword" TextMode="Password" class="input-text required-entry validate-password"
                                                runat="server"></asp:TextBox>
                                        </div>
                                    </li>
                                    <li>
                                        <label for="pass" class="required">
                                            Nhập lại mật khẩu</label>
                                        &nbsp;
                                        <asp:RequiredFieldValidator ID="reqValRePassword" Display="Dynamic" runat="server"
                                            ControlToValidate="txtPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ForeColor="Red" ID="rgeRePasswordLength" Display="Dynamic"
                                            runat="server" ControlToValidate="txtPassword" ValidationExpression="[\s\S]{6,30}"></asp:RegularExpressionValidator>
                                        <asp:CompareValidator ForeColor="Red" ID="cmpValRePassword" runat="server" ControlToCompare="txtPassword"
                                            ControlToValidate="txtRePassword" Operator="Equal" Display="Dynamic"></asp:CompareValidator>
                                        <div class="input-box">
                                            <asp:TextBox ID="txtRePassword" TextMode="Password" class="input-text required-entry validate-password"
                                                runat="server"></asp:TextBox>
                                        </div>
                                    </li>
                                </ul>
                                <div class="buttons-set">
                                    <asp:LinkButton ID="lbtRegister" Text="Đăng ký" class="button_large" runat="server"
                                        OnClick="lbtRegister_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <div id="registerResponse" class="p8" visible="false" runat="server">
                                <p style="line-height: 18px;">
                                    <asp:Literal ID="litRegisterResponse" runat="server"></asp:Literal>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
