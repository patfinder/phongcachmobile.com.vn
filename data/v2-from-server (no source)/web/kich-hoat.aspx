﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="kich-hoat.aspx.cs" Inherits="PhongCachMobile.kich_hoat" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litActivated" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content account-login" style="background: white; padding: 25px 30px 16px 30px;">
                                    <div class="col2-set">
                                        <div class="col-2 register" style="margin-right: 25px;">
                                            <div class="content">
                                                <h2>Kích hoạt tài khoản</h2>
                                                 <div id="resetActivatedTokenResponse" class="p8" runat="server">
                                                 <p style="line-height:18px;">
                                                    <asp:Literal ID="litActivateResponse" runat="server"></asp:Literal>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1 features">
                                            <div class="content">
                                                <h2>
                                                    Điểm nổi bật của chúng tôi</h2>
                                                <ul class="feature">
                                                    <li>Điện thoại nhập từ châu Âu</li>
                                                    <li>Sản phẩm chất lượng với giá cả cạnh tranh</li>
                                                    <li>Chế độ bảo hành chu đáo</li>
                                                    <li>Chương trình hậu mãi đa dạng</li>
                                                    <li>Đỗi ngũ kĩ thuật viên hỗ trợ nhiệt tình</li>
                                                    <li>Nhân viên tư vấn chuyên nghiệp</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
