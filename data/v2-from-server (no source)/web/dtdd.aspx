﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="dtdd.aspx.cs" Inherits="PhongCachMobile.dtdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/tm3DCircleCarousel.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="css/category.css" rel="stylesheet" type="text/css" />
    <script src="js/tms-0.3.js" type="text/javascript"></script>
    <script src="js/tms_presets.js" type="text/javascript"></script>
    <script src="js/jquery.cycle.all.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
  <script src="js/tm3DCircleCarousel.js" type="text/javascript"></script>
     <script type="text/javascript">
         $(window).load(function () {
             $('.slider')._TMS({
                 prevBu: '.prev',
                 nextBu: '.next',
                 playBu: '.play',
                 duration: 700,
                 easing: 'easeOutQuad',
                 preset: 'simpleFade',
                 pagination: true,
                 //pagNums:false,
                 slideshow: 8000,
                 numStatus: false,
                 banners: 'fade', // fromLeft, fromRight, fromTop, fromBottom
                 waitBannerAnimation: false
             })
         })
    </script>
    <script type="text/javascript">
        jQuery('#slideshow').cycle({
            fx: 'scrollHorz',
            speed: 'fast',
            pager: '#pagination1',
            timeout: 8000,
            speed: 700,
            cleartype: true,
            cleartypeNoBg: true
        });
    </script>
      <script type="text/javascript">
          function tabPrd() {
              jQuery('#tabAcs').hide("slow");
              jQuery('#tabPrd').show("slow");
          }

          function tabAcs() {
              jQuery('#tabAcs').show("slow");
              jQuery('#tabPrd').hide("slow");
          }

  </script>

    <script type="text/javascript">

        var win = $(window),
    doc = $(document),
    $splashGallery;

        function initCarousel() {
            // init carousel
            $splashGallery = $('.splash');
            $splashGallery
        .tooltip({
            track: true
        })
        .tm3DCircleCarousel({
            container: '.splashHolder',
            transformClasses: '.scale100, .scale90, .scale80, .scale70, .scale60, .scale50',
            clickableClasses: '.scale100, .scale90',
            itemOffset: 108,//328
            itemOffsetCenter: 78, //78
            useCSS3Animation: true,
            autoplay: {
                enable: true,
                timeout: 3000
            },
            onChange: function (element, currInd, length) {
            },
            onShowActions: function (e) {
                $splashGallery.tooltip('enable');
            },
            onHideActions: function (e) {
                $splashGallery.tooltip('enable');
            },
            onUserActivate: function (e) {
                $splashGallery.tooltip('disable');
            }
        });
        }


        /*---------------------- end ready -------------------------------*/

        win
.load(function () {
    initCarousel();
});

//        $(document).ready(function () {
//            jQuery(".link-compare").easyTooltip();
//        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="page-title category-title">
                                <h2>
                                    <asp:Literal ID="litMobile" runat="server"></asp:Literal></h2>
                            </div>
                            	<article id="content" style="padding-top:3px;">
		<div id="splashPage">
			<div class="splashHolder">
             <asp:Literal ID="litSlider" runat="server"></asp:Literal>
				
				<a href="#" class="prevButton"><span></span></a>
				<a href="#" class="nextButton"><span></span></a>
			</div>
		</div>
		
	</article>
	
<%--
                            <div id="minic_slider" class="theme-default">
                                <asp:Literal ID="litSlider" runat="server"></asp:Literal>
                            </div>
                            <script type="text/javascript">
                                $(window).load(function () {
                                    $('#slider').nivoSlider({
                                        effect: 'random',
                                        slices: 15,
                                        animSpeed: 500,
                                        pauseTime: 7000,
                                        startSlide: 0,
                                        directionNav: true,
                                        controlNav: false,
                                        controlNavThumbs: false,
                                        pauseOnHover: true,
                                        manualAdvance: false,
                                        prevText: 'Prev',
                                        nextText: 'Next',
                                        randomStart: false
                                    });
                                });
                            </script>--%>
                           <%-- <div id="customcontent_top" class=" clearfix">
                                <ul>
                                    <asp:Literal ID="litBanner" runat="server"></asp:Literal>
                                </ul>
                            </div>--%>
                            <div class="brands">
                                <asp:Literal ID="litBrand" runat="server"></asp:Literal>
                            </div>

                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#carousel').bxSlider({
                                        /*	auto: true,*/
                                        autoStart: true,
                                        auto: true,
                                        speed: 700,
                                        pause: 2000,
                                        displaySlideQty: 4,
                                        moveSlideQty: 1
                                    });
                                });
                            </script>
                            <div id="tmspecials">
                        <h4>
                            TOP 10 ĐƯỢC QUAN TÂM NHẤT TRONG TUẦN</h4>
                        <div class="block_content">
                            <asp:ListView ID="lstSpecial" runat="server">
                                <LayoutTemplate>
                                    <ul id="carousel" style="width: 999999px; position: relative; left: -256px;">
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </ul>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li class="pager" style="width: 180px; float: left; list-style: none;">
                                        <div>
                                            <a class="product_image" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>">
                                                <img src="images/product/<%# Eval("FirstImage")%>" width="180px" alt="<%# Eval("Name")%>"></a>
                                            <h5>
                                                <a href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>">
                                                    <%# Eval("Name")%></a></h5>
                                            <span class="reduction">
                                                <%# int.Parse(Eval("DiscountPrice").ToString() == "" ? Eval("CurrentPrice").ToString() : Eval("DiscountPrice").ToString()).ToString("#,##0")%>
                                                đ</span> <a class="button" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>">Xem</a>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                      <div id="featured_products">
                        <h4>
                            MỚI RA MẮT</h4>
                        <div class="block_content">
                            <ul>
                                <asp:ListView ID="lstFeatured" runat="server">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li class="ajax_block_product"><a class="product_image" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>"
                                            title="<%# Eval("Name")%>">
                                            <img src="images/product/<%# Eval("FirstImage")%>" width="180px" alt="<%# Eval("Name")%>"></a>
                                            <div>
                                                <h5>
                                                    <a href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>" title="<%# Eval("Name")%>">
                                                        <%# Eval("Name")%></a></h5>
                                                <span class="price">
                                                    <%# int.Parse(Eval("DiscountPrice").ToString() == "" ? Eval("CurrentPrice").ToString() : Eval("DiscountPrice").ToString()).ToString("#,##0")%>
                                                    đ</span> <a class="button" href="sp-<%# Eval("UrlName")%>-<%# Eval("Id")%>" title="View">
                                                        Xem</a>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ul>
                            <div class="clearblock">
                            </div>
                        </div>
                    </div>
                    <div class="slider-block">
                    <div class="slider">
                        <ul class="items">
                            <asp:ListView ID="lstBotSlider" runat="server">
                                <LayoutTemplate>
                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <li>
                                    <a href="<%# Eval("ActionURL") %>">
                                        <img alt="" src="images/slide/<%# Eval("DisplayImage") %>" />
                                        </a>
                                        <div class="banner">
                                            <h4>
                                                <%# Eval("Title") %></h4>
                                            <%# Eval("Text") %>
                                        </div>
                                    </li>
                                </ItemTemplate>
                            </asp:ListView>
                        </ul>
                    </div>
                </div>
                <div class="banners">
                    <asp:ListView ID="lstBanner" runat="server">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div>
                                <a href="<%# Eval("ActionURL") %>">
                                    <img alt="" src="images/banner/<%# Eval("DisplayImage") %>" /></a>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
                <div class="clear">
                </div>
                            <div class="col-main">
                                <div class="category-products">
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litTopItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị</label>
                                                <asp:DropDownList ID="ddlTopDisplay" runat="server" OnSelectedIndexChanged="ddlTopDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="12" Value="12" />
                                                    <asp:ListItem Text="20" Value="20" />
                                                    <asp:ListItem Text="28" Value="28" />
                                                </asp:DropDownList>
                                                <label>
                                                    &nbsp; &nbsp; &nbsp; Sắp xếp</label>
                                                <asp:DropDownList ID="ddlTopSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Mức giá giảm" Value="hightolowprice" />
                                                    <asp:ListItem Text="Mức giá tăng" Value="lowtohighprice" />
                                                    <asp:ListItem Text="Ngày đăng" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemTop" PageSize="12" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lstItem" runat="server">
                                        <LayoutTemplate>
                                            <ul class="products-grid" style="display: inline-block;">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                       
                                            <li class="item <%# (((ListViewDataItem)Container).DataItemIndex) % 4 == 3 ? "last" : "" %> ">
                                               <%# Eval("isOutOfStock").ToString() == "True" ? "<div class='badge-custom badge-tam-het'></div>" : "" %>
                                            <%# Eval("isComingSoon").ToString() == "True" ? "<div class='badge-custom badge-sap-co-hang'></div>" : "" %>
                                            <div class="desc">
                                                <h2 class="product-name product-name-height">
                                                    <a href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                        <%#Eval("Name")%></a></h2>
                                                <div class=" std">
                                                    <%#Eval("ShortDesc")%>
                                                </div>
                                                </div>
                                                <div class="grid-inner">
                                                    <a href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" class="product-image">
                                                        <img class="pr-img" src="images/product/<%#Eval("FirstImage")%>" style="width:auto; max-height:150px;"
                                                            alt="<%#Eval("Name")%>"></a>
                                                </div>
                                                <div class="product-box-2">
                                                    <div class="product-atr-height">
                                                        <div class="price-box">
                                                            <span class="regular-price" id="product-price-26"><span class="price" <%# Eval("isOutOfStock").ToString() == "True" ? "style='color:#515151'" : "" %>>
                                                                <%# Eval("sPrice") %>
                                                                đ</span> </span> 
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="actions">
                                                        <a class="btnAddCart" href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" <%# Eval("isOutOfStock").ToString() == "True" ? "style='background: #515151;'" : "" %>>
                                                            <span>chi tiết </span></a> <%# (Eval("DiscountPrice").ToString() != "") ? "<img src='images/hotprice.png' style='margin-top:-10px;' height='40px' alt='' />" : ((Eval("IsGifted").ToString() == "" || Eval("IsGifted").ToString() == "False") ? "" : "<img src='images/giftpack.png' style='margin-top:-10px;' height='40px' alt='' />")%> 
                                                        <%-- <ul class="add-to-links">
                                                            <li>
                                                                <asp:LinkButton ToolTip="Thêm vào so sánh"  ID="lbtCompare" CommandName="addCompare" CssClass="link-compare" runat="server">
                                                                Thêm vào so sánh</asp:LinkButton>
                                                            </li>
                                                        </ul>--%>
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litBotItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị</label>
                                                <asp:DropDownList ID="ddlBotDisplay" runat="server" OnSelectedIndexChanged="ddlBotDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="12" Value="12" />
                                                    <asp:ListItem Text="20" Value="20" />
                                                    <asp:ListItem Text="28" Value="28" />
                                                </asp:DropDownList>
                                                <label>
                                                    &nbsp; &nbsp; &nbsp; Sắp xếp</label>
                                                <asp:DropDownList ID="ddlBotSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBotSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Mức giá giảm" Value="hightolowprice" />
                                                    <asp:ListItem Text="Mức giá tăng" Value="lowtohighprice" />
                                                    <asp:ListItem Text="Ngày đăng" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemBot" PageSize="12" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Lọc sản phẩm</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <div class="product-filter">
                                                    <p>
                                                        MỨC GIÁ</p>
                                                    <asp:DropDownList ID="ddlPrice" runat="server" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="ddlPrice_SelectedIndexChanged">
                                                        <asp:ListItem Text="Tất cả" Value="tat-ca" />
                                                        <asp:ListItem Text="Dưới 1 triệu" Value="duoi-1-trieu" />
                                                        <asp:ListItem Text="Dưới 2 triệu" Value="duoi-2-trieu" />
                                                        <asp:ListItem Text="Dưới 3 triệu" Value="duoi-3-trieu" />
                                                        <asp:ListItem Text="Dưới 5 triệu" Value="duoi-5-trieu" />
                                                        <asp:ListItem Text="Dưới 8 triệu" Value="duoi-8-trieu" />
                                                        <asp:ListItem Text="Dưới 10 triệu" Value="duoi-10-trieu" />
                                                        <asp:ListItem Text="Trên 10 triệu" Value="tren-10-trieu" />
                                                    </asp:DropDownList>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="product-filter">
                                                    <p>
                                                        HỆ ĐIỀU HÀNH</p>
                                                    <asp:DropDownList ID="ddlOS" runat="server" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="ddlOS_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </li>
                                            <%-- <li>
                                                <div class="product-filter">
                                                    <p>TÍNH NĂNG SẢN PHẨM</p>
                                                    <asp:DropDownList ID="ddlFeatures" runat="server" Width="100%" 
                                                        AutoPostBack="True" onselectedindexchanged="ddlFeatures_SelectedIndexChanged">
                                                   
                                                    </asp:DropDownList>
                                                </div>
                                            </li>--%>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litTablet" runat="server"></asp:Literal>
                                   <%-- <a href="may-tinh-bang">
                                        <asp:Image ID="imgTablet" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                <asp:Literal ID="litAccessory" runat="server"></asp:Literal>
                                   <%-- <a href="phu-kien">
                                        <asp:Image ID="imgAccessory" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                <asp:Literal ID="litApplication" runat="server"></asp:Literal>
                                   <%-- <a href="ung-dung">
                                        <asp:Image ID="imgApplication" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                <asp:Literal ID="litGame" runat="server"></asp:Literal>
                                    <%--<a href="game">
                                        <asp:Image ID="imgGame" runat="server" />
                                    </a>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
