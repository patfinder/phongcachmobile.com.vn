﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="dang-nhap.aspx.cs" Inherits="PhongCachMobile.dang_nhap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litLogin" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content account-login" style="background: white; padding: 25px 30px 16px 30px;">
                                    <div class="col2-set">
                                        <div class="col-2 registered-users" style="margin-right: 25px;">
                                            <div class="content">
                                                <h2>
                                                    Bạn đã là thành viên</h2>
                                                <p>
                                                    Nếu bạn đã có tài khoản, hãy đăng nhập.</p>
                                                <ul class="form-list">
                                                    <li>
                                                        <label for="email" class="required" style="width: 100%;">
                                                            Tài khoản &nbsp; </label>
                                                        <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtEmail" CssClass="input-text required-entry validate-email" runat="server"></asp:TextBox>
                                                            &nbsp;
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <label for="pass" class="required">
                                                            Mật khẩu</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValPassword" Display="Dynamic" runat="server"
                                                            ControlToValidate="txtPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ForeColor="Red" ID="rgePasswordLength" Display="Dynamic"
                                                            runat="server" ControlToValidate="txtPassword" ValidationExpression="[\s\S]{6,30}"></asp:RegularExpressionValidator>
                                                     
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtPassword" TextMode="Password" class="input-text required-entry validate-password"
                                                                runat="server"></asp:TextBox>
                                                        </div>
                                                    </li>
                                                </ul>
                                                      <p> <asp:Label ID="lblLoginResponse" runat="server"></asp:Label></p>
                                                <div class="buttons-set">
                                                    <a href="quen-mat-khau" class="f-left">Quên mật khẩu?</a>
                                                    <asp:LinkButton ID="lbtLogin" Text="Đăng nhập" class="button_large" runat="server"
                                                        OnClick="lbtLogin_Click"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1 new-users" style="margin-right:25px;">
                                            <div class="content">
                                                <h2>
                                                    Thành viên mới?</h2>
                                              <p>Bằng cách tạo tài khoản, bạn sẽ dễ dàng quản lý thông tin tài khoản, tham dự chương trình khuyến mãi, và các hoạt động cộng đồng của <font style='color:red;font-weight:bold;'>PhongCachMobile</font></p>
                                                <div class="buttons-set">
                                                      <a href="dang-ky" class="button_large"><span><span>Đăng ký miễn phí &nbsp;</span></span></a>
                                                </div>
                                            </div>
                                        </div>

                                          <div class="col-1 features">
                                            <div class="content">
                                                <h2>Điểm nổi bật của chúng tôi</h2>
                                                  <ul class="feature">
                                                    <li>"Since 2009", là điểm kinh doanh uy tín bậc nhất các sản phẩm ĐTDĐ / Máy tính bảng / Phụ kiện cao cấp</li>
                                                    <li>Sản phẩm đa dạng, chất lượng chuẩn từ nhà sản xuất chính hãng</li>
                                                    <li>Giá bán được cập nhật liên tục theo giá thế giới và luôn là giá tốt nhất.</li>
                                                    <li>Dịch vụ hậu mãi, bảo hành chu đáo và trung thực.</li>
                                                    <li>Đội ngũ nhân viên thân thiện, nhiệt tình.</li>
                                                </ul>
                                               
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
