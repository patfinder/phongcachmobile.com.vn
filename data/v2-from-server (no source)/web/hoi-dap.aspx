﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="hoi-dap.aspx.cs" Inherits="PhongCachMobile.hoi_dap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="fb-root">
    </div>
    <script>        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));</script>
    
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2>
                                        Hỏi đáp</h2>
                                </div>
                                <div class="category-products" style="display: inline-block;">
                                    <ul class="news-grid">
                                        <li class="item ">
                                            <p style="padding-bottom:7px;"><span style="font-weight:bold;">CHỦ ĐỀ</span></p>
                                          <asp:TextBox ID="txtTitle" Width="98%" style="padding:5px;" runat="server" ></asp:TextBox><br /><br />
                                             <p style="padding-bottom:7px;"><span style="font-weight:bold;">NỘI DUNG CÂU HỎI</span></p>
                                             <asp:TextBox ID="txtAsk" Height="100" Width="98%" TextMode="MultiLine" runat="server" ></asp:TextBox>
                                            <br />
                                              <asp:RequiredFieldValidator style="margin-top:8px;" ID="reqValAsk" CssClass="fleft" Display="Dynamic" runat="server"
                                ControlToValidate="txtAsk" ForeColor="Red"></asp:RequiredFieldValidator>
                                   <asp:RequiredFieldValidator style="margin-top:8px;" ID="reqValTitle" CssClass="fleft" Display="Dynamic" runat="server"
                                ControlToValidate="txtTitle" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator style="margin-top:8px;" ForeColor="Red" CssClass="fleft" ID="rgeAskLength" Display="Dynamic" runat="server" 
                                ControlToValidate="txtAsk" ValidationExpression="[\s\S]{20,1000}"></asp:RegularExpressionValidator>
                            <asp:LinkButton style="margin-top:13px" CssClass="button_large fright reg2" 
                                ID="lbtAsk" runat="server" onclick="lbtAsk_Click"></asp:LinkButton>

                                <br /><br /><br /><br />
                                        </li>
                                        <asp:ListView ID="lstQAQ" runat="server">
                                            <LayoutTemplate>
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <li class="item ">
                                                    <h3 class="product-name">
                                                        <a href="qaq-<%#Eval("UrlTitle")%>-<%#Eval("Id")%>" title="<%#Eval("Title")%>">
                                                            <%#Eval("Title")%></a>
                                                        <%#Eval("isSolved").ToString() == "True" ? 
                                                        "<span style='padding-left:20px;color:#75c311;'><img width='25px;' src='../images/checkdone.png' alt='' /> Đã giải quyết</span>":""%></h3>
                                                    <div class="info">
                                                        đăng bởi <span class="nameuser"><a href="#">
                                                            <%#Eval("Emailname")%></a></span> vào <span class="timepost">
                                                                <%#Eval("sPostedDate")%></span> <span class="viewcount"><i class="icon-eye-open"></i>
                                                                    <span class="count">
                                                                        <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%></span> lượt xem</span>
                                                        <span class="commentcount"><i class="icon-comment-alt"></i><span class="count">
                                                            <%#int.Parse(Eval("CommentCount").ToString()).ToString("#,##0")%></span> trả lời
                                                            &nbsp; &nbsp; &nbsp; </span>
                                                    </div>
                                                    <div class="desc">
                                                        <%#Eval("Ask")%></div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </ul>
                                </div>
                                <div class="toolbar">
                                    <div class="pager">
                                        <p class="amount">
                                            <asp:Literal ID="litBotItemsIndex" runat="server"></asp:Literal>
                                        </p>
                                        <div class="limiter">
                                            <label>
                                                Hiển thị</label>
                                            <asp:DropDownList ID="ddlBotDisplay" runat="server" OnSelectedIndexChanged="ddlBotDisplay_SelectedIndexChanged"
                                                AutoPostBack="True">
                                                <asp:ListItem Text="10" Value="10" />
                                                <asp:ListItem Text="15" Value="15" />
                                                <asp:ListItem Text="20" Value="20" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="pages">
                                            <strong>Trang:</strong>
                                            <ol>
                                                <asp:DataPager ID="dpgLstItemBot" PageSize="10" PagedControlID="lstQAQ" runat="server"
                                                    OnPreRender="dpgLstItem_PreRender">
                                                    <Fields>
                                                        <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                    </Fields>
                                                </asp:DataPager>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Lọc hỏi đáp</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <div class="product-filter">
                                                    <p>
                                                        SẮP XẾP</p>
                                                    <asp:DropDownList ID="ddlSort" runat="server" Width="100%" AutoPostBack="True" OnSelectedIndexChanged="ddlSort_SelectedIndexChanged">
                                                        <asp:ListItem Text="Mới nhất" Value="moi-nhat" />
                                                        <asp:ListItem Text="Lượt xem" Value="luot-xem" />
                                                        <asp:ListItem Text="Lượt trả lời" Value="luot-tra-loi" />
                                                    </asp:DropDownList>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll " id="divPrdNews" runat="server">
                                    <div class="block-title">
                                        <strong><span>Thống kê hỏi đáp</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <div>
                                                    <p>
                                                        <span class="qaq-stat">
                                                            <asp:Literal ID="litNumberAsk" runat="server"></asp:Literal></span> câu hỏi</p>
                                                    <p>
                                                        <span class="qaq-stat">
                                                            <asp:Literal ID="litNumberAnswer" runat="server"></asp:Literal></span> lượt
                                                        trả lời</p>
                                                    <p>
                                                        <span class="qaq-stat">
                                                            <asp:Literal ID="litNumberUser" runat="server"></asp:Literal></span> thành viên
                                                        tham gia</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Chúng tôi trên Facebook</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <div class="fb-like-box" data-href="https://www.facebook.com/phongcachmobile.com.vn"  data-width="219" data-height="330" data-colorscheme="light" data-show-faces="true" data-header="true" data-show-border="false"></div>
                                    </div>
                                </div>
                                <div class="block block-poll last_block">
                                    <div class="block-title">
                                        <strong><span>Quy định tham gia</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <p style="line-height:18px;">- Sử dụng <font style="font-weight:bold;color:#ff3708">tiếng Việt có dấu</font> khi tham gia.</p>
                                                <p style="line-height:18px;">- <font style="font-weight:bold;color:#ff3708">Chủ đề</font> cần ngắn gọn, đầy đủ.</p>
                                                <p style="line-height:18px;">- <font style="font-weight:bold;color:#ff3708">Nội dung câu hỏi</font> phải phù hợp với chủ đề.</p>
                                                <p style="line-height:18px;">- Khi vấn đề đã được giải quyết, vui lòng check vào nút <font style="font-weight:bold;color:#ff3708">đã giải quyết</font>.</p>
                                                <p style="line-height:18px;">- Mọi câu hỏi không phù hợp với quy định sẽ được <font style="font-weight:bold;color:#ff3708">xóa mà không cần báo trước</font>.</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
