﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="chat-luong-hang-hoa.aspx.cs" Inherits="PhongCachMobile.chat_luong_hang_hoa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litProductQuality" runat="server"></asp:Literal></span></strong>
                                </div>
                                <asp:Literal ID="litProdcutQualityContent" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
