﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="loai-phu-kien-dtdd.aspx.cs" Inherits="PhongCachMobile.loai_phu_kien_dtdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="js/jquery.nivo.slider.js" type="text/javascript"></script>
    <link href="css/category.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="page-title category-title">
                                <h2>
                                    <asp:Literal ID="litMobile" runat="server"></asp:Literal></h2>
                            </div>
                            <div id="minic_slider" class="theme-default">
                                <asp:Literal ID="litSlider" runat="server"></asp:Literal>
                            </div>
                            <script type="text/javascript">
                                $(window).load(function () {
                                    $('#slider').nivoSlider({
                                        effect: 'random',
                                        slices: 15,
                                        boxCols: 8,
                                        boxRows: 4,
                                        animSpeed: 500,
                                        pauseTime: 7000,
                                        startSlide: 0,
                                        directionNav: true,
                                        controlNav: false,
                                        controlNavThumbs: false,
                                        pauseOnHover: true,
                                        manualAdvance: false,
                                        prevText: 'Prev',
                                        nextText: 'Next',
                                        randomStart: false
                                    });
                                });
                            </script>
                            <div id="customcontent_top" class=" clearfix">
                                <ul>
                                    <asp:Literal ID="litBanner" runat="server"></asp:Literal>
                                </ul>
                            </div>
                            <div class="col-main">
                                <div class="category-products">
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litTopItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị</label>
                                                <asp:DropDownList ID="ddlTopDisplay" runat="server" OnSelectedIndexChanged="ddlTopDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="12" Value="12" />
                                                    <asp:ListItem Text="20" Value="20" />
                                                    <asp:ListItem Text="28" Value="28" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemTop" PageSize="12" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lstItem" runat="server">
                                        <LayoutTemplate>
                                            <ul class="products-grid" style="display: inline-block;">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="item <%# (((ListViewDataItem)Container).DataItemIndex) % 4 == 3 ? "last" : "" %> ">
                                               <%# Eval("isOutOfStock").ToString() == "True" ? "<div class='badge-custom badge-tam-het'></div>" : "" %>
                                            <%# Eval("isComingSoon").ToString() == "True" ? "<div class='badge-custom badge-sap-co-hang'></div>" : "" %>
                                            <div class="desc">
                                                <h2 class="product-name product-name-height">
                                                    <a href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                        <%#Eval("Name")%></a></h2>
                                                <div class=" std">
                                                    <%#Eval("ShortDesc")%>
                                                </div>
                                                </div>
                                                <div class="grid-inner">
                                                    <a href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" class="product-image">
                                                        <img class="pr-img" src="images/product/<%#Eval("FirstImage")%>"  style="width:auto; max-height:150px;"
                                                            alt="<%#Eval("Name")%>"></a>
                                                </div>
                                                <div class="product-box-2">
                                                    <div class="product-atr-height">
                                                        <div class="price-box">
                                                            <span class="regular-price" id="product-price-26"><span class="price" <%# Eval("isOutOfStock").ToString() == "True" ? "style='color: #515151;'" : "" %>>
                                                                <%# Eval("sPrice") %>
                                                                đ</span> </span>
                                                        </div>
                                                    </div>
                                                    <div class="actions">
                                                        <a class="btnAddCart" href="sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" <%# Eval("isOutOfStock").ToString() == "True" ? "style='background:#515151'" : "" %>>
                                                            <span>chi tiết </span></a> <%# (Eval("DiscountPrice").ToString() != "") ? "<img src='images/hotprice.png' style='margin-top:-10px;' height='40px' alt='' />" : ((Eval("IsGifted").ToString() == "" || Eval("IsGifted").ToString() == "False") ? "" : "<img src='images/giftpack.png' style='margin-top:-10px;' height='40px' alt='' />")%> 
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litBotItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị</label>
                                                <asp:DropDownList ID="ddlBotDisplay" runat="server" OnSelectedIndexChanged="ddlBotDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="12" Value="12" />
                                                    <asp:ListItem Text="20" Value="20" />
                                                    <asp:ListItem Text="28" Value="28" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemBot" PageSize="12" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <%--<div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Khi mua phụ kiện</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulFeature">
                                            <li>Nhận hàng trong 30 phút tại TP.Hồ Chí Minh</li>
                                            <li>Đổi trả miễn phí trong 30 ngày</li>
                                            <li>Bảo hành 1 đổi 1 trong vòng 1 năm</li>
                                            <li>Thu cũ - Đổi mới - Giá rẻ nhất</li>
                                        </ul>
                                    </div>
                                </div>--%>
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litPhone" runat="server"></asp:Literal>
                                  <%--  <a href="dtdd">
                                        <asp:Image ID="imgPhone" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                 <asp:Literal ID="litTablet" runat="server"></asp:Literal>
                                   <%-- <a href="may-tinh-bang">
                                        <asp:Image ID="imgTablet" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litApplication" runat="server"></asp:Literal>
                                    <%--<a href="ung-dung">
                                        <asp:Image ID="imgApplication" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litGame" runat="server"></asp:Literal>
                                   <%-- <a href="game">
                                        <asp:Image ID="imgGame" runat="server" />
                                    </a>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
