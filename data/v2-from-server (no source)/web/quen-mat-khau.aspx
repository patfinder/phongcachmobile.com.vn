﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true" CodeBehind="quen-mat-khau.aspx.cs" Inherits="PhongCachMobile.quen_mat_khau" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
<div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litRegister" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content account-login" style="background: white; padding: 25px 30px 16px 30px;">
                                    <div class="col2-set">
                                        <div class="col-2 register" style="margin-right: 25px;">
                                            <div class="content">
                                                <h2>Thông tin tài khoản</h2>
                                                    <div id="frmInput" runat="server">
                                                <ul class="form-list">
                                                    <li>
                                                        <p>Để thiết lập lại mật khẩu cho tài khoản. Bạn hãy cung cấp thông tin tài khoản - địa chỉ email vào ô bên dưới.</p>
                                                        <label for="email" class="required">
                                                            Tài khoản</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <asp:Label ID="lblIsAvailabelAccount" runat="server"></asp:Label>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtEmail"  CssClass="input-text required-entry validate-email" runat="server"></asp:TextBox>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <div class="buttons-set">
                                                 <asp:Label ID="lblEmailResponse" runat="server"></asp:Label>
                                                    <asp:LinkButton ID="lbtSend" Text="Gửi" class="button_large" runat="server" onclick="lbtSend_Click"></asp:LinkButton>
                                                </div>
                                                </div>
                                                 <div id="forgetPasswordResponse" class="p8" visible="false" runat="server">
                                                 <p style="line-height:18px;">
                                                    <asp:Literal ID="litForgetPasswordResponse" runat="server"></asp:Literal>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1 features">
                                            <div class="content">
                                                <h2>
                                                    Điểm nổi bật của chúng tôi</h2>
                                                <ul class="feature">
                                                    <li>Điện thoại nhập từ châu Âu</li>
                                                    <li>Sản phẩm chất lượng với giá cả cạnh tranh</li>
                                                    <li>Chế độ bảo hành chu đáo</li>
                                                    <li>Chương trình hậu mãi đa dạng</li>
                                                    <li>Đỗi ngũ kĩ thuật viên hỗ trợ nhiệt tình</li>
                                                    <li>Nhân viên tư vấn chuyên nghiệp</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
