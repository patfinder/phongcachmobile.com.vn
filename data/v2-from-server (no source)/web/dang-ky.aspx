﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="dang-ky.aspx.cs" Inherits="PhongCachMobile.dang_ky" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="block">
                                <div class="block-title">
                                    <strong><span>
                                        <asp:Literal ID="litRegister" runat="server"></asp:Literal></span></strong>
                                </div>
                                <div class="block-content account-login" style="background: white; padding: 25px 30px 16px 30px;">
                                    <div class="col2-set">
                                        <div class="col-2 register" style="margin-right: 25px;">
                                            <div class="content">
                                                <h2>
                                                    Thông tin đăng ký tài khoản</h2>
                                                    <div id="frmInput" runat="server">
                                                <ul class="form-list">
                                                    <li>
                                                        <p>
                                                            Thông tin về tỉnh / thành phố nơi bạn đang sống và làm việc sẽ giúp chúng tôi phục
                                                            vụ tốt hơn.</p>
                                                        <label style="width:100%">
                                                            Tỉnh / Thành phố &nbsp; </label>
                                                        <br /><br />
                                                        <asp:DropDownList ID="ddlState" runat="server">
                                                        </asp:DropDownList>
                                                        <br />
                                                    </li>
                                                    <li>
                                                        <p>
                                                            Để đảm bảo cho việc kích hoạt tài khoản, và tham gia các hoạt động của <font style='color: red;
                                                                font-weight: bold;'>PhongCachMobile</font>. Chúng tôi đề nghị bạn sử dụng email
                                                            thật để đăng ký</p>
                                                        <br />
                                                        <label for="email" class="required" style="width:100%">
                                                            Tài khoản</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <asp:Label ID="lblIsAvailabelAccount" runat="server"></asp:Label>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtEmail" OnTextChanged="txtEmail_TextChanged" AutoPostBack="True"
                                                                CssClass="input-text required-entry validate-email" runat="server"></asp:TextBox>
                                                            &nbsp;
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <label for="pass" class="required">
                                                            Mật khẩu</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValPassword" Display="Dynamic" runat="server"
                                                            ControlToValidate="txtPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ForeColor="Red" ID="rgePasswordLength" Display="Dynamic"
                                                            runat="server" ControlToValidate="txtPassword" ValidationExpression="[\s\S]{6,30}"></asp:RegularExpressionValidator>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtPassword" TextMode="Password" class="input-text required-entry validate-password"
                                                                runat="server"></asp:TextBox>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <label for="pass" class="required">
                                                            Nhập lại mật khẩu</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValRePassword" Display="Dynamic" runat="server"
                                                            ControlToValidate="txtPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ForeColor="Red" ID="rgeRePasswordLength" Display="Dynamic"
                                                            runat="server" ControlToValidate="txtPassword" ValidationExpression="[\s\S]{6,30}"></asp:RegularExpressionValidator>
                                                        <asp:CompareValidator ForeColor="Red" ID="cmpValRePassword" runat="server" ControlToCompare="txtPassword"
                                                            ControlToValidate="txtRePassword" Operator="Equal" Display="Dynamic"></asp:CompareValidator>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtRePassword" TextMode="Password" class="input-text required-entry validate-password"
                                                                runat="server"></asp:TextBox>
                                                        </div>
                                                    </li>
                                                </ul>
                                               
                                                                                               <div class="buttons-set">
                                             
                                                    <asp:LinkButton ID="lbtRegister" Text="Đăng ký" class="button_large" runat="server" onclick="lbtRegister_Click"></asp:LinkButton>
                                                </div>
                                                </div>
                                                 <div id="registerResponse" class="p8" visible="false" runat="server">
                                                 <p style="line-height:18px;">
                                                    <asp:Literal ID="litRegisterResponse" runat="server"></asp:Literal>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1 features">
                                            <div class="content">
                                                <h2>
                                                    Điểm nổi bật của chúng tôi</h2>
                                           <ul class="feature">
                                                    <li>"Since 2009", là điểm kinh doanh uy tín bậc nhất các sản phẩm ĐTDĐ / Máy tính bảng / Phụ kiện cao cấp</li>
                                                    <li>Sản phẩm đa dạng, chất lượng chuẩn từ nhà sản xuất chính hãng</li>
                                                    <li>Giá bán được cập nhật liên tục theo giá thế giới và luôn là giá tốt nhất.</li>
                                                    <li>Dịch vụ hậu mãi, bảo hành chu đáo và trung thực.</li>
                                                    <li>Đội ngũ nhân viên thân thiện, nhiệt tình.</li>
                                                </ul>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
