﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="gia-hot.aspx.cs" Inherits="PhongCachMobile.gia_hot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <%-- <script type="text/javascript">
        $(document).ready(function () {
            jQuery(".product-image").easyTooltip({
                useElement: "hotdesc2"
            });
        });
    </script>--%>
    <%-- <script type="text/javascript" src="js/liteaccordion.jquery.js"></script>
    <script src="js/tms-0.3.js" type="text/javascript"></script>
    <script src="js/tms_presets.js" type="text/javascript"></script>--%>
    <%--  <script src="js/jquery.cycle.all.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>--%>
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <link rel='stylesheet' id='options_typography_Anton-css' href='http://fonts.googleapis.com/css?family=Anton'
        type='text/css' media='all' />
    <script type="text/javascript">
        jQuery(window).load(function () {
            // nivoslider init
            jQuery('#slider').nivoSlider({
                effect: 'boxRain',
                slices: 15,
                boxCols: 8,
                boxRows: 8,
                animSpeed: 500,
                pauseTime: 5000,
                directionNav: false,
                directionNavHide: false,
                controlNav: false,
                captionOpacity: 1,
                beforeChange: function () {
                    jQuery('.nivo-caption').stop().animate({ left: "-400px" }, { easing: "easeInBack" });
                },
                afterChange: function () {
                    jQuery('.nivo-caption').stop().animate({ left: "130px" }, { easing: "easeOutBack" });
                }
            });
        });
    </script>
    <style type="text/css">
        #slider h2
        {
            font-family: Anton, sans-serif;
        }
        
        #slider h2 .small
        {
            font-size: 75%;
        }
        
        /* Slider
---------------------------------------- */
        #slider-wrapper
        {
            height: 451px;
            overflow: hidden;
            position: relative;
            padding-bottom: 15px;
        }
        #slider
        {
            position: absolute;
            left: 50%;
            margin-left: -600px;
            width: 1200px;
            height: 452px;
            background: url(images/ajax-loader.gif) no-repeat 50% 50%;
            overflow: hidden;
        }
        #slider img
        {
            position: absolute;
            top: 0px;
            left: 0px;
            display: none;
        }
        #slider a
        {
            border: 0;
            display: block;
        }
        
        
        /* The Nivo Slider styles */
        .nivoSlider
        {
            position: relative;
        }
        .nivoSlider img
        {
            position: absolute;
            top: 0px;
            left: 0px;
        }
        /* If an image is wrapped in a link */
        .nivoSlider a.nivo-imageLink
        {
            position: absolute;
            top: 0px;
            left: 50%;
            width: 940px;
            height: 100%;
            border: 0;
            padding: 0;
            margin: 0 0 0 -470px;
            z-index: 98;
            display: none;
        }
        /* The slices in the Slider */
        .nivo-slice
        {
            display: block;
            position: absolute;
            z-index: 50;
            height: 100%;
        }
        .nivo-box
        {
            display: block;
            position: absolute;
            z-index: 5;
        }
        /* Caption styles */
        .nivo-caption
        {
            width: 400px;
            position: absolute;
            left: 130px;
            top: 169px;
            margin: 0;
            background: none;
            color: #fff;
            opacity: 1; /* Overridden by captionOpacity setting */
            width: 100%;
            z-index: 89;
        }
        .nivo-caption .nivo-caption-inner
        {
            padding: 0;
            margin: 0;
        }
        .nivo-caption a
        {
            display: inline !important;
        }
        .nivo-caption h2
        {
            font-weight: normal;
            font-size: 48px;
            line-height: 48px;
            color: white;
            text-shadow: 1px 1px rgba(0,0,0,.1);
        }
        .nivo-caption h2 small
        {
            display: block;
            font-size: 30px;
            line-height: 30px;
        }
        .nivo-html-caption
        {
            display: none;
        }
        /* Direction nav styles (e.g. Next & Prev) */
        .nivo-directionNav a
        {
            position: absolute;
            top: 45%;
            z-index: 99;
            cursor: pointer;
        }
        .nivo-prevNav
        {
            left: 0px;
        }
        .nivo-nextNav
        {
            right: 0px;
        }
        /* Control nav styles (e.g. 1,2,3...) */
        .nivo-controlNav
        {
            position: absolute;
            width: 940px;
            height: 13px;
            left: 50%;
            bottom: 165px;
            margin-left: -470px;
        }
        .nivo-controlNav a
        {
            position: relative;
            z-index: 99;
            cursor: pointer;
            display: inline-block !important;
            vertical-align: top;
            width: 13px;
            height: 0;
            padding: 13px 0 0 0;
            overflow: hidden;
            background-repeat: no-repeat;
            background-position: -21px 0;
            background-image: url(images/control_nav.png);
            margin: 0 8px 0 0;
        }
        .nivo-controlNav a.active, .nivo-controlNav a:hover
        {
            background-position: 0 0;
        }
        
        .nivo-directionNav
        {
        }
        .nivo-directionNav a
        {
            position: absolute;
            display: block;
            width: 36px;
            height: 35px;
            text-indent: -9999px;
            border: 0;
            top: 70%;
            background: url(images/direction_nav.png) no-repeat 0 0;
        }
        a.nivo-nextNav
        {
            left: 170px;
            background-position: -36px 0;
        }
        a.nivo-nextNav:hover
        {
            background-position: -36px -35px;
        }
        a.nivo-prevNav
        {
            left: 130px;
            background-position: 0 0;
        }
        a.nivo-prevNav:hover
        {
            background-position: 0 -35px;
        }
        
        #slider .nivo-controlNav img
        {
            display: inline; /* Unhide the thumbnails */
            position: relative;
            margin-right: 10px;
            width: 120px;
            height: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <asp:Literal ID="litTooltip" runat="server"></asp:Literal>

    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <!-- Center -->
                <div id="center_column" class="center_column">
                    <section id="slider-wrapper">
    <div class="container_12 clearfix">
      <div class="grid_12">
      <div style="height:459px;">
        <div style="width:454px;height:455px;float:left;padding-right:10px;">
          <div style="height:145px;"><asp:HyperLink ID="hpl1stTopBanner" runat="server"></asp:HyperLink>
          <div class="banner_title" style="top:120px;" id="div1stTopBanner" runat="server">
					<asp:Literal ID="lit1stTopBanner" runat="server"></asp:Literal>
		</div>
        
        </div>
        <div style="height:145px;padding-top:10px;"><asp:HyperLink ID="hpl2ndTopBanner" runat="server"></asp:HyperLink>
         <div class="banner_title" style="top:275px;" id="div2ndTopBanner" runat="server">
					<asp:Literal ID="lit2ndTopBanner" runat="server"></asp:Literal>
		</div></div>
        <div style="height:145px;padding-top:10px;"><asp:HyperLink ID="hpl3rdTopBanner" runat="server"></asp:HyperLink>
        <div class="banner_title" style="top:429px;" id="div3rdTopBanner" runat="server">
					<asp:Literal ID="lit3rdTopBanner" runat="server"></asp:Literal>
		</div></div>
        </div>
        <div style="width:600px;height:455px;float:right;"><asp:HyperLink ID="hpl4thTopBanner" runat="server"></asp:HyperLink></div>
      
      </div>
        <%--<div id="slider" class="nivoSlider"> 
         <asp:Literal ID="litTopSlider" runat="server"></asp:Literal>
                                 
                  style="border: 1px solid #dcdcdc;"             
        </div>
         <asp:Literal ID="litTopSliderCaption" runat="server"></asp:Literal>--%>
        
      </div>
    </div>
  </section>
                    <%--<div id="tmslider1">
                        <div id="one" style="width: 1062px; height: 427px;" class="accordion basic">
                            <ol>
                                <asp:ListView ID="lstTopSlider" runat="server">
                                    <LayoutTemplate>
                                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <li class="slide">
                                            <h2>
                                                <span class="slide_name">
                                                    <%#Eval("Title")%></span><span class="slide_number">0<%# (((ListViewDataItem)Container).DataItemIndex) + 1 %></span></h2>
                                            <div>
                                                <div>
                                                    <a href="<%#Eval("ActionUrl")%>">
                                                        <img src="images/slide/<%#Eval("DisplayImage")%>" alt=""></a>
                                                    <div>
                                                        <h2>
                                                            <a href="<%#Eval("ActionUrl")%>">
                                                                <%#Eval("Title")%></a></h2>
                                                        <%#Eval("Text")%>
                                                        <a class="tmslider1_btn" href="<%#Eval("ActionUrl")%>">Xem ngay</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:ListView>
                            </ol>
                        </div>
                    </div>--%>
                    <%--  <script type="text/javascript">
                        $('#one').liteAccordion({
                            onTriggerSlide: function () {
                                this.find('figcaption').fadeOut();
                            },
                            onSlideAnimComplete: function () {
                                this.find('figcaption').fadeIn();
                            },
                            autoPlay: true,
                            pauseOnHover: true,
                            theme: 'basic',
                            rounded: false,
                            enumerateSlides: false
                        }).find('figcaption:first').show();
                    </script>--%>
                </div>
                <div class="clearblock">
                </div>
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2 class="subtitle" style="text-transform: none;">
                                        <asp:Literal ID="litSubtitle" runat="server"></asp:Literal></h2>
                                </div>
                                <div id="tabPrd">
                                    <%--<div class="hotprd" style="padding-right:0px;">
                                        <div class="topbg">
                                            <div class="title">
                                        <p style="text-align:left;"><span style="font-family:'Open Sans';font-size:16px;color:#ffffff;"><b>SCHEDULE</b></span></p>
                                        </div>
                                        <div class="subtitle">
                                        <p style="text-align:left;"><span style="font-family:'Open Sans';font-size:14px;color:#7ecefd;">SERVICES</span></p>
                                        </div>
                                        
                                        </div>
                                        <div class="imgprd">
                                        <img src="images/product/prd_011.jpg" class="" style="width: 100%;">
                                        </div>
                                        <div class="botbg">
                                        <div class="text">
                                       <p style="text-align:left;"><span style="font-family:'Arial';font-size:14px;color:#8c9ba7;">Tincidunt dolor nunc vule putate ulrips cons. Donec semp ertet lacinia te ultric eupien disse eltel.</span></p>
                                        </div>
                                        </div>
                                    </div>--%>
                                    <asp:Literal ID="litHotPrd" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstDiscountNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll" id="divPrdNews" runat="server">
                                    <div class="block-title">
                                        <strong><span>Tin thị trường</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstPrdNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll" id="divPoll" runat="server">
                                    <div class="block-title">
                                        <strong><span>
                                            <asp:Literal ID="litYourVote" runat="server"></asp:Literal></span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <p class="block-subtitle">
                                            <asp:Literal ID="litPollName" runat="server"></asp:Literal></p>
                                        <asp:RadioButtonList ID="rblPoll" runat="server">
                                        </asp:RadioButtonList>
                                        <asp:ListView ID="lstPollAnswer" runat="server">
                                            <LayoutTemplate>
                                                <ul style="display: inline-block;">
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </ul>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <li class="answer">
                                                    <%# Eval("Name") %>
                                                    <span>-
                                                        <%# Eval("Value") %>
                                                        (<%# Eval("Percent") %>%)</span> </li>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <div class="actions">
                                            <asp:Button ID="btnVote" CssClass="button" runat="server" Text="Bình chọn" OnClick="btnVote_Click" />
                                        </div>
                                    </div>
                                </div>

                                <div class="block block-poll last_block" id="divBannerBot" runat="server" style="border:none;">
                                    <asp:HyperLink ID="hplBannerBot" runat="server"></asp:HyperLink>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
