﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucHeader.ascx.cs" Inherits="PhongCachMobile.master.uc.ucHeader" %>

<div id="header">
    <a id="header_logo" href="gia-hot" title="Phong Cach Mobile">
        <img class="logo" src="images/logo.png" alt="Phong Cach Mobile" />
    </a>
    <div id="header_right">
        <div class="clearblock">
        </div>
        <div id="header_user" style="width:650px;">
    
            <ul>
                <li id="header_user_info" style="float:right;"> 
                    <asp:Literal ID="litAccount" runat="server"></asp:Literal>
                    </li>
             <%--   <li id="your_account"><a href="tai-khoan"
                    title="Tài khoản">
                    <asp:Literal ID="litAccount" runat="server"></asp:Literal></a></li>--%>
               
            </ul>
        </div>
        <div class="clearblock">
        </div>
        <asp:Literal ID="litMenuTop" runat="server"></asp:Literal>
        <script type="text/javascript" src="js/superfish.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('ul.sf-menu').superfish({
                    delay: 1000,
                    animation: { opacity: 'show', height: 'show' },
                    speed: 'fast',
                    autoArrows: false,
                    dropShadows: false
                });
            });
        </script>
        <div class="clearblock">
        </div>
        <div id="tmcategories">
            <asp:Literal ID="litMenuCategory" runat="server"></asp:Literal>
              <div style="float:right;margin-top: 8px;margin-right: 30px;vertical-align:middle;">
            <asp:TextBox ID="txtSearch" style="width: 180px;padding: 4px 8px 4px 8px;border-radius:3px;" runat="server"></asp:TextBox> &nbsp; 
               <span style="margin-top: 5px;font-weight: bold;display: inline-block;">
               <a id="lbtSearch" style="color:White;cursor:pointer;" runat="server">Tìm</a></span> 
        </div>
        </div>
        <!-- /TM Categories -->
    </div>
</div>
