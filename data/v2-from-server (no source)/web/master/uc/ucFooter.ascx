﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucFooter.ascx.cs" Inherits="PhongCachMobile.master.uc.ucFooter" %>
	<div id="footer">
		<div id="tmfooterlinks">
          <p style="padding:30px 0px 0px 30px;color:white; font-weight:bold;">HỆ THỐNG PHONGCACHMOBILE <span style="float:right">Thời gian mở cửa: 9h00 - 21h00, kể cả thứ 7 và CN &nbsp; &nbsp; &nbsp; </span></p>

          <div style="width:550px;">
		<ul>
			<%--<li style='font-weight:bold;color:Red;'>Văn phòng & chi nhánh</li>--%>
			<li><font color='red'>1.</font> 58 Trần Quang Khải, P.Tân Định, Q.1, TP.HCM - ĐT: 08 35268254 - 08 35268255</li>
			
            <li><font color='red'>2.</font> 35A Xô Viết Nghệ Tĩnh, P.An Cư, Q.Ninh Kiều, TP.Cần Thơ - ĐT: 071 03819477 - 071 03819478</li>
		</ul>
	</div>

	<div style="width:140px;">
		<ul>
            <li><a href="gioi-thieu">Giới thiệu</a></li>
			<li><a href="sitemap">Sitemap</a></li>
            <li><a href="chat-luong-hang-hoa">Chất lượng hàng hóa</a></li>
            <li><a href="hop-tac-kinh-doanh">Hợp tác kinh doanh</a></li>
			<li><a href="mua-hang-tu-xa">Mua hàng từ xa</a></li>
		</ul>
	</div>
	<div style="width:130px;">
		<ul>
			<li><a href="gia-hot">Sản phẩm giá hot</a></li>
			<li><a href="tin-khuyen-mai">Tin khuyến mãi</a></li>
			<li><a href="tin-thi-truong">Tin thị trường</a></li>
			<li><a href="quy-dinh-bao-hanh">Quy định bảo hành</a></li>
			<li><a href="sua-chua-cai-dat">Sửa chữa & cài đặt</a></li>
		</ul>
	</div>
    <div style="width:200px;">
		<ul>
			<li>Lượt truy cập: <font style="color:red;font-weight:bold;">
                <asp:Literal ID="litTotalVisit" runat="server"></asp:Literal></font></li>
            <li>Online: <font style="color:red;font-weight:bold;">
                <asp:Literal ID="litOnline" runat="server"></asp:Literal></font></li>
			<li>Hỗ trợ kỹ thuật</li>
			<li>[TPHCM]: 08 35268255. Ext 103 - 104</li>
			<li>[TP.Cần Thơ]: 071 03819476</li>
		</ul>
	</div>
</div>
		<!-- [[%FOOTER_LINK]] -->	</div>

