﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="mucRightPanel.ascx.cs"
    Inherits="PhongCachMobile.master.uc.mucRightPanel" %>
<div data-role="panel" id="right-panel" data-position="right" data-theme="g">
    <!-- Phone Number -->
    <div class="r-phone">
        <a href="tel:610383766284"><i class="fa fa-phone-square fa-2x"></i>&nbsp; &nbsp; 08
            35268254 - 08 35268255</a></div>
    <!-- Latest Tweets Starts -->
    <div class="r-tweets">
        <i class="fa fa-gift fa-2x"></i><span class="heading">TIN KHUYẾN MÃI</span>
        <div id="tweet_container">
            <div class="holder">
                <div class="tweets">
                    <asp:ListView ID="lstNews" runat="server">
                        <LayoutTemplate>
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <div class="tweet" style="display: block;">
                                <a href="m-tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                    <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                        width="60px" class="tweet_foto" />
                                </a>
                                <div class="tweet_text">
                                    <a class="profile" href="m-tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>"><%#Eval("Title")%></a>
                                    </div>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
    </div>
    <!-- Latest Tweets Ends -->
    <!-- Contact Info Starts -->
    <div class="r-contact-info">
        <i class="fa fa-envelope fa-2x"></i><span class="heading">LIÊN HỆ</span><br>
        <i class="fa fa-location-arrow"></i>&nbsp;&nbsp;<strong>58 TRẦN QUANG KHẢI</strong><br />
        &nbsp; &nbsp; &nbsp;P.Tân Định, Q.1, TP.HCM<br />
        <i class="fa fa-phone"></i>&nbsp; 08 35268254 - 08 35268255<br />
        <br />
        <i class="fa fa-location-arrow"></i>&nbsp;&nbsp;<strong>35A XÔ VIẾT NGHỆ TĨNH</strong><br />
        &nbsp; &nbsp; &nbsp;P.An Cư, Q.Ninh Kiều, TP.Cần Thơ<br />
        <i class="fa fa-phone"></i>&nbsp; 071 03819477 - 071 03819478<br />
        <br />
        <i class="fa fa-envelope"></i>&nbsp;&nbsp;<a style="font-size: 12px;" href="mailto:marketing@phongcachmobile.vn"
            target="_blank">marketing@phongcachmobile.vn</a><br /><br />
        <i class="fa fa-cog fa-2x"></i><span class="heading">HỖ TRỢ KĨ THUẬT</span><br/>
         <i class="fa fa-location-arrow"></i>&nbsp;&nbsp;<strong>TP HCM</strong><br />
           <i class="fa fa-phone"></i>&nbsp; 08 35268255. Ext 103 - 104<br /><br />
        <i class="fa fa-location-arrow"></i>&nbsp;&nbsp;<strong>TP.Cần Thơ</strong><br />
       <i class="fa fa-phone"></i>&nbsp; 071 03819476<br />
    </div>
    <!-- Contact Info Ends -->
    <!-- Social Icons Starts -->
    <div class="l-social-icons">
        <ul>
            <li><a href='https://www.facebook.com/phongcachmobile.com.vn' target="_blank">
                <img src="m/images/icons/social-icons/fb.png" width="36" height="36" /></a></li>
            <li><a href='https://www.youtube.com/user/phongcachmobile' target="_blank">
                <img src="m/images/icons/social-icons/youtube.png" width="36" height="36" /></a></li>
        </ul>
    </div>
    <!-- Social Icons Ends -->
    <!-- Social Like Buttons-->
    <div class="ui-grid-b social-like">
        <!-- Facebook Like Button -->
        <div class="ui-block-a">
            <div id="fb-root">
            </div>
            <script>                (function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "http://connect.facebook.net/en_US/all.js#xfbml=1";
                    fjs.parentNode.insertBefore(js, fjs);
                } (document, 'script', 'facebook-jssdk'));
            </script>
            <div class="fb-like" data-href="https://www.facebook.com/phongcachmobile.com.vn"
                data-send="false" data-layout="standard" data-width="" data-show-faces="true">
            </div>
        </div>
    </div>
    <!-- /Social Like Buttons-->
</div>
