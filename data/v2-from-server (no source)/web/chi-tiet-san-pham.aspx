﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="chi-tiet-san-pham.aspx.cs" Inherits="PhongCachMobile.chi_tiet_san_pham" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" />
    <link href="css/jqzoom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.fancybox-1.3.4.js" type="text/javascript"></script>
    <script src="js/jquery.scrollTo-1.4.2-min.js" type="text/javascript"></script>
    <script src="js/jquery.serialScroll-1.2.2-min.js" type="text/javascript"></script>
    <script src="js/product.js" type="text/javascript"></script>
    <script src="js/jquery.jqzoom.js" type="text/javascript"></script>
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.idTabs.modified.js" type="text/javascript"></script>
    <script type="text/javascript">
        function replyComment(id, alias) {
            $('#content_hdfCommentId').val(id);
            $('#content_txtComment').focus().val('@' + alias + ': ');
            $('#_comment').focus();
        }

        $(document).ready(function () {
            $('#emoticons img').click(function () {
                var smiley = $(this).attr('alt');
                ins2pos(smiley, 'content_txtComment');
            });

            function ins2pos(str, id) {
                var TextArea = document.getElementById(id);
                var val = TextArea.value;
                var before = val.substring(0, TextArea.selectionStart);
                var after = val.substring(TextArea.selectionEnd, val.length);

                TextArea.value = before + str + after;
                setCursor(TextArea, before.length + str.length);

            }

            function setCursor(elem, pos) {
                if (elem.setSelectionRange) {
                    elem.focus();
                    elem.setSelectionRange(pos, pos);
                } else if (elem.createTextRange) {
                    var range = elem.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                }
            }
        });
    </script>
    <script type="text/javascript">        var switchTo5x = true;</script>
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">        stLight.options({ publisher: "da6af5b0-d769-45cc-8003-392451774e04", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="fb-root">
    </div>
    <script>        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));</script>
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2 class="subtitle">
                                        <asp:HyperLink ID="hplCategory" runat="server"></asp:HyperLink>
                                        <span class="navigation-pipe">&gt;</span>
                                        <asp:HyperLink ID="hplSubCategory" runat="server"></asp:HyperLink>
                                        <span class="navigation-pipe">&gt;</span> <span>
                                            <asp:Literal ID="litPrdName" runat="server"></asp:Literal></span></h2>
                                </div>
                                <div class="category-products" style="display: inline-block;">
                                    <div class="prd">
                                        <div id="center_column" class="center_column" style="width: 100%;">
                                            <script type="text/javascript">
                                                var jqZoomEnabled = true;
                                            </script>
                                            <div id="primary_block" class="clearfix">
                                                <div id="pb-right-column">
                                                    <div id="image-block" class="bordercolor" style="display: table-cell; max-width: 304px;
                                                        max-height: 270px;">
                                                        <asp:Image ID="imgDisplay" Style="max-width: 100%; max-height: 100%;" class="jqzoom"
                                                            runat="server" />
                                                    </div>
                                                    <div id="views_block">
                                                        <div id="thumbs_list">
                                                            <ul id="thumbs_list_frame">
                                                                <asp:Literal ID="litImgThumb" runat="server"></asp:Literal>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <ul id="usefull_link_block" class="bordercolor">
                                                        <li style="background: none;">Lượt xem: <font color="red">
                                                            <asp:Literal ID="litViewCount" runat="server"></asp:Literal></font></li>
                                                        <li style="background: none;">Bình luận: <font color="red">
                                                            <asp:Literal ID="litCommentCount" runat="server"></asp:Literal></font></li>
                                                        <li class="print"><a href="javascript:print();">In ra</a></li>
                                                    </ul>
                                                </div>
                                                <div id="pb-left-column">
                                                    <h1>
                                                        <asp:Literal ID="litAnotherPrdName" runat="server"></asp:Literal></h1>
                                                    <div id="buy_block" class="bordercolor">
                                                        <div class="price bordercolor">
                                                            <span class="our_price_display">
                                                                <asp:Literal ID="litPriceType" runat="server"></asp:Literal>:
                                                                <br />
                                                                <span id="our_price_display" class="price">
                                                                    <asp:Literal ID="litPrdPrice" runat="server"></asp:Literal></span> </span>
                                                            <asp:Image ID="imgHotPrice" style="height: 40px;padding-left: 20px;" ImageUrl="images/hotprice.png" runat="server" />
                                                             <asp:Image ID="imgGifted" style="height: 40px;padding-left: 20px;" ImageUrl="images/giftpack.png" runat="server" />
                                                        </div>
                                                        <div class="other_options bordercolor" id="discount" runat="server">
                                                            <div id="discountdesc">
                                                                <asp:Literal ID="litDiscountDesc" runat="server"></asp:Literal>
                                                            </div>

                                                              
                                                            <%--<div id="other_prices">Giá gốc: 
                                                                <p id="old_price" style="padding:5px 0 0 0 ;">
                                                                    <span id="old_price_display" style="font-size: 16px;">
                                                                        <asp:Literal ID="litOriginalPrice" runat="server"></asp:Literal></span> 
                                                                </p>
                                                            </div>
                                                            <div id="attributes">
                                                                <span class="discount">Đang giảm giá</span>
                                                                <div class="clearblock">
                                                                </div>
                                                            </div>--%>
                                                            <div class="clearblock">
                                                            </div>
                                                        </div>
                                                      <div class="share bordercolor">
                                                            <!-- AddThis Button BEGIN -->
                                                            <div class="addthis_toolbox addthis_default_style ">
                                                                <span class='st_facebook_hcount' displaytext='Facebook'></span><span class='st_twitter_hcount'
                                                                    displaytext='Tweet'></span><span class='st_googleplus_hcount' displaytext='Google +'>
                                                                    </span><span class='st_email_hcount' displaytext='Email'></span>
                                                            </div>
                                                        </div>
                                                        <div class="clearblock">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                              <div id="short_description_block" class="bordercolor">
                                                            <div id="short_description_content" class="rte align_justify">
                                                                <asp:Literal ID="litRemark" runat="server"></asp:Literal></div>
                                                            <%--      <p class="buttons_bottom_block">
                                                                <a href="javascript:{}" class="button">chi tiết </a></p>--%>
                                                        </div>
                                                      <br />
                                                      
                                            <div id="more_info_block" class="clear" runat="server">
                                                <ul id="more_info_tabs" class="idTabs idTabsShort">
                                                    <li><a id="more_info_tab_more_info" href="#idTab1" class="">Giới thiệu chi tiết &nbsp;</a></li>
                                                    <li><a id="more_info_tab_outstanding" href="#idTab2" class="selected">Điểm nổi bật &nbsp;</a></li>
                                                    <li><a id="more_info_tab_data_sheet" href="#idTab3">Thông số kĩ thuật &nbsp;</a></li>
                                                    <li><a id="more_info_tab_asc" href="#idTab4">Phụ kiện &nbsp;</a></li>
                                                    <li><a id="more_info_tab_video" href="#idTab5">Quảng cáo &nbsp;</a></li>
                                                    <li><a id="more_info_tab_point" style="color: red" href="#idTab6">Điểm tích lũy</a></li>
                                                </ul>
                                                <div id="more_info_sheets" class="bgcolor bordercolor">
                                                    <div id="idTab1" class="block_hidden_only_for_screen">
                                                        <div>
                                                            <asp:Literal ID="litLongDesc" runat="server"></asp:Literal>
                                                        </div>
                                                    </div>
                                                    <div id="idTab2">
                                                        <asp:Literal ID="litOutstanding" runat="server"></asp:Literal>
                                                    </div>
                                                    <div id="idTab3">
                                                        <asp:Literal ID="litFeatures" runat="server"></asp:Literal>
                                                    </div>
                                                    <div id="idTab4">
                                                        <asp:Literal ID="litAccessories" runat="server"></asp:Literal>
                                                    </div>
                                                    <div id="idTab5">
                                                        <asp:Literal ID="litVideo" runat="server"></asp:Literal>
                                                    </div>
                                                    <div id="idTab6" style="height: 50px;">
                                                        <span style="padding-top: 4px; display: inline-flex; font-weight: bold;">Mã số thẻ:
                                                        </span>
                                                        <asp:TextBox ID="txtCardCode" Style="padding: 4px 8px;" runat="server"></asp:TextBox>
                                                        &nbsp; &nbsp; &nbsp;
                                                        <asp:Button ID="btnCheck" Text="Kiểm tra" Style="cursor: pointer; padding-top: 4px;"
                                                            Font-Bold="true" runat="server" OnClick="btnCheck_Click" CausesValidation="false" /><br />
                                                        <br />
                                                        <asp:Literal ID="litCardInfo" runat="server"></asp:Literal>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div style="height:410px;" id="samePrice" runat="server">
                                <div class="page-title category-title">
                                    <h2 class="subtitle">
                                        Sản phẩm có cùng mức giá</h2>
                                </div>
                                  <asp:Literal ID="litSamePricePrd" runat="server"></asp:Literal>
                                  </div>
                                  <div>
                                <div class="page-title category-title">
                                    <h2 class="subtitle">
                                        Những sản phẩm vừa xem</h2>
                                </div>
                                <asp:Literal ID="litOtherPrd" runat="server"></asp:Literal>
                                </div>
                                <div class="prd">
                                    <article class="grid_8 fleft" id="_comment">
                   		 <h2 class="p10"><asp:Literal ID="litComment" runat="server"></asp:Literal></h2> 
                         <div runat="server" id="form4" clientidmode="Static">
                            <figure class="avatar">
                                <asp:Image Width="50px" ID="imgAvatar" runat="server"></asp:Image>
                            </figure>
                            <div style="overflow:hidden">
                            <asp:TextBox ID="txtComment" Height="80" Width="100%" TextMode="MultiLine" runat="server" style="margin-bottom:7px;"></asp:TextBox>

                            <div id="emoticons">
                            <asp:Literal ID="litEmotion" runat="server"></asp:Literal>
                               
                            </div>
                             <asp:RequiredFieldValidator style="margin-top:8px;" ID="reqValComment" CssClass="fleft" Display="Dynamic" runat="server"
                                ControlToValidate="txtComment" ForeColor="Red"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator style="margin-top:8px;" ForeColor="Red" CssClass="fleft" ID="rgeCommentLength" Display="Dynamic" runat="server" 
                                ControlToValidate="txtComment" ValidationExpression="[\s\S]{20,1000}"></asp:RegularExpressionValidator>
                            <asp:LinkButton style="margin-top:13px" CssClass="button_large fright reg2" 
                                ID="lbtSend" runat="server" onclick="lbtSend_Click"></asp:LinkButton>
                            </div>
                            <br />
                            <div class="bar">
						<span class="total_comment"><asp:Literal ID="litTotalComment" runat="server"></asp:Literal></span>
                    </div>
                    <asp:HiddenField ID="hdfCommentId" runat="server"></asp:HiddenField>
                    <asp:ListView ID="lstComment" runat="server">
                            <LayoutTemplate>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                            </LayoutTemplate>
                            <ItemTemplate>
                            <div class="comment <%# (((ListViewDataItem)Container).DataItemIndex)  % 10 == 9 ? "nobg" : "" %>">
                                <figure class="page1-img2">
                                    <img width="50px" src="<%#Eval("Avatar")%>">
                                </figure>
                                <div class="mheight extra-wrap">
                                    <span>
                                        <a class="link6"><%#Eval("Alias")%></a> - <%#Eval("PostedDate")%> 
                                    </span>

                                    <span class='fright'>#<%# Eval("Index") %></span><a onclick="return replyComment(<%# Eval("Id") %>, '<%# Eval("Alias") %>');" href="#_comment" class="fright link8">Trả lời</a>
                                    <br /><span><%#Eval("Description")%></span>
                                  
                                    </div>
                                </div>
                                    <div class="btmline"></div>
                                    <%# Eval("ChildComment") %>
                            </ItemTemplate>
                        </asp:ListView>

                        <br />
                                   <div class="pagenavi">
                    <asp:DataPager ID="dpLstComment" runat="server" PagedControlID="lstComment" PageSize="10"
                        OnPreRender="dpLstComment_PreRender">
                        <Fields>
                            <asp:NumericPagerField NumericButtonCssClass="inactive" CurrentPageLabelCssClass="current" />
                        </Fields>
                    </asp:DataPager>
                </div>

                            <%--<div class="comment"><figure class="page1-img2"><img width="50px" src="images/avatar/7.png" title="Tiêu đề tin tức 5" alt="Tiêu đề tin tức 5"></figure><div class="extra-wrap"><span><a class="link6" href="thong-tin-tiêu-đề-tin-tức-5-7" title="Tiêu đề tin tức 5">Tiêu đề tin tức 5</a></span><br><span>Lượt xem: <font color="red">5.213</font></span><br><span>Giới thiệu ngắn về tin tức 5</span></div></div>--%>
                            </div>
                    </article>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                                <div class="block block-poll" id="divPrdTrick" runat="server">
                                    <div class="block-title">
                                        <strong><span>Tư vấn sử dụng</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstPrdTrick" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <div class="product-name">
                                                            <a target="_blank" href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%--lượt xem:
                                                            <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%>--%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll" id="divVideo" runat="server">
                                    <div class="block-title">
                                        <strong><span>Video</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstVideo" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li style="display: inline-block;">
                                                        <div class="product-name">
                                                        <a class="youtube playvbtn" href="http://www.youtube.com/embed/<%# Eval("Video")%>"><img src="images/playbtn.png"></a>


                                                            <a target="_blank" href="http://www.youtube.com/embed/<%# Eval("Video")%>">
                                                                <img src="http://i1.ytimg.com/vi/<%# Eval("Video")%>/default.jpg" class="newsImg"
                                                                    alt="" width="60px" />
                                                            </a><a target="_blank" href="http://www.youtube.com/embed/<%# Eval("Video")%>">
                                                                <%# Eval("Title")%></a>
                                                        </div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstDiscountNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a> lượt xem:
                                                            <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll" id="divPrdNews" runat="server">
                                    <div class="block-title">
                                        <strong><span>Tin thị trường</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstPrdNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-poll last_block" id="div1" runat="server">
                                    <div class="block-title">
                                        <strong><span>Chúng tôi trên Facebook</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                   <div class="fb-like-box" data-href="https://www.facebook.com/phongcachmobile.com.vn"  data-width="219" data-height="330" data-colorscheme="light" data-show-faces="true" data-header="true" data-show-border="false"></div>

                                        <%--<div class="fb-like-box" data-href="https://www.facebook.com/phongcachmobile.com.vn"
                                            data-width="219" data-height="The pixel height of the plugin" data-colorscheme="light"
                                            data-show-faces="true" data-header="false" data-stream="false" data-show-border="false">
                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
