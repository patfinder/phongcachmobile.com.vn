﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-loai-phu-kien-mtb.aspx.cs" Inherits="PhongCachMobile.m_loai_phu_kien_mtb" %>
<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <div class="index-pic-text">
            <div class="ui-responsive">
                 <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                             <asp:Literal ID="litMobile" runat="server"></asp:Literal></h2>
                    </div>
                 <asp:ListView ID="lstItem" runat="server">
                                        <LayoutTemplate>
                                            <ul class="products-grid" style="text-align:center;">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="item">
                                               <%# Eval("isOutOfStock").ToString() == "True" ? "<div class='badge-custom badge-tam-het'></div>" : "" %>
                                            <%# Eval("isComingSoon").ToString() == "True" ? "<div class='badge-custom badge-sap-co-hang'></div>" : "" %>
                                              <div class="desc">
                                                <h2 class="product-name product-name-height">
                                                    <a href="m-sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                        <%#Eval("Name")%></a></h2>
                                                <div class=" std">
                                                    <%#Eval("ShortDesc")%>
                                                </div>
                                                </div>
                                                <div class="grid-inner">
                                                    <a href="m-sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>" class="product-image">
                                                        <img class="pr-img" src="images/product/<%#Eval("FirstImage")%>" width="150" style="max-height:150px;"
                                                            alt="<%#Eval("Name")%>"></a>
                                                </div>
                                                <div class="product-box-2">
                                                    <div class="product-atr-height">
                                                        <div class="price-box">
                                                            <span class="regular-price"><span class="price">
                                                                <%# Eval("sPrice") %>
                                                                đ</span> </span>
                                                        </div>
                                                    </div>
                                                    <div class="actions">
                                                        <a class="btnAddCart" href="m-sp-<%#Eval("UrlName")%>-<%#Eval("Id")%>" title="<%#Eval("Name")%>">
                                                            <span>Xem chi tiết</span></a> <%# (Eval("DiscountPrice").ToString() != "") ? "<img src='images/hotprice.png' style='margin-top:-10px;' height='40px' alt='' />" : ((Eval("IsGifted").ToString() == "" || Eval("IsGifted").ToString() == "False") ? "" : "<img src='images/giftpack.png' style='margin-top:-10px;' height='40px' alt='' />")%> 
                                                    </div>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>

                                <div class="toolbar">
                                        <div class="pager">
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                   <asp:DataPager ID="dpgLstItemBot" PageSize="6" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>

                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
