﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-chi-tiet-tin.aspx.cs" Inherits="PhongCachMobile.m_chi_tiet_tin" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">        stLight.options({ publisher: "da6af5b0-d769-45cc-8003-392451774e04", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                <div style="line-height:18px;">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="col-main">
                                <div class="page-title category-title">
                                    <h2>
                                        <asp:Literal ID="litTitle" runat="server"></asp:Literal></h2>
                                </div>
                                <div class="category-products" style="display: inline-block;">
                                    <ul class="news-grid">
                                        <li class="item ">
                                            <div class="info">&nbsp;đăng <span class="nameuser"><a href="#">
                                                    <asp:Literal ID="litFullname" runat="server"></asp:Literal></a></span> vào <span
                                                        class="timepost">
                                                        <asp:Literal ID="litPostedDate" runat="server"></asp:Literal></span><br /><span class="viewcount">
                                                            <i class="icon-eye-open"></i><span class="count"><asp:Literal ID="litViewCount" runat="server"></asp:Literal></span> lượt xem</span><br />
                                            </div>
                                            <a href="#" class="news-image" style="border:none;">
                                                <asp:Image ID="imgDisplay" runat="server" />
                                            </a>
                                            <div class="desc">
                                                <asp:Literal ID="litLongDesc" runat="server"></asp:Literal>
                                            </div>
                                            <br />
                                            <span class='st_facebook_hcount' displaytext='Facebook'></span><span class='st_twitter_hcount'
                                                displaytext='Tweet'></span><span class='st_googleplus_hcount' displaytext='Google +'>
                                                </span><span class='st_email_hcount' displaytext='Email'></span>
                                            <br />
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>