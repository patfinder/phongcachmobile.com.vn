﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/MHome.Master" AutoEventWireup="true" CodeBehind="m-dang-nhap.aspx.cs" Inherits="PhongCachMobile.m_dang_nhap" %>

<%@ Register Src="master/uc/mucFooter.ascx" TagName="mFooter" TagPrefix="mucFooter" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div data-role="content">
        <!-- Text with Pictures Starts -->
        <div class="index-pic-text">
            <div class="ui-responsive">
                <div style="line-height:18px;">
                    <div class="page-title category-title">
                        <h2 class="subtitle">
                            <asp:Literal ID="litLogin" runat="server"></asp:Literal></h2>
                    </div>
                   <div class="registered-users">
                                            <div class="content">
                                                <h2>
                                                    Bạn đã là thành viên</h2>
                                                <p>
                                                    Nếu bạn đã có tài khoản, hãy đăng nhập.</p>
                                                <ul class="form-list">
                                                    <li>
                                                        <label for="email" class="required">
                                                            Tài khoản</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ID="rgeValEmail" runat="server" ControlToValidate="txtEmail"
                                                            Display="Dynamic" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtEmail" CssClass="input-text required-entry validate-email" runat="server"></asp:TextBox>
                                                            &nbsp;
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <label for="pass" class="required">
                                                            Mật khẩu</label>
                                                        &nbsp;
                                                        <asp:RequiredFieldValidator ID="reqValPassword" Display="Dynamic" runat="server"
                                                            ControlToValidate="txtPassword" ForeColor="Red"></asp:RequiredFieldValidator>
                                                        <asp:RegularExpressionValidator ForeColor="Red" ID="rgePasswordLength" Display="Dynamic"
                                                            runat="server" ControlToValidate="txtPassword" ValidationExpression="[\s\S]{6,30}"></asp:RegularExpressionValidator>
                                                     
                                                        <div class="input-box">
                                                            <asp:TextBox ID="txtPassword" TextMode="Password" class="input-text required-entry validate-password"
                                                                runat="server"></asp:TextBox>
                                                        </div>
                                                    </li>
                                                </ul>
                                                      <p> <asp:Label ID="lblLoginResponse" runat="server"></asp:Label></p>
                                                <div class="buttons-set">
                                                    <a href="quen-mat-khau" style="color:#7f7f7f" class="f-left">Quên mật khẩu?</a>
                                                    <asp:LinkButton ID="lbtLogin" Text="Đăng nhập" class="button_large" runat="server"
                                                        OnClick="lbtLogin_Click"></asp:LinkButton> &nbsp;  <a href="m-dang-ky" class="button_large"><span><span>Đăng ký miễn phí</span></span></a>
                                                </div>
                                            </div>
                                        </div>
                </div>
            </div>
            <!-- /grid-a -->
        </div>
        <!-- Photo Gallery Ends -->
        <mucFooter:mFooter ID="idMUCFooter" runat="server" />
        <!-- Footer Ends -->
    </div>
</asp:Content>
