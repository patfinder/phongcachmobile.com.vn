﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/Home.Master" AutoEventWireup="true"
    CodeBehind="loai-game.aspx.cs" Inherits="PhongCachMobile.loai_game" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/category.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

     
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="server">
    <div id="columns">
        <div id="columns2">
            <div id="columns3">
                <div class="page">
                    <div class="main-container col2-right-layout">
                        <div class="main">
                            <div class="page-title category-title">
                                <h2>
                                    <asp:Literal ID="litMobile" runat="server"></asp:Literal></h2>
                            </div>
                            <div id="customcontent_top2" class=" clearfix">
                                <ul class="row">
                                    <li class="item  itemmrg span4"><a href="game-giai-tri-22">
                                        <img width="340px" src="images/giaitri.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    giải trí</h2>
                                                <p>
                                                    Dành cho mọi lứa tuổi.</p>
                                            </div>
                                        </div>
                                    </a></li>
                                    <%--<li class="item itemmrg span4"><a href="game-tri-tue-23">
                                        <img width="340px" src="images/tritue_caudo.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    trí tuệ</h2>
                                                <p>
                                                    Dành cho những ai thích suy nghĩ.</p>
                                            </div>
                                        </div>
                                    </a></li>--%>
                                    <li class="item itemmrg span4"><a href="game-nhap-vai-hanh-dong-24">
                                        <img width="340px" src="images/nhapvai.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    nhập vai, hành động</h2>
                                                <p>
                                                    Game đi cảnh, tìm đường, làm nhiệm vụ.</p>
                                            </div>
                                        </div>
                                    </a></li>
                                    <%--<li class="lastmrgitem itemmrg span4"><a href="game-doi-khang-26">
                                        <img width="340px" src="images/doikhang.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    đối kháng</h2>
                                                <p>
                                                    Thể loại đấu võ, hai người chơi đánh nhau.</p>
                                            </div>
                                        </div>
                                    </a></li>--%>
                                    <li class="lastmrgitem  item  span4"><a href="game-the-thao-online-27">
                                        <img width="340px" src="images/the-thao.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    thể thao, online</h2>
                                                <p>
                                                    Game đua xe, đá banh...</p>
                                            </div>
                                        </div>
                                    </a></li>
                                    <%--<li class="item  lastmrg span4"><a href="game-dua-xe-28">
                                        <img width="340px" src="images/duaxe.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    đua xe</h2>
                                                <p>
                                                    Tập trung những game về thể thao và đua xe.</p>
                                            </div>
                                        </div>
                                    </a></li>--%>
                                    <%--<li class="item itemmrg span4"><a href="game-thieu-nhi-29">
                                        <img width="340px" src="images/thieunhi.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    thiếu nhi</h2>
                                                <p>
                                                    Dành cho các bạn nhỏ.</p>
                                            </div>
                                        </div>
                                    </a></li>--%>
                                    <%--<li class="itemmrg  item  span4"><a href="game-quan-ly-30">
                                        <img width="340px" src="images/quanly.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    chiến thuật</h2>
                                                <p>
                                                    Xây dựng, quản lý, mở rộng quan hệ.</p>
                                            </div>
                                        </div>
                                    </a></li>--%>
                                    <%--<li class="item  lastmrg span4"><a href="game-khac-31">
                                        <img width="340px" src="images/gamekhac.jpg" alt="">
                                        <div class="contentDiv">
                                            <div class="con_indent">
                                                <h2>
                                                    khác</h2>
                                                <p>
                                                    Những game không thuộc các thể loại trên.</p>
                                            </div>
                                        </div>
                                    </a></li>--%>
                                </ul>
                            </div>
                            <div class="col-main">
                                <div class="category-products">
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litTopItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                            <label>HĐH</label>
                                                <asp:DropDownList ID="ddlOS" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlOS_SelectedIndexChanged">
                                                    </asp:DropDownList> &nbsp; 
                                                <label>
                                                    Hiển thị</label>
                                                <asp:DropDownList ID="ddlTopDisplay" runat="server" OnSelectedIndexChanged="ddlTopDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="9" Value="9" />
                                                    <asp:ListItem Text="15" Value="15" />
                                                    <asp:ListItem Text="30" Value="30" />
                                                </asp:DropDownList>
                                                <label>
                                                    &nbsp; Sắp xếp</label>
                                                <asp:DropDownList ID="ddlTopSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTopSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Ngày cập nhật" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                               <%--     <asp:ListItem Text="Lượt tải" Value="downloadcount" />--%>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemTop" PageSize="9" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:ListView ID="lstItem" runat="server"   onitemdatabound="lstItem_ItemDataBound">
                                        <LayoutTemplate>
                                            <ul id="product_list" class="bordercolor list">
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                            </ul>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <li class="ajax_block_product bordercolor">
                                                <img src="images/game/<%#Eval("DisplayImage")%>" alt="<%#Eval("Name")%>">
                                                <div class="center_block">
                                                    <div class="product_flags">
                                                        <%#Eval("Free")%>
                                                        <span class="availability bordercolor">Cập nhật:
                                                            <%#Eval("PostedDate")%></span> <span class="availability bordercolor"><font style="color: red;">
                                                                <%#int.Parse(Eval("ViewCount").ToString()).ToString("#,##0")%></font>&nbsp; lượt
                                                                xem</span> <%--<span class="availability bordercolor"><font style="color: red;">
                                                                    <%#int.Parse(Eval("DownloadCount").ToString()).ToString("#,##0")%></font>&nbsp;
                                                                    lượt tải</span>--%>
                                                    </div>
                                                    <h3>
                                                        <%#Eval("Name")%></h3>
                                                    <p class="product_desc">
                                                        <%#Eval("Description")%></p>
                                                </div>
                                                <div class="right_block bordercolor">
                                                    <p class="compare checkbox">
                                                        <label for="comparator_item_list1">
                                                            Nhà sản xuất:</label>
                                                        <span class="property">
                                                            <%#Eval("Developer")%></span></p>
                                                    <p class="compare checkbox">
                                                        <label for="comparator_item_list1">
                                                            Yêu cầu:</label>
                                                        <span class="property">
                                                            <%#Eval("RequiredOS")%></span></p>
                                                    <p class="compare checkbox">
                                                        <label for="comparator_item_list1">
                                                            Phiên bản:</label>
                                                        <span class="property">
                                                            <%#Eval("CurrentVersion")%></span></p>
                                                    <p class="compare checkbox">
                                                        <label for="comparator_item_list1">
                                                            Dung lượng:</label>
                                                        <span class="property">
                                                            <%#Eval("Filesize")%></span></p>
                                              <p class="compare checkbox">
                                                                <asp:HiddenField ID="hdfName" Value='<%#Eval("Filename")%>' runat="server" />
                                                                  <asp:HiddenField ID="hdfId" Value='<%#Eval("Id")%>' runat="server" />
                                                                <asp:HyperLink ID="hplDownload" class="exclusive ajax_add_to_cart_button"  runat="server">Tải về &nbsp;</asp:HyperLink>
                                                                </p>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:ListView>
                                    <div class="toolbar">
                                        <div class="pager">
                                            <p class="amount">
                                                <asp:Literal ID="litBotItemsIndex" runat="server"></asp:Literal>
                                            </p>
                                            <div class="limiter">
                                                <label>
                                                    Hiển thị</label>
                                                <asp:DropDownList ID="ddlBotDisplay" runat="server" OnSelectedIndexChanged="ddlBotDisplay_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="9" Value="9" />
                                                    <asp:ListItem Text="15" Value="15" />
                                                    <asp:ListItem Text="30" Value="30" />
                                                </asp:DropDownList>
                                                <label>
                                                    &nbsp; &nbsp; &nbsp; Sắp xếp</label>
                                                <asp:DropDownList ID="ddlBotSort" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBotSort_SelectedIndexChanged">
                                                    <asp:ListItem Text="Ngày cập nhật" Value="posteddate" />
                                                    <asp:ListItem Text="Lượt xem" Value="viewcount" />
                                                    <%--<asp:ListItem Text="Lượt tải" Value="downloadcount" />--%>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="pages">
                                                <strong>Trang:</strong>
                                                <ol>
                                                    <asp:DataPager ID="dpgLstItemBot" PageSize="9" PagedControlID="lstItem" runat="server"
                                                        OnPreRender="dpgLstItem_PreRender">
                                                        <Fields>
                                                            <asp:NumericPagerField CurrentPageLabelCssClass="current" />
                                                        </Fields>
                                                    </asp:DataPager>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-right sidebar">
                             <div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Chính sách ưu dãi</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <div class="product-filter" style="font-size:12px;color:Black;">
                                                    <asp:Literal ID="litAdvantage" runat="server"></asp:Literal>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <%--<div class="block block-poll">
                                    <div class="block-title">
                                        <strong><span>Lọc sản phẩm</span></strong>
                                    </div>
                                    <div class="block-content" style="background: white;">
                                        <ul class="ulRecentNews">
                                            <li>
                                                <div class="product-filter">
                                                    <p>
                                                        HỆ ĐIỀU HÀNH</p>
                                                
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>--%>
                                <div class="block block-cart">
                                    <div class="block-title">
                                        <strong><span>Tin khuyến mãi</span></strong>
                                    </div>
                                    <div class="block-content">
                                        <ul class="ulRecentNews">
                                            <asp:ListView ID="lstNews" runat="server">
                                                <LayoutTemplate>
                                                    <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <li><a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                        <img src="images/news/<%# Eval("DisplayImage")%>" title="<%# Eval("Title")%>" alt="<%# Eval("UrlTitle")%>"
                                                            width="60px" class="newsImg" />
                                                    </a>
                                                        <div class="product-name">
                                                            <a href="tt-<%# Eval("UrlTitle")%>-<%# Eval("Id")%>">
                                                                <%# Eval("Title")%></a>
                                                            <%# Eval("TimeSpan")%></div>
                                                    </li>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </ul>
                                    </div>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litPhone" runat="server"></asp:Literal>
                                 <%--   <a href="dtdd">
                                        <asp:Image ID="imgPhone" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litTablet" runat="server"></asp:Literal>
                                   <%-- <a href="may-tinh-bang">
                                        <asp:Image ID="imgTablet" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litAccessory" runat="server"></asp:Literal>
                                  <%--  <a href="phu-kien">
                                        <asp:Image ID="imgAccessory" runat="server" />
                                    </a>--%>
                                </div>
                                <div class="block block-cart" style="border: 1px solid #bdbdbd">
                                    <asp:Literal ID="litApplication" runat="server"></asp:Literal>
                                  <%--  <a href="ung-dung">
                                        <asp:Image ID="imgApplication" runat="server" />
                                    </a>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
